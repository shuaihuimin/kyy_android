package com.xht.kuaiyouyi;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.alibaba.fastjson.JSONObject;
import com.xht.kuaiyouyi.ui.enterprise.bean.ApprovalDetailBean;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.xht.kuaiyouyi", appContext.getPackageName());
    }

    @Test
    public void testJson(){
        String jsonStr = "{\"flow_data\":[{\"status_name\":\"已同意\",\"approve_opinion\":\"\",\"status\":true,\"approve_time\":\"1534336247\",\"name\":\"兰博基尼\",\"names\":\"兰博基尼\",\"approve_mode\":0,\"approve_status\":\"2\"},{\"status\":false,\"names\":\"兰博基尼、思维\",\"approve_mode\":\"1\"},{\"status\":false,\"names\":\"兰博基尼、思维\",\"approve_mode\":\"2\"}],\"goods_info\":{\"order_main_state\":\"2001\",\"order_main_pay_active_name\":\"银行转账\",\"order_main_pay_currency_symbol\":\"HK$\",\"order_main_goods_total\":\"63500.00\",\"order_main_deal_active\":\"信用支付\",\"order_main_pay_two\":\"0.00\",\"supplement_amount\":\"0.00\",\"order_main_state_name\":\"待付款\",\"order_main_pay_currency_rate\":\"1.1393\",\"order_list\":[{\"goods_name\":\"劳力士Rolex MILGAUSS 116400GV-72400 自动机械钢带男表联保正品\",\"goods_pay_price\":\"63200.00\",\"goods_num\":\"1\",\"image_60_url\":\"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627750479728_60.png\",\"profit_rate\":0,\"goods_price\":\"63200.00\",\"goods_id\":\"100002\",\"customs_charges_fee\":0,\"freight_fee\":0},{\"goods_name\":\"【鼎力】自行走曲臂式GTBZ-AE\",\"goods_pay_price\":\"200.00\",\"goods_num\":\"2\",\"image_60_url\":\"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796293756348179_60.png\",\"profit_rate\":0,\"goods_price\":\"100.00\",\"goods_id\":\"100362\",\"customs_charges_fee\":0,\"freight_fee\":0},{\"goods_name\":\"惠普(HP) LaserJet Pro M1139 黑白激光多功能一体机 (打印 复印 扫描)\",\"goods_pay_price\":\"100.00\",\"goods_num\":\"1\",\"image_60_url\":\"http://192.168.0.2/data/upload/shop/store/goods/22/22_05803917426286848_60.jpg\",\"profit_rate\":0,\"goods_price\":\"100.00\",\"goods_id\":\"100477\",\"customs_charges_fee\":0,\"freight_fee\":0}],\"order_main_other_total\":\"0.00\",\"order_main_pay_currency_name\":\"港币\",\"beginTime\":{\"min\":49,\"sec\":58,\"hour\":10,\"day\":4},\"order_main_sn\":\"1000000000049601\",\"order_main_amount\":\"63500.00\",\"order_main_pay_one\":\"63500.00\"},\"copy_to_member\":[\"兰博基尼\"],\"approve_order_info\":{\"status_name\":\"兰博基尼、思维会签中\",\"is_approve\":false,\"status\":\"0\",\"approve_num\":\"2018081520120232687\",\"id\":\"495\",\"apply_reason\":\"个非官方个\",\"company_id\":\"91\",\"approve_id\":\"1491\",\"member_truename\":\"兰博基尼\",\"is_creator\":true,\"expect_delivery_time\":\"1535385600\"}}";
//        ApprovalDetailBean a = new Gson().fromJson(jsonStr, ApprovalDetailBean.class);
        ApprovalDetailBean a = JSONObject.parseObject(jsonStr,ApprovalDetailBean.class);
        a=null;
    }
}
