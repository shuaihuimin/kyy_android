package com.xht.kuaiyouyi.api

import android.os.Environment
import com.xht.kuaiyouyi.BuildConfig


/**
 * 常量类
 *
 * @author 俊华
 * @Time 2017-12-28
 * @Email 俊华
 */
object KyyConstants {

    const val ACT_LOGIN = "com.xht.kyy.act.login"
    /**
     * 本地缓存目录
     */
    val CACHE_DIR: String


    /**
     * 文件保存目录
     */
    val DOWNLOAD_DIR: String
    /**
     * 表情缓存目录
     */
    val CACHE_DIR_SMILEY: String

    /**
     * 图片缓存目录
     */
    val CACHE_DIR_IMAGE: String

    /**
     * 待上传图片缓存目录
     */
    val CACHE_DIR_UPLOADING_IMG: String


    /**
     * 登录标识
     */
    val REQUEST_LOGIN = 2

    /**
     * 登录成功广播返回标识
     */
    val LOGIN_SUCCESS = 0x22
    val SETTING = 0x12
    /**
     * 会员
     */
    val MEMBER = 0x156
    val LOGOUT = 4
    val GOODS_DETAILS = 0x123
    /**
     * IM新消息刷新页面返回标识
     */
    val IM_UPDATA_UI = "10"

    /**
     * IM好友列表状态刷新页面返回标识
     */
    val IM_FRIENDS_LIST_UPDATA_UI = "11"

    /**
     * 与服务器端连接的协议名
     */
    //val PROTOCOL=BuildConfig.PROTOCOL

    /**
     * 服务器域名
     */
    val HOST = BuildConfig.DOMAIN
    //	public static final String HOST = "b2b2c.shopnctest.com/test1";

    /**
     * IM服务器地址、端口号
     */
    //val IM_HOST = PROTOCOL + BuildConfig.IM_HOST

    /**
     * 服务器端口号
     */
    //val PORT = BuildConfig.PORT

    /**
     * 应用上下文名
     */
    //val APP = BuildConfig.APP

    /**
     * 应用上下文完整路径
     * 外网www.dev.kuaiyouyi.com
     * 内网http://192.168.0.2
     *
     */
    val URL_CONTEXTPATH = HOST+"mobile/index.php?"
    /**
     * 首页请求地址act=xht_index&op=index act=index&op=index
     *
     */
    val URL_HOME = URL_CONTEXTPATH + "act=xht_index&op=index"

    /**
     * 分页
     */
    val URL_LOAD= URL_CONTEXTPATH+"act=xht_kyy&op=index"

    /**
     * 一级分类请求地址act=xht_goods&op=goods_class  act=goods_class
     */
    val URL_GOODSCLASS = URL_CONTEXTPATH + "act=xht_goods&op=goods_class"

    /**
    * 分类筛选 act=xht_search&op=index
    */
    val URL_GET_GOODSCEEN= URL_CONTEXTPATH+"act=xht_search&op=index"

    /**
     * 商品详情请求地址
     */
    val URL_GOODSDETAILS = URL_CONTEXTPATH + "act=xht_goods&op=index"

    /**
     * 商品详情规格请求地址
     */
    val URL_GOODSSPEC = URL_CONTEXTPATH + "act=xht_goods&op=comments_allspec"
    /**
     * 登录请求地址act=xht_member&op=get_token  act=login
     */
    val URL_LOGIN = URL_CONTEXTPATH + "act=xht_member&op=get_token"
    /**
     * 安全验证
     */
    val URL_SAFETY_VERIFY = URL_CONTEXTPATH + "act=xht_member&op=ck_tel_safety"
    /**
     * 产品列表
     */
    val URL_GOODSLIST = URL_CONTEXTPATH + "act=xht_goods&op=goods_list"
    /**
     * 上传文件地址
     */
    val URL_UPLOADIMG = URL_CONTEXTPATH + "act=xht_company&op=upload"

    /**
     * 我的首页
     */
    val URL_MINE_INDEX= URL_CONTEXTPATH+"act=xht_member&op=mine_index_info"
    /*
     *汇率
     */
    val URL_EXCHANGE_RATE= URL_CONTEXTPATH+"act=xht_kyy&op=setcurrency"

    /**
     * 搜索公司地址
     */
    val URL_EP_SEARCH = URL_CONTEXTPATH + "act=xht_company&op=search"

    /**
     * 获取信用额度详情
     */
    val URL_EP_CREDIT = URL_CONTEXTPATH + "act=xht_company&op=credit"


    /**
     * 加入公司
     */
    val URL_EP_SAVEJOIN = URL_CONTEXTPATH + "act=xht_company&op=savejoin"

    /**
     * 我的公司主页
     */
    val URL_EP_COMPANY = URL_CONTEXTPATH + "act=xht_company&op=index"
    /**
     * 创建审批
     */
    val URL_CREATE_APPROVE= URL_CONTEXTPATH+"act=xht_company_approve&op=savesponsor"
    /**
     * 发起审批
     */
    val URL_ORDER_APPROVE= URL_CONTEXTPATH+"act=xht_approve&op=initiate"
    /**
     * 审批详情
     */
    val URL_EP_APPROVE_DETIAL = URL_CONTEXTPATH + "act=xht_approve&op=index"

    /**
     * 审批列表  act=xht_company_approve&op=copyTo&company_id=60
     *         act=xht_company_approve&op=copyTo&company_id=60
     */
    val URL_EP_APPROVAL_LIST = URL_CONTEXTPATH + "act=xht_company_approve&op=index"


    /**
     * 会员列表  act=xht_company_approve&op=copyTo&company_id=60
     *         act=xht_company_approve&op=copyTo&company_id=60
     */
    val URL_EP_COMPANY_USERS = URL_CONTEXTPATH + "act=xht_company&op=users"
    /**
     * 邀请成员
     */
    val URL_INVITE = URL_CONTEXTPATH + "act=xht_company&op=invite"

    /**
     * 店铺收藏列表
     */

    val URL_STORE_COLLECTION_LIST= URL_CONTEXTPATH+"act=xht_member_favorite_store&op=index"

    /*
     *店铺信息
     */

    val URL_STORE_CONTENT= URL_CONTEXTPATH+"act=xht_show_store&op=store_info"
    /**
     * 店铺商品、全部商品、新品上架
     */
    val URL_STORE_ALLGOODS= URL_CONTEXTPATH+"act=xht_show_store&op=index"

    /**
     * 公司档案
     */
    val URL_STORE_PROFILE= URL_CONTEXTPATH+"act=xht_show_store&op=company_profile"

    /**
     * 店铺分类
     */
    val URL_STORE_CLASS= URL_CONTEXTPATH+"act=xht_show_store&op=store_genre"
    /**
     *店铺收藏或者删除收藏
     */
    val URL_STORE_FOCUS= URL_CONTEXTPATH+"act=xht_member_favorite_store&op=favorite_store"

    /**
     * 收货地址列表请求地址
     */
    val URL_ADDRESS_LIST = URL_CONTEXTPATH + "act=xht_settings&op=address"
    /**
     * 取支票地址地址列表请求地址
     */
    val URL_CHECK_ADDRESS_LIST = URL_CONTEXTPATH + "act=xht_settings&op=address_check"


    /**
     * 添加购物车请求地址
     */
    val URL_ADD_CART = URL_CONTEXTPATH + "act=xht_cart&op=add"

    /*
     * 删除购物车
     */
    val URL_CLEAR_CART= URL_CONTEXTPATH+"act=xht_cart&op=delAll"


    /**
     * 购物车列表请求地址
     */
    val URL_CART_LIST = URL_CONTEXTPATH + "act=xht_cart&op=index"


//    /**
//     * 注册  act=xht_member&op=reg  act=login&op=register弃用登录与注册合成一个接口
//     */
//    val URL_REGISTER = URL_CONTEXTPATH + "act=xht_member&op=reg"

    /**
     * 会员聊天--会员信息 调用接口(get)
     * index.php?act=member_chat&op=get_info
     * 请求参数
     * key 当前登录令牌
     * u_id  会员编号
     * t 查询类型('member_id','member_name','store_id','member')
     *
     * act=xht_goods&op=mypage_num
     */
    val URL_MEMBER_CHAT_GET_USER_INFO = URL_CONTEXTPATH + "act=xht_goods&op=mypage_num"
    /**
     * 会员聊天--聊天记录查询
     * 调用接口(post)
     * index.php?act=member_chat&op=get_chat_log
     * 请求参数
     * key 当前登录令牌
     * t_id 接收消息会员编号
     * t 查询天数('7','15','30')
     * page 每页显示数量，可为空，默认为5个
     */

    //获取短信动态码           act=connect&op=get_sms_captcha         act=xht_member&op=get_sms_captcha1
    val URL_CONNECT_GET_SMS_CAPTCHA = URL_CONTEXTPATH + "act=xht_member&op=get_sms_captcha1"
    //验证短信动态码 act=xht_member&op=ck_sms_captcha   act=connect&op=check_sms_captcha
    val URL_CONNECT_CHECK_SMS_CAPTCHA = URL_CONTEXTPATH + "act=xht_member&op=ck_sms_captcha"

    //WAP找回密码  act=xht_member&op=reset_pwd
    val URL_FIND_PASSWORD = URL_CONTEXTPATH + "act=xht_member&op=reset_pwd"

    //修改密码
    val URL_MODIFY_PASSWORD= URL_CONTEXTPATH+"act=xht_settings&op=modify_password_update"

    //热搜
    val URL_SEARCH_HOT = URL_CONTEXTPATH + "act=xht_settings&op=hot"

    //公司列表
    val URL_EP_LIST = URL_CONTEXTPATH + "act=xht_company&op=get_mycompanies"
    //收藏商品列表
    val URL_FAVORITE_GOODS_LIST = URL_CONTEXTPATH + "act=xht_member_favorite_goods&op=index"

    //新增收货地址
    val URL_NEW_ADDRESS= URL_CONTEXTPATH+"act=xht_settings&op=address_addone"

    //新增收支票地址
    val URL_NEW_CHECK_ADDRESS= URL_CONTEXTPATH+"act=xht_settings&op=address_check_addone"

    //编辑收货地址
    val URL_EDIT_ADDRESS= URL_CONTEXTPATH+"act=xht_settings&op=address_editone"

    //编辑收支票地址
    val URL_EDIT_CHECK_ADDRESS= URL_CONTEXTPATH+"act=xht_settings&op=address_check_editone"

    //设置默认地址
    val URL_DEFAULT_ADDRESS= URL_CONTEXTPATH+"act=xht_settings&op=address_default"

    //设置收支票默认地址
    val URL_DEFAULT_CHECK_ADDRESS= URL_CONTEXTPATH+"act=xht_settings&op=address_check_default"

    //删除收支票地址
    val URL_DELETE_CHECK_ADDRESS= URL_CONTEXTPATH+"act=xht_settings&op=address_check_delete"

    //删除收货地址
    val URL_DELETE_ADDRESS= URL_CONTEXTPATH+"act=xht_settings&op=address_delete"

    //品牌馆
    val URL_BRAND_INDEX= URL_CONTEXTPATH+"act=xht_brand&op=index"
    /*
     *品牌馆详情
     */
    val URL_BRAND_DETILS= URL_CONTEXTPATH+"act=xht_brand&op=brand_name"

    //客服帮助
    val URL_SERVICE_HELP= URL_CONTEXTPATH+"act=xht_article&op=index"

    //工作通知列表
    val URL_WORK_NOTIFY_LIST= URL_CONTEXTPATH+"act=xht_messages&op=approvemsg"

    //系统公告列表
    val URL_SYSTEM_NOTICE_LIST= URL_CONTEXTPATH+"act=xht_messages&op=ggmsg";

    //验证消息列表
    val URL_VERIFY_MESSAGE_LIST= URL_CONTEXTPATH+"act=xht_messages&op=index"

    //系统消息列表
    val URL_SYSTEM_MESSAGE_LIST= URL_CONTEXTPATH+"act=xht_messages&op=system_msg";

    //验证消息中的同意与取消
    val URL_VERIFY_MESSAGE_ISAGREE= URL_CONTEXTPATH+"act=xht_messages&op=isagree"

    /**
     * 扫码登录
     */
    val URL_SWEEP_YARD_LOGIN= URL_CONTEXTPATH+"act=xht_member&op=scan_qrcode_login"

    //商品店铺加入收藏或者删除收藏
    val URL_FAVORITE= URL_CONTEXTPATH+"act=xht_member_favorite_goods&op=collect_new"


    //采购方式---公司列表
    val URL_ORDER_COMPANY_LIST= URL_CONTEXTPATH+"act=xht_order&op=get_company_list"

    /**
     * 确认订单
     */
    val URL_CONFIRM_ORDER= URL_CONTEXTPATH+"act=xht_order&op=index"
    /*
     *提交订单
     */
    val URL_SUBMIT_ORDER= URL_CONTEXTPATH+"act=xht_order&op=make_order"
    /*
     *订单详情
     */
    val URL_ORDER_DETAIL= URL_CONTEXTPATH+"act=xht_order_main&op=order_detail"

    //撤销审批
    val URL_APPROVAL_REPEAL= URL_CONTEXTPATH+"act=xht_company_approve&op=repeal"

    //审批意见
    val URL_APPROVAL_OPINION= URL_CONTEXTPATH+"act=xht_company_approve&op=approve"

    //审批人，抄送人
    val URL_APPROVAL_PEOPLE= URL_CONTEXTPATH+"act=xht_approve&op=getApproveConfig"

    //订单列表
    val URL_ORDER_LIST= URL_CONTEXTPATH+"act=xht_order&op=my_order_list"

    //取消订单
    val URL_ORDER_CANCEL= URL_CONTEXTPATH+"act=xht_order&op=cancel_order"

    //上传支票
    val URL_UPDATE_CHEQUE= URL_CONTEXTPATH+"act=xht_order_main&op=submit_bank_transfer"
    //在线支付(收银台)
    val URL_ORDER_PAY_CHECK= URL_CONTEXTPATH+"act=xht_order&op=pay"
    //在线支付
    val URL_ORDER_PAY= URL_CONTEXTPATH+"act=xht_payment&op=real_order"
    //支付回调结果
    val URL_PAY_RESULT= URL_CONTEXTPATH+"act=xht_payment&op=return_url"
    //物流信息
    val URL_LOGISTICS= URL_CONTEXTPATH+"act=xht_order_main&op=logistics"

    //未读消息的统计
    val URL_UNREAD_MSG= URL_CONTEXTPATH+"act=xht_messages&op=show_received_num";

    //工作通知/验证处理删除
    val URL_DELETE_WORK_VERIFY_MSG= URL_CONTEXTPATH+"act=xht_messages&op=del_company_msg";

    //系统消息的删除
    val URL_DELETE_SYSTEM_MSG= URL_CONTEXTPATH+"act=xht_messages&op=del_msg";

    //工作通知/验证消息 读取操作
    val URL_READ_COMPANY_MSG= URL_CONTEXTPATH+"act=xht_messages&op=readCompanyMessage"

    //系统消息读取操作
    val URL_READ_SYSTEM_MSG= URL_CONTEXTPATH+"act=xht_messages&op=readMessage"

    //购物车商品数量修改
    val URL_CART_UPDATE_NUM= URL_CONTEXTPATH+"act=xht_cart&op=update"


    //获取待付款和待我审批数量,主页企业模块的数量提示
    val URL_GET_MAIN_NUM = URL_CONTEXTPATH+"act=xht_company&op=company_message_all"


    val URL_UPADATE_VERSION = URL_CONTEXTPATH+"act=xht_kyy&op=versionUpdate";

    //二手市场
    val URL_SEACOND_MARKET= URL_CONTEXTPATH+"act=xht_second_hand&op=goods_list"

    //登出
    val URL_QUIT_LOGIN = URL_CONTEXTPATH+"act=xht_member&op=quit_login";




    //聊天列表
    val URL_CHAT_LIST = URL_CONTEXTPATH+"act=xht_chat&op=get_user_list";

    //聊天记录
    val URL_CHAT_LOG = URL_CONTEXTPATH+"act=xht_chat&op=get_chat_log";

    //聊天记录
    val URL_CHAT_UPDATE_MSG_READ = URL_CONTEXTPATH+"act=xht_chat&op=update_msg_read";

    init {
        if (Environment.MEDIA_MOUNTED == Environment
                .getExternalStorageState()) {
            CACHE_DIR = Environment.getExternalStorageDirectory().absolutePath + "/kuaiyouyi/"
            DOWNLOAD_DIR = Environment.getExternalStorageDirectory().absolutePath + "/kyy/"
        } else {
            CACHE_DIR = Environment.getRootDirectory().absolutePath + "/kuaiyouyi/"
            DOWNLOAD_DIR = Environment.getRootDirectory().absolutePath + "/kyy/"
        }
        CACHE_DIR_SMILEY = "$CACHE_DIR/smiley"
        CACHE_DIR_IMAGE = "$CACHE_DIR/pic"
        CACHE_DIR_UPLOADING_IMG = "$CACHE_DIR/uploading_img"
    }

}
