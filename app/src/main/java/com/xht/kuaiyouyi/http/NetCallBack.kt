package com.xht.kuaiyouyi.http

import android.support.annotation.NonNull
import android.util.Log
import com.xht.kuaiyouyi.bean.ResponseData
import java.lang.reflect.ParameterizedType


/**
 * Created by 俊华 on 2017/9/23.
 * 所有的okttp请求结果 基类
 */
interface NetCallBack<Result> {
    /**
     * 成功
     */
    fun onSuccess(@NonNull result: Result)

    /**
     * 失败  errCode 为自定义的ErrorStatus
     *
     */
    fun onFailure(@NonNull errCode: Int, @NonNull err: String)

    /**
     * 只能在kotlin语言中拿到，在java语言中处理过于复杂
     * 获取真实 Result 类型 如com.xht.kuaiyouyi.bean.CommentTags
     */
    fun getRealType(): Class<Result> {
        val gs = this@NetCallBack.javaClass.genericInterfaces[0] as ParameterizedType
        return gs.actualTypeArguments[0] as Class<Result>
    }

}