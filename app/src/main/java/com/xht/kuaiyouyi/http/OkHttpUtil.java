package com.xht.kuaiyouyi.http;

import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.SignUtil;
import com.xht.kuaiyouyi.utils.SystemUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Administrator on 2018/6/18.
 */

public class OkHttpUtil {
    private final OkHttpClient client;
    private final String uuid;
    private Handler handler;
    public OkHttpUtil(Handler handler) {
        this.handler = handler;
        uuid = Settings.Secure.getString(KyyApp.context.getContentResolver(), Settings.Secure.ANDROID_ID);
        client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)      //设置连接超时
                .readTimeout(60, TimeUnit.SECONDS)         //设置读超时
                .writeTimeout(60, TimeUnit.SECONDS)        //设置写超时
                .retryOnConnectionFailure(true)            //是否自动重连
                .build();
    }

    public void post(String url) {
        String t = SystemUtil.INSTANCE.getTime(SystemUtil.INSTANCE.getTimeStr(System.currentTimeMillis()+""));
        Map<String, Object> map = new HashMap<>();
        map.put("device_uuid", uuid);
        map.put("device_name", Build.MODEL);
        map.put("sign", SignUtil.sign(map, t));//参数逻辑
        map.put("token_id", Login.Companion.getInstance().getToken_id());
        map.put("sign_time", t);
        FormBody.Builder builder = new FormBody.Builder();
        for (Map.Entry object : map.entrySet()) {
            String key = (String) object.getKey();
            String value = object.getValue().toString();
            builder.add(key, value);
        }
        Request request = new Request.Builder().url(url).post(builder.build()).build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i("11111", "eee=" + e.getLocalizedMessage());
                handler.sendMessage(handler.obtainMessage(2,e.getLocalizedMessage()));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String str = response.body().string();
                Log.i("11111", "str=" + str);
                if (response!=null){
                    if (response.isSuccessful()){
                        handler.sendMessage(handler.obtainMessage(1,str));
                    }
                }

            }
        });
    }
}
