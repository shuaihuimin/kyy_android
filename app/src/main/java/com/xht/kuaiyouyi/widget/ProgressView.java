package com.xht.kuaiyouyi.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.utils.Utils;

public class ProgressView extends View {
    private Paint mPaintIn;
    private Paint mPaintOut;

    private float left;
    private float top;
    private float right;
    private float bottom;
    private int mProgress = 0;

    private RectF mInRecF;
    private RectF mOutRecF;
    private int mCurrentProgress;//实际的进度

    private android.os.Handler mHandler = new android.os.Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(Message msg) {
            if(msg.what==1){
                if(mProgress<mCurrentProgress){
                    mProgress=mProgress+3;
                    invalidate();
                }else {
                    mHandler.removeMessages(1);
                }
            }
        }
    };

    public ProgressView(Context context) {
        super(context);
        init();
    }

    public ProgressView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        left = Utils.dp2px(getContext(), 15f) / 2f;
        top = Utils.dp2px(getContext(), 15f) / 2f;
        right = width - Utils.dp2px(getContext(), 15f) / 2f;
        bottom = MeasureSpec.getSize(heightMeasureSpec) - Utils.dp2px(getContext(), 15f) / 2f;
        mInRecF.left = left;
        mInRecF.top = top;
        mInRecF.right = right;
        mInRecF.bottom = bottom;
        mOutRecF.left = left;
        mOutRecF.top = top;
        mOutRecF.right = right;
        mOutRecF.bottom = bottom;
    }

    private void init() {
        mPaintIn = new Paint();
        mPaintIn.setColor(getResources().getColor(R.color.grey_8));
        mPaintIn.setStrokeWidth(Utils.dp2px(getContext(), 15f));
        mPaintIn.setStyle(Paint.Style.STROKE);
        mPaintIn.setAntiAlias(true);
        mInRecF = new RectF();

        mPaintOut = new Paint();
        mPaintOut.setStrokeWidth(Utils.dp2px(getContext(), 15f));
        mPaintOut.setAntiAlias(true);
        mPaintOut.setStyle(Paint.Style.STROKE);
        mPaintOut.setColor(getResources().getColor(R.color.blue_normal));
        mPaintOut.setStrokeCap(Paint.Cap.ROUND);
        mOutRecF = new RectF();


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(mInRecF, 0, 360, false, mPaintIn);
        if(mProgress >0){
            canvas.drawArc(mOutRecF, 270, -mProgress, false, mPaintOut);
            if(mProgress<mCurrentProgress){
                mHandler.sendEmptyMessageDelayed(1,1L);
            }else {
                mHandler.removeMessages(1);
            }
        }
    }

    public void setProgess(int progess) {
        this.mCurrentProgress = progess*360/100;
        mHandler.sendEmptyMessage(1);
    }
}
