package com.xht.kuaiyouyi.widget;

import android.content.Context;
import android.view.View;

import com.xht.kuaiyouyi.R;

import q.rorbin.badgeview.QBadgeView;

public class BadgeView extends QBadgeView {
    public BadgeView(Context context, View view, int textSize, float padding) {
        super(context);
        bindTarget(view);
        setBadgeTextSize(textSize, true);
        setBadgeBackgroundColor(getResources().getColor(R.color.orange_1));
        setBadgePadding(padding, true);
        setBadgeNumber(0);
        setShowShadow(false);
    }
}
