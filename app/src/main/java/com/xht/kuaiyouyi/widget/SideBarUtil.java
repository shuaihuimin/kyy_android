package com.xht.kuaiyouyi.widget;

import com.xht.kuaiyouyi.ui.home.entity.BrandListBean;

import java.util.ArrayList;
import java.util.List;

public class SideBarUtil {
    /**
     * 根据当前选中的项获取其第一次出现该项首字母的位置
     *
     * @param position 当前选中的位置
     * @param datas    数据源
     * @return
     */
    public static int getPositionForSection(int position, List<BrandListBean.BrandBean.DataBean> datas) {
        // 当前选中的项
        BrandListBean.BrandBean.DataBean sideBase = datas.get(position);
        for (int i = 0; i < datas.size(); i++) {
            String firstStr = datas.get(i).getBrand_initial().toUpperCase();
            // 返回第一次出现该项首字母的位置
            if (firstStr.equals(sideBase.getBrand_initial())) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 根据当前选中的项是否是该字母最后出现的位置
     *
     * @param position 当前选中的位置
     * @param datas    数据源
     * @return
     */
    public static boolean isLast(int position, List<BrandListBean.BrandBean.DataBean> datas) {
        // 当前选中的项
        BrandListBean.BrandBean.DataBean dataBean = datas.get(position);
        //是最后一条记录的时候
        if (position == datas.size() - 1) {
            return true;
        }

        return !dataBean.getBrand_initial().toUpperCase().equals(datas.get(position + 1).getBrand_initial());
    }

    /**
     * 获取所选中的索引在列表中的位置
     *
     * @param list
     * @param letter
     * @return
     */
    public static int getLetterPosition(List<BrandListBean.BrandBean.DataBean> list, String letter) {
        int position = -1;

        if (list != null && !list.isEmpty() && !"".equals(letter)) {
            for (int i = 0; i < list.size(); i++) {
                BrandListBean.BrandBean.DataBean bean = list.get(i);
                if (bean.getBrand_initial().equals(letter)) {
                    position = i;
                    break;
                }
            }
        }
        return position;
    }

    /**
     * 筛选出数据源中所包含的全部索引值
     *
     * @param list
     * @return
     */
    public static String[] getLetters(List<BrandListBean.BrandBean.DataBean> list) {
        List<String> letters = new ArrayList<>();
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                if (!letters.contains(list.get(i).getBrand_initial())) {
                    letters.add(list.get(i).getBrand_initial());
                }
            }
        }
        return (String[]) letters.toArray(new String[letters.size()]);
    }
}
