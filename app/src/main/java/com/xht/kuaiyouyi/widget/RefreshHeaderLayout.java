package com.xht.kuaiyouyi.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshKernel;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.xht.kuaiyouyi.R;

public class RefreshHeaderLayout extends LinearLayout implements RefreshHeader {
    private TextView tv_two_level;
    private TextView tv_title;
    private TextView tv_status;
    private ImageView iv_refresh_logo;
    private RelativeLayout rl_head;
    private Animation mAnimation;
    public RefreshHeaderLayout(Context context) {
        super(context);
        initView(context);
    }

    public RefreshHeaderLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public RefreshHeaderLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater.from(context).inflate(R.layout.refresh_header_layout, this);
        rl_head = findViewById(R.id.rl_head);
        tv_two_level = findViewById(R.id.tv_two_level);
        tv_title = findViewById(R.id.tv_title);
        tv_status = findViewById(R.id.tv_status);
        iv_refresh_logo = findViewById(R.id.iv_refresh_logo);
        mAnimation = AnimationUtils.loadAnimation(context,R.anim.anim_round_rotate);
    }


    @NonNull
    @Override
    public View getView() {
        return this;
    }

    @NonNull
    @Override
    public SpinnerStyle getSpinnerStyle() {
        return SpinnerStyle.Translate;
    }

    @Override
    public void setPrimaryColors(int... colors) {

    }

    public void setHeaderBackgroundResource(int backgroundColorRes){
        rl_head.setBackgroundResource(backgroundColorRes);
    }

    public void setHeaderIconResource(int imageRes){
        iv_refresh_logo.setImageResource(imageRes);
    }

    public void setTextColor(int titleColorRes,int statusColorRes,int twoLevelRes){
        tv_title.setTextColor(titleColorRes);
        tv_status.setTextColor(statusColorRes);
        tv_two_level.setTextColor(twoLevelRes);
    }

    @Override
    public void onStartAnimator(@NonNull RefreshLayout layout, int height, int extendHeight) {
        iv_refresh_logo.startAnimation(mAnimation);
    }

    @Override
    public int onFinish(@NonNull RefreshLayout layout, boolean success) {
        iv_refresh_logo.clearAnimation();
        return 0;
    }

    @Override
    public void onStateChanged(RefreshLayout refreshLayout, RefreshState oldState, RefreshState newState) {
        switch (newState) {
            case None:
            case PullDownToRefresh:
                tv_status.setVisibility(View.VISIBLE);
                tv_title.setVisibility(View.VISIBLE);
                tv_status.setText(R.string.refresh_header_pulldown);
                tv_two_level.setVisibility(View.INVISIBLE);
                break;
            case Refreshing:
                tv_status.setVisibility(View.VISIBLE);
                tv_title.setVisibility(View.VISIBLE);
                tv_two_level.setVisibility(View.INVISIBLE);
                tv_status.setText(R.string.refresh_header_refreshing);
                break;
            case ReleaseToRefresh:
                tv_status.setVisibility(View.VISIBLE);
                tv_title.setVisibility(View.VISIBLE);
                tv_two_level.setVisibility(View.INVISIBLE);
                tv_status.setText(R.string.refresh_header_release);
                break;
            case ReleaseToTwoLevel:
                tv_status.setVisibility(View.INVISIBLE);
                tv_title.setVisibility(View.INVISIBLE);
                tv_two_level.setVisibility(View.VISIBLE);
                break;
    }
    }


    @Override
    public boolean isSupportHorizontalDrag() {
        return false;
    }

    @Override
    public void onInitialized(@NonNull RefreshKernel kernel, int height, int extendHeight) {
        //设置下拉刷新的背景
        kernel.requestDrawBackgroundFor(this,ContextCompat.getColor(getContext(), R.color.grey_bg));
    }

    @Override
    public void onMoving(boolean isDragging, float percent, int offset, int height, int maxDragHeight) {

    }

    @Override
    public void onHorizontalDrag(float percentX, int offsetX, int offsetMax) {

    }

    @Override
    public void onReleased(RefreshLayout refreshLayout, int height, int extendHeight) {

    }

}
