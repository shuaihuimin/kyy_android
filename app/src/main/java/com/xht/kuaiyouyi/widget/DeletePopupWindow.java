package com.xht.kuaiyouyi.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.xht.kuaiyouyi.R;

public class DeletePopupWindow {
    private Context mContext;
    private OnClickDeleteListener mOnClickDeleteListener;
    private PopupWindow mPopupWindow;
    private View itemView;

    public interface OnClickDeleteListener {
        void onDelete();
    }

    public void setOnClickDeleteListener(OnClickDeleteListener listener) {
        mOnClickDeleteListener = listener;
    }

    public DeletePopupWindow(Context context,View view) {
        mContext = context;
        this.itemView = view;
    }

    public DeletePopupWindow show() {
        //popupwindow的宽与高
        int pWidth=0;
        int pHeight=0;

        if(mPopupWindow==null){
            mPopupWindow = new PopupWindow(mContext);
            View view = LayoutInflater.from(mContext).inflate(R.layout.delete_button, null);
            mPopupWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
            mPopupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            mPopupWindow.setContentView(view);
            mPopupWindow.setFocusable(true);
            mPopupWindow.setOutsideTouchable(true);
            mPopupWindow.setBackgroundDrawable(new ColorDrawable(mContext.getResources().getColor(android.R.color.transparent)));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mOnClickDeleteListener!=null){
                        mOnClickDeleteListener.onDelete();
                    }
                    mPopupWindow.dismiss();
                }
            });
            int width =View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
            int height =View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
            view.measure(width,height);
            pHeight=view.getMeasuredHeight();
            pWidth=view.getMeasuredWidth();
        }
        mPopupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE && mPopupWindow.isShowing()) {
                    mPopupWindow.dismiss();
                    return false;
                }
                return false;
            }
        });


        mPopupWindow.showAsDropDown(itemView,itemView.getWidth()/2-pWidth/2,-pHeight-itemView.getHeight()/2);
        return this;

    }


}
