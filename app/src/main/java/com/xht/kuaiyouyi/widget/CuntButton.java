package com.xht.kuaiyouyi.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

import com.xht.kuaiyouyi.R;

/**
 * 倒计时按钮
 */
@SuppressLint("AppCompatCustomView")
public class CuntButton extends Button{


    private String text;
    private String content;


    public CuntButton(Context context) {
        this(context, null);
    }

    public CuntButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CuntButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getResources().obtainAttributes(attrs, R.styleable.CountButton);
        long countDownInterval = typedArray.getInteger(R.styleable.CountButton_countDownInterval, 1)*1000;
        long millisInFuture = typedArray.getInteger(R.styleable.CountButton_millisInFuture, 5)*1000;


        timer = new MyCount(millisInFuture, countDownInterval);

        typedArray.recycle();

    }


    public void start() {
        content = getText().toString().trim();
        timer.start();
        this.setEnabled(false);
    }

    public void cancel() {
        timer.cancel();
    }


    private MyCount timer;

    private class MyCount extends CountDownTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

            String time = "" + (millisUntilFinished / 1000);

            Log.i(TAG, "onTick: "+time);
            CuntButton.this.setText("( "+time+" S )");
        }

        private static final String TAG = "MyCount";

        @Override
        public void onFinish() {
            CuntButton.this.setText(content);
            CuntButton.this.setEnabled(true);

        }
    }

}
