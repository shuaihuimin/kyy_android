package com.xht.kuaiyouyi.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshKernel;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.xht.kuaiyouyi.R;

public class RefreshFooterLayout extends LinearLayout implements RefreshFooter {
    private TextView tv_status;
    private View view_left_line, view_right_line;
    private ImageView iv_load_logo;
    private Animation mAnimation;
    private LinearLayout linearLayout;
    protected boolean mLoadmoreFinished = false;

    public RefreshFooterLayout(Context context) {
        super(context);
        initView(context);
    }

    public RefreshFooterLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public RefreshFooterLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater.from(context).inflate(R.layout.refresh_footer_layout, this);
        tv_status = findViewById(R.id.tv_status);
        view_left_line = findViewById(R.id.view_left_line);
        view_right_line = findViewById(R.id.view_right_line);
        iv_load_logo = findViewById(R.id.iv_load_logo);
        linearLayout=findViewById(R.id.linear);
        mAnimation = AnimationUtils.loadAnimation(context,R.anim.anim_round_rotate);
    }


    @NonNull
    @Override
    public View getView() {
        return this;
    }

    @NonNull
    @Override
    public SpinnerStyle getSpinnerStyle() {
        return SpinnerStyle.Translate;
    }


    @Override
    public void onStartAnimator(@NonNull RefreshLayout layout, int height, int extendHeight) {
        iv_load_logo.startAnimation(mAnimation);
    }

    @Override
    public int onFinish(@NonNull RefreshLayout layout, boolean success) {
        if(!mLoadmoreFinished){
            iv_load_logo.clearAnimation();
            return 500;
        }
        return 0;
    }

    @Override
    public void onStateChanged(RefreshLayout refreshLayout, RefreshState oldState, RefreshState newState) {
        if(!mLoadmoreFinished){
            switch (newState) {
                case None:
                case Loading:
                    tv_status.setText(R.string.refresh_footer_loading);
                    linearLayout.setVisibility(VISIBLE);
                    view_left_line.setVisibility(GONE);
                    view_right_line.setVisibility(GONE);
                    iv_load_logo.setVisibility(VISIBLE);
                    break;
                case LoadFinish:
//                    tv_status.setText("");
                    linearLayout.setVisibility(GONE);
                    view_left_line.setVisibility(GONE);
                    view_right_line.setVisibility(GONE);
                    iv_load_logo.setVisibility(GONE);
                    break;
            }
        }
    }


    @Override
    public boolean isSupportHorizontalDrag() {
        return false;
    }

    @Override
    public void onInitialized(@NonNull RefreshKernel kernel, int height, int extendHeight) {

    }

    @Override
    public void onMoving(boolean isDragging, float percent, int offset, int height, int maxDragHeight) {

    }


    @Override
    public void onReleased(RefreshLayout refreshLayout, int height, int extendHeight) {

    }

    @Override
    public void onHorizontalDrag(float percentX, int offsetX, int offsetMax) {

    }

    @Override
    public void setPrimaryColors(int... colors) {

    }

    @Override
    public boolean setNoMoreData(boolean noMoreData) {
        if(mLoadmoreFinished!=noMoreData){
            mLoadmoreFinished = noMoreData;
            if(noMoreData){
                tv_status.setText(R.string.refresh_footer_allloaded);
                linearLayout.setVisibility(VISIBLE);
                view_left_line.setVisibility(VISIBLE);
                view_right_line.setVisibility(VISIBLE);
                iv_load_logo.setVisibility(GONE);
                iv_load_logo.clearAnimation();
            }
        }
        return true;
    }

}
