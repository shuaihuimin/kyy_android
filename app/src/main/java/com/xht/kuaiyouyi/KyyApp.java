package com.xht.kuaiyouyi;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Process;
import android.support.multidex.MultiDex;
import android.webkit.WebView;

import com.huawei.android.hms.agent.HMSAgent;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshFooterCreator;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreator;
import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.squareup.leakcanary.LeakCanary;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;
import com.xht.kuaiyouyi.ui.cart.entity.CartIndexBean;
import com.xht.kuaiyouyi.ui.message.util.IMUtil;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.RefreshFooterLayout;
import com.xht.kuaiyouyi.widget.RefreshHeaderLayout;

import org.litepal.LitePal;
import org.litepal.LitePalDB;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by shuaihuimin on 2018/6/13.
 */

public class KyyApp extends Application {
    public static Context context;
    private boolean isSettedLanguage;//是否已经设置了语言
    public static int activitynum;
    public CartIndexBean cartIndexBean;
    @Override
    public void onCreate() {
        super.onCreate();
        //判断是不是主进程，防止多次初始化，解决IM模块一条消息会接收两次的问题
        if(Utils.getProcessName(this, Process.myPid()).equals(BuildConfig.APPLICATION_ID)){
            context = getApplicationContext();
            CrashReport.initCrashReport(getApplicationContext(), BuildConfig.BUGLY_ID, BuildConfig.IS_BUGLY);
            LitePal.initialize(this);
            if (LeakCanary.isInAnalyzerProcess(this)) {
                return;
            }
            LeakCanary.install(this);

            if(Utils.isHUAWEI()){
                HMSAgent.init(this);
            }else {
                JPushInterface.init(this);
            }
            if (Login.Companion.getInstance().isLogin()) {
                IMUtil.initIM();
                Utils.getUnreadMsg();
                LitePal.use(LitePalDB.fromDefault("kyy_" + Login.Companion.getInstance().getUid()));
                if(Login.Companion.getInstance().is_set_alias().equals("0")){
                    JPushInterface.setAlias(this,0,Login.Companion.getInstance().getUid());
                    Login.Companion.getInstance().set_set_alias("1");
                }
            }

            //注册生命周期的监听
            registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
                @Override
                public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                    activitynum++;
                    if(!isSettedLanguage){
                        Utils.setLanguage(activity);
                        isSettedLanguage = true;
                    }
                }

                @Override
                public void onActivityStarted(Activity activity) {
                }

                @Override
                public void onActivityResumed(Activity activity) {

                }

                @Override
                public void onActivityPaused(Activity activity) {

                }

                @Override
                public void onActivityStopped(Activity activity) {

                }

                @Override
                public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

                }

                @Override
                public void onActivityDestroyed(Activity activity) {
                    activitynum--;
                }
            });
        }
        new WebView(this).destroy();

        //友盟初始化
        UMConfigure.init(this,BuildConfig.UMENG_ID
                ,"kuaiyouyi",UMConfigure.DEVICE_TYPE_PHONE,null);
        UMConfigure.setEncryptEnabled(true);
        PlatformConfig.setDing(BuildConfig.DING_ID);
        PlatformConfig.setWeixin(BuildConfig.WEIXIN_ID,BuildConfig.App_SING);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    static {
        //设置header
        SmartRefreshLayout.setDefaultRefreshHeaderCreator(new DefaultRefreshHeaderCreator() {
            @Override
            public RefreshHeader createRefreshHeader(Context context, RefreshLayout layout) {
                return new RefreshHeaderLayout(context);
            }
        });
        //设置footer
        SmartRefreshLayout.setDefaultRefreshFooterCreator(new DefaultRefreshFooterCreator() {
            @Override
            public RefreshFooter createRefreshFooter(Context context, RefreshLayout layout) {
                return new RefreshFooterLayout(context);
            }
        });
    }

}
