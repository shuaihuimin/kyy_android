package com.xht.kuaiyouyi.utils

import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.xht.kuaiyouyi.KyyApp



/**
 * Created 俊华 jh on 2017/9/20.
 * toast Android8.0需要权限
 */
object ToastU {
    private var toast: Toast? = null
    private var oneTime: Long = 0L
    private var twoTime: Long = 0L
    private  var oldMsg: String?=""

    fun showToast( s: String?) {

        if (TextUtils.isEmpty(s)){
            return
        }
        Log.e("showToast", "--name : "+ Thread.currentThread().name+"，msg="+s)
        if (toast == null) {
            toast = Toast.makeText(KyyApp.context, s, Toast.LENGTH_SHORT)
            toast?.show()
            oneTime = System.currentTimeMillis()
        } else {
            twoTime = System.currentTimeMillis()
            if (s .equals(oldMsg) ) {
                if (twoTime - oneTime > Toast.LENGTH_SHORT) {
                    toast ?.show()
                }
            } else {
                oldMsg = s
                toast ?.setText(ZFChange.setText(s as String))
                toast ?.show()
            }
        }
        oneTime = twoTime
    }

    fun showLongToast( s: String) {
        Log.e("showToast", "--name : "+ Thread.currentThread().name)
        if (toast == null) {
            toast = Toast.makeText(KyyApp.context, s, Toast.LENGTH_LONG)
            toast?.show()
            oneTime = System.currentTimeMillis()
        } else {
            twoTime = System.currentTimeMillis()
            if (s .equals(oldMsg) ) {
                if (twoTime - oneTime > Toast.LENGTH_LONG) {
                    toast ?.show()
                }
            } else {
                oldMsg = s
                toast ?.setText(ZFChange.setText(s))
                toast ?.show()
            }
        }
        oneTime = twoTime
    }

    fun showToast( resId: Int) {
        showToast(KyyApp.context.getString(resId) as String)
    }
}