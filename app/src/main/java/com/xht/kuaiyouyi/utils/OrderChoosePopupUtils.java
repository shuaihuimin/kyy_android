package com.xht.kuaiyouyi.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.cart.adapter.OrderPopuAdapter;
import com.xht.kuaiyouyi.ui.cart.entity.OrderChooseBean;

import java.util.List;

public class OrderChoosePopupUtils {
    private static CommonPopupWindow popupWindow;
    private static int select=0;
    private static String content;

    public static void choosepopp(final Context context, final List<OrderChooseBean> list, String title){
        final View upView = LayoutInflater.from(KyyApp.context).inflate(R.layout.dialog_procurement_method, null);
        //测量View的宽高
        CommonUtil.measureWidthAndHeight(upView);
        if (popupWindow == null ){
            popupWindow = new CommonPopupWindow.Builder(KyyApp.context)
                    .setView(upView)
                    .setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    .setBackGroundLevel(0.5f)//取值范围0.0f-1.0f 值越小越暗
                    .create();
            TextView tv_title=upView.findViewById(R.id.tv_dialog_procurement_method);
            tv_title.setText(title);
            Button btnsubmit=upView.findViewById(R.id.bt_submit);
            btnsubmit.setVisibility(View.GONE);
            ListView listview=upView.findViewById(R.id.lv_company_list);
            list.get(select).setSelect(true);
            final OrderPopuAdapter orderChooseAdapter = new OrderPopuAdapter(context,list);
            listview.setAdapter(orderChooseAdapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    list.get(position).setSelect(true);
                    select=position;
                    orderChooseAdapter.notifyDataSetChanged();
                }
            });
            btnsubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    content=list.get(select).getName();
                    popupWindow.dismiss();
                }
            });
        }
        popupWindow.showAtLocation(upView, Gravity.BOTTOM, 0, 0);
    }

    public static String getcontent(){
        return content;
    }

    public static void movepop(){
        if(popupWindow!=null){
            popupWindow.dismiss();
        }
    }

}
