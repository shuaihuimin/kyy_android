package com.xht.kuaiyouyi.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.widget.FullScreenDialog;

public class DialogUtils {
    public static final int NO_ICON = -1; // 无图标
    private static FullScreenDialog fullScreenDialog=null;

    /**
     * * 创建消息对话框 *
     *
     * @param context  上下文 必填
     * @param iconId   图标，如： R.drawable.icon 或 DialogTool.NO_ICON 必填
     * @param title    标题 必填
     * @param message  显示内容 必填
     * @param btnName  按钮名称 必填
     * @param listener 监听器，需实现 android.content.DialogInterface.OnClickListener 接口 必填
     * @return
     */
    public static Dialog createMessageDialog(Context context, String title,
                                             String message, String btnName, OnClickListener listener, int iconId) {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (iconId != NO_ICON) {
            // 设置对话框图标
            builder.setIcon(iconId);
        }
        // 设置对话框标题
        builder.setTitle(title);
        // 设置对话框消息
        builder.setMessage(message);
        // 设置按钮
        builder.setPositiveButton(btnName, listener);
        // 创建一个消息对话框
        dialog = builder.create();
        return dialog;
    }

    /**
     * * 创建警示（确认、取消）对话框 *
     *
     * @param context             上下文 必填
     * @param iconId              图标，如： R.drawable.icon 或 DialogTool.NO_ICON 必填
     * @param title               标题 必填
     * @param message             显示内容 必填
     * @param positiveBtnName     确定按钮名称 必填
     * @param negativeBtnName     取消按钮名称 必填
     * @param positiveBtnListener 监听器，需实现 android.content.DialogInterface.OnClickListener接口 必填
     * @param negativeBtnListener 监听器，需实现 android.content.DialogInterface.OnClickListener接口 必填
     * @return
     */
    public static Dialog createConfirmDialog(Context context, String title,
                                             String message, String positiveBtnName, String negativeBtnName,
                                             OnClickListener positiveBtnListener,
                                             OnClickListener negativeBtnListener, int iconId) {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (iconId != NO_ICON) {
            // 设置对话框图标
            builder.setIcon(iconId);
        }
        // 设置对话框标题
        builder.setTitle(title);
        // 设置对话框消息
        builder.setMessage(message);
        // 设置确定按钮
        builder.setPositiveButton(positiveBtnName, positiveBtnListener);
        // 设置取消按钮
        builder.setNegativeButton(negativeBtnName, negativeBtnListener);
        // 创建一个消息对话框
        dialog = builder.create();
        return dialog;
    }

    /**
     * * 创建单选对话框 *
     *
     * @param context             上下文 必填
     * @param iconId              图标，如： R.drawable.icon 或 DialogTool.NO_ICON 必填
     * @param title               标题 必填
     * @param itemsString         选择项 必填
     * @param positiveBtnName     确定按钮名称 必填
     * @param negativeBtnName     取消按钮名称 必填
     * @param positiveBtnListener 监听器，需实现 android.content.DialogInterface.OnClickListener 接口 必填
     * @param negativeBtnListener 监听器，需实现 android.content.DialogInterface.OnClickListener 接口 必填
     * @param itemClickListener   监听器，需实现 android.content.DialogInterface.OnClickListener 接口 必填
     * @return
     */
    public static Dialog createSingleChoiceDialog(Context context,
                                                  String title, String[] itemsString, String positiveBtnName,
                                                  String negativeBtnName, OnClickListener positiveBtnListener,
                                                  OnClickListener negativeBtnListener,
                                                  OnClickListener itemClickListener, int iconId) {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (iconId != NO_ICON) {
            // 设置对话框图标
            builder.setIcon(iconId);
        }
        // 设置对话框标题
        builder.setTitle(title);
        // 设置单选选项, 参数0: 默认第一个单选按钮被选中
        builder.setSingleChoiceItems(itemsString, 0, itemClickListener);
        // 设置确定按钮
        builder.setPositiveButton(positiveBtnName, positiveBtnListener);
        // 设置取消按钮
        builder.setNegativeButton(negativeBtnName, negativeBtnListener);
        // 创建一个消息对话框
        dialog = builder.create();
        return dialog;
    }

    /**
     * * 创建复选对话框 *
     *
     * @param context             上下文 必填
     * @param iconId              图标，如： R.drawable.icon 或 DialogTool.NO_ICON 必填
     * @param title               标题 必填
     * @param itemsString         选择项 必填
     * @param positiveBtnName     确定按钮名称 必填
     * @param negativeBtnName     取消按钮名称 必填
     * @param positiveBtnListener 监听器，需实现 android.content.DialogInterface.OnClickListener 接口 必填
     * @param negativeBtnListener 监听器，需实现 android.content.DialogInterface.OnClickListener 接口 必填
     * @param itemClickListener   监听器，需实现
     *                            android.content.DialogInterface.OnMultiChoiceClickListener接口
     *                            必填
     * @return
     */
    public static Dialog createMultiChoiceDialog(Context context, String title,
                                                 String[] itemsString, String positiveBtnName,
                                                 String negativeBtnName, OnClickListener positiveBtnListener,
                                                 OnClickListener negativeBtnListener,
                                                 OnMultiChoiceClickListener itemClickListener, int iconId) {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (iconId != NO_ICON) {
            // 设置对话框图标
            builder.setIcon(iconId);
        }
        // 设置对话框标题
        builder.setTitle(title);
        // 设置选项
        builder.setMultiChoiceItems(itemsString, null, itemClickListener);
        // 设置确定按钮
        builder.setPositiveButton(positiveBtnName, positiveBtnListener);
        // 设置确定按钮
        builder.setNegativeButton(negativeBtnName, negativeBtnListener);
        // 创建一个消息对话框
        dialog = builder.create();
        return dialog;
    }

    /**
     * * 创建列表对话框 *
     *
     * @param context             上下文 必填
     * @param iconId              图标，如： R.drawable.icon 或 DialogTool.NO_ICON 必填
     * @param title               标题 必填
     * @param itemsString         列表项 必填
     * @param negativeBtnName     取消按钮名称 必填
     * @param negativeBtnListener 监听器，需实现 android.content.DialogInterface.OnClickListener 接口 必填
     * @return
     */
    public static Dialog createListDialog(Context context, String title,
                                          String[] itemsString, String negativeBtnName,
                                          OnClickListener negativeBtnListener,
                                          OnClickListener itemClickListener, int iconId) {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (iconId != NO_ICON) {
            // 设置对话框图标
            builder.setIcon(iconId);
        }
        // 设置对话框标题
        builder.setTitle(title);
        // 设置列表选项
        builder.setItems(itemsString, itemClickListener);
        // 设置确定按钮
        builder.setNegativeButton(negativeBtnName, negativeBtnListener);
        // 创建一个消息对话框
        dialog = builder.create();
        return dialog;
    }

    /**
     * * 创建自定义（含确认、取消）对话框 *
     *
     * @param context             上下文 必填
     * @param iconId              图标，如： R.drawable.icon 或 DialogTool.NO_ICON 必填
     * @param title               标题 必填
     * @param positiveBtnName     确定按钮名称 必填
     * @param negativeBtnName     取消按钮名称 必填
     * @param positiveBtnListener 监听器，需实现 android.content.DialogInterface.OnClickListener 接口 必填
     * @param negativeBtnListener 监听器，需实现 android.content.DialogInterface.OnClickListener 接口 必填
     * @param view                对话框中自定义视图 必填
     * @return
     */
    public static Dialog createRandomDialog(Context context, String title,
                                            String positiveBtnName, String negativeBtnName,
                                            OnClickListener positiveBtnListener,
                                            OnClickListener negativeBtnListener, View view, int iconId) {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (iconId != NO_ICON) {
            // 设置对话框图标
            builder.setIcon(iconId);
        }
        // 设置对话框标题
        builder.setTitle(title);
        builder.setView(view);
        // 设置确定按钮
        builder.setPositiveButton(positiveBtnName, positiveBtnListener);
        // 设置确定按钮
        builder.setNegativeButton(negativeBtnName, negativeBtnListener);
        // 创建一个消息对话框
        dialog = builder.create();
        return dialog;
    }


    /**
     *
     * @param context  上下文
     * @param message 显示内容
     * @param rightBtnName 右边按钮的文字
     * @param leftBtnName  左边按钮的文字
     * @param onRightBtnListener  右边按钮的点击事件
     * @param onLeftBtnListener   左边按钮的点击事件
     * @param canceledOnTouchOutside 点击外面是否能取消
     * @param cancelable 点击返回键是否能取消
     * @return
     */
    public static void createTwoBtnDialog(Context context, String message,
                                            String rightBtnName, String leftBtnName,
                                            final OnRightBtnListener onRightBtnListener,
                                            final OnLeftBtnListener onLeftBtnListener,
                                            boolean canceledOnTouchOutside,boolean cancelable){
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        dialog = builder.create();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_twobtn,null);
        TextView tv_content = view.findViewById(R.id.tv_content);
        Button bt_left = view.findViewById(R.id.bt_left);
        Button bt_right = view.findViewById(R.id.bt_right);
        tv_content.setText(message);
        bt_left.setText(leftBtnName);
        bt_right.setText(rightBtnName);
        final Dialog finalDialog = dialog;
        dialog.setCanceledOnTouchOutside(canceledOnTouchOutside);
        dialog.setCancelable(cancelable);
        bt_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onLeftBtnListener!=null){
                    onLeftBtnListener.setOnLeftListener(finalDialog);
                }
                finalDialog.dismiss();
            }
        });
        bt_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onRightBtnListener!=null){
                    onRightBtnListener.setOnRightListener(finalDialog);
                }
                finalDialog.dismiss();
            }
        });
        dialog.show();
        //setContentView一定要放在show后面才行显示出页面
        dialog.setContentView(view);
    }

    /**
     *
     * @param context  上下文
     * @param message 显示内容
     * @param btnName  按钮的文字
     * @param onLeftBtnListener   按钮的点击事件
     * @param canceledOnTouchOutside 点击外面是否能取消
     * @param cancelable 点击返回键是否能取消
     * @return
     */
    public static void createOneBtnDialog(Context context, String message,
                                           String btnName,
                                          final OnLeftBtnListener onLeftBtnListener,
                                          boolean canceledOnTouchOutside,boolean cancelable){
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        dialog = builder.create();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_onebtn,null);
        TextView tv_content = view.findViewById(R.id.tv_content);
        Button bt_left = view.findViewById(R.id.bt_left);
        tv_content.setText(message);
        bt_left.setText(btnName);
        final Dialog finalDialog = dialog;
        dialog.setCanceledOnTouchOutside(canceledOnTouchOutside);
        dialog.setCancelable(cancelable);
        bt_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onLeftBtnListener!=null){
                    onLeftBtnListener.setOnLeftListener(finalDialog);
                }
                finalDialog.dismiss();
            }
        });
        dialog.show();
        //setContentView一定要放在show后面才行显示出页面
        dialog.setContentView(view);
    }

    /**
     *
     * @param context 上下文，不能是ApplicationContext
     * @param message 显示文字
     */
    public static void createTipAllTextDialog(Context context,String message){
        final FullScreenDialog dialog = new FullScreenDialog(context);
        dialog.show();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_tip_all_text,null);
        TextView tv_tip = view.findViewById(R.id.tv_tip);
        tv_tip.setText(message);
        dialog.setContentView(view);
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        },1000);

    }

    /**
     *
     * @param context 上下文
     * @param message 显示文字
     * @param imageResId 图片资源文件
     */
    public static void createTipImageAndTextDialog(final Context context, String message, int imageResId){
        final FullScreenDialog dialog = new FullScreenDialog(context);
        dialog.show();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_tip_image_text,null);
        TextView tv_tip = view.findViewById(R.id.tv_tip);
        ImageView iv_tip = view.findViewById(R.id.iv_tip);
        tv_tip.setText(message);
        iv_tip.setImageResource(imageResId);
        dialog.setContentView(view);
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(dialog.isShowing()&&!((Activity)context).isFinishing()){
                    dialog.dismiss();
                }
            }
        },1000);
    }

    public static void createTipAllLoadDialog(Context context){
        if(fullScreenDialog!=null&&fullScreenDialog.isShowing()){
            fullScreenDialog.dismiss();
            fullScreenDialog=null;
        }
        fullScreenDialog = new FullScreenDialog(context);
        fullScreenDialog.show();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_tip_lode_layout,null);
        TextView tv_tip = view.findViewById(R.id.tv_tip);
        ImageView iv_tip = view.findViewById(R.id.iv_tip);
        tv_tip.setText(R.string.loading_text);
        fullScreenDialog.setContentView(view);
    }

    public static void moven(){
        if(fullScreenDialog!=null){
            if(fullScreenDialog.isShowing()){
                fullScreenDialog.dismiss();
                fullScreenDialog=null;
            }
        }
    }

    public interface OnRightBtnListener {
        void setOnRightListener(Dialog dialog);
    }
    public interface OnLeftBtnListener {
        void setOnLeftListener(Dialog dialog);
    }

}
