package com.xht.kuaiyouyi.utils

import android.app.Activity
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog

/**
 * 统一的qmui对话框
 */
object TipDialog {

    private var tipDialog: QMUITipDialog? = null

    val mHandler by lazy {
        Handler(Looper.getMainLooper())
    }

    fun remove() {
        if (tipDialog != null) {
            if (tipDialog?.isShowing!!) {
                tipDialog?.dismiss()
                tipDialog = null
            }
        }
    }

    /**
     * 只显示消息
     */
    fun showTipMsg(activity: Activity?, msg: String?,cancelable:Boolean=false) {

        if (activity==null){
            return
        }
        remove()
        if (TextUtils.isEmpty(msg)) {
            return
        }

        tipDialog = QMUITipDialog.Builder(activity)
                .setIconType(QMUITipDialog.Builder.ICON_TYPE_NOTHING)
                .setTipWord(ZFChange.setText(msg))
                .create()
        tipDialog?.show()
        removeDelayed(1500)
    }

    /**
     * 显示成功消息
     */
    fun showTipSuc(activity: Activity?,msg: String?,cancelable:Boolean=false) {
        if (activity==null){
            return
        }
        remove()
        if (TextUtils.isEmpty(msg)) {
            tipDialog = QMUITipDialog.Builder(activity)
                    .setIconType(QMUITipDialog.Builder.ICON_TYPE_SUCCESS)
                    .create()
        } else {
            tipDialog = QMUITipDialog.Builder(activity)
                    .setIconType(QMUITipDialog.Builder.ICON_TYPE_SUCCESS)
                    .setTipWord(ZFChange.setText(msg))
                    .create()
        }
        tipDialog?.show()

        removeDelayed(2000)
    }

    /**
     * 显示失败消息
     */
    fun showTipFail(activity: Activity?, msg: String?,cancelable:Boolean=false) {

        if (activity==null){
            return
        }
        remove()

        if (TextUtils.isEmpty(msg)) {
            tipDialog = QMUITipDialog.Builder(activity)
                    .setIconType(QMUITipDialog.Builder.ICON_TYPE_FAIL)
                    .create()
        } else {
            tipDialog = QMUITipDialog.Builder(activity)
                    .setIconType(QMUITipDialog.Builder.ICON_TYPE_FAIL)
                    .setTipWord(ZFChange.setText(msg))
                    .create()
        }

        tipDialog?.show()
        removeDelayed(2500)

    }

    /**
     * 显示loading消息
     */
    fun showTipLoad(activity: Activity?,msg: String?) {

        if (activity==null){
            return
        }
        remove()
        if (TextUtils.isEmpty(msg)) {
            tipDialog = QMUITipDialog.Builder(activity)
                    .setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING)
                    .create()
        } else {
            tipDialog = QMUITipDialog.Builder(activity)
                    .setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING)
                    .setTipWord(ZFChange.setText(msg))
                    .create()
        }

        tipDialog?.show()
    }

    private fun removeDelayed(time: Long) {
        mHandler.postDelayed({
            remove()
        }, time)
    }
}

