package com.xht.kuaiyouyi.utils

/**
 * Created by shuaihuimin on 2018/6/15.
 */

import android.text.TextUtils
import android.util.Log
import com.xht.kuaiyouyi.ui.mine.setting.CurrencyActivity
import com.xht.kuaiyouyi.ui.mine.setting.LanguageActivity

/**
 * Created by pjh on 18/3/8.
 * 登录管理
 */
class Login {

    private constructor()

    object Inner {
        val ins: Login = Login()
    }

    companion object {
        fun getInstance(): Login {
            return Inner.ins
        }
    }

    //    {"username":"test123456","key":"f36626b0f95b557471fa989d0674c009","userid":"31"}


    var username: String by Preference("login_username", "")
    var token: String by Preference("login_token", "")
    var token_id: String by Preference("login_token_id", "")
    var memberAvatar: String by Preference("login_memberAvatar", "")
    var uid: String by Preference("uid","")
    var phone: String by Preference("phone","")
    var unread_msg_num: String by Preference("unread_msg_num","0")//消息中心的4个tab的未读数量之和
    var is_set_alias: String by Preference("is_set_alias","0")//极光推送是否设置过别名
    var main_num: String by Preference("main_num","0")//待付款和待我审批数量


    //语言设置
    var languageCode: String by Preference("language_code",LanguageActivity.TRADITIONAL_CHINESE);
    //币别设置
    var currencyCode: String by Preference("currency_code",CurrencyActivity.HKD);

    //是否需要显示引导页
    var isShowGuide: String by Preference("is_show_guide","");

    //选中的企业
    var enterprise_company_id: String by Preference("enterprise_company_id", "")

    var huawei_push_token: String by Preference("huawei_push_token", "")

    var is_show_pull_up_tip: String by Preference("is_show_pull_up_tip", "1")



    fun isLogin(): Boolean {
        Log.i("info","----------token_id"+token_id);
        if (TextUtils.isEmpty(token)) {
            return false
        } else if (TextUtils.isEmpty(token_id)) {
            return false
        } else {
            return true
        }

    }

}