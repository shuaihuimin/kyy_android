package com.xht.kuaiyouyi.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.LocaleList;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.xht.kuaiyouyi.BuildConfig;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.cart.entity.CurrencytypeBean;
import com.xht.kuaiyouyi.ui.enterprise.bean.MainNumBean;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.message.bean.UnreadMessageBean;
import com.xht.kuaiyouyi.ui.mine.setting.CurrencyActivity;
import com.xht.kuaiyouyi.ui.mine.setting.LanguageActivity;
import com.xht.kuaiyouyi.widget.RoundBackgroundColorSpan;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.RoundingMode;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

public class Utils {
    private boolean isOpen=false;

    /**
     * 获取时间戳的前10位
     *
     * @return
     */
    public static String getSignTime() {
        String sign_time = System.currentTimeMillis() / 1000 + "";
        return sign_time;
    }

    /**
     * @param secondStr 秒
     * @return 字符串，类似2018-10-08
     */
    @SuppressLint("SimpleDateFormat")
    public static String getDisplayData(String secondStr) {
        if (TextUtils.isEmpty(secondStr)) {
            return "";
        }
        long millisecond = Long.valueOf(secondStr) * 1000;
        Date date = new Date(millisecond);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    /**
     * @param secondStr 秒
     * @return 返回时间到分钟数 2018-08-05 23:43
     */
    @SuppressLint("SimpleDateFormat")
    public static String getDisplayDataAndTime(String secondStr) {
        if (TextUtils.isEmpty(secondStr)) {
            return "";
        }
        long millisecond = Long.valueOf(secondStr) * 1000;
        Date date = new Date(millisecond);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return format.format(date);
    }

    /**
     * @param secondStr 秒
     * @return 返回时间到分钟数 2018-08-05 23:43
     */
    @SuppressLint("SimpleDateFormat")
    public static String getDisplayTime(String secondStr) {
        if (TextUtils.isEmpty(secondStr)) {
            return "";
        }
        long millisecond = Long.valueOf(secondStr) * 1000;
        Date date = new Date(millisecond);
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        return format.format(date);
    }

    public static String getIPAddress(Context context) {
        NetworkInfo info = ((ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            if (info.getType() == ConnectivityManager.TYPE_MOBILE) {//当前使用2G/3G/4G网络
                try {
                    for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                        NetworkInterface intf = en.nextElement();
                        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                            InetAddress inetAddress = enumIpAddr.nextElement();
                            if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                                return inetAddress.getHostAddress();
                            }
                        }
                    }
                } catch (SocketException e) {
                    e.printStackTrace();
                }

            } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {//当前使用无线网络
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                return intIP2StringIP(wifiInfo.getIpAddress());
            }
        }
        return null;
    }

    /**
     * 将得到的int类型的IP转换为String类型
     *
     * @param ip
     * @return
     */
    public static String intIP2StringIP(int ip) {
        return (ip & 0xFF) + "." +
                ((ip >> 8) & 0xFF) + "." +
                ((ip >> 16) & 0xFF) + "." +
                (ip >> 24 & 0xFF);
    }

    public static void log(String tag, String content) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, content);
        }
    }

    public static int getIntRandom(int max) {
        return (int) (Math.random() * max);
    }

    public static String getDisplayPhone(String phone) {
        return phone.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
    }

    /**
     * android 8.0以上的content需要是acitivity
     * @param activity
     */
    public static void setLanguage(Activity activity) {
        String langCode = Login.Companion.getInstance().getLanguageCode();
        Configuration config = activity.getResources().getConfiguration();     // 获得设置对象
        DisplayMetrics dm = activity.getResources().getDisplayMetrics();
        Locale locale;
        switch (langCode) {
            case LanguageActivity.SIMPLE_CHINESE:
                locale = Locale.SIMPLIFIED_CHINESE;
                break;
            case LanguageActivity.TRADITIONAL_CHINESE:
                locale = Locale.TAIWAN;
                break;
            default:
                locale = Locale.TAIWAN;
                break;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocales(new LocaleList(locale));
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(locale);
        }else {
            config.locale = locale;
        }
        activity.getResources().updateConfiguration(config, dm);
    }


    /**
     * @param d double字符
     * @return
     */
    public static String getDisplayMoney(double d) {
        DecimalFormat df1 = new DecimalFormat("#,###0.00");
        DecimalFormat df2 = new DecimalFormat("#,###.00");
        if (d > 1) {
            return df2.format(d);
        } else {
            return df1.format(d);
        }
    }

//    public static int dp2px(Context context,float dpValue){
//        float scale=context.getResources().getDisplayMetrics().density;
//        return (int)(dpValue*scale+0.5f);
//    }

    /**
     * px转换成dp
     */
    public static int px2dp(Context context,float pxValue){
        float scale=context.getResources().getDisplayMetrics().density;
        return (int)(pxValue/scale+0.5f);
    }


    /**
     * @Title: fmtMicrometer
     * @Description: 格式化数字为千分位
     * @param text
     * @return    设定文件
     * @return String    返回类型
     */
    public static String fmtMicrometer(String text,boolean halfUp) {
        DecimalFormat df = null;
        if (text.indexOf(".") > 0) {

            if (text.length() - text.indexOf(".") - 1 == 0) {
                df = new DecimalFormat("###,##0.");
            } else if (text.length() - text.indexOf(".") - 1 == 1) {
                df = new DecimalFormat("###,##0.0");
            } else {

                String intNumber = text.substring(text.indexOf(".")+1,text.length());
                if (intNumber.equals("00")){
                    df = new DecimalFormat("###,##0.00");
//                    df = new DecimalFormat("###,##0");
                }else{
                    df = new DecimalFormat();
                    df.setMaximumFractionDigits(2);
                    df.setGroupingSize(3);
                    df.setRoundingMode(halfUp ? RoundingMode.HALF_UP:RoundingMode.FLOOR);
                }
//                MyLog.e("infoxiaoshu-->","---"+intNumber);


            }
        } else {
            df = new DecimalFormat("###,##0");
        }
        double number = 0.0;
        try {
            number = Double.parseDouble(text);
        } catch (Exception e) {
            number = 0.0;
        }
        return df.format(number);
    }


    /**
     * 获取未读消息数
     */
    public static void getUnreadMsg(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_UNREAD_MSG())
                .withPOST(new NetCallBack<UnreadMessageBean>() {

                    @NotNull
                    @Override
                    public Class<UnreadMessageBean> getRealType() {
                        return UnreadMessageBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {

                    }

                    @Override
                    public void onSuccess(@NonNull UnreadMessageBean unreadMessageBean) {
                        if(unreadMessageBean.getMessage()!=null){
                            int num = unreadMessageBean.getMessage().getNewapporove()+
                                    unreadMessageBean.getMessage().getNewchat()+
                                    unreadMessageBean.getMessage().getNewcompany()+
                                    unreadMessageBean.getMessage().getNewsystem();
                            Login.Companion.getInstance().setUnread_msg_num(
                                    num+""
                            );
                            EventBus.getDefault().post(new MessageEvent(MessageEvent.UNREAD_MSG_NUM));
                        }

                    }
                }, false);
    }

    /**
     * 获取待付款和待我审批数量
     */
    public static void getMainNum(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_GET_MAIN_NUM())
                .withPOST(new NetCallBack<MainNumBean>() {

                    @NotNull
                    @Override
                    public Class<MainNumBean> getRealType() {
                        return MainNumBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {

                    }

                    @Override
                    public void onSuccess(@NonNull MainNumBean mainNumBean) {
                        Login.Companion.getInstance().setMain_num(mainNumBean.getMessage_count()+"");
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_MAIN_NUM));

                    }
                }, false);
    }

    /**
     * 获取进程的名字
     * @param context
     * @param pid
     * @return
     */
    public static String getProcessName(Context context, int pid) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps != null && !runningApps.isEmpty()) {
            for (ActivityManager.RunningAppProcessInfo procInfo : runningApps) {
                if (procInfo.pid == pid) {
                    return procInfo.processName;
                }
            }
        }
        return null;
    }

    /**
     * 删除文件或者文件夹
     * @param dir
     * @return
     */
    public static boolean deleteDir(File dir) {
        if(dir == null){
            return false;
        }
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    /**
     * 获取对应货币的显示值
     * @param currencytype
     * @param totalPrice  人民币
     */
    public static String getCurrencyDisplayMoney(CurrencytypeBean currencytype, double totalPrice) {
        double rate=0;
        String symbol = "";
        switch (Login.Companion.getInstance().getCurrencyCode()){
            case CurrencyActivity.RMB:
                return "";
            case CurrencyActivity.MOP:
                rate = currencytype.getMOP().getRate();
                symbol = currencytype.getMOP().getSymbol();
                break;
            case CurrencyActivity.HKD:
                rate = currencytype.getHKD().getRate();
                symbol = currencytype.getHKD().getSymbol();
                break;
            case CurrencyActivity.USA:
                rate = currencytype.getUSD().getRate();
                symbol = currencytype.getUSD().getSymbol();
                break;
        }

        String result = "(~" + symbol + Utils.getDisplayMoney(rate*totalPrice) +")";
        return result;
    }


    /**
     * 掉此方法输入所要转换的时间输入例如（"2014年06月14日"）返回时间戳
     *
     * @param time
     * @return
     */
    public static String data(String time) {
        SimpleDateFormat sdr = new SimpleDateFormat("yyyy年MM月dd日",
                Locale.CHINA);
        Date date;
        String times = null;
        try {
            date = sdr.parse(time);
            long l = date.getTime();
            String stf = String.valueOf(l);
            times = stf.substring(0, 10);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return times;
    }

    /**
     * 调用此方法输入所要转换的时间戳输入例如（1402733340）输出（"2014-06-14  16:09:00"）
     *
     * @param time
     * @return
     */
    public static String timedate(String time) {
        SimpleDateFormat sdr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        @SuppressWarnings("unused")
        long lcc = Long.valueOf(time);
        int i = Integer.parseInt(time);
        String times = sdr.format(new Date(i * 1000L));
        return times;

    }

    /**
     * 适配设置魅族手机的状态栏字体颜色是黑色（深色）
     * @param window
     * @param dark 深色（黑色）
     * @return true代表设置成功
     */
    public static boolean FlymeSetStatusBarLightMode(Window window, boolean dark) {
        boolean result = false;
        if (window != null) {
            try {
                WindowManager.LayoutParams lp = window.getAttributes();
                Field darkFlag = WindowManager.LayoutParams.class
                        .getDeclaredField("MEIZU_FLAG_DARK_STATUS_BAR_ICON");
                Field meizuFlags = WindowManager.LayoutParams.class
                        .getDeclaredField("meizuFlags");
                darkFlag.setAccessible(true);
                meizuFlags.setAccessible(true);
                int bit = darkFlag.getInt(null);
                int value = meizuFlags.getInt(lp);
                if (dark) {
                    value |= bit;
                } else {
                    value &= ~bit;
                }
                meizuFlags.setInt(lp, value);
                window.setAttributes(lp);
                result = true;
            } catch (Exception e) {

            }
        }
        return result;
    }

    /*
     *设置屏幕半透明
     */
    public static void SetWindBg(Activity activity){
        Window mWindow = activity.getWindow();
        WindowManager.LayoutParams params = mWindow.getAttributes();
        params.alpha = 0.5f;
        mWindow.setAttributes(params);
    }

    /**
     * convert dp to its equivalent px
     *
     * 将dp转换为与之相等的px
     */
    public static int dp2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    //获取屏幕宽
    public static int getWindowWidth(Activity activity){
        WindowManager manager = activity.getWindowManager();
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        int width = outMetrics.widthPixels;
        return width;
    }
    //获取屏幕的高
    public static int getWindoHeight(Activity activity){
//        DisplayMetrics dm = activity.getResources().getDisplayMetrics();
//        int heigth = dm.heightPixels;
//        return (int) (heigth);
        WindowManager manager = activity.getWindowManager();
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        int height= outMetrics.heightPixels;
        return height;
    }

    /**
     * 判断是否是emui系统，emui系统是华为的定制系统
     * @return
     */
    public static boolean isHUAWEI(){
        int emuiApiLevel = 0;
        try {
            Class cls = Class.forName("android.os.SystemProperties");
            Method method = cls.getDeclaredMethod("get", new Class[]{String.class});
            emuiApiLevel = Integer.parseInt((String) method.invoke(cls, new Object[]{"ro.build.hw_emui_api_level"}));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return emuiApiLevel > 0;
//        return Build.MANUFACTURER.equalsIgnoreCase("huawei");
    }

    //复制内容到剪切板
    public static void getCopy(Context context,String content){
        //获取剪贴板管理器：
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        // 创建普通字符型ClipData
        ClipData mClipData = ClipData.newPlainText("Label",content);
        // 将ClipData内容放到系统剪贴板里。
        cm.setPrimaryClip(mClipData);
    }


    /**
     *
     * @param imgPath 图片的路径
     * @return 图片的宽和高
     */
    public static int[] getImageWidthAndHeight(String imgPath){
        if(!new File(imgPath).exists()){
            return new int[]{0,0};
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imgPath,options);
        return new int[]{options.outWidth,options.outHeight};
    }

    //拷贝文件
    public static boolean CopyFile(File in, File out) throws Exception {
        try {
            FileInputStream fis = new FileInputStream(in);
            FileOutputStream fos = new FileOutputStream(out);
            byte[] buf = new byte[1024];
            int i = 0;
            while ((i = fis.read(buf)) != -1) {
                fos.write(buf, 0, i);
            }
            fis.close();
            fos.close();
            return true;
        } catch (IOException ie) {
            ie.printStackTrace();
            return false;
        }
    }
    //拷贝文件
    public static boolean CopyFile(String infile, String outfile) throws Exception {
        try {
            File in = new File(infile);
            File out = new File(outfile);
            return CopyFile(in, out);
        } catch (IOException ie) {
            ie.printStackTrace();
            return false;
        }

    }

    /**
     * 判断文件是否存在
     * @param path
     * @return
     */
    public static boolean isFileExist(String path){
        if(!TextUtils.isEmpty(path)&&new File(path).exists()){
            return true;
        }
        return false;
    }


    /**
     * 标题加二手标签
     */
    public static SpannableString secondLabel(String title){

        SpannableString spannableString=new SpannableString("二手"+title);
        spannableString.setSpan(new RoundBackgroundColorSpan(Color.parseColor("#43c783"),Color.parseColor("#FFFFFF")), 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    /**
     * 关闭软键盘
     */
    public static void hintKeyBoard(Activity activity) {
        //拿到InputMethodManager
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        //如果window上view获取焦点 && view不为空
        if (imm.isActive() && activity.getCurrentFocus() != null) {
            //拿到view的token 不为空
            if (activity.getCurrentFocus().getWindowToken() != null) {
                //表示软键盘窗口总是隐藏，除非开始时以SHOW_FORCED显示。
                imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }
    /**
     * 获取文件的类型
     * @param fileName
     * @return 文件类型
     */
    public static String getFileType(String fileName){
        if(TextUtils.isEmpty(fileName) || !fileName.contains(".")){
            return null;
        }
        return fileName.substring(fileName.lastIndexOf(".")+1,fileName.length());
    }

    /**
     *
     * @param
     * @return 返回显示的长度
     */
    public static String getDisplayLength(long size){
        long rest = 0;
        if(size < 1024){
            return String.valueOf(size) + "B";
        }else{
            size /= 1024;
        }

        if(size < 1024){
            return String.valueOf(size) + "KB";
        }else{
            rest = size % 1024;
            size /= 1024;
        }

        if(size < 1024){
            size = size * 100;
            return String.valueOf((size / 100)) + "." + String.valueOf((rest * 100 / 1024 % 100)) + "MB";
        }else{
            size = size * 100 / 1024;
            return String.valueOf((size / 100)) + "." + String.valueOf((size % 100)) + "GB";
        }
    }

}
