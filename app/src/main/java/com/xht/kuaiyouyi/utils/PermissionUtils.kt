//package com.xht.kuaiyouyi.utils
//
//import android.Manifest
//import android.content.Intent
//import android.util.Log
//import com.xht.kuaiyouyi.R
//import me.weyye.hipermission.HiPermission
//import me.weyye.hipermission.PermissionCallback
//import me.weyye.hipermission.PermissionItem
//import java.util.*
//
///**
// * Created by shuaihuimin on 2018/6/19.
// * 权限管理类
// */
//class PermissionUtils {
//
//    /**
//     * 6.0以下版本(系统自动申请) 不会弹框
//     * 有些厂商修改了6.0系统申请机制，他们修改成系统自动申请权限了
//     */
//    private fun Any.checkPermission(Class: Any) {
//        val permissionItems = ArrayList<PermissionItem>()
//        permissionItems.add(PermissionItem(Manifest.permission.READ_PHONE_STATE, "手机状态", R.drawable.permission_ic_phone))
//        permissionItems.add(PermissionItem(Manifest.permission.WRITE_EXTERNAL_STORAGE, "存储空间", R.drawable.permission_ic_storage))
//
//        HiPermission.create(Any)
//                .title("亲爱的上帝")
//                .msg("为了能够正常使用，请开启这些权限吧！")
//                .permissions(permissionItems)
//                .style(R.style.PermissionDefaultBlueStyle)
//                .animStyle(R.style.PermissionAnimScale)
//                .checkMutiPermission(object : PermissionCallback {
//                    override fun onClose() {
//
//                        ToastU.showToast("用户关闭了权限")
//                    }
//
//                    override fun onFinish() {
////                        ToastU.showToast("初始化完毕！")
////                        layout_splash.startAnimation(alphaAnimation)
//
//
//                    }
//
//                    override fun onDeny(permission: String, position: Int) {
//                        Log.e("info", "permission_onDeny" + permission)
//                    }
//
//                    override fun onGuarantee(permission: String, position: Int) {
//                        Log.e("info", "permission_onGuarantee" + permission)
//                    }
//                })
//    }
//
//    /**
//     * 打开相机前要检查权限
//     */
//    private fun checkCameraPermission() {
//        val permissionItems = ArrayList<PermissionItem>()
//        permissionItems.add(PermissionItem(Manifest.permission.CAMERA, "摄像头", R.drawable.permission_ic_phone))
//
//        HiPermission.create()
//                .title("亲爱的上帝")
//                .msg("为了能够正常使用，请开启摄像头权限吧！")
//                .permissions(permissionItems)
//                .style(R.style.PermissionDefaultBlueStyle)
//                .animStyle(R.style.PermissionAnimScale)
//                .checkMutiPermission(object : PermissionCallback {
//                    override fun onClose() {
//
//                        ToastU.showToast("用户关闭了权限")
//                    }
//
//                    override fun onFinish() {
////                        ToastU.showToast("初始化完毕！")
////                        layout_splash.startAnimation(alphaAnimation)
//                        val intent = Intent(, CaptureActivity::class.java)
//                        startActivityForResult(intent, 0x123)
//
//                    }
//
//                    override fun onDeny(permission: String, position: Int) {
//                        Log.e(TAG, "permission_onDeny" + permission)
//                    }
//
//                    override fun onGuarantee(permission: String, position: Int) {
//                        Log.e(TAG, "permission_onGuarantee" + permission)
//                    }
//                })
//    }
//
//}