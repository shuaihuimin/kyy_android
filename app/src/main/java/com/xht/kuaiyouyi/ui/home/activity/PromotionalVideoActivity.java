package com.xht.kuaiyouyi.ui.home.activity;

import android.content.Intent;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;

import butterknife.BindView;

/**
 * 宣传视频页面
 */
public class PromotionalVideoActivity extends BaseActivity {

    @BindView(R.id.tv_pu_up_to_homepage)
    TextView tv_pu_up_to_homepage;
    @BindView(R.id.rl_second_floor)
    RelativeLayout rl_second_floor;
    @BindView(R.id.iv_play_video)
    ImageView iv_play_video;

    private float mPosX;
    private float mPosY;
    private float mCurPosX;
    private float mCurPosY;

    @Override
    protected int getLayout() {
        return R.layout.activity_promotionalvideo;
    }

    @Override
    protected void initView() {
        tv_pu_up_to_homepage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rl_second_floor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mPosX = event.getX();
                        mPosY = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                    case MotionEvent.ACTION_UP:
                        mCurPosX = event.getX();
                        mCurPosY = event.getY();
                        if (mCurPosY - mPosY < 0
                                && (Math.abs(mCurPosY - mPosY) > 25)) {
                            //向上滑动
                            onBackPressed();
                        }
                        break;
                }
                return true;
            }
        });
        iv_play_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri uri = Uri.parse("https://kuaiyouyi.oss-cn-shenzhen.aliyuncs.com/video/promotion_video/%E5%BF%AB%E4%BC%98%E6%98%93%E5%AE%A3%E4%BC%A0%E7%89%87%E6%88%90%E7%89%87.mp4");
                intent.setDataAndType(uri, "video/*");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
    }
}
