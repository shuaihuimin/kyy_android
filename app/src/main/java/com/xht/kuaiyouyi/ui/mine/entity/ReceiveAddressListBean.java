package com.xht.kuaiyouyi.ui.mine.entity;

import java.io.Serializable;
import java.util.List;

public class ReceiveAddressListBean implements Serializable{


    private List<AddressBean> address;

    public List<AddressBean> getAddress() {
        return address;
    }

    public void setAddress(List<AddressBean> address) {
        this.address = address;
    }

    public static class AddressBean implements Serializable{
        /**
         * address_id : 194
         * member_id : 1
         * true_name : test
         * area_id : 55
         * city_id : 40
         * city_id_2 : 0
         * area_info : 天津 天津市 和平區
         * address : 1231323133
         * tel_phone : 13631224533
         * mob_phone : 18815512220
         * is_default : 0
         * dlyp_id : 0
         * mobile_zone1 : 86
         * mobile_zone2 : 852
         * tel_zone :
         * mobile_zone1_spare : 86
         * mob_phone_spare : 13631224521
         */

        private String address_id;
        private String member_id;
        private String true_name;
        private String area_id;
        private String city_id;
        private String city_id_2;
        private String area_info;
        private String address;
        private String tel_phone;
        private String mob_phone;
        private String is_default;
        private String dlyp_id;
        private String mobile_zone1;
        private String mobile_zone2;
        private String tel_zone="";
        private String mobile_zone1_spare;
        private String mob_phone_spare;

        public String getAddress_id() {
            return address_id;
        }

        public void setAddress_id(String address_id) {
            this.address_id = address_id;
        }

        public String getMember_id() {
            return member_id;
        }

        public void setMember_id(String member_id) {
            this.member_id = member_id;
        }

        public String getTrue_name() {
            return true_name;
        }

        public void setTrue_name(String true_name) {
            this.true_name = true_name;
        }

        public String getArea_id() {
            return area_id;
        }

        public void setArea_id(String area_id) {
            this.area_id = area_id;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getCity_id_2() {
            return city_id_2;
        }

        public void setCity_id_2(String city_id_2) {
            this.city_id_2 = city_id_2;
        }

        public String getArea_info() {
            return area_info;
        }

        public void setArea_info(String area_info) {
            this.area_info = area_info;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getTel_phone() {
            return tel_phone;
        }

        public void setTel_phone(String tel_phone) {
            this.tel_phone = tel_phone;
        }

        public String getMob_phone() {
            return mob_phone;
        }

        public void setMob_phone(String mob_phone) {
            this.mob_phone = mob_phone;
        }

        public String getIs_default() {
            return is_default;
        }

        public void setIs_default(String is_default) {
            this.is_default = is_default;
        }

        public String getDlyp_id() {
            return dlyp_id;
        }

        public void setDlyp_id(String dlyp_id) {
            this.dlyp_id = dlyp_id;
        }

        public String getMobile_zone1() {
            return mobile_zone1;
        }

        public void setMobile_zone1(String mobile_zone1) {
            this.mobile_zone1 = mobile_zone1;
        }

        public String getMobile_zone2() {
            return mobile_zone2;
        }

        public void setMobile_zone2(String mobile_zone2) {
            this.mobile_zone2 = mobile_zone2;
        }

        public String getTel_zone() {
            return tel_zone;
        }

        public void setTel_zone(String tel_zone) {
            this.tel_zone = tel_zone;
        }

        public String getMobile_zone1_spare() {
            return mobile_zone1_spare;
        }

        public void setMobile_zone1_spare(String mobile_zone1_spare) {
            this.mobile_zone1_spare = mobile_zone1_spare;
        }

        public String getMob_phone_spare() {
            return mob_phone_spare;
        }

        public void setMob_phone_spare(String mob_phone_spare) {
            this.mob_phone_spare = mob_phone_spare;
        }
    }
}
