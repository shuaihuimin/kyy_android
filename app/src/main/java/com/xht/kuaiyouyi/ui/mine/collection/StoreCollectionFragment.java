package com.xht.kuaiyouyi.ui.mine.collection;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.classic.common.MultipleStatusView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.goods.StoreActivity;
import com.xht.kuaiyouyi.ui.mine.adapter.StoreCollectionAdapter;
import com.xht.kuaiyouyi.ui.mine.entity.StoreCollectionBean;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class StoreCollectionFragment extends BaseFragment {
    @BindView(R.id.mRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.listview_order)
    ListView listView;
    @BindView(R.id.multipleStatusView)
    MultipleStatusView multipleStatusView;
    @BindView(R.id.iv_to_top)
    ImageView iv_to_top;

    private StoreCollectionBean mStoreCollectionBean;
    private StoreCollectionAdapter mAdapter;
    private String mKyy ="";



    public static StoreCollectionFragment newInstance() {
        Bundle args = new Bundle();
        StoreCollectionFragment fragment = new StoreCollectionFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        requestStoreLsit();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle=new Bundle();
                bundle.putString("store_id",mStoreCollectionBean.getFavorites_list().get(position).getStore_id());
                goToActivity(StoreActivity.class,bundle);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                DialogUtils.createTwoBtnDialog(getActivity(), getResources().getString(R.string.cancel_collection_tip),
                        getResources().getString(R.string.dialog_confirm),
                        getResources().getString(R.string.dialog_cancel),
                        new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(Dialog dialog) {
                                requestCancelStoreCollection(mStoreCollectionBean.
                                        getFavorites_list().get(position).getStore_id(),position);
                            }
                        }, new DialogUtils.OnLeftBtnListener() {
                            @Override
                            public void setOnLeftListener(Dialog dialog) {

                            }
                        },true,true);

                return true;
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                requestStoreLsit();
            }
        });
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                mKyy = "";
                requestStoreLsit();
            }
        });
        iv_to_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.smoothScrollToPosition(0);
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    View firstVisibleItemView = listView.getChildAt(0);
                    if (firstVisibleItemView != null && firstVisibleItemView.getTop() == 0) {
                        //滚动到了顶部
                        iv_to_top.setVisibility(View.GONE);
                    }else {
                        //不在顶部
                        iv_to_top.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        multipleStatusView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestStoreLsit();
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_collection;
    }

    private void requestStoreLsit(){
        if(TextUtils.isEmpty(mKyy)){
            multipleStatusView.showLoading();
        }
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_STORE_COLLECTION_LIST())
                .withLoadPOST(new NetCallBack<StoreCollectionBean>() {

                    @NotNull
                    @Override
                    public Class<StoreCollectionBean> getRealType() {
                        return StoreCollectionBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity() == null){
                            return;
                        }
                        if (TextUtils.isEmpty(mKyy)) {
                            multipleStatusView.setClickable(true);
                            multipleStatusView.showError();
                        }else {
                            Toast.makeText(getContext(), getString(R.string.not_network), Toast.LENGTH_SHORT).show();
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull StoreCollectionBean storeCollectionBean) {
                        if(getActivity() == null){
                            return;
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                        if(TextUtils.isEmpty(mKyy)){
                            //刷新
                            mStoreCollectionBean = storeCollectionBean;
                            if(mStoreCollectionBean.getFavorites_list()!=null && mStoreCollectionBean.getFavorites_list().size()>0){
                                multipleStatusView.showContent();
                                mAdapter = new StoreCollectionAdapter(getContext(),storeCollectionBean.getFavorites_list());
                                listView.setAdapter(mAdapter);
                                mKyy = mStoreCollectionBean.getKyy();
                            }else {
                                multipleStatusView.showEmpty();
                                multipleStatusView.setClickable(false);
                                smartRefreshLayout.setEnableLoadMore(false);
                            }
                            if(smartRefreshLayout.getState() == RefreshState.Refreshing){
                                smartRefreshLayout.finishRefresh();
                            }
                            smartRefreshLayout.setNoMoreData(false);
                            iv_to_top.setVisibility(View.GONE);
                        }else {
                            //分页加载
                            if(storeCollectionBean.getFavorites_list()!=null && storeCollectionBean.getFavorites_list().size()>0){
                                mKyy = storeCollectionBean.getKyy();
                                mStoreCollectionBean.getFavorites_list().addAll(storeCollectionBean.getFavorites_list());
                                mAdapter.notifyDataSetChanged();
                            }else {
                                smartRefreshLayout.setNoMoreData(true);
                                smartRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
                            }
                        }
                    }
                },false, mKyy);

    }

    //取消收藏
    private void requestCancelStoreCollection(String store_id, final int position){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_FAVORITE())
                .addParam("fid", store_id)
                .addParam("type", "1")
                .addParam("fav_type", "2")
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity() == null){
                            return;
                        }
                        Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        if(getActivity() == null){
                            return;
                        }
                        mStoreCollectionBean.getFavorites_list().remove(position);
                        mAdapter.notifyDataSetChanged();
                        if(mStoreCollectionBean.getFavorites_list().size()==0){
                            multipleStatusView.showEmpty();
                        }
                    }
                },false);

    }
}
