package com.xht.kuaiyouyi.ui.home.entity;

import java.util.List;

/**
 * 首页 数据集合
 */
public class HomeData {


    /**
     * banner : [{"adv_type":"2","image":"http://192.168.0.2/data/upload/shop/adv/05769287024930069.jpg","url_type":"","url_value":""},{"adv_type":"1","image":"http://192.168.0.2/data/upload/shop/adv/05769294798586357.jpg","url_type":"","url_value":""},{"adv_type":"1","image":"http://192.168.0.2/data/upload/shop/adv/05769294669040049.jpg","url_type":"","url_value":""},{"adv_type":"1","image":"http://192.168.0.2/data/upload/shop/adv/05769287265935928.jpg","url_type":"","url_value":""},{"adv_type":"2","image":"http://192.168.0.2/data/upload/shop/adv/","url_type":"","url_value":""}]
     * web_html : [{"style_name":"1","web_name":"精选品牌","more_url":"","title_color":"#654","bg_color":"#555","brand_list":[{"brand_id":"353","brand_name":"古今","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04431807497959652_sm.jpg"},{"brand_id":"351","brand_name":"曼妮芬（ManniForm）","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04431810033957836_sm.jpg"},{"brand_id":"348","brand_name":"奥唯嘉（Ovega）","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04431812331259168_sm.jpg"},{"brand_id":"345","brand_name":"乐扣乐扣","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399904614221217_sm.jpg"},{"brand_id":"344","brand_name":"乔曼帝","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399904423386126_sm.jpg"},{"brand_id":"342","brand_name":"富安娜","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399904168163421_sm.jpg"},{"brand_id":"329","brand_name":"欧司朗","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399902537418591_sm.jpg"},{"brand_id":"313","brand_name":"皮克朋","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399891285897466_sm.jpg"},{"brand_id":"225","brand_name":"伊莱克斯","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399834676870929_sm.gif"},{"brand_id":"226","brand_name":"艾力斯特","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399835435836906_sm.jpg"}]},{"style_name":"2","web_name":"工程整机","more_url":"http://www.baidu.com","title_color":"#222222","bg_color":"#f6f6f6","recommend_list":[{"pic_name":"电动电钻,商品精选热销","url_type":"2","url_value":"1063","pic_id":"50","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-50.jpg?475"},{"pic_name":"机械专造,商品精选热销","url_type":"2","url_value":"1063","pic_id":"60","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-60.jpg?240"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"11","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-11.jpg?794"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"31","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-31.jpg?511"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"21","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-21.jpg?646"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"12","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-12.jpg?363"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"32","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-32.jpg?580"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"24","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-24.jpg?658"}],"category_list":null,"brand_list":[]},{"style_name":"3","web_name":"家装建材","more_url":"","title_color":"#222222","bg_color":"#f6f6f6","adv":{"url_type":"2","url_value":"1063","pic_id":"2","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-633-712-2.jpg?501"},"goods_recommend_list":[{"goods_id":"100497","goods_name":"自行走曲臂式 FDF D RR","goods_price":"1.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796245285261627_240.png"},{"goods_id":"100362","goods_name":"自行走曲臂式GTBZ-AE","goods_price":"100.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796293756348179_240.png"},{"goods_id":"100344","goods_name":"自行走曲臂式","goods_price":"10.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796245285261627_240.png"},{"goods_id":"100473","goods_name":"222","goods_price":"100.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803134834338328_240.jpg"},{"goods_id":"100472","goods_name":"螺丝刀","goods_price":"10.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803176481834773_240.jpg"},{"goods_id":"100470","goods_name":"螺丝刀 发发发分付付付付付付付付付付付付付付 3是的 分 1 1 1","goods_price":"333.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803145000439866_240.jpg"},{"goods_id":"100469","goods_name":"222","goods_price":"2222.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803134834338328_240.jpg"}]},{"style_name":"3","web_name":"五金工具","more_url":"","title_color":"#222222","bg_color":"#f6f6f6","adv":{"url_type":"2","url_value":"1","pic_id":"2","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-634-714-2.jpg?977"},"goods_recommend_list":[{"goods_id":"100385","goods_name":"劳力士ROLEX-潜航者型 116610-LV-97200自动机械钢带男表联保正品 蓝色 1.5寸","goods_price":"1.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627678636481_240.jpg"},{"goods_id":"100384","goods_name":"劳力士ROLEX-潜航者型 116610-LV-97200自动机械钢带男表联保正品 绿色 1.5寸","goods_price":"1.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627678636481_240.jpg"},{"goods_id":"100383","goods_name":"劳力士ROLEX-潜航者型 116610-LV-97200自动机械钢带男表联保正品 红色 1.5寸","goods_price":"1.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627678636481_240.jpg"},{"goods_id":"100377","goods_name":"劳力士Rolex 蚝式恒动 115234-CA-72190自动机械钢带男表联保正品 555 22","goods_price":"100.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627843241680_240.jpg"},{"goods_id":"100376","goods_name":"劳力士Rolex 蚝式恒动 115234-CA-72190自动机械钢带男表联保正品 yellow 2","goods_price":"100.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627843241680_240.jpg"},{"goods_id":"100368","goods_name":"劳力士Rolex 蚝式恒动系列 自动机械钢带男表 正品116231-G-63201","goods_price":"100500.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627871532105_240.png"},{"goods_id":"100365","goods_name":"劳力士Rolex 宇宙计型迪通拿 自动机械皮带男表 正品116519 CR.TB 藍色 1.5","goods_price":"1.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627931531971_240.jpg"},{"goods_id":"100364","goods_name":"劳力士Rolex 宇宙计型迪通拿 自动机械皮带男表 正品116519 CR.TB 紅色 1.5","goods_price":"1.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627931531971_240.jpg"}],"brand_list":[{"brand_id":"340","brand_name":"博洋家纺","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399903956723469_sm.jpg"},{"brand_id":"339","brand_name":"安睡宝","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399903832203331_sm.jpg"}]}]
     * goods_list : [{"goods_id":"100001","goods_name":"劳力士Rolex 深海系列 自动机械钢带男士表 联保正品116660 98210","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627707766698_360.png","goods_price":"87500.00","goods_salenum":"8","is_enquiry":"0","goods_state":"1"},{"goods_id":"100002","goods_name":"劳力士Rolex MILGAUSS 116400GV-72400 自动机械钢带男表联保正品","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627750479728_360.png","goods_price":"63200.00","goods_salenum":"5","is_enquiry":"0","goods_state":"1"},{"goods_id":"100003","goods_name":"劳力士Rolex 日志型系列 自动机械钢带男士表 联保正品 116333","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627769865296_360.jpg","goods_price":"89200.00","goods_salenum":"1","is_enquiry":"0","goods_state":"1"},{"goods_id":"100009","goods_name":"劳力士Rolex 日志型系列 116200 63200 自动机械钢带男表联保正品","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627958339099_360.jpg","goods_price":"52800.00","goods_salenum":"5","is_enquiry":"0","goods_state":"1"},{"goods_id":"100237","goods_name":"批发商品测试","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_360.gif","goods_price":"3000000.00","goods_salenum":"12","is_enquiry":"0","goods_state":"1"},{"goods_id":"100240","goods_name":"劳力士Rolex 蚝式恒动系列自动机械钢带男表正品116523-8DI-78593 2","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627900055146_360.png","goods_price":"300.00","goods_salenum":"2","is_enquiry":"0","goods_state":"1"},{"goods_id":"100241","goods_name":"劳力士Rolex 蚝式恒动系列自动机械钢带男表正品116523-8DI-78593 3","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627900055146_360.png","goods_price":"400.00","goods_salenum":"2","is_enquiry":"0","goods_state":"1"},{"goods_id":"100268","goods_name":"劳力士Rolex 日志型系列 自动机械钢带男表 联保正品 116233 紅色","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627799921979_360.jpg","goods_price":"800.00","goods_salenum":"4","is_enquiry":"0","goods_state":"1"},{"goods_id":"100269","goods_name":"劳力士Rolex 日志型系列 自动机械钢带男表 联保正品 116233 藍色","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627799921979_360.jpg","goods_price":"800.00","goods_salenum":"0","is_enquiry":"0","goods_state":"1"},{"goods_id":"100344","goods_name":"自行走曲臂式","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796245285261627_360.png","goods_price":"10.00","goods_salenum":"63","is_enquiry":"0","goods_state":"1"}]
     * head_pic : {"supplier":"http://192.168.0.2/data/upload/shop/common/supplier.jpg","customer":"http://192.168.0.2/data/upload/shop/common/customer.jpg","brand":"http://192.168.0.2/data/upload/shop/common/brand.jpg"}
     * kyy : eyJQIjoxLCJLIjoia3l5XzAwMyJ9
     * currencytype : {"CNY":{"name":"人民币","symbol":"￥","currency":"CNY","cackeKey":"","rate":0},"HKD":{"name":"港币","symbol":"HK$","currency":"HKD","cackeKey":"cacheCurrencyHKD","rate":1.1498},"MOP":{"name":"葡币","symbol":"MOP$","currency":"MOP","cackeKey":"cacheCurrencyMOP","rate":1.18265},"USD":{"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.1463}}
     */

    private HeadPicBean head_pic;
    private String kyy;
    private CurrencytypeBean currencytype;
    private List<BannerBean> banner;
    private List<WebHtmlBean> web_html;
    private List<GoodsListBean> goods_list;

    public HeadPicBean getHead_pic() {
        return head_pic;
    }

    public void setHead_pic(HeadPicBean head_pic) {
        this.head_pic = head_pic;
    }

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public CurrencytypeBean getCurrencytype() {
        return currencytype;
    }

    public void setCurrencytype(CurrencytypeBean currencytype) {
        this.currencytype = currencytype;
    }

    public List<BannerBean> getBanner() {
        return banner;
    }

    public void setBanner(List<BannerBean> banner) {
        this.banner = banner;
    }

    public List<WebHtmlBean> getWeb_html() {
        return web_html;
    }

    public void setWeb_html(List<WebHtmlBean> web_html) {
        this.web_html = web_html;
    }

    public List<GoodsListBean> getGoods_list() {
        return goods_list;
    }

    public void setGoods_list(List<GoodsListBean> goods_list) {
        this.goods_list = goods_list;
    }

    public static class HeadPicBean {
        /**
         * supplier : http://192.168.0.2/data/upload/shop/common/supplier.jpg
         * customer : http://192.168.0.2/data/upload/shop/common/customer.jpg
         * brand : http://192.168.0.2/data/upload/shop/common/brand.jpg
         */

        private String supplier;
        private String customer;
        private String brand;

        public String getSupplier() {
            return supplier;
        }

        public void setSupplier(String supplier) {
            this.supplier = supplier;
        }

        public String getCustomer() {
            return customer;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }
    }

    public static class CurrencytypeBean {
        /**
         * CNY : {"name":"人民币","symbol":"￥","currency":"CNY","cackeKey":"","rate":0}
         * HKD : {"name":"港币","symbol":"HK$","currency":"HKD","cackeKey":"cacheCurrencyHKD","rate":1.1498}
         * MOP : {"name":"葡币","symbol":"MOP$","currency":"MOP","cackeKey":"cacheCurrencyMOP","rate":1.18265}
         * USD : {"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.1463}
         */

        private CNYBean CNY;
        private HKDBean HKD;
        private MOPBean MOP;
        private USDBean USD;

        public CNYBean getCNY() {
            return CNY;
        }

        public void setCNY(CNYBean CNY) {
            this.CNY = CNY;
        }

        public HKDBean getHKD() {
            return HKD;
        }

        public void setHKD(HKDBean HKD) {
            this.HKD = HKD;
        }

        public MOPBean getMOP() {
            return MOP;
        }

        public void setMOP(MOPBean MOP) {
            this.MOP = MOP;
        }

        public USDBean getUSD() {
            return USD;
        }

        public void setUSD(USDBean USD) {
            this.USD = USD;
        }

        public static class CNYBean {
            /**
             * name : 人民币
             * symbol : ￥
             * currency : CNY
             * cackeKey :
             * rate : 0
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class HKDBean {
            /**
             * name : 港币
             * symbol : HK$
             * currency : HKD
             * cackeKey : cacheCurrencyHKD
             * rate : 1.1498
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class MOPBean {
            /**
             * name : 葡币
             * symbol : MOP$
             * currency : MOP
             * cackeKey : cacheCurrencyMOP
             * rate : 1.18265
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class USDBean {
            /**
             * name : 美元
             * symbol : US$
             * currency : USD
             * cackeKey : cacheCurrencyUSD
             * rate : 0.1463
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }
    }

    public static class BannerBean {
        /**
         * adv_type : 2
         * image : http://192.168.0.2/data/upload/shop/adv/05769287024930069.jpg
         * url_type :
         * url_value :
         */

        private String adv_type;
        private String image;
        private String url_type;
        private String url_value;

        public String getAdv_type() {
            return adv_type;
        }

        public void setAdv_type(String adv_type) {
            this.adv_type = adv_type;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getUrl_type() {
            return url_type;
        }

        public void setUrl_type(String url_type) {
            this.url_type = url_type;
        }

        public String getUrl_value() {
            return url_value;
        }

        public void setUrl_value(String url_value) {
            this.url_value = url_value;
        }
    }

    public static class WebHtmlBean {
        /**
         * style_name : 1
         * web_name : 精选品牌
         * more_url :
         * title_color : #654
         * bg_color : #555
         * brand_list : [{"brand_id":"353","brand_name":"古今","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04431807497959652_sm.jpg"},{"brand_id":"351","brand_name":"曼妮芬（ManniForm）","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04431810033957836_sm.jpg"},{"brand_id":"348","brand_name":"奥唯嘉（Ovega）","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04431812331259168_sm.jpg"},{"brand_id":"345","brand_name":"乐扣乐扣","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399904614221217_sm.jpg"},{"brand_id":"344","brand_name":"乔曼帝","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399904423386126_sm.jpg"},{"brand_id":"342","brand_name":"富安娜","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399904168163421_sm.jpg"},{"brand_id":"329","brand_name":"欧司朗","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399902537418591_sm.jpg"},{"brand_id":"313","brand_name":"皮克朋","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399891285897466_sm.jpg"},{"brand_id":"225","brand_name":"伊莱克斯","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399834676870929_sm.gif"},{"brand_id":"226","brand_name":"艾力斯特","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399835435836906_sm.jpg"}]
         * recommend_list : [{"pic_name":"电动电钻,商品精选热销","url_type":"2","url_value":"1063","pic_id":"50","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-50.jpg?475"},{"pic_name":"机械专造,商品精选热销","url_type":"2","url_value":"1063","pic_id":"60","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-60.jpg?240"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"11","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-11.jpg?794"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"31","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-31.jpg?511"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"21","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-21.jpg?646"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"12","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-12.jpg?363"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"32","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-32.jpg?580"},{"pic_name":"高空作业平台,平台推荐","url_type":"2","url_value":"1063","pic_id":"24","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-632-711-1-24.jpg?658"}]
         * category_list : null
         * adv : {"url_type":"2","url_value":"1063","pic_id":"2","pic_img":"http://192.168.0.2/data/upload/shop/editor/web-633-712-2.jpg?501"}
         * goods_recommend_list : [{"goods_id":"100497","goods_name":"自行走曲臂式 FDF D RR","goods_price":"1.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796245285261627_240.png"},{"goods_id":"100362","goods_name":"自行走曲臂式GTBZ-AE","goods_price":"100.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796293756348179_240.png"},{"goods_id":"100344","goods_name":"自行走曲臂式","goods_price":"10.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796245285261627_240.png"},{"goods_id":"100473","goods_name":"222","goods_price":"100.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803134834338328_240.jpg"},{"goods_id":"100472","goods_name":"螺丝刀","goods_price":"10.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803176481834773_240.jpg"},{"goods_id":"100470","goods_name":"螺丝刀 发发发分付付付付付付付付付付付付付付 3是的 分 1 1 1","goods_price":"333.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803145000439866_240.jpg"},{"goods_id":"100469","goods_name":"222","goods_price":"2222.00","goods_pic":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803134834338328_240.jpg"}]
         */

        private String style_name;
        private String web_name;
        private String more_url;
        private String title_color;
        private String bg_color;
        private Object category_list;
        private AdvBean adv;
        private List<BrandListBean> brand_list;
        private List<RecommendListBean> recommend_list;
        private List<GoodsRecommendListBean> goods_recommend_list;

        public String getStyle_name() {
            return style_name;
        }

        public void setStyle_name(String style_name) {
            this.style_name = style_name;
        }

        public String getWeb_name() {
            return web_name;
        }

        public void setWeb_name(String web_name) {
            this.web_name = web_name;
        }

        public String getMore_url() {
            return more_url;
        }

        public void setMore_url(String more_url) {
            this.more_url = more_url;
        }

        public String getTitle_color() {
            return title_color;
        }

        public void setTitle_color(String title_color) {
            this.title_color = title_color;
        }

        public String getBg_color() {
            return bg_color;
        }

        public void setBg_color(String bg_color) {
            this.bg_color = bg_color;
        }

        public Object getCategory_list() {
            return category_list;
        }

        public void setCategory_list(Object category_list) {
            this.category_list = category_list;
        }

        public AdvBean getAdv() {
            return adv;
        }

        public void setAdv(AdvBean adv) {
            this.adv = adv;
        }

        public List<BrandListBean> getBrand_list() {
            return brand_list;
        }

        public void setBrand_list(List<BrandListBean> brand_list) {
            this.brand_list = brand_list;
        }

        public List<RecommendListBean> getRecommend_list() {
            return recommend_list;
        }

        public void setRecommend_list(List<RecommendListBean> recommend_list) {
            this.recommend_list = recommend_list;
        }

        public List<GoodsRecommendListBean> getGoods_recommend_list() {
            return goods_recommend_list;
        }

        public void setGoods_recommend_list(List<GoodsRecommendListBean> goods_recommend_list) {
            this.goods_recommend_list = goods_recommend_list;
        }

        public static class AdvBean {
            /**
             * url_type : 2
             * url_value : 1063
             * pic_id : 2
             * pic_img : http://192.168.0.2/data/upload/shop/editor/web-633-712-2.jpg?501
             */

            private String url_type;
            private String url_value;
            private String pic_id;
            private String pic_img;

            public String getUrl_type() {
                return url_type;
            }

            public void setUrl_type(String url_type) {
                this.url_type = url_type;
            }

            public String getUrl_value() {
                return url_value;
            }

            public void setUrl_value(String url_value) {
                this.url_value = url_value;
            }

            public String getPic_id() {
                return pic_id;
            }

            public void setPic_id(String pic_id) {
                this.pic_id = pic_id;
            }

            public String getPic_img() {
                return pic_img;
            }

            public void setPic_img(String pic_img) {
                this.pic_img = pic_img;
            }
        }

        public static class BrandListBean {
            private String pic_name;
            private String url_type;
            private String url_value;
            private String pic_id;
            private String pic_img;

            public String getPic_name() {
                return pic_name;
            }

            public void setPic_name(String pic_name) {
                this.pic_name = pic_name;
            }

            public String getUrl_type() {
                return url_type;
            }

            public void setUrl_type(String url_type) {
                this.url_type = url_type;
            }

            public String getUrl_value() {
                return url_value;
            }

            public void setUrl_value(String url_value) {
                this.url_value = url_value;
            }

            public String getPic_id() {
                return pic_id;
            }

            public void setPic_id(String pic_id) {
                this.pic_id = pic_id;
            }

            public String getPic_img() {
                return pic_img;
            }

            public void setPic_img(String pic_img) {
                this.pic_img = pic_img;
            }
        }

        public static class RecommendListBean {
            /**
             * pic_name : 电动电钻,商品精选热销
             * url_type : 2
             * url_value : 1063
             * pic_id : 50
             * pic_img : http://192.168.0.2/data/upload/shop/editor/web-632-711-1-50.jpg?475
             */

            private String pic_name;
            private String url_type;
            private String url_value;
            private String pic_id;
            private String pic_img;

            public String getPic_name() {
                return pic_name;
            }

            public void setPic_name(String pic_name) {
                this.pic_name = pic_name;
            }

            public String getUrl_type() {
                return url_type;
            }

            public void setUrl_type(String url_type) {
                this.url_type = url_type;
            }

            public String getUrl_value() {
                return url_value;
            }

            public void setUrl_value(String url_value) {
                this.url_value = url_value;
            }

            public String getPic_id() {
                return pic_id;
            }

            public void setPic_id(String pic_id) {
                this.pic_id = pic_id;
            }

            public String getPic_img() {
                return pic_img;
            }

            public void setPic_img(String pic_img) {
                this.pic_img = pic_img;
            }
        }

        public static class GoodsRecommendListBean {
            /**
             * goods_id : 100497
             * goods_name : 自行走曲臂式 FDF D RR
             * goods_price : 1.00
             * goods_pic : http://192.168.0.2/data/upload/shop/store/goods/1/1_05796245285261627_240.png
             */

            private String goods_id;
            private String goods_name;
            private String goods_price;
            private String goods_pic;
            private String is_enquiry;

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(String goods_price) {
                this.goods_price = goods_price;
            }

            public String getGoods_pic() {
                return goods_pic;
            }

            public void setGoods_pic(String goods_pic) {
                this.goods_pic = goods_pic;
            }

            public String getIs_enquiry() {
                return is_enquiry;
            }

            public void setIs_enquiry(String is_enquiry) {
                this.is_enquiry = is_enquiry;
            }
        }
    }

    public static class GoodsListBean {
        /**
         * goods_id : 100001
         * goods_name : 劳力士Rolex 深海系列 自动机械钢带男士表 联保正品116660 98210
         * goods_image : http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627707766698_360.png
         * goods_price : 87500.00
         * goods_salenum : 8
         * is_enquiry : 0
         * goods_state : 1
         */

        private String goods_id;
        private String goods_name;
        private String goods_image;
        private String goods_price;
        private String goods_salenum;
        private String is_enquiry;
        private String goods_state;
        private int is_second_hand;


        public int getIs_second_hand() {
            return is_second_hand;
        }

        public void setIs_second_hand(int is_second_hand) {
            this.is_second_hand = is_second_hand;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_image() {
            return goods_image;
        }

        public void setGoods_image(String goods_image) {
            this.goods_image = goods_image;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getGoods_salenum() {
            return goods_salenum;
        }

        public void setGoods_salenum(String goods_salenum) {
            this.goods_salenum = goods_salenum;
        }

        public String getIs_enquiry() {
            return is_enquiry;
        }

        public void setIs_enquiry(String is_enquiry) {
            this.is_enquiry = is_enquiry;
        }

        public String getGoods_state() {
            return goods_state;
        }

        public void setGoods_state(String goods_state) {
            this.goods_state = goods_state;
        }
    }
}
