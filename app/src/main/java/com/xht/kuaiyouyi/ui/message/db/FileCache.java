package com.xht.kuaiyouyi.ui.message.db;

import org.litepal.crud.LitePalSupport;

/**
 * 文件缓存表
 */
public class FileCache extends LitePalSupport {
    private String file_url;//下载链接
    private String file_name;
    private long file_length;
    private String file_type;
    private String file_local_path;//本地路径


    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public long getFile_length() {
        return file_length;
    }

    public void setFile_length(long file_length) {
        this.file_length = file_length;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getFile_local_path() {
        return file_local_path;
    }

    public void setFile_local_path(String file_local_path) {
        this.file_local_path = file_local_path;
    }
}
