package com.xht.kuaiyouyi.ui.home.entity;

import java.util.List;

public class ExchangerateBean {


    /**
     * flag : 1
     * err_code : 0
     * msg :
     * data : {"currency":[{"name":"人民币","symbol":"￥","currency":"CNY","cackeKey":"","rate":0},{"name":"港币","symbol":"HK$","currency":"HKD","cackeKey":"cacheCurrencyHKD","rate":1.1498},{"name":"葡币","symbol":"MOP$","currency":"MOP","cackeKey":"cacheCurrencyMOP","rate":1.18265},{"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.1463}]}
     */

        private List<CurrencyBean> currency;

        public List<CurrencyBean> getCurrency() {
            return currency;
        }

        public void setCurrency(List<CurrencyBean> currency) {
            this.currency = currency;
        }

        public static class CurrencyBean {
            /**
             * name : 人民币
             * symbol : ￥
             * currency : CNY
             * cackeKey :
             * rate : 0
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }
}
