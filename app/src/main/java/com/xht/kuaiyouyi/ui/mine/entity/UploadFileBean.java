package com.xht.kuaiyouyi.ui.mine.entity;

public class UploadFileBean {

    /**
     * file_name : http://192.168.0.2/data/upload/shop/company/logo_5b4722c4cf351.jpeg
     */

    private String file_name;
    private String file_name_short;

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_name_short() {
        return file_name_short;
    }

    public void setFile_name_short(String file_name_short) {
        this.file_name_short = file_name_short;
    }
}
