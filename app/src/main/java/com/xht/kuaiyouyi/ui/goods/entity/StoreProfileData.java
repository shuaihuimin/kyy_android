package com.xht.kuaiyouyi.ui.goods.entity;

import java.util.List;

public class StoreProfileData {

        /**
         * image_arr : [{"image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05863644205577206_360.jpg","image_name":"营业执照"},{"image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_360.gif","image_name":"1"},{"image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_360.gif","image_name":"2"},{"image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_360.gif","image_name":"3"},{"image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_360.gif","image_name":"4"},{"image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_360.gif","image_name":"5"}]
         * info : {"content":" 珠海快優易科技有限公司(原珠海新海通電子商務有限公司)成立 於2015年，是一家專業為港澳客戶提供跨境一站式電子商務整合服務的公司。作為機械工程配件與建築材料供應鏈服務平臺，新海通始終堅持 \u201c坦誠、務實、高效、創新\u201d的經營理念，依託互聯網技術打通線上線下渠道，為供應鏈上下游提供建材貿易、平臺集采、品質背書、產品檢驗、報關貨運及售後維修等綜合服務，目標成為供應鏈最大整合者。","detail_content":" 珠海快優易科技有限公司(原珠海新海通電子商務有限公司)成立 於2015年，是一家專業為港澳客戶提供跨境一站式電子商務整合服務的公司。作為機械工程配件與建築材料供應鏈服務平臺，新海通始終堅持 \u201c坦誠、務實、高效、創新\u201d的經營理念，依託互聯網技術打通線上線下渠道，為供應鏈上下游提供建材貿易、平臺集采、品質背書、產品檢驗、報關貨運及售後維修等綜合服務，目標成為供應鏈最大整合者。","status":"1","store_id":"1","certification_id":"6"}
         */

        private InfoBean info;
        private List<ImageArrBean> image_arr;

        public InfoBean getInfo() {
            return info;
        }

        public void setInfo(InfoBean info) {
            this.info = info;
        }

        public List<ImageArrBean> getImage_arr() {
            return image_arr;
        }

        public void setImage_arr(List<ImageArrBean> image_arr) {
            this.image_arr = image_arr;
        }

        public static class InfoBean {
            /**
             * content :  珠海快優易科技有限公司(原珠海新海通電子商務有限公司)成立 於2015年，是一家專業為港澳客戶提供跨境一站式電子商務整合服務的公司。作為機械工程配件與建築材料供應鏈服務平臺，新海通始終堅持 “坦誠、務實、高效、創新”的經營理念，依託互聯網技術打通線上線下渠道，為供應鏈上下游提供建材貿易、平臺集采、品質背書、產品檢驗、報關貨運及售後維修等綜合服務，目標成為供應鏈最大整合者。
             * detail_content :  珠海快優易科技有限公司(原珠海新海通電子商務有限公司)成立 於2015年，是一家專業為港澳客戶提供跨境一站式電子商務整合服務的公司。作為機械工程配件與建築材料供應鏈服務平臺，新海通始終堅持 “坦誠、務實、高效、創新”的經營理念，依託互聯網技術打通線上線下渠道，為供應鏈上下游提供建材貿易、平臺集采、品質背書、產品檢驗、報關貨運及售後維修等綜合服務，目標成為供應鏈最大整合者。
             * status : 1
             * store_id : 1
             * certification_id : 6
             */

            private String content;
            private String detail_content;
            private String status;
            private String store_id;
            private String certification_id;
            private String store_name;

            public String getStore_name() {
                return store_name;
            }

            public void setStore_name(String store_name) {
                this.store_name = store_name;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getDetail_content() {
                return detail_content;
            }

            public void setDetail_content(String detail_content) {
                this.detail_content = detail_content;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getStore_id() {
                return store_id;
            }

            public void setStore_id(String store_id) {
                this.store_id = store_id;
            }

            public String getCertification_id() {
                return certification_id;
            }

            public void setCertification_id(String certification_id) {
                this.certification_id = certification_id;
            }
        }

        public static class ImageArrBean {
            /**
             * image : http://192.168.0.2/data/upload/shop/store/goods/1/1_05863644205577206_360.jpg
             * image_name : 营业执照
             */

            private String image;
            private String image_name;

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getImage_name() {
                return image_name;
            }

            public void setImage_name(String image_name) {
                this.image_name = image_name;
            }
        }
}
