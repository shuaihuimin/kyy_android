package com.xht.kuaiyouyi.ui.cart.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.cart.entity.CompanyListBean;

public class CompanyListAdapter extends BaseAdapter {
    private Context mContext;
    private CompanyListBean mCompanyListBean;


    public CompanyListAdapter(Context context, CompanyListBean companyListBean) {
        this.mContext = context;
        this.mCompanyListBean = companyListBean;
    }

    @Override
    public int getCount() {
        if(mCompanyListBean!=null&&mCompanyListBean.getList()!=null&&mCompanyListBean.getList().size()>0){
            return mCompanyListBean.getList().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mCompanyListBean.getList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.item_procurement_method, null);
            viewHolder = new ViewHolder();
            viewHolder.iv_selected = convertView.findViewById(R.id.iv_selected);
            viewHolder.tv_company_name = convertView.findViewById(R.id.tv_company_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tv_company_name.setText(mCompanyListBean.getList().get(position).getCompany_name());
        if(mCompanyListBean.getList().get(position).isSelect()){
            viewHolder.iv_selected.setImageResource(R.mipmap.pop_icon_tick);
        }else {
            viewHolder.iv_selected.setImageResource(android.R.color.transparent);
        }
        return convertView;
    }

    /**
     * 删除或者设置默认地址
     * @param position
     * @param type    DELETE_ADDRESS或者DEFAULT_ADDRESS
     * @param url
     */
    private void requestCheckAddressDefaultOrDelete(final int position, final int type, String url) {
//        NetUtil.Companion.getInstance().url(url)
//                .addParam("address_check_id",mCheckAddressListBean.getAddress().get(position).getAddress_check_id())
//                .withPOST(new NetCallBack<String>() {
//
//                    @NotNull
//                    @Override
//                    public Class<String> getRealType() {
//                        return String.class;
//                    }
//
//                    @Override
//                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
//                        Toast.makeText(mContext,err,Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onSuccess(@NonNull String str) {
//                        if(type == DEFAULT_ADDRESS){
//                            if(mSelectPosition != null){
//                                mCheckAddressListBean.getAddress().get(mSelectPosition).setIs_default("0");
//                            }
//                            mCheckAddressListBean.getAddress().get(position).setIs_default("1");
//                        }
//                        if(type == DELETE_ADDRESS){
//                            mCheckAddressListBean.getAddress().remove(position);
//                        }
//                        notifyDataSetChanged();
//
//                    }
//                },false);
    }

    private class ViewHolder {
        private TextView tv_company_name;
        private ImageView iv_selected;
    }
}
