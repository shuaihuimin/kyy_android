package com.xht.kuaiyouyi.ui.mine.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.login.activity.InputPhoneActivity;
import com.xht.kuaiyouyi.ui.login.activity.SetPasswordActivity;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.ButtomDialog;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;


/**
 * 身份验证动态验证码页面
 * (更换密码，更换手机号（旧手机验证））
 *
 *
 */
public class AuthenticationActivity extends BaseActivity{
    @BindView(R.id.tv_title)
    TextView title;
    @BindView(R.id.iv_back)
    ImageView back_image;
    @BindView(R.id.inequipment_code)
    EditText clearEditText;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.tv_count_down)
    TextView tv_count_down;
    @BindView(R.id.tv_regain)
    TextView tv_regain;
    @BindView(R.id.tv_phone)
    TextView tv_phone;
    @BindView(R.id.tv_phone_disable)
    TextView tv_phone_disable;

    private String mPhone;
    private int mCountDown = 60;//倒计时60s开始

    private String mType;

    private ButtomDialog mButtomDialog;

    /**
     * 中国大陆、中国香港、中国澳门
     * 香港00852-6854 0809
     * 澳門00853-6242 5277
     * 大陆0756-6700866
     */
    private String[] mTeles={"0756-6700866","00852-68540809","00853-62425277"};

    private android.os.Handler mHandler = new android.os.Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(Message msg) {
            mCountDown = mCountDown-1;
            if(mCountDown>0){
                tv_count_down.setText(mCountDown + "s"+getString(R.string.can_resend));
                mHandler.sendEmptyMessageDelayed(1,1000);
            }else {
                tv_count_down.setVisibility(View.GONE);
                tv_regain.setVisibility(View.VISIBLE);
                tv_regain.setText(R.string.regain);
            }
        }
    };
    @Override
    protected int getLayout() {
        return R.layout.activity_authentication;
    }

    @Override
    protected void initView() {
        mPhone= Login.Companion.getInstance().getPhone();
        mType=getIntent().getStringExtra(Contant.TYPE);

        title.setText(R.string.identity_verify);
        tv_phone.setText(Utils.getDisplayPhone(mPhone));
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captcha(mPhone,clearEditText.getText().toString().trim());
            }
        });
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        clearEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0){
                    btn_submit.setEnabled(true);
                }
                if(s.length() <= 0){
                    btn_submit.setEnabled(false);
                }
            }
        });
        tv_regain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestVerifyCode();
            }
        });

        tv_phone_disable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showButtonDialog();
            }
        });

    }

    //验证动态验证码
    private void captcha(final String phone, final String cet_code_s){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CONNECT_CHECK_SMS_CAPTCHA())
                .addParam("phone",phone)
                .addParam("captcha",cet_code_s)
                .addParam("type",mType)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(AuthenticationActivity.this,err,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        if(isFinishing()){
                            return;
                        }
                        Bundle bundle = new Bundle();
                        switch (mType){
                            case Contant.TYPE_CHANGE_PHONE:
                                bundle.putString(Contant.TYPE,Contant.TYPE_CHANGE_PHONE);
                                goToActivity(InputPhoneActivity.class,bundle);
                                finish();
                                break;
                            case Contant.TYPE_CHANGE_PASSWORD:
                                bundle.putString(Contant.TYPE,Contant.TYPE_CHANGE_PASSWORD);
                                goToActivity(SetPasswordActivity.class,bundle);
                                finish();
                                break;
                        }
                    }
                },false);

    }
    

    private void requestVerifyCode() {
        DialogUtils.createTipAllLoadDialog(AuthenticationActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CONNECT_GET_SMS_CAPTCHA())
                .addParam("phone", mPhone)
                .addParam("type", mType)
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(AuthenticationActivity.this,err,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(getApplicationContext(),R.string.verify_code_sended,Toast.LENGTH_SHORT).show();
                        tv_regain.setVisibility(View.GONE);
                        tv_count_down.setVisibility(View.VISIBLE);
                        mCountDown = 60;
                        tv_count_down.setText(mCountDown + "s"+getString(R.string.can_resend));
                        mHandler.sendEmptyMessageDelayed(1,1000);
                    }
                }, false);
    }

    @Override
    protected void onDestroy() {
        if(mHandler.hasMessages(1)){
            mHandler.removeMessages(1);
        }
        mHandler = null;
        super.onDestroy();
    }

    private void showButtonDialog() {
        if(mButtomDialog == null){
            View view = LayoutInflater.from(this).inflate(R.layout.dialog_buttom_four_btn,null);
            mButtomDialog = new ButtomDialog(this,view,false,true);
            final TextView tv_cancel = view.findViewById(R.id.tv_cancel);
            TextView tv_cn = view.findViewById(R.id.tv_cn);
            tv_cn.setText(getString(R.string.cn)+" "+mTeles[0]);
            TextView tv_hk = view.findViewById(R.id.tv_hk);
            tv_hk.setText(getString(R.string.cn_hk)+" "+mTeles[1]);
            TextView tv_mo = view.findViewById(R.id.tv_mo);
            tv_mo.setText(getString(R.string.cn_macau)+" "+mTeles[2]);
            tv_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                }
            });
            tv_cn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    call(mTeles[0]);
                }
            });
            tv_hk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    call(mTeles[1]);
                }
            });
            tv_mo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    call(mTeles[2]);
                }
            });
        }
        mButtomDialog.show();

    }

    private void call(String tele){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + tele));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
        mButtomDialog.dismiss();
    }
}
