package com.xht.kuaiyouyi.ui.message.bean;

public class PushBean {
    private String approve_order_id;
    private String order_main_id;
    private String company_id;
    private String goods_id;

    public String getApprove_order_id() {
        return approve_order_id;
    }

    public void setApprove_order_id(String approve_order_id) {
        this.approve_order_id = approve_order_id;
    }

    public String getOrder_main_id() {
        return order_main_id;
    }

    public void setOrder_main_id(String order_main_id) {
        this.order_main_id = order_main_id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }
}
