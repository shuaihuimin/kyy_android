package com.xht.kuaiyouyi.ui;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.utils.Login;

public class GuideActivity extends BaseActivity {
    private int[] mImageRess = {R.mipmap.guide_1, R.mipmap.guide_2, R.mipmap.guide_3, R.mipmap.guide_4};
    private ViewPager vp_guide;
    private TextView tv_experience;

    @Override
    protected int getLayout() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_guide;
    }

    @Override
    protected void initView() {
        vp_guide = findViewById(R.id.vp_guide);
        tv_experience = findViewById(R.id.tv_experience);
        MyAdapter mAdapter = new MyAdapter();
        vp_guide.setAdapter(mAdapter);
        vp_guide.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == mImageRess.length - 1) {
                    tv_experience.setVisibility(View.VISIBLE);
                } else {
                    tv_experience.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onBackPressed() {

    }

    public void toMainActivity(View view) {
        /**
         * 切换为非全屏
         */
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        goToActivity(MainActivity.class);
        Login.Companion.getInstance().setShowGuide("0");
        finish();
    }

    class MyAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mImageRess.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View view = View.inflate(GuideActivity.this,R.layout.item_guide_img,null);
            ImageView iv_guide = view.findViewById(R.id.iv_guide);
            iv_guide.setImageResource(mImageRess[position]);
            container.addView(iv_guide);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }
    }


}
