package com.xht.kuaiyouyi.ui.mine.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.cart.order.ChooseAddressActivity;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.mine.address.AddressListFragment;
import com.xht.kuaiyouyi.ui.mine.address.NewAddressActivity;
import com.xht.kuaiyouyi.ui.mine.collection.OnShowEmptyView;
import com.xht.kuaiyouyi.ui.mine.entity.ReceiveAddressListBean;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

public class ReceiveAddressListAdapter extends BaseAdapter {
    private Context mContext;
    private ReceiveAddressListBean mReceiveAddressListBean;
    private AddressListFragment mFragment;
    private ChooseAddressActivity mActivity;
    private Integer mSelectPosition;
    private int flag;

    private final int DELETE_ADDRESS = 1;
    private final int DEFAULT_ADDRESS = 2;

    private OnShowEmptyView mShowEmptyListener;

    public ReceiveAddressListAdapter(Context context, ReceiveAddressListBean receiveAddressListBean,AddressListFragment fragment,ChooseAddressActivity activity,int flag) {
        this.mContext = context;
        this.mReceiveAddressListBean = receiveAddressListBean;
        this.mFragment = fragment;
        this.mActivity=activity;
        this.flag=flag;
    }

    @Override
    public int getCount() {
        if (mReceiveAddressListBean != null && mReceiveAddressListBean.getAddress() != null && mReceiveAddressListBean.getAddress().size() > 0) {
            return mReceiveAddressListBean.getAddress().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mReceiveAddressListBean.getAddress().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.item_address, null);
            viewHolder = new ViewHolder();
            viewHolder.iv_check = convertView.findViewById(R.id.iv_check);
            viewHolder.iv_delete = convertView.findViewById(R.id.iv_delete);
            viewHolder.tv_name = convertView.findViewById(R.id.tv_name);
            viewHolder.tv_mobile = convertView.findViewById(R.id.tv_mobile);
            viewHolder.tv_standby_mobile = convertView.findViewById(R.id.tv_standby_mobile);
            viewHolder.tv_telephone = convertView.findViewById(R.id.tv_telephone);
            viewHolder.tv_address = convertView.findViewById(R.id.tv_address);
            viewHolder.tv_edit = convertView.findViewById(R.id.tv_edit);
            viewHolder.ll_check = convertView.findViewById(R.id.ll_check);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_name.setText(mReceiveAddressListBean.getAddress().get(position).getTrue_name());
        if(mReceiveAddressListBean.getAddress().get(position).getIs_default().equals("1")){
            viewHolder.iv_check.setImageResource(R.mipmap.pop_icon_tick);
            mSelectPosition = position;
        }else {
            viewHolder.iv_check.setImageResource(R.mipmap.address_icon_unselected_nor);
        }
        viewHolder.tv_address.setText(mReceiveAddressListBean.getAddress().get(position).getArea_info()
                + mReceiveAddressListBean.getAddress().get(position).getAddress());
        viewHolder.tv_mobile.setText("+" + mReceiveAddressListBean.getAddress().get(position).getMobile_zone1()
                + "  " +mReceiveAddressListBean.getAddress().get(position).getMob_phone());
        if(!TextUtils.isEmpty(mReceiveAddressListBean.getAddress().get(position).getMob_phone_spare())){
            viewHolder.tv_standby_mobile.setVisibility(View.VISIBLE);
            viewHolder.tv_standby_mobile.setText("+" + mReceiveAddressListBean.getAddress().get(position).getMobile_zone1_spare()
                    + "  " +mReceiveAddressListBean.getAddress().get(position).getMob_phone_spare());
        }else {
            viewHolder.tv_standby_mobile.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(mReceiveAddressListBean.getAddress().get(position).getTel_phone())){
            viewHolder.tv_telephone.setVisibility(View.VISIBLE);
            viewHolder.tv_telephone.setText("+" + mReceiveAddressListBean.getAddress().get(position).getMobile_zone2()
                    + "  " +mReceiveAddressListBean.getAddress().get(position).getTel_zone() +
                    mReceiveAddressListBean.getAddress().get(position).getTel_phone());
        }else {
            viewHolder.tv_telephone.setVisibility(View.GONE);
        }
        viewHolder.ll_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mReceiveAddressListBean.getAddress().get(position).getIs_default().equals("0")){
                    requestReceiveAddressDefaultOrDelete(position,DEFAULT_ADDRESS,KyyConstants.INSTANCE.getURL_DEFAULT_ADDRESS());
                }
            }
        });
        viewHolder.tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag==AddressListFragment.FLAG) {
                    Intent intent = new Intent(mContext, NewAddressActivity.class);
                    intent.putExtra(Contant.TYPE, NewAddressActivity.TYPE_EDIT_RECEIVE_ADDRESS);
                    intent.putExtra(NewAddressActivity.RECEIVE_ADDRESS_BEAN, mReceiveAddressListBean
                            .getAddress().get(position));
                    mFragment.startActivityForResult(intent, AddressListFragment.REQUEST_CODE);
                }else if(flag==ChooseAddressActivity.FLAG){
                    Intent intent = new Intent(mContext, NewAddressActivity.class);
                    intent.putExtra(Contant.TYPE, NewAddressActivity.TYPE_EDIT_RECEIVE_ADDRESS);
                    intent.putExtra(NewAddressActivity.RECEIVE_ADDRESS_BEAN, mReceiveAddressListBean
                            .getAddress().get(position));
                    mActivity.startActivityForResult(intent, AddressListFragment.REQUEST_CODE);
                }
            }
        });
        final TextView textView=viewHolder.tv_edit;
        viewHolder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag==ChooseAddressActivity.FLAG && mReceiveAddressListBean.getAddress().size()==1){

                    textView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            DialogUtils.createTipAllTextDialog(mContext,mContext.getString(R.string.toast_detils));
                        }
                    },1000);
                }else{
                    DialogUtils.createTwoBtnDialog(mContext,
                            mContext.getResources().getString(R.string.are_you_sure_delete_receive_address),
                            mContext.getResources().getString(R.string.dialog_confirm),
                            mContext.getResources().getString(R.string.dialog_cancel),
                            new DialogUtils.OnRightBtnListener() {
                                @Override
                                public void setOnRightListener(Dialog dialog) {
                                    requestReceiveAddressDefaultOrDelete(position,DELETE_ADDRESS,KyyConstants.INSTANCE.getURL_DELETE_ADDRESS());
                                }
                            },null,false,true);
                }

            }
        });
        return convertView;
    }

    /**
     * 删除或者设置默认地址
     * @param position
     * @param type    DELETE_ADDRESS或者DEFAULT_ADDRESS
     * @param url
     */
    private void requestReceiveAddressDefaultOrDelete(final int position, final int type, String url) {
        NetUtil.Companion.getInstance().url(url)
                .addParam("address_id",mReceiveAddressListBean.getAddress().get(position).getAddress_id())
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        Toast.makeText(mContext,err,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        String tip="";
                        if(type == DEFAULT_ADDRESS){
                            if(mSelectPosition != null){
                                mReceiveAddressListBean.getAddress().get(mSelectPosition).setIs_default("0");
                            }
                            mReceiveAddressListBean.getAddress().get(position).setIs_default("1");
                            tip = mContext.getString(R.string.set_successful);
                        }
                        if(type == DELETE_ADDRESS){
                            mReceiveAddressListBean.getAddress().remove(position);
                            tip = mContext.getString(R.string.delete_succeful);
                            if(mReceiveAddressListBean.getAddress().size()==0){
                                if(mShowEmptyListener!=null){
                                    mShowEmptyListener.onShowEmptyView();
                                }
                            }
                        }
                        DialogUtils.createTipImageAndTextDialog(mContext,tip,R.mipmap.png_icon_popup_ok);
                        notifyDataSetChanged();

                    }
                },false);
    }

    public void setShowEmptyListener(OnShowEmptyView listener){
        mShowEmptyListener = listener;
    }

    private class ViewHolder {
        private TextView tv_name, tv_mobile, tv_standby_mobile, tv_telephone, tv_address, tv_edit;
        private ImageView iv_delete, iv_check;
        private LinearLayout ll_check;
    }
}
