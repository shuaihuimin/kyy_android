package com.xht.kuaiyouyi.ui.mine.order;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.widget.BadgeView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MineOrderActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView title;
    @BindView(R.id.iv_back)
    ImageView backimg;
    @BindView(R.id.iv_right)
    ImageView iv_right;
    @BindView(R.id.tl_tabs)
    TabLayout tl_tabs;
    @BindView(R.id.vp_fragment)
    ViewPager vp_fragment;

    private BadgeView mBadgeView;

    public final static int TAB_ALL_ORDER = 0;
    public final static int TAB_WAIT_PAY_ORDER = 1;
    public final static int TAB_WAIT_SEND_ORDER = 2;
    public final static int TAB_WAIT_RECEIVE_ORDER = 3;
    public final static int TAB_COMPLETE_ORDER = 4;


    public final static String ORDER_TYPE = "order_type";

    private List<OrderListfragment> mFragments;
    private int[] mTabTitles = {R.string.all_tab,R.string.mine_wait_payment,R.string.mine_wait_send
            ,R.string.mine_wait_receive,R.string.completed};

    private int mType;

    /**
     * 企业订单或者我的订单
     */
    private int mOrderType;
    @Override
    protected int getLayout() {
        return R.layout.activity_mineorder;
    }

    @Override
    protected void initView() {
        mBadgeView = new BadgeView(this,iv_right,10,4);
        iv_right.setVisibility(View.VISIBLE);
        backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        iv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(MessageActivity.class);
            }
        });
        mType = getIntent().getIntExtra(Contant.TYPE,TAB_ALL_ORDER);
        mOrderType = getIntent().getIntExtra(ORDER_TYPE,OrderListfragment.TYPE_MINE_ORDER);
        if(mOrderType==OrderListfragment.TYPE_MINE_ORDER){
            title.setText(R.string.mine_order);
        }else if(mOrderType==OrderListfragment.TYPE_ENTERPRISE_ORDER){
            title.setText(R.string.enterprise_order);
        }
        mFragments = new ArrayList<>();
        OrderListfragment fragmentAllOrder = new OrderListfragment();
        fragmentAllOrder.setType(TAB_ALL_ORDER,mOrderType);

        OrderListfragment fragmentWaitPayOrder = new OrderListfragment();
        fragmentWaitPayOrder.setType(TAB_WAIT_PAY_ORDER,mOrderType);

        OrderListfragment fragmentWaitSendOrder = new OrderListfragment();
        fragmentWaitSendOrder.setType(TAB_WAIT_SEND_ORDER,mOrderType);

        OrderListfragment fragmentWaitReceiveOrder = new OrderListfragment();
        fragmentWaitReceiveOrder.setType(TAB_WAIT_RECEIVE_ORDER,mOrderType);

        OrderListfragment fragmentCompleteOrder = new OrderListfragment();
        fragmentCompleteOrder.setType(TAB_COMPLETE_ORDER,mOrderType);


        mFragments.add(fragmentAllOrder);
        mFragments.add(fragmentWaitPayOrder);
        mFragments.add(fragmentWaitSendOrder);
        mFragments.add(fragmentWaitReceiveOrder);
        mFragments.add(fragmentCompleteOrder);

        MyAdapter adapter = new MineOrderActivity.MyAdapter(getSupportFragmentManager());
        vp_fragment.setAdapter(adapter);
        tl_tabs.setupWithViewPager(vp_fragment);
        vp_fragment.setCurrentItem(mType);
        showUnreadMsgNum();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event){
        if(event.getMessage().equals(MessageEvent.REFRESH_ORDER_LIST)){
            for (OrderListfragment fragment:mFragments){
                if(fragment.isAdded()){
                    fragment.mKyy="";
                    fragment.requestOrderList();
                }
            }
        }

        if(event.getMessage().equals(MessageEvent.UNREAD_MSG_NUM)){
            showUnreadMsgNum();
        }
    }

    private void showUnreadMsgNum() {
        mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
    }

    class MyAdapter extends FragmentPagerAdapter {

        private MyAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public OrderListfragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        //重写这个方法，将设置每个Tab的标题
        @Override
        public CharSequence getPageTitle(int position) {
            return getString(mTabTitles[position]);
        }
    }
}
