package com.xht.kuaiyouyi.ui.login.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.login.LoginUtil;
import com.xht.kuaiyouyi.ui.login.entity.AreaCodeBean;
import com.xht.kuaiyouyi.ui.login.entity.LoginBean;
import com.xht.kuaiyouyi.ui.mine.entity.GetSmsCaptcha1Bean;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by shuaihuimin on 2018/6/22.
 */

public class LoginActivity extends BaseActivity {
    @BindView(R.id.et_phone)
    EditText phoneEditText;
    @BindView(R.id.et_loin_pwd)
    EditText pwdEditText;
    @BindView(R.id.iv_see_pwd)
    ImageView iv_see_pwd;
    @BindView(R.id.btn_login)
    Button loginbtn;
    @BindView(R.id.tv_forget_password)
    TextView tv_forget_password;
    @BindView(R.id.btn_regist)
    TextView regisView;
    @BindView(R.id.tv_login_type)
    TextView tv_login_type;
    @BindView(R.id.ll_forget_password)
    LinearLayout ll_forget_password;
    @BindView(R.id.tv_get_verify_code)
    TextView tv_get_verify_code;
    @BindView(R.id.tv_area)
    TextView tv_area;
    @BindView(R.id.rl_area)
    RelativeLayout rl_area;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;



    private String mPhone = "";
    private String mPassword = "";
    private String mVerifyCode = "";
    private String mAreaCode = "86";

    private static final int PASSWORD_LOGIN = 0;
    private static final int VERIFY_CODE_LOGIN = 1;
    private int mLoginType = PASSWORD_LOGIN;

    private int mCountDown = 60;//倒计时60s开始

    private OptionsPickerView mOptionsPickerView;

    private final String[] AREA_CODES = {"86", "853", "852"};
    private List<String> mAreaDisplayList;
    private List<AreaCodeBean> mPickerViewData;

    private int mVerifyCodeType;

    private boolean isStop;
    private boolean isRequestedVerifyCode;//是否请求过短信验证码，短信登录的时候使用到

    private android.os.Handler mHandler = new android.os.Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(Message msg) {
            mCountDown = mCountDown-1;
            if(mCountDown>0){
                tv_get_verify_code.setText(mCountDown + "s" + getResources().getString(R.string.regain));
                tv_get_verify_code.setEnabled(false);
                tv_get_verify_code.setTextColor(getResources().getColor(R.color.grey_5));
                mHandler.sendEmptyMessageDelayed(1,1000);
            }else {
                tv_get_verify_code.setText(getResources().getString(R.string.get_dycode));
                tv_get_verify_code.setEnabled(true);
                tv_get_verify_code.setTextColor(getResources().getColor(R.color.blue_normal));
            }
        }
    };

    @Override
    protected int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        tv_title.setVisibility(View.INVISIBLE);
        iv_back.setImageResource(R.mipmap.login_icon_x_nor);
        pwdEditText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
        //注册
        regisView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(RegisteredActivity.class);
            }
        });

        //登录
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPhone = phoneEditText.getText().toString().trim();
                if(mLoginType == PASSWORD_LOGIN){
                    mPassword = pwdEditText.getText().toString().trim();
                    if(TextUtils.isEmpty(mPassword) || TextUtils.isEmpty(mPhone)){
                        Toast.makeText(LoginActivity.this,R.string.login_no_empty_tip,
                                Toast.LENGTH_SHORT).show();
                    }else {
                        passwordLogin();
                    }
                }
                if(mLoginType == VERIFY_CODE_LOGIN){
                    mVerifyCode = pwdEditText.getText().toString().trim();
                    if(!isRequestedVerifyCode){
                        Toast.makeText(LoginActivity.this,R.string.please_request_verify_code,
                                Toast.LENGTH_SHORT).show();
                    }else if(TextUtils.isEmpty(mVerifyCode) || TextUtils.isEmpty(mPhone)){
                        Toast.makeText(LoginActivity.this,R.string.login_no_empty_tip_verify,
                                Toast.LENGTH_SHORT).show();
                    }else {
                        verifyCodeLogin();
                    }

                }
                
                
            }
        });

        //忘记密码
        tv_forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Contant.TYPE,Contant.TYPE_RESET_PASSWORD);
                goToActivity(InputPhoneActivity.class,bundle);
            }
        });

        iv_see_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pwdEditText.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    //设置密码不可见
                    pwdEditText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                    iv_see_pwd.setImageResource(R.mipmap.login_icon_closedeyes_nor);
                } else {
                    //设置密码可见
                    pwdEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    iv_see_pwd.setImageResource(R.mipmap.login_icon_eye_nor);
                }
                pwdEditText.setSelection(pwdEditText.getText().length());
            }
        });

        tv_login_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mLoginType == PASSWORD_LOGIN){
                    mLoginType = VERIFY_CODE_LOGIN;
                    tv_login_type.setText(R.string.password_login);
                    tv_get_verify_code.setVisibility(View.VISIBLE);
                    ll_forget_password.setVisibility(View.GONE);
                    pwdEditText.setHint(R.string.input_check_code_hint);
                    //设置验证码可见并且是只能输入数字
                    pwdEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    Drawable drawableLeftVerify = getResources().getDrawable(
                            R.mipmap.login_icon_verification_nor);
                    pwdEditText.setCompoundDrawablesWithIntrinsicBounds(drawableLeftVerify,null,null,null);
                    phoneEditText.setCompoundDrawables(null,null,null,null);
                    tv_area.setText("+" + mAreaCode);
                    rl_area.setVisibility(View.VISIBLE);
                }else if(mLoginType == VERIFY_CODE_LOGIN){
                    mLoginType = PASSWORD_LOGIN;
                    tv_login_type.setText(R.string.verify_code_login);
                    tv_get_verify_code.setVisibility(View.GONE);
                    ll_forget_password.setVisibility(View.VISIBLE);
                    //设置密码不可见
                    pwdEditText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                    iv_see_pwd.setImageResource(R.mipmap.login_icon_closedeyes_nor);
                    Drawable drawableLeftPassword = getResources().getDrawable(
                            R.mipmap.login_icon_password_nor);
                    pwdEditText.setHint(R.string.login_pwd_h);
                    pwdEditText.setCompoundDrawablesWithIntrinsicBounds(drawableLeftPassword,null,null,null);
                    Drawable drawableLeft = getResources().getDrawable(
                            R.mipmap.login_icon_user_nor);
                    phoneEditText.setCompoundDrawablesWithIntrinsicBounds(drawableLeft,null,null,null);
                    rl_area.setVisibility(View.GONE);
                }
                pwdEditText.setText("");
                mPassword = "";
                mVerifyCode = "";
            }
        });

        tv_get_verify_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPhone = phoneEditText.getText().toString().trim();
                if(!TextUtils.isEmpty(mPhone)){
                    requestLoginVerifyCode();
                }else {
                    Toast.makeText(LoginActivity.this,R.string.phone_empty_tip
                            ,Toast.LENGTH_SHORT).show();
                }
            }
        });

        tv_area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOptionsPickerView == null) {
                    mOptionsPickerView = new OptionsPickerBuilder(LoginActivity.this, new OnOptionsSelectListener() {
                        @Override
                        public void onOptionsSelect(int options1, int options2, int options3, View v) {
                            mAreaCode = AREA_CODES[options1];
                            tv_area.setText("+" + mAreaCode);
                        }
                    })
                            .setLayoutRes(R.layout.picker_view_phone_area_code, new CustomListener() {
                                @Override
                                public void customLayout(View v) {
                                    TextView tv_cancel = v.findViewById(R.id.tv_cancel);
                                    TextView tv_confirm = v.findViewById(R.id.tv_confirm);
                                    tv_confirm.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mOptionsPickerView.returnData();
                                            mOptionsPickerView.dismiss();
                                        }
                                    });
                                    tv_cancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mOptionsPickerView.dismiss();
                                        }
                                    });
                                }
                            })
                            .isDialog(false)
                            .setOutSideCancelable(false)
                            .build();
                    setPickViewData();
                    mOptionsPickerView.setPicker(mPickerViewData);
                }
                mOptionsPickerView.show();
            }
        });

        phoneEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setLoginBtnEnable();
            }
        });

        pwdEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setLoginBtnEnable();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setLoginBtnEnable(){
        if(phoneEditText.getText().toString().trim().length()>0
                &&pwdEditText.getText().toString().trim().length()>0){
            loginbtn.setEnabled(true);
        }else {
            loginbtn.setEnabled(false);
        }
    }

    /**
     * 请求登录验证码
     */
    private void requestLoginVerifyCode() {
        DialogUtils.createTipAllLoadDialog(LoginActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CONNECT_GET_SMS_CAPTCHA())
                .addParam("phone", mPhone)
                .addParam("type", Contant.TYPE_lOGIN)
                .addParam("area_code", mAreaCode)
                .withPOST(new NetCallBack<GetSmsCaptcha1Bean>() {

                    @NotNull
                    @Override
                    public Class<GetSmsCaptcha1Bean> getRealType() {
                        return GetSmsCaptcha1Bean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(LoginActivity.this,err,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull GetSmsCaptcha1Bean getSmsCaptcha1Bean) {
                        if(isFinishing()){
                            return;
                        }
                        isRequestedVerifyCode = true;
                        Toast.makeText(getApplicationContext(),R.string.verify_code_sended,Toast.LENGTH_SHORT).show();
                        DialogUtils.moven();
                        mCountDown = 60;
                        mHandler.sendEmptyMessageDelayed(1,1000);
                        if(getSmsCaptcha1Bean.getType() != 0){
                            mVerifyCodeType = getSmsCaptcha1Bean.getType();
                        }

                    }
                }, false);
    }

    /**
     * 密码登录
     */
    private void passwordLogin() {
        DialogUtils.createTipAllLoadDialog(LoginActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGIN())
                .addParam("username", mPhone)
                .addParam("password", mPassword)
                .addParam("client", "android")
                .addParam("l_l_p_type", Utils.isHUAWEI()?"1":"")
                .addParam("l_l_p_token", Login.Companion.getInstance().getHuawei_push_token())
                .withPOST(new NetCallBack<LoginBean>() {

                    @NotNull
                    @Override
                    public Class<LoginBean> getRealType() {
                        return LoginBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(LoginActivity.this,err,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull LoginBean loginBean) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        if(TextUtils.isEmpty(loginBean.getToken())){
                           //token为空的时候需要去安全验证
                           Bundle bundle = new Bundle();
                           bundle.putString(Contant.PHONE,mPhone);
                           bundle.putString(Contant.AREA_CODE,loginBean.getMobile_zone());
                           bundle.putString(Contant.PASSWORD,mPassword);
                           goToActivity(StartVerifyActivity.class,bundle);
                        }else {
                            LoginUtil.loginSaveDataAndInit(getApplicationContext(),loginBean,mPhone);
                            finish();
                        }
                    }
                }, false);
    }


    /**
     * 短信登录
     */
    private void verifyCodeLogin(){
        DialogUtils.createTipAllLoadDialog(LoginActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGIN())
                .addParam("username", mPhone)
                .addParam("client", "android")
                .addParam("area_code", mAreaCode)
                .addParam("key_code", mVerifyCode)
                .addParam("type", mVerifyCodeType+"")
                .addParam("l_l_p_type", Utils.isHUAWEI()?"1":"")
                .addParam("l_l_p_token", Login.Companion.getInstance().getHuawei_push_token())
                .withPOST(new NetCallBack<LoginBean>() {

                    @NotNull
                    @Override
                    public Class<LoginBean> getRealType() {
                        return LoginBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(LoginActivity.this,err,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull LoginBean loginBean) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        LoginUtil.loginSaveDataAndInit(getApplicationContext(),loginBean,mPhone);
                        finish();
                    }
                }, false);
    }

    private void setPickViewData() {
        if (mAreaDisplayList == null) {
            mAreaDisplayList = new ArrayList<>();
            mAreaDisplayList.add(getResources().getString(R.string.cn) + AREA_CODES[0]);
            mAreaDisplayList.add(getResources().getString(R.string.cn_macau) + AREA_CODES[1]);
            mAreaDisplayList.add(getResources().getString(R.string.cn_hk) + AREA_CODES[2]);
        }
        if (mPickerViewData == null) {
            mPickerViewData = new ArrayList<>();
            for (int i = 0; i < AREA_CODES.length; i++) {
                mPickerViewData.add(new AreaCodeBean(mAreaDisplayList.get(i)));
            }
        }

    }

    @Override
    protected void onDestroy() {
        if(mHandler.hasMessages(1)){
            mHandler.removeMessages(1);
        }
        mHandler = null;
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isStop = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isStop = false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.EVENT_ClOSE_LOGINACTIVITY)){
            if(isStop){
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        setResult(MainActivity.RESULT_OK);
        super.onBackPressed();
    }

}
