package com.xht.kuaiyouyi.ui.goods;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.goods.adapter.GoodsCaseAdapter;
import com.xht.kuaiyouyi.ui.goods.entity.GoodsData;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 商品--案例
 */
public class Fragment_case extends BaseFragment {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.textView)
    TextView textView;

    public static int size;


    private List<GoodsData.GoodsBean.GoodsCaseBean> caselist=new ArrayList<>();
    private GoodsCaseAdapter goodsCaseAdapter;
    private String goods_id;
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            imageView.setVisibility(View.VISIBLE);
            textView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    };
    public static Fragment_case newInstance(String goodsid){
        Fragment_case fragment = new Fragment_case();
        Bundle bundle=new Bundle();
        bundle.putString("goods_id",goodsid);
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        goods_id=getArguments().getString("goods_id");
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        goodsCaseAdapter=new GoodsCaseAdapter(getActivity(),R.layout.item_fragment_case_recycler,caselist);
        recyclerView.setAdapter(goodsCaseAdapter);
        getData();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_goodscase_layout;
    }

    private void getData(){
        DialogUtils.createTipAllLoadDialog(getActivity());
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_GOODSDETAILS())
                .addParam("goods_id", goods_id)
                .withPOST(new NetCallBack<GoodsData>() {

                    @NotNull
                    @Override
                    public Class<GoodsData> getRealType() {
                        return GoodsData.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        DialogUtils.moven();
                    }

                    @Override
                    public void onSuccess(@NonNull GoodsData goodsData) {
                        if(getActivity()==null){
                            return;
                        }
                        DialogUtils.moven();
                        if(goodsData.getGoods().getGoods_case().size()!=0){
                            for(int i=0;i<goodsData.getGoods().getGoods_case().size();i++){
                                caselist.add(goodsData.getGoods().getGoods_case().get(i));
                            }
                            recyclerView.setVisibility(View.VISIBLE);
                        }
                        size=caselist.size();
                        goodsCaseAdapter.notifyDataSetChanged();
                        if(caselist.size()==0){
                            handler.sendEmptyMessage(1);
                        }
                    }
                }, false);
    }

}
