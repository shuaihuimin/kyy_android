package com.xht.kuaiyouyi.ui.goods;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.classic.common.MultipleStatusView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.adapter.StoreAdapter;
import com.xht.kuaiyouyi.ui.goods.entity.StoregoodsBean;
import com.xht.kuaiyouyi.utils.GlideImageLoader;
import com.xht.kuaiyouyi.utils.ProportionscreenUtils;
import com.xht.kuaiyouyi.utils.Utils;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class Fragment_storehome extends BaseFragment {
    @BindView(R.id.mRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.multipleStatusView)
    MultipleStatusView multipleStatusView;
    @BindView(R.id.recycler_store)
    RecyclerView recyclerView;
    StoreAdapter adapter;
    private List<StoregoodsBean.ListBean> list=new ArrayList<>();
    private String store_id;
    private boolean isrefresh;
    private String kyy="";
    private List<String> lunbolist=new ArrayList<>();
    private List<StoregoodsBean.CurrencytypeBean> currlist=new ArrayList<>();
    private int img_height;
    private int state=0;
    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(list.size()==0){
                multipleStatusView.showEmpty();
            }
        }
    };

    public static Fragment_storehome newInstance(String storeid) {
        Bundle args = new Bundle();
        args.putString("store_id",storeid);
        Fragment_storehome fragment = new Fragment_storehome();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        store_id=getArguments().getString("store_id");
        img_height=(Utils.getWindowWidth(getActivity())-8)/2;
        adapter=new StoreAdapter(getActivity(),R.layout.home_recycler_item_layout,list,currlist,1,img_height);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        getinit(store_id);
        initlode();
        if(lunbolist.size()!=0){
            View bannerview=View.inflate(getActivity(),R.layout.store_recycler_head_layout,null);
            adapter.addHeaderView(bannerview,0);
            Banner banner=(Banner) bannerview.findViewById(R.id.banner);
            int screen=getActivity().getWindowManager().getDefaultDisplay().getWidth();
            float height= ProportionscreenUtils.getHeight(375,170,(int) screen);
            ViewGroup.LayoutParams params=banner.getLayoutParams();
            params.height=(int) height;
            banner.setLayoutParams(params);
            banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
            banner.setImageLoader(new GlideImageLoader());
            banner.setImages(lunbolist);
            //设置轮播时间
            banner.setDelayTime(3000);
            banner.setIndicatorGravity(BannerConfig.CENTER);
            banner.start();
        }
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(state!=0){
                    if (dy < 0) {
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.STORE_DOWN));
                    } else if (dy > 0) {
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.STORE_UP));
                    }
                }else {
                    state=1;
                    EventBus.getDefault().post(new MessageEvent(MessageEvent.STORE_DOWN));
                }
            }
        });
        multipleStatusView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getinit(store_id);
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_newstores_layout;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        EventBus.getDefault().post(new MessageEvent(MessageEvent.STORE_DOWN));
    }

    private void getinit(String store_id){
        if(kyy.equals("")){
            multipleStatusView.showLoading();
        }
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_STORE_ALLGOODS())
                .addParam("store_id",store_id)
                .addParam("store_type","1")
                .addParam("order","0")
                .addParam("key","1")
                .withLoadPOST(new NetCallBack<StoregoodsBean>() {

                    @NotNull
                    @Override
                    public Class<StoregoodsBean> getRealType() {
                        return StoregoodsBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        if(TextUtils.isEmpty(kyy)){
                            multipleStatusView.showError();
                        }else {
                            Toast.makeText(getContext(), getString(R.string.not_network), Toast.LENGTH_SHORT).show();
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull StoregoodsBean storegoodsBean) {
                        if(getActivity()==null){
                            return;
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                        multipleStatusView.showContent();
                        currlist.add(storegoodsBean.getCurrencytype());
                        if(storegoodsBean.getList()!=null && storegoodsBean.getList().size()!=0){
                            for(int i=0;i<storegoodsBean.getList().size();i++){
                                list.add(storegoodsBean.getList().get(i));
                            }
                        }else {
                            if(!TextUtils.isEmpty(kyy)){
                                smartRefreshLayout.finishLoadMoreWithNoMoreData();
                                smartRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
                            }
                        }
                        kyy=storegoodsBean.getKyy();
                        adapter.notifyDataSetChanged();
                        handler.sendEmptyMessage(0);
                    }
                },false,kyy);
    }

    private void initlode(){
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                smartRefreshLayout.setNoMoreData(false) ;
                getinit(store_id);
            }
        });

    }
}
