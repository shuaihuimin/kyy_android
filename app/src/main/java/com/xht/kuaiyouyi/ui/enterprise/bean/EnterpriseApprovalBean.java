package com.xht.kuaiyouyi.ui.enterprise.bean;

import java.util.List;

public class EnterpriseApprovalBean {


    /**
     * company_id : 90
     * member_infoId : 168
     * result : [{"member_truename":"花","expect_delivery_time":"1534262400","start_time":"1534148474","status":"3","apply_reason":"订单","status_name":"审批拒绝","approve_order_id":"475"},{"member_truename":"花","expect_delivery_time":"1533916800","start_time":"1534144981","status":"0","apply_reason":"111","status_name":"审批中","approve_order_id":"474"},{"member_truename":"花","expect_delivery_time":"1533916800","start_time":"1533888525","status":"0","apply_reason":"第三方控件的是JFK的时间疯狂的世界科技的水库附近的时刻加分的考生的身份的身份 ","status_name":"审批中","approve_order_id":"473"},{"member_truename":"花","expect_delivery_time":"1533657600","start_time":"1533717100","status":"2","apply_reason":"22","status_name":"审批通过","approve_order_id":"470"},{"member_truename":"花","expect_delivery_time":"1533657600","start_time":"1533708566","status":"2","apply_reason":"大","status_name":"审批通过","approve_order_id":"469"},{"member_truename":"花","expect_delivery_time":"1533657600","start_time":"1533700964","status":"2","apply_reason":"的说法","status_name":"审批通过","approve_order_id":"468"},{"member_truename":"花","expect_delivery_time":"1534867200","start_time":"1533629063","status":"0","apply_reason":"订单","status_name":"审批中","approve_order_id":"462"},{"member_truename":"花","expect_delivery_time":"1534867200","start_time":"1533628652","status":"0","apply_reason":"发放","status_name":"审批中","approve_order_id":"460"},{"member_truename":"花","expect_delivery_time":"1534780800","start_time":"1533628144","status":"0","apply_reason":"辅导辅导费","status_name":"审批中","approve_order_id":"458"},{"member_truename":"花","expect_delivery_time":"1533657600","start_time":"1533624577","status":"0","apply_reason":"的疯狂的事李开复看到了顺丰快递是开发了独守空房立刻大师傅的历史 地方大师傅独守空房但是JFK角度思考房价的快速减肥肯定是JFK都是进口发动机看","status_name":"审批中","approve_order_id":"456"}]
     * kyy : eyJQIjoxLCJLIjoia3l5XzAyNiIsIk8iOiIzLTkwIn0=
     */

    private int company_id;
    private String member_infoId;
    private String kyy;
    private List<ResultBean> result;

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getMember_infoId() {
        return member_infoId;
    }

    public void setMember_infoId(String member_infoId) {
        this.member_infoId = member_infoId;
    }

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * member_truename : 花
         * expect_delivery_time : 1534262400
         * start_time : 1534148474
         * status : 3
         * apply_reason : 订单
         * status_name : 审批拒绝
         * approve_order_id : 475
         */

        private String member_truename;
        private String expect_delivery_time;
        private String start_time;
        private int status;
        private String apply_reason;
        private String status_name;
        private long approve_order_id;
        private int is_read=1;

        public String getMember_truename() {
            return member_truename;
        }

        public void setMember_truename(String member_truename) {
            this.member_truename = member_truename;
        }

        public String getExpect_delivery_time() {
            return expect_delivery_time;
        }

        public void setExpect_delivery_time(String expect_delivery_time) {
            this.expect_delivery_time = expect_delivery_time;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getApply_reason() {
            return apply_reason;
        }

        public void setApply_reason(String apply_reason) {
            this.apply_reason = apply_reason;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public long getApprove_order_id() {
            return approve_order_id;
        }

        public void setApprove_order_id(long approve_order_id) {
            this.approve_order_id = approve_order_id;
        }

        public int isIs_read() {
            return is_read;
        }

        public void setIs_read(int is_read) {
            this.is_read = is_read;
        }
    }
}
