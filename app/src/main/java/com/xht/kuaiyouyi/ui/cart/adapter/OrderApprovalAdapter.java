package com.xht.kuaiyouyi.ui.cart.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.cart.entity.OrderApprovalBean;
import com.xht.kuaiyouyi.utils.Utils;

import java.util.List;

public class OrderApprovalAdapter extends BaseQuickAdapter<OrderApprovalBean.GoodsListBean.OrderListBean,BaseViewHolder> {
    private Context mContext;
    private List<OrderApprovalBean.GoodsListBean.OrderListBean> list;
    public OrderApprovalAdapter(Context context,int layoutResId, @Nullable List<OrderApprovalBean.GoodsListBean.OrderListBean> data) {
        super(layoutResId, data);
        this.mContext=context;
        this.list=data;
    }

    public OrderApprovalAdapter(@Nullable List<OrderApprovalBean.GoodsListBean.OrderListBean> data) {
        super(data);
    }

    public OrderApprovalAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, OrderApprovalBean.GoodsListBean.OrderListBean item) {
        Glide.with(mContext).load(item.getImage_60_url())
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                .into((ImageView) helper.getView(R.id.iv_goods_img));
        if(item.getIs_second_hand()==1){
            helper.setText(R.id.tv_goods_name,Utils.secondLabel(item.getGoods_name()));
        }else {
            helper.setText(R.id.tv_goods_name,item.getGoods_name());
        }
//        helper.setText(R.id.tv_goods_name,item.getGoods_name())
        helper.setText(R.id.tv_goods_price,"¥"+Utils.getDisplayMoney(Double.parseDouble(item.getGoods_price())))
                .setText(R.id.tv_goods_num,"x"+item.getGoods_num())
                .setText(R.id.tv_small_total,"¥"+Utils.getDisplayMoney(Double.parseDouble(item.getGoods_pay_price())))
                .setText(R.id.tv_goods_model,item.getGoods_spec())
                .setText(R.id.tv_goods_freight,"¥"+Utils.getDisplayMoney(Double.parseDouble(item.getFreight_fee())));
        if(item.getCustoms_charges_fee().equals("0")){
            helper.getView(R.id.tv_test_fee_title).setVisibility(View.GONE);
            helper.getView(R.id.tv_test_fee).setVisibility(View.GONE);
        }else {
            helper.getView(R.id.tv_test_fee_title).setVisibility(View.VISIBLE);
            helper.getView(R.id.tv_test_fee).setVisibility(View.VISIBLE);
            helper.setText(R.id.tv_test_fee,"¥"+Utils.getDisplayMoney(Double.parseDouble(item.getCustoms_charges_fee())));
        }

        if(helper.getPosition()==list.size()-1){
            helper.getView(R.id.v_line).setVisibility(View.GONE);
        }else {
            helper.getView(R.id.v_line).setVisibility(View.VISIBLE);
        }

    }
}
