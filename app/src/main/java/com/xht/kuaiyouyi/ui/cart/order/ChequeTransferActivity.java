package com.xht.kuaiyouyi.ui.cart.order;

import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.model.TResult;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.cart.entity.OrderDetilsBean;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.SignUtil;
import com.xht.kuaiyouyi.utils.SystemUtil;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.ButtomDialog;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChequeTransferActivity extends TakePhotoActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_click_updata)
    TextView tv_click_updata;
    @BindView(R.id.tv_pay_currency)
    TextView tv_pay_currency;
    @BindView(R.id.tv_transfer_amount)
    TextView tv_transfer_amount;
    @BindView(R.id.tv_transfer_amount_state)
    TextView tv_transfer_amount_state;
    @BindView(R.id.tv_check_make_invoice)
    TextView tv_check_make_invoice;
    @BindView(R.id.iv_check_pictures)
    ImageView iv_check_pictures;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    private ButtomDialog mButtomDialog;
    private Uri mImageUri;
    private String uploadIconPath;
    private String transfer_img;
    private OrderDetilsBean orderDetilsBean;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(getLayout());
        ButterKnife.bind(this);
        initView();
    }

    @Override
    public void takeCancel() {
        super.takeCancel();
    }

    @Override
    public void takeFail(TResult result, String msg) {
        super.takeFail(result, msg);
    }

    @Override
    public void takeSuccess(TResult result) {
        super.takeSuccess(result);
        uploadIconPath = result.getImage().getOriginalPath();
        requestUploadIcon(uploadIconPath);
        Glide.with(ChequeTransferActivity.this).load(uploadIconPath).into(iv_check_pictures);
        tv_click_updata.setVisibility(View.GONE);
    }


    protected int getLayout() {
        return R.layout.activity_chequetransfer;
    }


    protected void initView() {
        tv_title.setText(getString(R.string.upload_cheque));
        orderDetilsBean= JSONObject.parseObject(getIntent().getStringExtra("order_detils"),OrderDetilsBean.class);
        tv_pay_currency.setText(orderDetilsBean.getGoods().getOrder_main_pay_currency_name());
        if(!TextUtils.isEmpty(orderDetilsBean.getOther().getCheck_head_name())){
            tv_check_make_invoice.setText(orderDetilsBean.getOther().getCheck_head_name());
        }
        if(orderDetilsBean.getGoods().getOrder_main_deal_active().equals("1")){
            tv_transfer_amount.setText(orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                    + Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_amount())*
                    Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate())));
        }else if(orderDetilsBean.getGoods().getOrder_main_deal_active().equals("2")){
            if(orderDetilsBean.getGoods().getOrder_main_state().equals("2010")){
                tv_transfer_amount.setText(orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                        + Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_one())*
                        Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate())));
                tv_transfer_amount_state.setText(getString(R.string.frist_phase));
            }else if(orderDetilsBean.getGoods().getOrder_main_state().equals("2011")){
                tv_transfer_amount.setText(orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                        + Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_two())*
                        Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate())));
                tv_transfer_amount_state.setText(getString(R.string.second_phase));
            }
        }
        iv_check_pictures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showButtonDialog();
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(transfer_img)){
                    DialogUtils.createTwoBtnDialog(ChequeTransferActivity.this,
                            getString(R.string.check_dialog),
                            getString(R.string.dialog_confirm),
                            getString(R.string.dialog_cancel),
                            new DialogUtils.OnRightBtnListener() {
                                @Override
                                public void setOnRightListener(Dialog dialog) {
                                    ChequeTransferActivity.this.finish();
                                }
                            },null,false,true);
                }else {
                    ChequeTransferActivity.this.finish();
                }
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(uploadIconPath)){
                    ToastU.INSTANCE.showToast(getString(R.string.check_dailog));
                }else{
                    uploadCheque(transfer_img);
                }
            }
        });

    }

    private void showButtonDialog() {
        if(mButtomDialog == null){
            View view = LayoutInflater.from(this).inflate(R.layout.dialog_buttom_three_btn,null);
            mButtomDialog = new ButtomDialog(this,view,false,true);
            final TextView tv_three = view.findViewById(R.id.tv_three);
            TextView tv_one = view.findViewById(R.id.tv_one);
            TextView tv_two = view.findViewById(R.id.tv_two);

            tv_three.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                }
            });
            tv_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),  System.currentTimeMillis() + ".jpg");
                    mImageUri = Uri.fromFile(file);
                    getTakePhoto().onPickFromCapture(mImageUri);
                    mButtomDialog.dismiss();
                }
            });
            tv_two.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),  System.currentTimeMillis() + ".jpg");
                    mImageUri = Uri.fromFile(file);
                    getTakePhoto().onPickFromGallery();
                    mButtomDialog.dismiss();
                }
            });
        }
        mButtomDialog.show();

    }

    //上传图片
    private void requestUploadIcon(final String uploadIconPath) {
        DialogUtils.createTipAllLoadDialog(this);
        String t = SystemUtil.INSTANCE.getTime(SystemUtil.INSTANCE.
                getTimeStr("" + System.currentTimeMillis()));
        Map<String,Object> map = new HashMap<>();
        map.put("device_uuid", NetUtil.Companion.getUuid());
        map.put("device_name", NetUtil.Companion.getDevice_name());
        map.put("type", "2");
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_UPLOADIMG())
                .addParam("device_uuid", NetUtil.Companion.getUuid())
                .addParam("device_name", NetUtil.Companion.getDevice_name())
                .addParam("token_id", Login.Companion.getInstance().getToken_id())
                .addParam("sign_time", t)
                .addParam("sign", SignUtil.sign(map, t))
                .addParam("type", "2")
                .addParam(new File(uploadIconPath).getName(),new File(uploadIconPath))
                .withPOSTFile(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onSuccess(@NonNull String uploadImageBean) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        try {
                            org.json.JSONObject jsonObject=new org.json.JSONObject(uploadImageBean);
                            transfer_img = jsonObject.getString("file_name_short");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(ChequeTransferActivity.this,err,Toast.LENGTH_SHORT).show();
                    }
                },false);
    }

    private void uploadCheque(final String transfer_img){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_UPDATE_CHEQUE())
                .addParam("pay_type","2")
                .addParam("order_main_id",getIntent().getStringExtra("order_id"))
                .addParam("order_main_sn",orderDetilsBean.getGoods().getOrder_main_sn())
                .addParam("transfer_img",transfer_img)
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        DialogUtils.createOneBtnDialog(ChequeTransferActivity.this, getString(R.string.check_successful)
                                , getString(R.string.know), new DialogUtils.OnLeftBtnListener() {
                                    @Override
                                    public void setOnLeftListener(Dialog dialog) {
                                        EventBus.getDefault().post(new MessageEvent(MessageEvent.ORDERDETILS));
                                        EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_ORDER_LIST));
                                        EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_APPROVAL_DETAIL));
                                        ChequeTransferActivity.this.finish();
                                    }
                                },false,false);
                    }
                },false);
    }

    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config=new Configuration();
        config.setToDefaults();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocales(res.getConfiguration().getLocales());
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(res.getConfiguration().locale);
        }else {
            config.locale = res.getConfiguration().locale;
        }
        res.updateConfiguration(config,res.getDisplayMetrics() );
        return res;
    }

}
