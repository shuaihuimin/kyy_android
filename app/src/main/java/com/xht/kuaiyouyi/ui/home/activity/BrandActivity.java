package com.xht.kuaiyouyi.ui.home.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.home.adapter.BrandListAdapter;
import com.xht.kuaiyouyi.ui.home.entity.BrandListBean;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.widget.SideBar;
import com.xht.kuaiyouyi.widget.SideBarUtil;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 品牌馆
 */
public class BrandActivity extends BaseActivity {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.lv_brand)
    ListView lv_brand;
    @BindView(R.id.sb_index)
    SideBar sb_index;
    @BindView(R.id.tv_display_select)
    TextView tv_display_select;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;

    private BrandListBean mBrandListBean;
    private List<BrandListBean.BrandBean.DataBean> mDatas;//数据源
    private BrandListAdapter mAdapter;

    @Override
    protected int getLayout() {
        return R.layout.activity_brand;
    }

    @Override
    protected void initView() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_title.setText(R.string.brand);
        lv_brand.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle=new Bundle();
                bundle.putString("brand_id",mDatas.get(position).getBrand_id());
                goToActivity(BrandDetilsActivity.class,bundle);
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestBrand();
            }
        });
        requestBrand();

    }

    private void requestBrand() {
        DialogUtils.createTipAllLoadDialog(BrandActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_BRAND_INDEX())
                .addParam("brand_class_id","0")
                .withPOST(new NetCallBack<BrandListBean>() {
                    @NotNull
                    @Override
                    public Class<BrandListBean> getRealType() {
                        return BrandListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                       // ToastU.INSTANCE.showToast( getString(R.string.not_network));
                        error_retry_view.setVisibility(View.VISIBLE);
                        DialogUtils.moven();
                    }

                    @Override
                    public void onSuccess(@NonNull BrandListBean brandListBean) {
                        if(isFinishing()){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        DialogUtils.moven();
                       if(brandListBean.getBrand()!=null&&brandListBean.getBrand().size()>0){
                           mDatas = new ArrayList<>();
                           for (BrandListBean.BrandBean brandBean:brandListBean.getBrand()) {
                               mDatas.addAll(brandBean.getData());
                           }
                           mAdapter = new BrandListAdapter(BrandActivity.this,mDatas);
                           lv_brand.setAdapter(mAdapter);
                           sb_index.setLetter(SideBarUtil.getLetters(mDatas));
                           sb_index.setTextView(tv_display_select);
                           sb_index.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {
                               @Override
                               public void onTouchingLetterChanged(String s) {
                                   int position = SideBarUtil.getLetterPosition(mDatas, s);
                                   if (position != -1) {
                                       lv_brand.setSelection(position);
                                   }
                               }
                           });
                       }
                    }
                }, false);
    }


}
