package com.xht.kuaiyouyi.ui.cart.order;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.cart.adapter.OrderGoodsAdapter;
import com.xht.kuaiyouyi.ui.cart.entity.ConfirmOrderBean;
import com.xht.kuaiyouyi.utils.CommonPopupWindow;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.CustomExpandableListView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import butterknife.BindView;

public class ConfirmOrderActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_confirmorder_name)
    TextView tv_confirmorder_name;
    @BindView(R.id.tv_confirmorder_phone)
    TextView tv_confirmorder_phone;
    @BindView(R.id.tv_confirmorder_addres)
    TextView tv_confirmorder_addres;
    @BindView(R.id.relative_confirmorder_addres)
    RelativeLayout relative_confirmorder_addres;
    @BindView(R.id.customExpandableListView)
    CustomExpandableListView customExpandableListView;
    @BindView(R.id.relative_pay_way_order)
    RelativeLayout relative_pay_way_order;
    @BindView(R.id.relative_trading_way)
    RelativeLayout relative_trading_way;
    @BindView(R.id.btn_submit_order)
    Button btn_submit_order;
    @BindView(R.id.tv_total_price)
    TextView tv_total_price;
    @BindView(R.id.tv_amount_payable)
    TextView tv_amount_payable;
    @BindView(R.id.tv_total_dollar)
    TextView tv_total_dollar;
    @BindView(R.id.tv_zhifu_way)
    TextView tv_zhifu_way;
    @BindView(R.id.tv_trading_way)
    TextView tv_trading_way;
    @BindView(R.id.relative_payment_show)
    RelativeLayout relative_payment_show;
    @BindView(R.id.relative_bank)
    RelativeLayout relative_bank;
    @BindView(R.id.relative_cheak)
    RelativeLayout relative_cheak;
    @BindView(R.id.tv_bank_area)
    TextView tv_bank_area;
    @BindView(R.id.tv_order_out_bank)
    TextView tv_order_out_bank;
    @BindView(R.id.tv_bank_currency)
    TextView tv_bank_currency;
    @BindView(R.id.tv_cheak_address)
    TextView tv_cheak_address;
    @BindView(R.id.tv_cheak_currency)
    TextView tv_cheak_currency;
    @BindView(R.id.relative_down_payment)
    RelativeLayout relative_down_payment;
    @BindView(R.id.relative_phase_payment)
    RelativeLayout relative_phase_payment;
    @BindView(R.id.tv_freight_always)
    TextView tv_freight_always;
    @BindView(R.id.tv_total_amount_goods)
    TextView tv_total_amount_goods;
    @BindView(R.id.tv_total_amount_order)
    TextView tv_total_amount_order;
    @BindView(R.id.tv_order_down_payment)
    TextView tv_order_down_payment;
    @BindView(R.id.tv_order_tail_section)
    TextView tv_order_tail_section;
    @BindView(R.id.tv_order_added_new)
    TextView tv_order_added_new;
    @BindView(R.id.tv_added_new)
    TextView tv_added_new;
    @BindView(R.id.tv_first_phase_payment)
    TextView tv_first_phase_payment;
    @BindView(R.id.tv_order_first_phase_payment)
    TextView tv_order_first_phase_payment;
    @BindView(R.id.tv_order_second_phase_payment)
    TextView tv_order_second_phase_payment;

    //交易货币标识
    private String pay_currency="RMB";
    //支票支付取支票地址id
    private String address_check_id;
    //支付方式
    private String pay_active;
    //银行地区id
    private String bank_area;
    //银行名称id
    private String bank_item;
    //银行币别id
    private String bank_currency;
    //支票币别
    private String pay_check;
    //收货地址id
    private String address_id;
    //交易方式
    private String deal_active;
    //购买方式：个人、企业id
    private int buy_company_id;
    //购买者id
    private String buy_member_id;
    //商品id|商品数量
    private String cart_str;
    //记录支付方式状态
    private int state=1;
    private CommonPopupWindow popuptrading;
    private String order;
    public final static int REQUEST_CODE = 1000;
    private ConfirmOrderBean confirmOrderBean=new ConfirmOrderBean();
    private ConfirmOrderBean.DataListBean dataListBean;
    private OrderGoodsAdapter orderGoodsAdapter;
    private Gson gson=new Gson();
    private double currency;

    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:
                    tv_confirmorder_name.setText(dataListBean.getAddress_list().getTrue_name());
                    tv_confirmorder_phone.setText(dataListBean.getAddress_list().getMob_phone());
                    tv_confirmorder_addres.setText(dataListBean.getAddress_list().getArea_info()+dataListBean.getAddress_list().getAddress());

                    tv_freight_always.setText("¥"+ Utils.getDisplayMoney(dataListBean.getFreight_price()));
                    tv_total_amount_goods.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(dataListBean.getGoods_price())));
                    tv_total_amount_order.setText("¥"+Utils.getDisplayMoney(dataListBean.getAll_price()));
                    buy_company_id=dataListBean.getBuy_company_id();
                    if(buy_company_id!=0){
                        //第一阶段第二阶段款项
                        tv_order_first_phase_payment.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(dataListBean.getOrder_main_pay_first())));
                        tv_order_second_phase_payment.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(dataListBean.getOrder_main_pay_end())));
                    }
                    //应付总金额
                    setTotalAmount(dataListBean.getAll_price());
            }
        }
    };
    @Override
    protected int getLayout() {
        return R.layout.activity_confirmorder;
    }

    @Override
    protected void initView() {
        tv_title.setText(getString(R.string.confirm_order));
        order=getIntent().getExtras().getString("order_content");
        confirmOrderBean=gson.fromJson(order, ConfirmOrderBean.class);
        dataListBean=confirmOrderBean.getData_list();

        //获取所有商品id和数量的拼接字符串
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<dataListBean.getCart_id().size();i++){
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append(dataListBean.getCart_id().get(i));
        }
        cart_str=sb.toString();

        //默认地址
        if(dataListBean.getAddress_list()!=null){
            tv_confirmorder_name.setText(dataListBean.getAddress_list().getTrue_name());
            tv_confirmorder_phone.setText(dataListBean.getAddress_list().getMob_phone());
            tv_confirmorder_addres.setText(dataListBean.getAddress_list().getArea_info()+dataListBean.getAddress_list().getAddress());
            address_id=dataListBean.getAddress_list().getAddress_id();
        }
        //商品清单
        orderGoodsAdapter=new OrderGoodsAdapter(ConfirmOrderActivity.this,dataListBean);
        customExpandableListView.setAdapter(orderGoodsAdapter);
        for (int i = 0; i < dataListBean.getStore_list().size(); i++) {
            customExpandableListView.expandGroup(i); //关键步骤4:初始化，将ExpandableListView以展开的方式显示
        }

        buy_member_id=confirmOrderBean.getData_list().getBuy_member_id();
        //根据公司id判断有没有交易方式
        buy_company_id=dataListBean.getBuy_company_id();
        if(buy_company_id!=0){
            if(Double.parseDouble(confirmOrderBean.getData_list().getCompany_count_money())>0){
                relative_trading_way.setVisibility(View.VISIBLE);
            }else {
                deal_active="1";
            }
            if(confirmOrderBean.getData_list().getIs_enabled_approve().equals("1")){
                btn_submit_order.setText(getString(R.string.submit_approval));
            }else {
                btn_submit_order.setText(getString(R.string.submit_order));
            }
        }else{
            deal_active="1";
            btn_submit_order.setText(getString(R.string.submit_order));
        }
        //总金额(运费、订单、商品)
        tv_freight_always.setText("¥"+ Utils.getDisplayMoney(dataListBean.getFreight_price()));
        tv_total_amount_goods.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(dataListBean.getGoods_price())));
        tv_total_amount_order.setText("¥"+Utils.getDisplayMoney(dataListBean.getAll_price()));
        tv_total_price.setText("¥"+Utils.getDisplayMoney(dataListBean.getAll_price()));
        click();
    }

    private void click(){
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmOrderActivity.this.finish();
            }
        });
        btn_submit_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_submit_order.setClickable(false);
                submitOrders();
            }
        });
        customExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
        relative_confirmorder_addres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmOrderActivity.this, ChooseAddressActivity.class);
                intent.putExtra("type",ChooseAddressActivity.TYPE_RECEIVE);
                startActivityForResult(intent, 1);
            }
        });
        relative_pay_way_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmOrderActivity.this, ChoicePaymentActivity.class);
                intent.putExtra("blanklist",order);
                intent.putExtra("state",state);
                startActivityForResult(intent, 2);
            }
        });

        relative_trading_way.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = LayoutInflater.from(ConfirmOrderActivity.this).inflate(R.layout.popup_trading_way,null);
                if(popuptrading==null){
                    popuptrading = new CommonPopupWindow.Builder(ConfirmOrderActivity.this)
                            .setView(view)
                            .setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                            .setBackGroundLevel(0.5f)//取值范围0.0f-1.0f 值越小越暗
                            .create();
                    final ImageView iv_full_payment=(ImageView) view.findViewById(R.id.iv_full_payment);
                    final ImageView iv_credit_payment=view.findViewById(R.id.iv_credit_payment);
                    TextView tv_available_credit_nor=view.findViewById(R.id.tv_available_credit_nor);
                    TextView tv_available_credit=view.findViewById(R.id.tv_available_credit);
                    tv_available_credit.setText(getString(R.string.available_credit)+confirmOrderBean.getData_list().getCompany_remain_money());
                    if(Double.parseDouble(confirmOrderBean.getData_list().getCompany_remain_money())>0){
                        tv_available_credit_nor.setVisibility(View.GONE);
                    }
                    RelativeLayout relative_full_payment=view.findViewById(R.id.relative_full_payment);
                    relative_full_payment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deal_active="1";
                            iv_full_payment.setImageResource(R.mipmap.pop_icon_tick);
                            iv_credit_payment.setImageResource(R.color.transparent);
                            tv_trading_way.setText(getString(R.string.full_payment));
                            relative_down_payment.setVisibility(View.GONE);
                            relative_phase_payment.setVisibility(View.GONE);
                            setTotalAmount(dataListBean.getAll_price());
                            popuptrading.dismiss();
                        }
                    });
                    RelativeLayout relative_credit_payment=view.findViewById(R.id.relative_credit_payment);
                    relative_credit_payment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(Double.parseDouble(confirmOrderBean.getData_list().getCompany_remain_money())>0){
                                deal_active="2";
                                iv_full_payment.setImageResource(R.color.transparent);
                                iv_credit_payment.setImageResource(R.mipmap.pop_icon_tick);
                                tv_trading_way.setText(getString(R.string.credit_payment));
                                relative_down_payment.setVisibility(View.VISIBLE);
                                relative_phase_payment.setVisibility(View.VISIBLE);
                                //首付款尾款
                                tv_order_down_payment.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(dataListBean.getOrder_main_pay_one())));
                                tv_order_tail_section.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(dataListBean.getOrder_main_pay_two())));
                                tv_order_first_phase_payment.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(dataListBean.getOrder_main_pay_first())));
                                tv_order_second_phase_payment.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(dataListBean.getOrder_main_pay_end())));
                                //setTotalAmount(Double.parseDouble(dataListBean.getOrder_main_pay_first()));
                                popuptrading.dismiss();
                                if(Double.parseDouble(dataListBean.getSupplement_amount())>0){
                                    tv_first_phase_payment.setText(getString(R.string.first_phase_payment_new));
                                    tv_added_new.setVisibility(View.VISIBLE);
                                    tv_order_added_new.setVisibility(View.VISIBLE);
                                    tv_order_added_new.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(dataListBean.getSupplement_amount())));
                                    DialogUtils.createOneBtnDialog(ConfirmOrderActivity.this,getString(R.string.lack_credit),getString(R.string.know), new DialogUtils.OnLeftBtnListener() {
                                        @Override
                                        public void setOnLeftListener(Dialog dialog) {

                                        }
                                    },false,false);
                                }

                            }
                        }
                    });
                }
                Utils.SetWindBg(ConfirmOrderActivity.this);
                popuptrading.showAtLocation(view, Gravity.BOTTOM, 0, 0);
            }
        });

        customExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });

        tv_added_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.createOneBtnDialog(ConfirmOrderActivity.this, "（" + getString(R.string.order_line_credit) + "¥"
                        + confirmOrderBean.getData_list().getCompany_remain_money() + getString(R.string.order_insufficient_credit)
                        + "¥" + confirmOrderBean.getData_list().getOrder_main_pay_two() +
                        getString(R.string.order_supplement) + "¥" + confirmOrderBean.getData_list().getSupplement_amount() + "）", getString(R.string.know), new DialogUtils.OnLeftBtnListener() {
                    @Override
                    public void setOnLeftListener(Dialog dialog) {

                    }
                },false,false);
            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode==RESULT_OK){
            address_id=data.getStringExtra("address_id");
            confirmorder();

        }else if(requestCode==2 && resultCode==RESULT_OK){
            pay_active=data.getStringExtra("pay_active");
            state=Integer.parseInt(pay_active);
            if(pay_active.equals("1")){
                tv_zhifu_way.setText(getString(R.string.bank_transfer));
                relative_payment_show.setVisibility(View.VISIBLE);
                relative_bank.setVisibility(View.VISIBLE);
                relative_cheak.setVisibility(View.GONE);
                tv_bank_area.setText(data.getStringExtra("bank_area_name"));
                tv_order_out_bank.setText(data.getStringExtra("bank_item_name"));
                tv_bank_currency.setText(data.getStringExtra("bank_currency_name"));
                pay_currency=data.getStringExtra("pay_currency");

            }else if(pay_active.equals("2")){
                tv_zhifu_way.setText(getString(R.string.cheque_account));
                relative_payment_show.setVisibility(View.VISIBLE);
                relative_cheak.setVisibility(View.VISIBLE);
                relative_bank.setVisibility(View.GONE);
                tv_cheak_address.setText(data.getStringExtra("cheac_address"));
                tv_cheak_currency.setText(data.getStringExtra("check_currency"));
                pay_currency=data.getStringExtra("pay_currency");

            }else {
                //在线支付只能是人民币
                pay_currency= "CNY";
                tv_zhifu_way.setText(getString(R.string.online_transfer));
                relative_payment_show.setVisibility(View.GONE);
            }
            //应付总金额
            setTotalAmount(confirmOrderBean.getData_list().getAll_price());
            bank_area=data.getStringExtra("bank_area");
            bank_item=data.getStringExtra("bank_item");
            bank_currency=data.getStringExtra("bank_currency");
            pay_check=data.getStringExtra("pay_check");
            address_check_id=data.getStringExtra("cheac_address_id");

        }
    }

    //确认订单
    private void confirmorder(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CONFIRM_ORDER())
                .addParam("company_id",buy_company_id)
                .addParam("ifcart",dataListBean.getIfcart())
                .addParam("cart_str",cart_str)
                .addParam("address_id",address_id)
                .withPOST(new NetCallBack<ConfirmOrderBean>() {

                    @NotNull
                    @Override
                    public Class<ConfirmOrderBean> getRealType() {
                        return ConfirmOrderBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        ToastU.INSTANCE.showToast(err);
                    }

                    @Override
                    public void onSuccess(@NonNull ConfirmOrderBean data) {
                        if(isFinishing()){
                            return;
                        }
                        confirmOrderBean=data;
                        dataListBean=confirmOrderBean.getData_list();
                        orderGoodsAdapter=new OrderGoodsAdapter(ConfirmOrderActivity.this,dataListBean);
                        customExpandableListView.setAdapter(orderGoodsAdapter);
                        for (int i = 0; i < dataListBean.getStore_list().size(); i++) {
                            customExpandableListView.expandGroup(i); //关键步骤4:初始化，将ExpandableListView以展开的方式显示
                        }
                        handler.sendEmptyMessage(1);
                    }
                },false);
    }

    //提交订单
    private void submitOrders(){
        if(TextUtils.isEmpty(address_id)){
            ToastU.INSTANCE.showToast(getString(R.string.choose_address));
            btn_submit_order.setClickable(true);
            return;
        }
        if(buy_company_id!=0){
            if(Double.parseDouble(confirmOrderBean.getData_list().getCompany_count_money())>0){
                if(TextUtils.isEmpty(tv_trading_way.getText().toString())){
                    btn_submit_order.setClickable(true);
                    ToastU.INSTANCE.showToast(getString(R.string.trading_way));
                    return;
                }
            }
        }
        if(TextUtils.isEmpty(pay_active)){
            btn_submit_order.setClickable(true);
            ToastU.INSTANCE.showToast(getString(R.string.zhifu_way));
            return;
        }
        DialogUtils.createTipAllLoadDialog(ConfirmOrderActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_SUBMIT_ORDER())
                .addParam("cart_str",cart_str)
                .addParam("ifcart",dataListBean.getIfcart())
                .addParam("address_id",address_id)
                .addParam("pay_currency",pay_currency)
                .addParam("deal_active",deal_active)
                .addParam("pay_active",pay_active)
                .addParam("buy_company_id",buy_company_id)
                .addParam("buy_member_id",buy_member_id)
                .addParam("bank_area",bank_area==null?"":bank_area)
                .addParam("bank_item",bank_item==null?"":bank_item)
                .addParam("bank_currency",bank_currency==null?"":bank_currency)
                .addParam("pay_check",pay_check==null?"":pay_check)
                .addParam("address_check_id",address_check_id==null?"":address_check_id)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        btn_submit_order.setClickable(true);
                        tv_added_new.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogUtils.createTipImageAndTextDialog(ConfirmOrderActivity.this,getString(R.string.enterprise_submit_fail),R.mipmap.png_icon_popup_wrong);
                            }
                        },1000);

                    }

                    @Override
                    public void onSuccess(@NonNull String data) {
                        if(isFinishing()){
                            return;
                        }
                        btn_submit_order.setClickable(true);
                        try {
                            final org.json.JSONObject jsonObject = new org.json.JSONObject(data);
                            if(jsonObject.getInt("company_id")>0 && confirmOrderBean.getData_list().getIs_enabled_approve().equals("1")){
                                tv_added_new.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //发起审批
                                        try {
                                            approval(jsonObject.getString("order_main_id"),jsonObject.getString("company_id"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },1000);

                            }else {
                                final org.json.JSONObject finalJsonObject = jsonObject;
                                final org.json.JSONObject finalJsonObject1 = jsonObject;
                                tv_added_new.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Bundle bundle=new Bundle();
                                        try {
                                            bundle.putString("order_main_id", jsonObject.getString("order_main_id"));
                                            goToActivity(OrderDetilsActivity.class,bundle);
                                            ConfirmOrderActivity.this.finish();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                },1000);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },false);
    }

    //发起审批
    private void approval(String order_id,String company_id){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ORDER_APPROVE())
                .addParam("order_main_id",order_id)
                .addParam("company_id",company_id)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        ToastU.INSTANCE.showToast(err);
                        DialogUtils.moven();

                    }

                    @Override
                    public void onSuccess(@NonNull String data) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Bundle bundle=new Bundle();
                        bundle.putString("order_approval",data);
                        goToActivity(OrderApprovalActivity.class,bundle);
                        ConfirmOrderActivity.this.finish();

                    }
                },false);
    }

    private void setTotalAmount(double allprice){
        //订单总金额
        tv_total_price.setText("¥"+Utils.getDisplayMoney(allprice));
        if (pay_currency.equals("HKD")) {
            tv_total_dollar.setVisibility(View.VISIBLE);
            currency =allprice * confirmOrderBean.getCurrencytype().getHKD().getRate();
            tv_total_dollar.setText("（~"+confirmOrderBean.getCurrencytype().getHKD().getSymbol()+Utils.getDisplayMoney(currency)+"）");
        } else if (pay_currency.equals("MOP")) {
            tv_total_dollar.setVisibility(View.VISIBLE);
            currency = allprice * confirmOrderBean.getCurrencytype().getMOP().getRate();
            tv_total_dollar.setText("（~"+confirmOrderBean.getCurrencytype().getMOP().getSymbol()+Utils.getDisplayMoney(currency)+"）");
        }else if(pay_currency.equals("RMB")){
            tv_total_dollar.setVisibility(View.GONE);
        }else if(pay_currency.equals("USD")){
            tv_total_dollar.setVisibility(View.VISIBLE);
            currency = allprice * confirmOrderBean.getCurrencytype().getUSD().getRate();
            tv_total_dollar.setText("（~"+confirmOrderBean.getCurrencytype().getUSD().getSymbol()+Utils.getDisplayMoney(currency)+"）");
        }
    }

}
