package com.xht.kuaiyouyi.ui.message.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadSampleListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.xht.kuaiyouyi.BuildConfig;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.message.db.FileCache;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Utils;

import org.litepal.LitePal;

import java.io.File;
import java.text.MessageFormat;
import java.util.List;

import butterknife.BindView;

/**
 * 文件预览页面
 */
public class FilePreviewActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_file_type)
    ImageView iv_file_type;
    @BindView(R.id.tv_file_name)
    TextView tv_file_name;
    @BindView(R.id.tv_status)
    TextView tv_status;
    @BindView(R.id.rl_status)
    RelativeLayout rl_status;

    private final int PERMISSION_CODE = 2000;

    public static final String KEY_FILE_NAME = "key_file_name";
    public static final String KEY_FILE_URL = "key_file_url";
    public static final String KEY_FILE_LENGTH = "key_file_length";

    private String mFileName;
    private String mFileUrl;
    private String mFileLocalPath;
    private long mFileLength;

    private int mFlag = 0;//底部按钮的状态，0是下载，1是下载完成


    @Override
    protected int getLayout() {
        return R.layout.activity_filepreview;
    }

    @Override
    protected void initView() {
        mFileName = getIntent().getStringExtra(KEY_FILE_NAME);
        mFileUrl = getIntent().getStringExtra(KEY_FILE_URL);
        mFileLength = getIntent().getLongExtra(KEY_FILE_LENGTH, 0);
        tv_title.setText(mFileName);
        tv_file_name.setText(mFileName);
        setImgType();

        List<FileCache> mFileCache = LitePal.where("file_url='" + mFileUrl + "'").find(FileCache.class);
        if (mFileCache != null && mFileCache.size() > 0) {
            if (new File(mFileCache.get(0).getFile_local_path()).exists()) {
                mFlag = 1;
                mFileLocalPath = mFileCache.get(0).getFile_local_path();
                setTv_status();
            } else {
                //文件不存在了，删除掉以前的缓存记录
                LitePal.deleteAll(FileCache.class, "file_url='" + mFileUrl + "'");
                mFlag = 0;
                setTv_status();
            }
        } else {
            mFlag = 0;
            setTv_status();

        }
        rl_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mFlag) {
                    case 0:
                        if (ActivityCompat.checkSelfPermission(FilePreviewActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            downLoadFile();
                        } else {
                            ActivityCompat.requestPermissions(FilePreviewActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE);
                        }
                        break;
                    case 1:
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        //第二个参数需要和manifest中的fileprovider配置的authorities值一致
                        intent.setData(FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID + ".shareprovider", new File(mFileLocalPath)));
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        if (intent.resolveActivity(getPackageManager()) != null) {
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.unknow_file_noopen, Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setImgType() {
        if (mFileName.endsWith(".pdf")) {
            iv_file_type.setImageResource(R.mipmap.preview_icon_pdf);
        } else if (mFileName.endsWith(".ppt")|| mFileName.endsWith(".pptx")) {
            iv_file_type.setImageResource(R.mipmap.preview_icon_ppt);
        } else if (mFileName.endsWith(".docx") || mFileName.endsWith(".doc")) {
            iv_file_type.setImageResource(R.mipmap.preview_icon_docx);
        } else if (mFileName.endsWith(".xls") || mFileName.endsWith(".xlsx")) {
            iv_file_type.setImageResource(R.mipmap.preview_icon_xls);
        } else {
            iv_file_type.setImageResource(R.mipmap.preview_icon_file);
        }
    }

    /**
     * 文件不用创建，直接把路径给到FileDownloader就行
     */
    private void downLoadFile() {
        FileDownloader.setup(this);
        File target = null;
        try {
            //这里得到的就是我们要的文件了，接下来是保存文件。
            target = new File(KyyConstants.INSTANCE.getDOWNLOAD_DIR() + mFileName);//filepath是目标保存文件的路径，根据自己的项目需要去配置
            if (!target.getParentFile().exists()) {
                target.getParentFile().mkdir();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (target != null) {
            DialogUtils.createTipAllLoadDialog(FilePreviewActivity.this);
            FileDownloader.getImpl().create(mFileUrl)
                    .setPath(target.getPath())
                    .setCallbackProgressTimes(300)
                    .setMinIntervalUpdateSpeed(400)
                    .setListener(new FileDownloadSampleListener() {
                        @Override
                        protected void completed(BaseDownloadTask task) {
                            if (!isFinishing()) {
                                DialogUtils.moven();
                                mFlag = 1;
                                mFileLocalPath = task.getTargetFilePath();
                                setTv_status();
                            }

                            //下载成功之后缓存本地路径
                            FileCache fileCache = new FileCache();
                            fileCache.setFile_length(mFileLength);
                            fileCache.setFile_url(mFileUrl);
                            fileCache.setFile_name(mFileName);
                            fileCache.setFile_type(Utils.getFileType(mFileName));
                            fileCache.setFile_local_path(mFileLocalPath);
                            fileCache.save();

                            Toast.makeText(getApplicationContext(), MessageFormat.format(getString(R.string.file_download_sucessfull), KyyConstants.INSTANCE.getDOWNLOAD_DIR()), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        protected void error(BaseDownloadTask task, Throwable e) {
                            DialogUtils.moven();
                            Toast.makeText(getApplicationContext(), R.string.file_download_failure, Toast.LENGTH_SHORT).show();
                        }
                    }).start();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                downLoadFile();
            } else {
                Toast.makeText(getApplicationContext(), R.string.tip_permission_storage, Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void setTv_status() {
        Drawable drawableLeft = null;
        switch (mFlag) {
            case 0:
                tv_status.setText(getString(R.string.download) + "（" + Utils.getDisplayLength(mFileLength) + "）");
                drawableLeft = getResources().getDrawable(
                        R.mipmap.preview_icon_download);
                tv_status.setCompoundDrawablesWithIntrinsicBounds(drawableLeft,
                        null, null, null);
                tv_status.setCompoundDrawablePadding(Utils.dp2px(getApplicationContext(), 5));
                break;
            case 1:
                tv_status.setText(R.string.open);
                drawableLeft = getResources().getDrawable(
                        R.mipmap.preview_icon_open);
                tv_status.setCompoundDrawablesWithIntrinsicBounds(drawableLeft,
                        null, null, null);
                tv_status.setCompoundDrawablePadding(Utils.dp2px(getApplicationContext(), 5));
                break;
        }
    }
}
