package com.xht.kuaiyouyi.ui.home.activity;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.classic.common.MultipleStatusView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.home.adapter.BrandDetilsAdapter;
import com.xht.kuaiyouyi.ui.home.entity.BrandDetilsBean;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.BadgeView;
import com.xht.kuaiyouyi.widget.RoundImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class BrandDetilsActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.edit_brand_detils_search)
    EditText edit_search;
    @BindView(R.id.image_brand_avatar)
    RoundImageView image_brand_avatar;
    @BindView(R.id.tv_brand_detils_name)
    TextView tv_brand_detils_name;
    @BindView(R.id.recycler_brand_detils)
    RecyclerView recyclerView;
    @BindView(R.id.multipleStatusView)
    MultipleStatusView multipleStatusView;
    @BindView(R.id.mRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.radioGroup_brand_detils)
    RadioGroup radioGroup;
    @BindView(R.id.rb_zonghe)
    RadioButton rb_zonghe;
    @BindView(R.id.rb_xiaoliang)
    RadioButton rb_xiaoliang;
    @BindView(R.id.rb_price)
    RadioButton rb_price;
    @BindView(R.id.iv_switch)
    ImageView iv_switch;
    @BindView(R.id.iv_message)
    ImageView iv_message;
    @BindView(R.id.error_retry_view)
    RelativeLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;
    @BindView(R.id.iv_back_err)
    ImageView iv_back_err;
    @BindView(R.id.appbarlayout)
    AppBarLayout appBarLayout;
    private String kyy="";
    private String brand_id;
    private String order_keyword="goods_id";
    private String order="desc";
    private String keyword="";
    private List<BrandDetilsBean.GoodsListBean> list=new ArrayList<>();
    private BrandDetilsBean.CurrencytypeBean currlist=null;
    private BrandDetilsBean brandDetils;
    private BrandDetilsAdapter adapter;
    private boolean flag=false;
    private int info=1;
    private int state=0;
    private int img_height;

    private BadgeView mBadgeView;


    Handler handler=new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    tv_brand_detils_name.setText(brandDetils.getBrand_info().getBrand_name());
                    if(!TextUtils.isEmpty(brandDetils.getBrand_info().getBrand_pic())){
                        Glide.with(BrandDetilsActivity.this)
                                .load(brandDetils.getBrand_info().getBrand_pic())
                                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_rectangle))
                                .into(image_brand_avatar);
                    }
                    if(list.size()==0){
                        multipleStatusView.showEmpty();
                    }
                    break;
            }
        }
    };

    @Override
    protected int getLayout() {
        return R.layout.activity_brand_detils;
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        mBadgeView = new BadgeView(this,iv_message,10,4);
        brand_id=getIntent().getExtras().getString("brand_id");
        img_height=(Utils.getWindowWidth(BrandDetilsActivity.this)-8)/2;
        if(!TextUtils.isEmpty(brand_id)){
            getData();
        }
        click();
        if(Login.Companion.getInstance().isLogin()){
            showUnreadMsgNum();
        }
        adapter=new BrandDetilsAdapter(BrandDetilsActivity.this,R.layout.store_recycler_item_layout,list,currlist,true,img_height);
        recyclerView.setLayoutManager(new LinearLayoutManager(BrandDetilsActivity.this));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Utils.hintKeyBoard(BrandDetilsActivity.this);
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });
        iv_back_err.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BrandDetilsActivity.this.finish();
            }
        });
    }

    private void getData(){
        if(kyy.equals("")){
            list.clear();
            multipleStatusView.showLoading();
        }
        NetUtil.Companion.getInstance()
                .url(KyyConstants.INSTANCE.getURL_BRAND_DETILS())
                .addParam("brand_id",brand_id)
                .addParam("order",order)
                .addParam("order_keyword",order_keyword)
                .addParam("keyword",keyword)
                .withLoadPOST(new NetCallBack<BrandDetilsBean>() {
            @NotNull
            @Override
            public Class<BrandDetilsBean> getRealType() {
                return BrandDetilsBean.class;
            }

            @Override
            public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                if(isFinishing()){
                    return;
                }
                if(TextUtils.isEmpty(kyy)){
                    error_retry_view.setVisibility(View.VISIBLE);
                    smartRefreshLayout.setVisibility(View.GONE);
                    appBarLayout.setVisibility(View.GONE);
                }else {
                    ToastU.INSTANCE.showToast( getString(R.string.not_network));
                }
                if(smartRefreshLayout.getState() == RefreshState.Loading){
                    smartRefreshLayout.finishLoadMore();
                }
            }

            @Override
            public void onSuccess(@NonNull BrandDetilsBean brandDetilsBean) {
                if(isFinishing()){
                    return;
                }
                error_retry_view.setVisibility(View.GONE);
                smartRefreshLayout.setVisibility(View.VISIBLE);
                appBarLayout.setVisibility(View.VISIBLE);
                if(smartRefreshLayout.getState() == RefreshState.Loading){
                    smartRefreshLayout.finishLoadMore();
                }
                multipleStatusView.showContent();
                brandDetils=brandDetilsBean;
                handler.sendEmptyMessage(0);
                if(brandDetilsBean!=null && brandDetilsBean.getGoods_list().size()!=0){
                    kyy=brandDetilsBean.getKyy();
                    for(int i=0;i<brandDetilsBean.getGoods_list().size();i++){
                        list.add(brandDetilsBean.getGoods_list().get(i));
                    }
                }else {
                    smartRefreshLayout.finishLoadMoreWithNoMoreData();
//                    smartRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
                }
                if(brandDetilsBean.getCurrencytype()!=null){
                    currlist=brandDetilsBean.getCurrencytype();
                }
                if(state==0){
                    adapter=new BrandDetilsAdapter(BrandDetilsActivity.this,R.layout.store_recycler_item_layout,list,currlist,true,img_height);
                    recyclerView.setLayoutManager(new LinearLayoutManager(BrandDetilsActivity.this));
                    recyclerView.setAdapter(adapter);
                    state=1;
                }else {
                    adapter.notifyDataSetChanged();
                }
                if(list.size()==0){
                    multipleStatusView.showEmpty();
                }
            }

        },false,kyy);

    }

    private void click(){
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BrandDetilsActivity.this.finish();
            }
        });
        rb_zonghe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag=true){
                    flag=false;
                }else {
                    flag=true;
                }
                kyy="";
                order_keyword="goods_id";
                rb_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_price_nor),null);
                getData();
            }
        });

        rb_xiaoliang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag=true){
                    flag=false;
                }else {
                    flag=true;
                }
                kyy="";
                order_keyword="goods_salenum";
                rb_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_price_nor),null);
                getData();
            }
        });

        rb_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kyy="";
                order_keyword="goods_price";
                if(flag){
                    flag=false;
                    rb_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.shop_icon_priceup_sel),null);
                    order="desc";

                }else {
                    flag=true;
                    rb_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.shop_icon_pricedown_sel),null);
                    order="asc";
                }
                getData();
            }
        });

        iv_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(info==0){
                    info=1;
                    iv_switch.setImageResource(R.mipmap.search_icon_palace_nor);
                    recyclerView.setLayoutManager(new LinearLayoutManager(BrandDetilsActivity.this));
                    adapter=new BrandDetilsAdapter(BrandDetilsActivity.this,R.layout.store_recycler_item_layout,list,currlist,true,img_height);
                    recyclerView.setAdapter(adapter);
                }else {
                    info=0;
                    iv_switch.setImageResource(R.mipmap.search_icon_list_nor);
                    GridLayoutManager manager = new GridLayoutManager(BrandDetilsActivity.this, 2,GridLayoutManager.VERTICAL,false);
                    recyclerView.setLayoutManager(manager);
                    adapter=new BrandDetilsAdapter(BrandDetilsActivity.this,R.layout.home_recycler_item_layout,list,currlist,false,img_height);
                    recyclerView.setAdapter(adapter);
                }
            }
        });

        multipleStatusView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });

        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                smartRefreshLayout.setNoMoreData(false) ;
                getData();
            }
        });

        edit_search.setOnEditorActionListener(new TextView.OnEditorActionListener(){

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //判断是否是“完成”键
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    //隐藏软键盘
                    InputMethodManager imm = (InputMethodManager) v
                            .getContext().getSystemService(
                                    Context.INPUT_METHOD_SERVICE);
                    if (imm.isActive()) {
                        imm.hideSoftInputFromWindow(
                                v.getApplicationWindowToken(), 0);
                    }

                    if(!edit_search.getText().toString().isEmpty()){
                        keyword=edit_search.getText().toString();
                    }
                    kyy="";
                    getData();
                    return true;
                }
                return false;
            }
        });

        iv_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Login.Companion.getInstance().isLogin()){
                    goToActivity(MessageActivity.class);
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void showUnreadMsgNum() {
        mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event){
        if(event.getMessage().equals(MessageEvent.UNREAD_MSG_NUM)){
            showUnreadMsgNum();
        }
    }
}
