package com.xht.kuaiyouyi.ui.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.home.entity.BrandListBean;
import com.xht.kuaiyouyi.widget.SideBarUtil;

import java.util.List;

public class BrandListAdapter extends BaseAdapter {
    private Context mContext;
    private List<BrandListBean.BrandBean.DataBean> mBrandListBeans;

    public BrandListAdapter(Context mContext, List<BrandListBean.BrandBean.DataBean> mBrandListBeans) {
        this.mContext = mContext;
        this.mBrandListBeans = mBrandListBeans;
    }

    @Override
    public int getCount() {
        return mBrandListBeans.size();
    }

    @Override
    public Object getItem(int i) {
        return mBrandListBeans.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        BrandListBean.BrandBean.DataBean brandBean = mBrandListBeans.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_brand, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_brand_name = convertView.findViewById(R.id.tv_brand_name);
            viewHolder.tv_index_title = convertView.findViewById(R.id.tv_index_title);
            viewHolder.iv_brand_icon = convertView.findViewById(R.id.iv_brand_icon);
            viewHolder.view_line = convertView.findViewById(R.id.view_line);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // 如果当前位置为第一次出现该类首字母的位置,则显示headerTv
        if (position == SideBarUtil.getPositionForSection(position, mBrandListBeans)) {
            viewHolder.tv_index_title.setVisibility(View.VISIBLE);
            viewHolder.tv_index_title.setText(brandBean.getBrand_initial());
        } else {
            viewHolder.tv_index_title.setVisibility(View.GONE);
        }
        if(SideBarUtil.isLast(position,mBrandListBeans)){
            viewHolder.view_line.setVisibility(View.GONE);
        }else {
            viewHolder.view_line.setVisibility(View.VISIBLE);
        }
        viewHolder.tv_brand_name.setText(brandBean.getBrand_name());
        Glide.with(mContext).load(brandBean.getBrand_pic())
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_rectangle))
                .into(viewHolder.iv_brand_icon);
        return convertView;
    }

    /**
     * vh
     */
    class ViewHolder {
        TextView tv_brand_name;
        ImageView iv_brand_icon;
        TextView tv_index_title;
        View view_line;
    }
}
