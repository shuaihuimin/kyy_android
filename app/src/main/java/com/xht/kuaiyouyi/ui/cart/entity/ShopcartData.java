package com.xht.kuaiyouyi.ui.cart.entity;

import java.util.List;

public class ShopcartData {
    private String store_id;
    private String store_name;
    private List<GoodsInfo> list;

    public ShopcartData(String store_id, String store_name, List<GoodsInfo> list) {
        this.store_id = store_id;
        this.store_name = store_name;
        this.list = list;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public List<GoodsInfo> getList() {
        return list;
    }

    public void setList(List<GoodsInfo> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "ShopcartData{" +
                "store_id='" + store_id + '\'' +
                ", store_name='" + store_name + '\'' +
                ", list=" + list +
                '}';
    }
}
