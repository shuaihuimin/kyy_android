package com.xht.kuaiyouyi.ui.goods.entity;

import java.util.List;

public class GoodsGagueData {



        /**
         * goods_storage : 160
         * spec_all_info : [{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"紅色丨20丨1.5丨X110","goods_id":"100309","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"藍色丨20丨1.5丨X110","goods_id":"100310","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"紅色丨30丨1.5丨X110","goods_id":"100311","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"藍色丨30丨1.5丨X110","goods_id":"100312","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"紅色丨20丨3.3丨X110","goods_id":"100313","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"藍色丨20丨3.3丨X110","goods_id":"100314","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"紅色丨30丨3.3丨X110","goods_id":"100315","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"藍色丨30丨3.3丨X110","goods_id":"100316","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"紅色丨20丨1.5丨X503","goods_id":"100317","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"藍色丨20丨1.5丨X503","goods_id":"100318","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"紅色丨30丨1.5丨X503","goods_id":"100319","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"藍色丨30丨1.5丨X503","goods_id":"100320","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"紅色丨20丨3.3丨X503","goods_id":"100321","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"藍色丨20丨3.3丨X503","goods_id":"100322","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"紅色丨30丨3.3丨X503","goods_id":"100323","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"},{"goods_promotion_price":"5.00","goods_price":5,"goods_marketprice":"10.00","goods_spec":"藍色丨30丨3.3丨X503","goods_id":"100324","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif","goods_state":"1","goods_storage":10,"goods_verify":"1"}]
         * spec_checked : [{"id":1,"name":"颜色","item":[{"id":28,"name":"紅色"},{"id":29,"name":"藍色"}]},{"id":2,"name":"尺寸","item":[{"id":22,"name":"20"},{"id":23,"name":"30"}]},{"id":3,"name":"尺码","item":[{"id":24,"name":"1.5"},{"id":25,"name":"3.3"}]},{"id":4,"name":"型号","item":[{"id":26,"name":"X110"},{"id":27,"name":"X503"}]}]
         * currencytype : {"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.1592}
         */

        private int goods_storage;
        private CurrencytypeBean currencytype;
        private List<SpecAllInfoBean> spec_all_info;
        private List<SpecCheckedBean> spec_checked;
        private List<BatchPriceBean> batch_price;

        public int getGoods_storage() {
            return goods_storage;
        }

        public void setGoods_storage(int goods_storage) {
            this.goods_storage = goods_storage;
        }

        public CurrencytypeBean getCurrencytype() {
            return currencytype;
        }

        public void setCurrencytype(CurrencytypeBean currencytype) {
            this.currencytype = currencytype;
        }

        public List<SpecAllInfoBean> getSpec_all_info() {
            return spec_all_info;
        }

        public void setSpec_all_info(List<SpecAllInfoBean> spec_all_info) {
            this.spec_all_info = spec_all_info;
        }

        public List<SpecCheckedBean> getSpec_checked() {
            return spec_checked;
        }

        public void setSpec_checked(List<SpecCheckedBean> spec_checked) {
            this.spec_checked = spec_checked;
        }

        public List<BatchPriceBean> getBatch_price() {
            return batch_price;
        }

        public void setBatch_price(List<BatchPriceBean> batch_price) {
            this.batch_price = batch_price;
        }

    public static class CurrencytypeBean {
            /**
             * name : 美元
             * symbol : US$
             * currency : USD
             * cackeKey : cacheCurrencyUSD
             * rate : 0.1592
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class SpecAllInfoBean {
            /**
             * goods_promotion_price : 5.00
             * goods_price : 5
             * goods_marketprice : 10.00
             * goods_spec : 紅色丨20丨1.5丨X110
             * goods_id : 100309
             * goods_image : http://192.168.0.2/data/upload/shop/common/default_goods_image_240.gif
             * goods_state : 1
             * goods_storage : 10
             * goods_verify : 1
             */

            private String goods_promotion_price;
            private double goods_price;
            private String goods_marketprice;
            private String goods_spec;
            private String goods_id;
            private String goods_image;
            private String goods_state;
            private int goods_storage;
            private String goods_verify;
            private String is_enquiry;

            public String getGoods_promotion_price() {
                return goods_promotion_price;
            }

            public void setGoods_promotion_price(String goods_promotion_price) {
                this.goods_promotion_price = goods_promotion_price;
            }

            public double getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(double goods_price) {
                this.goods_price = goods_price;
            }

            public String getGoods_marketprice() {
                return goods_marketprice;
            }

            public void setGoods_marketprice(String goods_marketprice) {
                this.goods_marketprice = goods_marketprice;
            }

            public String getGoods_spec() {
                return goods_spec;
            }

            public void setGoods_spec(String goods_spec) {
                this.goods_spec = goods_spec;
            }

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getGoods_image() {
                return goods_image;
            }

            public void setGoods_image(String goods_image) {
                this.goods_image = goods_image;
            }

            public String getGoods_state() {
                return goods_state;
            }

            public void setGoods_state(String goods_state) {
                this.goods_state = goods_state;
            }

            public int getGoods_storage() {
                return goods_storage;
            }

            public void setGoods_storage(int goods_storage) {
                this.goods_storage = goods_storage;
            }

            public String getGoods_verify() {
                return goods_verify;
            }

            public void setGoods_verify(String goods_verify) {
                this.goods_verify = goods_verify;
            }

            public String getIs_enquiry() {
                return is_enquiry;
            }

            public void setIs_enquiry(String is_enquiry) {
                this.is_enquiry = is_enquiry;
            }
        }

        public static class SpecCheckedBean {
            /**
             * id : 1
             * name : 颜色
             * item : [{"id":28,"name":"紅色"},{"id":29,"name":"藍色"}]
             */

            private int id;
            private String name;
            private List<ItemBean> item;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public List<ItemBean> getItem() {
                return item;
            }

            public void setItem(List<ItemBean> item) {
                this.item = item;
            }

            public static class ItemBean {
                /**
                 * id : 28
                 * name : 紅色
                 */

                private int id;
                private String name;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }
            }
        }

    public static class BatchPriceBean {
        /**
         * mix : 1
         * value : 150
         * max : 10
         */

        private int mix;
        private double value;
        private int max;

        public int getMix() {
            return mix;
        }

        public void setMix(int mix) {
            this.mix = mix;
        }

        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public int getMax() {
            return max;
        }

        public void setMax(int max) {
            this.max = max;
        }
    }
}
