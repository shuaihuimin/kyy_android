package com.xht.kuaiyouyi.ui.enterprise;

import android.os.Build;
import android.provider.Settings;

import com.xht.kuaiyouyi.KyyApp;

public class Constant {

    public static final String DEVICE_NAME = Build.MODEL;
    public static final String DEVICE_UUID = Settings.Secure.getString(KyyApp.context.getContentResolver(), Settings.Secure.ANDROID_ID);
    public static String SIGN = "";
    public static String TOKEN_ID = "";

    public static final String COMPANY_ID = "company_id";

}
