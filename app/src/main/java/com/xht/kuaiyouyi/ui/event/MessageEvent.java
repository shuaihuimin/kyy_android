package com.xht.kuaiyouyi.ui.event;

public class MessageEvent{
    private String message;
    private String keyword;
    private int is_second_hand;

    public static final String EVENT_ClOSE_LOGINACTIVITY = "event_login";
    public static final String UPDATE_AVATAR="update_avatar";
    public static final String UNREAD_MSG_NUM="unread_msg_num";
    public static final String ORDERDETILS="orderdetils";
    public static final String REFRESH_ORDER_LIST = "refresh_order_list";
    public static final String REFRESH_APPROVAL_LIST = "refresh_approval_list";
    public static final String REFRESH_APPROVAL_DETAIL = "refresh_approval_detail";
    public static final String REFRESH_MAIN_NUM = "refresh_main_num";//企业模块的待付款和待我审批数量更新事件
    public static final String CART="finish";
    public static final String KEYWORD="keyword";
    public static final String SCAN="scan";
    public static final String Goods="goods";
    public static final String GOODS_DETILS="GOODS_DETILS";
    public static final String GOODS_CASE="GOODS_CASE";
    public static final String STORE_UP="UP";
    public static final String STORE_DOWN="DOWN";
    public static final String REFRESH_MAIN_CART="refresh_main_cart";
    public static final String TO_PROMOTIONAL_VIDEO_ACTIVITY ="to_promotional_video_activity";


    public  MessageEvent(String message){
        this.message=message;
    }

    public MessageEvent(String message,String keyword){
        this.message=message;
        this.keyword=keyword;
    }

    public MessageEvent(String message, String keyword,int is_second_hand) {
        this.message = message;
        this.keyword=keyword;
        this.is_second_hand = is_second_hand;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getIs_second_hand() {
        return is_second_hand;
    }

    public void setIs_second_hand(int is_second_hand) {
        this.is_second_hand = is_second_hand;
    }
}


