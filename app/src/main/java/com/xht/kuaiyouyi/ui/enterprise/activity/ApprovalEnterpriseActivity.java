package com.xht.kuaiyouyi.ui.enterprise.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.enterprise.Constant;
import com.xht.kuaiyouyi.ui.enterprise.fragment.ApprovalListFragment;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.widget.BadgeView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ApprovalEnterpriseActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.vp_enterprise_approval_content)
    ViewPager vp_enterprise_approval_content;
    @BindView(R.id.tl_enterprise_approval_tab)
    TabLayout tl_enterprise_approval_tab;
    @BindView(R.id.iv_right)
    ImageView iv_right;

    private BadgeView mBadgeView;



    private List<ApprovalListFragment> mFragments;
    private int[] mTabTitles = {R.string.approval_wait,R.string.approvaled,R.string.approval_start
            ,R.string.approval_copy_to};
    private MyAdapter adapter;
    public static final String APPROVE_ID = "approve_id";

    private String mCompanyId;
    private int mApproveId;
    @Override
    protected int getLayout() {
        return R.layout.activity_enterprise_approval;
    }

    @Override
    protected void initView() {
        mBadgeView = new BadgeView(this,iv_right,10,4);
        iv_right.setVisibility(View.VISIBLE);
        iv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(MessageActivity.class);
            }
        });
        EventBus.getDefault().register(this);
        tv_title.setText(R.string.enterprise_approval);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mCompanyId = getIntent().getStringExtra(Constant.COMPANY_ID);
        mApproveId = getIntent().getIntExtra(APPROVE_ID,ApprovalListFragment.APPROVAL_WAIT);

        mFragments = new ArrayList<>();
        ApprovalListFragment fragmentWait = new ApprovalListFragment();
        fragmentWait.setId(ApprovalListFragment.APPROVAL_WAIT);
        ApprovalListFragment fragmentApprovaled = new ApprovalListFragment();
        fragmentApprovaled.setId(ApprovalListFragment.APPROVALED);
        ApprovalListFragment fragmentStart = new ApprovalListFragment();
        fragmentStart.setId(ApprovalListFragment.APPROVAL_START);
        ApprovalListFragment fragmentCopyTo = new ApprovalListFragment();
        fragmentCopyTo.setId(ApprovalListFragment.APPROVAL_COPY_TO);

        mFragments.add(fragmentWait);
        mFragments.add(fragmentApprovaled);
        mFragments.add(fragmentStart);
        mFragments.add(fragmentCopyTo);

        adapter = new MyAdapter(getSupportFragmentManager());
        vp_enterprise_approval_content.setAdapter(adapter);
        tl_enterprise_approval_tab.setupWithViewPager(vp_enterprise_approval_content);

        vp_enterprise_approval_content.setCurrentItem(mApproveId - 1);
        showUnreadMsgNum();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event){
        if(event.getMessage().equals(MessageEvent.REFRESH_APPROVAL_LIST)){
            for(ApprovalListFragment fragment : mFragments){
                if(fragment.isAdded()){
                    fragment.mKyy="";
                    fragment.requestApprovalList();
                }
            }
        }

        if(event.getMessage().equals(MessageEvent.UNREAD_MSG_NUM)){
            showUnreadMsgNum();
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void showUnreadMsgNum() {
        mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public ApprovalListFragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        //重写这个方法，将设置每个Tab的标题
        @Override
        public CharSequence getPageTitle(int position) {
            return getString(mTabTitles[position]);
        }
    }
}
