package com.xht.kuaiyouyi.ui.message.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.WebActivity;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.message.adapter.SystemNoticeListAdapter;
import com.xht.kuaiyouyi.ui.message.bean.SystemNoticeListBean;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/**
 * 跳公告详情页
 */
public class SystemNoticeFragment extends BaseFragment{
    @BindView(R.id.lv_content)
    ListView lv_content;
    @BindView(R.id.srl_loadmore)
    SmartRefreshLayout srl_loadmore;
    @BindView(R.id.rl_empty_message_list)
    RelativeLayout rl_empty_message_list;


    private String mKyy="";
    private SystemNoticeListBean mSystemNoticeListBean;
    private SystemNoticeListAdapter mAdapter;


    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        srl_loadmore.setEnableRefresh(false);
        requestSystemNoticeList();
        lv_content.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString(WebActivity.TITLE,getString(R.string.tab_system_notice));
                bundle.putString(WebActivity.WEB_URL, mSystemNoticeListBean.getMessage_list()
                        .get(position).getUrl());
                goToActivity(WebActivity.class,bundle);
            }
        });
        srl_loadmore.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                requestSystemNoticeList();
            }
        });
    }

    private void requestSystemNoticeList() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_SYSTEM_NOTICE_LIST())
                .withLoadPOST(new NetCallBack<SystemNoticeListBean>() {
                    @NotNull
                    @Override
                    public Class<SystemNoticeListBean> getRealType() {
                        return SystemNoticeListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                        if(srl_loadmore.getState() == RefreshState.Loading){
                            srl_loadmore.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull SystemNoticeListBean systemNoticeListBean) {
                        if(getActivity()==null){
                            return;
                        }
                        if(TextUtils.isEmpty(mKyy)){
                            if(systemNoticeListBean.getMessage_list()==null||systemNoticeListBean.getMessage_list().size()==0){
                                rl_empty_message_list.setVisibility(View.VISIBLE);
                                return;
                            }
                            srl_loadmore.setEnableLoadMore(true);
                            mSystemNoticeListBean = systemNoticeListBean;
                            mAdapter = new SystemNoticeListAdapter(getContext(),mSystemNoticeListBean);
                            lv_content.setAdapter(mAdapter);
                            mKyy = systemNoticeListBean.getKyy();
                        }else {
                            if(systemNoticeListBean.getMessage_list()!=null&&systemNoticeListBean.getMessage_list().size()>0){
                                mSystemNoticeListBean.getMessage_list().addAll(systemNoticeListBean.getMessage_list());
                                mAdapter.notifyDataSetChanged();
                                mKyy = systemNoticeListBean.getKyy();
                            }else {
                                srl_loadmore.setNoMoreData(true);
                            }
                            srl_loadmore.finishLoadMore();
                        }
                    }
                }, false,mKyy);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_message;
    }

}
