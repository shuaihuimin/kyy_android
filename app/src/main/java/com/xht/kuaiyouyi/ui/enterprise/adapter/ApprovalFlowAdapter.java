package com.xht.kuaiyouyi.ui.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.enterprise.activity.ApprovalDetailEnterpriseActivity;
import com.xht.kuaiyouyi.ui.enterprise.bean.ApprovalDetailBean;
import com.xht.kuaiyouyi.utils.Utils;

import java.util.List;

/**
 * 审批流程
 */
public class ApprovalFlowAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<ApprovalDetailBean.FlowDataBean> mFlowDataBeans;

    private static final int TYPE_COMPLETE = 0;
    private static final int TYPE_ONE_PEOPLE = 1;
    private static final int TYPE_TWO_PEOPLE = 2;

    public ApprovalFlowAdapter(Context mContext, List<ApprovalDetailBean.FlowDataBean> flowDataBeans) {
        this.mContext = mContext;
        this.mFlowDataBeans = flowDataBeans;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_COMPLETE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_enterprise_flow_complete, parent, false);
            return new CompleteViewHolder(view);
        }
        if (viewType == TYPE_ONE_PEOPLE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_enterprise_flow_one_people, parent, false);
            return new OnePeopleViewHolder(view);
        }
        if (viewType == TYPE_TWO_PEOPLE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_enterprise_flow_two_people, parent, false);
            return new TwoPeopleViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CompleteViewHolder) {
            CompleteViewHolder completeViewHolder = (CompleteViewHolder) holder;
            if (position == 0) {
                completeViewHolder.tv_line_top.setVisibility(View.INVISIBLE);
            }
            if (position == mFlowDataBeans.size() - 1) {
                completeViewHolder.tv_line_bottom.setVisibility(View.INVISIBLE);
            }
            completeViewHolder.tv_name.setText(mFlowDataBeans.get(position).getName());
            completeViewHolder.tv_time.setText(Utils.getDisplayData(mFlowDataBeans.get(position).getApprove_time()));
            if (TextUtils.isEmpty(mFlowDataBeans.get(position).getApprove_opinion())) {
                completeViewHolder.tv_opinion.setVisibility(View.GONE);
            } else {
                completeViewHolder.tv_opinion.setText(mFlowDataBeans.get(position).getApprove_opinion());
                completeViewHolder.tv_opinion.setVisibility(View.VISIBLE);
            }

            switch (mFlowDataBeans.get(position).getStatus_name()) {
                case ApprovalDetailEnterpriseActivity.APPROVAL_PASS:
                    completeViewHolder.tv_status.setTextColor(mContext.getResources().getColor(R.color.green_1));
                    completeViewHolder.tv_status.setText(R.string.approval_agree);
                    break;
                case ApprovalDetailEnterpriseActivity.APPROVAL_REJECT:
                    completeViewHolder.tv_status.setTextColor(mContext.getResources().getColor(R.color.red_1));
                    completeViewHolder.tv_status.setText(R.string.approval_reject);
                    break;
                case 1:
                    completeViewHolder.tv_status.setTextColor(mContext.getResources().getColor(R.color.green_1));
                    completeViewHolder.tv_status.setText(R.string.approval_reject);
                    break;
                case 5:
                    completeViewHolder.tv_status.setTextColor(mContext.getResources().getColor(R.color.text_grey));
                    completeViewHolder.tv_status.setText(R.string.launch_approval);
                    break;
                case 4:
                    completeViewHolder.tv_status.setTextColor(mContext.getResources().getColor(R.color.orange_1));
                    completeViewHolder.tv_status.setText(R.string.approval_repeal);
                    break;
            }

        }
        if (holder instanceof OnePeopleViewHolder) {
            OnePeopleViewHolder onePeopleViewHolder = (OnePeopleViewHolder) holder;
            if (position == mFlowDataBeans.size() - 1) {
                onePeopleViewHolder.tv_line_bottom.setVisibility(View.INVISIBLE);
            }
            if (mFlowDataBeans.get(position).getStatus_name()==1) {
                onePeopleViewHolder.iv_point.setImageResource(R.mipmap.purchase_icon_point_sel);
                onePeopleViewHolder.tv_status.setText(R.string.approval_ing);
            } else {
                onePeopleViewHolder.iv_point.setImageResource(R.mipmap.purchase_icon_point_nor);
                onePeopleViewHolder.tv_status.setText("");
            }
            onePeopleViewHolder.tv_name.setText(mFlowDataBeans.get(position).getNames());

        }
        if (holder instanceof TwoPeopleViewHolder) {
            TwoPeopleViewHolder twoPeopleViewHolder = (TwoPeopleViewHolder) holder;
            if (position == mFlowDataBeans.size() - 1) {
                twoPeopleViewHolder.tv_line_bottom.setVisibility(View.INVISIBLE);
            }
            if (mFlowDataBeans.get(position).getStatus_name()==1) {
                twoPeopleViewHolder.iv_point.setImageResource(R.mipmap.purchase_icon_point_sel);
            } else {
                twoPeopleViewHolder.iv_point.setImageResource(R.mipmap.purchase_icon_point_nor);
            }
            if (mFlowDataBeans.get(position).getStatus_name()==1) {
                twoPeopleViewHolder.iv_point.setImageResource(R.mipmap.purchase_icon_point_sel);
                twoPeopleViewHolder.tv_status.setText(R.string.approval_ing);

            } else {
                twoPeopleViewHolder.iv_point.setImageResource(R.mipmap.purchase_icon_point_nor);
                twoPeopleViewHolder.tv_status.setText("");
            }
            if (mFlowDataBeans.get(position).getApprove_mode() == 1) {
                //会签
                twoPeopleViewHolder.tv_desc.setText(R.string.all_sign_tip);
                twoPeopleViewHolder.tv_name.setText(mFlowDataBeans.get(position).getNames());
                String[] names = mFlowDataBeans.get(position).getNames().split("、");
                twoPeopleViewHolder.tv_num.setText(names.length + mContext.getResources().getString(R.string.all_sign));
            } else if (mFlowDataBeans.get(position).getApprove_mode() == 2) {
                //或签
                twoPeopleViewHolder.tv_desc.setText(R.string.or_sign_tip);
                twoPeopleViewHolder.tv_name.setText(mFlowDataBeans.get(position).getNames());
                String[] names = mFlowDataBeans.get(position).getNames().split("、");
                twoPeopleViewHolder.tv_num.setText(names.length + mContext.getResources().getString(R.string.or_sign));
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mFlowDataBeans == null) {
            return 0;
        }
        return mFlowDataBeans.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mFlowDataBeans.get(position).isStatus()) {
            return TYPE_COMPLETE;
        }
        if (mFlowDataBeans.get(position).getStatus_name()==4) {
            return TYPE_COMPLETE;
        }
        if (!mFlowDataBeans.get(position).isStatus()) {
            if (mFlowDataBeans.get(position).getApprove_mode() == 0) {
                return TYPE_ONE_PEOPLE;
            } else {
                return TYPE_TWO_PEOPLE;
            }
        }
        return 0;
    }

    private class CompleteViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_line_top, tv_line_bottom, tv_name, tv_status, tv_time, tv_opinion;
        private ImageView iv_point;

        public CompleteViewHolder(View itemView) {
            super(itemView);
            tv_line_top = itemView.findViewById(R.id.tv_line_top);
            tv_line_bottom = itemView.findViewById(R.id.tv_line_bottom);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_opinion = itemView.findViewById(R.id.tv_opinion);
            iv_point = itemView.findViewById(R.id.iv_point);
        }


    }

    private class OnePeopleViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_line_top, tv_line_bottom, tv_name, tv_status;
        private ImageView iv_point;

        public OnePeopleViewHolder(View itemView) {
            super(itemView);
            tv_line_top = itemView.findViewById(R.id.tv_line_top);
            tv_line_bottom = itemView.findViewById(R.id.tv_line_bottom);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_status = itemView.findViewById(R.id.tv_status);
            iv_point = itemView.findViewById(R.id.iv_point);
        }


    }

    private class TwoPeopleViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_line_top, tv_line_bottom, tv_num, tv_desc, tv_status, tv_name;
        private ImageView iv_point;

        public TwoPeopleViewHolder(View itemView) {
            super(itemView);
            tv_line_top = itemView.findViewById(R.id.tv_line_top);
            tv_line_bottom = itemView.findViewById(R.id.tv_line_bottom);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_num = itemView.findViewById(R.id.tv_num);
            tv_desc = itemView.findViewById(R.id.tv_desc);
            iv_point = itemView.findViewById(R.id.iv_point);
        }


    }
}
