package com.xht.kuaiyouyi.ui.mine.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.mine.entity.StoreCollectionBean;
import com.xht.kuaiyouyi.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreCollectionAdapter extends BaseAdapter {
    private Context context;
    private List<StoreCollectionBean.FavoritesListBean> list;

    public StoreCollectionAdapter(Context context, List<StoreCollectionBean.FavoritesListBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView==null){
            convertView=View.inflate(context, R.layout.item_storecollection_layout,null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        }else {
            viewHolder= (ViewHolder) convertView.getTag();
        }

        if(list.get(position).getIs_second_hand().equals("1")){
            viewHolder.nameview.setText(Utils.secondLabel(list.get(position).getStore_name()));
        }else {
            viewHolder.nameview.setText(list.get(position).getStore_name());
        }
        Glide.with(context).load(list.get(position).getImg())
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_rectangle))
                .into(viewHolder.imageView);
        if(position==0){
            viewHolder.view_top.setVisibility(View.VISIBLE);
        }else {
            viewHolder.view_top.setVisibility(View.GONE);
        }
        if(position==list.size()-1){
            viewHolder.view_line.setVisibility(View.GONE);
        }else {
            viewHolder.view_line.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    class ViewHolder{
        @BindView(R.id.store_name)
        TextView nameview;
        @BindView(R.id.store_image)
        ImageView imageView;
        @BindView(R.id.view_top)
        View view_top;
        @BindView(R.id.view_line)
        View view_line;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
