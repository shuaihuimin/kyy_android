package com.xht.kuaiyouyi.ui.home.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;

import butterknife.BindView;

public class SweepYardActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_content)
    TextView tv_content;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @Override
    protected int getLayout() {
        return R.layout.activity_sweepyardactivity;
    }

    @Override
    protected void initView() {
        tv_title.setText(getString(R.string.saoma));
        tv_content.setText(getIntent().getExtras().getString("result"));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SweepYardActivity.this.finish();
            }
        });
    }
}
