package com.xht.kuaiyouyi.ui.goods;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.entity.StoreData;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.ui.message.activity.ChatActivity;
import com.xht.kuaiyouyi.ui.mine.adapter.FragmentAdapter;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.TabLayoutUtils;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.widget.BadgeView;
import com.xht.kuaiyouyi.widget.RoundImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class StoreActivity extends BaseActivity implements AppBarLayout.OnOffsetChangedListener{
    @BindView(R.id.appbarlayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tvSearch)
    TextView tvSearch;
    @BindView(R.id.image_store_avatar)
    RoundImageView image_avatar;
    @BindView(R.id.iv_message)
    ImageView iv_message;
    @BindView(R.id.tv_store_name)
    TextView tv_name;
    @BindView(R.id.tv_store_focus)
    TextView tv_store_focus;
    @BindView(R.id.tl_tabs)
    TabLayout tabLayout;
    @BindView(R.id.vp_fragment)
    ViewPager viewPager;
    @BindView(R.id.linear_store)
    LinearLayout linear_store;
    @BindView(R.id.linear_service)
    LinearLayout linear_service;
    @BindView(R.id.relative)
    RelativeLayout relative;
    @BindView(R.id.relative2)
    RelativeLayout relative2;
    @BindView(R.id.error_retry_view)
    RelativeLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;
    @BindView(R.id.iv_back_err)
    ImageView iv_back_err;

    private BadgeView mBadgeView;

    public final static int TAB_STORE_INDEX = 0;
    public final static int TAB_GOODS_INDEX = 1;
    public final static int TAB_NEWSTORE_INDEX = 2;

    private AppBarLayout.LayoutParams layoutParams;
    private View mAppBarChildAt;

    private List<Fragment> fragments=new ArrayList<>();
    private int[] tabs=new int[]{R.string.store_home,R.string.all_store, R.string.company_profile};
    private StoreData.StoreBean storeBean;
    private String store_id;
    private String isfocus="0";

    private int dy;


    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    if(!TextUtils.isEmpty(storeBean.getMb_title_img())){
//                        Glide.with(StoreActivity.this).load(storeBean.getMb_title_img()).into(image_banner);
//                        RequestOptions options = new RequestOptions()
//                                .centerCrop()
//                                //.priority(Priority.HIGH)
//                                .bitmapTransform(new GrayscaleTransformation())
//                                .diskCacheStrategy(DiskCacheStrategy.ALL);

                        appBarLayout.setBackgroundColor(getResources().getColor(R.color.color_title_bg));
                        Glide.with(StoreActivity.this).load(storeBean.getMb_title_img())
                                .apply(RequestOptions.placeholderOf(R.mipmap.shop_bg_default))
                                .into(new SimpleTarget<Drawable>(200,200) {
                                    @Override
                                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                        appBarLayout.setBackground(resource);
                                    }
                                });
                    }
                    if(!TextUtils.isEmpty(storeBean.getStore_label())){
                        Glide.with(StoreActivity.this).load(storeBean.getStore_label()).into(image_avatar);

                    }
                    if(storeBean.getStore_name()!=null){
                        tv_name.setText(storeBean.getStore_name());
                    }
                    showTablist(tabs,storeBean.getStore_id());
                    if(storeBean.getIs_fav()!=null){
                        isfocus=storeBean.getIs_fav();
                        if(isfocus.equals("0")){
                            tv_store_focus.setText(R.string.collection);
                            tv_store_focus.setBackgroundResource(R.drawable.btn_radius_blue_bg);
                            tv_store_focus.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.mipmap.shop_icon_add),null,null,null);
                        } else{
                            tv_store_focus.setText(R.string.cancel_collection);
                            tv_store_focus.setBackgroundResource(R.drawable.store_collection);
                            tv_store_focus.setCompoundDrawablesWithIntrinsicBounds(null,null,null,null);
                        }
                    }
                    break;
                case 1:
                    if(isfocus.equals("0")){
                        tv_store_focus.setText(R.string.collection);
                        tv_store_focus.setBackgroundResource(R.drawable.btn_radius_blue_bg);
                        tv_store_focus.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.mipmap.shop_icon_add),null,null,null);
                    }else{
                        tv_store_focus.setText(R.string.cancel_collection);
                        tv_store_focus.setBackgroundResource(R.drawable.store_collection);
                        tv_store_focus.setCompoundDrawablesWithIntrinsicBounds(null,null,null,null);
                    }

            }
        }
    };

    @Override
    protected int getLayout() {
        return R.layout.activity_store;
    }

    @Override
    protected void initView() {
        appBarLayout.addOnOffsetChangedListener(this);
        mBadgeView = new BadgeView(this,iv_message,10,4);
        EventBus.getDefault().register(this);
        store_id=getIntent().getExtras().getString("store_id");
        if(store_id!=null){
            getinit(store_id);
        }
        click();
        showUnreadMsgNum();

    }

    private void showTablist(int[] tabs,String store_id){

        for (int i = 0; i < tabs.length; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(tabs[i]));
            TabLayoutUtils.reflex(tabLayout,30);
            switch (i) {
                case TAB_STORE_INDEX:
                    fragments.add(Fragment_storehome.newInstance(store_id));
                    break;
                case TAB_GOODS_INDEX:
                    fragments.add(Fragment_allgoods.newInstance(store_id));
                    break;
                case TAB_NEWSTORE_INDEX:
                    fragments.add(Fragment_profile.newInstance(store_id));
                    break;
                default:
                    fragments.add(Fragment_storehome.newInstance(store_id));
                    break;
            }
        }
        viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager(), fragments));
        viewPager.setCurrentItem(TAB_STORE_INDEX);//要设置到viewpager.setAdapter后才起作用
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setVerticalScrollbarPosition(TAB_STORE_INDEX);
        //tlTabs.setupWithViewPager方法内部会remove所有的tabs，这里重新设置一遍tabs的text，否则tabs的text不显示
        for (int i = 0; i < tabs.length; i++) {
            tabLayout.getTabAt(i).setText(tabs[i]);
        }
    }

    private void showUnreadMsgNum() {
        mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
    }

    private void getinit(final String store_id){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_STORE_CONTENT())
                .addParam("store_id",store_id)
                .withPOST(new NetCallBack<StoreData>() {

                    @NotNull
                    @Override
                    public Class<StoreData> getRealType() {
                        return StoreData.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        error_retry_view.setVisibility(View.VISIBLE);
                        appBarLayout.setVisibility(View.GONE);
                        viewPager.setVisibility(View.GONE);
                        relative2.setVisibility(View.GONE);
                    }

                    @Override
                    public void onSuccess(@NonNull StoreData storeData) {
                        if(isFinishing()){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        appBarLayout.setVisibility(View.VISIBLE);
                        viewPager.setVisibility(View.VISIBLE);
                        relative2.setVisibility(View.VISIBLE);
                        if(!TextUtils.isEmpty(storeData.getStore().getStore_id())){
                            storeBean=storeData.getStore();
                            handler.sendEmptyMessage(0);
                        }

                    }
                },false);
    }

    //店铺加关注,取消关注
    private void setfocus(String type){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_FAVORITE())
                .addParam("fid",store_id)
                .addParam("type",type)
                .addParam("fav_type","2")
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        ToastU.INSTANCE.showToast(getString(R.string.not_network));
                    }

                    @Override
                    public void onSuccess(@NonNull String string) {
                        if(isFinishing()){
                            return;
                        }
                        if(isfocus.equals("0")){
                            isfocus="1";
                            DialogUtils.createTipImageAndTextDialog(StoreActivity.this,getString(R.string.successful),R.mipmap.png_icon_popup_ok);
                        }else {
                            isfocus="0";
                            DialogUtils.createTipImageAndTextDialog(StoreActivity.this,getString(R.string.cancel),R.mipmap.png_icon_popup_ok);
                        }
                        handler.sendEmptyMessage(1);
                    }
                },false);
    }

    private void click(){
        iv_back_err.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StoreActivity.this.finish();
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getinit(store_id);
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StoreActivity.this.finish();
            }
        });

        tv_store_focus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.Companion.getInstance().isLogin()){
                    if (!isfocus.equals("0")) {
                        setfocus("1");
                    } else
                        setfocus("0");
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });

        linear_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("store_id",store_id);
                goToActivity(StoreClassificationActivity.class,bundle);
            }
        });

        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("store_id",store_id);
                bundle.putString("stc_id","");
                bundle.putString("keyword","");
                goToActivity(StoreTypelistActivity.class,bundle);
            }
        });
        iv_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Login.Companion.getInstance().isLogin()){
                    goToActivity(MessageActivity.class);
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
        linear_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.Companion.getInstance().isLogin()){
                    if(storeBean!=null && storeBean.getDefault_im()!=null){
                        Bundle bundle = new Bundle();
                        bundle.putString(ChatActivity.CHAT_ID,storeBean.getDefault_im());
                        bundle.putString(ChatActivity.STORE_AVATAR,storeBean.getStore_avatar());
                        bundle.putString(ChatActivity.STORE_NAME,storeBean.getStore_name());
                        try {
                            bundle.putLong(ChatActivity.STORE_ID, Long.parseLong(storeBean.getStore_id()));
                        }catch (Exception e){
                            bundle.putLong(ChatActivity.STORE_ID, 0);
                        }
                        goToActivity(ChatActivity.class,bundle);
                    }else {
                        ToastU.INSTANCE.showToast(getString(R.string.no_customer_service));
                    }
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                float i=(210+verticalOffset)/210.0f;
                Log.i("info","--------ver"+verticalOffset+"--"+i);
                image_avatar.setAlpha(i);
                tv_store_focus.setAlpha(i);
            }
        });


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.STORE_UP)){
            relative2.setVisibility(View.GONE);
        }else if(messageEvent.getMessage().equals(MessageEvent.STORE_DOWN)){
            relative2.setVisibility(View.VISIBLE);
        }

        if(messageEvent.getMessage().equals(MessageEvent.UNREAD_MSG_NUM)){
            showUnreadMsgNum();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        appBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        appBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        float i=(210+verticalOffset)/210.0f;
        Log.i("info","--------ver"+verticalOffset+"--"+i);
        image_avatar.setAlpha(i);
        tv_store_focus.setAlpha(i);
    }
}

