package com.xht.kuaiyouyi.ui.enterprise.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.enterprise.Constant;
import com.xht.kuaiyouyi.ui.enterprise.adapter.MemberListEnterpriseAdapter;
import com.xht.kuaiyouyi.ui.enterprise.bean.MemberListBean;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class MemberListEnterpriseActivity extends BaseActivity {
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_right)
    TextView tv_right;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.lv_enterprise_member_list)
    ListView lv_enterprise_member_list;
    @BindView(R.id.srl_refresh)
    SmartRefreshLayout srl_refresh;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;

    private String mCompanyId;
    private String mKyy="";
    private MemberListEnterpriseAdapter mAdapter;
    private MemberListBean mData;

    @Override
    protected int getLayout() {
        return R.layout.activity_enterprise_member_list;
    }

    @Override
    protected void initView() {
        iv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_title.setText(R.string.enterprise_member);
        tv_right.setVisibility(View.GONE);
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Constant.COMPANY_ID,mCompanyId);
                goToActivity(InviteMemberEnterpriseActivity.class,bundle);
            }
        });
        mCompanyId = getIntent().getStringExtra(Constant.COMPANY_ID);
        requestMemberList(mCompanyId);

        srl_refresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                requestMemberList(mCompanyId);
            }
        });
        srl_refresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                mKyy="";
                requestMemberList(mCompanyId);
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestMemberList(mCompanyId);
            }
        });
    }

    private void requestMemberList(String mCompanyId) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_EP_COMPANY_USERS())
                .addParam("company_id",mCompanyId)
                .withLoadPOST(new NetCallBack<MemberListBean>() {
                    @NotNull
                    @Override
                    public Class<MemberListBean> getRealType() {
                        return MemberListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        if(!TextUtils.isEmpty(mKyy)){
                            srl_refresh.finishLoadMore();
                            Toast.makeText(MemberListEnterpriseActivity.this,getString(R.string.not_network),Toast.LENGTH_SHORT).show();
                        }else {
                            error_retry_view.setVisibility(View.VISIBLE);
                        }
                        if(srl_refresh.getState() == RefreshState.Refreshing){
                            srl_refresh.finishRefresh();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull MemberListBean memberListBean) {
                        if(isFinishing()){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        if(TextUtils.isEmpty(mKyy)){
                            if(srl_refresh.getState() == RefreshState.Refreshing){
                                srl_refresh.finishRefresh();
                            }
                            srl_refresh.setNoMoreData(false);
                            if(memberListBean.getCompany()!=null){
                                if(memberListBean.getIs_invite()==1){
                                    tv_right.setVisibility(View.VISIBLE);
                                }else {
                                    tv_right.setVisibility(View.GONE);
                                }
                                mData = memberListBean;
                                mAdapter = new MemberListEnterpriseAdapter(MemberListEnterpriseActivity.this,mData);
                                lv_enterprise_member_list.setAdapter(mAdapter);
                                srl_refresh.setEnableLoadMore(true);
                                mKyy = memberListBean.getKyy();
                            }
                        }else {
                            srl_refresh.finishLoadMore();
                            if(memberListBean.getCompany()!=null){
                                if(memberListBean.getCompany().size()>0){
                                    mData.getCompany().addAll(memberListBean.getCompany());
                                    mAdapter.notifyDataSetChanged();
                                    mKyy=memberListBean.getKyy();
                                }else {
                                    srl_refresh.setNoMoreData(true);
                                }
                            }
                        }
                    }
                },false,mKyy);
    }
}
