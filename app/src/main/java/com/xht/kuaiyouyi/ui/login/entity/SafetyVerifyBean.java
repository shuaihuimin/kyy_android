package com.xht.kuaiyouyi.ui.login.entity;

public class SafetyVerifyBean {

    /**
     * err_msg : 短信验证码已发送，请输入短信验证码
     */

    private String err_msg;

    public String getErr_msg() {
        return err_msg;
    }

    public void setErr_msg(String err_msg) {
        this.err_msg = err_msg;
    }
}
