package com.xht.kuaiyouyi.ui.login.activity;

import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.login.LoginUtil;
import com.xht.kuaiyouyi.ui.login.entity.LoginBean;
import com.xht.kuaiyouyi.ui.login.entity.SafetyVerifyBean;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import butterknife.BindView;


/**
 * 身份验证动态验证码页面
 * (新设备登录,修改手机号码(新手机验证)）
 *
 *
 */
public class InequipmentActivity extends BaseActivity{
    @BindView(R.id.tv_title)
    TextView title;
    @BindView(R.id.iv_back)
    ImageView back_image;
    @BindView(R.id.inequipment_code)
    EditText clearEditText;
    @BindView(R.id.btn_ck)
    Button btn_next;
    @BindView(R.id.tv_count_down)
    TextView tv_count_down;
    @BindView(R.id.tv_regain)
    TextView tv_regain;
    @BindView(R.id.tv_phone)
    TextView tv_phone;

    private String mPhone;
    private String mAreaCode;
    private String mPassword;
    private int mCountDown = 60;//倒计时60s开始

    private String mType;

    private android.os.Handler mHandler = new android.os.Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(Message msg) {
            mCountDown = mCountDown-1;
            if(mCountDown>0){
                tv_count_down.setText(mCountDown + "s");
                tv_count_down.setVisibility(View.VISIBLE);
                tv_regain.setTextColor(getResources().getColor(R.color.grey_7));
                tv_regain.setEnabled(false);
                mHandler.sendEmptyMessageDelayed(1,1000);
            }else {
                tv_count_down.setVisibility(View.GONE);
                tv_regain.setEnabled(true);
                tv_regain.setTextColor(getResources().getColor(R.color.blue_normal));
            }
        }
    };
    @Override
    protected int getLayout() {
        return R.layout.activity_inequipment;
    }

    @Override
    protected void initView() {
        mType=getIntent().getStringExtra(Contant.TYPE);
        switch (mType){
            case Contant.TYPE_CHANGE_PHONE:
                mPhone=getIntent().getStringExtra(Contant.PHONE);
                mAreaCode=getIntent().getStringExtra(Contant.AREA_CODE);
                title.setText(R.string.verify_phone);
                break;
            case Contant.TYPE_CHANGE_DEVICE:
                mPhone=getIntent().getStringExtra(Contant.PHONE);
                mAreaCode=getIntent().getStringExtra(Contant.AREA_CODE);
                mPassword=getIntent().getStringExtra(Contant.PASSWORD);
                title.setText(R.string.verify_identity);
                back_image.setImageResource(R.mipmap.login_icon_x_nor);
                break;
        }

        tv_phone.setText(Utils.getDisplayPhone(mPhone));
        tv_count_down.setText(mCountDown + "s");
        mHandler.sendEmptyMessageDelayed(1,1000);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captcha(mPhone,clearEditText.getText().toString().trim());
            }
        });
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        clearEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0){
                    btn_next.setEnabled(true);
                }else {
                    btn_next.setEnabled(false);
                }
            }
        });
        tv_regain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mType.equals(Contant.TYPE_CHANGE_PHONE)){
                    requestSMS();
                }
                if(mType.equals(Contant.TYPE_CHANGE_DEVICE)){
                    requestVerifyCode();
                }
            }
        });

    }

    //验证动态验证码
    private void captcha(final String phone, final String cet_code_s){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CONNECT_CHECK_SMS_CAPTCHA())
                .addParam("phone",phone)
                .addParam("captcha",cet_code_s)
                .addParam("type",mType)
                .addParam("area_code",mAreaCode)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(InequipmentActivity.this,err,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        if(isFinishing()){
                            return;
                        }
                        if(mType.equals(Contant.TYPE_CHANGE_DEVICE)){
                            requestLogin(cet_code_s);
                        }
                        if(mType.equals(Contant.TYPE_CHANGE_PHONE)){
                            DialogUtils.createTipImageAndTextDialog(InequipmentActivity.this
                                    ,getString(R.string.change_phone_successful),R.mipmap.png_icon_popup_ok);
                            Login.Companion.getInstance().setPhone(phone);
                            title.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            },1000);
                        }

                    }
                },false);

    }

    /**
     * 在新设备登录才会用到
     * @param cet_code_s 验证码
     */
    private void requestLogin(String cet_code_s) {
        DialogUtils.createTipAllLoadDialog(InequipmentActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGIN())
                .addParam("username", mPhone)
                .addParam("password", mPassword)
                .addParam("client", "android")
                .addParam("key_code", cet_code_s)
                .addParam("l_l_p_type", Utils.isHUAWEI()?"1":"")
                .addParam("l_l_p_token", Login.Companion.getInstance().getHuawei_push_token())
                .withPOST(new NetCallBack<LoginBean>() {

                    @NotNull
                    @Override
                    public Class<LoginBean> getRealType() {
                        return LoginBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(InequipmentActivity.this,err,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull LoginBean loginBean) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        LoginUtil.loginSaveDataAndInit(getApplicationContext(),loginBean,mPhone);
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.EVENT_ClOSE_LOGINACTIVITY));
                        finish();
                    }
                }, false);

    }

    /**
     * 验证安全设备的验证码请求
     */
    private void requestVerifyCode() {
        DialogUtils.createTipAllLoadDialog(InequipmentActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_SAFETY_VERIFY())
                .addParam("member_mobile", mPhone)
                .addParam("mobile_zone", mAreaCode)
                .withPOST(new NetCallBack<SafetyVerifyBean>() {
                    @NotNull
                    @Override
                    public Class<SafetyVerifyBean> getRealType() {
                        return SafetyVerifyBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(InequipmentActivity.this,err,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull SafetyVerifyBean safetyVerifyBean) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(InequipmentActivity.this,safetyVerifyBean.getErr_msg(),
                                Toast.LENGTH_SHORT).show();
                        mCountDown = 60;
                        mHandler.sendEmptyMessageDelayed(1,1000);
                    }
                }, false);
    }

    /**
     * 修改手机新手机的验证码请求
     */
    private void requestSMS(){
        DialogUtils.createTipAllLoadDialog(InequipmentActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CONNECT_GET_SMS_CAPTCHA())
                .addParam("phone", mPhone)
                .addParam("type", mType)
                .addParam("area_code", mAreaCode)
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(InequipmentActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(getApplicationContext(),R.string.verify_code_sended,Toast.LENGTH_SHORT).show();
                        mCountDown = 60;
                        mHandler.sendEmptyMessageDelayed(1,1000);
                    }
                }, false);
    }

    @Override
    protected void onDestroy() {
        if(mHandler.hasMessages(1)){
            mHandler.removeMessages(1);
        }
        mHandler = null;
        super.onDestroy();
    }
}
