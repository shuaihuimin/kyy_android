package com.xht.kuaiyouyi.ui.goods.entity;

public class StoreData {

        /**
         * store : {"store_id":"1","store_name":"快优易自营","grade_id":"0","store_collect":"4","store_credit":{"store_desccredit":{"text":"描述","credit":"5.0"},"store_servicecredit":{"text":"服务","credit":"5.0"},"store_deliverycredit":{"text":"物流","credit":"5.0"}},"store_domain":null,"store_phone":"","store_avatar":"http://192.168.0.2/data/upload/shop/store/05773858668529672_sm.jpg","store_label":"http://192.168.0.2/data/upload/shop/common/default_store_avatar.png","store_banner":"http://192.168.0.2/data/upload/shop/common/default_store_avatar.png","mb_title_img":"http://192.168.0.2/data/upload/shop/common/default_store_avatar.png","is_own_shop":"1","store_workingtime":null,"store_sales":"3"}
         */

        private StoreBean store;

        public StoreBean getStore() {
            return store;
        }

        public void setStore(StoreBean store) {
            this.store = store;
        }

        public static class StoreBean {
            /**
             * store_id : 1
             * store_name : 快优易自营
             * grade_id : 0
             * store_collect : 4
             * store_credit : {"store_desccredit":{"text":"描述","credit":"5.0"},"store_servicecredit":{"text":"服务","credit":"5.0"},"store_deliverycredit":{"text":"物流","credit":"5.0"}}
             * store_domain : null
             * store_phone :
             * store_avatar : http://192.168.0.2/data/upload/shop/store/05773858668529672_sm.jpg
             * store_label : http://192.168.0.2/data/upload/shop/common/default_store_avatar.png
             * store_banner : http://192.168.0.2/data/upload/shop/common/default_store_avatar.png
             * mb_title_img : http://192.168.0.2/data/upload/shop/common/default_store_avatar.png
             * is_own_shop : 1
             * store_workingtime : null
             * store_sales : 3
             */
            private String is_fav;
            private String store_id;
            private String store_name;
            private String grade_id;
            private String store_collect;
            private StoreCreditBean store_credit;
            private Object store_domain;
            private String store_phone;
            private String store_avatar;
            private String store_label;
            private String store_banner;
            private String mb_title_img;
            private String default_im;
            private String default_avatar;
            private String is_own_shop;
            private Object store_workingtime;
            private String store_sales;

            public String getDefault_im() {
                return default_im;
            }

            public void setDefault_im(String default_im) {
                this.default_im = default_im;
            }

            public String getDefault_avatar() {
                return default_avatar;
            }

            public void setDefault_avatar(String default_avatar) {
                this.default_avatar = default_avatar;
            }

            public String getIs_fav() {
                return is_fav;
            }

            public void setIs_fav(String is_fav) {
                this.is_fav = is_fav;
            }

            public String getStore_id() {
                return store_id;
            }

            public void setStore_id(String store_id) {
                this.store_id = store_id;
            }

            public String getStore_name() {
                return store_name;
            }

            public void setStore_name(String store_name) {
                this.store_name = store_name;
            }

            public String getGrade_id() {
                return grade_id;
            }

            public void setGrade_id(String grade_id) {
                this.grade_id = grade_id;
            }

            public String getStore_collect() {
                return store_collect;
            }

            public void setStore_collect(String store_collect) {
                this.store_collect = store_collect;
            }

            public StoreCreditBean getStore_credit() {
                return store_credit;
            }

            public void setStore_credit(StoreCreditBean store_credit) {
                this.store_credit = store_credit;
            }

            public Object getStore_domain() {
                return store_domain;
            }

            public void setStore_domain(Object store_domain) {
                this.store_domain = store_domain;
            }

            public String getStore_phone() {
                return store_phone;
            }

            public void setStore_phone(String store_phone) {
                this.store_phone = store_phone;
            }

            public String getStore_avatar() {
                return store_avatar;
            }

            public void setStore_avatar(String store_avatar) {
                this.store_avatar = store_avatar;
            }

            public String getStore_label() {
                return store_label;
            }

            public void setStore_label(String store_label) {
                this.store_label = store_label;
            }

            public String getStore_banner() {
                return store_banner;
            }

            public void setStore_banner(String store_banner) {
                this.store_banner = store_banner;
            }

            public String getMb_title_img() {
                return mb_title_img;
            }

            public void setMb_title_img(String mb_title_img) {
                this.mb_title_img = mb_title_img;
            }

            public String getIs_own_shop() {
                return is_own_shop;
            }

            public void setIs_own_shop(String is_own_shop) {
                this.is_own_shop = is_own_shop;
            }

            public Object getStore_workingtime() {
                return store_workingtime;
            }

            public void setStore_workingtime(Object store_workingtime) {
                this.store_workingtime = store_workingtime;
            }

            public String getStore_sales() {
                return store_sales;
            }

            public void setStore_sales(String store_sales) {
                this.store_sales = store_sales;
            }

            public static class StoreCreditBean {
                /**
                 * store_desccredit : {"text":"描述","credit":"5.0"}
                 * store_servicecredit : {"text":"服务","credit":"5.0"}
                 * store_deliverycredit : {"text":"物流","credit":"5.0"}
                 */

                private StoreDesccreditBean store_desccredit;
                private StoreServicecreditBean store_servicecredit;
                private StoreDeliverycreditBean store_deliverycredit;

                public StoreDesccreditBean getStore_desccredit() {
                    return store_desccredit;
                }

                public void setStore_desccredit(StoreDesccreditBean store_desccredit) {
                    this.store_desccredit = store_desccredit;
                }

                public StoreServicecreditBean getStore_servicecredit() {
                    return store_servicecredit;
                }

                public void setStore_servicecredit(StoreServicecreditBean store_servicecredit) {
                    this.store_servicecredit = store_servicecredit;
                }

                public StoreDeliverycreditBean getStore_deliverycredit() {
                    return store_deliverycredit;
                }

                public void setStore_deliverycredit(StoreDeliverycreditBean store_deliverycredit) {
                    this.store_deliverycredit = store_deliverycredit;
                }

                public static class StoreDesccreditBean {
                    /**
                     * text : 描述
                     * credit : 5.0
                     */

                    private String text;
                    private String credit;

                    public String getText() {
                        return text;
                    }

                    public void setText(String text) {
                        this.text = text;
                    }

                    public String getCredit() {
                        return credit;
                    }

                    public void setCredit(String credit) {
                        this.credit = credit;
                    }
                }

                public static class StoreServicecreditBean {
                    /**
                     * text : 服务
                     * credit : 5.0
                     */

                    private String text;
                    private String credit;

                    public String getText() {
                        return text;
                    }

                    public void setText(String text) {
                        this.text = text;
                    }

                    public String getCredit() {
                        return credit;
                    }

                    public void setCredit(String credit) {
                        this.credit = credit;
                    }
                }

                public static class StoreDeliverycreditBean {
                    /**
                     * text : 物流
                     * credit : 5.0
                     */

                    private String text;
                    private String credit;

                    public String getText() {
                        return text;
                    }

                    public void setText(String text) {
                        this.text = text;
                    }

                    public String getCredit() {
                        return credit;
                    }

                    public void setCredit(String credit) {
                        this.credit = credit;
                    }
                }
            }
        }
}
