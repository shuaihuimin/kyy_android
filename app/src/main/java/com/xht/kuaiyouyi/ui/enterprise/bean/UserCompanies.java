package com.xht.kuaiyouyi.ui.enterprise.bean;

import java.util.List;

public class UserCompanies {


    /**
     * companies : [{"company_id":"81","company_name":"天天向上","company_confirm_status":"3","message_count":0},{"company_id":"77","company_name":"路易威登","company_confirm_status":"3","message_count":0},{"company_id":"60","company_name":"新海通科技有限公司1","company_confirm_status":"3","message_count":1}]
     * kyy : eyJLIjoia3l5XzAxNCIsIk8iOiIxIiwiUCI6MX0=
     */

    private List<CompaniesBean> companies;

    public List<CompaniesBean> getCompanies() {
        return companies;
    }

    public void setCompanies(List<CompaniesBean> companies) {
        this.companies = companies;
    }

    public static class CompaniesBean {
        /**
         * company_id : 81
         * company_name : 天天向上
         * company_confirm_status : 3
         * message_count : 0
         */

        private String company_id;
        private int company_status;
        private String company_name;
        private String company_confirm_status;
        private int message_count;

        public String getCompany_id() {
            return company_id;
        }

        public void setCompany_id(String company_id) {
            this.company_id = company_id;
        }

        public int getCompany_status() {
            return company_status;
        }

        public void setCompany_status(int company_status) {
            this.company_status = company_status;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public String getCompany_confirm_status() {
            return company_confirm_status;
        }

        public void setCompany_confirm_status(String company_confirm_status) {
            this.company_confirm_status = company_confirm_status;
        }

        public int getMessage_count() {
            return message_count;
        }

        public void setMessage_count(int message_count) {
            this.message_count = message_count;
        }
    }
}
