package com.xht.kuaiyouyi.ui.home.entity;

import java.util.List;

/**
 * Created by shuaihuimin on 2018/6/14.
 */

public class Recyclerhome {

    public static class DataBean {
        /**
         * lunbo_list : {"item":[{"image":"http://192.168.0.2/data/upload/shop/editor/web-101-101-1.jpg?454","source_id":"0","adv_type":"2"},{"image":"http://192.168.0.2/data/upload/shop/editor/web-101-101-5.jpg?166","source_id":"0","adv_type":"2"},{"image":"http://192.168.0.2/data/upload/shop/editor/web-101-101-2.jpg?331","source_id":"0","adv_type":"2"},{"image":"http://192.168.0.2/data/upload/shop/editor/web-101-101-3.jpg?249","source_id":"0","adv_type":"2"},{"image":"http://192.168.0.2/data/upload/shop/editor/web-101-101-1.jpg?454","source_id":"0","adv_type":"2"}]}
         * sort_list : {"item":[{"gc_id":"1","cn_adv1":"http://127.0.0.1/data/upload/shop/goods_class/04849383096194771.jpg"},{"gc_id":"2","cn_adv1":"http://127.0.0.1/data/upload/shop/goods_class/04849386427434184.jpg"},{"gc_id":"3","cn_adv1":"http://127.0.0.1/data/upload/shop/goods_class/04849387912371887.jpg"},{"gc_id":"256","cn_adv1":"http://127.0.0.1/data/upload/shop/goods_class/04849389240497918.jpg"},{"gc_id":"308","cn_adv1":"http://127.0.0.1/data/upload/shop/goods_class/04849391321812920.jpg"},{"gc_id":"470","cn_adv1":"http://127.0.0.1/data/upload/shop/goods_class/04849392722316549.jpg"}]}
         * brand_c_list : {"item":[{"brand_id":"285","brand_name":"雀巢","brand_pic":"http://127.0.0.1/data/upload/shop/brand/04399881950170970_sm.jpg"},{"brand_id":"267","brand_name":"惠氏","brand_pic":"http://127.0.0.1/data/upload/shop/brand/04399878077210018_sm.jpg"},{"brand_id":"264","brand_name":"凯镛","brand_pic":"http://127.0.0.1/data/upload/shop/brand/04399857579422195_sm.jpg"},{"brand_id":"263","brand_name":"长城葡萄酒","brand_pic":"http://127.0.0.1/data/upload/shop/brand/04399857399887704_sm.jpg"},{"brand_id":"262","brand_name":"善存","brand_pic":"http://127.0.0.1/data/upload/shop/brand/04399857246559825_sm.jpg"},{"brand_id":"261","brand_name":"自然之宝","brand_pic":"http://127.0.0.1/data/upload/shop/brand/04399857092677752_sm.jpg"},{"brand_id":"260","brand_name":"新西兰十一坊","brand_pic":"http://127.0.0.1/data/upload/shop/brand/04399856948519746_sm.jpg"},{"brand_id":"259","brand_name":"Lumi","brand_pic":"http://127.0.0.1/data/upload/shop/brand/04399856804968818_sm.jpg"},{"brand_id":"258","brand_name":"白兰氏","brand_pic":"http://127.0.0.1/data/upload/shop/brand/04399856638765013_sm.jpg"}]}
         * goods_list : {"item":[{"goods_id":"100000","goods_name":"劳力士ROLEX-潜航者型 116610-LV-97200自动机械钢带男表联保正品","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627678636481.jpg","goods_price":"70000.00","goods_salenum":"3"},{"goods_id":"100001","goods_name":"劳力士Rolex 深海系列 自动机械钢带男士表 联保正品116660 98210","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627707766698.png","goods_price":"87500.00","goods_salenum":"0"},{"goods_id":"100002","goods_name":"劳力士Rolex MILGAUSS 116400GV-72400 自动机械钢带男表联保正品","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627750479728.png","goods_price":"63200.00","goods_salenum":"1"},{"goods_id":"100003","goods_name":"劳力士Rolex 日志型系列 自动机械钢带男士表 联保正品 116333","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627769865296.jpg","goods_price":"89200.00","goods_salenum":"0"},{"goods_id":"100004","goods_name":"劳力士Rolex 日志型系列 自动机械钢带男表 联保正品 116233","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627799921979.jpg","goods_price":"97800.00","goods_salenum":"0"},{"goods_id":"100005","goods_name":"劳力士Rolex 蚝式恒动 115234-CA-72190自动机械钢带男表联保正品","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627843241680.jpg","goods_price":"65900.00","goods_salenum":"0"},{"goods_id":"100006","goods_name":"劳力士Rolex 蚝式恒动系列 自动机械钢带男表 正品116231-G-63201","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627871532105.png","goods_price":"100500.00","goods_salenum":"0"},{"goods_id":"100007","goods_name":"劳力士Rolex 蚝式恒动系列自动机械钢带男表正品116523-8DI-78593","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627900055146.png","goods_price":"146300.00","goods_salenum":"0"},{"goods_id":"100008","goods_name":"劳力士Rolex 宇宙计型迪通拿 自动机械皮带男表 正品116519 CR.TB","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627931531971.jpg","goods_price":"209500.00","goods_salenum":"0"},{"goods_id":"100009","goods_name":"劳力士Rolex 日志型系列 116200 63200 自动机械钢带男表联保正品","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627958339099.jpg","goods_price":"52800.00","goods_salenum":"0"}]}
         */

        private LunboListBean lunbo_list;
        private SortListBean sort_list;
        private BrandCListBean brand_c_list;
        private GoodsListBean goods_list;

        public LunboListBean getLunbo_list() {
            return lunbo_list;
        }

        public void setLunbo_list(LunboListBean lunbo_list) {
            this.lunbo_list = lunbo_list;
        }

        public SortListBean getSort_list() {
            return sort_list;
        }

        public void setSort_list(SortListBean sort_list) {
            this.sort_list = sort_list;
        }

        public BrandCListBean getBrand_c_list() {
            return brand_c_list;
        }

        public void setBrand_c_list(BrandCListBean brand_c_list) {
            this.brand_c_list = brand_c_list;
        }

        public GoodsListBean getGoods_list() {
            return goods_list;
        }

        public void setGoods_list(GoodsListBean goods_list) {
            this.goods_list = goods_list;
        }

        public static class LunboListBean {
            private List<ItemBean> item;

            public List<ItemBean> getItem() {
                return item;
            }

            public void setItem(List<ItemBean> item) {
                this.item = item;
            }

            public static class ItemBean {
                /**
                 * image : http://192.168.0.2/data/upload/shop/editor/web-101-101-1.jpg?454
                 * source_id : 0
                 * adv_type : 2
                 */

                private String image;
                private String source_id;
                private String adv_type;

                public String getImage() {
                    return image;
                }

                public void setImage(String image) {
                    this.image = image;
                }

                public String getSource_id() {
                    return source_id;
                }

                public void setSource_id(String source_id) {
                    this.source_id = source_id;
                }

                public String getAdv_type() {
                    return adv_type;
                }

                public void setAdv_type(String adv_type) {
                    this.adv_type = adv_type;
                }
            }
        }

        public static class SortListBean {
            private List<ItemBeanX> item;

            public List<ItemBeanX> getItem() {
                return item;
            }

            public void setItem(List<ItemBeanX> item) {
                this.item = item;
            }

            public static class ItemBeanX {
                /**
                 * gc_id : 1
                 * cn_adv1 : http://127.0.0.1/data/upload/shop/goods_class/04849383096194771.jpg
                 */

                private String gc_id;
                private String cn_adv1;

                public String getGc_id() {
                    return gc_id;
                }

                public void setGc_id(String gc_id) {
                    this.gc_id = gc_id;
                }

                public String getCn_adv1() {
                    return cn_adv1;
                }

                public void setCn_adv1(String cn_adv1) {
                    this.cn_adv1 = cn_adv1;
                }
            }
        }

        public static class BrandCListBean {
            private List<ItemBeanXX> item;

            public List<ItemBeanXX> getItem() {
                return item;
            }

            public void setItem(List<ItemBeanXX> item) {
                this.item = item;
            }

            public static class ItemBeanXX {
                /**
                 * brand_id : 285
                 * brand_name : 雀巢
                 * brand_pic : http://127.0.0.1/data/upload/shop/brand/04399881950170970_sm.jpg
                 */

                private String brand_id;
                private String brand_name;
                private String brand_pic;

                public String getBrand_id() {
                    return brand_id;
                }

                public void setBrand_id(String brand_id) {
                    this.brand_id = brand_id;
                }

                public String getBrand_name() {
                    return brand_name;
                }

                public void setBrand_name(String brand_name) {
                    this.brand_name = brand_name;
                }

                public String getBrand_pic() {
                    return brand_pic;
                }

                public void setBrand_pic(String brand_pic) {
                    this.brand_pic = brand_pic;
                }
            }
        }

        public static class GoodsListBean {
            private List<ItemBeanXXX> item;

            public List<ItemBeanXXX> getItem() {
                return item;
            }

            public void setItem(List<ItemBeanXXX> item) {
                this.item = item;
            }

            public static class ItemBeanXXX {
                /**
                 * goods_id : 100000
                 * goods_name : 劳力士ROLEX-潜航者型 116610-LV-97200自动机械钢带男表联保正品
                 * goods_image : http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627678636481.jpg
                 * goods_price : 70000.00
                 * goods_salenum : 3
                 */

                private String goods_id;
                private String goods_name;
                private String goods_image;
                private String goods_price;
                private String goods_salenum;

                public String getGoods_id() {
                    return goods_id;
                }

                public void setGoods_id(String goods_id) {
                    this.goods_id = goods_id;
                }

                public String getGoods_name() {
                    return goods_name;
                }

                public void setGoods_name(String goods_name) {
                    this.goods_name = goods_name;
                }

                public String getGoods_image() {
                    return goods_image;
                }

                public void setGoods_image(String goods_image) {
                    this.goods_image = goods_image;
                }

                public String getGoods_price() {
                    return goods_price;
                }

                public void setGoods_price(String goods_price) {
                    this.goods_price = goods_price;
                }

                public String getGoods_salenum() {
                    return goods_salenum;
                }

                public void setGoods_salenum(String goods_salenum) {
                    this.goods_salenum = goods_salenum;
                }
            }
        }
    }
}
