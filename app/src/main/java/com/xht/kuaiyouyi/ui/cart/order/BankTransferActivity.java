package com.xht.kuaiyouyi.ui.cart.order;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.model.TResult;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.cart.entity.OrderDetilsBean;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.SignUtil;
import com.xht.kuaiyouyi.utils.SystemUtil;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.ButtomDialog;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BankTransferActivity extends TakePhotoActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_click_updata)
    TextView tv_click_updata;
    @BindView(R.id.iv_check_pictures)
    ImageView iv_check_pictures;
    @BindView(R.id.tv_account_detils)
    TextView tv_account_detils;
    @BindView(R.id.tv_account_name)
    TextView tv_account_name;
    @BindView(R.id.tv_bank_account_num)
    TextView tv_bank_account_num;
    @BindView(R.id.tv_transfer_amount)
    TextView tv_transfer_amount;
    @BindView(R.id.tv_transfer_amount_state)
    TextView tv_transfer_amount_state;
    @BindView(R.id.tv_pay_currency)
    TextView tv_pay_currency;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.tv_transfer_date)
    TextView tv_transfer_date;
    @BindView(R.id.tv_roll_bank)
    TextView tv_roll_bank;
    @BindView(R.id.edit_transfer_name)
    EditText edit_transfer_name;
    @BindView(R.id.edit_transaction_number)
    EditText edit_transaction_number;

    private int mYear;
    private int mMonth;
    private int mDay;
    private String delivery_time;

    private ButtomDialog mButtomDialog;
    private Uri mImageUri;
    private String uploadIconPath;
    private String transfer_img;
    private OrderDetilsBean orderDetilsBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(getLayout());
        ButterKnife.bind(this);
        initView();
    }

    protected int getLayout() {
        return R.layout.activity_banktransfer;
    }

    protected void initView() {
        tv_title.setText(getString(R.string.information_transfer));
        orderDetilsBean= JSONObject.parseObject(getIntent().getStringExtra("order_detils"),OrderDetilsBean.class);
        tv_account_detils.setText(orderDetilsBean.getPay_info().getBank_currency().getAccount_name());
        tv_account_name.setText(orderDetilsBean.getPay_info().getBank_currency().getAccount_bank());
        tv_bank_account_num.setText(orderDetilsBean.getPay_info().getBank_currency().getAccount());
        tv_pay_currency.setText(orderDetilsBean.getPay_info().getBank_currency().getName());
        tv_roll_bank.setText(getString(R.string.roll_bank)+orderDetilsBean.getPay_info().getBank_item().getName());
        if(orderDetilsBean.getGoods().getOrder_main_deal_active().equals("1")){
            tv_transfer_amount.setText(orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                    + Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_amount())*
                    Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate())));
        }else if(orderDetilsBean.getGoods().getOrder_main_deal_active().equals("2")){
            if(orderDetilsBean.getGoods().getOrder_main_state().equals("2010")){
                tv_transfer_amount.setText(orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                        + Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_one())*
                        Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate())));
                tv_transfer_amount_state.setText(getString(R.string.frist_phase));
            }else if(orderDetilsBean.getGoods().getOrder_main_state().equals("2011")){
                tv_transfer_amount.setText(orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                        + Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_two())*
                        Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate())));
                tv_transfer_amount_state.setText(getString(R.string.second_phase));
            }
        }
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(transfer_img)){
                    DialogUtils.createTwoBtnDialog(BankTransferActivity.this,
                            getString(R.string.check_dialog),
                            getString(R.string.dialog_confirm),
                            getString(R.string.dialog_cancel),
                            new DialogUtils.OnRightBtnListener() {
                                @Override
                                public void setOnRightListener(Dialog dialog) {
                                    BankTransferActivity.this.finish();
                                }
                            },null,false,true);
                }else {
                    BankTransferActivity.this.finish();
                }
            }
        });
        iv_check_pictures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showButtonDialog();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadCheque(transfer_img);
            }
        });

        tv_transfer_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                showDatePicker();

            }
        });

    }

    @Override
    public void takeCancel() {
        super.takeCancel();
    }

    @Override
    public void takeFail(TResult result, String msg) {
        super.takeFail(result, msg);
    }

    @Override
    public void takeSuccess(TResult result) {
        super.takeSuccess(result);
        uploadIconPath=result.getImage().getOriginalPath();
        requestUploadIcon(uploadIconPath);
        Glide.with(BankTransferActivity.this).load(uploadIconPath).into(iv_check_pictures);
        tv_click_updata.setVisibility(View.GONE);
    }

    private void showButtonDialog() {
        if(mButtomDialog == null){
            View view = LayoutInflater.from(this).inflate(R.layout.dialog_buttom_three_btn,null);
            mButtomDialog = new ButtomDialog(this,view,false,true);
            final TextView tv_three = view.findViewById(R.id.tv_three);
            TextView tv_one = view.findViewById(R.id.tv_one);
            TextView tv_two = view.findViewById(R.id.tv_two);

            tv_three.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                }
            });
            tv_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),  System.currentTimeMillis() + ".jpg");
                    mImageUri = Uri.fromFile(file);
                    getTakePhoto().onPickFromCapture(mImageUri);
                    mButtomDialog.dismiss();
                }
            });
            tv_two.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),  System.currentTimeMillis() + ".jpg");
                    mImageUri = Uri.fromFile(file);
                    getTakePhoto().onPickFromGallery();
                    mButtomDialog.dismiss();
                }
            });
        }
        mButtomDialog.show();
    }

    //上传图片
    private void requestUploadIcon(final String uploadIconPath) {
        DialogUtils.createTipAllLoadDialog(this);
        String t = SystemUtil.INSTANCE.getTime(SystemUtil.INSTANCE.
                getTimeStr("" + System.currentTimeMillis()));
        Map<String,Object> map = new HashMap<>();
        map.put("device_uuid", NetUtil.Companion.getUuid());
        map.put("device_name", NetUtil.Companion.getDevice_name());
        map.put("type", "2");
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_UPLOADIMG())
                .addParam("device_uuid", NetUtil.Companion.getUuid())
                .addParam("device_name", NetUtil.Companion.getDevice_name())
                .addParam("token_id", Login.Companion.getInstance().getToken_id())
                .addParam("sign_time", t)
                .addParam("sign", SignUtil.sign(map, t))
                .addParam("type", "2")
                .addParam(new File(uploadIconPath).getName(),new File(uploadIconPath))
                .withPOSTFile(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onSuccess(@NonNull String uploadImageBean) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        try {
                            org.json.JSONObject jsonObject=new org.json.JSONObject(uploadImageBean);
                            transfer_img = jsonObject.getString("file_name_short");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(BankTransferActivity.this,err,Toast.LENGTH_SHORT).show();
                    }
                },false);
    }

    //上传信息
    private void uploadCheque(final String transfer_img){
        if(TextUtils.isEmpty(edit_transfer_name.getText())){
            Toast.makeText(BankTransferActivity.this,getString(R.string.transfer_person_company),Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(edit_transaction_number.getText())){
            Toast.makeText(BankTransferActivity.this,getString(R.string.transaction_number),Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(tv_transfer_date.getText())){
            Toast.makeText(BankTransferActivity.this,getString(R.string.transfer_date),Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(transfer_img)){
            Toast.makeText(BankTransferActivity.this,getString(R.string.transfer_dialog),Toast.LENGTH_SHORT).show();
            return;
        }
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_UPDATE_CHEQUE())
                .addParam("pay_type","1")
                .addParam("order_main_id",getIntent().getStringExtra("order_id"))
                .addParam("order_main_sn",orderDetilsBean.getGoods().getOrder_main_sn())
                .addParam("transfer_img",transfer_img)
                .addParam("transfer_time",delivery_time)
                .addParam("transfer_name",edit_transfer_name.getText().toString().trim())
                .addParam("transfer_bank_name",orderDetilsBean.getPay_info().getBank_item().getName())
                .addParam("transfer_serial_number",edit_transaction_number.getText().toString().trim())
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.createOneBtnDialog(BankTransferActivity.this, getString(R.string.zhangdan_successful)
                                , getString(R.string.know), new DialogUtils.OnLeftBtnListener() {
                                    @Override
                                    public void setOnLeftListener(Dialog dialog) {
                                        EventBus.getDefault().post(new MessageEvent(MessageEvent.ORDERDETILS));
                                        EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_ORDER_LIST));
                                        EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_APPROVAL_DETAIL));
                                        BankTransferActivity.this.finish();
                                    }
                                },false,false);
                    }
                },false);
    }

    //时间选择器
    private void showDatePicker(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(BankTransferActivity.this,R.style.MyDatePickerDialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        mYear = year;
                        mMonth = month;
                        mDay = dayOfMonth;
                        String days;
                        if (mMonth + 1 < 10) {
                            if (mDay < 10) {
                                days = new StringBuffer().append(mYear).append("年").append("0").
                                        append(mMonth + 1).append("月").append("0").append(mDay).append("日").toString();
                            } else {
                                days = new StringBuffer().append(mYear).append("年").append("0").
                                        append(mMonth + 1).append("月").append(mDay).append("日").toString();
                            }

                        } else {
                            if (mDay < 10) {
                                days = new StringBuffer().append(mYear).append("年").
                                        append(mMonth + 1).append("月").append("0").append(mDay).append("日").toString();
                            } else {
                                days = new StringBuffer().append(mYear).append("年").
                                        append(mMonth + 1).append("月").append(mDay).append("日").toString();
                            }

                        }
                        tv_transfer_date.setText(days);
                        delivery_time=Utils.data(days);
                    }
                },
                mYear, mMonth, mDay);

//        //设置起始日期和结束日期
//        DatePicker datePicker = datePickerDialog.getDatePicker();
//        datePicker.setMinDate(System.currentTimeMillis()-1000);
        datePickerDialog.updateDate(mYear,mMonth,mDay);
        datePickerDialog.show();
    }

    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config=new Configuration();
        config.setToDefaults();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocales(res.getConfiguration().getLocales());
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(res.getConfiguration().locale);
        }else {
            config.locale = res.getConfiguration().locale;
        }
        res.updateConfiguration(config,res.getDisplayMetrics() );
        return res;
    }
}
