package com.xht.kuaiyouyi.ui.mine.setting;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.utils.DialogUtils;

import butterknife.BindView;

public class UserFeedbackActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.et_user_feedback)
    EditText et_user_feedback;
    @BindView(R.id.tv_num_tip)
    TextView tv_num_tip;
    @BindView(R.id.bt_submit)
    Button bt_submit;

    @Override
    protected int getLayout() {
        return R.layout.activity_user_feedback;
    }

    @Override
    protected void initView() {
        tv_title.setText(R.string.user_feedback);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        et_user_feedback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = s.toString().length();
                if(length > 0){
                    bt_submit.setEnabled(true);
                    tv_num_tip.setText(length + "/200");
                }
                if(length == 0){
                    bt_submit.setEnabled(false);
                    tv_num_tip.setText("0/200");
                }
                if(length == 200){
                    DialogUtils.createTipAllTextDialog(UserFeedbackActivity.this,
                            getResources().getString(R.string.text_max_length_tip));
                }
            }
        });
    }

}
