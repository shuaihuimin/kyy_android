package com.xht.kuaiyouyi.ui.goods.entity;

import com.xht.kuaiyouyi.ui.search.entity.TypelistData;

import java.util.List;

public class StoregoodsBean {


        /**
         * list : [{"goods_id":"100628","goods_commonid":"100075","goods_name":"价格测试 AS","goods_jingle":"","goods_price":"10.00","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05805067837661061_360.png"},{"goods_id":"100529","goods_commonid":"100087","goods_name":"手表测试 22 55 春季","goods_jingle":"","goods_price":"12.00","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05832571255740670_360.gif"},{"goods_id":"100523","goods_commonid":"100086","goods_name":"列表测试 黑色 AS","goods_jingle":"","goods_price":"0.01","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05832565747581977_360.gif"},{"goods_id":"100516","goods_commonid":"100085","goods_name":"螺丝刀 AS 2123130","goods_jingle":"","goods_price":"12.00","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05832546260955428_360.jpg"},{"goods_id":"100510","goods_commonid":"100083","goods_name":"测试产品资料下载","goods_jingle":"","goods_price":"0.01","goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_360.gif"},{"goods_id":"100487","goods_commonid":"100073","goods_name":"普通输送 SSD350-5 350 38 1.5 182 5000*460*350","goods_jingle":"","goods_price":"555.65","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05804779414920592_360.jpg"},{"goods_id":"100430","goods_commonid":"100043","goods_name":"螺丝螺丝 大 AS 2123130","goods_jingle":"好用","goods_price":"10.00","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05797198150064774_360.jpg"},{"goods_id":"100429","goods_commonid":"100047","goods_name":"输送架","goods_jingle":"安全可靠","goods_price":"110.00","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05797226638095124_360.jpg"},{"goods_id":"100420","goods_commonid":"100045","goods_name":"测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试 大","goods_jingle":"我是广告我是广告我是广告我是广告我告我是广告我是广告我是广告我是广告我是广告我是广告我是广告我是广告我是广告我是广告我是广告我是广告我告我是广告我是广告我是广告我是广告我是广告我是广告我我","goods_price":"10.00","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05797205961743028_360.jpg"},{"goods_id":"100383","goods_commonid":"100000","goods_name":"劳力士ROLEX-潜航者型 116610-LV-97200自动机械钢带男表联保正品 红色 1.5寸 ","goods_jingle":"","goods_price":"1.00","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627678636481_360.jpg"}]
         * kyy : eyJQIjoxLCJLIjoia3l5XzAwNCIsIk8iOiItIn0=
         */

        private String kyy;
        private List<ListBean> list;
        private CurrencytypeBean currencytype;


        public String getKyy() {
            return kyy;
        }

        public void setKyy(String kyy) {
            this.kyy = kyy;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public CurrencytypeBean getCurrencytype() {
        return currencytype;
    }

        public void setCurrencytype(CurrencytypeBean currencytype) {
        this.currencytype = currencytype;
    }

    public static class CurrencytypeBean {
        /**
         * rate : 1.152
         * name : 港币
         * currency : HKD
         * symbol : HK$
         */

        private CurrencytypeBean.CNYBean CNY;
        private CurrencytypeBean.HKDBean HKD;
        private CurrencytypeBean.MOPBean MOP;
        private CurrencytypeBean.USDBean USD;

        public CurrencytypeBean.CNYBean getCNY() {
            return CNY;
        }

        public void setCNY(CurrencytypeBean.CNYBean CNY) {
            this.CNY = CNY;
        }

        public CurrencytypeBean.HKDBean getHKD() {
            return HKD;
        }

        public void setHKD(CurrencytypeBean.HKDBean HKD) {
            this.HKD = HKD;
        }

        public CurrencytypeBean.MOPBean getMOP() {
            return MOP;
        }

        public void setMOP(CurrencytypeBean.MOPBean MOP) {
            this.MOP = MOP;
        }

        public CurrencytypeBean.USDBean getUSD() {
            return USD;
        }

        public void setUSD(CurrencytypeBean.USDBean USD) {
            this.USD = USD;
        }

        public static class CNYBean {
            /**
             * name : 人民币
             * symbol : ￥
             * currency : CNY
             * cackeKey :
             * rate : 0
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class HKDBean {
            /**
             * name : 港币
             * symbol : HK$
             * currency : HKD
             * cackeKey : cacheCurrencyHKD
             * rate : 1.1498
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class MOPBean {
            /**
             * name : 葡币
             * symbol : MOP$
             * currency : MOP
             * cackeKey : cacheCurrencyMOP
             * rate : 1.18265
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class USDBean {
            /**
             * name : 美元
             * symbol : US$
             * currency : USD
             * cackeKey : cacheCurrencyUSD
             * rate : 0.1463
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }
    }

        public static class ListBean {
            /**
             * goods_id : 100628
             * goods_commonid : 100075
             * goods_name : 价格测试 AS
             * goods_jingle :
             * goods_price : 10.00
             * goods_image : http://192.168.0.2/data/upload/shop/store/goods/1/1_05805067837661061_360.png
             */

            private String goods_id;
            private String goods_commonid;
            private String goods_name;
            private String goods_jingle;
            private String goods_price;
            private String goods_image;
            private String is_enquiry;
            private String goods_state;
            private int is_second_hand;


            public int getIs_second_hand() {
                return is_second_hand;
            }

            public void setIs_second_hand(int is_second_hand) {
                this.is_second_hand = is_second_hand;
            }

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getGoods_commonid() {
                return goods_commonid;
            }

            public void setGoods_commonid(String goods_commonid) {
                this.goods_commonid = goods_commonid;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getGoods_jingle() {
                return goods_jingle;
            }

            public void setGoods_jingle(String goods_jingle) {
                this.goods_jingle = goods_jingle;
            }

            public String getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(String goods_price) {
                this.goods_price = goods_price;
            }

            public String getGoods_image() {
                return goods_image;
            }

            public void setGoods_image(String goods_image) {
                this.goods_image = goods_image;
            }

            public String getIs_enquiry() {
                return is_enquiry;
            }

            public void setIs_enquiry(String is_enquiry) {
                this.is_enquiry = is_enquiry;
            }

            public String getGoods_state() {
                return goods_state;
            }

            public void setGoods_state(String goods_state) {
                this.goods_state = goods_state;
            }
        }

}
