package com.xht.kuaiyouyi.ui.search.entity;

import java.util.List;

public class RightArrBean {
    /**
     * gc_id : 200
     * gc_name : 家纺
     * class3 : [{"gc_id":"206","gc_name":"床品件套","gc_image":""},{"gc_id":"207","gc_name":"被子","gc_image":""},{"gc_id":"208","gc_name":"枕芯枕套","gc_image":""},{"gc_id":"209","gc_name":"床单被罩","gc_image":""},{"gc_id":"210","gc_name":"毛巾被/毯","gc_image":""},{"gc_id":"211","gc_name":"床垫/床褥","gc_image":""},{"gc_id":"212","gc_name":"蚊帐/凉席","gc_image":""},{"gc_id":"213","gc_name":"抱枕坐垫","gc_image":""},{"gc_id":"214","gc_name":"毛巾家纺","gc_image":""},{"gc_id":"215","gc_name":"电热毯","gc_image":""},{"gc_id":"216","gc_name":"窗帘/窗纱","gc_image":""},{"gc_id":"217","gc_name":"酒店用品","gc_image":""}]
     */

    private String gc_id;
    private String gc_name;
    private List<ClassBean> class3;

    public RightArrBean(String gc_id, String gc_name, List<ClassBean> class3) {
        this.gc_id = gc_id;
        this.gc_name = gc_name;
        this.class3 = class3;
    }

    public String getGc_id() {
        return gc_id;
    }

    public void setGc_id(String gc_id) {
        this.gc_id = gc_id;
    }

    public String getGc_name() {
        return gc_name;
    }

    public void setGc_name(String gc_name) {
        this.gc_name = gc_name;
    }

    public List<ClassBean> getClass3() {
        return class3;
    }

    public void setClass3(List<ClassBean> class3) {
        this.class3 = class3;
    }

    public static class ClassBean {
        /**
         * gc_id : 206
         * gc_name : 床品件套
         * gc_image :
         */

        private String gc_id;
        private String gc_name;
        private String gc_image;

        public ClassBean(String gc_id, String gc_name, String gc_image) {
            this.gc_id = gc_id;
            this.gc_name = gc_name;
            this.gc_image = gc_image;
        }

        public String getGc_id() {
            return gc_id;
        }

        public void setGc_id(String gc_id) {
            this.gc_id = gc_id;
        }

        public String getGc_name() {
            return gc_name;
        }

        public void setGc_name(String gc_name) {
            this.gc_name = gc_name;
        }

        public String getGc_image() {
            return gc_image;
        }

        public void setGc_image(String gc_image) {
            this.gc_image = gc_image;
        }

        @Override
        public String toString() {
            return "ClassBean{" +
                    "gc_id='" + gc_id + '\'' +
                    ", gc_name='" + gc_name + '\'' +
                    ", gc_image='" + gc_image + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "RightArrBean{" +
                "gc_id='" + gc_id + '\'' +
                ", gc_name='" + gc_name + '\'' +
                ", class3=" + class3 +
                '}';
    }
}

