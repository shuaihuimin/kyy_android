package com.xht.kuaiyouyi.ui.home.entity;

import java.util.List;

public class SearchHotWordBean {


    private List<SearchBean> search;

    public List<SearchBean> getSearch() {
        return search;
    }

    public void setSearch(List<SearchBean> search) {
        this.search = search;
    }

    public static class SearchBean {
        /**
         * name : 61儿童节，童装5折狂甩
         * value : 童装
         */

        private String name;
        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
