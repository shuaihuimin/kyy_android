package com.xht.kuaiyouyi.ui.mine.entity;

public class MinenumBean {


    /**
     * member_info : {"area_code":"86","phone":"13484622656","name":"runmin","avatar":"http://192.168.0.2/data/upload/shop/avatar/avatar_28.jpg","member_id":"28","grade":{"level":1,"level_name":"V1","exppoints":1000}}
     * num_info : {"wait_pay_num":"15","wait_deliver_num":"4","wait_receive_num":"8","fav_goods_num":2,"fav_store_num":1}
     */

    private MemberInfoBean member_info;
    private NumInfoBean num_info;
    private boolean is_cmember;

    public MemberInfoBean getMember_info() {
        return member_info;
    }

    public void setMember_info(MemberInfoBean member_info) {
        this.member_info = member_info;
    }

    public NumInfoBean getNum_info() {
        return num_info;
    }

    public void setNum_info(NumInfoBean num_info) {
        this.num_info = num_info;
    }

    public static class MemberInfoBean {
        /**
         * area_code : 86
         * phone : 13484622656
         * name : runmin
         * avatar : http://192.168.0.2/data/upload/shop/avatar/avatar_28.jpg
         * member_id : 28
         * grade : {"level":1,"level_name":"V1","exppoints":1000}
         */

        private String area_code;
        private String phone;
        private String name;
        private String avatar;
        private String member_id;
        private GradeBean grade;

        public String getArea_code() {
            return area_code;
        }

        public void setArea_code(String area_code) {
            this.area_code = area_code;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getMember_id() {
            return member_id;
        }

        public void setMember_id(String member_id) {
            this.member_id = member_id;
        }

        public GradeBean getGrade() {
            return grade;
        }

        public void setGrade(GradeBean grade) {
            this.grade = grade;
        }

        public static class GradeBean {
            /**
             * level : 1
             * level_name : V1
             * exppoints : 1000
             */

            private int level;
            private String level_name;
            private int exppoints;

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public String getLevel_name() {
                return level_name;
            }

            public void setLevel_name(String level_name) {
                this.level_name = level_name;
            }

            public int getExppoints() {
                return exppoints;
            }

            public void setExppoints(int exppoints) {
                this.exppoints = exppoints;
            }
        }
    }

    public static class NumInfoBean {
        /**
         * wait_pay_num : 15
         * wait_deliver_num : 4
         * wait_receive_num : 8
         * fav_goods_num : 2
         * fav_store_num : 1
         */

        private int wait_pay_num;
        private int wait_deliver_num;
        private int wait_receive_num;
        private int fav_goods_num;
        private int fav_store_num;

        public int getWait_pay_num() {
            return wait_pay_num;
        }

        public void setWait_pay_num(int wait_pay_num) {
            this.wait_pay_num = wait_pay_num;
        }

        public int getWait_deliver_num() {
            return wait_deliver_num;
        }

        public void setWait_deliver_num(int wait_deliver_num) {
            this.wait_deliver_num = wait_deliver_num;
        }

        public int getWait_receive_num() {
            return wait_receive_num;
        }

        public void setWait_receive_num(int wait_receive_num) {
            this.wait_receive_num = wait_receive_num;
        }

        public int getFav_goods_num() {
            return fav_goods_num;
        }

        public void setFav_goods_num(int fav_goods_num) {
            this.fav_goods_num = fav_goods_num;
        }

        public int getFav_store_num() {
            return fav_store_num;
        }

        public void setFav_store_num(int fav_store_num) {
            this.fav_store_num = fav_store_num;
        }
    }

    public boolean isIs_cmember() {
        return is_cmember;
    }

    public void setIs_cmember(boolean is_cmember) {
        this.is_cmember = is_cmember;
    }
}
