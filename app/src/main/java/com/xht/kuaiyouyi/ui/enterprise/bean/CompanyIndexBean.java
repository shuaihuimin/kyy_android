package com.xht.kuaiyouyi.ui.enterprise.bean;

public class CompanyIndexBean {


    /**
     * info : {"company_id":"90","is_admin":"0","company_use_money":0,"company_wait_money":0,"gd_start_3":"0","company_name":"人人网科技有限公司","gd_start_1":"2","member":{"member_id":"28"},"ap_start_1":"0","company_count_money":"3000000.00","gd_start_2":"0","ap_start_2":"0","ap_start_3":"0"}
     */

    private InfoBean info;

    public InfoBean getInfo() {
        return info;
    }

    public void setInfo(InfoBean info) {
        this.info = info;
    }

    public static class InfoBean {
        /**
         * company_id : 90
         * is_admin : 0
         * company_use_money : 0
         * company_wait_money : 0
         * gd_start_3 : 0
         * company_name : 人人网科技有限公司
         * gd_start_1 : 2
         * member : {"member_id":"28"}
         * ap_start_1 : 0
         * company_count_money : 3000000.00
         * gd_start_2 : 0
         * ap_start_2 : 0
         * ap_start_3 : 0
         */

        private String company_id;
        private int company_status;
        private String is_admin;
        private double company_use_money;//可用额度
        private double company_wait_money;//待还账单
        private String company_name;
        private MemberBean member;
        private int is_show;
        //企业订单
        private int gd_start_1;
        private int gd_start_2;
        private int gd_start_3;
        //采购审批
        private int ap_start_1;
        private int ap_start_2;
        private int ap_start_3;
        private int message_show;

        public String getCompany_id() {
            return company_id;
        }

        public void setCompany_id(String company_id) {
            this.company_id = company_id;
        }

        public int getCompany_status() {
            return company_status;
        }

        public void setCompany_status(int company_status) {
            this.company_status = company_status;
        }

        public String getIs_admin() {
            return is_admin;
        }

        public void setIs_admin(String is_admin) {
            this.is_admin = is_admin;
        }

        public double getCompany_use_money() {
            return company_use_money;
        }

        public void setCompany_use_money(double company_use_money) {
            this.company_use_money = company_use_money;
        }

        public double getCompany_wait_money() {
            return company_wait_money;
        }

        public void setCompany_wait_money(double company_wait_money) {
            this.company_wait_money = company_wait_money;
        }

        public int getGd_start_3() {
            return gd_start_3;
        }

        public void setGd_start_3(int gd_start_3) {
            this.gd_start_3 = gd_start_3;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public int getGd_start_1() {
            return gd_start_1;
        }

        public void setGd_start_1(int gd_start_1) {
            this.gd_start_1 = gd_start_1;
        }

        public MemberBean getMember() {
            return member;
        }

        public void setMember(MemberBean member) {
            this.member = member;
        }

        public int getAp_start_1() {
            return ap_start_1;
        }

        public void setAp_start_1(int ap_start_1) {
            this.ap_start_1 = ap_start_1;
        }

        public int getIs_show() {
            return is_show;
        }

        public void setIs_show(int is_show) {
            this.is_show = is_show;
        }

        public int getGd_start_2() {
            return gd_start_2;
        }

        public void setGd_start_2(int gd_start_2) {
            this.gd_start_2 = gd_start_2;
        }

        public int getAp_start_2() {
            return ap_start_2;
        }

        public void setAp_start_2(int ap_start_2) {
            this.ap_start_2 = ap_start_2;
        }

        public int getAp_start_3() {
            return ap_start_3;
        }

        public void setAp_start_3(int ap_start_3) {
            this.ap_start_3 = ap_start_3;
        }

        public int getMessage_show() {
            return message_show;
        }

        public void setMessage_show(int message_show) {
            this.message_show = message_show;
        }

        public static class MemberBean {
            /**
             * member_id : 28
             */

            private String member_id;
            private String member_position;

            public String getMember_id() {
                return member_id;
            }

            public void setMember_id(String member_id) {
                this.member_id = member_id;
            }

            public String getMember_position() {
                return member_position;
            }

            public void setMember_position(String member_position) {
                this.member_position = member_position;
            }
        }
    }
}
