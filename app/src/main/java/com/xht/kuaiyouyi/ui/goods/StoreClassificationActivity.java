package com.xht.kuaiyouyi.ui.goods;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.classic.common.MultipleStatusView;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.goods.adapter.StoreClassificationAdapter;
import com.xht.kuaiyouyi.ui.goods.entity.StoreClassBean;
import com.xht.kuaiyouyi.ui.search.adapter.SectionedSpanSizeLookup;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 店铺分类
 */

public class StoreClassificationActivity extends BaseActivity implements StoreClassificationAdapter.OnItemClickListener{
    @BindView(R.id.tv_title)
    TextView titleView;
    @BindView(R.id.iv_back)
    ImageView imageback;
    @BindView(R.id.recycler_store)
    RecyclerView recyclerView;
    @BindView(R.id.multipleStatusView)
    MultipleStatusView multipleStatusView;
    private String store_id;
    private StoreClassificationAdapter storeClassificationAdapter;
    private List<StoreClassBean.StoreCateBean> list=new ArrayList<>();


    @Override
    protected int getLayout() {
        return R.layout.activity_store_classfication;
    }

    @Override
    protected void initView() {
        titleView.setText(R.string.store_classification);
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StoreClassificationActivity.this.finish();
            }
        });
        store_id=getIntent().getExtras().getString("store_id");
        storeClassificationAdapter=new StoreClassificationAdapter(StoreClassificationActivity.this,list);
        GridLayoutManager manager1 = new GridLayoutManager(this,2);
        //设置header
        manager1.setSpanSizeLookup(new SectionedSpanSizeLookup(storeClassificationAdapter,manager1));
        recyclerView.setLayoutManager(manager1);
        recyclerView.setAdapter(storeClassificationAdapter);
        storeClassificationAdapter.setItemClickListener(this);
        if(store_id!=null){
            getdata();
        }
        multipleStatusView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getdata();
            }
        });
    }

    private void getdata(){
        multipleStatusView.showLoading();
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_STORE_CLASS())
                .addParam("store_id",store_id)
                .withPOST(new NetCallBack<StoreClassBean>() {

            @NotNull
            @Override
            public Class<StoreClassBean> getRealType() {
                return StoreClassBean.class;
            }

            @Override
            public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                if(isFinishing()){
                    return;
                }
                //ToastU.INSTANCE.showToast(getString(R.string.not_network));
                multipleStatusView.showError();

            }

            @Override
            public void onSuccess(@NonNull StoreClassBean storeClassBean) {
                if(isFinishing()){
                    return;
                }
                multipleStatusView.showContent();
                if(storeClassBean.getStore_cate()!=null){
                    list.addAll(storeClassBean.getStore_cate());
                    storeClassificationAdapter.setData(list);
                }
                if(list.size()==0){
                    multipleStatusView.showEmpty();
                }
            }
        },false);

    }


    @Override
    public void onItemClick(JSONObject jsonObject) {
        int section = jsonObject.getInteger("section");
        int position = jsonObject.getInteger("position");
        Bundle bundle=new Bundle();
        bundle.putString("store_id",list.get(section).getStore_id());
        bundle.putString("stc_id",list.get(section).getChild().get(position).getStc_id());
        bundle.putString("keyword",list.get(section).getChild().get(position).getStc_name());
        goToActivity(StoreTypelistActivity.class,bundle);

    }
}
