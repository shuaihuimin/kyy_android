package com.xht.kuaiyouyi.ui.goods;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.classic.common.MultipleStatusView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.adapter.StoreAdapter;
import com.xht.kuaiyouyi.ui.goods.entity.StoregoodsBean;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.BadgeView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class StoreTypelistActivity extends BaseActivity{
    @BindView(R.id.multipleStatusView)
    MultipleStatusView multipleStatusView;
    @BindView(R.id.edit_Search)
    EditText edit_Search;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.iv_move)
    ImageView iv_move;
    @BindView(R.id.radiogroup)
    RadioGroup radioGroup;
    @BindView(R.id.btn_price)
    RadioButton btn_price;
    @BindView(R.id.iv_classification)
    ImageView iv_classification;
    @BindView(R.id.recycler_store)
    RecyclerView recyclerView;
    @BindView(R.id.mRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;

    private BadgeView mBadgeView;

    private String store_id,stc_id;
    private String kyy="";
    private boolean orderflag=false;
    private String key="4";
    private String order="0";
    private String keyword="";
    private int flag=1;
    private StoreAdapter adapter;
    private List<StoregoodsBean.ListBean> list=new ArrayList<>();
    private List<StoregoodsBean.CurrencytypeBean> currlist=new ArrayList<>();
    private int img_height;
    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(list.size()==0){
                multipleStatusView.showEmpty();
                multipleStatusView.setClickable(false);
            }
        }
    };
    @Override
    protected int getLayout() {
        return R.layout.activity_storetepylist;
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        mBadgeView = new BadgeView(this,iv_move,10,4);
        store_id=getIntent().getExtras().getString("store_id");
        stc_id=getIntent().getExtras().getString("stc_id");
        keyword=getIntent().getExtras().getString("keyword");
        img_height=(Utils.getWindowWidth(StoreTypelistActivity.this)-8)/2;
        adapter=new StoreAdapter(StoreTypelistActivity.this,R.layout.item_brand_detils_recycler,list,currlist,0,img_height);
//        recyclerView.addItemDecoration(new RecycleViewDivider(
//                StoreTypelistActivity.this, LinearLayoutManager.VERTICAL, 1, getResources().getColor(R.color.line_grey)));
        recyclerView.setLayoutManager(new LinearLayoutManager(StoreTypelistActivity.this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Utils.hintKeyBoard(StoreTypelistActivity.this);
            }
        });
        if(!TextUtils.isEmpty(keyword)){
            edit_Search.setText(keyword);
            keyword="";
        }
        if(store_id!=null){
            getinit(key,order,keyword);
        }
        multipleStatusView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getinit(key,order,keyword);
            }
        });
        click();
        initload();
        initRefresh();
        showUnreadMsgNum();
    }

    private void showUnreadMsgNum() {
        mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
    }

    private void getinit(final String key, String order, String kyeword){
        if(kyy.equals("")){
            multipleStatusView.showLoading();
        }
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_STORE_ALLGOODS())
                .addParam("store_id",store_id)
                .addParam("store_type","2")
                .addParam("stc_id",stc_id)
                .addParam("order",order)
                .addParam("key",key)
                .addParam("keyword",keyword==null?"":keyword)
                .withLoadPOST(new NetCallBack<StoregoodsBean>() {

                    @NotNull
                    @Override
                    public Class<StoregoodsBean> getRealType() {
                        return StoregoodsBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        if(TextUtils.isEmpty(kyy)){
                            multipleStatusView.showError();
                            multipleStatusView.setClickable(true);
                        }else {
                            Toast.makeText(StoreTypelistActivity.this, getString(R.string.not_network), Toast.LENGTH_SHORT).show();
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Refreshing){
                            smartRefreshLayout.finishRefresh();
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull StoregoodsBean storegoodsBean) {
                        if(isFinishing()){
                            return;
                        }
                        multipleStatusView.showContent();
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Refreshing){
                            smartRefreshLayout.finishRefresh();
                        }
                        currlist.add(storegoodsBean.getCurrencytype());
                        if(storegoodsBean.getList()!=null && storegoodsBean.getList().size()!=0){
                            list.addAll(storegoodsBean.getList());
                        }else {
                            if(!TextUtils.isEmpty(key)){
                                smartRefreshLayout.finishLoadMoreWithNoMoreData();
                                smartRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
                            }
                        }
                        kyy=storegoodsBean.getKyy();
                        adapter.notifyDataSetChanged();
                        handler.sendEmptyMessage(0);
                    }
                },false,kyy);
    }



    private void click(){
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StoreTypelistActivity.this.finish();
            }
        });
        iv_move.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Login.Companion.getInstance().isLogin()){
                    goToActivity(MessageActivity.class);
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
        edit_Search.setOnEditorActionListener(new TextView.OnEditorActionListener(){

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //判断是否是“完成”键
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    //隐藏软键盘
                    InputMethodManager imm = (InputMethodManager) v
                            .getContext().getSystemService(
                                    Context.INPUT_METHOD_SERVICE);
                    if (imm.isActive()) {
                        imm.hideSoftInputFromWindow(
                                v.getApplicationWindowToken(), 0);
                    }

                    if(!edit_Search.getText().toString().isEmpty()){
                        keyword=edit_Search.getText().toString();
                    }
                    kyy="";
                    list.clear();
                    getinit(key,order,keyword);
                    return true;
                }
                return false;
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.btn_recommended:
                        if(orderflag){
                            orderflag=false;
                        }else {
                            orderflag=true;
                        }
                        kyy="";
                        key="4";
                        order="0";
                        list.clear();
                        getinit(key,order,keyword);
                        btn_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_price_nor),null);
                        break;
                    case R.id.btn_sales:
                        if(orderflag){
                            orderflag=false;
                        }else {
                            orderflag=true;
                        }
                        kyy="";
                        key="1";
                        order="0";
                        list.clear();
                        getinit(key,order,keyword);
                        btn_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_price_nor),null);
                        break;
                    case R.id.btn_price:
                        kyy="";
                        key="3";
                        list.clear();
                        order = "0";
                        btn_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.shop_icon_priceup_sel),null);
                        getinit(key,order,keyword);
                        break;

                }
            }
        });

        btn_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kyy="";
                key="3";
                list.clear();
                if(orderflag) {
                    orderflag = false;
                    order = "0";
                    btn_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.shop_icon_pricedown_sel),null);
                }else{
                    orderflag=true;
                    order="1";
                    btn_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.shop_icon_priceup_sel),null);
                }
                getinit(key,order,keyword);
            }
        });
        iv_classification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag==0){
                    flag=1;
                    iv_classification.setImageResource(R.mipmap.search_icon_palace_nor);
                    adapter=new StoreAdapter(StoreTypelistActivity.this,R.layout.item_brand_detils_recycler,list,currlist,0,img_height);
                    recyclerView.setLayoutManager(new LinearLayoutManager(StoreTypelistActivity.this));
                    recyclerView.setAdapter(adapter);
                    recyclerView.scrollToPosition(0);


                }else {
                    flag=0;
                    iv_classification.setImageResource(R.mipmap.search_icon_list_nor);
                    adapter=new StoreAdapter(StoreTypelistActivity.this,R.layout.home_recycler_item_layout,list,currlist,1,img_height);
                    GridLayoutManager manager = new GridLayoutManager(StoreTypelistActivity.this, 2,GridLayoutManager.VERTICAL,false);
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setAdapter(adapter);
                    recyclerView.scrollToPosition(0);

                }
            }
        });
    }

    private void initload(){
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                smartRefreshLayout.setNoMoreData(false) ;
                getinit(key,order,keyword);
            }
        });
    }

    private void initRefresh(){
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                smartRefreshLayout.setNoMoreData(false) ;
                kyy="";
                list.clear();
                getinit(key,order,keyword);
            }
        });
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.UNREAD_MSG_NUM)){
            showUnreadMsgNum();
        }
    }
}
