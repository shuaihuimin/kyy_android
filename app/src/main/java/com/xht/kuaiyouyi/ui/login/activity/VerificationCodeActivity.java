package com.xht.kuaiyouyi.ui.login.activity;

import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/**
 * Created by shuaihuimin on 2018/6/22.
 * 验证动态验证码页面(注册，找回密码）
 */

public class VerificationCodeActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.et_verify_code)
    EditText et_verify_code;
    @BindView(R.id.tv_get_verify_code)
    TextView tv_get_verify_code;
    @BindView(R.id.btn_next)
    Button btn_next;

    private String mType;
    private String mPhone;
    private String mAreaCode;
    private String mVerifyCode;


    private int mCountDown = 60;//倒计时60s开始

    private android.os.Handler mHandler = new android.os.Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            mCountDown = mCountDown - 1;
            if (mCountDown > 0) {
                tv_get_verify_code.setText(mCountDown + "s" + getResources().getString(R.string.regain));
                tv_get_verify_code.setEnabled(false);
                tv_get_verify_code.setTextColor(getResources().getColor(R.color.grey_5));
                mHandler.sendEmptyMessageDelayed(1, 1000);
            } else {
                tv_get_verify_code.setText(getResources().getString(R.string.get_dycode));
                tv_get_verify_code.setEnabled(true);
                tv_get_verify_code.setTextColor(getResources().getColor(R.color.blue_normal));
            }
        }
    };

    @Override
    protected int getLayout() {
        return R.layout.activity_verification;
    }

    @Override
    protected void initView() {
        mType = getIntent().getStringExtra(Contant.TYPE);
        mPhone = getIntent().getStringExtra(Contant.PHONE);
        mAreaCode = getIntent().getStringExtra(Contant.AREA_CODE);
        switch (mType) {
            case Contant.TYPE_REGISTER:
                EventBus.getDefault().register(this);
                tv_title.setText(R.string.register);
                break;
            case Contant.TYPE_RESET_PASSWORD:
                tv_title.setText(R.string.find_password);
                break;
        }
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVerifyCode = et_verify_code.getText().toString().trim();
                captcha();
            }
        });
        tv_get_verify_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestVerifyCode();
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        et_verify_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(et_verify_code.getText().toString().trim().length()>0){
                    btn_next.setEnabled(true);
                }else {
                    btn_next.setEnabled(true);
                }
            }
        });
        mHandler.sendEmptyMessageDelayed(1, 1000);
    }

    /**
     * 验证动态验证码
     */
    private void captcha() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CONNECT_CHECK_SMS_CAPTCHA())
                .addParam("phone", mPhone)
                .addParam("captcha", mVerifyCode)
                .addParam("type", mType)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(VerificationCodeActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        if(isFinishing()){
                            return;
                        }
                        // TODO: 2018/7/31 跳到设置密码界面SetPasswordActivity
                        Bundle bundle = new Bundle();
                        switch (mType) {
                            case Contant.TYPE_REGISTER:
                                bundle.putString(Contant.PHONE,mPhone);
                                bundle.putString(Contant.AREA_CODE,mAreaCode);
                                bundle.putString(Contant.VERIFY_CODE,mVerifyCode);
                                bundle.putString(Contant.TYPE,Contant.TYPE_REGISTER);
                                goToActivity(SetPasswordActivity.class,bundle);
                                break;
                            case Contant.TYPE_RESET_PASSWORD:
                                bundle.putString(Contant.PHONE,mPhone);
                                bundle.putString(Contant.VERIFY_CODE,mVerifyCode);
                                bundle.putString(Contant.TYPE,Contant.TYPE_RESET_PASSWORD);
                                goToActivity(SetPasswordActivity.class,bundle);
                                finish();
                                break;
                        }
                    }
                }, false);

    }

    /**
     * 请求验证码
     */
    private void requestVerifyCode() {
        DialogUtils.createTipAllLoadDialog(VerificationCodeActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CONNECT_GET_SMS_CAPTCHA())
                .addParam("phone", mPhone)
                .addParam("type", mType)
                .addParam("area_code", mAreaCode)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(VerificationCodeActivity.this, err,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(getApplicationContext(),R.string.verify_code_sended,Toast.LENGTH_SHORT).show();
                        DialogUtils.moven();
                        mCountDown = 60;
                        mHandler.sendEmptyMessageDelayed(1, 1000);
                    }
                }, false);
    }

    @Override
    protected void onDestroy() {
        if(mHandler.hasMessages(1)){
            mHandler.removeMessages(1);
        }
        mHandler = null;
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.EVENT_ClOSE_LOGINACTIVITY)){
            finish();
        }
    }
}
