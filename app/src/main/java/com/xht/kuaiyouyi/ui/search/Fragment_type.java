package com.xht.kuaiyouyi.ui.search;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.home.activity.SearchGoodsActivity;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.ui.search.adapter.SectionedSpanSizeLookup;
import com.xht.kuaiyouyi.ui.search.adapter.TepyAdapter;
import com.xht.kuaiyouyi.ui.search.adapter.TypedataAdapter;
import com.xht.kuaiyouyi.ui.search.entity.RightArrBean;
import com.xht.kuaiyouyi.ui.search.entity.TypeData;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.GlideImageLoader;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.ProportionscreenUtils;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.widget.BadgeView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.zxing_android.CaptureActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by shuaihuimin on 2018/6/13.
 */

public class Fragment_type extends BaseFragment implements TepyAdapter.OnItemClickListener{
    @BindView(R.id.rv_menu)
    RecyclerView rcyctler_menu;
    @BindView(R.id.rv_content)
    RecyclerView rcyctler_content;
    @BindView(R.id.banner_typeitem)
    Banner banner;
    @BindView(R.id.tvSearch_type)
    TextView tv_search;
    @BindView(R.id.img_camera_type)
    ImageView img_camera_type;
    @BindView(R.id.im_type_help)
    ImageView im_type_help;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    private List<TypeData.ClassArrBean> type_list = new ArrayList<>();
    private TypedataAdapter typedataAdapter;
    //轮播图数据源
    private List<String> lunbolist = new ArrayList<>();
    //二级分类
    private TepyAdapter typeAdapter;
    private List<RightArrBean> rightlist=new ArrayList<>();
    private final int PERMISSION_CAMERA_REQUEST_CODE = 20;

    private BadgeView mBadgeView;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:
                    int screen=getActivity().getWindowManager().getDefaultDisplay().getWidth();
                    float height= ProportionscreenUtils.getHeight(263,90,(int) screen);
                    ViewGroup.LayoutParams params=banner.getLayoutParams();
                    params.height=(int) height;
                    banner.setLayoutParams(params);
                    banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
                    banner.setImageLoader(new GlideImageLoader());
                    banner.setImages(lunbolist);
                    banner.setDelayTime(1500);
                    banner.setIndicatorGravity(BannerConfig.CENTER);
                    banner.start();
                case 2:
                    typedataAdapter.notifyDataSetChanged();
                    break;
            }

        }
    };




    @Override
    protected void initView(View view, Bundle savedInstanceState) {
//        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        mBadgeView = new BadgeView(getContext(),im_type_help,10,4);
        getinfo();
        rcyctler_menu.setLayoutManager(new LinearLayoutManager(getActivity()));
        //rcyctler_menu.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        //二级分类
        typeAdapter=new TepyAdapter(getActivity(),rightlist);
        typeAdapter.setItemClickListener(this);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 3);
        //设置header
        manager.setSpanSizeLookup(new SectionedSpanSizeLookup(typeAdapter, manager));
        rcyctler_content.setLayoutManager(manager);
        rcyctler_content.setAdapter(typeAdapter);
        tv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString(TypelistActivity.CATE_ID,"");
                bundle.putInt("flag",0);
                goToActivity(SearchGoodsActivity.class,bundle);
            }
        });

        img_camera_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                    Intent intent = new Intent(getActivity(), CaptureActivity.class);
                    startActivityForResult(intent, 100);
                }else {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},PERMISSION_CAMERA_REQUEST_CODE);
                }
            }
        });

        im_type_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.Companion.getInstance().isLogin()){
                    goToActivity(MessageActivity.class);
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
        bt_load_again.setVisibility(View.VISIBLE);
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getinfo();
            }
        });
        showUnreadMsgNum();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_search_layout;
    }

    //加载一级分类
    private void getinfo() {
        DialogUtils.createTipAllLoadDialog(getActivity());
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_GOODSCLASS()).withPOST(new NetCallBack<TypeData>() {
            @NotNull
            @Override
            public Class<TypeData> getRealType() {
                return TypeData.class;
            }

            @Override
            public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                DialogUtils.moven();
                error_retry_view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(@NonNull TypeData data) {
                error_retry_view.setVisibility(View.GONE);
                if (data != null && data.getClass_arr() != null) {
                    for (int i = 0; i < data.getClass_arr().size(); i++) {
                        type_list.add(data.getClass_arr().get(i));
                    }
                    type_list.get(0).setSelect(true);
                    typedataAdapter = new TypedataAdapter(R.layout.item_menu, type_list,getActivity());
                    rcyctler_menu.setAdapter(typedataAdapter);
                    final TypeData finalData = data;
                    typedataAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                            int i  = 0;
                            for (TypeData.ClassArrBean classArrBean:type_list){
                                if (classArrBean.isSelect() == true){
                                    type_list.get(i).setSelect(false);
                                }
                                i+=1;
                            }
                            if(type_list!=null && type_list.size()!=0){
                                type_list.get(position).setSelect(true);
                                handler.sendEmptyMessage(2);
                                if(finalData.getClass_arr()!=null && finalData.getClass_arr().size()!=0){
                                    loadRightData(finalData.getClass_arr().get(position).getGc_id());
                                }
                            }
                        }
                    });
                    if(data.getClass_arr()!=null && finalData.getClass_arr().size()!=0){
                        loadRightData(data.getClass_arr().get(0).getGc_id());
                    }
                }

            }
        }, false);
    }

    //加载二级分类
    private void loadRightData(String id){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_GOODSCLASS()).addParam("gc_id", id).withPOST(new NetCallBack<String>() {
            @NotNull
            @Override
            public Class<String> getRealType() {
                return String.class;
            }

            @Override
            public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                DialogUtils.moven();
                ToastU.INSTANCE.showToast(getString(R.string.not_network));
            }

            @Override
            public void onSuccess(@NonNull String typeContentDate) {
                DialogUtils.moven();
                rcyctler_content.removeAllViews();
                rightlist.clear();
                RightArrBean rightArrBean=null;
                RightArrBean.ClassBean classBean=null;
                List<RightArrBean.ClassBean> listclass = null;
                String head_name,head_id,name,id,image;
                if(typeContentDate!=null){
                    try {
                        JSONObject jsonObject=new JSONObject(typeContentDate);
                        if(jsonObject!=null){
                            if(jsonObject.has("banner_arr")){
                                lunbolist.clear();
                                JSONArray jsonArray=jsonObject.getJSONArray("banner_arr");
                                for(int i=0;i<jsonArray.length();i++){
                                    JSONObject banner=jsonArray.getJSONObject(i);
                                    lunbolist.add(banner.getString("img"));
                                }
                            }
                            if(jsonObject.has("class_right_arr")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("class_right_arr");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject json = jsonArray.getJSONObject(i);
                                        head_name = json.getString("gc_name");
                                        head_id = json.getString("gc_id");
                                        listclass = new ArrayList<>();
                                        if (json.has("class3")) {
                                            JSONArray goods = json.getJSONArray("class3");
                                            for (int j = 0; j < goods.length(); j++) {
                                                JSONObject goodsobject = goods.getJSONObject(j);
                                                name = goodsobject.optString("gc_name");
                                                id = goodsobject.optString("gc_id");
                                                image = goodsobject.optString("gc_image");
                                                classBean = new RightArrBean.ClassBean(id, name, image);
                                                listclass.add(classBean);
                                            }
                                        }
                                        rightArrBean = new RightArrBean(head_id, head_name, listclass);
                                        rightlist.add(rightArrBean);
                                    }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                typeAdapter.setData(rightlist);
                handler.sendEmptyMessage(1);

            }
        }, false);
    }


    @Override
    public void onItemClick(com.alibaba.fastjson.JSONObject jsonObject) {
        if(rightlist!=null && rightlist.size()!=0) {
            int section = jsonObject.getInteger("section");
            if(rightlist.get(section).getClass3()!=null && rightlist.get(section).getClass3().size()!=0){
                int position = jsonObject.getInteger("position");
                if (position < rightlist.get(section).getClass3().size()) {
                    Bundle bundle = new Bundle();
                    bundle.putString(TypelistActivity.CATE_ID, rightlist.get(section).getClass3().get(position).getGc_id());
                    bundle.putString(TypelistActivity.KEYWORD, rightlist.get(section).getClass3().get(position).getGc_name());
                    goToActivity(TypelistActivity.class, bundle);
                }
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSION_CAMERA_REQUEST_CODE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                goToActivity(com.zxing_android.CaptureActivity.class);
            }else {
                Toast.makeText(getContext(),getString(R.string.tip_permission_camera),Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /**
         * 拿到解析完成的字符串
         */
        if (data != null) {
            String result=data.getStringExtra("result");
            if(result.contains("act=login&op=index")){
                String qr_qruuid=result.substring(result.indexOf("qruuid="),result.lastIndexOf("&"));
                String time=result.substring(result.indexOf("time="),result.length());
                final String qruuid=qr_qruuid.substring(7,qr_qruuid.length());
                final String qr_time=time.substring(5,time.length());
                if(Login.Companion.getInstance().isLogin()){
                    DialogUtils.createTwoBtnDialog(getActivity(), getString(R.string.quedinlogin),
                            getString(R.string.dialog_confirm),
                            getString(R.string.dialog_cancel),
                            new DialogUtils.OnRightBtnListener() {
                                @Override
                                public void setOnRightListener(Dialog dialog) {
                                    saomaLogin(qruuid,qr_time);
                                }
                            }, new DialogUtils.OnLeftBtnListener() {
                                @Override
                                public void setOnLeftListener(Dialog dialog) {

                                }
                            }, false, true);

                }else {
                    goToActivity(LoginActivity.class);
                }

            }else if(result.contains("act=goods&op=index")){
                String goods = result.substring(result.indexOf("goods_id="),result.length());
                String goods_id=goods.substring(9,goods.length());
                Bundle bundle=new Bundle();
                bundle.putString("goods_id",goods_id);
                goToActivity(GoodsActivity.class,bundle);
            } else {
                if(Patterns.WEB_URL.matcher(result).matches() || URLUtil.isValidUrl(result)){
                    Uri uri = Uri.parse(result);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }else {
//                    Bundle bundle=new Bundle();
//                    bundle.putString("result",result);
//                    goToActivity(SweepYardActivity.class,bundle);
                    ToastU.INSTANCE.showToast(R.string.invalid_barcode);
                }

            }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            showUnreadMsgNum();
            type_list.clear();
            rightlist.clear();
            lunbolist.clear();
            getinfo();
        }
    }

    private void showUnreadMsgNum() {
        if(Login.Companion.getInstance().isLogin()){
            mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
        }
    }

    @Override
    public void updateUnreadMsg() {
        showUnreadMsgNum();
    }

    private void saomaLogin(String qruuid,String qr_time){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_SWEEP_YARD_LOGIN())
                .addParam("qruuid",qruuid)
                .addParam("qr_time",qr_time)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        DialogUtils.createTipImageAndTextDialog(getActivity(),getString(R.string.login_err),R.mipmap.png_icon_popup_wrong);
                    }

                    @Override
                    public void onSuccess(@NonNull String string) {
                        DialogUtils.createTipImageAndTextDialog(getActivity(),getString(R.string.login_su),R.mipmap.png_icon_popup_ok);
                    }
                },false);

    }
}
