package com.xht.kuaiyouyi.ui.cart.entity;

/**
 * Created by Administrator on 2017/3/26.
 * 商品信息
 */

public class GoodsInfo {

    private String cart_id;
    private String buyer_id;
    private String store_id;
    private String store_name;
    private String goods_id;
    private String goods_name;
    private String goods_price;
    private String goods_num;
    private String goods_image;
    private String goods_storage;
    private boolean isChoosed;

    public GoodsInfo(String cart_id, String buyer_id, String store_id, String store_name, String goods_id, String goods_name, String goods_price, String goods_num, String goods_image,String goods_storage) {
        this.cart_id = cart_id;
        this.buyer_id = buyer_id;
        this.store_id = store_id;
        this.store_name = store_name;
        this.goods_id = goods_id;
        this.goods_name = goods_name;
        this.goods_price = goods_price;
        this.goods_num = goods_num;
        this.goods_image = goods_image;
        this.goods_storage=goods_storage;
    }

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(String buyer_id) {
        this.buyer_id = buyer_id;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(String goods_price) {
        this.goods_price = goods_price;
    }

    public String getGoods_num() {
        return goods_num;
    }

    public void setGoods_num(String goods_num) {
        this.goods_num = goods_num;
    }

    public String getGoods_image() {
        return goods_image;
    }

    public void setGoods_image(String goods_image) {
        this.goods_image = goods_image;
    }

    public String getGoods_storage() {
        return goods_storage;
    }

    public void setGoods_storage(String goods_storage) {
        this.goods_storage = goods_storage;
    }

    public boolean isChoosed() {
        return isChoosed;
    }

    public void setChoosed(boolean choosed) {
        isChoosed = choosed;
    }


    @Override
    public String toString() {
        return "GoodsInfo{" +
                "cart_id='" + cart_id + '\'' +
                ", buyer_id='" + buyer_id + '\'' +
                ", store_id='" + store_id + '\'' +
                ", store_name='" + store_name + '\'' +
                ", goods_id='" + goods_id + '\'' +
                ", goods_name='" + goods_name + '\'' +
                ", goods_price='" + goods_price + '\'' +
                ", goods_num='" + goods_num + '\'' +
                ", goods_image='" + goods_image + '\'' +
                ", goods_storage='" + goods_storage + '\'' +
                ", isChoosed=" + isChoosed +
                '}';
    }
}
