package com.xht.kuaiyouyi.ui.cart.entity;

public class PayBean {

    /**
     * alipay_trade_app_pay_response : {"code":"10000","msg":"Success","app_id":"2018070960521666","auth_app_id":"2018070960521666","charset":"utf-8","timestamp":"2018-09-06 11:28:26","total_amount":"0.01","trade_no":"2018090622001468231001816893","seller_id":"2088921309318723","out_trade_no":"440589548265753110"}
     * sign : L86SjsWzFmffy+rVIDe55eqYpVXecTv4uYKMQPRI1FsULSpz2vK1qLvkcoTDa3lj+eBe12f22GuSQGxNIM4uOrga4A4n1CG52IevZZxhOD1bwb/+sA+lGo3p+38kVvhECE1Les+GGOW/XKE63o4g/TKb8ts/yncjxYfII5GzFn23ZEcDY/pIijSHylxou8TCAWICuNfn8mBFB+ZtIf+lL7N6AoHySAx196jnM9YMZaF8EBjxaXdWxe+h+783B2XbW6UYCfL4Mnyrp+5agwR5rpiAyaGEeSTcn0JMTqIwUp7pCgb5fb719Bwotw23ODV76FkBlYH/GbFXH/cnYqsmRw==
     * sign_type : RSA2
     */

    private AlipayTradeAppPayResponseBean alipay_trade_app_pay_response;
    private String sign;
    private String sign_type;

    public AlipayTradeAppPayResponseBean getAlipay_trade_app_pay_response() {
        return alipay_trade_app_pay_response;
    }

    public void setAlipay_trade_app_pay_response(AlipayTradeAppPayResponseBean alipay_trade_app_pay_response) {
        this.alipay_trade_app_pay_response = alipay_trade_app_pay_response;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSign_type() {
        return sign_type;
    }

    public void setSign_type(String sign_type) {
        this.sign_type = sign_type;
    }

    public static class AlipayTradeAppPayResponseBean {
        /**
         * code : 10000
         * msg : Success
         * app_id : 2018070960521666
         * auth_app_id : 2018070960521666
         * charset : utf-8
         * timestamp : 2018-09-06 11:28:26
         * total_amount : 0.01
         * trade_no : 2018090622001468231001816893
         * seller_id : 2088921309318723
         * out_trade_no : 440589548265753110
         */

        private String code;
        private String msg;
        private String app_id;
        private String auth_app_id;
        private String charset;
        private String timestamp;
        private String total_amount;
        private String trade_no;
        private String seller_id;
        private String out_trade_no;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getApp_id() {
            return app_id;
        }

        public void setApp_id(String app_id) {
            this.app_id = app_id;
        }

        public String getAuth_app_id() {
            return auth_app_id;
        }

        public void setAuth_app_id(String auth_app_id) {
            this.auth_app_id = auth_app_id;
        }

        public String getCharset() {
            return charset;
        }

        public void setCharset(String charset) {
            this.charset = charset;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public String getTrade_no() {
            return trade_no;
        }

        public void setTrade_no(String trade_no) {
            this.trade_no = trade_no;
        }

        public String getSeller_id() {
            return seller_id;
        }

        public void setSeller_id(String seller_id) {
            this.seller_id = seller_id;
        }

        public String getOut_trade_no() {
            return out_trade_no;
        }

        public void setOut_trade_no(String out_trade_no) {
            this.out_trade_no = out_trade_no;
        }
    }
}
