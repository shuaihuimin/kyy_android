package com.xht.kuaiyouyi.ui.goods.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.goods.entity.GoodsData;
import com.xht.kuaiyouyi.ui.mine.adapter.CollectionAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GoodsCaseAdapter extends  BaseQuickAdapter<GoodsData.GoodsBean.GoodsCaseBean,BaseViewHolder> {
    private boolean isopen=false;
    private Context mcontext;
    private List<GoodsData.GoodsBean.GoodsCaseBean> list;

    public GoodsCaseAdapter(Context context,int layoutResId, @Nullable List<GoodsData.GoodsBean.GoodsCaseBean> data) {
        super(layoutResId, data);
        this.mcontext=context;
        this.list=data;
    }

    @Override
    protected void convert(final BaseViewHolder helper, GoodsData.GoodsBean.GoodsCaseBean item) {
        helper.setText(R.id.tv_content,item.getContent())
        .setText(R.id.tv_title,item.getName());
        if(!TextUtils.isEmpty(item.getImage())){
            Glide.with(mContext)
                    .load(item.getImage())
                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_column))
                    .into((ImageView) helper.getView(R.id.iv_img));

        }
        final TextView textView=helper.getView(R.id.tv_content).findViewById(R.id.tv_content);
        if(textView.length()<=45){
            helper.getView(R.id.tv_more).setVisibility(View.GONE);
        }else {
            helper.getView(R.id.tv_more).setVisibility(View.VISIBLE);
        }
        helper.getView(R.id.tv_more).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isopen) {
                    textView.setMaxLines(3);
                    isopen = false;
                    helper.setText(R.id.tv_more,mContext.getString(R.string.see_more));
                } else {
                    textView.setMaxLines(Integer.MAX_VALUE);
                    isopen = true;
                    helper.setText(R.id.tv_more,mContext.getString(R.string.souqi));
                }
            }
        });

    }


//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        final ViewHolder viewHolder;
//        if(convertView==null){
//            convertView=View.inflate(context, R.layout.item_fragment_case_recycler,null);
//            viewHolder=new ViewHolder(convertView);
//            convertView.setTag(viewHolder);
//
//        }else {
//            viewHolder= (ViewHolder) convertView.getTag();
//        }
//        viewHolder.tv_title.setText(list.get(position).getName());
//        viewHolder.tv_content.setText(list.get(position).getContent());
//        Log.i("info","--------img"+list.get(position).getImage());
//        if(!TextUtils.isEmpty(list.get(position).getImage())){
//            Glide.with(context)
//                    .load(list.get(position).getImage())
//                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_column))
//                    .into(viewHolder.image_head);
//        }
//        if(viewHolder.tv_content.getText().toString().length()<=45){
//            viewHolder.tv_more.setVisibility(View.GONE);
//        }else {
//            viewHolder.tv_more.setVisibility(View.VISIBLE);
//        }
//
//        final SpannableString mSpanALL = new SpannableString("学习Android很快乐*");
//        viewHolder.tv_more.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (isopen) {
//                    viewHolder.tv_content.setMaxLines(3);
//                    isopen = false;
//                    viewHolder.tv_more.setText(context.getString(R.string.see_more));
//                } else {
//                    viewHolder.tv_content.setMaxLines(Integer.MAX_VALUE);
//                    isopen = true;
//                    viewHolder.tv_more.setText(context.getString(R.string.souqi));
//                }
//            }
//        });
//
//        return convertView;
//    }

//    class ViewHolder{
//        @BindView(R.id.tv_content)
//        TextView tv_content;
//        @BindView(R.id.tv_more)
//        TextView tv_more;
//        @BindView(R.id.iv_img)
//        ImageView image_head;
//        @BindView(R.id.tv_title)
//        TextView tv_title;
//        public ViewHolder(View view) {
//            ButterKnife.bind(this, view);
//        }
//    }
}
