package com.xht.kuaiyouyi.ui.base;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.ButterKnife;

/**
 * Created by shuaihuimin on 2018/6/12.
 */

public abstract class BaseFragment extends Fragment {
    public BaseFragment mFragSelf;


    //初始化控件
    protected abstract void initView(View view, Bundle savedInstanceState);
    //获取布局文件ID
    protected abstract int getLayoutId();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        mFragSelf=this;
        ButterKnife.bind(this, view);
        initView(view,savedInstanceState);
        return view;
    }

    /**
     * toast
     * @param text
     */
    public void showToast(String text){
        Toast.makeText(getContext(), text,Toast.LENGTH_SHORT).show();
        //Toast.makeText(mActivity, text, Toast.LENGTH_SHORT).show();
    }


    /**
     * 快捷跳转到另一个Activity，可传递数据
     * @param activityClass  目标
     * @param bundle  数据
     */
    public void goToActivity(Class activityClass, Bundle bundle){

        Intent intent=new Intent(getContext(),activityClass);
        if (bundle!=null){
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }
    /**
     * 快捷跳转到另一个Activity
     * @param activityClass  目标
     */
    public void goToActivity(Class activityClass){
        Intent intent=new Intent(getContext(),activityClass);
        startActivity(intent);
    }


    public void updateUnreadMsg(){

    }

}
