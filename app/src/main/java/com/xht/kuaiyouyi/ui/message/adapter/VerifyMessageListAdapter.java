package com.xht.kuaiyouyi.ui.message.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.message.bean.VerifyMessageListBean;
import com.xht.kuaiyouyi.ui.message.fragment.VerifyMessageFragment;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.ButtomDialog;

import org.jetbrains.annotations.NotNull;

public class VerifyMessageListAdapter extends BaseAdapter {
    private Context mContext;
    private VerifyMessageListBean mVerifyMessageListBean;
    private ButtomDialog mButtomDialog;
    private int mPosition ;
    private VerifyMessageFragment.OnVerifyMessageReadNumUpdate mCallback;

    public VerifyMessageListAdapter(Context mContext, VerifyMessageListBean verifyMessageListBean, VerifyMessageFragment.OnVerifyMessageReadNumUpdate callback) {
        this.mContext = mContext;
        this.mVerifyMessageListBean = verifyMessageListBean;
        this.mCallback = callback;
    }

    @Override
    public int getCount() {
        if(mVerifyMessageListBean.getMessage_list()!=null){
            return mVerifyMessageListBean.getMessage_list().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mVerifyMessageListBean.getMessage_list().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.item_message_verify, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_message = convertView.findViewById(R.id.tv_message);
            viewHolder.tv_time = convertView.findViewById(R.id.tv_time);
            viewHolder.tv_status = convertView.findViewById(R.id.tv_status);
            viewHolder.tv_point = convertView.findViewById(R.id.tv_point);
            viewHolder.iv_next = convertView.findViewById(R.id.iv_next);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_time.setText(Utils.getDisplayDataAndTime(mVerifyMessageListBean.getMessage_list().get(position).getCreate_time()));
        if(mVerifyMessageListBean.getMessage_list().get(position).getType()==1
                ||mVerifyMessageListBean.getMessage_list().get(position).getType()==2){
            viewHolder.tv_message.setText(mVerifyMessageListBean.getMessage_list().get(position).getTo_data());
            viewHolder.tv_status.setVisibility(View.VISIBLE);
            switch (mVerifyMessageListBean.getMessage_list().get(position).getStatus()){
                case 0:
                    viewHolder.tv_status.setText(R.string.pending);
                    viewHolder.tv_status.setEnabled(true);
                    break;
                case 1:
                    viewHolder.tv_status.setText(R.string.agreed);
                    viewHolder.tv_status.setEnabled(false);
                    break;
                case 2:
                    viewHolder.tv_status.setText(R.string.rejected);
                    viewHolder.tv_status.setEnabled(false);
                    break;
            }

            viewHolder.tv_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPosition = position;
                    showButtonDialog();
                }
            });
        }else {
            viewHolder.tv_message.setText(mVerifyMessageListBean.getMessage_list().get(position).getData());
            viewHolder.tv_status.setVisibility(View.GONE);
        }


        if(mVerifyMessageListBean.getMessage_list().get(position).getIs_read()==1){
            viewHolder.tv_point.setVisibility(View.GONE);
        }else {
            viewHolder.tv_point.setVisibility(View.VISIBLE);
        }

        if(mVerifyMessageListBean.getMessage_list().get(position).getType() == 3){
            viewHolder.iv_next.setVisibility(View.VISIBLE);
        }else {
            viewHolder.iv_next.setVisibility(View.INVISIBLE);
        }
        return convertView;
    }

    private void requestIsAgree(final String status){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_VERIFY_MESSAGE_ISAGREE())
                .addParam("message_id",mVerifyMessageListBean.getMessage_list().get(mPosition).getId())
                .addParam("status",status)
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                        //30023:其他管理员已同意
                        if(errCode == 30023){
                            updateStatus("1");
                        }
                        //30024:其他管理员已拒绝
                        if(errCode == 30024){
                            updateStatus("2");
                        }
                        Toast.makeText(mContext, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        updateStatus(status);
                    }
                }, false);
    }

    /**
     * 更新状态
     * @param status 1-已同意,2-已拒绝
     */
    private void updateStatus(String status) {
        mVerifyMessageListBean.getMessage_list().get(mPosition)
                .setStatus(Integer.valueOf(status));
        mVerifyMessageListBean.getMessage_list().get(mPosition).setIs_read(1);
        if(mCallback!=null){
            mCallback.onVerifyMessageNumReadUpdate(1);
        }
        notifyDataSetChanged();
    }

    private class ViewHolder{
        private TextView tv_time,tv_message,tv_status,tv_point;
        private ImageView iv_next;
    }

    private void showButtonDialog() {
        if(mButtomDialog == null){
            View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_buttom_three_btn,null);
            mButtomDialog = new ButtomDialog(mContext,view,false,true);
            final TextView tv_three = view.findViewById(R.id.tv_three);
            TextView tv_one = view.findViewById(R.id.tv_one);
            tv_one.setText(R.string.consent);
            TextView tv_two = view.findViewById(R.id.tv_two);
            tv_two.setText(R.string.message_reject);
            tv_two.setTextColor(mContext.getResources().getColor(R.color.grey_4));

            tv_three.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                }
            });
            tv_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                    requestIsAgree("1");
                }
            });
            tv_two.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                    requestIsAgree("2");
                }
            });
        }
        mButtomDialog.show();

    }
}
