package com.xht.kuaiyouyi.ui.cart.entity;

public class OrderChooseBean {
    private String name;
    private String id;
    private String symbol;
    private boolean isSelect;

    public OrderChooseBean(String name,String id) {
        this.name = name;
        this.id=id;
    }
    public OrderChooseBean(String name,String id,String symbol) {
        this.name = name;
        this.id=id;
        this.symbol=symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "OrderChooseBean{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", symbol='" + symbol + '\'' +
                ", isSelect=" + isSelect +
                '}';
    }
}
