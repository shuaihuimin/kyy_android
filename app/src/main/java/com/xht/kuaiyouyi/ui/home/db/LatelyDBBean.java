package com.xht.kuaiyouyi.ui.home.db;

import org.litepal.crud.LitePalSupport;

public class LatelyDBBean extends LitePalSupport {
    private String hot_world;
    private long search_time;
    private int is_second_hand = 0;

    public String getHot_world() {
        return hot_world;
    }

    public void setHot_world(String hot_world) {
        this.hot_world = hot_world;
    }

    public long getSearch_time() {
        return search_time;
    }

    public void setSearch_time(long search_time) {
        this.search_time = search_time;
    }

    public int getIs_second_hand() {
        return is_second_hand;
    }

    public void setIs_second_hand(int is_second_hand) {
        this.is_second_hand = is_second_hand;
    }
}
