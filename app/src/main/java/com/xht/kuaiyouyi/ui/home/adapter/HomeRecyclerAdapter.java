package com.xht.kuaiyouyi.ui.home.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.home.entity.HomeData;
import com.xht.kuaiyouyi.ui.mine.setting.CurrencyActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import java.util.List;

/**
 * Created by shuaihuimin on 2018/6/14.
 * 首页recyclerview适配器
 */

public class HomeRecyclerAdapter extends BaseQuickAdapter<HomeData.GoodsListBean,BaseViewHolder>{
    private Context context;
    private List<HomeData.CurrencytypeBean> list;
    private int height;

    public HomeRecyclerAdapter(Context context,int layoutResId, @Nullable List<HomeData.GoodsListBean> data,List<HomeData.CurrencytypeBean> list,int height) {
        super(layoutResId, data);
        this.context=context;
        this.list=list;
        this.height=height;
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeData.GoodsListBean item) {
        double currency=0;String price="";
        if (item.getIs_enquiry().equals("0")) {
            helper.getView(R.id.tv_inquiry).setVisibility(View.GONE);
            helper.getView(R.id.tv_price).setVisibility(View.VISIBLE);
            helper.getView(R.id.tv_dollar).setVisibility(View.VISIBLE);
        } else {
            helper.getView(R.id.tv_inquiry).setVisibility(View.VISIBLE);
            helper.getView(R.id.tv_price).setVisibility(View.GONE);
            helper.getView(R.id.tv_dollar).setVisibility(View.GONE);
        }
        if(list.size()!=0) {
            if (Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.HKD)) {
                currency = Double.parseDouble(item.getGoods_price()) * list.get(0).getHKD().getRate();
                price = Utils.getDisplayMoney(currency);
                helper.setText(R.id.tv_dollar, "(~" + list.get(0).getHKD().getSymbol() + price + ")");
            } else if (Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.MOP)) {
                currency = Double.parseDouble(item.getGoods_price()) * list.get(0).getMOP().getRate();
                price = Utils.getDisplayMoney(currency);
                helper.setText(R.id.tv_dollar, "(~" + list.get(0).getMOP().getSymbol() + price + ")");
            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.RMB)){
                helper.getView(R.id.tv_dollar).setVisibility(View.GONE);
            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.USA)){
                currency = Double.parseDouble(item.getGoods_price()) * list.get(0).getUSD().getRate();
                price = Utils.getDisplayMoney(currency);
                helper.setText(R.id.tv_dollar, "(~" + list.get(0).getUSD().getSymbol() + price + ")");
            }
            helper.setText(R.id.tv_price, "¥"+Utils.getDisplayMoney(Double.parseDouble(item.getGoods_price())));
        }

        if(item.getIs_second_hand()==1){
            helper.setText(R.id.tv_name,Utils.secondLabel(item.getGoods_name()));
        }else {
            helper.setText(R.id.tv_name,item.getGoods_name());
        }
        Glide.with(context).load(item.getGoods_image())
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                .into((ImageView) helper.getView(R.id.iv_img));
        ViewGroup.LayoutParams layoutParams=helper.getView(R.id.iv_img).getLayoutParams();
        layoutParams.height= height;
    }


}
