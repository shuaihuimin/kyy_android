package com.xht.kuaiyouyi.ui.message.bean;

public class UpdateUserBean {
    private String u_id ;
    private String u_name ;
    private String avatar ;
    private String s_id ;
    private String s_name ;
    private String s_avatar ;

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getU_name() {
        return u_name;
    }

    public void setU_name(String u_name) {
        this.u_name = u_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public String getS_avatar() {
        return s_avatar;
    }

    public void setS_avatar(String s_avatar) {
        this.s_avatar = s_avatar;
    }
}
