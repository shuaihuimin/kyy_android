package com.xht.kuaiyouyi.ui.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.goods.StoreActivity;
import com.xht.kuaiyouyi.ui.home.entity.SecondMarkerketBean;
import com.xht.kuaiyouyi.ui.mine.setting.CurrencyActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.RoundBackgroundColorSpan;

import java.util.List;

public class SeacondMarketAdapter extends BaseQuickAdapter<SecondMarkerketBean.GoodsListBean,BaseViewHolder> {
    private Context mContext;
    private SecondMarkerketBean.CurrencytypeBean mCurrencytypeBean;

    public SeacondMarketAdapter(int layoutResId, @Nullable List<SecondMarkerketBean.GoodsListBean> data,Context context,SecondMarkerketBean.CurrencytypeBean currencytypeBean) {
        super(layoutResId, data);
        this.mContext=context;
        this.mCurrencytypeBean=currencytypeBean;
    }

    public SeacondMarketAdapter(@Nullable List<SecondMarkerketBean.GoodsListBean> data) {
        super(data);
    }

    public SeacondMarketAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final SecondMarkerketBean.GoodsListBean item) {
        double currency=0;String price="";
        if(!TextUtils.isEmpty(item.getGoods_image())){
            Glide.with(mContext).load(item.getGoods_image())
                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                    .into((ImageView) helper.getView(R.id.iv_img));
        }
        if (item.getIs_enquiry().equals("0")) {
            helper.getView(R.id.tv_inquiry).setVisibility(View.GONE);
            helper.getView(R.id.tv_price).setVisibility(View.VISIBLE);
            helper.getView(R.id.tv_dollar).setVisibility(View.VISIBLE);
        } else {
            helper.getView(R.id.tv_inquiry).setVisibility(View.VISIBLE);
            helper.getView(R.id.tv_price).setVisibility(View.GONE);
            helper.getView(R.id.tv_dollar).setVisibility(View.GONE);
        }

        if(mCurrencytypeBean!=null) {
            if (Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.HKD)) {
                currency = Double.parseDouble(item.getGoods_price()) * mCurrencytypeBean.getHKD().getRate();
                price = Utils.getDisplayMoney(currency);
                helper.setText(R.id.tv_price, "¥"+Utils.getDisplayMoney(Double.parseDouble(item.getGoods_price())))
                        .setText(R.id.tv_dollar, "(~" + mCurrencytypeBean.getHKD().getSymbol() + price+")");
            } else if (Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.MOP)) {
                currency = Double.parseDouble(item.getGoods_price()) * mCurrencytypeBean.getMOP().getRate();
                price = Utils.fmtMicrometer(Double.toString(currency), false);
                helper.setText(R.id.tv_price, "¥"+Utils.getDisplayMoney(Double.parseDouble(item.getGoods_price())))
                        .setText(R.id.tv_dollar, "(~" + mCurrencytypeBean.getMOP().getSymbol() + price+")");
            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.RMB)){
                helper.setText(R.id.tv_price, "¥"+Utils.getDisplayMoney(Double.parseDouble(item.getGoods_price())));
                helper.getView(R.id.tv_dollar).setVisibility(View.GONE);
            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.USA)){
                currency = Double.parseDouble(item.getGoods_price()) * mCurrencytypeBean.getUSD().getRate();
                price = Utils.fmtMicrometer(Double.toString(currency), false);
                helper.setText(R.id.tv_price, "¥"+Utils.getDisplayMoney(Double.parseDouble(item.getGoods_price())))
                        .setText(R.id.tv_dollar, "(~" + mCurrencytypeBean.getUSD().getSymbol() + price+")");
            }
        }
        helper.setText(R.id.tv_name,Utils.secondLabel(item.getGoods_name()));
        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("goods_id",item.getGoods_id());
                Intent intent=new Intent();
                intent.setClass(mContext, GoodsActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
        helper.setText(R.id.tv_store,item.getStore_name());
        helper.setOnClickListener(R.id.linear, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("store_id",item.getStore_id());
                Intent intent=new Intent();
                intent.putExtras(bundle);
                intent.setClass(mContext,StoreActivity.class);
                mContext.startActivity(intent);
            }
        });

        if(helper.getPosition()!=0){
            helper.setGone(R.id.view_line,false);
        }else if (helper.getPosition()==0){
            helper.setGone(R.id.view_line,true);
        }
    }
}
