package com.xht.kuaiyouyi.ui.enterprise.activity;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.enterprise.bean.CreditDetailBean;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.ProgressView;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class CreditEnterpriseActivity extends BaseActivity {
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_right)
    TextView tv_right;
    @BindView(R.id.tv_available_credit)
    TextView tv_available_credit;
    @BindView(R.id.tv_total_credit)
    TextView tv_total_credit;
    @BindView(R.id.tv_wait_repayment)
    TextView tv_wait_repayment;
    @BindView(R.id.tv_payed)
    TextView tv_payed;
    @BindView(R.id.pv_percent)
    ProgressView pv_percent;



    @Override
    protected int getLayout() {
        return R.layout.activity_enterprise_credit;
    }

    @Override
    protected void initView() {
        iv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_title.setText(R.string.enterprise_credit_amout);
        tv_right.setVisibility(View.GONE);
        //明细按钮
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        requestCredit();
    }

    private void requestCredit() {
        DialogUtils.createTipAllLoadDialog(this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_EP_CREDIT())
                .addParam("company_id", Login.Companion.getInstance().getEnterprise_company_id())
                .withPOST(new NetCallBack<CreditDetailBean>() {
                    @NotNull
                    @Override
                    public Class<CreditDetailBean> getRealType() {
                        return CreditDetailBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(CreditEnterpriseActivity.this,err,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull CreditDetailBean creditDetailBean) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        tv_available_credit.setText(Utils.getDisplayMoney(creditDetailBean.getInfo().getCompany_use_money()));
                        tv_total_credit.append("￥" + Utils.getDisplayMoney(creditDetailBean.getInfo().getCompany_total_money()));
                        tv_wait_repayment.setText(Utils.getDisplayMoney(creditDetailBean.getInfo().getCompany_wait_money()));
                        tv_payed.setText(Utils.getDisplayMoney(creditDetailBean.getInfo().getCompany_pay_money()));
                        int progress = (int) (creditDetailBean.getInfo().getCompany_use_money()/creditDetailBean.getInfo().getCompany_total_money()*100);
                        pv_percent.setProgess(progress);
                    }
                },false);
    }
}
