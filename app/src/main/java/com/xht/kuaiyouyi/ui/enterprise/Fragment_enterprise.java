package com.xht.kuaiyouyi.ui.enterprise;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.enterprise.activity.ApprovalEnterpriseActivity;
import com.xht.kuaiyouyi.ui.enterprise.activity.CreditEnterpriseActivity;
import com.xht.kuaiyouyi.ui.enterprise.activity.EnterpriseListActivity;
import com.xht.kuaiyouyi.ui.enterprise.activity.MemberListEnterpriseActivity;
import com.xht.kuaiyouyi.ui.enterprise.activity.SearchEnterpriseActivity;
import com.xht.kuaiyouyi.ui.enterprise.bean.CompanyIndexBean;
import com.xht.kuaiyouyi.ui.enterprise.bean.UserCompanies;
import com.xht.kuaiyouyi.ui.enterprise.fragment.ApprovalListFragment;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.ui.mine.order.MineOrderActivity;
import com.xht.kuaiyouyi.ui.mine.order.OrderListfragment;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.BadgeView;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import q.rorbin.badgeview.QBadgeView;

/**
 * Created by shuaihuimin on 2018/6/13.
 */

public class Fragment_enterprise extends BaseFragment {

    //无企业
    @BindView(R.id.rl_enterprise_empty)
    RelativeLayout rl_enterprise_empty;
    @BindView(R.id.btn_add_enterprise)
    Button btn_add_enterprise;


    //企业主页
    @BindView(R.id.rl_enterprise_index)
    RelativeLayout rl_enterprise_index;
    @BindView(R.id.rl_top)
    RelativeLayout rl_top;
    @BindView(R.id.srl_refresh_enterprise_index)
    SmartRefreshLayout srl_refresh_enterprise_index;
    @BindView(R.id.tv_enterprise_name)
    TextView tv_enterprise_name;
    @BindView(R.id.bt_enterprise_switch)
    Button bt_enterprise_switch;
    @BindView(R.id.tv_switch_point)
    TextView tv_switch_point;
    @BindView(R.id.tv_enterprise_position)
    TextView tv_enterprise_position;
    @BindView(R.id.tv_enterprise_status)
    TextView tv_enterprise_status;
    @BindView(R.id.iv_message)
    ImageView iv_message;
    @BindView(R.id.ll_enterprise_amount)
    LinearLayout ll_enterprise_amount;
    @BindView(R.id.rl_enterprise_credit)
    RelativeLayout rl_enterprise_credit;
    @BindView(R.id.tv_available_credit)
    TextView tv_available_credit;
    @BindView(R.id.rl_wait_bill)
    RelativeLayout rl_wait_bill;
    @BindView(R.id.tv_wait_bill)
    TextView tv_wait_bill;

    //采购审批
    @BindView(R.id.rl_wait_approval)
    RelativeLayout rl_wait_approval;
    @BindView(R.id.rl_i_launch)
    RelativeLayout rl_i_launch;
    TextView tv_i_launch_num;
    @BindView(R.id.rl_copy_to_me)
    RelativeLayout rl_copy_to_me;
    @BindView(R.id.rl_approvaled)
    RelativeLayout rl_approvaled;
    //企业订单
    @BindView(R.id.rl_wait_pay)
    RelativeLayout rl_wait_pay;
    @BindView(R.id.rl_wait_send)
    RelativeLayout rl_wait_send;
    @BindView(R.id.rl_wait_receive)
    RelativeLayout rl_wait_receive;
    @BindView(R.id.rl_all_order)
    RelativeLayout rl_all_order;
    @BindView(R.id.rl_enterprise_member)
    RelativeLayout rl_enterprise_member;

    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;

    private boolean isInitDataSuccessful;//第一次打开页面是否初始化成功

    private final int REQUEST_CODE = 200;

    //订单
    private QBadgeView mBadgeViewWaitPay;
    private QBadgeView mBadgeViewWaitSend;
    private QBadgeView mBadgeViewWaitReceive;
    @BindView(R.id.iv_wait_pay)
    ImageView iv_wait_pay;
    @BindView(R.id.iv_wait_send)
    ImageView iv_wait_send;
    @BindView(R.id.iv_wait_receive)
    ImageView iv_wait_receive;

    //采购审批
    private QBadgeView mBadgeViewWaitApproval;
    private QBadgeView mBadgeViewCopyTo;
    @BindView(R.id.iv_wait_approval)
    ImageView iv_wait_approval;
    @BindView(R.id.iv_copy_to)
    ImageView iv_copy_to;

    private QBadgeView mBadgeViewMessage;

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        setEmptyClickListenter();
        setEnterpriseIndexListener();
        srl_refresh_enterprise_index.setEnableLoadMore(false);
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstRequestData();
            }
        });
        bt_load_again.setVisibility(View.VISIBLE);
        setUpBadgeView();
        showUnreadMsgNum();
    }

    private void firstRequestData() {
        if(Login.Companion.getInstance().isLogin()){
            if (!TextUtils.isEmpty(Login.Companion.getInstance().getEnterprise_company_id())) {
                requestEnterpriseIndex(true);
            }else {
                requestEnterpriseList();
            }
        }
    }

    private void requestEnterpriseList() {
        DialogUtils.createTipAllLoadDialog(getContext());
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_EP_LIST())
                .withPOST(new NetCallBack<UserCompanies>() {
                    @NotNull
                    @Override
                    public Class<UserCompanies> getRealType() {
                        return UserCompanies.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        DialogUtils.moven();
                        error_retry_view.setVisibility(View.VISIBLE);
                        rl_enterprise_empty.setVisibility(View.GONE);
                    }

                    @Override
                    public void onSuccess(@NonNull UserCompanies userCompanies) {
                        DialogUtils.moven();
                        if(userCompanies.getCompanies()==null){
                            error_retry_view.setVisibility(View.VISIBLE);
                            rl_enterprise_empty.setVisibility(View.GONE);
                            return;
                        }
                        if(userCompanies.getCompanies().size()>0){
                            Login.Companion.getInstance().setEnterprise_company_id(userCompanies.getCompanies().get(0).getCompany_id());
                            requestEnterpriseIndex(true);
                        }else {
                            rl_enterprise_empty.setVisibility(View.VISIBLE);
                            rl_enterprise_index.setVisibility(View.GONE);
                            error_retry_view.setVisibility(View.GONE);
                            isInitDataSuccessful = true;
                        }

                    }
                }, false);
    }

    //无加入企业时的监听
    private void setEmptyClickListenter() {
        btn_add_enterprise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(SearchEnterpriseActivity.class);
            }
        });
    }

    //企业主页的监听
    private void setEnterpriseIndexListener() {
        srl_refresh_enterprise_index.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                requestEnterpriseIndex(false);
            }
        });

        iv_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(MessageActivity.class);
            }
        });

        bt_enterprise_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getContext(),EnterpriseListActivity.class),REQUEST_CODE);
            }
        });

        rl_enterprise_credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(CreditEnterpriseActivity.class);
            }
        });

        rl_wait_approval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToApprovalEnterpriseActivity(ApprovalListFragment.APPROVAL_WAIT);
            }
        });
        rl_i_launch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToApprovalEnterpriseActivity(ApprovalListFragment.APPROVAL_START);
            }
        });
        rl_copy_to_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToApprovalEnterpriseActivity(ApprovalListFragment.APPROVAL_COPY_TO);
            }
        });
        rl_approvaled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToApprovalEnterpriseActivity(ApprovalListFragment.APPROVALED);
            }
        });

        rl_wait_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putInt(Contant.TYPE, MineOrderActivity.TAB_WAIT_PAY_ORDER);
                bundle.putInt(MineOrderActivity.ORDER_TYPE, OrderListfragment.TYPE_ENTERPRISE_ORDER);
                goToActivity(MineOrderActivity.class,bundle);
            }
        });
        rl_wait_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putInt(Contant.TYPE,MineOrderActivity.TAB_WAIT_SEND_ORDER);
                bundle.putInt(MineOrderActivity.ORDER_TYPE,OrderListfragment.TYPE_ENTERPRISE_ORDER);
                goToActivity(MineOrderActivity.class,bundle);
            }
        });
        rl_wait_receive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putInt(Contant.TYPE,MineOrderActivity.TAB_WAIT_RECEIVE_ORDER);
                bundle.putInt(MineOrderActivity.ORDER_TYPE,OrderListfragment.TYPE_ENTERPRISE_ORDER);
                goToActivity(MineOrderActivity.class,bundle);
            }
        });
        rl_all_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putInt(Contant.TYPE,MineOrderActivity.TAB_ALL_ORDER);
                bundle.putInt(MineOrderActivity.ORDER_TYPE,OrderListfragment.TYPE_ENTERPRISE_ORDER);
                goToActivity(MineOrderActivity.class,bundle);
            }
        });


        rl_enterprise_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Constant.COMPANY_ID, Login.Companion.getInstance().getEnterprise_company_id());
                goToActivity(MemberListEnterpriseActivity.class, bundle);
            }
        });

    }

    public void requestEnterpriseIndex(boolean isShowLoading) {
        if(isShowLoading){
            DialogUtils.createTipAllLoadDialog(getContext());
        }
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_EP_COMPANY())
                .addParam("company_id", Login.Companion.getInstance().getEnterprise_company_id())
                .withPOST(new NetCallBack<CompanyIndexBean>() {
                    @NotNull
                    @Override
                    public Class<CompanyIndexBean> getRealType() {
                        return CompanyIndexBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(!isInitDataSuccessful){
                            error_retry_view.setVisibility(View.VISIBLE);
                        }
                        DialogUtils.moven();
                        Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                        if (srl_refresh_enterprise_index.getState() == RefreshState.Refreshing) {
                            srl_refresh_enterprise_index.finishRefresh();
                        }

                        //30001和30002的时候请求企业列表获取第一个企业显示
                        if(errCode==30001||errCode==30002){
                            Login.Companion.getInstance().setEnterprise_company_id("");
                            requestEnterpriseList();
                        }else {
                            Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull CompanyIndexBean companyIndexBean) {
                        error_retry_view.setVisibility(View.GONE);
                        isInitDataSuccessful = true;
                        DialogUtils.moven();
                        rl_enterprise_index.setVisibility(View.VISIBLE);
                        showUnreadMsgNum();
                        rl_enterprise_empty.setVisibility(View.GONE);
                        if (srl_refresh_enterprise_index.getState() == RefreshState.Refreshing) {
                            srl_refresh_enterprise_index.finishRefresh();
                        }
                        showEnterpriseIndex(companyIndexBean);
                    }
                }, false);
    }

    private void showEnterpriseIndex(CompanyIndexBean companyIndexBean) {
        CompanyIndexBean.InfoBean infoBean = companyIndexBean.getInfo();
        if (infoBean != null) {
            tv_enterprise_name.setText(infoBean.getCompany_name());
            GradientDrawable enterpriseStatusDrawable = (GradientDrawable) getResources().getDrawable(R.drawable.enterprise_status_bg);
            switch (infoBean.getCompany_status()){
                case 1:
                    //已认证
                    tv_enterprise_status.setText(R.string.enterprise_status_authenticated);
                    tv_enterprise_status.setTextColor(getResources().getColor(R.color.green_3));
                    enterpriseStatusDrawable.setColor(getResources().getColor(R.color.green_2));
                    break;
                case 2:
                    //已冻结
                    tv_enterprise_status.setText(R.string.enterprise_status_frozened);
                    tv_enterprise_status.setTextColor(getResources().getColor(R.color.red_1));
                    enterpriseStatusDrawable.setColor(getResources().getColor(R.color.red_2));
                    break;
                case 3:
                    //已解散
                    tv_enterprise_status.setText(R.string.search_enterprise_dissolved);
                    tv_enterprise_status.setTextColor(getResources().getColor(android.R.color.white));
                    enterpriseStatusDrawable.setColor(getResources().getColor(R.color.grey_5));
                    break;
                case 4:
                    //已过期
                    tv_enterprise_status.setText(R.string.search_enterprise_expired);
                    tv_enterprise_status.setTextColor(getResources().getColor(R.color.red_1));
                    enterpriseStatusDrawable.setColor(getResources().getColor(R.color.red_2));
                    break;
            }
            tv_enterprise_status.setBackground(enterpriseStatusDrawable);
//            CompanyIndexBean.InfoBean.MemberBean memberBean = infoBean.getMember();
//            if (memberBean != null) {
//                tv_enterprise_position.setText(memberBean.getMember_position());
//            }
            if(infoBean.getIs_show() == 1){
                rl_top.setBackgroundResource(R.mipmap.enterprise_bg);
                ll_enterprise_amount.setVisibility(View.VISIBLE);
                tv_available_credit.setText(Utils.getDisplayMoney(infoBean.getCompany_use_money()));
                tv_wait_bill.setText(Utils.getDisplayMoney(infoBean.getCompany_wait_money()));
            }else {
                ll_enterprise_amount.setVisibility(View.GONE);
                rl_top.setBackgroundResource(R.mipmap.enterprise_bg_nocredit);
            }

            mBadgeViewWaitApproval.setBadgeNumber(infoBean.getAp_start_1());
            mBadgeViewCopyTo.setBadgeNumber(infoBean.getAp_start_3());

            mBadgeViewWaitPay.setBadgeNumber(infoBean.getGd_start_1());
            mBadgeViewWaitSend.setBadgeNumber(infoBean.getGd_start_2());
            mBadgeViewWaitReceive.setBadgeNumber(infoBean.getGd_start_3());
            if(infoBean.getMessage_show()==1){
                tv_switch_point.setVisibility(View.VISIBLE);
            }else {
                tv_switch_point.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_enterprise;
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void event(MessageEvent event) {
//        if (event.getMessage().equals(MessageEvent.EVENT_LOGIN)) {
//            isInitDataSuccessful = false;
//            requestEnterpriseList();
//        }
//    }

    private void goToApprovalEnterpriseActivity(int approveId) {
        Bundle bundle = new Bundle();
        bundle.putString(Constant.COMPANY_ID, Login.Companion.getInstance().getEnterprise_company_id());
        bundle.putInt(ApprovalEnterpriseActivity.APPROVE_ID, approveId);
        goToActivity(ApprovalEnterpriseActivity.class, bundle);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        refreshData();
    }

    public void refreshData(){
        if(Login.Companion.getInstance().isLogin()&&!isHidden()){
            if(!TextUtils.isEmpty(Login.Companion.getInstance().getEnterprise_company_id())){
                if(isInitDataSuccessful){
                    requestEnterpriseIndex(false);
                }else {
                    requestEnterpriseIndex(true);
                }
            }else {
                requestEnterpriseList();
            }
            showUnreadMsgNum();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE&&resultCode== Activity.RESULT_OK){
            requestEnterpriseIndex(true);
        }
    }

    @Override
    public void updateUnreadMsg() {
        showUnreadMsgNum();
    }

    private void showUnreadMsgNum() {
        if(Login.Companion.getInstance().isLogin()){
            mBadgeViewMessage.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
        }
    }

    private void setUpBadgeView() {
        mBadgeViewWaitPay = new BadgeView(getContext(),iv_wait_pay,10,4);
        mBadgeViewWaitApproval = new BadgeView(getContext(),iv_wait_approval,10,4);
        mBadgeViewCopyTo = new BadgeView(getContext(),iv_copy_to,10,4);
        mBadgeViewWaitSend = new BadgeView(getContext(),iv_wait_send,10,4);
        mBadgeViewWaitReceive = new BadgeView(getContext(),iv_wait_receive,10,4);

        mBadgeViewMessage = new BadgeView(getContext(),iv_message,10,4);
    }
}
