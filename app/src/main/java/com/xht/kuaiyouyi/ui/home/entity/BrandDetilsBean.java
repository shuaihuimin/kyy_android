package com.xht.kuaiyouyi.ui.home.entity;

import java.util.List;

public class BrandDetilsBean {

    /**
     * brand_info : {"brand_id":"84","brand_name":"阿迪达斯","brand_pic":"http://127.0.0.1/data/upload/shop/brand/04397471910652190_sm.jpg","class_id":"1"}
     * goods_list : [{"goods_id":"100431","goods_name":"小五金 45 4 67 6 7 67 6","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/11/11_05798865350375316.jpg","goods_price":"10.00","gc_id_1":"1063","store_id":"11","is_enquiry":"0","goods_state":"1","store_name":"小猪佩奇的店铺"},{"goods_id":"100420","goods_name":"测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试 大","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05797205961743028.jpg","goods_price":"10.00","gc_id_1":"1063","store_id":"1","is_enquiry":"0","goods_state":"1","store_name":"快优易自营"},{"goods_id":"100432","goods_name":"小五金 45 4 5 67 6 7 67 6","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/11/11_05798865350375316.jpg","goods_price":"10.00","gc_id_1":"1063","store_id":"11","is_enquiry":"0","goods_state":"1","store_name":"小猪佩奇的店铺"},{"goods_id":"100628","goods_name":"价格测试 AS","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05805067837661061.png","goods_price":"10.00","gc_id_1":"1063","store_id":"1","is_enquiry":"0","goods_state":"1","store_name":"快优易自营"},{"goods_id":"100464","goods_name":"测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试 浊","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05797205961743028.jpg","goods_price":"1003450.00","gc_id_1":"1063","store_id":"1","is_enquiry":"0","goods_state":"1","store_name":"快优易自营"},{"goods_id":"100463","goods_name":"测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试 浊","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05797205961743028.jpg","goods_price":"1003450.00","gc_id_1":"1063","store_id":"1","is_enquiry":"0","goods_state":"1","store_name":"快优易自营"},{"goods_id":"100462","goods_name":"测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试 浊","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05797205961743028.jpg","goods_price":"1003450.00","gc_id_1":"1063","store_id":"1","is_enquiry":"0","goods_state":"1","store_name":"快优易自营"},{"goods_id":"100461","goods_name":"测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试 大","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05797205961743028.jpg","goods_price":"1003450.00","gc_id_1":"1063","store_id":"1","is_enquiry":"0","goods_state":"1","store_name":"快优易自营"},{"goods_id":"100460","goods_name":"测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试 浊","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05797205961743028.jpg","goods_price":"1003450.00","gc_id_1":"1063","store_id":"1","is_enquiry":"0","goods_state":"1","store_name":"快优易自营"},{"goods_id":"100434","goods_name":"小五金 45 4 5 67 6 67 6","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/11/11_05798865350375316.jpg","goods_price":"10.00","gc_id_1":"1063","store_id":"11","is_enquiry":"0","goods_state":"1","store_name":"小猪佩奇的店铺"}]
     * kyy : eyJLIjoia3l5XzAyMSIsIk8iOiI4NCIsIlAiOjF9
     * currencytype : {"name":"港币","symbol":"HK$","currency":"HKD","rate":1.1511}
     */

    private BrandInfoBean brand_info;
    private String kyy;
    private CurrencytypeBean currencytype;
    private List<GoodsListBean> goods_list;

    public BrandInfoBean getBrand_info() {
        return brand_info;
    }

    public void setBrand_info(BrandInfoBean brand_info) {
        this.brand_info = brand_info;
    }

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public CurrencytypeBean getCurrencytype() {
        return currencytype;
    }

    public void setCurrencytype(CurrencytypeBean currencytype) {
        this.currencytype = currencytype;
    }

    public List<GoodsListBean> getGoods_list() {
        return goods_list;
    }

    public void setGoods_list(List<GoodsListBean> goods_list) {
        this.goods_list = goods_list;
    }

    public static class BrandInfoBean {
        /**
         * brand_id : 84
         * brand_name : 阿迪达斯
         * brand_pic : http://127.0.0.1/data/upload/shop/brand/04397471910652190_sm.jpg
         * class_id : 1
         */

        private String brand_id;
        private String brand_name;
        private String brand_pic;
        private String class_id;

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }

        public String getBrand_pic() {
            return brand_pic;
        }

        public void setBrand_pic(String brand_pic) {
            this.brand_pic = brand_pic;
        }

        public String getClass_id() {
            return class_id;
        }

        public void setClass_id(String class_id) {
            this.class_id = class_id;
        }
    }

    public static class CurrencytypeBean {
        /**
         * CNY : {"name":"人民币","symbol":"￥","currency":"CNY","cackeKey":"","rate":0}
         * HKD : {"name":"港币","symbol":"HK$","currency":"HKD","cackeKey":"cacheCurrencyHKD","rate":1.1498}
         * MOP : {"name":"葡币","symbol":"MOP$","currency":"MOP","cackeKey":"cacheCurrencyMOP","rate":1.18265}
         * USD : {"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.1463}
         */

        private HomeData.CurrencytypeBean.CNYBean CNY;
        private HomeData.CurrencytypeBean.HKDBean HKD;
        private HomeData.CurrencytypeBean.MOPBean MOP;
        private HomeData.CurrencytypeBean.USDBean USD;

        public HomeData.CurrencytypeBean.CNYBean getCNY() {
            return CNY;
        }

        public void setCNY(HomeData.CurrencytypeBean.CNYBean CNY) {
            this.CNY = CNY;
        }

        public HomeData.CurrencytypeBean.HKDBean getHKD() {
            return HKD;
        }

        public void setHKD(HomeData.CurrencytypeBean.HKDBean HKD) {
            this.HKD = HKD;
        }

        public HomeData.CurrencytypeBean.MOPBean getMOP() {
            return MOP;
        }

        public void setMOP(HomeData.CurrencytypeBean.MOPBean MOP) {
            this.MOP = MOP;
        }

        public HomeData.CurrencytypeBean.USDBean getUSD() {
            return USD;
        }

        public void setUSD(HomeData.CurrencytypeBean.USDBean USD) {
            this.USD = USD;
        }

        public static class CNYBean {
            /**
             * name : 人民币
             * symbol : ￥
             * currency : CNY
             * cackeKey :
             * rate : 0
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class HKDBean {
            /**
             * name : 港币
             * symbol : HK$
             * currency : HKD
             * cackeKey : cacheCurrencyHKD
             * rate : 1.1498
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class MOPBean {
            /**
             * name : 葡币
             * symbol : MOP$
             * currency : MOP
             * cackeKey : cacheCurrencyMOP
             * rate : 1.18265
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class USDBean {
            /**
             * name : 美元
             * symbol : US$
             * currency : USD
             * cackeKey : cacheCurrencyUSD
             * rate : 0.1463
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }
    }

    public static class GoodsListBean {
        /**
         * goods_id : 100431
         * goods_name : 小五金 45 4 67 6 7 67 6
         * goods_image : http://127.0.0.1/data/upload/shop/store/goods/11/11_05798865350375316.jpg
         * goods_price : 10.00
         * gc_id_1 : 1063
         * store_id : 11
         * is_enquiry : 0
         * goods_state : 1
         * store_name : 小猪佩奇的店铺
         */

        private String goods_id;
        private String goods_name;
        private String goods_image;
        private String goods_price;
        private String gc_id_1;
        private String store_id;
        private String is_enquiry;
        private String goods_state;
        private String store_name;
        private int is_second_hand;


        public int getIs_second_hand() {
            return is_second_hand;
        }

        public void setIs_second_hand(int is_second_hand) {
            this.is_second_hand = is_second_hand;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_image() {
            return goods_image;
        }

        public void setGoods_image(String goods_image) {
            this.goods_image = goods_image;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getGc_id_1() {
            return gc_id_1;
        }

        public void setGc_id_1(String gc_id_1) {
            this.gc_id_1 = gc_id_1;
        }

        public String getStore_id() {
            return store_id;
        }

        public void setStore_id(String store_id) {
            this.store_id = store_id;
        }

        public String getIs_enquiry() {
            return is_enquiry;
        }

        public void setIs_enquiry(String is_enquiry) {
            this.is_enquiry = is_enquiry;
        }

        public String getGoods_state() {
            return goods_state;
        }

        public void setGoods_state(String goods_state) {
            this.goods_state = goods_state;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }
    }
}
