package com.xht.kuaiyouyi.ui.cart.adapter;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.cart.CartFragment;
import com.xht.kuaiyouyi.ui.cart.entity.CartIndexBean;
import com.xht.kuaiyouyi.ui.cart.entity.DeleteGoodsBean;
import com.xht.kuaiyouyi.ui.cart.widget.AmountEditViewCar;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.goods.StoreActivity;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

public class CartAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    //选中弹出框修改数量
    private AlertDialog mNumUpdateDialog;

    public CartAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getGroupCount() {
        if (((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list() != null && ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().size() > 0) {
            return ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().size();
        }
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getGoods() != null) {
            return ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getGoods().size();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getGoods().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final GroupViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new GroupViewHolder();
            convertView = View.inflate(mContext, R.layout.item_cart_store, null);
            viewHolder.tv_store_name = convertView.findViewById(R.id.tv_store_name);
            viewHolder.iv_check = convertView.findViewById(R.id.iv_check);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (GroupViewHolder) convertView.getTag();
        }
        if(((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).isCheck()){
            viewHolder.iv_check.setImageResource(R.mipmap.pop_icon_tick);
        }else {
            viewHolder.iv_check.setImageResource(R.mipmap.address_icon_unselected_nor);
        }
        viewHolder.tv_store_name.setText(((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getStore_name());
        viewHolder.iv_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).isCheck()){
                    ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).setCheck(false);
                    viewHolder.iv_check.setImageResource(R.mipmap.address_icon_unselected_nor);
                }else {
                    ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).setCheck(true);
                    viewHolder.iv_check.setImageResource(R.mipmap.pop_icon_tick);
                }

                //设置子item的状态，全选或者全不选
                for (CartIndexBean.StoreListBean.GoodsBean bean :
                        ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getGoods()) {
                    if (CartFragment.canSelect(bean)) {
                        bean.setCheck(((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).isCheck());
                    }
                }
                notifyDataSetChanged();
                EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_MAIN_CART));
                if (mOnAmountChangeListener != null) {
                    mOnAmountChangeListener.onAmountChange(getTotalAmount());
                }
                if (mOnItemAllCheckedListener != null) {
                    mOnItemAllCheckedListener.allChecked(checkIsAllChecked());
                }
            }
        });
        viewHolder.tv_store_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, StoreActivity.class);
                intent.putExtra("store_id",((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getStore_id());
                mContext.startActivity(intent);
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ChildViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ChildViewHolder();
            convertView = View.inflate(mContext, R.layout.item_cart_goods, null);
            viewHolder.iv_check = convertView.findViewById(R.id.iv_check);
            viewHolder.iv_goods_img = convertView.findViewById(R.id.iv_goods_img);
            viewHolder.tv_goods_name = convertView.findViewById(R.id.tv_goods_name);
            viewHolder.tv_spec_name = convertView.findViewById(R.id.tv_spec_name);
            viewHolder.tv_goods_price = convertView.findViewById(R.id.tv_goods_price);
            viewHolder.tv_goods_price_rmb = convertView.findViewById(R.id.tv_goods_price_rmb);
            viewHolder.tv_invaild = convertView.findViewById(R.id.tv_invaild);
            viewHolder.tv_no_quotation = convertView.findViewById(R.id.tv_no_quotation);
            viewHolder.aevc_num = convertView.findViewById(R.id.aevc_num);
            viewHolder.rl_goods = convertView.findViewById(R.id.rl_goods);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ChildViewHolder) convertView.getTag();
        }

        final CartIndexBean.StoreListBean.GoodsBean goodsBean = ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().
                get(groupPosition).getGoods().get(childPosition);
        if(!TextUtils.isEmpty(goodsBean.getIs_second_hand()) && goodsBean.getIs_second_hand().equals("1")){
            viewHolder.tv_goods_name.setText(Utils.secondLabel(goodsBean.getGoods_name()));
        }else {
            viewHolder.tv_goods_name.setText(goodsBean.getGoods_name());
        }
        //viewHolder.tv_goods_name.setText(goodsBean.getGoods_name());
        Glide.with(mContext).load(goodsBean.getGoods_image())
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                .into(viewHolder.iv_goods_img);
        if (!TextUtils.isEmpty(goodsBean.getSpec_name())) {
            viewHolder.tv_spec_name.setText(goodsBean.getSpec_name());
        } else {
            viewHolder.tv_spec_name.setText("");
        }
        if (goodsBean.getGoods_state() != 1) {
            viewHolder.iv_check.setVisibility(View.INVISIBLE);
            viewHolder.iv_goods_img.setVisibility(View.VISIBLE);
            viewHolder.tv_goods_name.setVisibility(View.VISIBLE);
            viewHolder.tv_goods_name.setTextColor(mContext.getResources().getColor(R.color.grey_7));
            viewHolder.tv_goods_price.setVisibility(View.GONE);
            viewHolder.tv_goods_price_rmb.setVisibility(View.GONE);
            viewHolder.tv_invaild.setVisibility(View.VISIBLE);
            viewHolder.tv_no_quotation.setVisibility(View.VISIBLE);
            viewHolder.aevc_num.setVisibility(View.GONE);
            viewHolder.tv_invaild.setText(R.string.invaild);
        } else {
            viewHolder.iv_check.setVisibility(View.VISIBLE);
            viewHolder.iv_goods_img.setVisibility(View.VISIBLE);
            viewHolder.tv_goods_name.setVisibility(View.VISIBLE);
            viewHolder.tv_goods_name.setTextColor(mContext.getResources().getColor(R.color.grey_4));
            if(goodsBean.getBatch()==null||goodsBean.getBatch().size()==0){
                viewHolder.tv_goods_price.setVisibility(View.VISIBLE);
                viewHolder.tv_goods_price.setText(Utils.getCurrencyDisplayMoney(((KyyApp)(KyyApp.context)).cartIndexBean.getCurrencytype(),goodsBean.getGoods_price()));
                viewHolder.tv_goods_price_rmb.setVisibility(View.VISIBLE);
                viewHolder.tv_goods_price_rmb.setText("¥" + Utils.getDisplayMoney(goodsBean.getGoods_price()));
            }else {
                for(CartIndexBean.StoreListBean.GoodsBean.BatchBean batchBean :goodsBean.getBatch()){
                    if(batchBean.getMix()<=goodsBean.getGoods_num()&&goodsBean.getGoods_num()<batchBean.getMax()){
                        viewHolder.tv_goods_price.setVisibility(View.VISIBLE);
                        viewHolder.tv_goods_price.setText(Utils.getCurrencyDisplayMoney(((KyyApp)(KyyApp.context)).cartIndexBean.getCurrencytype(),batchBean.getValue()));
                        viewHolder.tv_goods_price_rmb.setVisibility(View.VISIBLE);
                        viewHolder.tv_goods_price_rmb.setText("¥" + Utils.getDisplayMoney(batchBean.getValue()));
                        break;
                    }
                }
            }

            viewHolder.tv_invaild.setVisibility(View.INVISIBLE);
            viewHolder.tv_no_quotation.setVisibility(View.GONE);
            viewHolder.aevc_num.setVisibility(View.VISIBLE);
            viewHolder.aevc_num.setGoods_storage(goodsBean.getGoods_storage());
            if(goodsBean.isCheck()){
                viewHolder.iv_check.setImageResource(R.mipmap.pop_icon_tick);
            }else {
                viewHolder.iv_check.setImageResource(R.mipmap.address_icon_unselected_nor);
            }
            viewHolder.aevc_num.setMin_count(goodsBean.getGoods_min_count());
            viewHolder.aevc_num.setNum(goodsBean.getGoods_num());
            if (!TextUtils.isEmpty(goodsBean.getSpec_name())) {
                viewHolder.tv_spec_name.setText(goodsBean.getSpec_name());
            }
            if(goodsBean.getGoods_storage()==0 || (goodsBean.getBatch()!=null&&goodsBean.getBatch().size()>0&&goodsBean.getGoods_storage()<goodsBean.getBatch().get(0).getMix())){
                viewHolder.tv_invaild.setVisibility(View.VISIBLE);
                viewHolder.tv_invaild.setText(R.string.out_of_stock);
                viewHolder.aevc_num.setVisibility(View.GONE);
                viewHolder.iv_check.setVisibility(View.INVISIBLE);
            }

        }

        viewHolder.rl_goods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (goodsBean.getGoods_state() == 1||goodsBean.getGoods_state() == 0) {
                Intent intent = new Intent(mContext, GoodsActivity.class);
                intent.putExtra("goods_id", goodsBean.getGoods_id() + "");
                mContext.startActivity(intent);
           // }
            }
        });
        viewHolder.rl_goods.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_cart_menu,null);
                TextView tv_remove_to_collection = view.findViewById(R.id.tv_remove_to_collection);
                TextView tv_delete = view.findViewById(R.id.tv_delete);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                final Dialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.show();
                dialog.getWindow().setLayout(Utils.dp2px(mContext,250), LinearLayout.LayoutParams.WRAP_CONTENT);
                dialog.setContentView(view);
                tv_remove_to_collection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestRemoveToCollection(goodsBean.getCart_id());
                        dialog.dismiss();
                    }
                });
                tv_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogUtils.createTwoBtnDialog(mContext, mContext.getString(R.string.delete_goods_tip),
                                mContext.getString(R.string.dialog_confirm),
                                mContext.getString(R.string.dialog_cancel),
                                new DialogUtils.OnRightBtnListener() {
                                    @Override
                                    public void setOnRightListener(Dialog dialog) {
                                        requestDeleteSelectedGoods(goodsBean.getCart_id());
                                    }
                                }, new DialogUtils.OnLeftBtnListener() {
                                    @Override
                                    public void setOnLeftListener(Dialog dialog) {
                                    }
                                }, false, true);
                        dialog.dismiss();
                    }
                });
                return true;
            }
        });
        viewHolder.iv_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(goodsBean.isCheck()){
                    viewHolder.iv_check.setImageResource(R.mipmap.address_icon_unselected_nor);
                    goodsBean.setCheck(false);
                }else {
                    viewHolder.iv_check.setImageResource(R.mipmap.pop_icon_tick);
                    goodsBean.setCheck(true);
                }

                //遍历所有可点击的子item是否全部选中了
                boolean isAllChildCheck = true;
                for (CartIndexBean.StoreListBean.GoodsBean bean : ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getGoods()) {
                    if (CartFragment.canSelect(bean)) {
                        if (!bean.isCheck()) {
                            isAllChildCheck = false;
                            break;
                        }
                    }
                }
                //设置store是否选中
                ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).setCheck(isAllChildCheck);
                notifyDataSetChanged();
                EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_MAIN_CART));
                if (mOnAmountChangeListener != null) {
                    mOnAmountChangeListener.onAmountChange(getTotalAmount());
                }
                if (mOnItemAllCheckedListener != null) {
                    mOnItemAllCheckedListener.allChecked(checkIsAllChecked());
                }
            }
        });

        viewHolder.aevc_num.setOnAmountChangeListener(new AmountEditViewCar.OnAmountChangeListener() {
            @Override
            public void onAmountChange(View view, int amount) {
                requestGoodsNumUpdate(groupPosition,childPosition,amount);
            }
        });
        viewHolder.aevc_num.setOnNumClickListener(new AmountEditViewCar.OnNumClickListener() {
            @Override
            public void onNumClick(View view) {
                showChangeNumDialog(groupPosition,childPosition);
            }
        });

        return convertView;
    }

    /**
     * 子Item是否响应点击事件
     *
     * @param groupPosition
     * @param childPosition
     * @return
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


    private class GroupViewHolder {
        private TextView tv_store_name;
        private ImageView iv_check;
    }

    private class ChildViewHolder {
        private ImageView iv_goods_img,iv_check;
        private TextView tv_goods_name, tv_spec_name, tv_goods_price, tv_goods_price_rmb,
                tv_invaild, tv_no_quotation;
        private AmountEditViewCar aevc_num;
        private RelativeLayout rl_goods;
    }

    /**
     * 获取所有选中的子item总金额
     *
     * @return
     */
    public double getTotalAmount() {
        double amount = 0;
        for (CartIndexBean.StoreListBean storeListBean : ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list()) {
            for (CartIndexBean.StoreListBean.GoodsBean goodsBean : storeListBean.getGoods()) {
                if (goodsBean.isCheck()) {
                    if(goodsBean.getBatch()==null||goodsBean.getBatch().size()==0){
                        amount = amount + goodsBean.getGoods_price() * ((double) goodsBean.getGoods_num());
                    }else {
                        for (CartIndexBean.StoreListBean.GoodsBean.BatchBean batchBean:goodsBean.getBatch()){
                            if(batchBean.getMix()<=goodsBean.getGoods_num()&&goodsBean.getGoods_num()<batchBean.getMax()){
                                amount = amount + batchBean.getValue() * ((double) goodsBean.getGoods_num());
                                break;
                            }
                        }
                    }
                }
            }
        }
        return amount;
    }

    /**
     * 检查所有可选子item是否已经全选
     *
     * @return
     */
    public boolean checkIsAllChecked() {
        if (((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list()==null||((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().size()==0){
            return false;
        }
        boolean hasCanableSelect = false;//是否有可选的
        for (CartIndexBean.StoreListBean storeListBean : ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list()) {
            for (CartIndexBean.StoreListBean.GoodsBean goodsBean : storeListBean.getGoods()) {
                if (CartFragment.canSelect(goodsBean)) {
                    hasCanableSelect = true;
                    if (!goodsBean.isCheck()) {
                        return false;
                    }
                }
            }
        }
        if(hasCanableSelect){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 采购单页面全选CheckBox通知子Item刷新
     * 设置所有可选子item全选或者全不选
     *
     * @param isCheck
     * @param listener
     */
    public void setAllCheckMode(boolean isCheck, OnAllCheckModeChangeListener listener) {
        for (CartIndexBean.StoreListBean storeListBean : ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list()) {
            storeListBean.setCheck(isCheck);
            for (CartIndexBean.StoreListBean.GoodsBean goodsBean : storeListBean.getGoods()) {
                if (CartFragment.canSelect(goodsBean)) {
                    goodsBean.setCheck(isCheck);
                }
            }
        }
        notifyDataSetChanged();
        EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_MAIN_CART));
        //计算价格
        listener.onChange(getTotalAmount());

    }

    public interface OnAllCheckModeChangeListener {
        void onChange(double totalPrice);
    }

    //总金额发送改变的监听
    public interface OnAmountChangeListener {
        void onAmountChange(double totalPrice);
    }

    private OnAmountChangeListener mOnAmountChangeListener;

    public void setOnAmountChangeListener(OnAmountChangeListener listener) {
        this.mOnAmountChangeListener = listener;
    }

    //item全选或不是全选通知采购单页面更新
    public interface onItemAllCheckedListener {
        void allChecked(boolean IsAllChecked);
    }

    private onItemAllCheckedListener mOnItemAllCheckedListener;

    public void setOnItemAllCheckedListener(onItemAllCheckedListener listener) {
        this.mOnItemAllCheckedListener = listener;
    }

    /**
     * 编辑模式下的删除
     * oneCartId 这个有值就是长按删除
     */
    public void requestDeleteSelectedGoods(final String oneCartId){
        final String cartId = getSeletedCartId();
        DialogUtils.createTipAllLoadDialog(mContext);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CLEAR_CART())
                .addParam("cart_id", TextUtils.isEmpty(oneCartId)?cartId:oneCartId)
                .withPOST(new NetCallBack<DeleteGoodsBean>() {

                    @NotNull
                    @Override
                    public Class<DeleteGoodsBean> getRealType() {
                        return DeleteGoodsBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(((Activity)mContext).isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(mContext, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull DeleteGoodsBean deleteGoodsBean) {
                        if(((Activity)mContext).isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        if (deleteGoodsBean.getDel() == 1) {
                            deleteGoods(TextUtils.isEmpty(oneCartId)?cartId:oneCartId);
                            DialogUtils.createTipImageAndTextDialog(mContext,
                                    mContext.getString(R.string.delete_succeful),
                                    R.mipmap.png_icon_popup_ok);
                        }
                    }
                }, false);
    }

    /**
     * 移到收藏夹,id是cartId
     * id有值是收藏长按收藏
     * 没值是多选收藏
     */
    public void requestRemoveToCollection(String id) {
        final String cartId;
        if(TextUtils.isEmpty(id)){
            cartId = getSeletedCartId();
        }else {
            cartId = id;
        }
        if(!TextUtils.isEmpty(cartId)){
            DialogUtils.createTipAllLoadDialog(mContext);
            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_FAVORITE())
                    .addParam("fid", cartId)
                    .addParam("type", "0")
                    .addParam("fav_type", "3")
                    .withPOST(new NetCallBack<String>() {

                        @NotNull
                        @Override
                        public Class<String> getRealType() {
                            return String.class;
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(((Activity)mContext).isFinishing()){
                                return;
                            }
                            DialogUtils.moven();
                            Toast.makeText(mContext, err, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onSuccess(@NonNull String str) {
                            if(((Activity)mContext).isFinishing()){
                                return;
                            }
                            DialogUtils.moven();
                            deleteGoods(cartId);
                            DialogUtils.createTipImageAndTextDialog(mContext,
                                    mContext.getString(R.string.remove_succeful),
                                    R.mipmap.png_icon_popup_ok);
                        }
                    }, false);
        }else {
            Toast.makeText(mContext, mContext.getString(R.string.no_select_goods), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 购物车数量添加
     */
    private void requestGoodsNumUpdate(final int groupPosition, final int childPositin, final int amount) {
        DialogUtils.createTipAllLoadDialog(mContext);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CART_UPDATE_NUM())
                .addParam("cart_id", ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getGoods().get(childPositin).getCart_id())
                .addParam("quantity", amount)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(((Activity)mContext).isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(mContext, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(((Activity)mContext).isFinishing()){
                            return;
                        }
                        ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getGoods().get(childPositin).setGoods_num(amount);
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_MAIN_CART));
                        DialogUtils.moven();
                        notifyDataSetChanged();
                        if (((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getGoods().get(childPositin).isCheck()) {
                            //通知金额改变
                            if (mOnAmountChangeListener != null) {
                                mOnAmountChangeListener.onAmountChange(getTotalAmount());
                            }
                        }
                    }
                }, false);
    }

    /**
     * 移除收藏或者删除完成的商品
     * @param cartIds
     */
    private void deleteGoods(String cartIds) {
        String[] cartIdArray= cartIds.split(",");
        boolean findCartId = false;
        for(String cartId:cartIdArray){
            findCartId = false;
            for (CartIndexBean.StoreListBean storeListBean : ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list()) {
                for (CartIndexBean.StoreListBean.GoodsBean goodsBean : storeListBean.getGoods()) {
                    if(goodsBean.getCart_id().equals(cartId)){
                        storeListBean.getGoods().remove(goodsBean);
                        findCartId = true;
                        break;
                    }
                }
                if(findCartId){
                    if(storeListBean.getGoods().size()==0){
                        ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().remove(storeListBean);
                    }else {
                        for(CartIndexBean.StoreListBean.GoodsBean goodsBean:storeListBean.getGoods()){
                            if(goodsBean.getGoods_storage()>0&&goodsBean.getGoods_state()==1){
                                //有可选的就直接跳出循环
                                break;
                            }
                            //都是不可选的情况，设置父item是未选中状态
                            storeListBean.setCheck(false);
                        }
                    }
                    break;
                }
            }
        }
        notifyDataSetChanged();
        EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_MAIN_CART));
        if (mOnAmountChangeListener != null) {
            mOnAmountChangeListener.onAmountChange(getTotalAmount());
        }
        if (mOnItemAllCheckedListener != null) {
            mOnItemAllCheckedListener.allChecked(checkIsAllChecked());
        }
    }

    /**
     *
     * @return 选中的cartId
     */
    public String getSeletedCartId(){
        StringBuilder cartId = new StringBuilder();
        for (CartIndexBean.StoreListBean storeListBean : ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list()) {
            for (CartIndexBean.StoreListBean.GoodsBean goodsBean : storeListBean.getGoods()) {
                if(goodsBean.isCheck()){
                    cartId.append(goodsBean.getCart_id());
                    cartId.append(",");
                }
            }
        }
        if(!TextUtils.isEmpty(cartId)){
            cartId.deleteCharAt(cartId.length()-1);
        }
        return cartId.toString();
    }

    /**
     *
     * @return 选中的goodsId
     */
    private String getSeletedGoodsId(){
        StringBuilder goodsId = new StringBuilder();
        for (CartIndexBean.StoreListBean storeListBean : ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list()) {
            for (CartIndexBean.StoreListBean.GoodsBean goodsBean : storeListBean.getGoods()) {
                if(goodsBean.isCheck()){
                    goodsId.append(goodsBean.getGoods_id());
                    goodsId.append(",");
                }
            }
        }
        if(!TextUtils.isEmpty(goodsId)){
            goodsId.deleteCharAt(goodsId.length()-1);
        }
        return goodsId.toString();
    }


    /*
     * @return 选中goodsIdandNum
     */
    public String getSeletedCartIdandNum(){
        StringBuilder goodsCartIdNum = new StringBuilder();
        for (CartIndexBean.StoreListBean storeListBean : ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list()) {
            for (CartIndexBean.StoreListBean.GoodsBean goodsBean : storeListBean.getGoods()) {
                if(goodsBean.isCheck()){
                    goodsCartIdNum.append(goodsBean.getCart_id()+"|"+goodsBean.getGoods_num());
                    goodsCartIdNum.append(",");
                }
            }
        }
        if(!TextUtils.isEmpty(goodsCartIdNum)){
            goodsCartIdNum.deleteCharAt(goodsCartIdNum.length()-1);
        }
        return goodsCartIdNum.toString();
    }

    private void showChangeNumDialog(final int groupPosition, final int childPosition){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        mNumUpdateDialog = builder.create();
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_cart_num_update,null);
        final EditText et_num = view.findViewById(R.id.et_num);
        et_num.requestFocus();
        Button bt_cancel = view.findViewById(R.id.bt_cancel);
        Button bt_confirm = view.findViewById(R.id.bt_confirm);
        mNumUpdateDialog.setCanceledOnTouchOutside(true);
        mNumUpdateDialog.setCancelable(true);
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNumUpdateDialog.dismiss();
                et_num.setText("");
            }
        });
        bt_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numStr = et_num.getText().toString().trim();
                if(!TextUtils.isEmpty(numStr)){
                    int num = Integer.valueOf(numStr);
                    if(num<=((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getGoods().get(childPosition).getGoods_storage()){
                        requestGoodsNumUpdate(groupPosition,childPosition,num);
                        mNumUpdateDialog.dismiss();
                        et_num.setText("");
                    }else {
                        Toast.makeText(mContext,mContext.getString(R.string.count_beyond)
                                +((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(groupPosition).getGoods().get(childPosition).getGoods_storage(),Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        mNumUpdateDialog.show();
        //setContentView一定要放在show后面才行显示出页面

        //弹出软键盘
        mNumUpdateDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        mNumUpdateDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        mNumUpdateDialog.setContentView(view);
    }

}
