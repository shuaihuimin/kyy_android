package com.xht.kuaiyouyi.ui.cart.entity;

import java.util.List;

public class ConfirmOrderBean {
    /**
     * data_list : {"cart_id":["100002|1"],"address_list":{"is_default":"1","address":"澳门豆捞坊","city_id":"534","mob_phone":"18334712869","mobile_zone1":"86","member_id":"101","area_info":"澳門 澳門特別行政區 ","true_name":"帅慧敏","area_id":"34","address_id":"284"},"store_goods_total":[63200],"company_count_money":0,"goods_price":63200,"freight_price":0.54,"customs_charges_fee":0,"buy_company_id":0,"buy_member_id":"101","store_list":[{"store_id":"1","store_name":"快优易自营","is_own_shop":"1","default_im":"1","cart_list":[{"goods_name":"劳力士Rolex MILGAUSS 116400GV-72400 自动机械钢带男表联保正品","goods_num":1,"profit_rate":"0","goods_total":63200,"goods_price":"63200.00","goods_id":100002,"customs_charges_fee":0,"freight_fee":"0.54","goods_image_url":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627750479728_240.png"}]}],"all_price":63200.54,"company_remain_money":0}
     * bank_list : [{"sub":[{"sub":[{"id":"3","parent_id":"2","currency_type":"HKD","name":"港币","account_name":"珠海新海通电子商务有限公司","symbol":"HK$"}],"id":"2","parent_id":"1","name":"澳门骑士路支行"},{"sub":[{"id":"6","parent_id":"5","currency_type":"HKD","name":"港币","account_name":"珠海新海通电子商务有限公司","symbol":"HK$"}],"id":"5","parent_id":"1","name":"中国银行澳门支行1"}],"id":"1","name":"澳门"}]
     * currencytype : {"USD":{"rate":0.1454,"name":"美元","currency":"USD","symbol":"US$","cackeKey":"cacheCurrencyUSD"},"MOP":{"rate":1.17405,"name":"澳门币","currency":"MOP","symbol":"MOP$","cackeKey":"cacheCurrencyMOP"},"CNY":{"rate":1,"name":"人民币","currency":"CNY","symbol":"￥","cackeKey":""},"HKD":{"rate":1.1414,"name":"港币","currency":"HKD","symbol":"HK$","cackeKey":"cacheCurrencyHKD"}}
     * pay_check_list : [{"rate":"1.14140","text":"~HK$","name":"港币","currency":"HKD","symbol":"HK$","cackeKey":"cacheCurrencyHKD"},{"rate":"1.17405","text":"~MOP$","name":"澳门币","currency":"MOP","symbol":"MOP$","cackeKey":"cacheCurrencyMOP"}]
     */

    private DataListBean data_list;
    private CurrencytypeBean currencytype;
    private List<BankListBean> bank_list;
    private List<PayCheckListBean> pay_check_list;

    public DataListBean getData_list() {
        return data_list;
    }

    public void setData_list(DataListBean data_list) {
        this.data_list = data_list;
    }

    public CurrencytypeBean getCurrencytype() {
        return currencytype;
    }

    public void setCurrencytype(CurrencytypeBean currencytype) {
        this.currencytype = currencytype;
    }

    public List<BankListBean> getBank_list() {
        return bank_list;
    }

    public void setBank_list(List<BankListBean> bank_list) {
        this.bank_list = bank_list;
    }

    public List<PayCheckListBean> getPay_check_list() {
        return pay_check_list;
    }

    public void setPay_check_list(List<PayCheckListBean> pay_check_list) {
        this.pay_check_list = pay_check_list;
    }

    public static class DataListBean {
        /**
         * cart_id : ["100002|1"]
         * address_list : {"is_default":"1","address":"澳门豆捞坊","city_id":"534","mob_phone":"18334712869","mobile_zone1":"86","member_id":"101","area_info":"澳門 澳門特別行政區 ","true_name":"帅慧敏","area_id":"34","address_id":"284"}
         * store_goods_total : [63200]
         * company_count_money : 0
         * goods_price : 63200
         * freight_price : 0.54
         * customs_charges_fee : 0
         * buy_company_id : 0
         * buy_member_id : 101
         * store_list : [{"store_id":"1","store_name":"快优易自营","is_own_shop":"1","default_im":"1","cart_list":[{"goods_name":"劳力士Rolex MILGAUSS 116400GV-72400 自动机械钢带男表联保正品","goods_num":1,"profit_rate":"0","goods_total":63200,"goods_price":"63200.00","goods_id":100002,"customs_charges_fee":0,"freight_fee":"0.54","goods_image_url":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627750479728_240.png"}]}]
         * all_price : 63200.54
         * company_remain_money : 0
         */

        private AddressListBean address_list;
        private String company_count_money;
        private String goods_price;
        private double freight_price;
        private int customs_charges_fee;
        private int buy_company_id;
        private String buy_member_id;
        private String ifcart;
        private double all_price;
        private String company_remain_money;
        private List<String> cart_id;
        private List<Double> store_goods_total;
        private List<StoreListBean> store_list;
        private String order_main_pay_one;
        private String order_main_pay_two;
        private String supplement_amount;
        private String order_main_pay_first;
        private String order_main_pay_end;
        private String is_enabled_approve;

        public String getIs_enabled_approve() {
            return is_enabled_approve;
        }

        public void setIs_enabled_approve(String is_enabled_approve) {
            this.is_enabled_approve = is_enabled_approve;
        }

        public String getOrder_main_pay_one() {
            return order_main_pay_one;
        }

        public void setOrder_main_pay_one(String order_main_pay_one) {
            this.order_main_pay_one = order_main_pay_one;
        }

        public String getOrder_main_pay_two() {
            return order_main_pay_two;
        }

        public void setOrder_main_pay_two(String order_main_pay_two) {
            this.order_main_pay_two = order_main_pay_two;
        }

        public String getSupplement_amount() {
            return supplement_amount;
        }

        public void setSupplement_amount(String supplement_amount) {
            this.supplement_amount = supplement_amount;
        }

        public String getOrder_main_pay_first() {
            return order_main_pay_first;
        }

        public void setOrder_main_pay_first(String order_main_pay_first) {
            this.order_main_pay_first = order_main_pay_first;
        }

        public String getOrder_main_pay_end() {
            return order_main_pay_end;
        }

        public void setOrder_main_pay_end(String order_main_pay_end) {
            this.order_main_pay_end = order_main_pay_end;
        }

        public AddressListBean getAddress_list() {
            return address_list;
        }

        public void setAddress_list(AddressListBean address_list) {
            this.address_list = address_list;
        }

        public String getCompany_count_money() {
            return company_count_money;
        }

        public void setCompany_count_money(String company_count_money) {
            this.company_count_money = company_count_money;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public double getFreight_price() {
            return freight_price;
        }

        public void setFreight_price(double freight_price) {
            this.freight_price = freight_price;
        }

        public int getCustoms_charges_fee() {
            return customs_charges_fee;
        }

        public void setCustoms_charges_fee(int customs_charges_fee) {
            this.customs_charges_fee = customs_charges_fee;
        }

        public String getIfcart() {
            return ifcart;
        }

        public void setIfcart(String ifcart) {
            this.ifcart = ifcart;
        }

        public int getBuy_company_id() {
            return buy_company_id;
        }

        public void setBuy_company_id(int buy_company_id) {
            this.buy_company_id = buy_company_id;
        }

        public String getBuy_member_id() {
            return buy_member_id;
        }

        public void setBuy_member_id(String buy_member_id) {
            this.buy_member_id = buy_member_id;
        }

        public double getAll_price() {
            return all_price;
        }

        public void setAll_price(double all_price) {
            this.all_price = all_price;
        }

        public String getCompany_remain_money() {
            return company_remain_money;
        }

        public void setCompany_remain_money(String company_remain_money) {
            this.company_remain_money = company_remain_money;
        }

        public List<String> getCart_id() {
            return cart_id;
        }

        public void setCart_id(List<String> cart_id) {
            this.cart_id = cart_id;
        }

        public List<Double> getStore_goods_total() {
            return store_goods_total;
        }

        public void setStore_goods_total(List<Double> store_goods_total) {
            this.store_goods_total = store_goods_total;
        }

        public List<StoreListBean> getStore_list() {
            return store_list;
        }

        public void setStore_list(List<StoreListBean> store_list) {
            this.store_list = store_list;
        }

        public static class AddressListBean {
            /**
             * is_default : 1
             * address : 澳门豆捞坊
             * city_id : 534
             * mob_phone : 18334712869
             * mobile_zone1 : 86
             * member_id : 101
             * area_info : 澳門 澳門特別行政區
             * true_name : 帅慧敏
             * area_id : 34
             * address_id : 284
             */

            private String is_default;
            private String address;
            private String city_id;
            private String mob_phone;
            private String mobile_zone1;
            private String member_id;
            private String area_info;
            private String true_name;
            private String area_id;
            private String address_id;

            public String getIs_default() {
                return is_default;
            }

            public void setIs_default(String is_default) {
                this.is_default = is_default;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCity_id() {
                return city_id;
            }

            public void setCity_id(String city_id) {
                this.city_id = city_id;
            }

            public String getMob_phone() {
                return mob_phone;
            }

            public void setMob_phone(String mob_phone) {
                this.mob_phone = mob_phone;
            }

            public String getMobile_zone1() {
                return mobile_zone1;
            }

            public void setMobile_zone1(String mobile_zone1) {
                this.mobile_zone1 = mobile_zone1;
            }

            public String getMember_id() {
                return member_id;
            }

            public void setMember_id(String member_id) {
                this.member_id = member_id;
            }

            public String getArea_info() {
                return area_info;
            }

            public void setArea_info(String area_info) {
                this.area_info = area_info;
            }

            public String getTrue_name() {
                return true_name;
            }

            public void setTrue_name(String true_name) {
                this.true_name = true_name;
            }

            public String getArea_id() {
                return area_id;
            }

            public void setArea_id(String area_id) {
                this.area_id = area_id;
            }

            public String getAddress_id() {
                return address_id;
            }

            public void setAddress_id(String address_id) {
                this.address_id = address_id;
            }
        }

        public static class StoreListBean {
            /**
             * store_id : 1
             * store_name : 快优易自营
             * is_own_shop : 1
             * default_im : 1
             * cart_list : [{"goods_name":"劳力士Rolex MILGAUSS 116400GV-72400 自动机械钢带男表联保正品","goods_num":1,"profit_rate":"0","goods_total":63200,"goods_price":"63200.00","goods_id":100002,"customs_charges_fee":0,"freight_fee":"0.54","goods_image_url":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627750479728_240.png"}]
             */

            private String store_id;
            private String default_avatar;
            private String store_name;
            private String is_own_shop;
            private String default_im;
            private List<CartListBean> cart_list;

            public String getStore_id() {
                return store_id;
            }

            public void setStore_id(String store_id) {
                this.store_id = store_id;
            }

            public String getDefault_avatar() {
                return default_avatar;
            }

            public void setDefault_avatar(String default_avatar) {
                this.default_avatar = default_avatar;
            }

            public String getStore_name() {
                return store_name;
            }

            public void setStore_name(String store_name) {
                this.store_name = store_name;
            }

            public String getIs_own_shop() {
                return is_own_shop;
            }

            public void setIs_own_shop(String is_own_shop) {
                this.is_own_shop = is_own_shop;
            }

            public String getDefault_im() {
                return default_im;
            }

            public void setDefault_im(String default_im) {
                this.default_im = default_im;
            }

            public List<CartListBean> getCart_list() {
                return cart_list;
            }

            public void setCart_list(List<CartListBean> cart_list) {
                this.cart_list = cart_list;
            }

            public static class CartListBean {
                /**
                 * goods_name : 劳力士Rolex MILGAUSS 116400GV-72400 自动机械钢带男表联保正品
                 * goods_num : 1
                 * profit_rate : 0
                 * goods_total : 63200
                 * goods_price : 63200.00
                 * goods_id : 100002
                 * customs_charges_fee : 0
                 * freight_fee : 0.54
                 * goods_image_url : http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627750479728_240.png
                 */

                private String goods_name;
                private int goods_num;
                private String profit_rate;
                private String goods_total;
                private String goods_price;
                private int goods_id;
                private String customs_charges_fee;
                private String freight_fee;
                private String goods_image_url;
                private String goods_spec;
                private int is_second_hand;

                public int getIs_second_hand() {
                    return is_second_hand;
                }

                public void setIs_second_hand(int is_second_hand) {
                    this.is_second_hand = is_second_hand;
                }

                public String getGoods_name() {
                    return goods_name;
                }

                public void setGoods_name(String goods_name) {
                    this.goods_name = goods_name;
                }

                public int getGoods_num() {
                    return goods_num;
                }

                public void setGoods_num(int goods_num) {
                    this.goods_num = goods_num;
                }

                public String getProfit_rate() {
                    return profit_rate;
                }

                public void setProfit_rate(String profit_rate) {
                    this.profit_rate = profit_rate;
                }

                public String getGoods_total() {
                    return goods_total;
                }

                public void setGoods_total(String goods_total) {
                    this.goods_total = goods_total;
                }

                public String getGoods_price() {
                    return goods_price;
                }

                public void setGoods_price(String goods_price) {
                    this.goods_price = goods_price;
                }

                public int getGoods_id() {
                    return goods_id;
                }

                public void setGoods_id(int goods_id) {
                    this.goods_id = goods_id;
                }

                public String getCustoms_charges_fee() {
                    return customs_charges_fee;
                }

                public void setCustoms_charges_fee(String customs_charges_fee) {
                    this.customs_charges_fee = customs_charges_fee;
                }

                public String getFreight_fee() {
                    return freight_fee;
                }

                public void setFreight_fee(String freight_fee) {
                    this.freight_fee = freight_fee;
                }

                public String getGoods_image_url() {
                    return goods_image_url;
                }

                public void setGoods_image_url(String goods_image_url) {
                    this.goods_image_url = goods_image_url;
                }

                public String getGoods_spec() {
                    return goods_spec;
                }

                public void setGoods_spec(String goods_spec) {
                    this.goods_spec = goods_spec;
                }
            }
        }
    }

    public static class CurrencytypeBean {
        /**
         * USD : {"rate":0.1454,"name":"美元","currency":"USD","symbol":"US$","cackeKey":"cacheCurrencyUSD"}
         * MOP : {"rate":1.17405,"name":"澳门币","currency":"MOP","symbol":"MOP$","cackeKey":"cacheCurrencyMOP"}
         * CNY : {"rate":1,"name":"人民币","currency":"CNY","symbol":"￥","cackeKey":""}
         * HKD : {"rate":1.1414,"name":"港币","currency":"HKD","symbol":"HK$","cackeKey":"cacheCurrencyHKD"}
         */

        private USDBean USD;
        private MOPBean MOP;
        private CNYBean CNY;
        private HKDBean HKD;

        public USDBean getUSD() {
            return USD;
        }

        public void setUSD(USDBean USD) {
            this.USD = USD;
        }

        public MOPBean getMOP() {
            return MOP;
        }

        public void setMOP(MOPBean MOP) {
            this.MOP = MOP;
        }

        public CNYBean getCNY() {
            return CNY;
        }

        public void setCNY(CNYBean CNY) {
            this.CNY = CNY;
        }

        public HKDBean getHKD() {
            return HKD;
        }

        public void setHKD(HKDBean HKD) {
            this.HKD = HKD;
        }

        public static class USDBean {
            /**
             * rate : 0.1454
             * name : 美元
             * currency : USD
             * symbol : US$
             * cackeKey : cacheCurrencyUSD
             */

            private double rate;
            private String name;
            private String currency;
            private String symbol;
            private String cackeKey;

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }
        }

        public static class MOPBean {
            /**
             * rate : 1.17405
             * name : 澳门币
             * currency : MOP
             * symbol : MOP$
             * cackeKey : cacheCurrencyMOP
             */

            private double rate;
            private String name;
            private String currency;
            private String symbol;
            private String cackeKey;

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }
        }

        public static class CNYBean {
            /**
             * rate : 1
             * name : 人民币
             * currency : CNY
             * symbol : ￥
             * cackeKey :
             */

            private int rate;
            private String name;
            private String currency;
            private String symbol;
            private String cackeKey;

            public int getRate() {
                return rate;
            }

            public void setRate(int rate) {
                this.rate = rate;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }
        }

        public static class HKDBean {
            /**
             * rate : 1.1414
             * name : 港币
             * currency : HKD
             * symbol : HK$
             * cackeKey : cacheCurrencyHKD
             */

            private double rate;
            private String name;
            private String currency;
            private String symbol;
            private String cackeKey;

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }
        }
    }

    public static class BankListBean {
        /**
         * sub : [{"sub":[{"id":"3","parent_id":"2","currency_type":"HKD","name":"港币","account_name":"珠海新海通电子商务有限公司","symbol":"HK$"}],"id":"2","parent_id":"1","name":"澳门骑士路支行"},{"sub":[{"id":"6","parent_id":"5","currency_type":"HKD","name":"港币","account_name":"珠海新海通电子商务有限公司","symbol":"HK$"}],"id":"5","parent_id":"1","name":"中国银行澳门支行1"}]
         * id : 1
         * name : 澳门
         */

        private String id;
        private String name;
        private List<SubBeanX> sub;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<SubBeanX> getSub() {
            return sub;
        }

        public void setSub(List<SubBeanX> sub) {
            this.sub = sub;
        }

        public static class SubBeanX {
            /**
             * sub : [{"id":"3","parent_id":"2","currency_type":"HKD","name":"港币","account_name":"珠海新海通电子商务有限公司","symbol":"HK$"}]
             * id : 2
             * parent_id : 1
             * name : 澳门骑士路支行
             */

            private String id;
            private String parent_id;
            private String name;
            private List<SubBean> sub;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getParent_id() {
                return parent_id;
            }

            public void setParent_id(String parent_id) {
                this.parent_id = parent_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public List<SubBean> getSub() {
                return sub;
            }

            public void setSub(List<SubBean> sub) {
                this.sub = sub;
            }

            public static class SubBean {
                /**
                 * id : 3
                 * parent_id : 2
                 * currency_type : HKD
                 * name : 港币
                 * account_name : 珠海新海通电子商务有限公司
                 * symbol : HK$
                 */

                private String id;
                private String parent_id;
                private String currency_type;
                private String name;
                private String account_name;
                private String symbol;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getParent_id() {
                    return parent_id;
                }

                public void setParent_id(String parent_id) {
                    this.parent_id = parent_id;
                }

                public String getCurrency_type() {
                    return currency_type;
                }

                public void setCurrency_type(String currency_type) {
                    this.currency_type = currency_type;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getAccount_name() {
                    return account_name;
                }

                public void setAccount_name(String account_name) {
                    this.account_name = account_name;
                }

                public String getSymbol() {
                    return symbol;
                }

                public void setSymbol(String symbol) {
                    this.symbol = symbol;
                }
            }
        }
    }

    public static class PayCheckListBean {
        /**
         * rate : 1.14140
         * text : ~HK$
         * name : 港币
         * currency : HKD
         * symbol : HK$
         * cackeKey : cacheCurrencyHKD
         */

        private String rate;
        private String text;
        private String name;
        private String currency;
        private String symbol;
        private String cackeKey;

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getCackeKey() {
            return cackeKey;
        }

        public void setCackeKey(String cackeKey) {
            this.cackeKey = cackeKey;
        }
    }
}
