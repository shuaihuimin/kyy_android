package com.xht.kuaiyouyi.ui.search.entity;

/**
 * Created by shuaihuimin on 2018/6/29.
 */

public class ScreenHeadBenn {
    private String name;
    private String id;

    public ScreenHeadBenn(String id,String name){
        this.id=id;
        this.name=name;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
}
