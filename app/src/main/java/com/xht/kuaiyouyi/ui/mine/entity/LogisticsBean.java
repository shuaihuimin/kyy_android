package com.xht.kuaiyouyi.ui.mine.entity;

import java.util.List;

public class LogisticsBean {


    private List<LogisticsInfoBean> logistics_info;

    public List<LogisticsInfoBean> getLogistics_info() {
        return logistics_info;
    }

    public void setLogistics_info(List<LogisticsInfoBean> logistics_info) {
        this.logistics_info = logistics_info;
    }

    public static class LogisticsInfoBean {
        /**
         * order_logistic_text : 您的订单正在配送途中，请您准备签收（配送员：李伟，电话：13823078888），感谢您的耐心等待
         * order_logistic_person_id : 1
         * order_logistic_crtime : 1533608252
         * order_logistic_status : 1
         * order_logistic_type : 40
         */

        private String order_logistic_text;
        private String order_logistic_person_id;
        private String order_logistic_crtime;
        private String order_logistic_status;
        private String order_logistic_type;

        public String getOrder_logistic_text() {
            return order_logistic_text;
        }

        public void setOrder_logistic_text(String order_logistic_text) {
            this.order_logistic_text = order_logistic_text;
        }

        public String getOrder_logistic_person_id() {
            return order_logistic_person_id;
        }

        public void setOrder_logistic_person_id(String order_logistic_person_id) {
            this.order_logistic_person_id = order_logistic_person_id;
        }

        public String getOrder_logistic_crtime() {
            return order_logistic_crtime;
        }

        public void setOrder_logistic_crtime(String order_logistic_crtime) {
            this.order_logistic_crtime = order_logistic_crtime;
        }

        public String getOrder_logistic_status() {
            return order_logistic_status;
        }

        public void setOrder_logistic_status(String order_logistic_status) {
            this.order_logistic_status = order_logistic_status;
        }

        public String getOrder_logistic_type() {
            return order_logistic_type;
        }

        public void setOrder_logistic_type(String order_logistic_type) {
            this.order_logistic_type = order_logistic_type;
        }
    }
}
