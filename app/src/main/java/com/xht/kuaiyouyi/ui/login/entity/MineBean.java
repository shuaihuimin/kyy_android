package com.xht.kuaiyouyi.ui.login.entity;

public class MineBean {

        private String name;
        private String avatar;
        private MemberGradeBean member_grade;
        private String member_mobile;
        private String uid;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public MemberGradeBean getMember_grade() {
            return member_grade;
        }

        public void setMember_grade(MemberGradeBean member_grade) {
            this.member_grade = member_grade;
        }

        public String getMember_mobile() {
            return member_mobile;
        }

        public void setMember_mobile(String member_mobile) {
            this.member_mobile = member_mobile;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public static class MemberGradeBean {
            /**
             * level : 1
             * level_name : V1
             * exppoints : 1000
             */

            private int level;
            private String level_name;
            private int exppoints;

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public String getLevel_name() {
                return level_name;
            }

            public void setLevel_name(String level_name) {
                this.level_name = level_name;
            }

            public int getExppoints() {
                return exppoints;
            }

            public void setExppoints(int exppoints) {
                this.exppoints = exppoints;
            }
        }

}
