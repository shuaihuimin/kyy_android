package com.xht.kuaiyouyi.ui.home.entity;

import java.util.List;

public class BrandListBean {


    private List<BrandBean> brand;

    public List<BrandBean> getBrand() {
        return brand;
    }

    public void setBrand(List<BrandBean> brand) {
        this.brand = brand;
    }

    public static class BrandBean {
        /**
         * initial : A
         * data : [{"brand_id":"282","brand_name":"澳优","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399881087936122_sm.jpg"},{"brand_id":"274","brand_name":"嗳呵","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399879712252398_sm.jpg"},{"brand_id":"226","brand_name":"艾力斯特","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399835435836906_sm.jpg"},{"brand_id":"238","brand_name":"奥克斯","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399838633002147_sm.jpg"},{"brand_id":"339","brand_name":"安睡宝","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399903832203331_sm.jpg"},{"brand_id":"335","brand_name":"爱仕达","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399903259361371_sm.jpg"},{"brand_id":"348","brand_name":"奥唯嘉（Ovega）","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04431812331259168_sm.jpg"},{"brand_id":"310","brand_name":"艾威","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04399890796771131_sm.jpg"},{"brand_id":"140","brand_name":"爱丽","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04398103883736888_sm.jpg"},{"brand_id":"84","brand_name":"阿迪达斯","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04397471910652190_sm.jpg"},{"brand_id":"101","brand_name":"爱帝","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04398090218578648_sm.jpg"},{"brand_id":"192","brand_name":"爱普生","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04398158266047493_sm.jpg"},{"brand_id":"211","brand_name":"阿尔卡特","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04398161143888870_sm.jpg"},{"brand_id":"156","brand_name":"爱卡","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04398118792328978_sm.jpg"},{"brand_id":"174","brand_name":"acer","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04398155389308089_sm.jpg"},{"brand_id":"170","brand_name":"阿玛尼","brand_initial":"A","brand_pic":"http://192.168.0.2/data/upload/shop/brand/04398121209932680_sm.jpg"}]
         */

        private String initial;
        private List<DataBean> data;

        public String getInitial() {
            return initial;
        }

        public void setInitial(String initial) {
            this.initial = initial;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * brand_id : 282
             * brand_name : 澳优
             * brand_initial : A
             * brand_pic : http://192.168.0.2/data/upload/shop/brand/04399881087936122_sm.jpg
             */

            private String brand_id;
            private String brand_name;
            private String brand_initial;
            private String brand_pic;

            public String  getBrand_id() {
                return brand_id;
            }

            public void setBrand_id(String brand_id) {
                this.brand_id = brand_id;
            }

            public String getBrand_name() {
                return brand_name;
            }

            public void setBrand_name(String brand_name) {
                this.brand_name = brand_name;
            }

            public String getBrand_initial() {
                return brand_initial;
            }

            public void setBrand_initial(String brand_initial) {
                this.brand_initial = brand_initial;
            }

            public String getBrand_pic() {
                return brand_pic;
            }

            public void setBrand_pic(String brand_pic) {
                this.brand_pic = brand_pic;
            }
        }
    }
}
