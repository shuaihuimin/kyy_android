package com.xht.kuaiyouyi.ui.cart.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.cart.entity.ConfirmOrderBean;
import com.xht.kuaiyouyi.ui.cart.order.CommodityListActivity;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.goods.StoreActivity;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.activity.ChatActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderGoodsAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private ConfirmOrderBean.DataListBean mdataListBean;

    public OrderGoodsAdapter(Context context, ConfirmOrderBean.DataListBean dataListBean) {
        this.mContext = context;
        this.mdataListBean = dataListBean;
    }

    @Override
    public int getGroupCount() {
        if (mdataListBean.getStore_list() != null && mdataListBean.getStore_list().size() > 0) {
            return mdataListBean.getStore_list().size();
        }
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (mdataListBean.getStore_list().get(groupPosition).getCart_list() != null ) {
            return 1;
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mdataListBean.getStore_list().get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder groupViewHolder;
        if(convertView==null){
            convertView = View.inflate(mContext, R.layout.item_confirmorder_goods_head, null);
            groupViewHolder=new GroupViewHolder(convertView);
            convertView.setTag(groupViewHolder);
        }else {
            groupViewHolder= (GroupViewHolder) convertView.getTag();
        }
        groupViewHolder.tv_store_name.setText(mdataListBean.getStore_list().get(groupPosition).getStore_name());
        final String store_id=mdataListBean.getStore_list().get(groupPosition).getStore_id();
        groupViewHolder.tv_store_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("store_id", store_id);
                Intent intent=new Intent(mContext,StoreActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
        groupViewHolder.tv_customer_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.Companion.getInstance().isLogin()){
                    if(!TextUtils.isEmpty(mdataListBean.getStore_list().get(groupPosition).getDefault_im())){
                        Bundle bundle = new Bundle();
                        bundle.putString(ChatActivity.CHAT_ID,mdataListBean.getStore_list().get(groupPosition).getDefault_im());
                        bundle.putString(ChatActivity.STORE_AVATAR,mdataListBean.getStore_list().get(groupPosition).getDefault_avatar());
                        bundle.putString(ChatActivity.STORE_NAME,mdataListBean.getStore_list().get(groupPosition).getStore_name());
                        try {
                            bundle.putLong(ChatActivity.STORE_ID, Long.parseLong(mdataListBean.getStore_list().get(groupPosition).getStore_id()));
                        }catch (Exception e){
                            bundle.putLong(ChatActivity.STORE_ID, 0);
                        }
                        Intent intent=new Intent(mContext,ChatActivity.class);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                    }else {
                        ToastU.INSTANCE.showToast(mContext.getString(R.string.no_customer_service));
                    }
                }else {
                    Intent intent=new Intent(mContext,LoginActivity.class);
                    mContext.startActivity(intent);
                }
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder childViewHolder;
        if(convertView==null){
            convertView=View.inflate(mContext,R.layout.item_confirmorder_single_goods,null);
            childViewHolder=new ChildViewHolder(convertView);
            convertView.setTag(childViewHolder);
        }else {
            childViewHolder= (ChildViewHolder) convertView.getTag();
        }
        if(mdataListBean.getStore_list().get(groupPosition).getCart_list().size()!=0 && mdataListBean.getStore_list().get(groupPosition).getCart_list().size()>1){
            childViewHolder.relative_goods_single.setVisibility(View.GONE);
            childViewHolder.relative_afew.setVisibility(View.VISIBLE);
            childViewHolder.tv_goods_num.setText(mContext.getString(R.string.gong)+mdataListBean.getStore_list().get(groupPosition).getCart_list().size()+mContext.getString(R.string.jian));
            childViewHolder.linear_confirmorder.removeAllViews();
            for(int i=0;i<mdataListBean.getStore_list().get(groupPosition).getCart_list().size();i++){
                View view=View.inflate(mContext,R.layout.item_confirmorder_afew_goods,null);
                ImageView imageView=view.findViewById(R.id.iv_goods_image);
                Glide.with(mContext)
                        .load(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(i).getGoods_image_url())
                        .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                        .into(imageView);
                childViewHolder.linear_confirmorder.addView(view,i);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle=new Bundle();
                        bundle.putInt(CommodityListActivity.FLAG,2);
                        bundle.putString("goods_detils", JSONObject.toJSONString(mdataListBean.getStore_list().get(groupPosition)));
                        Intent intent=new Intent(mContext, CommodityListActivity.class);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                    }
                });
            }

        }else {
            childViewHolder.view2.setVisibility(View.GONE);
            childViewHolder.relative_goods_single.setVisibility(View.VISIBLE);
            childViewHolder.relative_afew.setVisibility(View.GONE);
            if(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getIs_second_hand()==1){
                childViewHolder.tv_name.setText(Utils.secondLabel(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getGoods_name()));
            }else {
                childViewHolder.tv_name.setText(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getGoods_name());
            }
            //childViewHolder.tv_name.setText(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getGoods_name());
            childViewHolder.tv_dollar.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getGoods_price())));
            childViewHolder.tv_order_num.setText("x"+mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getGoods_num());
            childViewHolder.tv_total_price.setText("¥"+ Utils.getDisplayMoney(Double.parseDouble(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getGoods_total())));
            childViewHolder.tv_model.setText(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getGoods_spec());
            Glide.with(mContext)
                    .load(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getGoods_image_url())
                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                    .into(childViewHolder.iv_goods_img);
            //运费
            childViewHolder.tv_goods_freight.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getFreight_fee())));
            childViewHolder.tv_test_fee.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getCustoms_charges_fee())));
            final String goods_id=Integer.toString(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getGoods_id());
            childViewHolder.relative_goods_single.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle=new Bundle();
                    bundle.putString("goods_id",goods_id);
                    Intent intent=new Intent(mContext, GoodsActivity.class);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
            if(mdataListBean.getStore_list().get(groupPosition).getCart_list().get(childPosition).getCustoms_charges_fee().equals("0")){
                childViewHolder.tv_test_fee.setVisibility(View.GONE);
                childViewHolder.tv_fee.setVisibility(View.GONE);
            }else {
                childViewHolder.tv_test_fee.setVisibility(View.VISIBLE);
                childViewHolder.tv_fee.setVisibility(View.VISIBLE);
            }
        }

        if(groupPosition==mdataListBean.getStore_list().size()-1){
            childViewHolder.view.setVisibility(View.GONE);
            childViewHolder.view4.setVisibility(View.GONE);
        }else{
            childViewHolder.view.setVisibility(View.GONE);
            childViewHolder.view4.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    static class GroupViewHolder {
        @BindView(R.id.tv_store_name)
        TextView tv_store_name;
        @BindView(R.id.tv_customer_service)
        TextView tv_customer_service;

        public GroupViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class ChildViewHolder {
        @BindView(R.id.iv_img)
        ImageView iv_goods_img;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_model)
        TextView tv_model;
        @BindView(R.id.tv_dollar)
        TextView tv_dollar;
        @BindView(R.id.tv_order_num)
        TextView tv_order_num;
        @BindView(R.id.tv_total_price)
        TextView tv_total_price;
        @BindView(R.id.tv_goods_num)
        TextView tv_goods_num;
        @BindView(R.id.tv_goods_freight)
        TextView tv_goods_freight;
        @BindView(R.id.tv_test_fee)
        TextView tv_test_fee;
        @BindView(R.id.tv_fee)
        TextView tv_fee;
        @BindView(R.id.relative_freight)
        RelativeLayout relative_freight;
        @BindView(R.id.relative_afew)
        RelativeLayout relative_afew;
        @BindView(R.id.horizontalscrollview_goods_single)
        HorizontalScrollView horizontalScrollView;
        @BindView(R.id.relative_bg)
        RelativeLayout relative_bg;
        @BindView(R.id.linear_confirmorder)
        LinearLayout linear_confirmorder;
        @BindView(R.id.relative_goods_single)
        RelativeLayout relative_goods_single;
        @BindView(R.id.lin)
        View view;
        @BindView(R.id.lin2)
        View view4;
        @BindView(R.id.view2)
        View view2;

        public ChildViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
