package com.xht.kuaiyouyi.ui.login;

import android.content.Context;

import com.xht.kuaiyouyi.ui.login.entity.LoginBean;
import com.xht.kuaiyouyi.ui.message.util.IMUtil;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import org.litepal.LitePal;
import org.litepal.LitePalDB;

import cn.jpush.android.api.JPushInterface;

public class LoginUtil {
    public static void loginSaveDataAndInit(Context context, LoginBean loginBean,String phone) {
        Login.Companion.getInstance().setToken_id(loginBean.getToken_id());
        Login.Companion.getInstance().setToken(loginBean.getToken());
        Login.Companion.getInstance().setUid(loginBean.getMember_info().getMember_id());
        Login.Companion.getInstance().setUsername(loginBean.getMember_info().getName());
        Login.Companion.getInstance().setMemberAvatar(loginBean.getMember_info().getAvatar());
        Login.Companion.getInstance().setPhone(phone);
        //登陆完成要出初始化IM，update_user
        IMUtil.initIM();
        Utils.getUnreadMsg();
        LitePal.use(LitePalDB.fromDefault("kyy_" + Login.Companion.getInstance().getUid()));
        if(!Utils.isHUAWEI()){
            JPushInterface.setAlias(context, 0, Login.Companion.getInstance().getUid());
            Login.Companion.getInstance().set_set_alias("1");
        }
    }
}
