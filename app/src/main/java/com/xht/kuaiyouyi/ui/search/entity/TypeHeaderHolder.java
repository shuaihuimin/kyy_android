package com.xht.kuaiyouyi.ui.search.entity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.xht.kuaiyouyi.R;

public class TypeHeaderHolder extends RecyclerView.ViewHolder {
    public TextView titleView;
    public TypeHeaderHolder(View itemView) {
        super(itemView);
        initView();
    }

    private void initView(){
        titleView=(TextView) itemView.findViewById(R.id.textView);
    }
}
