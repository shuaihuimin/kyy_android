package com.xht.kuaiyouyi.ui.search.entity;

import com.xht.kuaiyouyi.ui.home.entity.HomeData;

import java.util.List;

/**
 * Created by shuaihuimin on 2018/6/27.
 */

public class TypelistData  {

    /**
     * gc_id : 0
     * kyy : eyJQIjoxLCJLIjoia3l5XzAwNSIsIk8iOiItZ29vZHNfaWQtZGVzYy0tLVx1OTZmYlx1NmM2MC0tIn0=
     * currencytype : {"rate":1.152,"name":"港币","currency":"HKD","symbol":"HK$"}
     * goods_list : {"item":[{"store_id":"1","goods_name":"8V 邱健 T-875 深循環富液式電池 2V","goods_salenum":"37","goods_price":"0.01","goods_id":"101506","goods_image":"http://www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05828045899338693.jpg","gc_id_1":"1110"},{"store_id":"1","goods_name":"6V 邱健 T-145Plus 深循環富液式電池","goods_salenum":"0","goods_price":"1679.00","goods_id":"101363","goods_image":"http://www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05828042298137504.jpg","gc_id_1":"1110"},{"store_id":"1","goods_name":"6V 邱健 T-125Plus 深循環富液式電池","goods_salenum":"19","goods_price":"1368.50","goods_id":"101362","goods_image":"http://www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05828038541396964.jpg","gc_id_1":"1110"},{"store_id":"1","goods_name":"6V 邱健 T-105Plus 深循環富液式電池","goods_salenum":"3","goods_price":"1184.50","goods_id":"101361","goods_image":"http://www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05828035216554482.jpg","gc_id_1":"1110"},{"store_id":"1","goods_name":"6V 邱健 L16P-AC 深循環富液式電池","goods_salenum":"2","goods_price":"2645.00","goods_id":"101360","goods_image":"http://www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05828027217633106.jpg","gc_id_1":"1110"},{"store_id":"1","goods_name":"6V 邱健 J305P-AC 深循環富液式電池","goods_salenum":"2","goods_price":"2219.50","goods_id":"101359","goods_image":"http://www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05828027217633106.jpg","gc_id_1":"1110"},{"store_id":"1","goods_name":"12V 邱健 30XHS 深循環富液式電池","goods_salenum":"1","goods_price":"1564.00","goods_id":"101358","goods_image":"http://www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05828020700128617.jpg","gc_id_1":"1110"},{"store_id":"1","goods_name":"12V 邱健 27TMH 深循環富液式電池","goods_salenum":"1","goods_price":"1380.00","goods_id":"101353","goods_image":"http://www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05823858114828623.jpg","gc_id_1":"1110"},{"store_id":"1","goods_name":"柴油,工作高度18米鼎力 GTBZ-18AE自行走直臂式高空作業平臺（電池驅動）電池","goods_salenum":"1","goods_price":"368000.00","goods_id":"101352","goods_image":"http://www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05810800113724874.png","gc_id_1":"1100"}]}
     */

    private String gc_id;
    private String kyy;
    private CurrencytypeBean currencytype;
    private GoodsListBean goods_list;

    public String getGc_id() {
        return gc_id;
    }

    public void setGc_id(String gc_id) {
        this.gc_id = gc_id;
    }

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public CurrencytypeBean getCurrencytype() {
        return currencytype;
    }

    public void setCurrencytype(CurrencytypeBean currencytype) {
        this.currencytype = currencytype;
    }

    public GoodsListBean getGoods_list() {
        return goods_list;
    }

    public void setGoods_list(GoodsListBean goods_list) {
        this.goods_list = goods_list;
    }

    public static class CurrencytypeBean {
        /**
         * rate : 1.152
         * name : 港币
         * currency : HKD
         * symbol : HK$
         */

        private HomeData.CurrencytypeBean.CNYBean CNY;
        private HomeData.CurrencytypeBean.HKDBean HKD;
        private HomeData.CurrencytypeBean.MOPBean MOP;
        private HomeData.CurrencytypeBean.USDBean USD;

        public HomeData.CurrencytypeBean.CNYBean getCNY() {
            return CNY;
        }

        public void setCNY(HomeData.CurrencytypeBean.CNYBean CNY) {
            this.CNY = CNY;
        }

        public HomeData.CurrencytypeBean.HKDBean getHKD() {
            return HKD;
        }

        public void setHKD(HomeData.CurrencytypeBean.HKDBean HKD) {
            this.HKD = HKD;
        }

        public HomeData.CurrencytypeBean.MOPBean getMOP() {
            return MOP;
        }

        public void setMOP(HomeData.CurrencytypeBean.MOPBean MOP) {
            this.MOP = MOP;
        }

        public HomeData.CurrencytypeBean.USDBean getUSD() {
            return USD;
        }

        public void setUSD(HomeData.CurrencytypeBean.USDBean USD) {
            this.USD = USD;
        }

        public static class CNYBean {
            /**
             * name : 人民币
             * symbol : ￥
             * currency : CNY
             * cackeKey :
             * rate : 0
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class HKDBean {
            /**
             * name : 港币
             * symbol : HK$
             * currency : HKD
             * cackeKey : cacheCurrencyHKD
             * rate : 1.1498
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class MOPBean {
            /**
             * name : 葡币
             * symbol : MOP$
             * currency : MOP
             * cackeKey : cacheCurrencyMOP
             * rate : 1.18265
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class USDBean {
            /**
             * name : 美元
             * symbol : US$
             * currency : USD
             * cackeKey : cacheCurrencyUSD
             * rate : 0.1463
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }
    }

    public static class GoodsListBean {
        private List<ItemBean> item;

        public List<ItemBean> getItem() {
            return item;
        }

        public void setItem(List<ItemBean> item) {
            this.item = item;
        }

        public static class ItemBean {
            /**
             * store_id : 1
             * goods_name : 8V 邱健 T-875 深循環富液式電池 2V
             * goods_salenum : 37
             * goods_price : 0.01
             * goods_id : 101506
             * goods_image : http://www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05828045899338693.jpg
             * gc_id_1 : 1110
             */

            private String store_id;
            private String store_name;
            private String goods_name;
            private String goods_salenum;
            private String goods_price;
            private String goods_id;
            private String goods_image;
            private String gc_id_1;
            private String is_enquiry;
            private String goods_state;
            private int is_second_hand;


            public int getIs_second_hand() {
                return is_second_hand;
            }

            public void setIs_second_hand(int is_second_hand) {
                this.is_second_hand = is_second_hand;
            }

            public String getStore_id() {
                return store_id;
            }

            public void setStore_id(String store_id) {
                this.store_id = store_id;
            }

            public String getStore_name() {
                return store_name;
            }

            public void setStore_name(String store_name) {
                this.store_name = store_name;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getGoods_salenum() {
                return goods_salenum;
            }

            public void setGoods_salenum(String goods_salenum) {
                this.goods_salenum = goods_salenum;
            }

            public String getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(String goods_price) {
                this.goods_price = goods_price;
            }

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getGoods_image() {
                return goods_image;
            }

            public void setGoods_image(String goods_image) {
                this.goods_image = goods_image;
            }

            public String getGc_id_1() {
                return gc_id_1;
            }

            public void setGc_id_1(String gc_id_1) {
                this.gc_id_1 = gc_id_1;
            }

            public String getIs_enquiry() {
                return is_enquiry;
            }

            public void setIs_enquiry(String is_enquiry) {
                this.is_enquiry = is_enquiry;
            }

            public String getGoods_state() {
                return goods_state;
            }

            public void setGoods_state(String goods_state) {
                this.goods_state = goods_state;
            }
        }
    }
}
