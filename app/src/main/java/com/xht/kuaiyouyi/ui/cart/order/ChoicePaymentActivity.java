package com.xht.kuaiyouyi.ui.cart.order;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.cart.adapter.OrderPopuAdapter;
import com.xht.kuaiyouyi.ui.cart.entity.ConfirmOrderBean;
import com.xht.kuaiyouyi.ui.cart.entity.OrderChooseBean;
import com.xht.kuaiyouyi.utils.CommonPopupWindow;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ChoicePaymentActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.radiogroup_choice_payment)
    RadioGroup radiogroup_choice_payment;
    @BindView(R.id.radio_bank)
    RadioButton radio_bank;
    @BindView(R.id.radio_cheque_account)
    RadioButton radio_cheque_account;
    @BindView(R.id.radio_online_transfer)
    RadioButton radio_online_transfer;
    @BindView(R.id.relative_region_order)
    RelativeLayout relative_region_order;
    @BindView(R.id.relative_transfer_bank)
    RelativeLayout relative_transfer_bank;
    @BindView(R.id.relative_roll_out_currency)
    RelativeLayout relative_roll_out_currency;
    @BindView(R.id.btn_choice_payment)
    Button btn_choice_payment;
    @BindView(R.id.linear_bank)
    LinearLayout linear_bank;
    @BindView(R.id.linear_check)
    LinearLayout linear_check;
    @BindView(R.id.tv_region_order)
    TextView tv_region_order;
    @BindView(R.id.tv_select_currency)
    TextView tv_select_currency;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.tv_payment_currency)
    TextView tv_payment_currency;
    @BindView(R.id.tv_select_transfer_bank)
    TextView tv_select_transfer_bank;
    @BindView(R.id.relative_check_address)
    RelativeLayout relative_check_address;
    @BindView(R.id.relative_check_currency)
    RelativeLayout relative_check_currency;


    private ConfirmOrderBean confirmOrder;
    private Gson gson = new Gson();
    //adapter数据源
    private List<OrderChooseBean> listinit = new ArrayList<>();
    //选择银行卡数据源
    private List<OrderChooseBean> listblack;
    //选择地区数据源
    private List<OrderChooseBean> listregion = new ArrayList<>();
    //选择银行卡币种数据源
    private List<OrderChooseBean> listcurrency;
    //选择支票支付币种数据源
    private List<OrderChooseBean> listcheak=new ArrayList<>();
    //支票支付取支票地址id
    private String cheac_address_id = "";
    //支付方式
    private String pay_active = "1";
    //银行地区id
    private String bank_area = "";
    //银行名称id
    private String bank_item = "";
    //银行币别id
    private String bank_currency = "";
    //支票币别
    private String check_currency = "";
    //币别符号
    private String pay_currency;
    //默认false为切换了地区，true为没有更换地区
    private boolean type = false;
    private String typeString = "";
    private boolean type1 = false;
    private String typeString1 = "";
    private CommonPopupWindow popupWindow;
    public final static int REQUEST_CODE = 1000;
    private OrderPopuAdapter orderPopuAdapter;
    private TextView tv_head;
    private Button bt_submit;
    private ListView lv_company_list;
    private View view;
    private String name;
    private String name1;
    private int state;
    private int flag=0;

    @Override
    protected int getLayout() {
        return R.layout.activity_choice_payment;
    }

    @Override
    protected void initView() {
        tv_title.setText(getString(R.string.select_zhifu_way));
        String blanklist = getIntent().getStringExtra("blanklist");
        state=getIntent().getIntExtra("state",1);
        if(state==1){
            radio_bank.setChecked(true);
            linear_bank.setVisibility(View.VISIBLE);
            linear_check.setVisibility(View.GONE);
            pay_active = "1";
        }else if(state==2){
            radio_cheque_account.setChecked(true);
            linear_bank.setVisibility(View.GONE);
            linear_check.setVisibility(View.VISIBLE);
            pay_active = "2";
        }else if(state==3){
            radio_online_transfer.setChecked(true);
            linear_bank.setVisibility(View.GONE);
            linear_check.setVisibility(View.GONE);
            pay_active = "3";
        }
        confirmOrder = gson.fromJson(blanklist, ConfirmOrderBean.class);
        //设置银行卡选择地区数据
        if (confirmOrder != null && confirmOrder.getBank_list().size() != 0) {
            for (int i = 0; i < confirmOrder.getBank_list().size(); i++) {
                OrderChooseBean orderChooseBean = new OrderChooseBean(confirmOrder.getBank_list().get(i).getName(), confirmOrder.getBank_list().get(i).getId(), null);
                listregion.add(orderChooseBean);
            }
        }


        click();

    }

    void initPopupWindow() {
        orderPopuAdapter = new OrderPopuAdapter(ChoicePaymentActivity.this, listinit);
        view = LayoutInflater.from(ChoicePaymentActivity.this).inflate(R.layout.dialog_procurement_method, null);
        popupWindow = new CommonPopupWindow.Builder(ChoicePaymentActivity.this).setView(view).setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).setBackGroundLevel(0.5f)//取值范围0.0f-1.0f 值越小越暗
                .create();
        tv_head = view.findViewById(R.id.tv_dialog_procurement_method);
        lv_company_list = view.findViewById(R.id.lv_company_list);
        bt_submit = view.findViewById(R.id.bt_submit);
        bt_submit.setVisibility(View.INVISIBLE);
        lv_company_list.setAdapter(orderPopuAdapter);
        ImageView iv_close = view.findViewById(R.id.iv_close);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(popupWindow.isShowing()){
                    popupWindow.dismiss();
                }
            }
        });
    }

    private void click() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChoicePaymentActivity.this.finish();
            }
        });
        radiogroup_choice_payment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_bank:
                        linear_bank.setVisibility(View.VISIBLE);
                        linear_check.setVisibility(View.GONE);
                        pay_active = "1";
                        break;
                    case R.id.radio_cheque_account:
                        linear_bank.setVisibility(View.GONE);
                        linear_check.setVisibility(View.VISIBLE);
                        pay_active = "2";
                        break;
                    case R.id.radio_online_transfer:
                        linear_bank.setVisibility(View.GONE);
                        linear_check.setVisibility(View.GONE);
                        pay_active = "3";
                        break;
                }

            }
        });

        //银行卡转账选择地区
        relative_region_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow == null) {
                    initPopupWindow();
                }

                listinit.clear();
                listinit.addAll(listregion);
                tv_head.setText(getString(R.string.region_order));
                popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
                Utils.SetWindBg(ChoicePaymentActivity.this);
                lv_company_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        for (int i = 0; i < listregion.size(); i++) {
                            if (listregion.get(i).isSelect())
                                listregion.get(i).setSelect(false);
                        }
                        listregion.get(position).setSelect(true);
                        orderPopuAdapter.notifyDataSetChanged();
                        name = listregion.get(position).getName();
                        tv_region_order.setText(name);
                        bank_area = listregion.get(position).getId();
                        if (!typeString.equals(name)) {
                            type = true;
                            tv_select_transfer_bank.setText("");
                            tv_select_currency.setText("");
                        }
                        popupWindow.dismiss();
                    }
                });
            }
        });

        //选择银行卡
        relative_transfer_bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(tv_region_order.getText())) {
                    DialogUtils.createTipAllTextDialog(ChoicePaymentActivity.this, getString(R.string.transfer_bank));
                    return;
                }
                if (popupWindow == null) {
                    initPopupWindow();
                }
                else if (type == true) {
                    typeString = name;
                    type = false;
                    listblack = new ArrayList<>();
                    if (confirmOrder != null && confirmOrder.getBank_list().size() != 0) {
                        for (int i = 0; i < confirmOrder.getBank_list().size(); i++) {
                            if (tv_region_order.getText().equals(confirmOrder.getBank_list().get(i).getName())) {
                                for (int j = 0; j < confirmOrder.getBank_list().get(i).getSub().size(); j++) {
                                    String name = confirmOrder.getBank_list().get(i).getSub().get(j).getName();
                                    String id = confirmOrder.getBank_list().get(i).getSub().get(j).getId();
                                    OrderChooseBean orderChooseBean = new OrderChooseBean(name, id, null);
                                    listblack.add(orderChooseBean);
                                }
                            }
                        }
                    }
                    //记录上次选中结果
                    if (listblack.size() != 0) {
                        if (!TextUtils.isEmpty(tv_select_currency.getText())) {
                            for (int i = 0; i < listblack.size(); i++) {
                                if (listblack.get(i).getName().equals(tv_select_currency.getText())) {
                                    listblack.get(i).setSelect(true);
                                    tv_select_transfer_bank.setText(listblack.get(i).getName());
                                    bank_item = listblack.get(i).getId();
                                }
                            }
                        }
                    }
                }
                listinit.clear();
                listinit.addAll(listblack);
                tv_head.setText(getString(R.string.transfer_bank));
                orderPopuAdapter.notifyDataSetChanged();
                popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
                Utils.SetWindBg(ChoicePaymentActivity.this);
                lv_company_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        for (int i = 0; i < listblack.size(); i++) {
                            if (listblack.get(i).isSelect())
                                listblack.get(i).setSelect(false);
                        }
                        listblack.get(position).setSelect(true);
                        orderPopuAdapter.notifyDataSetChanged();
                        name1 = listblack.get(position).getName();
                        tv_select_transfer_bank.setText(name1);
                        bank_item = listblack.get(position).getId();
                        if (!typeString1.equals(name1)) {
                            type1 = true;
                            tv_select_currency.setText("");
                        }
                        popupWindow.dismiss();
                    }
                });
            }
        });

        //银行卡选择币别
        relative_roll_out_currency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(tv_select_transfer_bank.getText())) {
                    DialogUtils.createTipAllTextDialog(ChoicePaymentActivity.this, getString(R.string.select_transfer_bank));
                    return;
                }
                if (popupWindow == null) {
                    initPopupWindow();
                }
                else if (type1 == true) {
                    typeString1 = name1;
                    type1 = false;
                    listcurrency = new ArrayList<>();
                    if (confirmOrder != null && confirmOrder.getBank_list().size() != 0) {
                        for (int i = 0; i < confirmOrder.getBank_list().size(); i++) {
                            if (tv_region_order.getText().equals(confirmOrder.getBank_list().get(i).getName())) {
                                for (int j = 0; j < confirmOrder.getBank_list().get(i).getSub().size(); j++) {
                                    if (tv_select_transfer_bank.getText().
                                            equals(confirmOrder.getBank_list().get(i).getSub().get(j).getName())) {
                                        for (int k = 0; k < confirmOrder.getBank_list().get(i).getSub().get(j).getSub().size(); k++) {
                                            String name = confirmOrder.getBank_list().get(i).getSub().get(j).getSub().get(k).getName();
                                            String id = confirmOrder.getBank_list().get(i).getSub().get(j).getSub().get(k).getId();
                                            String symbol = confirmOrder.getBank_list().get(i).
                                                    getSub().get(j).getSub().get(k).getCurrency_type();
                                            OrderChooseBean orderChooseBean = new OrderChooseBean(name, id, symbol);
                                            listcurrency.add(orderChooseBean);
                                            Log.i("imfo","-----------list"+listcurrency.size());
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //记录上次选中结果
                    if (listcurrency.size() != 0) {
                        if (!TextUtils.isEmpty(tv_select_currency.getText())) {
                            for (int i = 0; i < listcurrency.size(); i++) {
                                if (listcurrency.get(i).getName().equals(tv_select_currency.getText())) {
                                    listcurrency.get(i).setSelect(true);
                                    tv_select_currency.setText(listcurrency.get(i).getName());
                                    bank_currency = listcurrency.get(i).getId();
                                    pay_currency = listcurrency.get(i).getSymbol();
                                }
                            }
                        }
                    }
                }
                listinit.clear();
                listinit.addAll(listcurrency);
                tv_head.setText(getString(R.string.select_currency));
                popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
                Utils.SetWindBg(ChoicePaymentActivity.this);
                lv_company_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        for (int i = 0; i < listcurrency.size(); i++) {
                            if (listcurrency.get(i).isSelect())
                                listcurrency.get(i).setSelect(false);
                        }
                        listcurrency.get(position).setSelect(true);
                        orderPopuAdapter.notifyDataSetChanged();
                        tv_select_currency.setText(listcurrency.get(position).getName());
                        bank_currency = listcurrency.get(position).getId();
                        pay_currency = listcurrency.get(position).getSymbol();
                        popupWindow.dismiss();
                    }
                });
            }
        });

        //选择支票地址
        relative_check_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChoicePaymentActivity.this, ChooseAddressActivity.class);
                intent.putExtra("type", ChooseAddressActivity.TYPE_CHECK);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

        //支票支付选择币种
        relative_check_currency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //设置支票币别
                if(flag==0){
                    flag=1;
                    listcheak = new ArrayList<>();
                    if (confirmOrder.getPay_check_list() != null) {
                        for (int i = 0; i < confirmOrder.getPay_check_list().size(); i++) {
                            String name = confirmOrder.getPay_check_list().get(i).getName();
                            String symbol = confirmOrder.getPay_check_list().get(i).getCurrency();
                            OrderChooseBean orderChooseBean = new OrderChooseBean(name, null, symbol);
                            listcheak.add(orderChooseBean);
                            Log.i("onfo","-------"+listcheak.size());
                        }
                    }
                }
                //记录上次选中结果
                if (listcheak.size() != 0) {
                    if (!TextUtils.isEmpty(tv_payment_currency.getText())) {
                        for (int i = 0; i < listcheak.size(); i++) {
                            if (listcheak.get(i).getName().equals(tv_payment_currency.getText())) {
                                listcheak.get(i).setSelect(true);
                                tv_payment_currency.setText(listcheak.get(i).getName());
                                check_currency = listcheak.get(i).getName();
                                pay_currency = listcheak.get(i).getSymbol();
                            }
                        }
                    }
                }

                if(popupWindow==null){
                    initPopupWindow();
                }
                Utils.SetWindBg(ChoicePaymentActivity.this);
                listinit.clear();
                listinit.addAll(listcheak);
                tv_head.setText(getString(R.string.select_currency));
                popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
                lv_company_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        for (int i = 0; i < listcheak.size(); i++) {
                            if (listcheak.get(i).isSelect())
                                listcheak.get(i).setSelect(false);
                        }
                        listcheak.get(position).setSelect(true);
                        orderPopuAdapter.notifyDataSetChanged();
                        tv_payment_currency.setText(listcheak.get(position).getName());
                        check_currency = listcheak.get(position).getName();
                        pay_currency = listcheak.get(position).getSymbol();
                        popupWindow.dismiss();

                    }
                });
                popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
            }
        });

        btn_choice_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pay_active.equals("1")) {
                    if (TextUtils.isEmpty(tv_region_order.getText())) {
                        DialogUtils.createTipAllTextDialog(ChoicePaymentActivity.this, getString(R.string.select_region_order));
                        return;
                    }

                    if (TextUtils.isEmpty(tv_select_transfer_bank.getText())) {
                        DialogUtils.createTipAllTextDialog(ChoicePaymentActivity.this, getString(R.string.select_transfer_bank));
                        return;
                    }

                    if (TextUtils.isEmpty(tv_select_currency.getText())) {
                        DialogUtils.createTipAllTextDialog(ChoicePaymentActivity.this, getString(R.string.roll_out_currency));
                        return;
                    }
                }
                else if (pay_active.equals("2")) {
                    if (TextUtils.isEmpty(tv_address.getText())) {
                        DialogUtils.createTipAllTextDialog(ChoicePaymentActivity.this, getString(R.string.get_check_address));
                        return;
                    }
                    if (TextUtils.isEmpty(tv_payment_currency.getText())) {
                        DialogUtils.createTipAllTextDialog(ChoicePaymentActivity.this, getString(R.string.payment_currency));
                        return;
                    }

                }
                else {

                }
                Intent intent = new Intent(ChoicePaymentActivity.this, ChoicePaymentActivity.class);
                intent.putExtra("pay_active", pay_active);
                intent.putExtra("bank_currency", bank_currency);
                intent.putExtra("bank_area", bank_area);
                intent.putExtra("bank_item", bank_item);
                intent.putExtra("bank_area_name", tv_region_order.getText().toString());
                intent.putExtra("bank_item_name", tv_select_transfer_bank.getText().toString());
                intent.putExtra("bank_currency_name", tv_select_currency.getText().toString());
                intent.putExtra("check_currency", check_currency);
                intent.putExtra("cheac_address", tv_address.getText().toString());
                intent.putExtra("cheac_address_id", cheac_address_id);
                intent.putExtra("pay_currency", pay_currency);
                setResult(RESULT_OK, intent);
                ChoicePaymentActivity.this.finish();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            cheac_address_id = data.getStringExtra("address_id");
            tv_address.setText(data.getStringExtra("name") + "  " + data.getStringExtra("mob_phone") + "  " + data.getStringExtra("area_info") + data.getStringExtra("address"));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
