package com.xht.kuaiyouyi.ui.search.entity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;


/**
 * Created by lyd10892 on 2016/8/23.
 */

public class HeaderHolder extends RecyclerView.ViewHolder {
    public View view2;
    public View view3;
    public View view4;
    public TextView titleView;
    public TextView openView;
    public TextView tv_title;
    public TextView tv_next;
    public HeaderHolder(View itemView) {
        super(itemView);
        initView();
    }

    private void initView() {
        titleView = (TextView) itemView.findViewById(R.id.textview_head);
        openView = (TextView) itemView.findViewById(R.id.tv_open);
        tv_title=(TextView) itemView.findViewById(R.id.tv_title);
        tv_next=(TextView) itemView.findViewById(R.id.tv_next);
        view2=(View)itemView.findViewById(R.id.view2);
        view3=(View) itemView.findViewById(R.id.view3);
        view4=(View) itemView.findViewById(R.id.view4);
    }
}
