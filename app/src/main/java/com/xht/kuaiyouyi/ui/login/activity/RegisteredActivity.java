package com.xht.kuaiyouyi.ui.login.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.xht.kuaiyouyi.BuildConfig;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.WebActivity;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.login.entity.AreaCodeBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by shuaihuimin on 2018/6/22.
 * 注册输入手机号码页面
 */

public class RegisteredActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView backimage;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_area)
    TextView tv_area;
    @BindView(R.id.et_phone)
    EditText et_phone;
    @BindView(R.id.btn_next)
    Button btn_next;
    @BindView(R.id.tv_protocol)
    TextView tv_protocol;
    private String mAreaCode = "86";

    private OptionsPickerView mOptionsPickerView;

    private final String[] AREA_CODE = {"86", "853", "852"};
    private List<String> mAreaDisplayList;
    private List<AreaCodeBean> mPickerViewData;


    @Override
    protected int getLayout() {
        return R.layout.activity_registered;
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        tv_title.setText(R.string.register);
        tv_area.setText("+" + mAreaCode);
        tv_area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOptionsPickerView == null) {
                    mOptionsPickerView = new OptionsPickerBuilder(RegisteredActivity.this, new OnOptionsSelectListener() {
                        @Override
                        public void onOptionsSelect(int options1, int options2, int options3, View v) {
                            mAreaCode = AREA_CODE[options1];
                            tv_area.setText("+" + mAreaCode);
                        }
                    })
                            .setLayoutRes(R.layout.picker_view_phone_area_code, new CustomListener() {
                                @Override
                                public void customLayout(View v) {
                                    TextView tv_cancel = v.findViewById(R.id.tv_cancel);
                                    TextView tv_confirm = v.findViewById(R.id.tv_confirm);
                                    tv_confirm.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mOptionsPickerView.returnData();
                                            mOptionsPickerView.dismiss();
                                        }
                                    });
                                    tv_cancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mOptionsPickerView.dismiss();
                                        }
                                    });
                                }
                            })
                            .isDialog(false)
                            .setOutSideCancelable(false)
                            .build();
                    setPickViewData();
                    mOptionsPickerView.setPicker(mPickerViewData);
                }
                mOptionsPickerView.show();
            }
        });
        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(et_phone.getText().toString().trim())){
                    getCode();
                }
            }
        });
        tv_protocol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(WebActivity.TITLE,getString(R.string.protocol_title));
                bundle.putString(WebActivity.WEB_URL, BuildConfig.DOMAIN+"member/index.php?act=login&op=regAgreement");
                goToActivity(WebActivity.class,bundle);
            }
        });

        et_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(et_phone.getText().toString().trim().length()>0){
                    btn_next.setEnabled(true);
                }else {
                    btn_next.setEnabled(false);
                }
            }
        });
    }

    //获取验证码
    private void getCode() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CONNECT_GET_SMS_CAPTCHA())
                .addParam("phone", et_phone.getText().toString().trim())
                .addParam("area_code", mAreaCode)
                .addParam("type", Contant.TYPE_REGISTER)
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode,  @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(RegisteredActivity.this,err,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(getApplicationContext(),R.string.verify_code_sended,Toast.LENGTH_SHORT).show();
                        Bundle bundle = new Bundle();
                        bundle.putString(Contant.TYPE,Contant.TYPE_REGISTER);
                        bundle.putString(Contant.PHONE,et_phone.getText().toString().trim());
                        bundle.putString(Contant.AREA_CODE,mAreaCode);
                        goToActivity(VerificationCodeActivity.class,bundle);
                    }
                }, false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.EVENT_ClOSE_LOGINACTIVITY)){
           finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void setPickViewData() {
        if (mAreaDisplayList == null) {
            mAreaDisplayList = new ArrayList<>();
            mAreaDisplayList.add(getResources().getString(R.string.cn) + AREA_CODE[0]);
            mAreaDisplayList.add(getResources().getString(R.string.cn_macau) + AREA_CODE[1]);
            mAreaDisplayList.add(getResources().getString(R.string.cn_hk) + AREA_CODE[2]);
        }
        if (mPickerViewData == null) {
            mPickerViewData = new ArrayList<>();
            for (int i = 0; i < AREA_CODE.length; i++) {
                mPickerViewData.add(new AreaCodeBean(mAreaDisplayList.get(i)));
            }
        }

    }
}
