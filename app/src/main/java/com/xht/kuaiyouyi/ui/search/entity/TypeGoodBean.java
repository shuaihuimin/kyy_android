package com.xht.kuaiyouyi.ui.search.entity;

/**
 * Created by shuaihuimin on 2018/6/27.
 */

public class TypeGoodBean {
    private String store_id;
    private String goods_name;
    private String goods_salenum;
    private String goods_price;
    private String goods_id;
    private String goods_image;
    private String gc_id_1;

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_salenum() {
        return goods_salenum;
    }

    public void setGoods_salenum(String goods_salenum) {
        this.goods_salenum = goods_salenum;
    }

    public String getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(String goods_price) {
        this.goods_price = goods_price;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(String goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_image() {
        return goods_image;
    }

    public void setGoods_image(String goods_image) {
        this.goods_image = goods_image;
    }

    public String getGc_id_1() {
        return gc_id_1;
    }

    public void setGc_id_1(String gc_id_1) {
        this.gc_id_1 = gc_id_1;
    }
}
