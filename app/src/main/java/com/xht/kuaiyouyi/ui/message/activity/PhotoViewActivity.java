package com.xht.kuaiyouyi.ui.message.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.OnPhotoTapListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.utils.Utils;

import java.io.File;
import java.text.MessageFormat;

import butterknife.BindView;

public class PhotoViewActivity extends BaseActivity {
    @BindView(R.id.pv_photo)
    PhotoView pv_photo;
    @BindView(R.id.tv_save_img)
    TextView tv_save_img;

    public final static String KEY_IMG = "key_img";
    public final static String TYPE = "type";
    public final static int TYPE_URL = 0;//加载链接
    public final static int TYPE_PATH = 1;//加载本地图片资源

    private SaveImgTask mSaveImgTask;

    private final int PERMISSION_CODE = 2000;

    private String mImgPath;

    //缓存图片
    private RequestOptions mGlideOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL);


    @Override
    protected int getLayout() {
        return R.layout.activity_photoview;
    }

    @Override
    protected void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            pv_photo.setTransitionName(getString(R.string.app_name));
        }
        mImgPath = getIntent().getStringExtra(KEY_IMG);
        int type = getIntent().getIntExtra(TYPE, TYPE_URL);
        if (!TextUtils.isEmpty(mImgPath)) {
            if (type == TYPE_PATH) {
                Glide.with(PhotoViewActivity.this).load(new File(mImgPath)).apply(mGlideOptions).into(pv_photo);
            } else {
                Glide.with(PhotoViewActivity.this).load(mImgPath).apply(mGlideOptions).into(pv_photo);
            }
            pv_photo.setOnPhotoTapListener(new OnPhotoTapListener() {
                @Override
                public void onPhotoTap(ImageView view, float x, float y) {
                    onBackPressed();
                }
            });
        }

        tv_save_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(PhotoViewActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    saveFile();
                }else {
                    ActivityCompat.requestPermissions(PhotoViewActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE);
                }
            }
        });

    }

    private void saveFile() {
        if (mSaveImgTask == null) {
            mSaveImgTask = new SaveImgTask();
        }
        mSaveImgTask.execute(mImgPath);
    }

    @Override
    public void onBackPressed() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (mSaveImgTask != null) {
            mSaveImgTask.cancel(true);
            mSaveImgTask = null;
        }
        super.onDestroy();
    }

    private class SaveImgTask extends AsyncTask<String, Void, File> {
        @Override
        protected File doInBackground(String... params) {
            String imgUrl = params[0];
            try {
                return Glide.with(PhotoViewActivity.this)
                        .load(imgUrl)
                        .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .get();
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(File result) {
            if (result == null) {
                return;
            }
            try {
                //这里得到的就是我们要的文件了，接下来是保存文件。
                File target = new File(KyyConstants.INSTANCE.getDOWNLOAD_DIR() + System.currentTimeMillis() + ".jpg");//filepath是目标保存文件的路径，根据自己的项目需要去配置
                if (!target.getParentFile().exists()) {
                    target.getParentFile().mkdir();
                }
                target.createNewFile();
                Utils.CopyFile(result, target);
                Toast.makeText(getApplicationContext(), MessageFormat.format(getString(R.string.image_save_folder),KyyConstants.INSTANCE.getDOWNLOAD_DIR()), Toast.LENGTH_SHORT).show();
                tv_save_img.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_CODE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                saveFile();
            }else {
                Toast.makeText(getApplicationContext(), R.string.tip_permission_storage, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
