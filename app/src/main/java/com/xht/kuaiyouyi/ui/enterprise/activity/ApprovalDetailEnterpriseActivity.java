package com.xht.kuaiyouyi.ui.enterprise.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.cart.order.OrderDetilsActivity;
import com.xht.kuaiyouyi.ui.enterprise.Constant;
import com.xht.kuaiyouyi.ui.enterprise.adapter.ApprovalFlowAdapter;
import com.xht.kuaiyouyi.ui.enterprise.adapter.GoodsListAdapter;
import com.xht.kuaiyouyi.ui.enterprise.bean.ApprovalDetailBean;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Utils;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.text.MessageFormat;
import java.util.List;

import butterknife.BindView;

public class ApprovalDetailEnterpriseActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_approval_status)
    TextView tv_approval_status;
    @BindView(R.id.tv_approval_num)
    TextView tv_approval_num;
    @BindView(R.id.tv_apply_people)
    TextView tv_apply_people;
    @BindView(R.id.tv_delivery_time)
    TextView tv_delivery_time;
    @BindView(R.id.tv_apply_reason)
    TextView tv_apply_reason;
    @BindView(R.id.rv_goods_container)
    RecyclerView rv_goods_container;//商品清单
    @BindView(R.id.tv_freight)
    TextView tv_freight;
    @BindView(R.id.tv_goods_total_amount)
    TextView tv_goods_total_amount;
    @BindView(R.id.tv_order_total_amout)
    TextView tv_order_total_amout;
    @BindView(R.id.tv_trade_way)
    TextView tv_trade_way;
    @BindView(R.id.tv_pay_way)
    TextView tv_pay_way;
    @BindView(R.id.tv_pay_currency)
    TextView tv_pay_currency;
    @BindView(R.id.tv_pay_rate)
    TextView tv_pay_rate;
    @BindView(R.id.tv_total_amount_payable)
    TextView tv_total_amount_payable;

    @BindView(R.id.rv_flow_container)
    RecyclerView rv_flow_container;//审批流程
    @BindView(R.id.mRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;

    //抄送人
    @BindView(R.id.rl_copy_to_me)
    RelativeLayout rl_copy_to_me;
    @BindView(R.id.tll_copy_to_container)
    TagFlowLayout tll_copy_to_container;

    //信用支付
    @BindView(R.id.rl_credit_pay)
    RelativeLayout rl_credit_pay;
    @BindView(R.id.rl_credit_pay_two)
    RelativeLayout rl_credit_pay_two;
    @BindView(R.id.tv_first_pay)
    TextView tv_first_pay;
    @BindView(R.id.tv_end_pay)
    TextView tv_end_pay;
    @BindView(R.id.tv_supplement)
    TextView tv_supplement;
    @BindView(R.id.tv_supplement_title)
    TextView tv_supplement_title;
    @BindView(R.id.rl_supplement)
    RelativeLayout rl_supplement;
    @BindView(R.id.iv_question)
    ImageView iv_question;
    @BindView(R.id.tv_one_pay)
    TextView tv_one_pay;
    @BindView(R.id.tv_two_pay)
    TextView tv_two_pay;
    @BindView(R.id.tv_stage_one_pay)
    TextView tv_stage_one_pay;
    @BindView(R.id.tv_tip)
    TextView tv_tip;
    @BindView(R.id.tv_second_pay_tip)
    TextView tv_second_pay_tip;
    @BindView(R.id.tv_tip_2)
    TextView tv_tip_2;
    @BindView(R.id.tv_stage_two_pay)
    TextView tv_stage_two_pay;

    @BindView(R.id.ll_approval_btn)
    LinearLayout ll_approval_btn;
    @BindView(R.id.tv_repeal)
    TextView tv_repeal;
    @BindView(R.id.tv_consent)
    TextView tv_consent;
    @BindView(R.id.view_line_consent)
    View view_line_consent;
    @BindView(R.id.tv_reject)
    TextView tv_reject;
    @BindView(R.id.view_line_reject)
    View view_line_reject;

    @BindView(R.id.rl_goto_pay)
    RelativeLayout rl_goto_pay;
    @BindView(R.id.bt_goto_pay)
    Button bt_goto_pay;

    @BindView(R.id.ll_detail)
    LinearLayout ll_detail;

    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;


    //别的页面传过来的信息
    private String mApproveOrderId;
    private String mCompanyId;

    //审批意见同意拒绝的时候使用
    private String mApproveId;

    public static final String APPROVE_ORDER_ID = "approve_order_id";

    public static final int CREDIT_PAY = 2;
    public static final int FULL_AMOUNT_PAY = 1;

    public static final int BANK_PAY = 1;
    public static final int CHECK_PAY = 2;
    public static final int ONLINE_PAY = 3;

    //0和1都代表审批中
    public static final int APPROVAL_ING_ONE = 0;
    public static final int APPROVAL_ING_TWO = 1;
    public static final int APPROVAL_PASS = 2;
    public static final int APPROVAL_REJECT = 3;
    public static final int APPROVAL_REPEAL = 4;
    public static final int APPROVAL_ORDER_CANCEL = 5;

    private AlertDialog mApprovalOpinionDialog;

    //审批状态，0为拒绝，1位同意
    private String mApprovalStatus;

    private boolean isSetResult;//返回刷新列表
    private ApprovalDetailBean mApprovalDetailBean;

    @Override
    protected int getLayout() {
        return R.layout.activity_approval_detail_enterprise;
    }

    @Override
    protected void initView() {

        ll_detail.setVisibility(View.GONE);
        tv_title.setText(R.string.approval_detils);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mApproveOrderId = getIntent().getStringExtra(APPROVE_ORDER_ID);
        mCompanyId = getIntent().getStringExtra(Constant.COMPANY_ID);
        requestApprovalDetail();
        tv_consent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mApprovalStatus = "1";
                showApprovalOpinionDialog();
            }
        });
        tv_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mApprovalStatus = "0";
                showApprovalOpinionDialog();
            }
        });
        tv_repeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.createTwoBtnDialog(ApprovalDetailEnterpriseActivity.this,
                        getString(R.string.sure_repeal_tip),
                        getString(R.string.dialog_confirm),
                        getString(R.string.dialog_cancel),
                        new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(Dialog dialog) {
                                requestRepeal();
                            }
                        }, null, false, true);
            }
        });
        bt_goto_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ApprovalDetailEnterpriseActivity.this, OrderDetilsActivity.class);
                intent.putExtra("order_main_id", mApprovalDetailBean.getGoods_info().getOrder_main_id());
                startActivity(intent);
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestApprovalDetail();
            }
        });
        iv_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.createOneBtnDialog(ApprovalDetailEnterpriseActivity.this
                        ,MessageFormat.format(getString(R.string.supplement_tip)
                                ,Utils.getDisplayMoney(mApprovalDetailBean.getGoods_info().getCompany_remain_money())
                                ,Utils.getDisplayMoney(mApprovalDetailBean.getGoods_info().getOrder_main_pay_second())
                                ,Utils.getDisplayMoney(mApprovalDetailBean.getGoods_info().getSupplement_amount()))
                        ,getString(R.string.dialog_confirm),null
                        ,true,true);
            }
        });
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                requestApprovalDetail();
            }
        });
        EventBus.getDefault().register(this);

    }

    private void requestApprovalDetail() {
        DialogUtils.createTipAllLoadDialog(this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_EP_APPROVE_DETIAL())
                .addParam("company_id", mCompanyId)
                .addParam("approve_order_id", mApproveOrderId)
                .withPOST(new NetCallBack<ApprovalDetailBean>() {
                    @NotNull
                    @Override
                    public Class<ApprovalDetailBean> getRealType() {
                        return ApprovalDetailBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Refreshing){
                            smartRefreshLayout.finishRefresh();
                        }
                        DialogUtils.moven();
                        //Toast.makeText(ApprovalDetailEnterpriseActivity.this, err, Toast.LENGTH_SHORT).show();
                        error_retry_view.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onSuccess(@NonNull ApprovalDetailBean approvalDetailBean) {
                        if(isFinishing()){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        if(smartRefreshLayout.getState() == RefreshState.Refreshing){
                            smartRefreshLayout.finishRefresh();
                        }
                        DialogUtils.moven();
                        mApprovalDetailBean = approvalDetailBean;
                        showData(mApprovalDetailBean);
                    }
                }, false);
    }


    /**
     * 撤销
     */
    private void requestRepeal() {
        DialogUtils.createTipAllLoadDialog(this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_APPROVAL_REPEAL())
                .addParam("company_id", mCompanyId)
                .addParam("approve_order_id", mApproveOrderId)
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(ApprovalDetailEnterpriseActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        isSetResult = true;
                        requestApprovalDetail();
                    }
                }, false);
    }

    private void requestApprovalOpinion(String status, String opinion) {
        DialogUtils.createTipAllLoadDialog(this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_APPROVAL_OPINION())
                .addParam("company_id", mCompanyId)
                .addParam("approve_id", mApproveId)
                .addParam("opinion", opinion)
                .addParam("status", status)
                .addParam("approve_config_id", mApprovalDetailBean.getApprove_order_info().getApprove_config_id())
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(ApprovalDetailEnterpriseActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        isSetResult = true;
                        requestApprovalDetail();
                    }
                }, false);
    }

    private void showData(ApprovalDetailBean approvalDetailBean) {
        switch (approvalDetailBean.getApprove_order_info().getStatus()) {
            case APPROVAL_ING_ONE:
                tv_approval_status.setText(R.string.approval_ing);
                break;
            case APPROVAL_ING_TWO:
                tv_approval_status.setText(R.string.approval_ing);
                break;
            case APPROVAL_PASS:
                tv_approval_status.setText(R.string.approval_agree);
                break;
            case APPROVAL_REJECT:
                tv_approval_status.setText(R.string.approval_reject);
                break;
            case APPROVAL_REPEAL:
                tv_approval_status.setText(R.string.approval_repeal);
                break;
            case APPROVAL_ORDER_CANCEL:
                tv_approval_status.setText(R.string.approval_order_cancel);
                break;
        }
        mApproveId = approvalDetailBean.getApprove_order_info().getApprove_id();
        tv_approval_num.setText(approvalDetailBean.getApprove_order_info().getApprove_num());
        tv_apply_people.setText(approvalDetailBean.getApprove_order_info().getApprove_creator());
        tv_delivery_time.setText(Utils.getDisplayData(approvalDetailBean.getApprove_order_info().getExpect_delivery_time()));
        tv_apply_reason.setText(approvalDetailBean.getApprove_order_info().getApply_reason());

        GoodsListAdapter goodsListAdapter = new GoodsListAdapter(this, approvalDetailBean.getGoods_info());
        rv_goods_container.setLayoutManager(new LinearLayoutManager(this));
        rv_goods_container.setAdapter(goodsListAdapter);
        rv_goods_container.setNestedScrollingEnabled(false);

        tv_freight.setText("￥" + Utils.getDisplayMoney(approvalDetailBean.getGoods_info().getOrder_main_other_total()));
        tv_goods_total_amount.setText("￥" + Utils.getDisplayMoney(approvalDetailBean.getGoods_info().getOrder_main_goods_total()));
        tv_order_total_amout.setText("￥" + Utils.getDisplayMoney(approvalDetailBean.getGoods_info().getOrder_main_amount()));

        if (approvalDetailBean.getGoods_info().getOrder_main_deal_active() == CREDIT_PAY) {
            int pay_time_limit = 0;//付款时限
            if(approvalDetailBean.getGoods_info().getOrder_main_pay_active()==3){
                //在线支付
                pay_time_limit = 3;
            }else {
                //银行转账和支票支付
                pay_time_limit = 7;
            }
            rl_credit_pay.setVisibility(View.VISIBLE);
            rl_credit_pay_two.setVisibility(View.VISIBLE);
            tv_first_pay.setText("￥" + Utils.getDisplayMoney(approvalDetailBean.getGoods_info().getOrder_main_pay_first()));
            tv_end_pay.setText("￥" + Utils.getDisplayMoney(approvalDetailBean.getGoods_info().getOrder_main_pay_second()));
            if (approvalDetailBean.getGoods_info().getSupplement_amount() > 0) {
                rl_supplement.setVisibility(View.VISIBLE);
                tv_supplement.setText("￥" + Utils.getDisplayMoney(approvalDetailBean.getGoods_info().getSupplement_amount()));
                tv_tip.setText(MessageFormat.format(getString(R.string.pay_and_freight_and_supplement_tip),pay_time_limit+""));
                tv_tip_2.setText(R.string.stage_one_payment_3);
            } else {
                rl_supplement.setVisibility(View.GONE);
                tv_tip.setText(MessageFormat.format(getString(R.string.pay_and_freight_tip),pay_time_limit+""));
                tv_tip_2.setText(R.string.stage_one_payment_2);
            }

            tv_one_pay.setText("￥" + Utils.getDisplayMoney(approvalDetailBean.getGoods_info().getOrder_main_pay_one()));
            tv_two_pay.setText("￥" + Utils.getDisplayMoney(approvalDetailBean.getGoods_info().getOrder_main_pay_two()));
            double currenyOneAmount = approvalDetailBean.getGoods_info().getOrder_main_pay_one()
                    * approvalDetailBean.getGoods_info().getOrder_main_pay_currency_rate();
            tv_stage_one_pay.setText(approvalDetailBean.getGoods_info()
                    .getOrder_main_pay_currency_symbol() + Utils.getDisplayMoney(currenyOneAmount));
            double currenyTwoAmount = approvalDetailBean.getGoods_info().getOrder_main_pay_two()
                    * approvalDetailBean.getGoods_info().getOrder_main_pay_currency_rate();
            tv_stage_two_pay.setText(approvalDetailBean.getGoods_info()
                    .getOrder_main_pay_currency_symbol() + Utils.getDisplayMoney(currenyTwoAmount));
            tv_trade_way.setText(R.string.credit_pay);
//            tv_second_pay_tip.setText(MessageFormat.format(getString(R.string.end_pay_tip),pay_time_limit+""));
            tv_second_pay_tip.setText(MessageFormat.format(getString(R.string.end_pay_tip),pay_time_limit+""));
        } else if (approvalDetailBean.getGoods_info().getOrder_main_deal_active() == FULL_AMOUNT_PAY) {
            rl_credit_pay.setVisibility(View.GONE);
            rl_credit_pay_two.setVisibility(View.GONE);
            tv_trade_way.setText(R.string.full_amount_pay);
        }

        switch (approvalDetailBean.getGoods_info().getOrder_main_pay_active()) {
            case BANK_PAY:
                tv_pay_way.setText(R.string.bank_pay);
                break;
            case CHECK_PAY:
                tv_pay_way.setText(R.string.check_pay);
                break;
            case ONLINE_PAY:
                tv_pay_way.setText(R.string.online_pay);
                break;
        }
        tv_pay_currency.setText(approvalDetailBean.getGoods_info().getOrder_main_pay_currency_name());
        tv_pay_rate.setText(approvalDetailBean.getGoods_info().getOrder_main_pay_currency_rate()+"");
        double currenyTotalAmount = approvalDetailBean.getGoods_info().getOrder_main_amount()
                * approvalDetailBean.getGoods_info().getOrder_main_pay_currency_rate();
        tv_total_amount_payable.setText(approvalDetailBean.getGoods_info()
                .getOrder_main_pay_currency_symbol() + Utils.getDisplayMoney(currenyTotalAmount));

        ApprovalDetailBean.FlowDataBean flowDataBean = new ApprovalDetailBean.FlowDataBean();
        flowDataBean.setStatus(true);
        flowDataBean.setName(approvalDetailBean.getApprove_order_info().getApprove_creator());
        flowDataBean.setStatus_name(5);
        flowDataBean.setApprove_time(approvalDetailBean.getApprove_order_info().getUpdate_time());
        approvalDetailBean.getFlow_data().add(0, flowDataBean);
        ApprovalFlowAdapter approvalFlowAdapter = new ApprovalFlowAdapter(this, approvalDetailBean.getFlow_data());
        rv_flow_container.setLayoutManager(new LinearLayoutManager(this));
        rv_flow_container.setAdapter(approvalFlowAdapter);
        rv_flow_container.setNestedScrollingEnabled(false);


        List<String> copyToMembers = approvalDetailBean.getCopy_to_member();
        if (copyToMembers == null || copyToMembers.size() == 0) {
            rl_copy_to_me.setVisibility(View.GONE);
        } else {
            rl_copy_to_me.setVisibility(View.VISIBLE);
            tll_copy_to_container.setAdapter(new TagAdapter<String>(copyToMembers) {
                @Override
                public View getView(FlowLayout parent, int position, String copyToMember) {
                    View view = View.inflate(ApprovalDetailEnterpriseActivity.this, R.layout.item_copy_to, null);
                    TextView tv_copy_to_name = view.findViewById(R.id.tv_copy_to_name);
                    tv_copy_to_name.setText(copyToMember);
                    return view;
                }
            });
        }

        if (approvalDetailBean.getApprove_order_info().getIs_goto_pay()==1) {
            rl_goto_pay.setVisibility(View.VISIBLE);
        } else {
            rl_goto_pay.setVisibility(View.GONE);
        }

        if (approvalDetailBean.getApprove_order_info().getStatus() == APPROVAL_ING_ONE ||
                approvalDetailBean.getApprove_order_info().getStatus() == APPROVAL_ING_TWO) {
            ll_approval_btn.setVisibility(View.VISIBLE);
            if (approvalDetailBean.getApprove_order_info().isIs_creator()) {
                tv_repeal.setVisibility(View.VISIBLE);
            } else {
                tv_repeal.setVisibility(View.GONE);
            }
            if (approvalDetailBean.getApprove_order_info().isIs_approve()) {
                tv_consent.setVisibility(View.VISIBLE);
                view_line_consent.setVisibility(View.VISIBLE);
                tv_reject.setVisibility(View.VISIBLE);
                view_line_reject.setVisibility(View.VISIBLE);
            } else {
                tv_consent.setVisibility(View.GONE);
                view_line_consent.setVisibility(View.GONE);
                tv_reject.setVisibility(View.GONE);
                view_line_reject.setVisibility(View.GONE);
            }
        } else {
            ll_approval_btn.setVisibility(View.GONE);
        }
        ll_detail.setVisibility(View.VISIBLE);

    }

    private void showApprovalOpinionDialog() {
        if (mApprovalOpinionDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            mApprovalOpinionDialog = builder.create();
            View view = LayoutInflater.from(this).inflate(R.layout.dialog_approval_opinion, null);
            final EditText et_opinion = view.findViewById(R.id.et_opinion);
            et_opinion.requestFocus();
            Button bt_cancel = view.findViewById(R.id.bt_cancel);
            Button bt_confirm = view.findViewById(R.id.bt_confirm);
            mApprovalOpinionDialog.setCanceledOnTouchOutside(false);
            mApprovalOpinionDialog.setCancelable(true);
            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mApprovalOpinionDialog.dismiss();
                    et_opinion.setText("");
                }
            });
            bt_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String opinion = et_opinion.getText().toString().trim();
                    requestApprovalOpinion(mApprovalStatus, opinion);
                    mApprovalOpinionDialog.dismiss();
                }
            });
            mApprovalOpinionDialog.show();
            //setContentView一定要放在show后面才行显示出页面

            mApprovalOpinionDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            mApprovalOpinionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            mApprovalOpinionDialog.setContentView(view);
        } else {
            mApprovalOpinionDialog.show();
        }

    }

    @Override
    public void onBackPressed() {
        if (isSetResult) {
            EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_APPROVAL_LIST));
        }
        if(KyyApp.activitynum==1){
            goToActivity(MainActivity.class);
        }
        super.onBackPressed();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event){
        if(event.getMessage().equals(MessageEvent.REFRESH_APPROVAL_DETAIL)){
            requestApprovalDetail();
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
