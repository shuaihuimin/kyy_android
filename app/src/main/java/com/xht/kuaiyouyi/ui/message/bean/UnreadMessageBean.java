package com.xht.kuaiyouyi.ui.message.bean;

public class UnreadMessageBean {

    /**
     * message : {"newcompany":2,"newapporove":1,"newsystem":0,"newlogistics":0,"newchat":0}
     */

    private MessageBean message;

    public MessageBean getMessage() {
        return message;
    }

    public void setMessage(MessageBean message) {
        this.message = message;
    }

    public static class MessageBean {
        /**
         * newcompany : 2
         * newapporove : 1
         * newsystem : 0
         * newlogistics : 0
         * newchat : 0
         */

        private int newcompany;//验证信息
        private int newapporove;//工作通知
        private int newsystem;//系统信息
        private int newchat;//聊天消息

        public int getNewcompany() {
            return newcompany;
        }

        public void setNewcompany(int newcompany) {
            this.newcompany = newcompany;
        }

        public int getNewapporove() {
            return newapporove;
        }

        public void setNewapporove(int newapporove) {
            this.newapporove = newapporove;
        }

        public int getNewsystem() {
            return newsystem;
        }

        public void setNewsystem(int newsystem) {
            this.newsystem = newsystem;
        }

        public int getNewchat() {
            return newchat;
        }

        public void setNewchat(int newchat) {
            this.newchat = newchat;
        }
    }
}
