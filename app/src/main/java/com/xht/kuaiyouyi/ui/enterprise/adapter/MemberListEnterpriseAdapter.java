package com.xht.kuaiyouyi.ui.enterprise.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.enterprise.bean.MemberListBean;

public class MemberListEnterpriseAdapter extends BaseAdapter {
    private Context mContext;
    private MemberListBean mMemberListBeans;

    public MemberListEnterpriseAdapter(Context mContext, MemberListBean memberListBeans) {
        this.mContext = mContext;
        this.mMemberListBeans = memberListBeans;
    }

    @Override
    public int getCount() {
        if(mMemberListBeans.getCompany() == null){
            return 0;
        }
        return mMemberListBeans.getCompany().size();
    }

    @Override
    public Object getItem(int position) {
        return mMemberListBeans.getCompany().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.item_enterprise_member_list, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_name = convertView.findViewById(R.id.tv_name);
            viewHolder.tv_role = convertView.findViewById(R.id.tv_role);
            viewHolder.view_top = convertView.findViewById(R.id.view_top);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if(position==0){
            viewHolder.view_top.setVisibility(View.VISIBLE);
        }else {
            viewHolder.view_top.setVisibility(View.GONE);
        }
        viewHolder.tv_name.setText(mMemberListBeans.getCompany().get(position).getMember_name());
        viewHolder.tv_role.setText(mMemberListBeans.getCompany().get(position).getMember_position());
        return convertView;
    }

    private class ViewHolder{
        private TextView tv_name,tv_role;
        private View view_top;
    }
}
