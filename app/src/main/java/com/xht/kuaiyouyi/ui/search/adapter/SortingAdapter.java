package com.xht.kuaiyouyi.ui.search.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSONObject;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.search.entity.DescHolder;
import com.xht.kuaiyouyi.ui.search.entity.HeaderHolder;
import com.xht.kuaiyouyi.ui.search.entity.ScreenBean;
import com.xht.kuaiyouyi.utils.HotelUtils;

import java.util.List;

/**
 * Created by shuaihuimin on 2018/6/29.
 * 分类筛选
 */

public class SortingAdapter extends SectionedRecyclerViewAdapter<HeaderHolder,DescHolder,RecyclerView.ViewHolder> implements View.OnClickListener{
    private Context context;
    private List<ScreenBean> list;
    private LayoutInflater mInflater;
    private SparseBooleanArray mBooleanMap;
    private OnItemClickListener mItemClickListener;
    private JSONObject jsonObject;
    private int flag;

    public SortingAdapter(Context context, List<ScreenBean> list, int flag){
        this.context = context;
        this.list=list;
        this.flag=flag;
        mInflater = LayoutInflater.from(context);
        mBooleanMap = new SparseBooleanArray();

    }


    public void setData(List<ScreenBean> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @Override
    protected int getSectionCount() {
        return HotelUtils.isEmpty(list)?0:list.size();
    }

    @Override
    protected int getItemCountForSection(int section) {

        int count = list.get(section).getTagInfo().size();
        if (count >= 3 && !mBooleanMap.get(section)) {
            count = 3;
        }

        return HotelUtils.isEmpty(list.get(section).getTagInfo()) ? 0 : count;
    }

    @Override
    protected boolean hasFooterInSection(int section) {
        return false;
    }

    @Override
    protected HeaderHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
        return new HeaderHolder(mInflater.inflate(R.layout.typelist_head_item_layout, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected DescHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.typelist_content_item_layout, parent,false);
        DescHolder descHolder=new DescHolder(view);
        descHolder.itemView.setOnClickListener(this);
        return descHolder;
    }

    @Override
    protected void onBindSectionHeaderViewHolder(final HeaderHolder holder, final int section) {

        holder.openView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isOpen = mBooleanMap.get(section);
                //String text = isOpen ? "展开" : "关闭";
                mBooleanMap.put(section, !isOpen);
                notifyDataSetChanged();
            }
        });
        boolean isOpen = mBooleanMap.get(section);
        if(isOpen == true){
            holder.openView.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.mipmap.search_icon_close_nor),null);
        }else {
            holder.openView.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.mipmap.search_icon_open_nor),null);
        }

        holder.titleView.setText(list.get(section).getName());
        //holder.openView.setText(mBooleanMap.get(section) ? "关闭" : "展开");
        //holder.openView.setText(R.string.completely);
        if(TextUtils.isEmpty(list.get(section).getTitle())){
            holder.openView.setText(R.string.completely);
            holder.openView.setTextColor(context.getResources().getColor(R.color.grey_7));
        }else {
//            String str=list.get(section).getTitle().substring(0,list.get(section).getTitle().length()-1);
//            holder.openView.setText(str);
//            holder.openView.setTextColor(context.getResources().getColor(R.color.red_1));
        }
        if(flag==1){
            holder.openView.setVisibility(View.VISIBLE);
            holder.tv_title.setVisibility(View.VISIBLE);
        }else if(flag==2){
            holder.openView.setVisibility(View.GONE);
            holder.tv_title.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onBindSectionFooterViewHolder(RecyclerView.ViewHolder holder, int section) {

    }

    @Override
    protected void onBindItemViewHolder(DescHolder holder, int section, int position) {
        if(!TextUtils.isEmpty(list.get(section).getTagInfo().get(position).getName())){
            holder.descView.setText(list.get(section).getTagInfo().get(position).getName());
        }
        if (list.get(section).getTagInfo().get(position).isSelect() == true){
            holder.descView.setBackground(context.getResources().getDrawable(R.drawable.specifications_bg));
        }else{
            //holder.descView.setBackgroundColor(context.getResources().getColor(R.color._ffffff));
            holder.descView.setBackground(context.getResources().getDrawable(R.drawable.specifications_fbg));
        }

        jsonObject = new JSONObject();
        jsonObject.put("section",section);
        jsonObject.put("position",position);
        jsonObject.put("name",list.get(section).getTagInfo().get(position).getName());
        holder.itemView.setTag(jsonObject);
    }

    @Override
    public void onClick(View v) {
        if (mItemClickListener!=null){
            mItemClickListener.onItemClick((JSONObject)v.getTag());
        }
    }

    public interface OnItemClickListener{
        void onItemClick(JSONObject jsonObject);
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }
}