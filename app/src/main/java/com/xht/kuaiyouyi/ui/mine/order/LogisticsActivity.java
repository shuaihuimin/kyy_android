package com.xht.kuaiyouyi.ui.mine.order;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.mine.adapter.LogisticsAdapter;
import com.xht.kuaiyouyi.ui.mine.entity.LogisticsBean;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class LogisticsActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.lv_content)
    ListView lv_content;

    private String mOrderId;

    public static String ORDER_ID = "order_id";

    @Override
    protected int getLayout() {
        return R.layout.activity_logistics;
    }

    @Override
    protected void initView() {
        mOrderId = getIntent().getStringExtra(ORDER_ID);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_title.setText(R.string.look_logistics);
        requestLogistics();
    }

    private void requestLogistics() {
        DialogUtils.createTipAllLoadDialog(this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGISTICS())
                .addParam("order_main_id", mOrderId)
                .withPOST(new NetCallBack<LogisticsBean>() {
                    @NotNull
                    @Override
                    public Class<LogisticsBean> getRealType() {
                        return LogisticsBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(LogisticsActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull LogisticsBean logisticsBean) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        if (logisticsBean.getLogistics_info() != null) {
                            LogisticsAdapter logisticsAdapter =
                                    new LogisticsAdapter(LogisticsActivity.this, logisticsBean);
                            lv_content.setAdapter(logisticsAdapter);
                            lv_content.setVisibility(View.VISIBLE);
                        }
                    }
                }, false);
    }


}
