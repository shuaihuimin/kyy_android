package com.xht.kuaiyouyi.ui;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.WindowManager;
import android.widget.ImageView;

import com.huawei.android.hms.agent.HMSAgent;
import com.huawei.android.hms.agent.common.handler.ConnectHandler;
import com.huawei.android.hms.agent.push.handler.GetTokenHandler;
import com.xht.kuaiyouyi.BuildConfig;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.login.entity.UpdateVersionBean;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class SplashActivity extends BaseActivity {
    @BindView(R.id.logo_center)
    ImageView logo_center;

    @Override
    protected int getLayout() {
        return R.layout.activity_splash;
    }


    @Override
    protected void initView() {
        requestCheckVersion();
        //请求华为推送token
        if(Utils.isHUAWEI()){
            HMSAgent.connect(this, new ConnectHandler() {
                @Override
                public void onConnect(int rst) {
                    HMSAgent.Push.getToken(new GetTokenHandler() {
                        @Override
                        public void onResult(int rst) {

                        }
                    });
                }
            });
        }
    }

    private void requestCheckVersion() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_UPADATE_VERSION())
                .addParam("client", "android")
                .addParam("version", BuildConfig.VERSION_NAME)
                .withPOST(new NetCallBack<UpdateVersionBean>() {

                    @NotNull
                    @Override
                    public Class<UpdateVersionBean> getRealType() {
                        return UpdateVersionBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        next();
                    }

                    @Override
                    public void onSuccess(@NonNull final UpdateVersionBean updateVersionBean) {
                        if (updateVersionBean.getRes() != null) {
                            final boolean isForceUpdate = updateVersionBean.getRes().isForceUpdate();
                            if (updateVersionBean.getRes().isIs_update()) {
                                String leftButtonText;
                                if (isForceUpdate) {
                                    leftButtonText = getString(R.string.exit);
                                } else {
                                    leftButtonText = getString(R.string.dialog_cancel);
                                }
                                DialogUtils.createTwoBtnDialog(SplashActivity.this
                                        , updateVersionBean.getRes().getUpgradePoint()
                                        , getString(R.string.dialog_confirm)
                                        , leftButtonText, new DialogUtils.OnRightBtnListener() {
                                            @Override
                                            public void setOnRightListener(Dialog dialog) {
                                                Uri uri = Uri.parse(updateVersionBean.getRes().getInstallPackage());
                                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }, new DialogUtils.OnLeftBtnListener() {
                                            @Override
                                            public void setOnLeftListener(Dialog dialog) {
                                                if(isForceUpdate){
                                                    finish();
                                                }else {
                                                    next();
                                                }
                                            }
                                        }, false, false);
                                return;
                            }
                        }
                        next();
                    }
                }, false);
    }

    public void next() {
        logo_center.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (TextUtils.isEmpty(Login.Companion.getInstance().isShowGuide())) {
                    goToActivity(GuideActivity.class);
                } else {
                    /**
                     * 切换为非全屏
                     */
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                    goToActivity(MainActivity.class);
                }
                finish();


            }
        }, 1000);
    }

    @Override
    public void onBackPressed() {

    }
}
