package com.xht.kuaiyouyi.ui.goods;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.OnPhotoTapListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.message.adapter.MyDynamicPageAdapter;
import com.xht.kuaiyouyi.widget.FixedViewpager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagesDisplayActivity extends BaseActivity {

    @BindView(R.id.viewpager)
    FixedViewpager vp_pager;
    @BindView(R.id.tv_num)
    TextView tv_indicator;//页数显示
    @BindView(R.id.linear)
    LinearLayout large_linear;//总布局
    private int pagerPosition;
    private String imgs[] = new String[1];//上个界面的地址
    private List<View> listViews = new ArrayList<View>();//填充的list容器
    private PhotoView iv_gridview_dynamic;//填充的imageview布局
    private MyDynamicPageAdapter adapter;//自定义viewpager适配器

    public static final String STATE_POSITION = "STATE_POSITION";//图片当前位置

    @Override
    protected int getLayout() {
        return R.layout.activity_preview;
    }

    @Override
    protected void initView() {
        pagerPosition = getIntent().getIntExtra("pos",0);
        imgs[0] = getIntent().getExtras().getString("img");
        //FrameLayout large_frame=(FrameLayout)findViewById(R.id.large_frame);
        large_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        large_linear.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        finish();
                        break;
                }

                return false;
            }
        });
        initImage();
        tv_indicator.setText((pagerPosition+1)+"/"+imgs.length);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setListener(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setListener(Bundle savedInstanceState){
        // 更新下标
        vp_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageSelected(int arg0) {
                tv_indicator.setText((arg0 + 1) + "/" + imgs.length);
            }

        });
        if (savedInstanceState != null) {
            pagerPosition = savedInstanceState.getInt(STATE_POSITION);
        }

        vp_pager.setCurrentItem(pagerPosition);
    }
    /**
     * 监听返回按钮
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
        }
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt(STATE_POSITION, vp_pager.getCurrentItem());
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

        public void onPageSelected(int arg0) {
            pagerPosition = arg0;
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void initImage() {
        for (int i = 0; i < imgs.length; i++) {
            View topView = LayoutInflater.from(this).inflate(
                    R.layout.activity_larger, null);
            iv_gridview_dynamic = (PhotoView) topView
                    .findViewById(R.id.iv_big);

            iv_gridview_dynamic.setOnPhotoTapListener(new OnPhotoTapListener() {
                @Override
                public void onPhotoTap(ImageView view, float x, float y) {
                    finish();
                }
            });

            Glide.with(this).load(imgs[i]).into(iv_gridview_dynamic);

            listViews.add(iv_gridview_dynamic);

        }

        vp_pager.setOnPageChangeListener(pageChangeListener);
        adapter = new MyDynamicPageAdapter(this,listViews);
        vp_pager.setAdapter(adapter);
        vp_pager.setCurrentItem(pagerPosition);
    }

}
