package com.xht.kuaiyouyi.ui.enterprise.adapter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.enterprise.bean.EnterpriseApprovalBean;
import com.xht.kuaiyouyi.utils.Utils;

public class ApprovalListAdapter extends BaseAdapter {
    private Context mContext;
    private EnterpriseApprovalBean mEnterpriseApprovalBean;
    private boolean mAllShowPoint;

    //0待审批 1审批中 2 审批通过 3 审批拒绝 4已撤销 5订单取消
    private final int APPROVAL_WAIT = 0;
    private final int APPROVAL_ING = 1;
    private final int APPROVAL_PASS = 2;
    private final int APPROVAL_REJECT = 3;
    private final int APPROVAL_REPEAL = 4;
    private final int ORDER_CANCEL = 5;

    public ApprovalListAdapter(Context mContext, EnterpriseApprovalBean enterpriseApprovalBean,boolean mAllShowPoint) {
        this.mContext = mContext;
        this.mEnterpriseApprovalBean = enterpriseApprovalBean;
        this.mAllShowPoint = mAllShowPoint;
    }

    @Override
    public int getCount() {
        if(mEnterpriseApprovalBean.getResult() == null){
            return 0;
        }
        return mEnterpriseApprovalBean.getResult().size();
    }

    @Override
    public Object getItem(int position) {
        return mEnterpriseApprovalBean.getResult().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.item_approval_list, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_name = convertView.findViewById(R.id.tv_name);
            viewHolder.tv_delivery_time = convertView.findViewById(R.id.tv_delivery_time);
            viewHolder.tv_apply_reason = convertView.findViewById(R.id.tv_apply_reason);
            viewHolder.tv_apply_time = convertView.findViewById(R.id.tv_apply_time);
            viewHolder.tv_approval_status = convertView.findViewById(R.id.tv_approval_status);
            viewHolder.tv_point = convertView.findViewById(R.id.tv_point);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_name.setText(mEnterpriseApprovalBean.getResult()
                .get(position).getMember_truename());
        viewHolder.tv_delivery_time.setText(Utils.getDisplayData(mEnterpriseApprovalBean
                .getResult().get(position).getExpect_delivery_time()));
        viewHolder.tv_apply_reason.setText(mEnterpriseApprovalBean.getResult()
                .get(position).getApply_reason());
        viewHolder.tv_apply_time.setText(Utils.getDisplayData(mEnterpriseApprovalBean
                .getResult().get(position).getStart_time()));
        viewHolder.tv_approval_status.setText(Html.fromHtml(mEnterpriseApprovalBean.getResult()
                .get(position).getStatus_name()));
        if(!mAllShowPoint){
            switch (mEnterpriseApprovalBean.getResult().get(position).getStatus()){
                case APPROVAL_WAIT:
                    viewHolder.tv_approval_status.setTextColor(mContext.getResources().getColor(R.color.blue_normal));
                    viewHolder.tv_approval_status.setBackgroundColor(mContext.getResources().getColor(R.color.blue_alpha));
                    break;
                case APPROVAL_ING:
                    viewHolder.tv_approval_status.setTextColor(mContext.getResources().getColor(R.color.blue_normal));
                    viewHolder.tv_approval_status.setBackgroundColor(mContext.getResources().getColor(R.color.blue_alpha));
                    break;
                case APPROVAL_PASS:
                    viewHolder.tv_approval_status.setTextColor(mContext.getResources().getColor(R.color.green_1));
                    viewHolder.tv_approval_status.setBackgroundColor(mContext.getResources().getColor(R.color.green_alpha));
                    break;
                case APPROVAL_REJECT:
                    viewHolder.tv_approval_status.setTextColor(mContext.getResources().getColor(R.color.red_1));
                    viewHolder.tv_approval_status.setBackgroundColor(mContext.getResources().getColor(R.color.red_alpha));
                    break;
                case APPROVAL_REPEAL:
                    viewHolder.tv_approval_status.setTextColor(mContext.getResources().getColor(R.color.orange_1));
                    viewHolder.tv_approval_status.setBackgroundColor(mContext.getResources().getColor(R.color.orange_alpha));
                    break;
                case ORDER_CANCEL:
                    viewHolder.tv_approval_status.setTextColor(mContext.getResources().getColor(R.color.blue_normal));
                    viewHolder.tv_approval_status.setBackgroundColor(mContext.getResources().getColor(R.color.blue_alpha));
                    break;
            }
        }else {
            //待我审批
            viewHolder.tv_approval_status.setVisibility(View.GONE);
            mEnterpriseApprovalBean.getResult().get(position).setIs_read(0);
        }

        if(mEnterpriseApprovalBean.getResult().get(position).isIs_read()==0){
            viewHolder.tv_point.setVisibility(View.VISIBLE);
        }else {
            viewHolder.tv_point.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    private class ViewHolder{
        private TextView tv_name,tv_apply_time,tv_apply_reason,tv_delivery_time,tv_approval_status,tv_point;
    }
}
