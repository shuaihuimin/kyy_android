package com.xht.kuaiyouyi.ui.enterprise.bean;

import java.util.List;

public class MemberListBean {


    /**
     * company : [{"member_name":"runmin","id":"154","member_id":"28","member_truename":"兰博基尼","company_id":"91","member_tel":"13484622656","member_email":"46516@qq.com","member_position":"负责人","is_admin":"1"},{"member_name":"helloworld","id":"171","member_id":"6","member_truename":"黄黄滨","company_id":"91","member_tel":"13417073722","member_email":null,"member_position":null,"is_admin":"0"}]
     * position : [{"role_id":"55","company_id":"91","role_name":"总经理","rules":"1,10,2,3,4,5,6,9,8"},{"role_id":"56","company_id":"91","role_name":"正式员工","rules":"2,3,5"}]
     * kyy : eyJLIjoia3l5XzAwMiIsIk8iOiI5MSIsIlAiOjF9
     */
    private String kyy;
    private List<CompanyBean> company;
    private int is_invite;

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public List<CompanyBean> getCompany() {
        return company;
    }

    public void setCompany(List<CompanyBean> company) {
        this.company = company;
    }

    public int getIs_invite() {
        return is_invite;
    }

    public void setIs_invite(int is_invite) {
        this.is_invite = is_invite;
    }

    public static class CompanyBean {
        /**
         * member_name : runmin
         * id : 154
         * member_id : 28
         * member_truename : 兰博基尼
         * company_id : 91
         * member_tel : 13484622656
         * member_email : 46516@qq.com
         * member_position : 负责人
         * is_admin : 1
         */

        private String member_name;
        private String id;
        private String member_id;
        private String member_truename;
        private String company_id;
        private String member_tel;
        private String member_email;
        private String member_position;
        private String is_admin;

        public String getMember_name() {
            return member_name;
        }

        public void setMember_name(String member_name) {
            this.member_name = member_name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMember_id() {
            return member_id;
        }

        public void setMember_id(String member_id) {
            this.member_id = member_id;
        }

        public String getMember_truename() {
            return member_truename;
        }

        public void setMember_truename(String member_truename) {
            this.member_truename = member_truename;
        }

        public String getCompany_id() {
            return company_id;
        }

        public void setCompany_id(String company_id) {
            this.company_id = company_id;
        }

        public String getMember_tel() {
            return member_tel;
        }

        public void setMember_tel(String member_tel) {
            this.member_tel = member_tel;
        }

        public String getMember_email() {
            return member_email;
        }

        public void setMember_email(String member_email) {
            this.member_email = member_email;
        }

        public String getMember_position() {
            return member_position;
        }

        public void setMember_position(String member_position) {
            this.member_position = member_position;
        }

        public String getIs_admin() {
            return is_admin;
        }

        public void setIs_admin(String is_admin) {
            this.is_admin = is_admin;
        }
    }

}
