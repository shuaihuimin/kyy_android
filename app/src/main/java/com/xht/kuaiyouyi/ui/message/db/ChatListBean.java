package com.xht.kuaiyouyi.ui.message.db;

import java.util.List;

/**
 * 聊天列表表
 */
public class ChatListBean {


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * u_id : 1
         * store_id : 1
         * u_name : 快优易自营
         * avatar : http://192.168.0.2/data/upload/shop/common/05950885781466344.png
         * recent : 1
         * m_id : 2466
         * time : 2018-11-02 16:27:59
         * r_state : 1
         * t_msg : hbh
         * store_name : 快优易自营
         * store_avatar : http://192.168.0.2/data/upload/shop/store/05916217113435759.gif
         * no_read_msg_num : 0
         */

        private String u_id;
        private String store_id;
        private String u_name;
        private String avatar;
        private int recent;
        private String m_id;
        private String time;
        private int r_state;
        private String t_msg;
        private String store_name;
        private String store_avatar;
        private int no_read_msg_num;
        private int msg_type;

        public String getU_id() {
            return u_id;
        }

        public void setU_id(String u_id) {
            this.u_id = u_id;
        }

        public String getStore_id() {
            return store_id;
        }

        public void setStore_id(String store_id) {
            this.store_id = store_id;
        }

        public String getU_name() {
            return u_name;
        }

        public void setU_name(String u_name) {
            this.u_name = u_name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public int getRecent() {
            return recent;
        }

        public void setRecent(int recent) {
            this.recent = recent;
        }

        public String getM_id() {
            return m_id;
        }

        public void setM_id(String m_id) {
            this.m_id = m_id;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public int getR_state() {
            return r_state;
        }

        public void setR_state(int r_state) {
            this.r_state = r_state;
        }

        public String getT_msg() {
            return t_msg;
        }

        public void setT_msg(String t_msg) {
            this.t_msg = t_msg;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }

        public String getStore_avatar() {
            return store_avatar;
        }

        public void setStore_avatar(String store_avatar) {
            this.store_avatar = store_avatar;
        }

        public int getNo_read_msg_num() {
            return no_read_msg_num;
        }

        public void setNo_read_msg_num(int no_read_msg_num) {
            this.no_read_msg_num = no_read_msg_num;
        }

        public int getMsg_type() {
            return msg_type;
        }

        public void setMsg_type(int msg_type) {
            this.msg_type = msg_type;
        }
    }
}
