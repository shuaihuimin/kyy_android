package com.xht.kuaiyouyi.ui.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.goods.StoreActivity;
import com.xht.kuaiyouyi.ui.home.entity.BrandDetilsBean;
import com.xht.kuaiyouyi.ui.mine.setting.CurrencyActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import java.util.List;

public class BrandDetilsAdapter extends BaseQuickAdapter<BrandDetilsBean.GoodsListBean,BaseViewHolder> {
    private Context context;
    private BrandDetilsBean.CurrencytypeBean currencytypeBean;
    private boolean islist;
    private int height;
    public BrandDetilsAdapter(Context context,int layoutResId, @Nullable List<BrandDetilsBean.GoodsListBean> data,BrandDetilsBean.CurrencytypeBean currencytypeBean,boolean islist,int height) {
        super(layoutResId, data);
        this.context=context;
        this.currencytypeBean=currencytypeBean;
        this.islist=islist;
        this.height=height;
    }

    public BrandDetilsAdapter(@Nullable List<BrandDetilsBean.GoodsListBean> data) {
        super(data);
    }

    public BrandDetilsAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final BrandDetilsBean.GoodsListBean item) {
        double currency=0;String price="";
        if(!islist){
            ViewGroup.LayoutParams layoutParams=helper.getView(R.id.iv_img).getLayoutParams();
            layoutParams.height= height;
        }
        if(!TextUtils.isEmpty(item.getGoods_image())){
            Glide.with(context).load(item.getGoods_image())
                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                    .into((ImageView) helper.getView(R.id.iv_img));
        }
        if (item.getIs_enquiry().equals("0")) {
            helper.getView(R.id.tv_inquiry).setVisibility(View.GONE);
            helper.getView(R.id.tv_price).setVisibility(View.VISIBLE);
            helper.getView(R.id.tv_dollar).setVisibility(View.VISIBLE);
        } else {
            helper.getView(R.id.tv_inquiry).setVisibility(View.VISIBLE);
            helper.getView(R.id.tv_price).setVisibility(View.GONE);
            helper.getView(R.id.tv_dollar).setVisibility(View.GONE);
        }
        if(currencytypeBean!=null) {
            if (Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.HKD)) {
                currency = Double.parseDouble(item.getGoods_price()) * currencytypeBean.getHKD().getRate();
                price = Utils.getDisplayMoney(currency);
                helper.setText(R.id.tv_price, "¥"+Utils.getDisplayMoney(Double.parseDouble(item.getGoods_price())))
                        .setText(R.id.tv_dollar, "(~" + currencytypeBean.getHKD().getSymbol() + price+")");
            } else if (Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.MOP)) {
                currency = Double.parseDouble(item.getGoods_price()) * currencytypeBean.getMOP().getRate();
                price = Utils.fmtMicrometer(Double.toString(currency), false);
                helper.setText(R.id.tv_price, "¥"+Utils.getDisplayMoney(Double.parseDouble(item.getGoods_price())))
                        .setText(R.id.tv_dollar, "(~" + currencytypeBean.getMOP().getSymbol() + price+")");
            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.RMB)){
                helper.setText(R.id.tv_price, "¥"+Utils.getDisplayMoney(Double.parseDouble(item.getGoods_price())));
                helper.getView(R.id.tv_dollar).setVisibility(View.GONE);
            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.USA)){
                currency = Double.parseDouble(item.getGoods_price()) * currencytypeBean.getUSD().getRate();
                price = Utils.fmtMicrometer(Double.toString(currency), false);
                helper.setText(R.id.tv_price, "¥"+Utils.getDisplayMoney(Double.parseDouble(item.getGoods_price())))
                        .setText(R.id.tv_dollar, "(~" + currencytypeBean.getUSD().getSymbol() + price+")");
            }
        }
        if(item.getIs_second_hand()==1){
            helper.setText(R.id.tv_name,Utils.secondLabel(item.getGoods_name()));
        }else {
            helper.setText(R.id.tv_name,item.getGoods_name());
        }
        //helper.setText(R.id.tv_price,item.getGoods_price());
        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("goods_id",item.getGoods_id());
                Intent intent=new Intent();
                intent.setClass(context, GoodsActivity.class);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

        if(islist){
            Log.i("info","----------store_id"+item.getStore_id());
            helper.setText(R.id.tv_store,item.getStore_name());
            helper.setOnClickListener(R.id.linear, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle=new Bundle();
                    bundle.putString("store_id",item.getStore_id());
                    Intent intent=new Intent();
                    intent.putExtras(bundle);
                    intent.setClass(context,StoreActivity.class);
                    context.startActivity(intent);
                }
            });
        }

        if(islist && helper.getPosition()!=0){
            helper.setGone(R.id.view_line,false);
        }else if (islist && helper.getPosition()==0){
            helper.setGone(R.id.view_line,true);
        }

    }
}
