package com.xht.kuaiyouyi.ui.cart.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.cart.entity.ConfirmOrderBean;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.utils.Utils;


public class ConfirmOrderAdapter extends BaseAdapter {
    private Context mContext;
    private ConfirmOrderBean.DataListBean.StoreListBean mgoodsBean;

    public ConfirmOrderAdapter(Context mContext, ConfirmOrderBean.DataListBean.StoreListBean mgoodsBean) {
        this.mContext = mContext;
        this.mgoodsBean = mgoodsBean;
    }

    @Override
    public int getCount() {
        if(mgoodsBean.getCart_list()!=null){
            return mgoodsBean.getCart_list().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mgoodsBean.getCart_list().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GoodsListAdapter.ChildViewHolder childViewHolder;
        if(convertView==null){
            convertView=View.inflate(mContext, R.layout.item_confirmorder_single_goods,null);
            childViewHolder=new GoodsListAdapter.ChildViewHolder(convertView);
            convertView.setTag(childViewHolder);
        }else {
            childViewHolder= (GoodsListAdapter.ChildViewHolder) convertView.getTag();
        }
        if(position==0){
            childViewHolder.view.setVisibility(View.VISIBLE);
        }else {
            childViewHolder.view.setVisibility(View.GONE);
        }
        childViewHolder.relative_goods_single.setVisibility(View.VISIBLE);
        childViewHolder.relative_afew.setVisibility(View.GONE);
        //childViewHolder.tv_name.setText(mgoodsBean.getCart_list().get(position).getGoods_name());
        if(mgoodsBean.getCart_list().get(position).getIs_second_hand()==1){
            childViewHolder.tv_name.setText(Utils.secondLabel(mgoodsBean.getCart_list().get(position).getGoods_name()));
        }else {
            childViewHolder.tv_name.setText(mgoodsBean.getCart_list().get(position).getGoods_name());
        }
        childViewHolder.tv_dollar.setText("¥"+ Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getCart_list().get(position).getGoods_price())));
        childViewHolder.tv_order_num.setText("x"+mgoodsBean.getCart_list().get(position).getGoods_num());
        childViewHolder.tv_total_price.setText("¥"+ Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getCart_list().get(position).getGoods_total())));
        childViewHolder.tv_model.setText(mgoodsBean.getCart_list().get(position).getGoods_spec());
        Glide.with(mContext).load(mgoodsBean.getCart_list().get(position).getGoods_image_url())
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                .into(childViewHolder.iv_goods_img);
        //运费
        childViewHolder.tv_goods_freight.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getCart_list().get(position).getFreight_fee())));
        childViewHolder.tv_test_fee.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getCart_list().get(position).getCustoms_charges_fee())));
        final String goods_id=Integer.toString(mgoodsBean.getCart_list().get(position).getGoods_id());
        childViewHolder.relative_goods_single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("goods_id",goods_id);
                Intent intent=new Intent(mContext, GoodsActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
        if(mgoodsBean.getCart_list().get(position).getCustoms_charges_fee().equals("0")){
            childViewHolder.tv_test_fee.setVisibility(View.GONE);
            childViewHolder.tv_fee.setVisibility(View.GONE);
        }else {
            childViewHolder.tv_test_fee.setVisibility(View.VISIBLE);
            childViewHolder.tv_fee.setVisibility(View.VISIBLE);
        }
        return convertView;
    }
}
