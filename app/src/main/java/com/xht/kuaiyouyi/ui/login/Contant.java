package com.xht.kuaiyouyi.ui.login;

public class Contant {
    public static final String TYPE_REGISTER = "1";//注册
    public static final String TYPE_lOGIN = "2";//登录
    public static final String TYPE_RESET_PASSWORD = "3";//重置密码
    public static final String TYPE_CHANGE_DEVICE = "4";//验证设备
    public static final String TYPE_CHANGE_PASSWORD = "5";//更改密码
    public static final String TYPE_CHANGE_PHONE = "7";//绑定手机修改
    public static final String TYPE_CHANGE_PAYMENT_CODE = "8";//支付密码修改


    public static final String TYPE = "type";
    public static final String PHONE = "phone";
    public static final String PASSWORD = "password";
    public static final String AREA_CODE = "area_code";
    public static final String VERIFY_CODE = "verify_code";
}
