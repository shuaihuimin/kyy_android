package com.xht.kuaiyouyi.ui.goods.entity;

import java.util.List;

public class demo {

    /**
     * goods_storage : 310
     * spec_all_info : [{"goods_promotion_price":"150.00","goods_price":150,"goods_marketprice":"160.00","goods_spec":"3m|250w","goods_id":"101007","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05896460911893666_360.jpg","goods_state":"1","goods_storage":100,"goods_verify":"1","is_enquiry":"0"},{"goods_promotion_price":"150.00","goods_price":150,"goods_marketprice":"160.00","goods_spec":"6m|250w","goods_id":"101008","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05896460911893666_360.jpg","goods_state":"1","goods_storage":100,"goods_verify":"1","is_enquiry":"0"},{"goods_promotion_price":"150.00","goods_price":150,"goods_marketprice":"160.00","goods_spec":"3m|360w","goods_id":"101009","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05896460911893666_360.jpg","goods_state":"1","goods_storage":10,"goods_verify":"1","is_enquiry":"0"},{"goods_promotion_price":"150.00","goods_price":150,"goods_marketprice":"160.00","goods_spec":"6m|360w","goods_id":"101010","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05896460911893666_360.jpg","goods_state":"1","goods_storage":100,"goods_verify":"1","is_enquiry":"0"}]
     * spec_checked : [{"id":32,"name":"输送带宽","item":[{"id":212,"name":"3m"},{"id":213,"name":"6m"}]},{"id":36,"name":"电机功率","item":[{"id":214,"name":"250w"},{"id":215,"name":"360w"}]}]
     * batch_price : [{"mix":1,"value":150,"max":10},{"mix":10,"value":"140","max":100},{"mix":100,"value":"130","max":null}]
     * currencytype : {"CNY":{"name":"人民币","symbol":"￥","currency":"CNY","cackeKey":"","rate":1},"HKD":{"name":"港币","symbol":"HK$","currency":"HKD","cackeKey":"cacheCurrencyHKD","rate":1.138434},"MOP":{"name":"澳门币","symbol":"MOP$","currency":"MOP","cackeKey":"cacheCurrencyMOP","rate":1.170138},"USD":{"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.144987}}
     */

    private int goods_storage;
    private CurrencytypeBean currencytype;
    private List<SpecAllInfoBean> spec_all_info;
    private List<SpecCheckedBean> spec_checked;
    private List<BatchPriceBean> batch_price;

    public int getGoods_storage() {
        return goods_storage;
    }

    public void setGoods_storage(int goods_storage) {
        this.goods_storage = goods_storage;
    }

    public CurrencytypeBean getCurrencytype() {
        return currencytype;
    }

    public void setCurrencytype(CurrencytypeBean currencytype) {
        this.currencytype = currencytype;
    }

    public List<SpecAllInfoBean> getSpec_all_info() {
        return spec_all_info;
    }

    public void setSpec_all_info(List<SpecAllInfoBean> spec_all_info) {
        this.spec_all_info = spec_all_info;
    }

    public List<SpecCheckedBean> getSpec_checked() {
        return spec_checked;
    }

    public void setSpec_checked(List<SpecCheckedBean> spec_checked) {
        this.spec_checked = spec_checked;
    }

    public List<BatchPriceBean> getBatch_price() {
        return batch_price;
    }

    public void setBatch_price(List<BatchPriceBean> batch_price) {
        this.batch_price = batch_price;
    }

    public static class CurrencytypeBean {
        /**
         * CNY : {"name":"人民币","symbol":"￥","currency":"CNY","cackeKey":"","rate":1}
         * HKD : {"name":"港币","symbol":"HK$","currency":"HKD","cackeKey":"cacheCurrencyHKD","rate":1.138434}
         * MOP : {"name":"澳门币","symbol":"MOP$","currency":"MOP","cackeKey":"cacheCurrencyMOP","rate":1.170138}
         * USD : {"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.144987}
         */

        private CNYBean CNY;
        private HKDBean HKD;
        private MOPBean MOP;
        private USDBean USD;

        public CNYBean getCNY() {
            return CNY;
        }

        public void setCNY(CNYBean CNY) {
            this.CNY = CNY;
        }

        public HKDBean getHKD() {
            return HKD;
        }

        public void setHKD(HKDBean HKD) {
            this.HKD = HKD;
        }

        public MOPBean getMOP() {
            return MOP;
        }

        public void setMOP(MOPBean MOP) {
            this.MOP = MOP;
        }

        public USDBean getUSD() {
            return USD;
        }

        public void setUSD(USDBean USD) {
            this.USD = USD;
        }

        public static class CNYBean {
            /**
             * name : 人民币
             * symbol : ￥
             * currency : CNY
             * cackeKey :
             * rate : 1
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private int rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public int getRate() {
                return rate;
            }

            public void setRate(int rate) {
                this.rate = rate;
            }
        }

        public static class HKDBean {
            /**
             * name : 港币
             * symbol : HK$
             * currency : HKD
             * cackeKey : cacheCurrencyHKD
             * rate : 1.138434
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class MOPBean {
            /**
             * name : 澳门币
             * symbol : MOP$
             * currency : MOP
             * cackeKey : cacheCurrencyMOP
             * rate : 1.170138
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class USDBean {
            /**
             * name : 美元
             * symbol : US$
             * currency : USD
             * cackeKey : cacheCurrencyUSD
             * rate : 0.144987
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }
    }

    public static class SpecAllInfoBean {
        /**
         * goods_promotion_price : 150.00
         * goods_price : 150
         * goods_marketprice : 160.00
         * goods_spec : 3m|250w
         * goods_id : 101007
         * goods_image : http://192.168.0.2/data/upload/shop/store/goods/1/1_05896460911893666_360.jpg
         * goods_state : 1
         * goods_storage : 100
         * goods_verify : 1
         * is_enquiry : 0
         */

        private String goods_promotion_price;
        private int goods_price;
        private String goods_marketprice;
        private String goods_spec;
        private String goods_id;
        private String goods_image;
        private String goods_state;
        private int goods_storage;
        private String goods_verify;
        private String is_enquiry;

        public String getGoods_promotion_price() {
            return goods_promotion_price;
        }

        public void setGoods_promotion_price(String goods_promotion_price) {
            this.goods_promotion_price = goods_promotion_price;
        }

        public int getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(int goods_price) {
            this.goods_price = goods_price;
        }

        public String getGoods_marketprice() {
            return goods_marketprice;
        }

        public void setGoods_marketprice(String goods_marketprice) {
            this.goods_marketprice = goods_marketprice;
        }

        public String getGoods_spec() {
            return goods_spec;
        }

        public void setGoods_spec(String goods_spec) {
            this.goods_spec = goods_spec;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_image() {
            return goods_image;
        }

        public void setGoods_image(String goods_image) {
            this.goods_image = goods_image;
        }

        public String getGoods_state() {
            return goods_state;
        }

        public void setGoods_state(String goods_state) {
            this.goods_state = goods_state;
        }

        public int getGoods_storage() {
            return goods_storage;
        }

        public void setGoods_storage(int goods_storage) {
            this.goods_storage = goods_storage;
        }

        public String getGoods_verify() {
            return goods_verify;
        }

        public void setGoods_verify(String goods_verify) {
            this.goods_verify = goods_verify;
        }

        public String getIs_enquiry() {
            return is_enquiry;
        }

        public void setIs_enquiry(String is_enquiry) {
            this.is_enquiry = is_enquiry;
        }
    }

    public static class SpecCheckedBean {
        /**
         * id : 32
         * name : 输送带宽
         * item : [{"id":212,"name":"3m"},{"id":213,"name":"6m"}]
         */

        private int id;
        private String name;
        private List<ItemBean> item;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<ItemBean> getItem() {
            return item;
        }

        public void setItem(List<ItemBean> item) {
            this.item = item;
        }

        public static class ItemBean {
            /**
             * id : 212
             * name : 3m
             */

            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }

    public static class BatchPriceBean {
        /**
         * mix : 1
         * value : 150
         * max : 10
         */

        private int mix;
        private int value;
        private int max;

        public int getMix() {
            return mix;
        }

        public void setMix(int mix) {
            this.mix = mix;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public int getMax() {
            return max;
        }

        public void setMax(int max) {
            this.max = max;
        }
    }
}
