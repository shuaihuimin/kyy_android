package com.xht.kuaiyouyi.ui.cart.order;

import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.cart.adapter.ConfirmOrderAdapter;
import com.xht.kuaiyouyi.ui.cart.adapter.GoodsListAdapter;
import com.xht.kuaiyouyi.ui.cart.entity.ConfirmOrderBean;
import com.xht.kuaiyouyi.ui.cart.entity.OrderDetilsBean;

import java.security.spec.MGF1ParameterSpec;

import butterknife.BindView;

public class CommodityListActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.listview)
    ListView listView;
    private OrderDetilsBean.GoodsBean.OrderListBean mgoodsbean;
    private ConfirmOrderBean.DataListBean.StoreListBean storeListBean;
    public static String FLAG="flag";
    @Override
    protected int getLayout() {
        return R.layout.activity_commoditylist;
    }

    @Override
    protected void initView() {
        tv_title.setText(getString(R.string.commodity_list));
        if(getIntent().getIntExtra(FLAG,0)==1){
            mgoodsbean= JSONObject.parseObject(getIntent().getStringExtra("goods_detils"),OrderDetilsBean.GoodsBean.OrderListBean.class);
            tv_num.setText(getString(R.string.gong)+mgoodsbean.getGoods_list().size()+getString(R.string.jian));
            GoodsListAdapter goodsListAdapter=new GoodsListAdapter(CommodityListActivity.this, mgoodsbean);
            listView.setAdapter(goodsListAdapter);
        }else if(getIntent().getIntExtra(FLAG,0)==2){
            storeListBean=JSONObject.parseObject(getIntent().getStringExtra("goods_detils"),ConfirmOrderBean.DataListBean.StoreListBean.class);
            tv_num.setText(getString(R.string.gong)+storeListBean.getCart_list().size()+getString(R.string.jian));
            storeListBean=JSONObject.parseObject(getIntent().getStringExtra("goods_detils"),ConfirmOrderBean.DataListBean.StoreListBean.class);
            ConfirmOrderAdapter confirmOrderAdapter=new ConfirmOrderAdapter(CommodityListActivity.this,storeListBean);
            listView.setAdapter(confirmOrderAdapter);
        }


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommodityListActivity.this.finish();
            }
        });
    }

}
