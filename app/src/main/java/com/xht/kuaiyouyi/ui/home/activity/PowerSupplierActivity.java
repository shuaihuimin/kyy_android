package com.xht.kuaiyouyi.ui.home.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;

import butterknife.BindView;

public class PowerSupplierActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @Override
    protected int getLayout() {
        return R.layout.activity_powersupplier;
    }

    @Override
    protected void initView() {
        tv_title.setText(getString(R.string.power_supplier)+getString(R.string.goods_supplier));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PowerSupplierActivity.this.finish();

            }
        });
    }
}
