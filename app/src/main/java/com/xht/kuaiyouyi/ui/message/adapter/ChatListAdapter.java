package com.xht.kuaiyouyi.ui.message.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.message.bean.MsgBean;
import com.xht.kuaiyouyi.ui.message.db.ChatListBean;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.BadgeView;

import java.util.List;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import q.rorbin.badgeview.QBadgeView;

public class ChatListAdapter extends BaseAdapter {
    private Context mContext;
    private List<ChatListBean.DataBean> mChatListBeans;

    public ChatListAdapter(Context mContext, List<ChatListBean.DataBean> chatListBeans) {
        this.mContext = mContext;
        this.mChatListBeans = chatListBeans;
    }

    @Override
    public int getCount() {
        if(mChatListBeans == null){
            return 0;
        }
        return mChatListBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return mChatListBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.item_message_chat_list, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_name = convertView.findViewById(R.id.tv_name);
            viewHolder.tv_message = convertView.findViewById(R.id.tv_message);
            viewHolder.tv_time = convertView.findViewById(R.id.tv_time);
            viewHolder.iv_avater = convertView.findViewById(R.id.iv_avater);
            viewHolder.view_top = convertView.findViewById(R.id.view_top);
            viewHolder.view_badgeview = convertView.findViewById(R.id.view_badgeview);
            viewHolder.badgeView = new BadgeView(mContext,viewHolder.view_badgeview,10,4);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.badgeView.setBadgeNumber(mChatListBeans.get(position).getNo_read_msg_num());
        viewHolder.tv_name.setText(mChatListBeans.get(position).getStore_name());
        if(mChatListBeans.get(position).getMsg_type()== MsgBean.MSG_TYPE_IMG){
            viewHolder.tv_message.setText(R.string.chat_list_img);
        }else if(mChatListBeans.get(position).getMsg_type()==MsgBean.MSG_TYPE_GOODS_INFO){
            viewHolder.tv_message.setText(R.string.chat_list_goods_info);
        }else if(mChatListBeans.get(position).getMsg_type()==MsgBean.MSG_TYPE_FILE){
            viewHolder.tv_message.setText(R.string.chat_list_file);
        }else {
            viewHolder.tv_message.setText(mChatListBeans.get(position).getT_msg());
        }
        viewHolder.tv_time.setText(Utils.getDisplayDataAndTime(mChatListBeans.get(position).getTime()));
        Glide.with(mContext).load(mChatListBeans.get(position).getStore_avatar())
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square).transform(new RoundedCornersTransformation(Utils.dp2px(mContext,10),Utils.dp2px(mContext,0.5f))))
                .into(viewHolder.iv_avater);
        if(position==0){
            viewHolder.view_top.setVisibility(View.VISIBLE);
        }else {
            viewHolder.view_top.setVisibility(View.GONE);
        }
        return convertView;
    }

    private class ViewHolder{
        private TextView tv_name,tv_message,tv_time;
        private ImageView iv_avater;
        private View view_top,view_badgeview;
        private QBadgeView badgeView;
    }

}
