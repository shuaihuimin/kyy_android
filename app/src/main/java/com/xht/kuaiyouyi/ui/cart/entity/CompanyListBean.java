package com.xht.kuaiyouyi.ui.cart.entity;

import java.util.List;

public class CompanyListBean {

    /**
     * num : 3
     * list : [{"company_id":"91","company_name":"联想 L"},{"company_id":"90","company_name":"人人网科技有限公司"},{"company_id":"77","company_name":"路易威登"},{"company_id":0,"company_name":"个人购"}]
     */

    private int num;
    private List<ListBean> list;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * company_id : 91
         * company_name : 联想 L
         */

        private String company_id;
        private String company_name;
        private boolean isSelect;

        public String getCompany_id() {
            return company_id;
        }

        public void setCompany_id(String company_id) {
            this.company_id = company_id;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public boolean isSelect() {
            return isSelect;
        }

        public void setSelect(boolean select) {
            isSelect = select;
        }
    }
}
