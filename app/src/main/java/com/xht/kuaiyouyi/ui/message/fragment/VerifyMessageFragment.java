package com.xht.kuaiyouyi.ui.message.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.message.adapter.VerifyMessageListAdapter;
import com.xht.kuaiyouyi.ui.message.bean.VerifyMessageListBean;
import com.xht.kuaiyouyi.ui.message.util.MessageUtil;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.widget.DeletePopupWindow;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;

/**
 * 验证消息    通过验证可以跳企业主页 ，其它不能跳转
 */
public class VerifyMessageFragment extends BaseFragment {
    @BindView(R.id.lv_content)
    ListView lv_content;
    @BindView(R.id.srl_loadmore)
    SmartRefreshLayout srl_loadmore;
    @BindView(R.id.rl_empty_message_list)
    RelativeLayout rl_empty_message_list;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;


    private String mKyy = "";
    private VerifyMessageListBean mVerifyMessageListBean;
    private VerifyMessageListAdapter mAdapter;

    private OnVerifyMessageReadNumUpdate mCallback;

    public static final String TO_COMPANY_INDEX = "to_compnay_index";

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mKyy = "";
        lv_content.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                VerifyMessageListBean.MessageListBean bean =
                        mVerifyMessageListBean.getMessage_list().get(position);
                if (bean.getStatus() == 0 && bean.getType() == 3 &&
                        bean.getMember_id().equals(Login.Companion.getInstance().getUid())) {
                    if(bean.getIs_read()==0){
                        MessageUtil.requestReadCompanyMessage(bean.getId());
                        mVerifyMessageListBean.getMessage_list().get(position).setIs_read(1);
                        mAdapter.notifyDataSetChanged();
                        mCallback.onVerifyMessageNumReadUpdate(1);
                    }
                    if (!TextUtils.isEmpty(bean.getCompany_id())) {
                        Login.Companion.getInstance().setEnterprise_company_id(bean.getCompany_id());
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(TO_COMPANY_INDEX, true);
                        goToActivity(MainActivity.class, bundle);
                    }
                }
            }
        });
        lv_content.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new DeletePopupWindow(getContext(), view).show().setOnClickDeleteListener(new DeletePopupWindow.OnClickDeleteListener() {
                    @Override
                    public void onDelete() {
                        DialogUtils.createTwoBtnDialog(getContext(),
                                getContext().getResources().getString(R.string.are_you_sure_delete_verify_message),
                                getContext().getResources().getString(R.string.dialog_confirm),
                                getContext().getResources().getString(R.string.dialog_cancel),
                                new DialogUtils.OnRightBtnListener() {
                                    @Override
                                    public void setOnRightListener(Dialog dialog) {
                                        requestDeleteMsg(position);
                                    }
                                },null,false,true);
                    }
                });
                return true;
            }
        });
        srl_loadmore.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                requestVerifyMessageList();
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestVerifyMessageList();
            }
        });
    }

    private void requestDeleteMsg(final int position) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_DELETE_WORK_VERIFY_MSG())
                .addParam("company_message_id", mVerifyMessageListBean.getMessage_list().get(position).getId())
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(getActivity()==null){
                            return;
                        }
                        DialogUtils.createTipImageAndTextDialog(getContext(),
                                getContext().getString(R.string.delete_succeful),
                                R.mipmap.png_icon_popup_ok);
                        mVerifyMessageListBean.getMessage_list().remove(position);
                        mAdapter.notifyDataSetChanged();
                        if(mVerifyMessageListBean.getMessage_list().size()==0){
                            rl_empty_message_list.setVisibility(View.VISIBLE);
                        }
                    }
                }, false);
    }

    private void requestVerifyMessageList() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_VERIFY_MESSAGE_LIST())
                .withLoadPOST(new NetCallBack<VerifyMessageListBean>() {
                    @NotNull
                    @Override
                    public Class<VerifyMessageListBean> getRealType() {
                        return VerifyMessageListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        if(TextUtils.isEmpty(mKyy)){
                            rl_empty_message_list.setVisibility(View.GONE);
                            error_retry_view.setVisibility(View.VISIBLE);
                        }else {
                            Toast.makeText(getContext(), getString(R.string.not_network), Toast.LENGTH_SHORT).show();
                        }
                        if (srl_loadmore.getState() == RefreshState.Loading) {
                            srl_loadmore.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull VerifyMessageListBean verifyMessageListBean) {
                        if(getActivity()==null){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        if (TextUtils.isEmpty(mKyy)) {
                            mKyy = verifyMessageListBean.getKyy();
                            if (verifyMessageListBean.getMessage_list() == null || verifyMessageListBean.getMessage_list().size() == 0) {
                                rl_empty_message_list.setVisibility(View.VISIBLE);
                                return;
                            }
                            srl_loadmore.setEnableLoadMore(true);
                            mVerifyMessageListBean = verifyMessageListBean;
                            mAdapter = new VerifyMessageListAdapter(getContext(), mVerifyMessageListBean,mCallback);
                            lv_content.setAdapter(mAdapter);
                        } else {
                            if (verifyMessageListBean.getMessage_list() != null && verifyMessageListBean.getMessage_list().size() > 0) {
                                mVerifyMessageListBean.getMessage_list().addAll(verifyMessageListBean.getMessage_list());
                                mAdapter.notifyDataSetChanged();
                                mKyy = verifyMessageListBean.getKyy();
                            } else {
                                srl_loadmore.setNoMoreData(true);
                            }
                            srl_loadmore.finishLoadMore();
                        }

                    }
                }, false, mKyy);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //第一次显示在用户面前的时候
        if (isVisibleToUser) {
            if (TextUtils.isEmpty(mKyy)) {
                requestVerifyMessageList();
            }
        }
        //隐藏在用户面前的时候
        if (!isVisibleToUser && !TextUtils.isEmpty(mKyy)) {
            if (mVerifyMessageListBean != null && mVerifyMessageListBean.getMessage_list() != null) {
                int mReadNum = 0;//已读数
                for (VerifyMessageListBean.MessageListBean bean : mVerifyMessageListBean.getMessage_list()) {
                    if((!((bean.getType()==1 || bean.getType()==2)&&bean.getStatus()==0) || bean.getType()==3)){
                        //不跳转的情况或者不是未处理的状态
                        if (bean.getIs_read() == 0) {
                            bean.setIs_read(1);
                            mReadNum++;
                        }
                    }
                }
                if (mReadNum > 0) {
                    mCallback.onVerifyMessageNumReadUpdate(mReadNum);
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_message;
    }

    /**
     * 获取小红点数，即时未读数
     *
     * @param messageListBeans 列表
     * @return
     */
    private int getUnreadNum(List<VerifyMessageListBean.MessageListBean> messageListBeans) {
        int i = 0;
        if (messageListBeans == null) {
            return 0;
        }
        for (VerifyMessageListBean.MessageListBean bean : messageListBeans) {
            if (bean.getIs_read() == 0) {
                i = i + 1;
            }
        }
        return i;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnVerifyMessageReadNumUpdate) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnVerifyMessageReadNumUpdate");
        }
    }

    public interface OnVerifyMessageReadNumUpdate {
        //已读数更新
        void onVerifyMessageNumReadUpdate(int num);
    }
}
