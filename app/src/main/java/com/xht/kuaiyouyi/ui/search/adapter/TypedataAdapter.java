package com.xht.kuaiyouyi.ui.search.adapter;

import android.support.v4.app.FragmentActivity;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.search.entity.TypeData;

import java.util.List;

/**
 * Created by shuaihuimin on 2018/6/20.
 */

public class TypedataAdapter extends BaseQuickAdapter<TypeData.ClassArrBean,BaseViewHolder> {
    private FragmentActivity activity;
    public TypedataAdapter(int layoutResId, List<TypeData.ClassArrBean> data, FragmentActivity activity) {
        super(layoutResId, data);
        this.activity = activity;
    }

    @Override
    protected void convert(BaseViewHolder helper, TypeData.ClassArrBean item) {
        if (item.isSelect() == true){
            helper.setText(R.id.item_name,item.getGc_name()).setTextColor(R.id.item_name,activity.getResources().getColor(R.color.blue_normal)).setVisible(R.id.v_state,true)
            .setBackgroundColor(R.id.item_name,activity.getResources().getColor(R.color._ffffff));
        }else {
            helper.setText(R.id.item_name, item.getGc_name()).setTextColor(R.id.item_name,activity.getResources().getColor(R.color.grey_4)).setVisible(R.id.v_state,false)
            .setBackgroundColor(R.id.item_name,activity.getResources().getColor(R.color.grey_bg));
        }
    }
}
