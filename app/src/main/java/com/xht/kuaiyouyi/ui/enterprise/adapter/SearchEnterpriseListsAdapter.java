package com.xht.kuaiyouyi.ui.enterprise.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.enterprise.activity.JoinEnterpriseActivity;
import com.xht.kuaiyouyi.ui.enterprise.bean.SearchCompanyBean;

import java.util.List;

public class SearchEnterpriseListsAdapter extends BaseAdapter {
    private Context mContext;
    private List<SearchCompanyBean.CompanyBean> mCompanyBeans;

    public SearchEnterpriseListsAdapter(Context mContext, List<SearchCompanyBean.CompanyBean> mCompanyBeans) {
        this.mContext = mContext;
        this.mCompanyBeans = mCompanyBeans;
    }

    @Override
    public int getCount() {
        return mCompanyBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return mCompanyBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.item_enterprise_search, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_enterprise_name = convertView.findViewById(R.id.tv_enterprise_name);
            viewHolder.bt_add = convertView.findViewById(R.id.bt_add);
            viewHolder.v_line = convertView.findViewById(R.id.v_line);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tv_enterprise_name.setText(mCompanyBeans.get(position).getCompany_name());
        if(mCompanyBeans.get(position).isIs_join()){
            viewHolder.bt_add.setText(R.string.enterprise_btn_added);
            viewHolder.bt_add.setEnabled(false);
        }else {
            viewHolder.bt_add.setText(R.string.enterprise_btn_add);
            viewHolder.bt_add.setEnabled(true);
        }

        viewHolder.bt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, JoinEnterpriseActivity.class);
                intent.putExtra(JoinEnterpriseActivity.COMPANY_NAME,mCompanyBeans.get(position).getCompany_name());
                intent.putExtra(JoinEnterpriseActivity.COMPANY_ID,mCompanyBeans.get(position).getCompany_id());
                intent.putExtra(JoinEnterpriseActivity.COMPANY_LOGO,mCompanyBeans.get(position).getCompany_logo());
                mContext.startActivity(intent);
            }
        });
        if(position==mCompanyBeans.size()-1){
            viewHolder.v_line.setVisibility(View.INVISIBLE);
        }else {
            viewHolder.v_line.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    private class ViewHolder{
        private TextView tv_enterprise_name;
        private Button bt_add;
        private View v_line;
    }
}
