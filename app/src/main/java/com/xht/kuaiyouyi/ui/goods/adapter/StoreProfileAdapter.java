package com.xht.kuaiyouyi.ui.goods.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xht.kuaiyouyi.ui.goods.entity.StoreProfileData;

import java.util.List;

public class StoreProfileAdapter extends BaseQuickAdapter<StoreProfileData,BaseViewHolder> {
    public StoreProfileAdapter(int layoutResId, @Nullable List<StoreProfileData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, StoreProfileData item) {

    }
}
