package com.xht.kuaiyouyi.ui.login.activity;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;

import butterknife.BindView;

/**
 * Created by shuaihuimin on 2018/6/25.
 */

public class RegisteredsufActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView titleview;
    @BindView(R.id.iv_back)
    ImageView imageback;
    @BindView(R.id.btn_toLogin)
    Button btn_toLogin;



    @Override
    protected int getLayout() {
        return R.layout.activity_registeedsuf;
    }

    @Override
    protected void initView() {
        titleview.setText(R.string.register);
        imageback.setVisibility(View.GONE);
        btn_toLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
}
