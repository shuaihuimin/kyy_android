package com.xht.kuaiyouyi.ui.cart.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.cart.entity.OrderApprovalBean;

import java.util.ArrayList;
import java.util.List;

public class ApprovalProcessDialog extends Dialog {
    private List<OrderApprovalBean.GroupsBean> mGroupsBeans;
    private Context mContext;
    private ImageView iv_close;
    private TextView tv_group;
    private View view_line_group;
    private TextView tv_approval_process;
    private View view_line_approval_process;
    private ListView lv_approval_process;
    private MyAdapter mAdapter;
    private List<String> mData;

    private Integer mSelectGroupPosition;
    private Integer mSelectChildPosition;
    private OnSelectPosition mListener;
    private boolean isSelectGroup = true;
    private boolean isSelectChild = false;


    public ApprovalProcessDialog(@NonNull Context context, List<OrderApprovalBean.GroupsBean> groupsBeans,OnSelectPosition listener) {
        super(context, R.style.Dialog_translucent);
        mGroupsBeans=new ArrayList<>();
        mGroupsBeans.addAll(groupsBeans);
        this.mContext = context;
        this.mListener = listener;
        init();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        View view = View.inflate(mContext, R.layout.dialog_select_approval_process, null);
        setContentView(view);//这行一定要写在前面
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);


        iv_close = view.findViewById(R.id.iv_close);
        tv_group = view.findViewById(R.id.tv_group);
        view_line_group = view.findViewById(R.id.view_line_group);
        tv_approval_process = view.findViewById(R.id.tv_approval_process);
        view_line_approval_process = view.findViewById(R.id.view_line_approval_process);
        lv_approval_process = view.findViewById(R.id.lv_approval_process);

        setGroupDatas();
        lv_approval_process.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isSelectGroup) {
                    tv_group.setEnabled(true);
                    tv_group.setText(mData.get(position));
                    tv_group.setTextColor(mContext.getResources().getColor(R.color.grey_4));
                    tv_approval_process.setTextColor(mContext.getResources().getColor(R.color.blue_normal));
                    view_line_group.setBackgroundResource(android.R.color.white);
                    view_line_approval_process.setBackgroundResource(R.color.blue_normal);
                    mSelectGroupPosition = position;
                    isSelectGroup = false;
                    isSelectChild = true;
                    setChildDatas(position);
                } else {
                    mSelectChildPosition = position;
                    tv_approval_process.setText(mData.get(position));
                    if(mListener!=null){
                        mListener.onSelectPosition(mSelectGroupPosition,mSelectChildPosition);
                    }
                    dismiss();
                }
            }
        });
        tv_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_group.setEnabled(false);
                tv_group.setTextColor(mContext.getResources().getColor(R.color.blue_normal));
                view_line_group.setBackgroundResource(R.color.blue_normal);
                tv_approval_process.setTextColor(mContext.getResources().getColor(R.color.grey_4));
                tv_approval_process.setText(R.string.approval_proccess);
                view_line_approval_process.setBackgroundResource(android.R.color.white);
                mSelectChildPosition=0;
                isSelectGroup = true;
                isSelectChild = false;
                setGroupDatas();
                lv_approval_process.setSelection(mSelectGroupPosition);
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void setChildDatas(int groupPosition) {
        mData = new ArrayList<>();
        for (OrderApprovalBean.GroupsBean.GroupApprovesBean bean : mGroupsBeans.get(groupPosition).getGroup_approves()) {
            mData.add(bean.getApprove_name());
        }
        mAdapter = new MyAdapter();
        lv_approval_process.setAdapter(mAdapter);
    }

    private void setGroupDatas(){
//        mSelectGroupPosition = null;
        mData = new ArrayList<>();
        for(OrderApprovalBean.GroupsBean bean:mGroupsBeans){
            mData.add(bean.getGroup_name());
        }
        mAdapter = new MyAdapter();
        lv_approval_process.setAdapter(new MyAdapter());
    }

    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.item_approval_process, null);
                holder = new ViewHolder();
                holder.tv_approval_process_name = convertView.findViewById(R.id.tv_approval_process_name);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if(isSelectGroup){
                if(mSelectGroupPosition!=null&&mSelectGroupPosition==position){
                    holder.tv_approval_process_name.setTextColor(mContext.getResources().getColor(R.color.blue_normal));
                }else {
                    holder.tv_approval_process_name.setTextColor(mContext.getResources().getColor(R.color.grey_4));
                }
            }
            holder.tv_approval_process_name.setText(mData.get(position));
            return convertView;
        }

        class ViewHolder {
            private TextView tv_approval_process_name;
        }
    }

    public interface OnSelectPosition{
        void onSelectPosition(int groupPosition,int childPosition);
    }

}
