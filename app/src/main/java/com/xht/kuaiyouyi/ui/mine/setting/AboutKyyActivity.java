package com.xht.kuaiyouyi.ui.mine.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xht.kuaiyouyi.BuildConfig;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.WebActivity;
import com.xht.kuaiyouyi.ui.base.BaseActivity;

import butterknife.BindView;

public class AboutKyyActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_version)
    TextView tv_version;
    @BindView(R.id.rl_function_introduction)
    RelativeLayout rl_function_introduction;
    @BindView(R.id.rl_platform_protocol)
    RelativeLayout rl_platform_protocol;

    @Override
    protected int getLayout() {
        return R.layout.activity_about_kyy;
    }

    @Override
    protected void initView() {
        tv_title.setText(R.string.about_kyy);
        tv_version.setText("v"+ BuildConfig.VERSION_NAME);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        rl_function_introduction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(WebActivity.TITLE,getString(R.string.function_introduction));
                bundle.putString(WebActivity.WEB_URL, BuildConfig.DOMAIN+"member/index.php?act=login&op=privacyPolicy");
                goToActivity(WebActivity.class,bundle);
            }
        });
        rl_platform_protocol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(WebActivity.TITLE,getString(R.string.protocol_title));
                bundle.putString(WebActivity.WEB_URL, BuildConfig.DOMAIN+"member/index.php?act=login&op=regAgreement");
                goToActivity(WebActivity.class,bundle);
            }
        });
    }

}
