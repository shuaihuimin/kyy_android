package com.xht.kuaiyouyi.ui.search.entity;

import java.util.ArrayList;

/**
 * Created by shuaihuimin on 2018/6/29.
 */

public class ScreenBean {
    private String id;
    private String name;
    private ArrayList<TagInfo> tagInfo;
    private String title;


    public ScreenBean(String id, String name, ArrayList<TagInfo> tagInfo) {
        this.id = id;
        this.name = name;
        this.tagInfo=tagInfo;
        title = "";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<TagInfo> getTagInfo() {
        return tagInfo;
    }

    public void setTagInfo(ArrayList<TagInfo> tagInfo) {
        this.tagInfo = tagInfo;
    }

    public static class TagInfo{
        private String name;
        private String id;
        private boolean select;
        public boolean isSelect() {
            return select;
        }

        public void setSelect(boolean select) {
            this.select = select;
        }
        public TagInfo(String id,String name){
            this.id=id;
            this.name=name;
        }
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
    }

    @Override
    public String toString() {
        return "ScreenBean{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", tagInfo=" + tagInfo +
                '}';
    }
}
