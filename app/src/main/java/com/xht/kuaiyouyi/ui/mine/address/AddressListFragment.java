package com.xht.kuaiyouyi.ui.mine.address;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.mine.adapter.CheckAddressListAdapter;
import com.xht.kuaiyouyi.ui.mine.adapter.ReceiveAddressListAdapter;
import com.xht.kuaiyouyi.ui.mine.collection.OnShowEmptyView;
import com.xht.kuaiyouyi.ui.mine.entity.CheckAddressListBean;
import com.xht.kuaiyouyi.ui.mine.entity.ReceiveAddressListBean;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class AddressListFragment extends BaseFragment {

    @BindView(R.id.lv_content)
    ListView lv_content;
    @BindView(R.id.bt_add_address)
    Button bt_add_address;
    @BindView(R.id.ll_empty)
    LinearLayout ll_empty;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;

    private int mType;
    public final static int REQUEST_CODE = 1000;
    public final static int FLAG=0;

    /**
     * @param type 收货地址或者取支票地址
     */
    public void setType(int type) {
        mType = type;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        bt_add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mType == AddressListActivity.TYPE_RECEIVE) {
                    Intent intent = new Intent(getContext(), NewAddressActivity.class);
                    intent.putExtra(Contant.TYPE, NewAddressActivity.TYPE_NEW_RECEIVE_ADDRESS);
                    startActivityForResult(intent, REQUEST_CODE);
                }
                if (mType == AddressListActivity.TYPE_CHECK) {
                    Intent intent = new Intent(getContext(), NewAddressActivity.class);
                    intent.putExtra(Contant.TYPE, NewAddressActivity.TYPE_NEW_CHECK_ADDRESS);
                    startActivityForResult(intent, REQUEST_CODE);
                }

            }
        });

        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestAddressList();
            }
        });
        requestAddressList();


    }

    private void requestAddressList() {
        if(mType == AddressListActivity.TYPE_RECEIVE){
            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ADDRESS_LIST())
                    .withPOST(new NetCallBack<ReceiveAddressListBean>() {
                        @NotNull
                        @Override
                        public Class<ReceiveAddressListBean> getRealType() {
                            return ReceiveAddressListBean.class;
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(getContext() != null){
                                //Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                                error_retry_view.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onSuccess(@NonNull ReceiveAddressListBean receiveAddressListBean) {
                            if(getActivity() == null){
                                return;
                            }
                            error_retry_view.setVisibility(View.GONE);
                            if(receiveAddressListBean.getAddress()!=null&&receiveAddressListBean.getAddress().size()==0){
                                ll_empty.setVisibility(View.VISIBLE);
                            }
                            if(receiveAddressListBean.getAddress()!=null&&receiveAddressListBean.getAddress().size()>0){
                                ll_empty.setVisibility(View.GONE);
                            }
                            ReceiveAddressListAdapter receiveAddressListAdapter = new ReceiveAddressListAdapter(getContext(), receiveAddressListBean,AddressListFragment.this, null,FLAG);
                            lv_content.setAdapter(receiveAddressListAdapter);
                            receiveAddressListAdapter.setShowEmptyListener(new OnShowEmptyView() {
                                @Override
                                public void onShowEmptyView() {
                                    ll_empty.setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    }, false);
        }
        if(mType == AddressListActivity.TYPE_CHECK){
            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CHECK_ADDRESS_LIST())
                    .withPOST(new NetCallBack<CheckAddressListBean>() {
                        @NotNull
                        @Override
                        public Class<CheckAddressListBean> getRealType() {
                            return CheckAddressListBean.class;
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(getActivity() == null){
                                return;
                            }
                            //Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                            error_retry_view.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onSuccess(@NonNull CheckAddressListBean checkAddressListBean) {
                            if(getActivity() == null){
                                return;
                            }
                            error_retry_view.setVisibility(View.GONE);
                            if(checkAddressListBean.getAddress()!=null&&checkAddressListBean.getAddress().size()==0){
                                ll_empty.setVisibility(View.VISIBLE);
                            }
                            if(checkAddressListBean.getAddress()!=null&&checkAddressListBean.getAddress().size()>0){
                                ll_empty.setVisibility(View.GONE);
                            }
                            CheckAddressListAdapter checkAddressListAdapter = new CheckAddressListAdapter(getContext(), checkAddressListBean,AddressListFragment.this,null,FLAG);
                            lv_content.setAdapter(checkAddressListAdapter);
                            checkAddressListAdapter.setShowEmptyListener(new OnShowEmptyView() {
                                @Override
                                public void onShowEmptyView() {
                                    ll_empty.setVisibility(View.VISIBLE);
                                }
                            });

                        }
                    }, false);
        }

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_address_list;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            requestAddressList();
        }
    }
}
