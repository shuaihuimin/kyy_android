package com.xht.kuaiyouyi.ui.message.bean;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class MsgBean {
    public static final int MSG_TYPE_TEXT = 0;
    public static final int MSG_TYPE_IMG = 1;
    public static final int MSG_TYPE_GOODS_INFO = 2;
    public static final int MSG_TYPE_FILE = 3;

    @IntDef({MSG_TYPE_TEXT, MSG_TYPE_IMG,MSG_TYPE_GOODS_INFO,MSG_TYPE_FILE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface MsgType {}


    private long f_id;
    private String f_ip;
    private String f_name;
    private long goods_id;
    private long store_id;
    private long t_id;
    private String t_msg;
    private String t_name;
    private int msg_type;
    private String add_time;
    private long m_id;
    private PicSizeBean picSize;
    private UserBean user;
    private StoreBean store_info;
    private GoodsInfoBean goods_info;
    private FileBean file;

    public long getF_id() {
        return f_id;
    }

    public void setF_id(long f_id) {
        this.f_id = f_id;
    }

    public String getF_ip() {
        return f_ip;
    }

    public void setF_ip(String f_ip) {
        this.f_ip = f_ip;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public long getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(long goods_id) {
        this.goods_id = goods_id;
    }

    public long getStore_id() {
        return store_id;
    }

    public void setStore_id(long store_id) {
        this.store_id = store_id;
    }

    public long getT_id() {
        return t_id;
    }

    public void setT_id(long t_id) {
        this.t_id = t_id;
    }

    public String getT_msg() {
        return t_msg;
    }

    public void setT_msg(String t_msg) {
        this.t_msg = t_msg;
    }

    public String getT_name() {
        return t_name;
    }

    public void setT_name(String t_name) {
        this.t_name = t_name;
    }

    public int getMsg_type() {
        return msg_type;
    }

    public void setMsg_type(int msg_type) {
        this.msg_type = msg_type;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public long getM_id() {
        return m_id;
    }

    public void setM_id(long m_id) {
        this.m_id = m_id;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public StoreBean getStore_info() {
        return store_info;
    }

    public void setStore_info(StoreBean store_info) {
        this.store_info = store_info;
    }

    public GoodsInfoBean getGoods_info() {
        return goods_info;
    }

    public void setGoods_info(GoodsInfoBean goods_info) {
        this.goods_info = goods_info;
    }

    public FileBean getFile() {
        return file;
    }

    public void setFile(FileBean file) {
        this.file = file;
    }

    public static class UserBean {
        private long u_id;
        private String u_name;
        private String avatar;

        public long getU_id() {
            return u_id;
        }

        public void setU_id(long u_id) {
            this.u_id = u_id;
        }

        public String getU_name() {
            return u_name;
        }

        public void setU_name(String u_name) {
            this.u_name = u_name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }

    public static class StoreBean {
        private long store_id;
        private String store_name;
        private String store_avatar;

        public long getStore_id() {
            return store_id;
        }

        public void setStore_id(long store_id) {
            this.store_id = store_id;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }

        public String getStore_avatar() {
            return store_avatar;
        }

        public void setStore_avatar(String store_avatar) {
            this.store_avatar = store_avatar;
        }
    }

    public static class GoodsInfoBean {
        private long goods_id;
        private long store_id;
        private String goods_commonid;
        private String goods_name;
        private String goods_image;
        private String goods_price;
        private String goods_marketprice;
        private String goods_promotion_price;
        private String brand_id;
        private int is_enquiry;
        private String url;
        private String pic;
        private String pic24;
        private String pic36;

        public long getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(long goods_id) {
            this.goods_id = goods_id;
        }

        public long getStore_id() {
            return store_id;
        }

        public void setStore_id(long store_id) {
            this.store_id = store_id;
        }

        public String getGoods_commonid() {
            return goods_commonid;
        }

        public void setGoods_commonid(String goods_commonid) {
            this.goods_commonid = goods_commonid;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_image() {
            return goods_image;
        }

        public void setGoods_image(String goods_image) {
            this.goods_image = goods_image;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getGoods_marketprice() {
            return goods_marketprice;
        }

        public void setGoods_marketprice(String goods_marketprice) {
            this.goods_marketprice = goods_marketprice;
        }

        public String getGoods_promotion_price() {
            return goods_promotion_price;
        }

        public void setGoods_promotion_price(String goods_promotion_price) {
            this.goods_promotion_price = goods_promotion_price;
        }

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public int getIs_enquiry() {
            return is_enquiry;
        }

        public void setIs_enquiry(int is_enquiry) {
            this.is_enquiry = is_enquiry;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getPic24() {
            return pic24;
        }

        public void setPic24(String pic24) {
            this.pic24 = pic24;
        }

        public String getPic36() {
            return pic36;
        }

        public void setPic36(String pic36) {
            this.pic36 = pic36;
        }
    }

    public static class PicSizeBean{
        //图片需要的字段
        private int width;
        private int height;

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }
    }

    public static class FileBean{

        /**
         * file_name : 2.mp4
         * file_size : 1744303
         * file_type : mp4
         * file_url : #
         */

        private String file_name;
        private long file_size;
        private String file_type;
        private String file_url;

        public String getFile_name() {
            return file_name;
        }

        public void setFile_name(String file_name) {
            this.file_name = file_name;
        }

        public long getFile_size() {
            return file_size;
        }

        public void setFile_size(long file_size) {
            this.file_size = file_size;
        }

        public String getFile_type() {
            return file_type;
        }

        public void setFile_type(String file_type) {
            this.file_type = file_type;
        }

        public String getFile_url() {
            return file_url;
        }

        public void setFile_url(String file_url) {
            this.file_url = file_url;
        }
    }

    public PicSizeBean getPicSize() {
        return picSize;
    }

    public void setPicSize(PicSizeBean picSize) {
        this.picSize = picSize;
    }
}
