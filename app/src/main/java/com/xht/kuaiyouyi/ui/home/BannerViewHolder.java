package com.xht.kuaiyouyi.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.zhouwei.mzbanner.holder.MZViewHolder;

/**
 * Created by shuaihuimin on 2018/6/14.
 */

public class BannerViewHolder implements MZViewHolder<String> {
    private ImageView mImageView;
    @Override
    public View createView(Context context) {
        // 返回页面布局
        View view = LayoutInflater.from(context).inflate(R.layout.banner_item,null);
        mImageView = (ImageView) view.findViewById(R.id.banner_image);
        return view;
    }

    @Override
    public void onBind(Context context, int position, String data) {
        // 数据绑定
        //mImageView.setImageResource(data);
        Glide.with(context).load(data)
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_rectangle)).into(mImageView);
    }
}

