package com.xht.kuaiyouyi.ui.login.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.login.entity.SafetyVerifyBean;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/**
 * Created by shuaihuimin on 2018/6/22.
 * 安全认证
 */

public class StartVerifyActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_start_verify)
    TextView tv_start_verify;


    private String mPhone;
    private String mAreaCode;

    @Override
    protected int getLayout() {
        return R.layout.activity_start_verify;
    }

    @Override
    protected void initView() {
        tv_title.setText(R.string.verify_identity);
        mPhone = getIntent().getStringExtra(Contant.PHONE);
        mAreaCode = getIntent().getStringExtra(Contant.AREA_CODE);
        tv_start_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(mPhone) && !TextUtils.isEmpty(mAreaCode)){
                    requestVerifyDevice();
                }
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void requestVerifyDevice() {
        DialogUtils.createTipAllLoadDialog(StartVerifyActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_SAFETY_VERIFY())
                .addParam("member_mobile", mPhone)
                .addParam("mobile_zone", mAreaCode)
                .withPOST(new NetCallBack<SafetyVerifyBean>() {
                    @NotNull
                    @Override
                    public Class<SafetyVerifyBean> getRealType() {
                        return SafetyVerifyBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(StartVerifyActivity.this,err,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull SafetyVerifyBean safetyVerifyBean) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(StartVerifyActivity.this,safetyVerifyBean.getErr_msg(),
                                Toast.LENGTH_SHORT).show();
                        Bundle bundle = getIntent().getExtras();
                        bundle.putString(Contant.TYPE,Contant.TYPE_CHANGE_DEVICE);
                        goToActivity(InequipmentActivity.class,bundle);
                        finish();
                    }
                }, false);
    }


}
