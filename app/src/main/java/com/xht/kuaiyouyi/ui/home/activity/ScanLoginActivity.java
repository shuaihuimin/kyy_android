package com.xht.kuaiyouyi.ui.home.activity;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class ScanLoginActivity extends BaseActivity{
    @BindView(R.id.tv_scan_login_ok)
    TextView tv_scan_login_ok;
    @BindView(R.id.tv_scan_login_nor)
    TextView tv_scan_login_nor;
    @BindView(R.id.tv_scan_close)
    TextView tv_scan_close;
    @Override
    protected int getLayout() {
        return R.layout.activity_scanlogin;
    }

    @Override
    protected void initView() {
        final String qr_time=getIntent().getStringExtra("qr_time");
        final String qr_uuid=getIntent().getStringExtra("qruuid");
        tv_scan_login_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saomaLogin(qr_uuid,qr_time);
            }
        });
        tv_scan_login_nor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_scan_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void saomaLogin(String qruuid,String qr_time){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_SWEEP_YARD_LOGIN())
                .addParam("qruuid",qruuid)
                .addParam("qr_time",qr_time)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.createTipImageAndTextDialog(ScanLoginActivity.this,getString(R.string.not_network),R.mipmap.png_icon_popup_wrong);
                    }

                    @Override
                    public void onSuccess(@NonNull String string) {
                        if(isFinishing()){
                            return;
                        }
//                        DialogUtils.createTipImageAndTextDialog(ScanLoginActivity.this,getString(R.string.login_su),R.mipmap.png_icon_popup_ok);
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.SCAN));
                        ScanLoginActivity.this.finish();
                    }
                },false);

    }
}
