package com.xht.kuaiyouyi.ui.mine.adapter;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.cart.order.OrderDetilsActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.StoreActivity;
import com.xht.kuaiyouyi.ui.mine.collection.OnShowEmptyView;
import com.xht.kuaiyouyi.ui.mine.entity.OrderListBean;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.text.MessageFormat;

public class OrderListAdapter extends BaseAdapter {
    private Context mContext;
    private OrderListBean mOrderListBean;
    private OnShowEmptyView mShowEmptyListener;

    //一定要从0开始
    private final static int TYPE_ONE_FULL = 0;
    private final static int TYPE_ONE_CREDIT = 1;
    private final static int TYPE_MORE_FULL = 2;
    private final static int TYPE_MORE_CREDIT = 3;

    private final String PAY_TIME_LIMIT_ONLINE = "3";
    private final String PAY_TIME_LIMIT_OTHER = "7";

    private boolean is_all_order;//是否是全部订单tab

    public OrderListAdapter(Context context, OrderListBean orderListBean,boolean is_all_order) {
        this.mContext = context;
        this.mOrderListBean = orderListBean;
        this.is_all_order = is_all_order;
    }

    @Override
    public int getCount() {
        if (mOrderListBean.getOrder_main_list() == null) {
            return 0;
        }
        return mOrderListBean.getOrder_main_list().size();
    }

    @Override
    public Object getItem(int position) {
        return mOrderListBean.getOrder_main_list().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OneFullHolder oneFullHolder = null;
        OneCreditHolder oneCreditHolder = null;
        MoreFullHolder moreFullHolder = null;
        MoreCreditHolder moreCreditHolder = null;
        switch (getItemViewType(position)) {
            case TYPE_ONE_FULL:
                if (convertView == null) {
                    convertView = View.inflate(mContext, R.layout.item_order_list_one_full, null);
                    oneFullHolder = new OneFullHolder(convertView);
                    convertView.setTag(oneFullHolder);
                } else {
                    oneFullHolder = (OneFullHolder) convertView.getTag();
                }
                oneFullHolder.setData(position);
                break;
            case TYPE_ONE_CREDIT:
                if (convertView == null) {
                    convertView = View.inflate(mContext, R.layout.item_order_list_one_credit, null);
                    oneCreditHolder = new OneCreditHolder(convertView);
                    convertView.setTag(oneCreditHolder);
                } else {
                    oneCreditHolder = (OneCreditHolder) convertView.getTag();
                }
                oneCreditHolder.setData(position);
                break;
            case TYPE_MORE_FULL:
                if (convertView == null) {
                    convertView = View.inflate(mContext, R.layout.item_order_list_more_full, null);
                    moreFullHolder = new MoreFullHolder(convertView);
                    convertView.setTag(moreFullHolder);
                } else {
                    moreFullHolder = (MoreFullHolder) convertView.getTag();
                }
                moreFullHolder.setData(position);
                break;
            case TYPE_MORE_CREDIT:
                if (convertView == null) {
                    convertView = View.inflate(mContext, R.layout.item_order_list_more_credit, null);
                    moreCreditHolder = new MoreCreditHolder(convertView);
                    convertView.setTag(moreCreditHolder);
                } else {
                    moreCreditHolder = (MoreCreditHolder) convertView.getTag();
                }
                moreCreditHolder.setData(position);
                break;

        }
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        if (mOrderListBean.getOrder_main_list().get(position).getOrder_goods_arr().size() == 1) {
            if (mOrderListBean.getOrder_main_list().get(position).getOrder_main_deal_active() == 1) {
                return TYPE_ONE_FULL;
            }
            if (mOrderListBean.getOrder_main_list().get(position).getOrder_main_deal_active() == 2) {
                return TYPE_ONE_CREDIT;
            }
        }

        if (mOrderListBean.getOrder_main_list().get(position).getOrder_goods_arr().size() > 1) {
            if (mOrderListBean.getOrder_main_list().get(position).getOrder_main_deal_active() == 1) {
                return TYPE_MORE_FULL;
            }
            if (mOrderListBean.getOrder_main_list().get(position).getOrder_main_deal_active() == 2) {
                return TYPE_MORE_CREDIT;
            }
        }
        return 8;
    }

    private class OneFullHolder {
        private ImageView iv_icon, iv_goods_img;
        private TextView tv_order_no, tv_status, tv_store_name, tv_goods_name, tv_goods_price,
                tv_goods_num, tv_goods_model, tv_num, tv_order_total;
        private Button bt_cancel_order, bt_pay, bt_look;

        public OneFullHolder(View itemView) {
//            bt_buy_again = itemView.findViewById(R.id.bt_buy_again);
            bt_cancel_order = itemView.findViewById(R.id.bt_cancel_order);
            bt_look = itemView.findViewById(R.id.bt_look);
            bt_pay = itemView.findViewById(R.id.bt_pay);
            iv_goods_img = itemView.findViewById(R.id.iv_goods_img);
            iv_icon = itemView.findViewById(R.id.iv_icon);
            tv_goods_model = itemView.findViewById(R.id.tv_goods_model);
            tv_goods_name = itemView.findViewById(R.id.tv_goods_name);
            tv_goods_num = itemView.findViewById(R.id.tv_goods_num);
            tv_goods_price = itemView.findViewById(R.id.tv_goods_price);
            tv_num = itemView.findViewById(R.id.tv_num);
            tv_order_no = itemView.findViewById(R.id.tv_order_no);
            tv_order_total = itemView.findViewById(R.id.tv_order_total);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_store_name = itemView.findViewById(R.id.tv_store_name);
        }

        private void setData(int position) {
            final OrderListBean.OrderMainListBean bean = mOrderListBean.getOrder_main_list().get(position);
            if (bean.getOrder_main_company_id() == 0) {
                iv_icon.setImageResource(R.mipmap.order_icon_personal_nor);
            } else {
                iv_icon.setImageResource(R.mipmap.order_icon_enterprise_nor);
            }
            tv_order_no.setText(bean.getOrder_main_sn());
            tv_status.setText(bean.getOrder_main_state_name());
            tv_store_name.setText(bean.getStore_name());
            tv_store_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, StoreActivity.class);
                    intent.putExtra("store_id", bean.getStore_id());
                    mContext.startActivity(intent);
                }
            });
            Glide.with(mContext).load(bean.getOrder_goods_arr().get(0).getGoods_image())
                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                    .into(iv_goods_img);
            if(bean.getOrder_goods_arr().get(0).getIs_second_hand()==1){
                tv_goods_name.setText(Utils.secondLabel(bean.getOrder_goods_arr().get(0).getGoods_name()));
            }else {
                tv_goods_name.setText(bean.getOrder_goods_arr().get(0).getGoods_name());
            }
            tv_goods_model.setText(bean.getOrder_goods_arr().get(0).getGoods_spec());
            tv_goods_price.setText("￥" + Utils.getDisplayMoney(bean.getOrder_goods_arr().get(0).getGoods_price()));
            tv_goods_num.setText("x" + bean.getOrder_goods_arr().get(0).getGoods_num());
            tv_num.setText(MessageFormat.format(mContext.getResources().getString(R.string.total_goods_num), bean.getOrder_main_count() + ""));
            tv_order_total.setText("￥" + Utils.getDisplayMoney(bean.getOrder_main_amount()));
            setBtnState(position,bean.isIs_pay_order(),bean.isIs_cancel_order(), bt_pay, bt_cancel_order, bt_look);
        }
    }

    private class OneCreditHolder {
        private ImageView iv_icon, iv_goods_img;
        private TextView tv_order_no, tv_status, tv_store_name, tv_goods_name, tv_goods_price,
                tv_goods_num, tv_goods_model, tv_num, tv_order_total, tv_second_pay, tv_first_pay,
                tv_first_pay_tip,tv_second_pay_tip;
        private Button bt_pay_first, bt_pay_second, bt_cancel_order, bt_look;

        public OneCreditHolder(View itemView) {
//            bt_buy_again = itemView.findViewById(R.id.bt_buy_again);
            bt_cancel_order = itemView.findViewById(R.id.bt_cancel_order);
            bt_look = itemView.findViewById(R.id.bt_look);
            iv_goods_img = itemView.findViewById(R.id.iv_goods_img);
            iv_icon = itemView.findViewById(R.id.iv_icon);
            tv_goods_model = itemView.findViewById(R.id.tv_goods_model);
            tv_goods_name = itemView.findViewById(R.id.tv_goods_name);
            tv_goods_num = itemView.findViewById(R.id.tv_goods_num);
            tv_goods_price = itemView.findViewById(R.id.tv_goods_price);
            tv_num = itemView.findViewById(R.id.tv_num);
            tv_order_no = itemView.findViewById(R.id.tv_order_no);
            tv_order_total = itemView.findViewById(R.id.tv_order_total);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_store_name = itemView.findViewById(R.id.tv_store_name);
            bt_pay_first = itemView.findViewById(R.id.bt_pay_first);
            bt_pay_second = itemView.findViewById(R.id.bt_pay_second);
//            tv_overdue_second = itemView.findViewById(R.id.tv_overdue_second);
            tv_first_pay = itemView.findViewById(R.id.tv_first_pay);
            tv_second_pay = itemView.findViewById(R.id.tv_second_pay);
            tv_first_pay_tip = itemView.findViewById(R.id.tv_first_pay_tip);
            tv_second_pay_tip = itemView.findViewById(R.id.tv_second_pay_tip);
        }

        private void setData(int position) {
            final OrderListBean.OrderMainListBean bean = mOrderListBean.getOrder_main_list().get(position);
            if (bean.getOrder_main_company_id() == 0) {
                iv_icon.setImageResource(R.mipmap.order_icon_personal_nor);
            } else {
                iv_icon.setImageResource(R.mipmap.order_icon_enterprise_nor);
            }
            tv_order_no.setText(bean.getOrder_main_sn());
            tv_status.setText(bean.getOrder_main_state_name());
            tv_store_name.setText(bean.getStore_name());
            tv_store_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, StoreActivity.class);
                    intent.putExtra("store_id", bean.getStore_id());
                    mContext.startActivity(intent);
                }
            });
            Glide.with(mContext).load(bean.getOrder_goods_arr().get(0).getGoods_image())
                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                    .into(iv_goods_img);

            if(bean.getOrder_goods_arr().get(0).getIs_second_hand()==1){
                tv_goods_name.setText(Utils.secondLabel(bean.getOrder_goods_arr().get(0).getGoods_name()));
            }else {
                tv_goods_name.setText(bean.getOrder_goods_arr().get(0).getGoods_name());
            }
            tv_goods_model.setText(bean.getOrder_goods_arr().get(0).getGoods_spec());
            tv_goods_price.setText("￥" + Utils.getDisplayMoney(bean.getOrder_goods_arr().get(0).getGoods_price()));
            tv_goods_num.setText("x" + bean.getOrder_goods_arr().get(0).getGoods_num());
            tv_num.setText(MessageFormat.format(mContext.getResources().getString(R.string.total_goods_num), bean.getOrder_main_count() + ""));
            tv_order_total.setText("￥" + Utils.getDisplayMoney(bean.getOrder_main_amount()));
            tv_first_pay.setText("￥" + Utils.getDisplayMoney(bean.getOrder_main_pay_one()));
            tv_second_pay.setText("￥" + Utils.getDisplayMoney(bean.getOrder_main_pay_two()));
            if(bean.getOrder_main_pay_active()==3){
                //在线支付
                tv_first_pay_tip.setText(MessageFormat.format(mContext.getResources().getString(R.string.first_stage_tip),PAY_TIME_LIMIT_ONLINE));
                tv_second_pay_tip.setText(MessageFormat.format(mContext.getResources().getString(R.string.second_stage_tip),PAY_TIME_LIMIT_ONLINE));
            }else {
                //支票支付和银行转账
                tv_first_pay_tip.setText(MessageFormat.format(mContext.getResources().getString(R.string.first_stage_tip),PAY_TIME_LIMIT_OTHER));
                tv_second_pay_tip.setText(MessageFormat.format(mContext.getResources().getString(R.string.second_stage_tip),PAY_TIME_LIMIT_OTHER));
            }
            setCreditPayBtnState(position,bt_pay_first,bt_pay_second);

//            if (bean.getOrder_main_second_pay_end() == 0) {
//                tv_overdue_second.setVisibility(View.VISIBLE);
//            } else {
//                tv_overdue_second.setVisibility(View.GONE);
//            }
            setBtnState(position,bean.isIs_pay_order(),bean.isIs_cancel_order(),null, bt_cancel_order, bt_look);
        }
    }

    private class MoreFullHolder {
        private ImageView iv_icon, iv_next;
        private TextView tv_order_no, tv_status, tv_store_name, tv_order_total, tv_num;
        private Button bt_cancel_order, bt_pay, bt_look;
        private LinearLayout ll_goods;

        public MoreFullHolder(View itemView) {
//            bt_buy_again = itemView.findViewById(R.id.bt_buy_again);
            bt_cancel_order = itemView.findViewById(R.id.bt_cancel_order);
            bt_look = itemView.findViewById(R.id.bt_look);
            bt_pay = itemView.findViewById(R.id.bt_pay);
            iv_icon = itemView.findViewById(R.id.iv_icon);
            iv_next = itemView.findViewById(R.id.iv_next);
            ll_goods = itemView.findViewById(R.id.ll_goods);
            tv_order_no = itemView.findViewById(R.id.tv_order_no);
            tv_order_total = itemView.findViewById(R.id.tv_order_total);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_store_name = itemView.findViewById(R.id.tv_store_name);
            tv_num = itemView.findViewById(R.id.tv_num);
        }

        private void setData(final int position) {
            final OrderListBean.OrderMainListBean bean = mOrderListBean.getOrder_main_list().get(position);
            if (bean.getOrder_main_company_id() == 0) {
                iv_icon.setImageResource(R.mipmap.order_icon_personal_nor);
            } else {
                iv_icon.setImageResource(R.mipmap.order_icon_enterprise_nor);
            }
            tv_order_no.setText(bean.getOrder_main_sn());
            tv_status.setText(bean.getOrder_main_state_name());
            tv_store_name.setText(bean.getStore_name());
            if (TextUtils.isEmpty(bean.getStore_id())) {
                iv_next.setVisibility(View.GONE);
            } else {
                iv_next.setVisibility(View.VISIBLE);
                tv_store_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, StoreActivity.class);
                        intent.putExtra("store_id", bean.getStore_id());
                        mContext.startActivity(intent);
                    }
                });
            }

            tv_num.setText(MessageFormat.format(mContext.getResources().getString(R.string.total_goods_num), bean.getOrder_main_count() + ""));
            ll_goods.removeAllViews();
            for (OrderListBean.OrderMainListBean.OrderGoodsArrBean goods : bean.getOrder_goods_arr()) {
                View imgView = View.inflate(mContext, R.layout.item_order_list_goos_img, null);
                ImageView iv_goods_img = imgView.findViewById(R.id.iv_goods_img);
                Glide.with(mContext).load(goods.getGoods_image())
                        .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                        .into(iv_goods_img);
                ll_goods.addView(imgView);
            }
            tv_order_total.setText("￥" + Utils.getDisplayMoney(bean.getOrder_main_amount()));
            setBtnState(position,bean.isIs_pay_order(),bean.isIs_cancel_order(), bt_pay, bt_cancel_order, bt_look);
            ll_goods.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext,OrderDetilsActivity.class);
                    intent.putExtra("order_main_id"
                            ,mOrderListBean.getOrder_main_list().get(position).getOrder_main_id());
                    mContext.startActivity(intent);
                }
            });
        }
    }

    private class MoreCreditHolder {
        private ImageView iv_icon, iv_next;
        private TextView tv_order_no, tv_status, tv_store_name, tv_order_total, tv_num,
                tv_second_pay, tv_first_pay,tv_first_pay_tip,tv_second_pay_tip;
        private Button bt_pay_first, bt_pay_second, bt_cancel_order, bt_look;
        private LinearLayout ll_goods;

        public MoreCreditHolder(View itemView) {
//            bt_buy_again = itemView.findViewById(R.id.bt_buy_again);
            bt_cancel_order = itemView.findViewById(R.id.bt_cancel_order);
            bt_look = itemView.findViewById(R.id.bt_look);
            iv_icon = itemView.findViewById(R.id.iv_icon);
            iv_next = itemView.findViewById(R.id.iv_next);
            ll_goods = itemView.findViewById(R.id.ll_goods);
            tv_order_no = itemView.findViewById(R.id.tv_order_no);
            tv_order_total = itemView.findViewById(R.id.tv_order_total);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_num = itemView.findViewById(R.id.tv_num);
            tv_store_name = itemView.findViewById(R.id.tv_store_name);
            bt_pay_first = itemView.findViewById(R.id.bt_pay_first);
            bt_pay_second = itemView.findViewById(R.id.bt_pay_second);
//            tv_overdue_second = itemView.findViewById(R.id.tv_overdue_second);
            tv_first_pay = itemView.findViewById(R.id.tv_first_pay);
            tv_second_pay = itemView.findViewById(R.id.tv_second_pay);
            tv_first_pay_tip = itemView.findViewById(R.id.tv_first_pay_tip);
            tv_second_pay_tip = itemView.findViewById(R.id.tv_second_pay_tip);
        }

        private void setData(final int position) {
            final OrderListBean.OrderMainListBean bean = mOrderListBean.getOrder_main_list().get(position);
            if (bean.getOrder_main_company_id() == 0) {
                iv_icon.setImageResource(R.mipmap.order_icon_personal_nor);
            } else {
                iv_icon.setImageResource(R.mipmap.order_icon_enterprise_nor);
            }
            tv_order_no.setText(bean.getOrder_main_sn());
            tv_status.setText(bean.getOrder_main_state_name());
            tv_store_name.setText(bean.getStore_name());
            if (TextUtils.isEmpty(bean.getStore_id())) {
                iv_next.setVisibility(View.GONE);
            } else {
                iv_next.setVisibility(View.VISIBLE);
                tv_store_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, StoreActivity.class);
                        intent.putExtra("store_id", bean.getStore_id());
                        mContext.startActivity(intent);
                    }
                });
            }
            tv_num.setText(MessageFormat.format(mContext.getResources().getString(R.string.total_goods_num), bean.getOrder_main_count() + ""));
            ll_goods.removeAllViews();
            for (OrderListBean.OrderMainListBean.OrderGoodsArrBean goods : bean.getOrder_goods_arr()) {
                View imgView = View.inflate(mContext, R.layout.item_order_list_goos_img, null);
                ImageView iv_goods_img = imgView.findViewById(R.id.iv_goods_img);
                Glide.with(mContext).load(goods.getGoods_image())
                        .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                        .into(iv_goods_img);
                ll_goods.addView(imgView);
            }
            ll_goods.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext,OrderDetilsActivity.class);
                    intent.putExtra("order_main_id"
                            ,mOrderListBean.getOrder_main_list().get(position).getOrder_main_id());
                    mContext.startActivity(intent);
                }
            });
            tv_order_total.setText("￥" + Utils.getDisplayMoney(bean.getOrder_main_amount()));
            tv_first_pay.setText("￥" + Utils.getDisplayMoney(bean.getOrder_main_pay_one()));
            tv_second_pay.setText("￥" + Utils.getDisplayMoney(bean.getOrder_main_pay_two()));
            if(bean.getOrder_main_pay_active()==3){
                //在线支付
                tv_first_pay_tip.setText(MessageFormat.format(mContext.getResources().getString(R.string.first_stage_tip),PAY_TIME_LIMIT_ONLINE));
                tv_second_pay_tip.setText(MessageFormat.format(mContext.getResources().getString(R.string.second_stage_tip),PAY_TIME_LIMIT_ONLINE));
            }else {
                //支票支付和银行转账
                tv_first_pay_tip.setText(MessageFormat.format(mContext.getResources().getString(R.string.first_stage_tip),PAY_TIME_LIMIT_OTHER));
                tv_second_pay_tip.setText(MessageFormat.format(mContext.getResources().getString(R.string.second_stage_tip),PAY_TIME_LIMIT_OTHER));
            }
            setCreditPayBtnState(position,bt_pay_first,bt_pay_second);

//            if (bean.getOrder_main_second_pay_end() == 0) {
//                tv_overdue_second.setVisibility(View.VISIBLE);
//            } else {
//                tv_overdue_second.setVisibility(View.GONE);
//            }

            setBtnState(position,bean.isIs_pay_order(),bean.isIs_cancel_order(),null, bt_cancel_order, bt_look);
        }
    }

    private void requestCancelOrder(final int position) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ORDER_CANCEL())
                .addParam("order_main_id",mOrderListBean.getOrder_main_list().get(position).getOrder_main_id())
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        Toast.makeText(mContext,err,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        DialogUtils.createTipImageAndTextDialog(mContext,
                                mContext.getString(R.string.order_cancel_succeful),
                                R.mipmap.png_icon_popup_ok);
                        if(!is_all_order){
                            mOrderListBean.getOrder_main_list().remove(position);
                            notifyDataSetChanged();
                            if(mOrderListBean.getOrder_main_list().size()==0){
                                if(mShowEmptyListener!=null){
                                    mShowEmptyListener.onShowEmptyView();
                                }
                            }
                        }
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_ORDER_LIST));
                    }
                },false);
    }

    private void setBtnState(final int position, boolean is_pay_order,boolean is_cancel_order, Button bt_pay, Button bt_cancel_order, Button bt_look) {
        if(is_cancel_order){
            bt_cancel_order.setVisibility(View.VISIBLE);
        }else {
            bt_cancel_order.setVisibility(View.GONE);
        }
        bt_cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.createTwoBtnDialog(mContext,
                        mContext.getResources().getString(R.string.are_you_sure_cancel_order),
                        mContext.getResources().getString(R.string.dialog_confirm),
                        mContext.getResources().getString(R.string.dialog_cancel),
                        new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(Dialog dialog) {
                                requestCancelOrder(position);
                            }
                        },null,false,true);
            }
        });

        if (bt_pay != null) {
            //有付款权限并且是待付款的状态
            if(is_pay_order&&mOrderListBean.getOrder_main_list().get(position).getOrder_main_state()==1){
                bt_pay.setVisibility(View.VISIBLE);
                bt_pay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, OrderDetilsActivity.class);
                        intent.putExtra("order_main_id"
                                ,mOrderListBean.getOrder_main_list().get(position).getOrder_main_id());
                        mContext.startActivity(intent);
                    }
                });
            }else {
                bt_pay.setVisibility(View.GONE);
            }
        }


        if(bt_cancel_order.getVisibility()!=View.VISIBLE&&(bt_pay==null||bt_pay.getVisibility()!=View.VISIBLE)){
            bt_look.setVisibility(View.VISIBLE);
            bt_look.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, OrderDetilsActivity.class);
                    intent.putExtra("order_main_id"
                            ,mOrderListBean.getOrder_main_list().get(position).getOrder_main_id());
                    mContext.startActivity(intent);
                }
            });
        }else {
            bt_look.setVisibility(View.GONE);
        }


//        bt_buy_again.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(mContext,"确认订单页面",Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    private void setCreditPayBtnState(final int position, Button bt_pay_first, Button bt_pay_second) {
        int first_pay_status = mOrderListBean.getOrder_main_list().get(position).getOrder_main_deal_one_state();
        int second_pay_status = mOrderListBean.getOrder_main_list().get(position).getOrder_main_deal_two_state();
        if(first_pay_status == 1){
            bt_pay_first.setText(R.string.enterprise_wait_pay);
            bt_pay_first.setEnabled(false);
        }else if (first_pay_status == 2) {
            bt_pay_first.setText(R.string.payed);
            bt_pay_first.setEnabled(false);
        } else if (first_pay_status == 3) {
            bt_pay_first.setText(R.string.canceled);
            bt_pay_first.setEnabled(false);
        }else if(first_pay_status == 4){
            bt_pay_first.setText(R.string.approvaling);
            bt_pay_first.setEnabled(false);
        }

        if(second_pay_status == 1){
            bt_pay_second.setText(R.string.enterprise_wait_pay);
            bt_pay_second.setEnabled(false);
        }else if (second_pay_status == 2) {
            bt_pay_second.setText(R.string.payed);
            bt_pay_second.setEnabled(false);
        } else if (second_pay_status == 3) {
            bt_pay_second.setText(R.string.canceled);
            bt_pay_second.setEnabled(false);
        }else if(second_pay_status == 4){
            bt_pay_second.setText(R.string.approvaling);
            bt_pay_second.setEnabled(false);
        }


        //有付款权限的时候
        if(mOrderListBean.getOrder_main_list().get(position).isIs_pay_order()){
            if (first_pay_status == 1) {
                bt_pay_first.setText(R.string.payment);
                bt_pay_first.setEnabled(true);
            } else {
                if (second_pay_status == 1) {
                    bt_pay_second.setText(R.string.payment);
                    bt_pay_second.setEnabled(true);
                }
            }

            bt_pay_first.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, OrderDetilsActivity.class);
                    intent.putExtra("order_main_id"
                            ,mOrderListBean.getOrder_main_list().get(position).getOrder_main_id());
                    mContext.startActivity(intent);
                }
            });
            bt_pay_second.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, OrderDetilsActivity.class);
                    intent.putExtra("order_main_id"
                            ,mOrderListBean.getOrder_main_list().get(position).getOrder_main_id());
                    mContext.startActivity(intent);
                }
            });
        }
    }

    public void setShowEmptyListener(OnShowEmptyView listener){
        mShowEmptyListener = listener;
    }
}
