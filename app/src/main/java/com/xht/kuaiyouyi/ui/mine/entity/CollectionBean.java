package com.xht.kuaiyouyi.ui.mine.entity;

import com.xht.kuaiyouyi.ui.cart.entity.CurrencytypeBean;

import java.util.List;

public class CollectionBean {

    /**
     * favorites_list : [{"goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627707766698_360.png","goods_name":"劳力士Rolex 深海系列 自动机械钢带男士表 联保正品116660 98210","log_price":"87500.00","log_msg":"87500.00","log_id":"274","goods_id":"100001","state":"1","is_enquiry":0},{"goods_image":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803176481834773_360.jpg","goods_name":"螺丝刀","log_price":"10.00","log_msg":"10.00","log_id":"334","goods_id":"100472","state":"1","is_enquiry":0},{"goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05797181694049592_360.png","goods_name":"桅柱式amwp9.511-2100","log_price":"500.00","log_msg":"500.00","log_id":"335","goods_id":"100382","state":"1","is_enquiry":0},{"goods_image":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803134834338328_360.jpg","goods_name":"222","log_price":"2222.00","log_msg":"2222.00","log_id":"336","goods_id":"100469","state":"1","is_enquiry":0},{"goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05804779414920592_360.jpg","goods_name":"普通输送 SSD350-5 350 38 1.5 182 5000*460*350","log_price":"555.65","log_msg":"555.65","log_id":"337","goods_id":"100487","state":"0","is_enquiry":0}]
     * kyy : eyJQIjoxLCJLIjoia3l5XzAxMiJ9
     */

    private String kyy;
    private List<FavoritesListBean> favorites_list;
    private CurrencytypeBean currencytype;

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public List<FavoritesListBean> getFavorites_list() {
        return favorites_list;
    }

    public void setFavorites_list(List<FavoritesListBean> favorites_list) {
        this.favorites_list = favorites_list;
    }

    public static class FavoritesListBean {
        /**
         * goods_image : http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627707766698_360.png
         * goods_name : 劳力士Rolex 深海系列 自动机械钢带男士表 联保正品116660 98210
         * log_price : 87500.00
         * log_msg : 87500.00
         * log_id : 274
         * goods_id : 100001
         * state : 1
         * is_enquiry : 0
         */

        private String goods_image;
        private String goods_name;
        private double log_price;
        private String log_msg;
        private String log_id;
        private String goods_id;
        private String state;
        private int is_enquiry;
        private int is_spec_pre;
        private String brand_name;
        private String is_second_hand;


        public String getIs_second_hand() {
            return is_second_hand;
        }

        public void setIs_second_hand(String is_second_hand) {
            this.is_second_hand = is_second_hand;
        }

        public String getGoods_image() {
            return goods_image;
        }

        public void setGoods_image(String goods_image) {
            this.goods_image = goods_image;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public double getLog_price() {
            return log_price;
        }

        public void setLog_price(double log_price) {
            this.log_price = log_price;
        }

        public String getLog_msg() {
            return log_msg;
        }

        public void setLog_msg(String log_msg) {
            this.log_msg = log_msg;
        }

        public String getLog_id() {
            return log_id;
        }

        public void setLog_id(String log_id) {
            this.log_id = log_id;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public int getIs_enquiry() {
            return is_enquiry;
        }

        public void setIs_enquiry(int is_enquiry) {
            this.is_enquiry = is_enquiry;
        }

        public int getIs_spec_pre() {
            return is_spec_pre;
        }

        public void setIs_spec_pre(int is_spec_pre) {
            this.is_spec_pre = is_spec_pre;
        }

        public String getBrand_name() {
            return brand_name;
        }

        public void setBrand_name(String brand_name) {
            this.brand_name = brand_name;
        }
    }

    public CurrencytypeBean getCurrencytype() {
        return currencytype;
    }

    public void setCurrencytype(CurrencytypeBean currencytype) {
        this.currencytype = currencytype;
    }
}
