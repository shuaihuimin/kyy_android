package com.xht.kuaiyouyi.ui.mine;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.ui.mine.address.AddressListActivity;
import com.xht.kuaiyouyi.ui.mine.collection.CollectionActivity;
import com.xht.kuaiyouyi.ui.mine.entity.MinenumBean;
import com.xht.kuaiyouyi.ui.mine.order.MineOrderActivity;
import com.xht.kuaiyouyi.ui.mine.order.OrderListfragment;
import com.xht.kuaiyouyi.ui.mine.setting.CurrencyActivity;
import com.xht.kuaiyouyi.ui.mine.setting.LanguageActivity;
import com.xht.kuaiyouyi.ui.mine.setting.SettingActivity;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.widget.BadgeView;
import com.xht.kuaiyouyi.widget.RoundImageView;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import q.rorbin.badgeview.QBadgeView;

/**
 * Created by shuaihuimin on 2018/6/13.
 */

public class Fragment_mine extends BaseFragment {
    @BindView(R.id.srl_refresh)
    SmartRefreshLayout srl_refresh;
    @BindView(R.id.iv_message)
    ImageView iv_message;
    @BindView(R.id.civ_avater)
    RoundImageView civ_avater;
    @BindView(R.id.tv_user_name)
    TextView tv_user_name;
    @BindView(R.id.tv_enterprise_member)
    TextView tv_enterprise_member;
    @BindView(R.id.ll_goods_collect)
    LinearLayout ll_goods_collect;
    @BindView(R.id.tv_goods_collect)
    TextView tv_goods_collect;
    @BindView(R.id.ll_store_collect)
    LinearLayout ll_store_collect;
    @BindView(R.id.tv_store_collect)
    TextView tv_store_collect;
    @BindView(R.id.ll_my_collect)
    LinearLayout ll_my_collect;
    @BindView(R.id.rl_wait_payment)
    RelativeLayout rl_wait_payment;
    @BindView(R.id.rl_wait_send)
    RelativeLayout rl_wait_send;
    @BindView(R.id.rl_wait_receive)
    RelativeLayout rl_wait_receive;
    @BindView(R.id.ll_all_order)
    LinearLayout ll_all_order;
    @BindView(R.id.ll_address_manager)
    LinearLayout ll_address_manager;
    @BindView(R.id.ll_settings_language)
    LinearLayout ll_settings_language;
    @BindView(R.id.ll_settings_currency)
    LinearLayout ll_settings_currency;
    @BindView(R.id.ll_sevices_help)
    LinearLayout ll_sevices_help;
    @BindView(R.id.iv_wait_pay)
    ImageView iv_wait_pay;
    @BindView(R.id.iv_wait_send)
    ImageView iv_wait_send;
    @BindView(R.id.iv_wait_receive)
    ImageView iv_wait_receive;

    private QBadgeView mBadgeViewWaitPay;
    private QBadgeView mBadgeViewWaitSend;
    private QBadgeView mBadgeViewWaitReceive;

    private QBadgeView mBadgeViewMessage;

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        srl_refresh.setEnableLoadMore(false);
        DialogUtils.createTipAllLoadDialog(getActivity());
        click();
        setUpBadgeView();
    }

    private void setUpBadgeView() {
        mBadgeViewWaitPay = new QBadgeView(getContext());
        mBadgeViewWaitPay.bindTarget(iv_wait_pay);
        mBadgeViewWaitPay.setBadgeTextSize(12, true);
        mBadgeViewWaitPay.setBadgeBackgroundColor(getResources().getColor(R.color.orange_1));
        mBadgeViewWaitPay.setBadgePadding(4, true);
        mBadgeViewWaitPay.setBadgeNumber(0);
        mBadgeViewWaitPay.setShowShadow(false);

        mBadgeViewWaitSend = new QBadgeView(getContext());
        mBadgeViewWaitSend.bindTarget(iv_wait_send);
        mBadgeViewWaitSend.setBadgeTextSize(12, true);
        mBadgeViewWaitSend.setBadgeBackgroundColor(getResources().getColor(R.color.orange_1));
        mBadgeViewWaitSend.setBadgePadding(4, true);
        mBadgeViewWaitSend.setBadgeNumber(0);
        mBadgeViewWaitSend.setShowShadow(false);

        mBadgeViewWaitReceive = new QBadgeView(getContext());
        mBadgeViewWaitReceive.bindTarget(iv_wait_receive);
        mBadgeViewWaitReceive.setBadgeTextSize(12, true);
        mBadgeViewWaitReceive.setBadgeBackgroundColor(getResources().getColor(R.color.orange_1));
        mBadgeViewWaitReceive.setBadgePadding(4, true);
        mBadgeViewWaitReceive.setBadgeNumber(0);
        mBadgeViewWaitReceive.setShowShadow(false);

        mBadgeViewMessage = new BadgeView(getContext(),iv_message,10,4);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mine_layout;
    }

    private void getMineIndexInfo() {
        if (Login.Companion.getInstance().isLogin()) {
            showUnreadMsgNum();
            tv_user_name.setText(Login.Companion.getInstance().getUsername());
            Glide.with(this).load(Login.Companion.getInstance().getMemberAvatar()).into(civ_avater);
            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_MINE_INDEX())
                    .addParam("type", "3")
                    .withPOST(new NetCallBack<MinenumBean>() {

                        @NotNull
                        @Override
                        public Class<MinenumBean> getRealType() {
                            return MinenumBean.class;
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(getActivity()==null){
                                return;
                            }
                            DialogUtils.moven();
                            if (srl_refresh.getState() == RefreshState.Refreshing) {
                                srl_refresh.finishRefresh();
                                Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onSuccess(@NonNull MinenumBean minenumBean) {
                            if(getActivity()==null){
                                return;
                            }
                            DialogUtils.moven();
                            if (minenumBean.getMember_info() != null) {
                                Login.Companion.getInstance().setUsername(minenumBean.getMember_info().getName());
                                Login.Companion.getInstance().setMemberAvatar(minenumBean.getMember_info().getAvatar());
                                Login.Companion.getInstance().setPhone(minenumBean.getMember_info().getPhone());
                                tv_user_name.setText(minenumBean.getMember_info().getName());
                                if (getActivity() != null) {
                                    Glide.with(getActivity())
                                            .load(Login.Companion.getInstance().getMemberAvatar())
                                            .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                                            .into(civ_avater);
                                }
                            }

                            if (minenumBean.getNum_info() != null) {
                                tv_goods_collect.setText(minenumBean.getNum_info().getFav_goods_num() + "");
                                tv_store_collect.setText(minenumBean.getNum_info().getFav_store_num() + "");
                                int wait_pay_num = minenumBean.getNum_info().getWait_pay_num();
                                int wait_send_num = minenumBean.getNum_info().getWait_deliver_num();
                                int wait_receive_num = minenumBean.getNum_info().getWait_receive_num();
                                mBadgeViewWaitPay.setBadgeNumber(wait_pay_num);
                                mBadgeViewWaitSend.setBadgeNumber(wait_send_num);
                                mBadgeViewWaitReceive.setBadgeNumber(wait_receive_num);
                            }

                            if(minenumBean.isIs_cmember()){
                                tv_enterprise_member.setVisibility(View.VISIBLE);
                            }else {
                                tv_enterprise_member.setVisibility(View.GONE);
                            }

                            if (srl_refresh.getState() == RefreshState.Refreshing) {
                                srl_refresh.finishRefresh();
                            }

                        }
                    }, false);

        }
    }

    private void click() {
        ll_goods_collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Contant.TYPE, CollectionActivity.TAB_GOODS_INDEX);
                goToActivity(CollectionActivity.class, bundle);
            }
        });
        ll_store_collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Contant.TYPE, CollectionActivity.TAB_STORE_INDEX);
                goToActivity(CollectionActivity.class, bundle);
            }
        });
        ll_my_collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Contant.TYPE, CollectionActivity.TAB_GOODS_INDEX);
                goToActivity(CollectionActivity.class, bundle);
            }
        });
        rl_wait_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Contant.TYPE, MineOrderActivity.TAB_WAIT_PAY_ORDER);
                bundle.putInt(MineOrderActivity.ORDER_TYPE, OrderListfragment.TYPE_MINE_ORDER);
                goToActivity(MineOrderActivity.class, bundle);
            }
        });

        rl_wait_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Contant.TYPE, MineOrderActivity.TAB_WAIT_SEND_ORDER);
                bundle.putInt(MineOrderActivity.ORDER_TYPE, OrderListfragment.TYPE_MINE_ORDER);
                goToActivity(MineOrderActivity.class, bundle);
            }
        });

        rl_wait_receive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Contant.TYPE, MineOrderActivity.TAB_WAIT_RECEIVE_ORDER);
                bundle.putInt(MineOrderActivity.ORDER_TYPE, OrderListfragment.TYPE_MINE_ORDER);
                goToActivity(MineOrderActivity.class, bundle);
            }
        });
        ll_all_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Contant.TYPE, MineOrderActivity.TAB_ALL_ORDER);
                bundle.putInt(MineOrderActivity.ORDER_TYPE, OrderListfragment.TYPE_MINE_ORDER);
                goToActivity(MineOrderActivity.class, bundle);
            }
        });
        ll_address_manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(AddressListActivity.class);
            }
        });
        ll_settings_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(LanguageActivity.class);
            }
        });
        ll_settings_currency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(CurrencyActivity.class);
            }
        });
        ll_sevices_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                goToActivity(ServiceHelpActivity.class);
                goToActivity(SettingActivity.class);
            }
        });
        iv_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(MessageActivity.class);
            }
        });
        srl_refresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                getMineIndexInfo();
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            getMineIndexInfo();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isHidden()){
            getMineIndexInfo();
        }
    }

    @Override
    public void updateUnreadMsg() {
        showUnreadMsgNum();
    }

    private void showUnreadMsgNum() {
        if(Login.Companion.getInstance().isLogin()){
            mBadgeViewMessage.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
        }
    }

}
