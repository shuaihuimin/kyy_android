package com.xht.kuaiyouyi.ui.home.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.home.db.LatelyDBBean;

import java.util.List;

public class SearchLatelyWorldAdapter extends BaseAdapter {
    private Context mContext;
    private List<LatelyDBBean> mLatelyDBBeans;

    public SearchLatelyWorldAdapter(Context mContext, List<LatelyDBBean> latelyDBBeans) {
        this.mContext = mContext;
        this.mLatelyDBBeans = latelyDBBeans;
    }

    @Override
    public int getCount() {
        if(mLatelyDBBeans == null){
            return 0;
        }
        return mLatelyDBBeans.size();
    }

    @Override
    public Object getItem(int position) {
        if(mLatelyDBBeans!=null & mLatelyDBBeans.size()>0){
            return mLatelyDBBeans.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.item_search_lately_word, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_lately_world = convertView.findViewById(R.id.tv_lately_world);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tv_lately_world.setText(mLatelyDBBeans.get(position).getHot_world());
        return convertView;
    }

    private class ViewHolder{
        private TextView tv_lately_world;
    }
}
