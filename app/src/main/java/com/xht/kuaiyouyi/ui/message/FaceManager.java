package com.xht.kuaiyouyi.ui.message;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.TypedValue;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.message.adapter.FaceAdapter;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author by Jaiky, Email jaikydota@163.com, Date on 10/12/2014.
 * PS: Not easy to write code, please indicate.
 */
public class FaceManager {

    private static FaceManager ourInstance;

    public static FaceManager getInstance() {
        if (ourInstance == null)
            ourInstance = new FaceManager();
        return ourInstance;
    }

    private SparseIntArray faceMap = null;//imgTag,resId
    private SparseArray<String> faceStr;//imgTag,source
    private Map<String, Integer> faceInt;//imgTag,resId

    public int getFace(int faceNum) {
        return faceMap.get(faceNum);
    }

    private FaceManager() {
        faceMap = new SparseIntArray();
        faceStr = new SparseArray<>();
        faceInt = new HashMap<>();

        faceMap.put(0, R.mipmap.face_smile);
        faceStr.append(0, ":smile:");
        faceInt.put(":smile:", R.mipmap.face_smile);

        faceMap.put(1, R.mipmap.face_sad);
        faceStr.append(1, ":sad:");
        faceInt.put(":sad:", R.mipmap.face_sad);

        faceMap.put(2, R.mipmap.face_biggrin);
        faceStr.append(2, ":biggrin:");
        faceInt.put(":biggrin:", R.mipmap.face_biggrin);

        faceMap.put(3, R.mipmap.face_cry);
        faceStr.append(3, ":cry:");
        faceInt.put(":cry:", R.mipmap.face_cry);

        faceMap.put(4, R.mipmap.face_huffy);
        faceStr.append(4, ":huffy:");
        faceInt.put(":huffy:", R.mipmap.face_huffy);

        faceMap.put(5, R.mipmap.face_shocked);
        faceStr.append(5, ":shocked:");
        faceInt.put(":shocked:", R.mipmap.face_shocked);

        faceMap.put(6, R.mipmap.face_tongue);
        faceStr.append(6, ":tongue:");
        faceInt.put(":tongue:", R.mipmap.face_tongue);

        faceMap.put(7, R.mipmap.face_cogitate);
        faceStr.append(7, ":cogitate:");
        faceInt.put(":cogitate:", R.mipmap.face_cogitate);

        faceMap.put(8, R.mipmap.face_titter);
        faceStr.append(8, ":titter:");
        faceInt.put(":titter:", R.mipmap.face_titter);

        faceMap.put(9, R.mipmap.face_sweat);
        faceStr.append(9, ":sweat:");
        faceInt.put(":sweat:", R.mipmap.face_sweat);

        faceMap.put(10, R.mipmap.face_ok);
        faceStr.append(10, ":ok:");
        faceInt.put(":ok:", R.mipmap.face_ok);

        faceMap.put(11, R.mipmap.face_lol);
        faceStr.append(11, ":lol:");
        faceInt.put(":lol:", R.mipmap.face_lol);

        faceMap.put(12, R.mipmap.face_loveliness);
        faceStr.append(12, ":loveliness:");
        faceInt.put(":loveliness:", R.mipmap.face_loveliness);

        faceMap.put(13, R.mipmap.face_laugh);
        faceStr.append(13, ":laugh:");
        faceInt.put(":laugh:", R.mipmap.face_laugh);

        faceMap.put(14, R.mipmap.face_untitled);
        faceStr.append(14, ":untitled:");
        faceInt.put(":untitled:", R.mipmap.face_untitled);

        faceMap.put(15, R.mipmap.face_dizzy);
        faceStr.append(15, ":dizzy:");
        faceInt.put(":dizzy:", R.mipmap.face_dizzy);

        faceMap.put(16, R.mipmap.face_shutup);
        faceStr.append(16, ":shutup:");
        faceInt.put(":shutup:", R.mipmap.face_shutup);

        faceMap.put(17, R.mipmap.face_sleepy);
        faceStr.append(17, ":sleepy:");
        faceInt.put(":sleepy:", R.mipmap.face_sleepy);

        faceMap.put(18, R.mipmap.face_victory);
        faceStr.append(18, ":victory:");
        faceInt.put(":victory:", R.mipmap.face_victory);

        faceMap.put(19, R.mipmap.face_handshake);
        faceStr.append(19, ":handshake:");
        faceInt.put(":handshake:", R.mipmap.face_handshake);

        faceMap.put(20, R.mipmap.face_grimacing);
        faceStr.append(20, ":grimacing:");
        faceInt.put(":grimacing:", R.mipmap.face_grimacing);

        faceMap.put(21, R.mipmap.face_like);
        faceStr.append(21, ":like:");
        faceInt.put(":like:", R.mipmap.face_like);

        faceMap.put(22, R.mipmap.face_plead);
        faceStr.append(22, ":plead:");
        faceInt.put(":plead:", R.mipmap.face_plead);

        faceMap.put(23, R.mipmap.face_supercilious);
        faceStr.append(23, ":supercilious:");
        faceInt.put(":supercilious:", R.mipmap.face_supercilious);
    }

    public int getEmojiCount() {
        return faceMap.size();
    }

    public int getPageCount() {
        if (getEmojiCount() % FaceAdapter.FACE_COUNT == 0) {
            return getEmojiCount() / FaceAdapter.FACE_COUNT;
        }
        return getEmojiCount() / FaceAdapter.FACE_COUNT + 1;
    }

    public SpannableString getEmotionContent(final Context context, String source) {
        SpannableString spannableString = new SpannableString(source);
        Resources res = context.getResources();

        String regexEmotion = ":[a-zA-Z]+:";
        Pattern patternEmotion = Pattern.compile(regexEmotion);
        Matcher matcherEmotion = patternEmotion.matcher(spannableString);

        while (matcherEmotion.find()) {
            // 获取匹配到的具体字符
            String key = matcherEmotion.group();
            // 匹配字符串的开始位置
            int start = matcherEmotion.start();
            // 利用表情名字获取到对应的图片
            Integer imgRes = getImgByName(key);
            if (imgRes != null) {
                Bitmap bitmap = BitmapFactory.decodeResource(res, imgRes);
                Drawable drawable=new BitmapDrawable(context.getResources(),bitmap);
                int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 17,
                        context.getResources().getDisplayMetrics());
                drawable.setBounds(0, 0, size, size);
                ImageSpan span = new ImageSpan(drawable);
                spannableString.setSpan(span, start, start + key.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        return spannableString;
    }

    public Integer getImgByName(String key) {
        return faceInt.get(key);
    }

    public String getSourceByImgTag(int imgTag) {
        return faceStr.get(imgTag);
    }

}
