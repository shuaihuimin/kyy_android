package com.xht.kuaiyouyi.ui;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.utils.Utils;

import butterknife.BindView;

public class WebActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.wv_content)
    WebView wv_content;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;
    private boolean islode=true;
    @Override
    protected int getLayout() {
        return R.layout.activity_web;
    }

    private String mTitle;
    private String mWebUrl;

    public static final String TITLE="title";
    public static final String WEB_URL="web_url";


    @Override
    protected void initView() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mTitle = getIntent().getStringExtra(TITLE);
        mWebUrl = getIntent().getStringExtra(WEB_URL);

        tv_title.setText(mTitle);
        WebSettings webSettings = wv_content.getSettings();
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setJavaScriptEnabled(true);

        wv_content.loadUrl(mWebUrl);

        wv_content.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                islode=false;
                Log.i("info","------zzzz"+errorCode);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.i("info","------zzzz2"+islode);
                if(islode){
                    error_retry_view.setVisibility(View.GONE);
                }else {
                    error_retry_view.setVisibility(View.VISIBLE);
                }
            }
        });

        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                islode=true;
                wv_content.loadUrl(mWebUrl);
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (wv_content!= null) {
            wv_content.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            wv_content.clearHistory();
            ((ViewGroup) wv_content.getParent()).removeView(wv_content);
            wv_content.destroy();
            wv_content= null;
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if(wv_content.canGoBack()){
            wv_content.goBack();
            return;
        }
        super.onBackPressed();
    }
}
