package com.xht.kuaiyouyi.ui.home.entity;

import java.util.List;

public class SecondMarkerketBean {

    /**
     * goods_list : [{"goods_id":"101024","goods_name":"机械姬","goods_image":"http://127.0.0.1/data/upload/shop/common/default_goods_image_1280.gif","goods_price":"100000.00","goods_salenum":"2","store_id":"1","is_enquiry":"0","goods_state":"1","is_second_hand":"1","store_name":"快优易自营"},{"goods_id":"101025","goods_name":"自行走曲臂式高空作業平臺(柴油驅動)","goods_image":"http://127.0.0.1/data/upload/shop/common/default_goods_image_1280.gif","goods_price":"120000.00","goods_salenum":"0","store_id":"1","is_enquiry":"0","goods_state":"1","is_second_hand":"1","store_name":"快优易自营"},{"goods_id":"101026","goods_name":"二手星星","goods_image":"http://127.0.0.1/data/upload/shop/common/default_goods_image_1280.gif","goods_price":"10000.00","goods_salenum":"1","store_id":"1","is_enquiry":"0","goods_state":"1","is_second_hand":"1","store_name":"快优易自营"},{"goods_id":"101027","goods_name":"二手un","goods_image":"http://127.0.0.1/data/upload/shop/common/default_goods_image_1280.gif","goods_price":"100.00","goods_salenum":"1","store_id":"1","is_enquiry":"0","goods_state":"1","is_second_hand":"1","store_name":"快优易自营"},{"goods_id":"101023","goods_name":"二手设备","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05924016071497333_1280.jpg","goods_price":"2000.00","goods_salenum":"0","store_id":"1","is_enquiry":"0","goods_state":"1","is_second_hand":"1","store_name":"快优易自营"},{"goods_id":"101028","goods_name":"规格在这里","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05930890712732525_1280.jpg","goods_price":"168.00","goods_salenum":"0","store_id":"1","is_enquiry":"1","goods_state":"1","is_second_hand":"1","store_name":"快优易自营"},{"goods_id":"101031","goods_name":"螺丝刀","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05931695878298226_1280.jpg","goods_price":"10.00","goods_salenum":"0","store_id":"1","is_enquiry":"0","goods_state":"1","is_second_hand":"1","store_name":"快优易自营"},{"goods_id":"101034","goods_name":"21313","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05937991363605228_1280.jpg","goods_price":"12.00","goods_salenum":"0","store_id":"1","is_enquiry":"0","goods_state":"1","is_second_hand":"1","store_name":"快优易自营"},{"goods_id":"101035","goods_name":"1231","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05937991862518866_1280.jpg","goods_price":"80.00","goods_salenum":"0","store_id":"1","is_enquiry":"0","goods_state":"1","is_second_hand":"1","store_name":"快优易自营"},{"goods_id":"101036","goods_name":"123","goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05937992440740873_1280.jpg","goods_price":"80.00","goods_salenum":"0","store_id":"1","is_enquiry":"0","goods_state":"1","is_second_hand":"1","store_name":"快优易自营"}]
     * kyy : eyJQIjoxLCJLIjoia3l5XzAyOCJ9
     * currencytype : {"CNY":{"name":"人民币","symbol":"￥","currency":"CNY","cackeKey":"","rate":1},"HKD":{"name":"港币","symbol":"HK$","currency":"HKD","cackeKey":"cacheCurrencyHKD","rate":1.138434},"MOP":{"name":"澳门币","symbol":"MOP$","currency":"MOP","cackeKey":"cacheCurrencyMOP","rate":1.170138},"USD":{"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.144987}}
     */

    private String kyy;
    private CurrencytypeBean currencytype;
    private List<GoodsListBean> goods_list;

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public CurrencytypeBean getCurrencytype() {
        return currencytype;
    }

    public void setCurrencytype(CurrencytypeBean currencytype) {
        this.currencytype = currencytype;
    }

    public List<GoodsListBean> getGoods_list() {
        return goods_list;
    }

    public void setGoods_list(List<GoodsListBean> goods_list) {
        this.goods_list = goods_list;
    }

    public static class CurrencytypeBean {
        /**
         * CNY : {"name":"人民币","symbol":"￥","currency":"CNY","cackeKey":"","rate":1}
         * HKD : {"name":"港币","symbol":"HK$","currency":"HKD","cackeKey":"cacheCurrencyHKD","rate":1.138434}
         * MOP : {"name":"澳门币","symbol":"MOP$","currency":"MOP","cackeKey":"cacheCurrencyMOP","rate":1.170138}
         * USD : {"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.144987}
         */

        private CNYBean CNY;
        private HKDBean HKD;
        private MOPBean MOP;
        private USDBean USD;

        public CNYBean getCNY() {
            return CNY;
        }

        public void setCNY(CNYBean CNY) {
            this.CNY = CNY;
        }

        public HKDBean getHKD() {
            return HKD;
        }

        public void setHKD(HKDBean HKD) {
            this.HKD = HKD;
        }

        public MOPBean getMOP() {
            return MOP;
        }

        public void setMOP(MOPBean MOP) {
            this.MOP = MOP;
        }

        public USDBean getUSD() {
            return USD;
        }

        public void setUSD(USDBean USD) {
            this.USD = USD;
        }

        public static class CNYBean {
            /**
             * name : 人民币
             * symbol : ￥
             * currency : CNY
             * cackeKey :
             * rate : 1
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private int rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public int getRate() {
                return rate;
            }

            public void setRate(int rate) {
                this.rate = rate;
            }
        }

        public static class HKDBean {
            /**
             * name : 港币
             * symbol : HK$
             * currency : HKD
             * cackeKey : cacheCurrencyHKD
             * rate : 1.138434
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class MOPBean {
            /**
             * name : 澳门币
             * symbol : MOP$
             * currency : MOP
             * cackeKey : cacheCurrencyMOP
             * rate : 1.170138
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class USDBean {
            /**
             * name : 美元
             * symbol : US$
             * currency : USD
             * cackeKey : cacheCurrencyUSD
             * rate : 0.144987
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }
    }

    public static class GoodsListBean {
        /**
         * goods_id : 101024
         * goods_name : 机械姬
         * goods_image : http://127.0.0.1/data/upload/shop/common/default_goods_image_1280.gif
         * goods_price : 100000.00
         * goods_salenum : 2
         * store_id : 1
         * is_enquiry : 0
         * goods_state : 1
         * is_second_hand : 1
         * store_name : 快优易自营
         */

        private String goods_id;
        private String goods_name;
        private String goods_image;
        private String goods_price;
        private String goods_salenum;
        private String store_id;
        private String is_enquiry;
        private String goods_state;
        private String is_second_hand;
        private String store_name;

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_image() {
            return goods_image;
        }

        public void setGoods_image(String goods_image) {
            this.goods_image = goods_image;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getGoods_salenum() {
            return goods_salenum;
        }

        public void setGoods_salenum(String goods_salenum) {
            this.goods_salenum = goods_salenum;
        }

        public String getStore_id() {
            return store_id;
        }

        public void setStore_id(String store_id) {
            this.store_id = store_id;
        }

        public String getIs_enquiry() {
            return is_enquiry;
        }

        public void setIs_enquiry(String is_enquiry) {
            this.is_enquiry = is_enquiry;
        }

        public String getGoods_state() {
            return goods_state;
        }

        public void setGoods_state(String goods_state) {
            this.goods_state = goods_state;
        }

        public String getIs_second_hand() {
            return is_second_hand;
        }

        public void setIs_second_hand(String is_second_hand) {
            this.is_second_hand = is_second_hand;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }
    }
}
