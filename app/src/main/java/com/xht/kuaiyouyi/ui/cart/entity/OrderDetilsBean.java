package com.xht.kuaiyouyi.ui.cart.entity;

import java.util.List;

public class OrderDetilsBean {


    /**
     * address : {"address":"行不行v","area_info":"天津 天津市 河東區","true_name":"46766767","mob_phone":"4646646","address_id":"266","mobile_zone1":"86"}
     * pay_info : []
     * time : {"order_pay":"0","order_pay_end":"0","order_pay_beg":"0","create_order":"1535511515","order_finish":"0","shipping":"0"}
     * other : {"approve_order_id":0,"is_pay_auth":true,"order_main_status":"等待付款","order_status":"200588855516705028","is_submit_payinfo":false,"buy_goods_name":"个人"}
     * goods : {"order_main_state":"1010","order_main_pay_currency_symbol":"MOP$","order_main_goods_total":12319,"order_main_deal_active":"1","order_main_pay_two":0,"supplement_amount":0,"order_main_pay_active":"3","order_main_state_name":"待付款","order_main_pay_currency_rate":"1.1701","order_list":[{"store_id":"15","goods_count":4,"store_name":"ssssssss","order_sn":"1000000000068902","order_amount":"1009.00","goods_list":[{"goods_name":"【美好家】螺丝刀 发发发分付付付付付付付付付付付付付付 3是的 分 1 1 1","goods_pay_price":999,"goods_num":"3","image_240_url":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803145000439866_240.jpg","profit_rate":0,"goods_price":333,"goods_id":"100470","customs_charges_fee":0,"freight_fee":0,"goods_spec":"尺寸：发发发分付付付付付付付付付付付付付付,尺码：3是的,型号：分,季节：1,test：1,33：1"},{"goods_name":"螺丝刀","goods_pay_price":10,"goods_num":"1","image_240_url":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803176481834773_240.jpg","profit_rate":0,"goods_price":10,"goods_id":"100472","customs_charges_fee":0,"freight_fee":0}]},{"store_id":"22","goods_count":2,"store_name":"企业购","order_sn":"1000000000068903","order_amount":"11310.00","goods_list":[{"goods_name":"惠普(HP) LaserJet Pro M1139 黑白激光多功能一体机 (打印 复印 扫描)","goods_pay_price":100,"goods_num":"1","image_240_url":"http://192.168.0.2/data/upload/shop/store/goods/22/22_05803917426286848_240.jpg","profit_rate":0,"goods_price":100,"goods_id":"100477","customs_charges_fee":0,"freight_fee":0},{"goods_name":"【挪巍】混凝土搅拌机","goods_pay_price":11210,"goods_num":"1","image_240_url":"http://192.168.0.2/data/upload/shop/store/goods/22/22_05804040514366836_240.jpg","profit_rate":0,"goods_price":11210,"goods_id":"100478","customs_charges_fee":0,"freight_fee":0}]}],"order_main_other_total":0,"order_main_pay_currency_name":"澳门币","beginTime":{"min":59,"sec":57,"hour":23,"day":0,"type":1},"order_main_sn":"1000000000068901","order_main_amount":12319,"order_main_pay_one":12319}
     */

    private AddressBean address;
    private TimeBean time;
    private OtherBean other;
    private GoodsBean goods;
    private PayInfoBean pay_info;
    private LogisticsBean logistics;

    public AddressBean getAddress() {
        return address;
    }

    public void setAddress(AddressBean address) {
        this.address = address;
    }

    public TimeBean getTime() {
        return time;
    }

    public void setTime(TimeBean time) {
        this.time = time;
    }

    public OtherBean getOther() {
        return other;
    }

    public void setOther(OtherBean other) {
        this.other = other;
    }

    public GoodsBean getGoods() {
        return goods;
    }

    public void setGoods(GoodsBean goods) {
        this.goods = goods;
    }

    public LogisticsBean getLogistics() {
        return logistics;
    }

    public void setLogistics(LogisticsBean logistics) {
        this.logistics = logistics;
    }

    public PayInfoBean getPay_info() {
        return pay_info;
    }

    public void setPay_info(PayInfoBean pay_info) {
        this.pay_info = pay_info;
    }

    public static class PayInfoBean {
        /**
         * collector : []
         * address : {"address_id":"90","true_name":"展鸿","area_info":"北京 北京市 東城區","address":"是的是的","mob_phone":"13444444444","mobile_zone1":"86"}
         */

        private AddressBeanX address;
        private List<CollectorBean> collector;
        public AddressBeanX getAddress() {
            return address;
        }

        public void setAddress(AddressBeanX address) {
            this.address = address;
        }

        public List<CollectorBean> getCollector() {
            return collector;
        }

        public void setCollector(List<CollectorBean> collector) {
            this.collector = collector;
        }

        public static class CollectorBean {
            /**
             * pay_remark : 订单全额款项
             * serial_number : 83452345235
             * id : 1
             * is_delete : 0
             * mobile : 13545678900
             * name : 猪猪侠3
             * sex : 1
             * update_time : 1523945034
             */

            private String pay_remark;
            private String serial_number;
            private String id;
            private String is_delete;
            private String mobile;
            private String name;
            private String sex;
            private String update_time;
            private String avatar;

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getPay_remark() {
                return pay_remark;
            }

            public void setPay_remark(String pay_remark) {
                this.pay_remark = pay_remark;
            }

            public String getSerial_number() {
                return serial_number;
            }

            public void setSerial_number(String serial_number) {
                this.serial_number = serial_number;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getIs_delete() {
                return is_delete;
            }

            public void setIs_delete(String is_delete) {
                this.is_delete = is_delete;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSex() {
                return sex;
            }

            public void setSex(String sex) {
                this.sex = sex;
            }

            public String getUpdate_time() {
                return update_time;
            }

            public void setUpdate_time(String update_time) {
                this.update_time = update_time;
            }
        }

        public static class AddressBeanX {
            /**
             * address_id : 90
             * true_name : 展鸿
             * area_info : 北京 北京市 東城區
             * address : 是的是的
             * mob_phone : 13444444444
             * mobile_zone1 : 86
             */

            private String address_id;
            private String true_name;
            private String area_info;
            private String address;
            private String mob_phone;
            private String mobile_zone1;

            public String getAddress_id() {
                return address_id;
            }

            public void setAddress_id(String address_id) {
                this.address_id = address_id;
            }

            public String getTrue_name() {
                return true_name;
            }

            public void setTrue_name(String true_name) {
                this.true_name = true_name;
            }

            public String getArea_info() {
                return area_info;
            }

            public void setArea_info(String area_info) {
                this.area_info = area_info;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getMob_phone() {
                return mob_phone;
            }

            public void setMob_phone(String mob_phone) {
                this.mob_phone = mob_phone;
            }

            public String getMobile_zone1() {
                return mobile_zone1;
            }

            public void setMobile_zone1(String mobile_zone1) {
                this.mobile_zone1 = mobile_zone1;
            }
        }



        /**
         * bank_item : {"name":"澳门骑士路支行"}
         * bank_currency : {"account_bank":"中国银行澳门分行","account_name":"珠海新海通电子商务有限公司","account":"11001013900053023958","name":"港币(HKD)"}
         */

        private BankItemBean bank_item;
        private BankCurrencyBean bank_currency;

        public BankItemBean getBank_item() {
            return bank_item;
        }

        public void setBank_item(BankItemBean bank_item) {
            this.bank_item = bank_item;
        }

        public BankCurrencyBean getBank_currency() {
            return bank_currency;
        }

        public void setBank_currency(BankCurrencyBean bank_currency) {
            this.bank_currency = bank_currency;
        }

        public static class BankItemBean {
            /**
             * name : 澳门骑士路支行
             */

            private String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class BankCurrencyBean {
            /**
             * account_bank : 中国银行澳门分行
             * account_name : 珠海新海通电子商务有限公司
             * account : 11001013900053023958
             * name : 港币(HKD)
             */

            private String account_bank;
            private String account_name;
            private String account;
            private String name;

            public String getAccount_bank() {
                return account_bank;
            }

            public void setAccount_bank(String account_bank) {
                this.account_bank = account_bank;
            }

            public String getAccount_name() {
                return account_name;
            }

            public void setAccount_name(String account_name) {
                this.account_name = account_name;
            }

            public String getAccount() {
                return account;
            }

            public void setAccount(String account) {
                this.account = account;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }

    public static class AddressBean {
        /**
         * address : 行不行v
         * area_info : 天津 天津市 河東區
         * true_name : 46766767
         * mob_phone : 4646646
         * address_id : 266
         * mobile_zone1 : 86
         */

        private String address;
        private String area_info;
        private String true_name;
        private String mob_phone;
        private String address_id;
        private String mobile_zone1;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getArea_info() {
            return area_info;
        }

        public void setArea_info(String area_info) {
            this.area_info = area_info;
        }

        public String getTrue_name() {
            return true_name;
        }

        public void setTrue_name(String true_name) {
            this.true_name = true_name;
        }

        public String getMob_phone() {
            return mob_phone;
        }

        public void setMob_phone(String mob_phone) {
            this.mob_phone = mob_phone;
        }

        public String getAddress_id() {
            return address_id;
        }

        public void setAddress_id(String address_id) {
            this.address_id = address_id;
        }

        public String getMobile_zone1() {
            return mobile_zone1;
        }

        public void setMobile_zone1(String mobile_zone1) {
            this.mobile_zone1 = mobile_zone1;
        }
    }

    public static class TimeBean {
        /**
         * order_pay : 0
         * order_pay_end : 0
         * order_pay_beg : 0
         * create_order : 1535511515
         * order_finish : 0
         * shipping : 0
         */

        private String order_pay;
        private String order_pay_end;
        private String order_pay_beg;
        private String create_order;
        private String order_finish;
        private String shipping;

        public String getOrder_pay() {
            return order_pay;
        }

        public void setOrder_pay(String order_pay) {
            this.order_pay = order_pay;
        }

        public String getOrder_pay_end() {
            return order_pay_end;
        }

        public void setOrder_pay_end(String order_pay_end) {
            this.order_pay_end = order_pay_end;
        }

        public String getOrder_pay_beg() {
            return order_pay_beg;
        }

        public void setOrder_pay_beg(String order_pay_beg) {
            this.order_pay_beg = order_pay_beg;
        }

        public String getCreate_order() {
            return create_order;
        }

        public void setCreate_order(String create_order) {
            this.create_order = create_order;
        }

        public String getOrder_finish() {
            return order_finish;
        }

        public void setOrder_finish(String order_finish) {
            this.order_finish = order_finish;
        }

        public String getShipping() {
            return shipping;
        }

        public void setShipping(String shipping) {
            this.shipping = shipping;
        }
    }

    public static class OtherBean {
        /**
         * approve_order_id : 0
         * is_pay_auth : true
         * order_main_status : 等待付款
         * order_status : 200588855516705028
         * is_submit_payinfo : false
         * buy_goods_name : 个人
         */
        private String check_head_name;
        private String approve_order_id;
        private boolean is_pay_auth;
        private String order_main_status;
        private String order_status;
        private boolean is_submit_payinfo;
        private boolean is_cancel_auth;
        private String buy_goods_name;
        private String company_id;
        private String approve_order_status;
        private String first_status;
        private String second_status;
        private String company_remain_money;
        private String company_count_money;

        public String getCheck_head_name() {
            return check_head_name;
        }

        public void setCheck_head_name(String check_head_name) {
            this.check_head_name = check_head_name;
        }

        public String getCompany_remain_money() {
            return company_remain_money;
        }

        public void setCompany_remain_money(String company_remain_money) {
            this.company_remain_money = company_remain_money;
        }

        public String getCompany_count_money() {
            return company_count_money;
        }

        public void setCompany_count_money(String company_count_money) {
            this.company_count_money = company_count_money;
        }

        public boolean isIs_cancel_auth() {
            return is_cancel_auth;
        }

        public void setIs_cancel_auth(boolean is_cancel_auth) {
            this.is_cancel_auth = is_cancel_auth;
        }

        public String getApprove_order_id() {
            return approve_order_id;
        }

        public void setApprove_order_id(String approve_order_id) {
            this.approve_order_id = approve_order_id;
        }

        public boolean isIs_pay_auth() {
            return is_pay_auth;
        }

        public void setIs_pay_auth(boolean is_pay_auth) {
            this.is_pay_auth = is_pay_auth;
        }

        public String getOrder_main_status() {
            return order_main_status;
        }

        public void setOrder_main_status(String order_main_status) {
            this.order_main_status = order_main_status;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public boolean isIs_submit_payinfo() {
            return is_submit_payinfo;
        }

        public void setIs_submit_payinfo(boolean is_submit_payinfo) {
            this.is_submit_payinfo = is_submit_payinfo;
        }

        public String getBuy_goods_name() {
            return buy_goods_name;
        }

        public void setBuy_goods_name(String buy_goods_name) {
            this.buy_goods_name = buy_goods_name;
        }

        public String getCompany_id() {
            return company_id;
        }

        public void setCompany_id(String company_id) {
            this.company_id = company_id;
        }

        public String getApprove_order_status() {
            return approve_order_status;
        }

        public void setApprove_order_status(String approve_order_status) {
            this.approve_order_status = approve_order_status;
        }

        public String getFirst_status() {
            return first_status;
        }

        public void setFirst_status(String first_status) {
            this.first_status = first_status;
        }

        public String getSecond_status() {
            return second_status;
        }

        public void setSecond_status(String second_status) {
            this.second_status = second_status;
        }
    }

    public static class GoodsBean {
        /**
         * order_main_state : 1010
         * order_main_pay_currency_symbol : MOP$
         * order_main_goods_total : 12319
         * order_main_deal_active : 1
         * order_main_pay_two : 0
         * supplement_amount : 0
         * order_main_pay_active : 3
         * order_main_state_name : 待付款
         * order_main_pay_currency_rate : 1.1701
         * order_list : [{"store_id":"15","goods_count":4,"store_name":"ssssssss","order_sn":"1000000000068902","order_amount":"1009.00","goods_list":[{"goods_name":"【美好家】螺丝刀 发发发分付付付付付付付付付付付付付付 3是的 分 1 1 1","goods_pay_price":999,"goods_num":"3","image_240_url":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803145000439866_240.jpg","profit_rate":0,"goods_price":333,"goods_id":"100470","customs_charges_fee":0,"freight_fee":0,"goods_spec":"尺寸：发发发分付付付付付付付付付付付付付付,尺码：3是的,型号：分,季节：1,test：1,33：1"},{"goods_name":"螺丝刀","goods_pay_price":10,"goods_num":"1","image_240_url":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803176481834773_240.jpg","profit_rate":0,"goods_price":10,"goods_id":"100472","customs_charges_fee":0,"freight_fee":0}]},{"store_id":"22","goods_count":2,"store_name":"企业购","order_sn":"1000000000068903","order_amount":"11310.00","goods_list":[{"goods_name":"惠普(HP) LaserJet Pro M1139 黑白激光多功能一体机 (打印 复印 扫描)","goods_pay_price":100,"goods_num":"1","image_240_url":"http://192.168.0.2/data/upload/shop/store/goods/22/22_05803917426286848_240.jpg","profit_rate":0,"goods_price":100,"goods_id":"100477","customs_charges_fee":0,"freight_fee":0},{"goods_name":"【挪巍】混凝土搅拌机","goods_pay_price":11210,"goods_num":"1","image_240_url":"http://192.168.0.2/data/upload/shop/store/goods/22/22_05804040514366836_240.jpg","profit_rate":0,"goods_price":11210,"goods_id":"100478","customs_charges_fee":0,"freight_fee":0}]}]
         * order_main_other_total : 0
         * order_main_pay_currency_name : 澳门币
         * beginTime : {"min":59,"sec":57,"hour":23,"day":0,"type":1}
         * order_main_sn : 1000000000068901
         * order_main_amount : 12319
         * order_main_pay_one : 12319
         */

        private String order_main_second_pay_end;
        private String order_main_state;
        private String order_main_pay_currency_symbol;
        private String order_main_goods_total;
        private String order_main_deal_active;
        private String order_main_pay_two;
        private String supplement_amount;
        private String order_main_pay_active;
        private String order_main_state_name;
        private String order_main_pay_currency_rate;
        private String order_main_other_total;
        private String order_main_pay_currency_name;
        private BeginTimeBean beginTime;
        private String order_main_sn;
        private String order_main_amount;
        private String order_main_pay_one;
        private String order_main_pay_beg;
        private String order_main_pay_end;

        public String getOrder_main_second_pay_end() {
            return order_main_second_pay_end;
        }

        public void setOrder_main_second_pay_end(String order_main_second_pay_end) {
            this.order_main_second_pay_end = order_main_second_pay_end;
        }

        public String getOrder_main_pay_beg() {
            return order_main_pay_beg;
        }

        public void setOrder_main_pay_beg(String order_main_pay_beg) {
            this.order_main_pay_beg = order_main_pay_beg;
        }

        public String getOrder_main_pay_end() {
            return order_main_pay_end;
        }

        public void setOrder_main_pay_end(String order_main_pay_end) {
            this.order_main_pay_end = order_main_pay_end;
        }

        private List<OrderListBean> order_list;

        public String getOrder_main_state() {
            return order_main_state;
        }

        public void setOrder_main_state(String order_main_state) {
            this.order_main_state = order_main_state;
        }

        public String getOrder_main_pay_currency_symbol() {
            return order_main_pay_currency_symbol;
        }

        public void setOrder_main_pay_currency_symbol(String order_main_pay_currency_symbol) {
            this.order_main_pay_currency_symbol = order_main_pay_currency_symbol;
        }

        public String getOrder_main_goods_total() {
            return order_main_goods_total;
        }

        public void setOrder_main_goods_total(String order_main_goods_total) {
            this.order_main_goods_total = order_main_goods_total;
        }

        public String getOrder_main_deal_active() {
            return order_main_deal_active;
        }

        public void setOrder_main_deal_active(String order_main_deal_active) {
            this.order_main_deal_active = order_main_deal_active;
        }

        public String getOrder_main_pay_two() {
            return order_main_pay_two;
        }

        public void setOrder_main_pay_two(String order_main_pay_two) {
            this.order_main_pay_two = order_main_pay_two;
        }

        public String getSupplement_amount() {
            return supplement_amount;
        }

        public void setSupplement_amount(String  supplement_amount) {
            this.supplement_amount = supplement_amount;
        }

        public String getOrder_main_pay_active() {
            return order_main_pay_active;
        }

        public void setOrder_main_pay_active(String order_main_pay_active) {
            this.order_main_pay_active = order_main_pay_active;
        }

        public String getOrder_main_state_name() {
            return order_main_state_name;
        }

        public void setOrder_main_state_name(String order_main_state_name) {
            this.order_main_state_name = order_main_state_name;
        }

        public String getOrder_main_pay_currency_rate() {
            return order_main_pay_currency_rate;
        }

        public void setOrder_main_pay_currency_rate(String order_main_pay_currency_rate) {
            this.order_main_pay_currency_rate = order_main_pay_currency_rate;
        }

        public String getOrder_main_other_total() {
            return order_main_other_total;
        }

        public void setOrder_main_other_total(String order_main_other_total) {
            this.order_main_other_total = order_main_other_total;
        }

        public String getOrder_main_pay_currency_name() {
            return order_main_pay_currency_name;
        }

        public void setOrder_main_pay_currency_name(String order_main_pay_currency_name) {
            this.order_main_pay_currency_name = order_main_pay_currency_name;
        }

        public BeginTimeBean getBeginTime() {
            return beginTime;
        }

        public void setBeginTime(BeginTimeBean beginTime) {
            this.beginTime = beginTime;
        }

        public String getOrder_main_sn() {
            return order_main_sn;
        }

        public void setOrder_main_sn(String order_main_sn) {
            this.order_main_sn = order_main_sn;
        }

        public String getOrder_main_amount() {
            return order_main_amount;
        }

        public void setOrder_main_amount(String order_main_amount) {
            this.order_main_amount = order_main_amount;
        }

        public String getOrder_main_pay_one() {
            return order_main_pay_one;
        }

        public void setOrder_main_pay_one(String order_main_pay_one) {
            this.order_main_pay_one = order_main_pay_one;
        }

        public List<OrderListBean> getOrder_list() {
            return order_list;
        }

        public void setOrder_list(List<OrderListBean> order_list) {
            this.order_list = order_list;
        }

        public static class BeginTimeBean {
            /**
             * min : 59
             * sec : 57
             * hour : 23
             * day : 0
             * type : 1
             */

            private int min;
            private int sec;
            private int hour;
            private int day;
            private int type;

            public int getMin() {
                return min;
            }

            public void setMin(int min) {
                this.min = min;
            }

            public int getSec() {
                return sec;
            }

            public void setSec(int sec) {
                this.sec = sec;
            }

            public int getHour() {
                return hour;
            }

            public void setHour(int hour) {
                this.hour = hour;
            }

            public int getDay() {
                return day;
            }

            public void setDay(int day) {
                this.day = day;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }
        }

        public static class OrderListBean {
            /**
             * store_id : 15
             * goods_count : 4
             * store_name : ssssssss
             * order_sn : 1000000000068902
             * order_amount : 1009.00
             * goods_list : [{"goods_name":"【美好家】螺丝刀 发发发分付付付付付付付付付付付付付付 3是的 分 1 1 1","goods_pay_price":999,"goods_num":"3","image_240_url":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803145000439866_240.jpg","profit_rate":0,"goods_price":333,"goods_id":"100470","customs_charges_fee":0,"freight_fee":0,"goods_spec":"尺寸：发发发分付付付付付付付付付付付付付付,尺码：3是的,型号：分,季节：1,test：1,33：1"},{"goods_name":"螺丝刀","goods_pay_price":10,"goods_num":"1","image_240_url":"http://192.168.0.2/data/upload/shop/store/goods/15/15_05803176481834773_240.jpg","profit_rate":0,"goods_price":10,"goods_id":"100472","customs_charges_fee":0,"freight_fee":0}]
             */

            private String store_id;
            private String default_avatar;
            private String default_im;
            private int goods_count;
            private String store_name;
            private String order_sn;
            private String order_amount;

            private List<GoodsListBean> goods_list;

            public String getStore_id() {
                return store_id;
            }

            public void setStore_id(String store_id) {
                this.store_id = store_id;
            }

            public String getDefault_avatar() {
                return default_avatar;
            }

            public void setDefault_avatar(String default_avatar) {
                this.default_avatar = default_avatar;
            }

            public String getDefault_im() {
                return default_im;
            }

            public void setDefault_im(String default_im) {
                this.default_im = default_im;
            }

            public int getGoods_count() {
                return goods_count;
            }

            public void setGoods_count(int goods_count) {
                this.goods_count = goods_count;
            }

            public String getStore_name() {
                return store_name;
            }

            public void setStore_name(String store_name) {
                this.store_name = store_name;
            }

            public String getOrder_sn() {
                return order_sn;
            }

            public void setOrder_sn(String order_sn) {
                this.order_sn = order_sn;
            }

            public String getOrder_amount() {
                return order_amount;
            }

            public void setOrder_amount(String order_amount) {
                this.order_amount = order_amount;
            }

            public List<GoodsListBean> getGoods_list() {
                return goods_list;
            }

            public void setGoods_list(List<GoodsListBean> goods_list) {
                this.goods_list = goods_list;
            }

            public static class GoodsListBean {
                /**
                 * goods_name : 【美好家】螺丝刀 发发发分付付付付付付付付付付付付付付 3是的 分 1 1 1
                 * goods_pay_price : 999
                 * goods_num : 3
                 * image_240_url : http://192.168.0.2/data/upload/shop/store/goods/15/15_05803145000439866_240.jpg
                 * profit_rate : 0
                 * goods_price : 333
                 * goods_id : 100470
                 * customs_charges_fee : 0
                 * freight_fee : 0
                 * goods_spec : 尺寸：发发发分付付付付付付付付付付付付付付,尺码：3是的,型号：分,季节：1,test：1,33：1
                 */

                private String goods_name;
                private String goods_pay_price;
                private String goods_num;
                private String image_240_url;
                private int profit_rate;
                private String goods_price;
                private String goods_id;
                private String customs_charges_fee;
                private String freight_fee;
                private String goods_spec;
                private int is_second_hand;

                public int getIs_second_hand() {
                    return is_second_hand;
                }

                public void setIs_second_hand(int is_second_hand) {
                    this.is_second_hand = is_second_hand;
                }

                public String getGoods_name() {
                    return goods_name;
                }

                public void setGoods_name(String goods_name) {
                    this.goods_name = goods_name;
                }

                public String getGoods_pay_price() {
                    return goods_pay_price;
                }

                public void setGoods_pay_price(String goods_pay_price) {
                    this.goods_pay_price = goods_pay_price;
                }

                public String getGoods_num() {
                    return goods_num;
                }

                public void setGoods_num(String goods_num) {
                    this.goods_num = goods_num;
                }

                public String getImage_240_url() {
                    return image_240_url;
                }

                public void setImage_240_url(String image_240_url) {
                    this.image_240_url = image_240_url;
                }

                public int getProfit_rate() {
                    return profit_rate;
                }

                public void setProfit_rate(int profit_rate) {
                    this.profit_rate = profit_rate;
                }

                public String getGoods_price() {
                    return goods_price;
                }

                public void setGoods_price(String goods_price) {
                    this.goods_price = goods_price;
                }

                public String getGoods_id() {
                    return goods_id;
                }

                public void setGoods_id(String goods_id) {
                    this.goods_id = goods_id;
                }

                public String getCustoms_charges_fee() {
                    return customs_charges_fee;
                }

                public void setCustoms_charges_fee(String customs_charges_fee) {
                    this.customs_charges_fee = customs_charges_fee;
                }

                public String getFreight_fee() {
                    return freight_fee;
                }

                public void setFreight_fee(String freight_fee) {
                    this.freight_fee = freight_fee;
                }

                public String getGoods_spec() {
                    return goods_spec;
                }

                public void setGoods_spec(String goods_spec) {
                    this.goods_spec = goods_spec;
                }
            }
        }
    }
    public static class LogisticsBean {
        /**
         * order_logistics_id : 29
         * order_main_id : 146
         * text : 商家备货中
         * time : 1532401054
         */

        private String order_logistics_id;
        private String order_main_id;
        private String text;
        private String time;

        public String getOrder_logistics_id() {
            return order_logistics_id;
        }

        public void setOrder_logistics_id(String order_logistics_id) {
            this.order_logistics_id = order_logistics_id;
        }

        public String getOrder_main_id() {
            return order_main_id;
        }

        public void setOrder_main_id(String order_main_id) {
            this.order_main_id = order_main_id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }
    }
}
