package com.xht.kuaiyouyi.ui.mine.collection;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.classic.common.MultipleStatusView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.mine.adapter.CollectionAdapter;
import com.xht.kuaiyouyi.ui.mine.entity.CollectionBean;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class GoodsCollectionFragment extends BaseFragment {
    @BindView(R.id.mRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.listview_order)
    ListView listView;
    @BindView(R.id.multipleStatusView)
    MultipleStatusView multipleStatusView;
    @BindView(R.id.iv_to_top)
    ImageView iv_to_top;

    private CollectionBean mCollectionBean;
    private CollectionAdapter mAdapter;
    private String mKyy = "";

    public static GoodsCollectionFragment newInstance() {
        Bundle args = new Bundle();
        GoodsCollectionFragment fragment = new GoodsCollectionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        requestGoodsLsit();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString("goods_id", mCollectionBean.getFavorites_list().get(position).getGoods_id());
                goToActivity(GoodsActivity.class, bundle);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                DialogUtils.createTwoBtnDialog(getActivity(),
                        getResources().getString(R.string.cancel_collection_tip),
                        getResources().getString(R.string.dialog_confirm),
                        getResources().getString(R.string.dialog_cancel),
                        new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(Dialog dialog) {
                                requestCancelCollection(mCollectionBean.getFavorites_list().get(position).getGoods_id(),
                                        position);
                            }
                        }, new DialogUtils.OnLeftBtnListener() {
                            @Override
                            public void setOnLeftListener(Dialog dialog) {

                            }
                        }, true, true);
                return true;
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                requestGoodsLsit();
            }
        });
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                mKyy = "";
                requestGoodsLsit();
            }
        });

        iv_to_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.smoothScrollToPosition(0);
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    View firstVisibleItemView = listView.getChildAt(0);
                    if (firstVisibleItemView != null && firstVisibleItemView.getTop() == 0) {
                        //滚动到了顶部
                        iv_to_top.setVisibility(View.GONE);
                    }else {
                        //不在顶部
                        iv_to_top.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        multipleStatusView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestGoodsLsit();
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_collection;
    }


    private void requestGoodsLsit() {
        if (TextUtils.isEmpty(mKyy)) {
            multipleStatusView.showLoading();
        }
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_FAVORITE_GOODS_LIST())
                .withLoadPOST(new NetCallBack<CollectionBean>() {

                    @NotNull
                    @Override
                    public Class<CollectionBean> getRealType() {
                        return CollectionBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity() == null){
                            return;
                        }
                        if (TextUtils.isEmpty(mKyy)) {
                            multipleStatusView.showError();
                            multipleStatusView.setClickable(true);
                        }else {
                            Toast.makeText(getContext(), getString(R.string.not_network), Toast.LENGTH_SHORT).show();
                        }
                        if (smartRefreshLayout.getState() == RefreshState.Loading) {
                            smartRefreshLayout.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull CollectionBean collectionBean) {
                        if(getActivity() == null){
                            return;
                        }
                        if (smartRefreshLayout.getState() == RefreshState.Loading) {
                            smartRefreshLayout.finishLoadMore();
                        }
                        if (TextUtils.isEmpty(mKyy)) {
                            //刷新
                            mCollectionBean = collectionBean;
                            if (mCollectionBean.getFavorites_list() != null && mCollectionBean.getFavorites_list().size() > 0) {
                                multipleStatusView.showContent();
                                mAdapter = new CollectionAdapter(getContext(), mCollectionBean);
                                listView.setAdapter(mAdapter);
                                mKyy = mCollectionBean.getKyy();
                            } else {
                                multipleStatusView.setClickable(false);
                                multipleStatusView.showEmpty();
                            }
                            iv_to_top.setVisibility(View.GONE);
                        } else {
                            //分页加载
                            if (collectionBean.getFavorites_list() != null && collectionBean.getFavorites_list().size() > 0) {
                                mKyy = collectionBean.getKyy();
                                mCollectionBean.getFavorites_list().addAll(collectionBean.getFavorites_list());
                                mAdapter.notifyDataSetChanged();
                            } else {
                                smartRefreshLayout.setNoMoreData(true);
                                smartRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
                            }
                        }
                    }
                }, false, mKyy);

    }

    //取消收藏
    private void requestCancelCollection(String goods_id, final int position) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_FAVORITE())
                .addParam("fid", goods_id)
                .addParam("type", "1")
                .addParam("fav_type", "1")
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity() == null){
                            return;
                        }
                        Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        if(getActivity() == null){
                            return;
                        }
                        mCollectionBean.getFavorites_list().remove(position);
                        mAdapter.notifyDataSetChanged();
                        if(mCollectionBean.getFavorites_list().size()==0){
                            multipleStatusView.showEmpty();
                        }
                    }
                }, false);

    }

}
