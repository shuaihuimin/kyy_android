package com.xht.kuaiyouyi.ui.login.entity;

public class LoginBean {


    /**
     * token_id : 127
     * mobile_zone :
     * member_mobile :
     * token : 954bda2d0f0b28f378e4fa2a8b504f99
     * member_info : {"area_code":"86","name":"xht_8sh","avatar":"http://192.168.0.2/data/upload/shop/common/default_user_portrait.gif","member_id":"56","grade":{"level":0,"level_name":"V0","exppoints":0}}
     */

    private String token_id;
    private String mobile_zone;
    private String member_mobile;
    private String token;
    private MemberInfoBean member_info;

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getMobile_zone() {
        return mobile_zone;
    }

    public void setMobile_zone(String mobile_zone) {
        this.mobile_zone = mobile_zone;
    }

    public String getMember_mobile() {
        return member_mobile;
    }

    public void setMember_mobile(String member_mobile) {
        this.member_mobile = member_mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public MemberInfoBean getMember_info() {
        return member_info;
    }

    public void setMember_info(MemberInfoBean member_info) {
        this.member_info = member_info;
    }

    public static class MemberInfoBean {
        /**
         * area_code : 86
         * name : xht_8sh
         * avatar : http://192.168.0.2/data/upload/shop/common/default_user_portrait.gif
         * member_id : 56
         * grade : {"level":0,"level_name":"V0","exppoints":0}
         */

        private String area_code;
        private String name;
        private String avatar;
        private String member_id;
        private GradeBean grade;

        public String getArea_code() {
            return area_code;
        }

        public void setArea_code(String area_code) {
            this.area_code = area_code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getMember_id() {
            return member_id;
        }

        public void setMember_id(String member_id) {
            this.member_id = member_id;
        }

        public GradeBean getGrade() {
            return grade;
        }

        public void setGrade(GradeBean grade) {
            this.grade = grade;
        }

        public static class GradeBean {
            /**
             * level : 0
             * level_name : V0
             * exppoints : 0
             */

            private int level;
            private String level_name;
            private int exppoints;

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public String getLevel_name() {
                return level_name;
            }

            public void setLevel_name(String level_name) {
                this.level_name = level_name;
            }

            public int getExppoints() {
                return exppoints;
            }

            public void setExppoints(int exppoints) {
                this.exppoints = exppoints;
            }
        }
    }
}
