package com.xht.kuaiyouyi.ui.message.util;

import com.google.gson.Gson;
import com.xht.kuaiyouyi.BuildConfig;
import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.event.IMMessageEvent;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.message.bean.MsgBean;
import com.xht.kuaiyouyi.ui.message.bean.UpdateUserBean;
import com.xht.kuaiyouyi.ui.message.db.ChatListBean;
import com.xht.kuaiyouyi.ui.message.db.ChatMsgBean;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class IMUtil {

    public static long now_chat_store_id = 0;//正在聊天的店铺ID

    public static Socket socket;

    public static void initIM() {
        try {
            socket = IO.socket(BuildConfig.IM_HOST);
        } catch (URISyntaxException e) {

        }
        if (socket != null) {
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    UpdateUserBean updateUserBean = new UpdateUserBean();
                    updateUserBean.setU_id(Login.Companion.getInstance().getUid());
                    updateUserBean.setAvatar(Login.Companion.getInstance().getMemberAvatar());
                    updateUserBean.setU_name(Login.Companion.getInstance().getUsername());
                    String content = new Gson().toJson(updateUserBean);
                    try {
                        socket.emit("update_user", new JSONObject(content));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.log("imutil-log", "update_user" + content);
                }
            });

            socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Utils.log("imutil-log", "disconnect");
                }
            });

            socket.on("app_get_msg", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Utils.log("imutil-log", "app_get_msg" + args[0].toString());
                    MsgBean msgBean = null;
                    try {
                        msgBean = new Gson().fromJson(((JSONArray) args[0]).getString(0), MsgBean.class);
                        if (msgBean != null && msgBean.getStore_id() > 0) {
                            //聊天页数据
                            ChatMsgBean.ListBean listBean = new ChatMsgBean.ListBean();
                            listBean.setF_id(msgBean.getF_id());
                            listBean.setAdd_time(msgBean.getAdd_time());
                            listBean.setF_ip(msgBean.getF_ip());
                            listBean.setF_name(msgBean.getF_name());
                            listBean.setMsg_type(msgBean.getMsg_type());
                            listBean.setStore_id(msgBean.getStore_id());
                            if (msgBean.getMsg_type() == MsgBean.MSG_TYPE_IMG) {
                                listBean.setImageHeight(0);
                                listBean.setImageWidth(0);
                            }else if(msgBean.getMsg_type() == MsgBean.MSG_TYPE_FILE){
                                listBean.setFile(msgBean.getFile());
                            }
                            listBean.setT_msg(msgBean.getT_msg());
                            listBean.setT_id(msgBean.getT_id());
                            listBean.setT_name(msgBean.getT_name());
                            listBean.setM_id(msgBean.getM_id());
                            //通知聊天页更新
                            EventBus.getDefault().post(listBean);


                            //聊天列表的数据
                            ChatListBean.DataBean dataBean = new ChatListBean.DataBean();
                            dataBean.setStore_avatar(msgBean.getStore_info().getStore_avatar());
                            dataBean.setStore_id(msgBean.getStore_info().getStore_id() + "");
                            dataBean.setStore_name(msgBean.getStore_info().getStore_name());
                            if(IMUtil.now_chat_store_id == msgBean.getStore_info().getStore_id()){
                                dataBean.setNo_read_msg_num(0);
                            }else {
                                dataBean.setNo_read_msg_num(1);
                                Login.Companion.getInstance().setUnread_msg_num((Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num())+1)+"");
                                EventBus.getDefault().post(new MessageEvent(MessageEvent.UNREAD_MSG_NUM));
                            }
                            dataBean.setMsg_type(msgBean.getMsg_type());
                            dataBean.setT_msg(msgBean.getT_msg());
                            dataBean.setTime(msgBean.getAdd_time());
                            dataBean.setU_id(msgBean.getF_id() + "");
                            //通知聊天列表更新
                            EventBus.getDefault().post(dataBean);
                            if (IMUtil.now_chat_store_id == 0 || (IMUtil.now_chat_store_id != 0 && IMUtil.now_chat_store_id != msgBean.getStore_id())) {
                                String message = "";
                                switch (msgBean.getMsg_type()) {
                                    case MsgBean.MSG_TYPE_IMG:
                                        message = KyyApp.context.getResources().getString(R.string.chat_list_img);
                                        break;
                                    case MsgBean.MSG_TYPE_GOODS_INFO:
                                        message = KyyApp.context.getResources().getString(R.string.chat_list_goods_info);
                                        break;
                                    case MsgBean.MSG_TYPE_TEXT:
                                        message = msgBean.getT_msg();
                                        break;
                                    case MsgBean.MSG_TYPE_FILE:
                                        message = KyyApp.context.getResources().getString(R.string.chat_list_file);
                                        break;
                                }
                                //发送推送
                                EventBus.getDefault().post(new IMMessageEvent(message
                                        , dataBean.getStore_name(), dataBean.getU_id()
                                        , dataBean.getStore_avatar(), dataBean.getStore_id()));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

//            //服务端返回心跳事件
//            socket.on(Socket.EVENT_PONG, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Utils.log("imutil-log","EVENT_PONG");
//                }
//            });
//
//            //发送心跳事件的回调，心跳默认是25秒发送一次
//            socket.on(Socket.EVENT_PING, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Utils.log("imutil-log","EVENT_PING");
//                }
//            });

            socket.connect();
        }

    }

}
