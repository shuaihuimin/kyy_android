package com.xht.kuaiyouyi.ui.cart.order;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.cart.adapter.OrderDetilsAdapter;
import com.xht.kuaiyouyi.ui.cart.entity.OrderDetilsBean;
import com.xht.kuaiyouyi.ui.enterprise.Constant;
import com.xht.kuaiyouyi.ui.enterprise.activity.ApprovalDetailEnterpriseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.ui.mine.order.LogisticsActivity;
import com.xht.kuaiyouyi.utils.CommonPopupWindow;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.BadgeView;
import com.xht.kuaiyouyi.widget.CustomExpandableListView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import butterknife.BindView;

public class OrderDetilsActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.order_type)
    TextView order_type;
    @BindView(R.id.order_remaining_time)
    TextView order_remaining_time;
    @BindView(R.id.tv_orderdetail_name)
    TextView tv_orderdetail_name;
    @BindView(R.id.tv_orderdetail_phone)
    TextView tv_orderdetail_phone;
    @BindView(R.id.tv_orderdetail_addres)
    TextView tv_orderdetail_addres;

    @BindView(R.id.tv_freight_always)
    TextView tv_freight_always;
    @BindView(R.id.tv_total_amount_goods)
    TextView tv_total_amount_goods;
    @BindView(R.id.tv_total_amount_order)
    TextView tv_total_amount_order;

    @BindView(R.id.tv_sourcing_company)
    TextView tv_sourcing_company;
    @BindView(R.id.customExpandableListView)
    CustomExpandableListView customExpandableListView;
    @BindView(R.id.tv_approval_information)
    TextView tv_approval_information;
    @BindView(R.id.relative_approval_information)
    RelativeLayout relative_approval_information;
    @BindView(R.id.linear_orderdetils_stage_payment)
    LinearLayout linear_orderdetils_stage_payment;
    @BindView(R.id.tv_first_phase_supplement)
    TextView tv_first_phase_supplement;
    @BindView(R.id.linear_phase)
    LinearLayout linear_phase;
    @BindView(R.id.relative_down_payment)
    RelativeLayout relative_down_payment;
    @BindView(R.id.relative_phase_payment)
    RelativeLayout relative_phase_payment;

    @BindView(R.id.tv_first_phase_price)
    TextView tv_first_phase_price;
    @BindView(R.id.tv_first_phase_state)
    TextView tv_first_phase_state;
    @BindView(R.id.tv_secon_phase_price)
    TextView tv_secon_phase_price;
    @BindView(R.id.tv_secon_phase_state)
    TextView tv_secon_phase_state;


    @BindView(R.id.tv_orderdetils_trade_way)
    TextView tv_orderdetils_trade_way;
    @BindView(R.id.tv_orderdetils_pay_way)
    TextView tv_orderdetils_pay_way;
    @BindView(R.id.tv_orderdetils_pay_currency)
    TextView tv_orderdetils_pay_currency;
    @BindView(R.id.tv_orderdetils_amount_total)
    TextView tv_orderdetils_amount_total;
    @BindView(R.id.tv_trading_currency)
    TextView tv_trading_currency;
    @BindView(R.id.tv_order_number)
    TextView tv_order_number;

    @BindView(R.id.tv_added_new)
    TextView tv_added_new;
    @BindView(R.id.tv_order_added_new)
    TextView tv_order_added_new;
    @BindView(R.id.tv_order_down_payment)
    TextView tv_order_down_payment;
    @BindView(R.id.tv_order_tail_section)
    TextView tv_order_tail_section;
    @BindView(R.id.tv_first_phase_payment)
    TextView tv_first_phase_payment;
    @BindView(R.id.tv_order_first_phase_payment)
    TextView tv_order_first_phase_payment;
    @BindView(R.id.tv_order_second_phase_payment)
    TextView tv_order_second_phase_payment;
    @BindView(R.id.secon_phase_price)
    TextView secon_phase_price;


    @BindView(R.id.relative_btn)
    RelativeLayout relative_btn;
    @BindView(R.id.btn_upload_cheque)
    Button btn_upload_cheque;
    @BindView(R.id.btn_to_pay)
    Button btn_to_pay;
    @BindView(R.id.relative_receive_tickets)
    RelativeLayout relative_receive_tickets;
    @BindView(R.id.relative_confirmorder_logistics)
    RelativeLayout relative_confirmorder_logistics;
    @BindView(R.id.tv_logistics_name)
    TextView tv_logistics_name;
    @BindView(R.id.tv_logistics_time)
    TextView tv_logistics_time;

    @BindView(R.id.tv_order_copy)
    TextView tv_order_copy;
    @BindView(R.id.tv_submitorder_time)
    TextView tv_submitorder_time;
    @BindView(R.id.btn_transfer_information)
    Button btn_transfer_information;
    @BindView(R.id.btn_cance_order)
    Button btn_cance_order;
    @BindView(R.id.tv_pay_currency)
    TextView tv_pay_currency;
    @BindView(R.id.tv_trade_way)
    TextView tv_trade_way;
    @BindView(R.id.relative_cheque_payable)
    RelativeLayout relative_cheque_payable;
    @BindView(R.id.relative_cheque_address)
    RelativeLayout relative_cheque_address;
    @BindView(R.id.tv_cheque_address)
    TextView tv_cheque_address;
    @BindView(R.id.scrollview)
    ScrollView scrollview;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;

    @BindView(R.id.relative_payment_time)
    RelativeLayout relative_payment_time;
    @BindView(R.id.tv_payment_time)
    TextView tv_payment_time;
    @BindView(R.id.relative_delivery_time)
    RelativeLayout relative_delivery_time;
    @BindView(R.id.tv_order_delivery_time)
    TextView tv_order_delivery_time;
    @BindView(R.id.relative_deal_time)
    RelativeLayout relative_deal_time;
    @BindView(R.id.tv_deal_time)
    TextView tv_deal_time;
    @BindView(R.id.relative_firstphase_time)
    RelativeLayout relative_firstphase_time;
    @BindView(R.id.tv_firstphase_time)
    TextView tv_firstphase_time;
    @BindView(R.id.relative_second_phase_time)
    RelativeLayout relative_second_phase_time;
    @BindView(R.id.tv_second_phase_time)
    TextView tv_second_phase_time;
    @BindView(R.id.iv_right)
    ImageView iv_right;
    @BindView(R.id.tv_cheque_payable)
    TextView tv_cheque_payable;

    private BadgeView mBadgeView;

    private String company_id;
    private String approve_order_id;
    private String order_main_id;
    private String amount_total;
    private OrderDetilsBean orderDetilsBean;
    private OrderDetilsAdapter orderDetilsAdapter;

    private CommonPopupWindow popupCompany;


    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    error_retry_view.setVisibility(View.GONE);
                    scrollview.setVisibility(View.VISIBLE);
                    order_type.setText(orderDetilsBean.getOther().getOrder_main_status());

                    order_remaining_time.setVisibility(View.VISIBLE);
                    if(orderDetilsBean.getGoods().getOrder_main_state().equals("1010")
                            || orderDetilsBean.getGoods().getOrder_main_state().equals("2010")){

                        if(orderDetilsBean.getGoods().getBeginTime().getDay()==0
                                && orderDetilsBean.getGoods().getBeginTime().getHour()==0
                                && orderDetilsBean.getGoods().getBeginTime().getMin()==0
                                && orderDetilsBean.getGoods().getBeginTime().getSec()==0){
                            order_remaining_time.setText(getString(R.string.order_time_limit));
                        }else {
                            order_remaining_time.setText(getString(R.string.remaining_time)+orderDetilsBean.getGoods().getBeginTime().getDay()+getString(R.string.day)
                                    +orderDetilsBean.getGoods().getBeginTime().getHour()+getString(R.string.hour)
                                    +orderDetilsBean.getGoods().getBeginTime().getMin()+getString(R.string.min));
                        }

                    }else if(orderDetilsBean.getGoods().getOrder_main_state().equals("2020")
                            || orderDetilsBean.getGoods().getOrder_main_state().equals("1020")){
                        order_remaining_time.setText(getString(R.string.order_time_sendgoods));
                    }else if(orderDetilsBean.getGoods().getOrder_main_state().equals("1051")
                            || orderDetilsBean.getGoods().getOrder_main_state().equals("2051")){
                        order_remaining_time.setText(getString(R.string.order_time_cancel));
                    }else if(orderDetilsBean.getGoods().getOrder_main_state().equals("2050")
                            || orderDetilsBean.getGoods().getOrder_main_state().equals("1050")
                            || orderDetilsBean.getGoods().getOrder_main_state().equals("2052")
                            || orderDetilsBean.getGoods().getOrder_main_state().equals("1052")){
                        order_remaining_time.setText(getString(R.string.order_time_platform_cancel));
                    }else if (orderDetilsBean.getGoods().getOrder_main_state().equals("2060")
                            || orderDetilsBean.getGoods().getOrder_main_state().equals("2061")){
                        if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("2")){
                            order_remaining_time.setText(getString(R.string.order_time_tickets));
                        }else if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("1")){
                            order_remaining_time.setText(getString(R.string.order_time_financialaudit));
                        }
                    }else if(orderDetilsBean.getGoods().getOrder_main_state().equals("1060")){
                        order_remaining_time.setText(getString(R.string.order_time_financialaudit));
                    }else {
                        order_remaining_time.setVisibility(View.GONE);
                    }

                    tv_order_number.setText(orderDetilsBean.getGoods().getOrder_main_sn());

                    //取支票地址
                    if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("2")){
                        if(orderDetilsBean.getPay_info().getAddress().getArea_info()!=null){
                            tv_cheque_address.setText(orderDetilsBean.getPay_info().getAddress().getArea_info()
                                    +orderDetilsBean.getPay_info().getAddress().getAddress());
                        }
                    }

                    //支票抬头
                    if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("2")){
                        if(!TextUtils.isEmpty(orderDetilsBean.getOther().getCheck_head_name())){
                            tv_cheque_payable.setText(orderDetilsBean.getOther().getCheck_head_name());
                        }
                    }

                    //订单的时间
                    tv_submitorder_time.setText(Utils.timedate(orderDetilsBean.getTime().getCreate_order()));
                    if(!orderDetilsBean.getTime().getOrder_pay().equals("0")){
                        relative_payment_time.setVisibility(View.VISIBLE);
                        tv_payment_time.setText(Utils.timedate(orderDetilsBean.getTime().getOrder_pay()));
                    }
                    if(!orderDetilsBean.getTime().getShipping().equals("0")){
                        relative_delivery_time.setVisibility(View.VISIBLE);
                        tv_order_delivery_time.setText(Utils.timedate(orderDetilsBean.getTime().getShipping()));
                    }
                    if(!orderDetilsBean.getTime().getOrder_finish().equals("0")){
                        relative_deal_time.setVisibility(View.VISIBLE);
                        tv_deal_time.setText(Utils.timedate(orderDetilsBean.getTime().getOrder_finish()));
                    }
                    if(!orderDetilsBean.getTime().getOrder_pay_beg().equals("0")){
                        relative_payment_time.setVisibility(View.GONE);
                        relative_firstphase_time.setVisibility(View.VISIBLE);
                        tv_firstphase_time.setText(Utils.timedate(orderDetilsBean.getTime().getOrder_pay_beg()));
                    }
                    if(!orderDetilsBean.getTime().getOrder_pay_end().equals("0")){
                        relative_payment_time.setVisibility(View.GONE);
                        relative_second_phase_time.setVisibility(View.VISIBLE);
                        tv_second_phase_time.setText(Utils.timedate(orderDetilsBean.getTime().getOrder_pay_end()));
                    }

                    tv_orderdetail_name.setText(orderDetilsBean.getAddress().getTrue_name());
                    tv_orderdetail_phone.setText(orderDetilsBean.getAddress().getMob_phone());
                    tv_orderdetail_addres.setText(orderDetilsBean.getAddress().getArea_info()+orderDetilsBean.getAddress().getAddress());
                    tv_freight_always.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_other_total())));
                    tv_total_amount_goods.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_goods_total())));
                    tv_total_amount_order.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_amount())));
                    tv_sourcing_company.setText(orderDetilsBean.getOther().getBuy_goods_name());

//                    //判断交易方式
//                    if(orderDetilsBean.getOther().getBuy_goods_name().equals("个人")){
//                        tv_trade_way.setVisibility(View.GONE);
//                        tv_orderdetils_trade_way.setVisibility(View.GONE);
//                    }

                    tv_orderdetils_pay_currency.setText(orderDetilsBean.getGoods().getOrder_main_pay_currency_name());
                    //判断有无审批
                    if(!orderDetilsBean.getOther().getCompany_id().equals("0")){
                        relative_approval_information.setVisibility(View.VISIBLE);
                        if(orderDetilsBean.getOther().getApprove_order_status().equals("0")){
                            tv_approval_information.setText(getString(R.string.approval_information)+getString(R.string.approval_pending));
                        }else if(orderDetilsBean.getOther().getApprove_order_status().equals("1")){
                            tv_approval_information.setText(getString(R.string.approval_information)+getString(R.string.approval_wite));
                        }else if(orderDetilsBean.getOther().getApprove_order_status().equals("2")){
                            tv_approval_information.setText(getString(R.string.approval_information)+getString(R.string.approval_through));
                        }else if(orderDetilsBean.getOther().getApprove_order_status().equals("3")){
                            tv_approval_information.setText(getString(R.string.approval_information)+getString(R.string.approval_refused));
                        }else if(orderDetilsBean.getOther().getApprove_order_status().equals("4")){
                            tv_approval_information.setText(getString(R.string.approval_information)+getString(R.string.approval_withdrawn));
                        }
                    }

                    //判断有无物流信息
                    if(orderDetilsBean.getLogistics()!=null){
                        relative_confirmorder_logistics.setVisibility(View.VISIBLE);
                        tv_logistics_name.setText(orderDetilsBean.getLogistics().getText());
                        tv_logistics_time.setText(Utils.timedate(orderDetilsBean.getLogistics().getTime()));
                    }


                    //支付类型
                    if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("1")){
                        tv_orderdetils_pay_way.setText(getString(R.string.bank_transfer));
                    }else if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("2")){
                        tv_orderdetils_pay_way.setText(getString(R.string.cheque_account));
                    }else if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("3")){
                        tv_orderdetils_pay_way.setText(getString(R.string.online_transfer));
//                        tv_pay_currency.setVisibility(View.GONE);
//                        tv_orderdetils_pay_currency.setVisibility(View.GONE);
                    }
                    //是否分阶段付款
                    if(orderDetilsBean.getGoods().getOrder_main_deal_active().equals("1")){
                        tv_orderdetils_trade_way.setText(getString(R.string.full_payment));
                        amount_total=orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                                + Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_amount())*
                                Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate()));
                    }else if(orderDetilsBean.getGoods().getOrder_main_deal_active().equals("2")){
                        if(orderDetilsBean.getGoods().getOrder_main_state().equals("2010")){
                            amount_total=orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                                    +Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_one())*
                                    Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate()));
                        }else if(orderDetilsBean.getGoods().getOrder_main_state().equals("2011")){
                            amount_total=orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                                    +Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_two())*
                                    Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate()));
                        }
                        tv_orderdetils_trade_way.setText(getString(R.string.credit_payment));
                        linear_orderdetils_stage_payment.setVisibility(View.VISIBLE);
                        linear_phase.setVisibility(View.VISIBLE);
                        relative_down_payment.setVisibility(View.VISIBLE);
                        relative_phase_payment.setVisibility(View.VISIBLE);
                        if(Double.parseDouble(orderDetilsBean.getGoods().getSupplement_amount())>0){
                            if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("3")){
                                tv_first_phase_supplement.setText(getString(R.string.order_detils_first_detils_tow));
                            }else {
                                tv_first_phase_supplement.setText(getString(R.string.order_detils_first_detils));
                            }
                           tv_added_new.setVisibility(View.VISIBLE);
                           tv_order_added_new.setVisibility(View.VISIBLE);
                           tv_order_added_new.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getSupplement_amount())));
                        }else {
                            if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("3")){
                                tv_first_phase_supplement.setText(getString(R.string.order_detils_first_detils_new_tow));
                                secon_phase_price.setText(getString(R.string.order_detils_second_detils_tow));
                            }else {
                                tv_first_phase_supplement.setText(getString(R.string.order_detils_first_detils_new));
                                secon_phase_price.setText(getString(R.string.order_detils_second_detils));
                            }
                        }
                        tv_order_down_payment.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_beg())));
                        tv_order_tail_section.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_end())));
                        tv_order_first_phase_payment.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_one())));
                        tv_order_second_phase_payment.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_two())));
                        tv_first_phase_state.setText(orderDetilsBean.getOther().getFirst_status());
                        tv_secon_phase_state.setText(orderDetilsBean.getOther().getSecond_status());
                        if(orderDetilsBean.getOther().getFirst_status().equals("待付款")){
                            tv_first_phase_state.setTextColor(getResources().getColor(R.color.orange_1));
                        }else {
                            tv_first_phase_state.setTextColor(getResources().getColor(R.color.grey_4));
                        }
                        if(orderDetilsBean.getOther().getSecond_status().equals("待付款")){
                            tv_secon_phase_state.setTextColor(getResources().getColor(R.color.orange_1));
                        }else {
                            tv_secon_phase_state.setTextColor(getResources().getColor(R.color.grey_4));
                        }

                        tv_first_phase_price.setText(orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                                +Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_one())*
                                Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate())));
                        tv_secon_phase_price.setText(orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                                +Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_two())*
                                Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate())));

                    }

                    if(orderDetilsBean.getGoods().getOrder_main_pay_currency_name()!=null && orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()!=null){
                        tv_orderdetils_amount_total.setText(orderDetilsBean.getGoods().getOrder_main_pay_currency_symbol()
                                + Utils.getDisplayMoney(Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_amount())*
                                Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_currency_rate())));
                    }

                    //交易汇率
                    tv_trading_currency.setText("1:"+orderDetilsBean.getGoods().getOrder_main_pay_currency_rate());
                    buttonState();
                    break;
                case 2:
                    error_retry_view.setVisibility(View.VISIBLE);
                    scrollview.setVisibility(View.GONE);
                    break;

            }

        }
    };
    @Override
    protected int getLayout() {
        return R.layout.activity_orderdetils;
    }

    @Override
    protected void initView() {
        mBadgeView = new BadgeView(this,iv_right,10,4);
        tv_title.setText(R.string.order_detils);
        iv_right.setVisibility(View.VISIBLE);
        bt_load_again.setVisibility(View.VISIBLE);
        order_main_id=getIntent().getStringExtra("order_main_id");
        if(!TextUtils.isEmpty(order_main_id)){
            getData();
        }
        click();
        EventBus.getDefault().register(this);
        showUnreadMsgNum();

    }

    private void click(){
        iv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.Companion.getInstance().isLogin()){
                    goToActivity(MessageActivity.class);
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });
        customExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
        btn_cance_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.createTwoBtnDialog(OrderDetilsActivity.this,
                        getString(R.string.are_you_sure_cancel_order),
                        getString(R.string.dialog_confirm),
                        getString(R.string.dialog_cancel),
                        new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(Dialog dialog) {
                                cancelOrder();
                            }
                        },null,false,true);

            }
        });
        btn_to_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getalipay();
            }
        });
        btn_transfer_information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("order_id",order_main_id);
                bundle.putString("order_detils", JSONObject.toJSONString(orderDetilsBean));
                goToActivity(BankTransferActivity.class,bundle);
            }
        });
        btn_upload_cheque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("order_id",order_main_id);
                bundle.putString("order_detils", JSONObject.toJSONString(orderDetilsBean));
                goToActivity(ChequeTransferActivity.class,bundle);
            }
        });
        relative_approval_information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(company_id) && !TextUtils.isEmpty(approve_order_id)){
                    Bundle bundle=new Bundle();
                    bundle.putString(ApprovalDetailEnterpriseActivity.APPROVE_ORDER_ID,approve_order_id);
                    bundle.putString(Constant.COMPANY_ID,company_id);
                    goToActivity(ApprovalDetailEnterpriseActivity.class,bundle);
                }
            }
        });

        relative_confirmorder_logistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString(LogisticsActivity.ORDER_ID,order_main_id);
                goToActivity(LogisticsActivity.class,bundle);
            }
        });

        //收票人
        relative_receive_tickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = LayoutInflater.from(OrderDetilsActivity.this).inflate(R.layout.dialog_take_ticket_member,null);
                if(popupCompany==null){
                    popupCompany = new CommonPopupWindow.Builder(OrderDetilsActivity.this)
                            .setView(view)
                            .setWidthAndHeight(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                            .setBackGroundLevel(0.5f)//取值范围0.0f-1.0f 值越小越暗
                            .create();
                    initpopu(view);
                }
                popupCompany.showAtLocation(view, Gravity.CENTER, 0, 0);
                Utils.SetWindBg(OrderDetilsActivity.this);
            }
        });

        customExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    Bundle bundle=new Bundle();
                    bundle.putString("goods_id",orderDetilsBean.getGoods().getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getGoods_id());
                    goToActivity(GoodsActivity.class,bundle);
                return false;
            }
        });

        tv_order_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //复制到剪切板
                Utils.getCopy(OrderDetilsActivity.this,tv_order_number.getText().toString());
                tv_order_copy.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DialogUtils.createTipAllTextDialog(OrderDetilsActivity.this,getString(R.string.copy_su));
                    }
                },1000);

            }
        });
        tv_added_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double company_remain_money=Double.parseDouble(orderDetilsBean.getGoods().getOrder_main_pay_end())
                        -Double.parseDouble(orderDetilsBean.getGoods().getSupplement_amount());
                DialogUtils.createOneBtnDialog(OrderDetilsActivity.this, "（"+getString(R.string.order_line_credit)+"¥"
                        +company_remain_money+getString(R.string.order_insufficient_credit)
                        +"¥"+orderDetilsBean.getGoods().getOrder_main_pay_end()+
                        getString(R.string.order_supplement)+"¥"+orderDetilsBean.getGoods().getSupplement_amount()+"）", getString(R.string.know), new DialogUtils.OnLeftBtnListener() {
                    @Override
                    public void setOnLeftListener(Dialog dialog) {

                    }
                },false,false);
            }
        });
    }
    //初始化popupwindes
    private void initpopu(View view){
        ImageView image_close=(ImageView) view.findViewById(R.id.iv_close);
        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupCompany.dismiss();
            }
        });
        LinearLayout ll_take_ticket_members=(LinearLayout) view.findViewById(R.id.ll_take_ticket_members);
        final ImageView imageback=(ImageView) view.findViewById(R.id.iv_close);
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupCompany.dismiss();
            }
        });
        for(int i=0;i<orderDetilsBean.getPay_info().getCollector().size();i++){
            View itemview=View.inflate(OrderDetilsActivity.this, R.layout.item_take_ticket_member, null);
            TextView tv_stage=(TextView) itemview.findViewById(R.id.tv_stage);
            tv_stage.setText(orderDetilsBean.getPay_info().getCollector().get(i).getPay_remark());
            TextView tv_ticket_member_name=(TextView) itemview.findViewById(R.id.tv_ticket_member_name);
            tv_ticket_member_name.setText(orderDetilsBean.getPay_info().getCollector().get(i).getName());
            TextView tv_contact_phone=(TextView) itemview.findViewById(R.id.tv_contact_phone);
            tv_contact_phone.setText(orderDetilsBean.getPay_info().getCollector().get(i).getMobile());
            TextView tv_staff_id=(TextView) itemview.findViewById(R.id.tv_staff_id);
            tv_staff_id.setText(orderDetilsBean.getPay_info().getCollector().get(i).getSerial_number());
            ImageView iv_member_icon=(ImageView) itemview.findViewById(R.id.iv_member_icon);
            Glide.with(OrderDetilsActivity.this)
                    .load(orderDetilsBean.getPay_info().getCollector().get(i).getAvatar())
                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                    .into(iv_member_icon);
            ll_take_ticket_members.addView(itemview,i);
        }
    }
    //获取订单数据
    private void getData(){
        DialogUtils.createTipAllLoadDialog(OrderDetilsActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ORDER_DETAIL())
                .addParam("order_main_id",order_main_id)
                .withPOST(new NetCallBack<OrderDetilsBean>() {
                    @NotNull
                    @Override
                    public Class<OrderDetilsBean> getRealType() {
                        return OrderDetilsBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        handler.sendEmptyMessage(2);
                    }

                    @Override
                    public void onSuccess(@NonNull OrderDetilsBean data) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        orderDetilsBean=null;
                        orderDetilsBean=data;
                        company_id=orderDetilsBean.getOther().getCompany_id();
                        approve_order_id=orderDetilsBean.getOther().getApprove_order_id();
                        //商品清单
                        orderDetilsAdapter=new OrderDetilsAdapter(OrderDetilsActivity.this,orderDetilsBean.getGoods());
                        customExpandableListView.setAdapter(orderDetilsAdapter);
                        for (int i = 0; i < orderDetilsBean.getGoods().getOrder_list().size(); i++) {
                            customExpandableListView.expandGroup(i); //关键步骤4:初始化，将ExpandableListView以展开的方式显示
                        }
                        handler.sendEmptyMessage(1);
                    }
                },false);
    }

    //取消订单
    private void cancelOrder(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ORDER_CANCEL())
                .addParam("order_main_id",order_main_id)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        ToastU.INSTANCE.showToast(getString(R.string.not_network));
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_ORDER_LIST));
                        if(isFinishing()){
                            return;
                        }
                        tv_added_new.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogUtils.createTipImageAndTextDialog(OrderDetilsActivity.this,
                                        getString(R.string.order_cancel_succeful),
                                        R.mipmap.png_icon_popup_ok);
                            }
                        },1000);
                        getData();
                    }
                },false);
    }

    //判断支付方式：1.银行支付，2.支票支付，3.在线支付
    private void buttonState(){
        Log.i("info","--------order_state"+orderDetilsBean.getGoods().getOrder_main_state());
        if(orderDetilsBean.getGoods().getOrder_main_state().equals("1010") || orderDetilsBean.getGoods().getOrder_main_state().equals("2010")){
            if(orderDetilsBean.getOther().isIs_cancel_auth() || orderDetilsBean.getOther().isIs_pay_auth()){
                relative_btn.setVisibility(View.VISIBLE);
            }
            if(orderDetilsBean.getOther().isIs_cancel_auth()){
                btn_cance_order.setVisibility(View.VISIBLE);
            }
            order_remaining_time.setVisibility(View.VISIBLE);
            if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("1")){
                if(orderDetilsBean.getOther().isIs_pay_auth()){
                    btn_transfer_information.setVisibility(View.VISIBLE);
                }
            }else if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("2")){
                if(orderDetilsBean.getOther().isIs_pay_auth()){
                    btn_upload_cheque.setVisibility(View.VISIBLE);
                }
                relative_cheque_payable.setVisibility(View.VISIBLE);
                relative_cheque_address.setVisibility(View.VISIBLE);
            }else if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("3")){
                if(orderDetilsBean.getOther().isIs_pay_auth()){
                    btn_to_pay.setVisibility(View.VISIBLE);
                }
            }
        }else if(orderDetilsBean.getGoods().getOrder_main_state().equals("2001")){
            if(orderDetilsBean.getOther().isIs_cancel_auth()){
                relative_btn.setVisibility(View.VISIBLE);
                btn_cance_order.setVisibility(View.VISIBLE);
            }
        }else if(orderDetilsBean.getGoods().getOrder_main_state().equals("2011")){
            if(orderDetilsBean.getOther().isIs_pay_auth()){
                relative_btn.setVisibility(View.VISIBLE);
            }
            order_remaining_time.setVisibility(View.VISIBLE);
            if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("1")){
                if(orderDetilsBean.getOther().isIs_pay_auth()){
                    btn_transfer_information.setVisibility(View.VISIBLE);
                }
            }else if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("2")){
                if(orderDetilsBean.getOther().isIs_pay_auth()){
                    btn_upload_cheque.setVisibility(View.VISIBLE);
                }
                relative_cheque_payable.setVisibility(View.VISIBLE);
                relative_cheque_address.setVisibility(View.VISIBLE);
            }else if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("3")){
                if(orderDetilsBean.getOther().isIs_pay_auth()){
                    btn_to_pay.setVisibility(View.VISIBLE);
                }
            }
        }else {
            relative_btn.setVisibility(View.GONE);
        }

        if(orderDetilsBean.getGoods().getOrder_main_pay_active().equals("2") && orderDetilsBean.getPay_info().getCollector().size()!=0){
            relative_receive_tickets.setVisibility(View.VISIBLE);
        }

    }


    private void getalipay(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ORDER_PAY())
                .addParam("pay_sn",orderDetilsBean.getOther().getOrder_status())
                .addParam("payment_code","alipay")
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        ToastU.INSTANCE.showToast(getString(R.string.no_network));
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        if(isFinishing()){
                            return;
                        }
                        try {
                            org.json.JSONObject jsonObject=new org.json.JSONObject(s);
                            String orderstr=jsonObject.getString("alipay");
                            //orderInfo=new String(Base64.decode(orderout, Base64.DEFAULT));
                            Bundle bundle=new Bundle();
                            bundle.putString("amount_total",amount_total);
                            bundle.putString(PayTreasureActivity.ORDERSTR,orderstr);
                            goToActivity(PayTreasureActivity.class,bundle);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.ORDERDETILS)){
            getData();
        }

        if(messageEvent.getMessage().equals(MessageEvent.UNREAD_MSG_NUM)){
            showUnreadMsgNum();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    private void showUnreadMsgNum() {
        if(Login.Companion.getInstance().isLogin()){
            mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
        }
    }

    @Override
    public void onBackPressed() {
        if(KyyApp.activitynum==1){
            goToActivity(MainActivity.class);
        }
        super.onBackPressed();
    }
}
