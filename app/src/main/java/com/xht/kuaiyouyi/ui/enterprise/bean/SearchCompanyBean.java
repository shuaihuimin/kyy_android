package com.xht.kuaiyouyi.ui.enterprise.bean;

import java.util.List;

public class SearchCompanyBean {

    /**
     * company : [{"company_id":"69","company_name":"新海通科技有限公司（团）","company_logo":"","is_join":false},{"company_id":"60","company_name":"新海通科技有限公司","company_logo":"http://192.168.0.2/data/upload/shop/company/logo_5b0288b517acf.jpeg","is_join":true}]
     * kyy : eyJQIjoxLCJLIjoia3l5XzAxMyIsIk8iOiJcdTY1YjAifQ==
     */

    private String kyy;
    private List<CompanyBean> company;

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public List<CompanyBean> getCompany() {
        return company;
    }

    public void setCompany(List<CompanyBean> company) {
        this.company = company;
    }

    public static class CompanyBean {
        /**
         * company_id : 69
         * company_name : 新海通科技有限公司（团）
         * company_logo :
         * is_join : false
         */

        private String company_id;
        private String company_name;
        private String company_logo;
        private boolean is_join;

        public String getCompany_id() {
            return company_id;
        }

        public void setCompany_id(String company_id) {
            this.company_id = company_id;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public String getCompany_logo() {
            return company_logo;
        }

        public void setCompany_logo(String company_logo) {
            this.company_logo = company_logo;
        }

        public boolean isIs_join() {
            return is_join;
        }

        public void setIs_join(boolean is_join) {
            this.is_join = is_join;
        }
    }
}
