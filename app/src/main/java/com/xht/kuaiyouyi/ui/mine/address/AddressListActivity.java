package com.xht.kuaiyouyi.ui.mine.address;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class AddressListActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tl_tabs)
    TabLayout tl_tabs;
    @BindView(R.id.vp_fragment)
    ViewPager vp_fragment;


    public static int TYPE_RECEIVE = 1;
    public static int TYPE_CHECK = 2;
    private List<AddressListFragment> mFragments;
    private int[] mTabTitles = new int[]{R.string.receive_address, R.string.check_address};

    @Override
    protected int getLayout() {
        return R.layout.activity_address_list;
    }

    @Override
    protected void initView() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mFragments = new ArrayList<>();
        AddressListFragment receiveFragment = new AddressListFragment();
        receiveFragment.setType(TYPE_RECEIVE);
        AddressListFragment checkFragment = new AddressListFragment();
        checkFragment.setType(TYPE_CHECK);
        mFragments.add(receiveFragment);
        mFragments.add(checkFragment);
        MyAdapter adapter = new MyAdapter(getSupportFragmentManager());
        vp_fragment.setAdapter(adapter);
        tl_tabs.setupWithViewPager(vp_fragment);

        vp_fragment.setCurrentItem(0);
    }


    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public AddressListFragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        //重写这个方法，将设置每个Tab的标题
        @Override
        public CharSequence getPageTitle(int position) {
            return getString(mTabTitles[position]);
        }
    }

}
