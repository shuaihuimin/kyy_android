package com.xht.kuaiyouyi.ui.enterprise.bean;

public class CreditDetailBean {

    /**
     * info : {"company_total_money":10000000,"company_use_money":2548101.73,"company_wait_money":7451898.27,"company_pay_money":0}
     */

    private InfoBean info;

    public InfoBean getInfo() {
        return info;
    }

    public void setInfo(InfoBean info) {
        this.info = info;
    }

    public static class InfoBean {
        /**
         * company_total_money : 10000000
         * company_use_money : 2548101.73
         * company_wait_money : 7451898.27
         * company_pay_money : 0
         */

        private double company_total_money;
        private double company_use_money;
        private double company_wait_money;
        private double company_pay_money;

        public double getCompany_total_money() {
            return company_total_money;
        }

        public void setCompany_total_money(double company_total_money) {
            this.company_total_money = company_total_money;
        }

        public double getCompany_use_money() {
            return company_use_money;
        }

        public void setCompany_use_money(double company_use_money) {
            this.company_use_money = company_use_money;
        }

        public double getCompany_wait_money() {
            return company_wait_money;
        }

        public void setCompany_wait_money(double company_wait_money) {
            this.company_wait_money = company_wait_money;
        }

        public double getCompany_pay_money() {
            return company_pay_money;
        }

        public void setCompany_pay_money(double company_pay_money) {
            this.company_pay_money = company_pay_money;
        }
    }
}
