package com.xht.kuaiyouyi.ui.enterprise.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.enterprise.Constant;
import com.xht.kuaiyouyi.ui.enterprise.activity.ApprovalDetailEnterpriseActivity;
import com.xht.kuaiyouyi.ui.enterprise.adapter.ApprovalListAdapter;
import com.xht.kuaiyouyi.ui.enterprise.bean.EnterpriseApprovalBean;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.ToastU;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/**
 * 待我审批列表的都是小红点提醒，表示待办任务
 * 抄送我的有已读和未读状态
 */
public class ApprovalListFragment extends BaseFragment {
    @BindView(R.id.lv_content)
    ListView lv_content;
    @BindView(R.id.srl_loadmore)
    SmartRefreshLayout srl_loadmore;
    @BindView(R.id.rl_empty_approval_list)
    RelativeLayout rl_empty_approval_list;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;

    private int mApproveId = 0;

    public static final int APPROVAL_WAIT = 1;//待我审批
    public static final int APPROVALED = 2;//已审批
    public static final int APPROVAL_START = 3;//我发起
    public static final int APPROVAL_COPY_TO = 4;//抄送我的

    private EnterpriseApprovalBean mEnterpriseApprovalBean;
    public String mKyy="";
    private ApprovalListAdapter mAdapter;

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mKyy="";
        requestApprovalList();
        lv_content.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String approve_order_id = mEnterpriseApprovalBean.getResult().get(position).getApprove_order_id()+"";
                Intent intent = new Intent(getContext(),ApprovalDetailEnterpriseActivity.class);
                intent.putExtra(ApprovalDetailEnterpriseActivity.APPROVE_ORDER_ID,approve_order_id);
                intent.putExtra(Constant.COMPANY_ID,Login.Companion.getInstance().getEnterprise_company_id());
                startActivity(intent);
                if(mApproveId==APPROVAL_COPY_TO){
                    if(mEnterpriseApprovalBean.getResult().get(position).isIs_read()==0){
                        mEnterpriseApprovalBean.getResult().get(position).setIs_read(1);
                        mAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
        srl_loadmore.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                requestApprovalList();
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mKyy="";
                requestApprovalList();
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_approval_list;
    }

    public void setId(int approveId) {
        mApproveId = approveId;
    }

    public void requestApprovalList() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_EP_APPROVAL_LIST())
                .addParam("company_id", Login.Companion.getInstance().getEnterprise_company_id())
                .addParam("approve_id", mApproveId)
                .withLoadPOST(new NetCallBack<EnterpriseApprovalBean>() {
                    @NotNull
                    @Override
                    public Class<EnterpriseApprovalBean> getRealType() {
                        return EnterpriseApprovalBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity() == null){
                            return;
                        }
                        if(TextUtils.isEmpty(mKyy)){
                            error_retry_view.setVisibility(View.VISIBLE);
                            rl_empty_approval_list.setVisibility(View.GONE);
                        }else {
                            ToastU.INSTANCE.showToast(errCode + "--" + getString(R.string.not_network));
                        }
                        if(srl_loadmore.getState() == RefreshState.Loading){
                            srl_loadmore.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull EnterpriseApprovalBean enterpriseApprovalBean) {
                        if(getActivity() == null){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        if(TextUtils.isEmpty(mKyy)){
                            //刷新
                            srl_loadmore.setNoMoreData(false);
                            if(enterpriseApprovalBean.getResult()!=null){
                                if(enterpriseApprovalBean.getResult().size()==0){
                                    rl_empty_approval_list.setVisibility(View.VISIBLE);
                                }else {
                                    rl_empty_approval_list.setVisibility(View.GONE);
                                }
                                mEnterpriseApprovalBean = enterpriseApprovalBean;
                                if(mApproveId==APPROVAL_WAIT){
                                    mAdapter = new ApprovalListAdapter(getContext(),enterpriseApprovalBean,true);
                                }else {
                                    mAdapter = new ApprovalListAdapter(getContext(),enterpriseApprovalBean,false);
                                }

                                lv_content.setAdapter(mAdapter);
                                mKyy=mEnterpriseApprovalBean.getKyy();
                                srl_loadmore.setEnableLoadMore(true);
                            }
                        }else {
                            srl_loadmore.finishLoadMore();
                            if(enterpriseApprovalBean.getResult()!=null&&enterpriseApprovalBean.getResult().size()>0){
                                mEnterpriseApprovalBean.getResult().addAll(enterpriseApprovalBean.getResult());
                                mAdapter.notifyDataSetChanged();
                                mKyy=enterpriseApprovalBean.getKyy();
                            }else {
                                srl_loadmore.setNoMoreData(true);
                                srl_loadmore.setEnableFooterFollowWhenLoadFinished(true);
                            }
                        }
                    }
                }, false,mKyy);
    }

}
