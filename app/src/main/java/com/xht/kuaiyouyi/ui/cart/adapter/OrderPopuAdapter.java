package com.xht.kuaiyouyi.ui.cart.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.cart.entity.OrderChooseBean;

import java.util.List;


public class OrderPopuAdapter extends BaseAdapter {
    private Context mContext;
    private List<OrderChooseBean> list;

    public OrderPopuAdapter(Context mContext, List<OrderChooseBean> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.item_procurement_method, null);
            viewHolder = new ViewHolder();
            viewHolder.iv_selected = convertView.findViewById(R.id.iv_selected);
            viewHolder.tv_company_name = convertView.findViewById(R.id.tv_company_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tv_company_name.setText(list.get(position).getName());
        if(list.get(position).isSelect()){
            viewHolder.iv_selected.setImageResource(R.mipmap.pop_icon_tick);
        }else {
            viewHolder.iv_selected.setImageResource(android.R.color.transparent);
        }
        return convertView;
    }

    private class ViewHolder {
        TextView tv_company_name;
        ImageView iv_selected;
    }
}
