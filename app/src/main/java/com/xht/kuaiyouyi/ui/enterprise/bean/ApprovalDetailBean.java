package com.xht.kuaiyouyi.ui.enterprise.bean;

import java.util.List;

public class ApprovalDetailBean {


    /**
     * approve_order_info : {"id":"495","approve_num":"2018081520120232687","company_id":"91","expect_delivery_time":"1535385600","status":"0","apply_reason":"个非官方个","is_creator":true,"is_approve":false,"member_truename":"兰博基尼","status_name":"兰博基尼、思维会签中","approve_id":"1491"}
     * goods_info : {"beginTime":{"day":4,"hour":23,"min":40,"sec":24},"order_main_deal_active":"信用支付","order_main_pay_active_name":"银行转账","order_main_pay_currency_name":"港币","order_main_pay_currency_symbol":"HK$","order_main_pay_currency_rate":"1.1393","order_main_goods_total":"63500.00","order_main_other_total":"0.00","order_main_amount":"63500.00","order_main_pay_one":"63500.00","order_main_pay_two":"0.00","supplement_amount":"0.00","order_main_state":"2001","order_main_state_name":"待付款","order_main_sn":"1000000000049601","order_list":[{"goods_id":"100002","goods_name":"劳力士Rolex MILGAUSS 116400GV-72400 自动机械钢带男表联保正品","goods_spec":null,"image_60_url":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627750479728_60.png","goods_price":"63200.00","goods_num":"1","goods_pay_price":"63200.00","freight_fee":0,"profit_rate":0,"customs_charges_fee":0},{"goods_id":"100362","goods_name":"【鼎力】自行走曲臂式GTBZ-AE","goods_spec":null,"image_60_url":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796293756348179_60.png","goods_price":"100.00","goods_num":"2","goods_pay_price":"200.00","freight_fee":0,"profit_rate":0,"customs_charges_fee":0},{"goods_id":"100477","goods_name":"惠普(HP) LaserJet Pro M1139 黑白激光多功能一体机 (打印 复印 扫描)","goods_spec":null,"image_60_url":"http://192.168.0.2/data/upload/shop/store/goods/22/22_05803917426286848_60.jpg","goods_price":"100.00","goods_num":"1","goods_pay_price":"100.00","freight_fee":0,"profit_rate":0,"customs_charges_fee":0}]}
     * flow_data : [{"approve_mode":0,"status":true,"names":"兰博基尼","status_name":"已同意","name":"兰博基尼","approve_time":"1534336247","approve_status":"2","approve_opinion":""},{"approve_mode":"1","status":false,"names":"兰博基尼、思维","status_name":"已同意","name":"兰博基尼","approve_time":"1534336285","approve_status":"2","approve_opinion":""},{"approve_mode":"1","status":false,"names":"兰博基尼、思维","status_name":"审批中","name":"思维","approve_time":"1534336247","approve_status":"1","approve_opinion":null},{"approve_mode":"2","status":false,"names":"兰博基尼、思维","status_name":"","name":"兰博基尼","approve_time":null,"approve_status":"0","approve_opinion":null},{"approve_mode":"2","status":false,"names":"兰博基尼、思维","status_name":"","name":"思维","approve_time":null,"approve_status":"0","approve_opinion":null}]
     * copy_to_member : ["兰博基尼"]
     */

    private ApproveOrderInfoBean approve_order_info;
    private GoodsInfoBean goods_info;
    private List<FlowDataBean> flow_data;
    private List<String> copy_to_member;

    public ApproveOrderInfoBean getApprove_order_info() {
        return approve_order_info;
    }

    public void setApprove_order_info(ApproveOrderInfoBean approve_order_info) {
        this.approve_order_info = approve_order_info;
    }

    public GoodsInfoBean getGoods_info() {
        return goods_info;
    }

    public void setGoods_info(GoodsInfoBean goods_info) {
        this.goods_info = goods_info;
    }

    public List<FlowDataBean> getFlow_data() {
        return flow_data;
    }

    public void setFlow_data(List<FlowDataBean> flow_data) {
        this.flow_data = flow_data;
    }

    public List<String> getCopy_to_member() {
        return copy_to_member;
    }

    public void setCopy_to_member(List<String> copy_to_member) {
        this.copy_to_member = copy_to_member;
    }

    public static class ApproveOrderInfoBean {
        /**
         * id : 495
         * approve_num : 2018081520120232687
         * company_id : 91
         * expect_delivery_time : 1535385600
         * status : 0
         * apply_reason : 个非官方个
         * is_creator : true
         * is_approve : false
         * member_truename : 兰博基尼
         * status_name : 兰博基尼、思维会签中
         * approve_id : 1491
         */

        private String approve_config_id;
        private String id;
        private String approve_num;
        private String company_id;
        private String expect_delivery_time;
        private int is_pay;
        private int is_goto_pay;
        private int status;
        private String apply_reason;
        private boolean is_creator;
        private boolean is_approve;
        private String member_truename;
        private String approve_creator;
        private String status_name;
        private String approve_id;
        private String update_time;

        public String getApprove_config_id() {
            return approve_config_id;
        }

        public void setApprove_config_id(String approve_config_id) {
            this.approve_config_id = approve_config_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getApprove_num() {
            return approve_num;
        }

        public void setApprove_num(String approve_num) {
            this.approve_num = approve_num;
        }

        public String getCompany_id() {
            return company_id;
        }

        public void setCompany_id(String company_id) {
            this.company_id = company_id;
        }

        public String getExpect_delivery_time() {
            return expect_delivery_time;
        }

        public void setExpect_delivery_time(String expect_delivery_time) {
            this.expect_delivery_time = expect_delivery_time;
        }

        public int getIs_pay() {
            return is_pay;
        }

        public void setIs_pay(int is_pay) {
            this.is_pay = is_pay;
        }

        public int getIs_goto_pay() {
            return is_goto_pay;
        }

        public void setIs_goto_pay(int is_goto_pay) {
            this.is_goto_pay = is_goto_pay;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getApply_reason() {
            return apply_reason;
        }

        public void setApply_reason(String apply_reason) {
            this.apply_reason = apply_reason;
        }

        public boolean isIs_creator() {
            return is_creator;
        }

        public void setIs_creator(boolean is_creator) {
            this.is_creator = is_creator;
        }

        public boolean isIs_approve() {
            return is_approve;
        }

        public void setIs_approve(boolean is_approve) {
            this.is_approve = is_approve;
        }

        public String getMember_truename() {
            return member_truename;
        }

        public void setMember_truename(String member_truename) {
            this.member_truename = member_truename;
        }

        public String getApprove_creator() {
            return approve_creator;
        }

        public void setApprove_creator(String approve_creator) {
            this.approve_creator = approve_creator;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getApprove_id() {
            return approve_id;
        }

        public void setApprove_id(String approve_id) {
            this.approve_id = approve_id;
        }

        public String getUpdate_time() {
            return update_time;
        }

        public void setUpdate_time(String update_time) {
            this.update_time = update_time;
        }
    }

    public static class GoodsInfoBean {
        /**
         * beginTime : {"day":4,"hour":23,"min":40,"sec":24}
         * order_main_deal_active : 信用支付
         * order_main_pay_active_name : 银行转账
         * order_main_pay_currency_name : 港币
         * order_main_pay_currency_symbol : HK$
         * order_main_pay_currency_rate : 1.1393
         * order_main_goods_total : 63500.00
         * order_main_other_total : 0.00
         * order_main_amount : 63500.00
         * order_main_pay_one : 63500.00
         * order_main_pay_two : 0.00
         * supplement_amount : 0.00
         * order_main_state : 2001
         * order_main_state_name : 待付款
         * order_main_sn : 1000000000049601
         * order_list : [{"goods_id":"100002","goods_name":"劳力士Rolex MILGAUSS 116400GV-72400 自动机械钢带男表联保正品","goods_spec":null,"image_60_url":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627750479728_60.png","goods_price":"63200.00","goods_num":"1","goods_pay_price":"63200.00","freight_fee":0,"profit_rate":0,"customs_charges_fee":0},{"goods_id":"100362","goods_name":"【鼎力】自行走曲臂式GTBZ-AE","goods_spec":null,"image_60_url":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796293756348179_60.png","goods_price":"100.00","goods_num":"2","goods_pay_price":"200.00","freight_fee":0,"profit_rate":0,"customs_charges_fee":0},{"goods_id":"100477","goods_name":"惠普(HP) LaserJet Pro M1139 黑白激光多功能一体机 (打印 复印 扫描)","goods_spec":null,"image_60_url":"http://192.168.0.2/data/upload/shop/store/goods/22/22_05803917426286848_60.jpg","goods_price":"100.00","goods_num":"1","goods_pay_price":"100.00","freight_fee":0,"profit_rate":0,"customs_charges_fee":0}]
         */

        private double company_count_money;
        private double company_remain_money;
        private BeginTimeBean beginTime;
        private int order_main_deal_active;
        private int order_main_pay_active;
        private String order_main_pay_currency_name;
        private String order_main_pay_currency_symbol;
        private double order_main_pay_currency_rate;
        private double order_main_goods_total;
        private double order_main_other_total;
        private double order_main_amount;
        private double order_main_pay_first;
        private double order_main_pay_second;
        private double order_main_pay_one;
        private double order_main_pay_two;
        private double supplement_amount;
        private int order_main_state;
        private String order_main_state_name;
        private String order_main_sn;
        private String order_main_id;
        private List<OrderListBean> order_list;

        public double getCompany_count_money() {
            return company_count_money;
        }

        public void setCompany_count_money(double company_count_money) {
            this.company_count_money = company_count_money;
        }

        public double getCompany_remain_money() {
            return company_remain_money;
        }

        public void setCompany_remain_money(double company_remain_money) {
            this.company_remain_money = company_remain_money;
        }

        public BeginTimeBean getBeginTime() {
            return beginTime;
        }

        public void setBeginTime(BeginTimeBean beginTime) {
            this.beginTime = beginTime;
        }

        public int getOrder_main_deal_active() {
            return order_main_deal_active;
        }

        public void setOrder_main_deal_active(int order_main_deal_active) {
            this.order_main_deal_active = order_main_deal_active;
        }

        public int getOrder_main_pay_active() {
            return order_main_pay_active;
        }

        public void setOrder_main_pay_active(int order_main_pay_active) {
            this.order_main_pay_active = order_main_pay_active;
        }

        public String getOrder_main_pay_currency_name() {
            return order_main_pay_currency_name;
        }

        public void setOrder_main_pay_currency_name(String order_main_pay_currency_name) {
            this.order_main_pay_currency_name = order_main_pay_currency_name;
        }

        public String getOrder_main_pay_currency_symbol() {
            return order_main_pay_currency_symbol;
        }

        public void setOrder_main_pay_currency_symbol(String order_main_pay_currency_symbol) {
            this.order_main_pay_currency_symbol = order_main_pay_currency_symbol;
        }

        public double getOrder_main_pay_currency_rate() {
            return order_main_pay_currency_rate;
        }

        public void setOrder_main_pay_currency_rate(double order_main_pay_currency_rate) {
            this.order_main_pay_currency_rate = order_main_pay_currency_rate;
        }

        public double getOrder_main_goods_total() {
            return order_main_goods_total;
        }

        public void setOrder_main_goods_total(double order_main_goods_total) {
            this.order_main_goods_total = order_main_goods_total;
        }

        public double getOrder_main_other_total() {
            return order_main_other_total;
        }

        public void setOrder_main_other_total(double order_main_other_total) {
            this.order_main_other_total = order_main_other_total;
        }

        public double getOrder_main_amount() {
            return order_main_amount;
        }

        public void setOrder_main_amount(double order_main_amount) {
            this.order_main_amount = order_main_amount;
        }

        public double getOrder_main_pay_first() {
            return order_main_pay_first;
        }

        public void setOrder_main_pay_first(double order_main_pay_first) {
            this.order_main_pay_first = order_main_pay_first;
        }

        public double getOrder_main_pay_second() {
            return order_main_pay_second;
        }

        public void setOrder_main_pay_second(double order_main_pay_second) {
            this.order_main_pay_second = order_main_pay_second;
        }

        public double getOrder_main_pay_one() {
            return order_main_pay_one;
        }

        public void setOrder_main_pay_one(double order_main_pay_one) {
            this.order_main_pay_one = order_main_pay_one;
        }

        public double getOrder_main_pay_two() {
            return order_main_pay_two;
        }

        public void setOrder_main_pay_two(double order_main_pay_two) {
            this.order_main_pay_two = order_main_pay_two;
        }

        public double getSupplement_amount() {
            return supplement_amount;
        }

        public void setSupplement_amount(double supplement_amount) {
            this.supplement_amount = supplement_amount;
        }

        public int getOrder_main_state() {
            return order_main_state;
        }

        public void setOrder_main_state(int order_main_state) {
            this.order_main_state = order_main_state;
        }

        public String getOrder_main_state_name() {
            return order_main_state_name;
        }

        public void setOrder_main_state_name(String order_main_state_name) {
            this.order_main_state_name = order_main_state_name;
        }

        public String getOrder_main_sn() {
            return order_main_sn;
        }

        public void setOrder_main_sn(String order_main_sn) {
            this.order_main_sn = order_main_sn;
        }

        public String getOrder_main_id() {
            return order_main_id;
        }

        public void setOrder_main_id(String order_main_id) {
            this.order_main_id = order_main_id;
        }

        public List<OrderListBean> getOrder_list() {
            return order_list;
        }

        public void setOrder_list(List<OrderListBean> order_list) {
            this.order_list = order_list;
        }

        public static class BeginTimeBean {
            /**
             * day : 4
             * hour : 23
             * min : 40
             * sec : 24
             */

            private int day;
            private int hour;
            private int min;
            private int sec;

            public int getDay() {
                return day;
            }

            public void setDay(int day) {
                this.day = day;
            }

            public int getHour() {
                return hour;
            }

            public void setHour(int hour) {
                this.hour = hour;
            }

            public int getMin() {
                return min;
            }

            public void setMin(int min) {
                this.min = min;
            }

            public int getSec() {
                return sec;
            }

            public void setSec(int sec) {
                this.sec = sec;
            }
        }

        public static class OrderListBean {
            /**
             * goods_id : 100002
             * goods_name : 劳力士Rolex MILGAUSS 116400GV-72400 自动机械钢带男表联保正品
             * goods_spec : null
             * image_60_url : http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627750479728_60.png
             * goods_price : 63200.00
             * goods_num : 1
             * goods_pay_price : 63200.00
             * freight_fee : 0
             * profit_rate : 0
             * customs_charges_fee : 0
             */

            private String goods_id;
            private String goods_name;
            private String goods_spec;
            private String image_60_url;
            private double goods_price;
            private String goods_num;
            private double goods_pay_price;
            private double freight_fee;
            private double profit_rate;
            private double customs_charges_fee;
            private int is_second_hand;

            public int getIs_second_hand() {
                return is_second_hand;
            }

            public void setIs_second_hand(int is_second_hand) {
                this.is_second_hand = is_second_hand;
            }

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getGoods_spec() {
                return goods_spec;
            }

            public void setGoods_spec(String goods_spec) {
                this.goods_spec = goods_spec;
            }

            public String getImage_60_url() {
                return image_60_url;
            }

            public void setImage_60_url(String image_60_url) {
                this.image_60_url = image_60_url;
            }

            public double getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(double goods_price) {
                this.goods_price = goods_price;
            }

            public String getGoods_num() {
                return goods_num;
            }

            public void setGoods_num(String goods_num) {
                this.goods_num = goods_num;
            }

            public double getGoods_pay_price() {
                return goods_pay_price;
            }

            public void setGoods_pay_price(double goods_pay_price) {
                this.goods_pay_price = goods_pay_price;
            }

            public double getFreight_fee() {
                return freight_fee;
            }

            public void setFreight_fee(double freight_fee) {
                this.freight_fee = freight_fee;
            }

            public double getProfit_rate() {
                return profit_rate;
            }

            public void setProfit_rate(int profit_rate) {
                this.profit_rate = profit_rate;
            }

            public double getCustoms_charges_fee() {
                return customs_charges_fee;
            }

            public void setCustoms_charges_fee(double customs_charges_fee) {
                this.customs_charges_fee = customs_charges_fee;
            }
        }
    }

    public static class FlowDataBean {
        /**
         * approve_mode : 0
         * status : true
         * names : 兰博基尼
         * status_name : 已同意
         * name : 兰博基尼
         * approve_time : 1534336247
         * approve_status : 2
         * approve_opinion :
         */

        private int approve_mode;
        private boolean status;
        private String names;
        private int status_name;
        private String name;
        private String approve_time;
        private int approve_status;
        private String approve_opinion;

        public int getApprove_mode() {
            return approve_mode;
        }

        public void setApprove_mode(int approve_mode) {
            this.approve_mode = approve_mode;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public String getNames() {
            return names;
        }

        public void setNames(String names) {
            this.names = names;
        }

        public int getStatus_name() {
            return status_name;
        }

        public void setStatus_name(int status_name) {
            this.status_name = status_name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getApprove_time() {
            return approve_time;
        }

        public void setApprove_time(String approve_time) {
            this.approve_time = approve_time;
        }

        public int getApprove_status() {
            return approve_status;
        }

        public void setApprove_status(int approve_status) {
            this.approve_status = approve_status;
        }

        public String getApprove_opinion() {
            return approve_opinion;
        }

        public void setApprove_opinion(String approve_opinion) {
            this.approve_opinion = approve_opinion;
        }
    }
}
