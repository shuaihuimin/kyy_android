package com.xht.kuaiyouyi.ui.event;

/**
 * 最近聊天列表的未读消息提示的刷新
 */
public class ChatListUpdateUnreadEvent {
    private String store_id;

    public ChatListUpdateUnreadEvent(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_id() {
        return store_id;
    }
}


