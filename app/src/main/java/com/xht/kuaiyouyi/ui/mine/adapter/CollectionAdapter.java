package com.xht.kuaiyouyi.ui.mine.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.mine.entity.CollectionBean;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Utils;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CollectionAdapter extends BaseAdapter {
    private Context context;
    private CollectionBean mData;
    public CollectionAdapter(Context context, CollectionBean data) {
        this.mData=data;
        this.context = context;
    }

    @Override
    public int getCount() {
        if(mData!=null&&mData.getFavorites_list()!=null){
            return mData.getFavorites_list().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mData.getFavorites_list().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView==null){
            convertView=View.inflate(context, R.layout.item_goodscollection_layout,null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        }else {
            viewHolder= (ViewHolder) convertView.getTag();
        }
        if(mData.getFavorites_list().get(position).getIs_second_hand().equals("1")){
            if(mData.getFavorites_list().get(position).getIs_spec_pre()==1){
                viewHolder.nameview.setText(Utils.secondLabel("【"+mData.getFavorites_list().get(position).getBrand_name()+"】"+
                        mData.getFavorites_list().get(position).getGoods_name()));
            }else {
                viewHolder.nameview.setText(Utils.secondLabel(mData.getFavorites_list().get(position).getGoods_name()));
            }
        }else {
            if(mData.getFavorites_list().get(position).getIs_spec_pre()==1){
                viewHolder.nameview.setText("【"+mData.getFavorites_list().get(position).getBrand_name()+"】"+
                        mData.getFavorites_list().get(position).getGoods_name());
            }else {
                viewHolder.nameview.setText(mData.getFavorites_list().get(position).getGoods_name());
            }
        }


        Glide.with(context).load(mData.getFavorites_list().get(position).getGoods_image())
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                .into(viewHolder.imageView);
        if(mData.getFavorites_list().get(position).getState().equals("1")){
//            viewHolder.iv_add_goods.setVisibility(View.VISIBLE);
            viewHolder.tv_disable.setVisibility(View.GONE);
            viewHolder.tv_goods_price_rmb.setVisibility(View.VISIBLE);
            viewHolder.nameview.setTextColor(context.getResources().getColor(R.color.grey_4));
            if(mData.getFavorites_list().get(position).getIs_enquiry() == 1){
                viewHolder.tv_inquiry.setVisibility(View.VISIBLE);
                viewHolder.tv_goods_price_rmb.setVisibility(View.GONE);
                viewHolder.tv_goods_price.setVisibility(View.GONE);
            }else {
                viewHolder.tv_inquiry.setVisibility(View.GONE);
                viewHolder.tv_goods_price_rmb.setVisibility(View.VISIBLE);
                viewHolder.tv_goods_price_rmb.setText("¥"+Utils.getDisplayMoney(mData.getFavorites_list().get(position).getLog_price()));
                viewHolder.tv_goods_price.setVisibility(View.VISIBLE);
                viewHolder.tv_goods_price.setText(Utils.getCurrencyDisplayMoney(mData.getCurrencytype(),mData.getFavorites_list().get(position).getLog_price()));
            }
            viewHolder.iv_add_goods.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestAddToCart(position);
                }
            });
        }else {
            //失效
//            viewHolder.iv_add_goods.setVisibility(View.GONE);
            viewHolder.nameview.setTextColor(context.getResources().getColor(R.color.grey_7));
            viewHolder.tv_inquiry.setVisibility(View.GONE);
            viewHolder.tv_disable.setVisibility(View.VISIBLE);
            viewHolder.tv_goods_price_rmb.setVisibility(View.GONE);
            viewHolder.tv_goods_price.setVisibility(View.GONE);
        }
        if(position == mData.getFavorites_list().size()-1){
            viewHolder.v_line.setVisibility(View.GONE);
        }else {
            viewHolder.v_line.setVisibility(View.VISIBLE);
        }
        if(position==0){
            viewHolder.view_top.setVisibility(View.VISIBLE);
        }else {
            viewHolder.view_top.setVisibility(View.GONE);
        }
        return convertView;
    }

    private void requestAddToCart(int position) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ADD_CART())
                .addParam("goods_id",mData.getFavorites_list().get(position).getGoods_id())
                .addParam("quantity","1")
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        Toast.makeText(context,err,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        DialogUtils.createTipAllTextDialog(context,context.getString(R.string.add_to_cart_succeful));
                    }
                },false);
    }

    class ViewHolder{
         @BindView(R.id.goods_name)
         TextView nameview;
         @BindView(R.id.goods_image)
         ImageView imageView;
         @BindView(R.id.iv_add_goods)
         ImageView iv_add_goods;
         @BindView(R.id.tv_inquiry)
         TextView tv_inquiry;//请联系客服询价
         @BindView(R.id.tv_disable)
         TextView tv_disable;
         @BindView(R.id.tv_goods_price_rmb)
         TextView tv_goods_price_rmb;
        @BindView(R.id.tv_goods_price)
         TextView tv_goods_price;
         @BindView(R.id.v_line)
         View v_line;
        @BindView(R.id.view_top)
        View view_top;

         public ViewHolder(View view) {
             ButterKnife.bind(this, view);
         }
    }
}
