package com.xht.kuaiyouyi.ui.mine.setting;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.utils.Login;

import butterknife.BindView;

public class CurrencyActivity extends BaseActivity {
    @BindView(R.id.iv_left)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_right)
    TextView tv_right;
    @BindView(R.id.ll_RMB)
    LinearLayout ll_RMB;
    @BindView(R.id.iv_RMB_check)
    ImageView iv_RMB_check;
    @BindView(R.id.ll_HKD)
    LinearLayout ll_HKD;
    @BindView(R.id.iv_HKD_check)
    ImageView iv_HKD_check;
    @BindView(R.id.ll_MOP)
    LinearLayout ll_MOP;
    @BindView(R.id.iv_MOP_check)
    ImageView iv_MOP_check;
    @BindView(R.id.ll_USA)
    LinearLayout ll_USA;
    @BindView(R.id.iv_USA_check)
    ImageView iv_USA_check;

    public static final String RMB = "1";
    public static final String HKD = "2";
    public static final String MOP = "3";
    public static final String USA = "4";
    private String mCurrentCurrency = "";

    @Override
    protected int getLayout() {
        return R.layout.activity_currency;
    }

    @Override
    protected void initView() {
        tv_title.setText(R.string.currency_setting);
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setText(R.string.complete);
        showCheckedStatus(Login.Companion.getInstance().getCurrencyCode());
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login.Companion.getInstance().setCurrencyCode(mCurrentCurrency);
                finish();
            }
        });
        ll_RMB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckedStatus(RMB);
            }
        });
        ll_HKD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckedStatus(HKD);
            }
        });
        ll_MOP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckedStatus(MOP);
            }
        });
        ll_USA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCheckedStatus(USA);
            }
        });
    }

    /**
     * 显示状态
     * @param checkCurreny
     */
    private void showCheckedStatus(String checkCurreny) {
        switch (checkCurreny){
            case RMB:
                if(!mCurrentCurrency.equals(RMB)){
                    iv_RMB_check.setVisibility(View.VISIBLE);
                    iv_HKD_check.setVisibility(View.GONE);
                    iv_MOP_check.setVisibility(View.GONE);
                    iv_USA_check.setVisibility(View.GONE);
                }
                break;
            case HKD:
                if(!mCurrentCurrency.equals(HKD)){
                    iv_RMB_check.setVisibility(View.GONE);
                    iv_HKD_check.setVisibility(View.VISIBLE);
                    iv_MOP_check.setVisibility(View.GONE);
                    iv_USA_check.setVisibility(View.GONE);
                }
                break;
            case MOP:
                if(!mCurrentCurrency.equals(MOP)){
                    iv_RMB_check.setVisibility(View.GONE);
                    iv_HKD_check.setVisibility(View.GONE);
                    iv_MOP_check.setVisibility(View.VISIBLE);
                    iv_USA_check.setVisibility(View.GONE);
                }
                break;
            case USA:
                if(!mCurrentCurrency.equals(USA)){
                    iv_RMB_check.setVisibility(View.GONE);
                    iv_HKD_check.setVisibility(View.GONE);
                    iv_MOP_check.setVisibility(View.GONE);
                    iv_USA_check.setVisibility(View.VISIBLE);
                }
                break;
        }
        mCurrentCurrency = checkCurreny;
    }
}
