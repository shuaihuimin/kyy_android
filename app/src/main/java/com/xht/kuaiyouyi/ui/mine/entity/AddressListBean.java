package com.xht.kuaiyouyi.ui.mine.entity;

import java.util.List;

public class AddressListBean {


    /**
     * area_name : 北京
     * area_id : 1
     * child : [{"area_name":"北京市","area_id":"36","child":[{"area_name":"東城區","area_id":"37"},{"area_name":"西城區","area_id":"38"},{"area_name":"朝陽區","area_id":"41"},{"area_name":"豐台區","area_id":"42"},{"area_name":"石景山區","area_id":"43"},{"area_name":"海澱區","area_id":"44"},{"area_name":"門頭溝區","area_id":"45"},{"area_name":"房山區","area_id":"46"},{"area_name":"通州區","area_id":"47"},{"area_name":"順義區","area_id":"48"},{"area_name":"昌平區","area_id":"49"},{"area_name":"大興區","area_id":"50"},{"area_name":"懷柔區","area_id":"51"},{"area_name":"平谷區","area_id":"52"},{"area_name":"密雲縣","area_id":"53"},{"area_name":"延慶縣","area_id":"54"},{"area_name":"其他","area_id":"566"}]}]
     */

    private String area_name;
    private String area_id;
    private List<ChildBeanX> child;

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getArea_id() {
        return area_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public List<ChildBeanX> getChild() {
        return child;
    }

    public void setChild(List<ChildBeanX> child) {
        this.child = child;
    }

    public static class ChildBeanX {
        /**
         * area_name : 北京市
         * area_id : 36
         * child : [{"area_name":"東城區","area_id":"37"},{"area_name":"西城區","area_id":"38"},{"area_name":"朝陽區","area_id":"41"},{"area_name":"豐台區","area_id":"42"},{"area_name":"石景山區","area_id":"43"},{"area_name":"海澱區","area_id":"44"},{"area_name":"門頭溝區","area_id":"45"},{"area_name":"房山區","area_id":"46"},{"area_name":"通州區","area_id":"47"},{"area_name":"順義區","area_id":"48"},{"area_name":"昌平區","area_id":"49"},{"area_name":"大興區","area_id":"50"},{"area_name":"懷柔區","area_id":"51"},{"area_name":"平谷區","area_id":"52"},{"area_name":"密雲縣","area_id":"53"},{"area_name":"延慶縣","area_id":"54"},{"area_name":"其他","area_id":"566"}]
         */

        private String area_name;
        private String area_id;
        private List<ChildBean> child;

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public String getArea_id() {
            return area_id;
        }

        public void setArea_id(String area_id) {
            this.area_id = area_id;
        }

        public List<ChildBean> getChild() {
            return child;
        }

        public void setChild(List<ChildBean> child) {
            this.child = child;
        }

        public static class ChildBean {
            /**
             * area_name : 東城區
             * area_id : 37
             */

            private String area_name;
            private String area_id;

            public String getArea_name() {
                return area_name;
            }

            public void setArea_name(String area_name) {
                this.area_name = area_name;
            }

            public String getArea_id() {
                return area_id;
            }

            public void setArea_id(String area_id) {
                this.area_id = area_id;
            }
        }
    }
}
