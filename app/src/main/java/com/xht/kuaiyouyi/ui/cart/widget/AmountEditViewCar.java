package com.xht.kuaiyouyi.ui.cart.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;

/**
 * 购物车专用
 */
public class AmountEditViewCar extends LinearLayout implements View.OnClickListener {
    private int amount = 1; //购买数量
    private int goods_storage = 200; //商品库存
    private int min_count =1;//最小数量

    private OnAmountChangeListener mListener;
    private OnNumClickListener mNumListener;

    private EditText etAmount;
    private ImageView btnDecrease;
    private ImageView btnIncrease;

    public AmountEditViewCar(Context context) {
        this(context, null);
    }

    public AmountEditViewCar(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.view_cart_amount, this);
        etAmount = (EditText) findViewById(R.id.etAmount);
        btnDecrease = (ImageView) findViewById(R.id.btnDecrease);
        btnIncrease = (ImageView) findViewById(R.id.btnIncrease);
        btnDecrease.setOnClickListener(this);
        btnIncrease.setOnClickListener(this);
        etAmount.setOnClickListener(this);
        etAmount.setFocusable(false);

//        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.AmountView);
//        int btnWidth = obtainStyledAttributes.getDimensionPixelSize(R.styleable.AmountView_btnWidth, LinearLayout.LayoutParams.WRAP_CONTENT);
//        int tvWidth = obtainStyledAttributes.getDimensionPixelSize(R.styleable.AmountView_tvWidth, 80);
//        int tvTextSize = obtainStyledAttributes.getDimensionPixelSize(R.styleable.AmountView_tvTextSize, 0);
//        int btnTextSize = obtainStyledAttributes.getDimensionPixelSize(R.styleable.AmountView_btnTextSize, 0);
//        obtainStyledAttributes.recycle();

//        LinearLayout.LayoutParams btnParams = new LinearLayout.LayoutParams(btnWidth, LinearLayout.LayoutParams.MATCH_PARENT);
//        btnDecrease.setLayoutParams(btnParams);
//        btnIncrease.setLayoutParams(btnParams);
//        if (btnTextSize != 0) {
//            btnDecrease.setTextSize(TypedValue.COMPLEX_UNIT_PX, btnTextSize);
//            btnIncrease.setTextSize(TypedValue.COMPLEX_UNIT_PX, btnTextSize);
//        }
//
//        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(tvWidth, LinearLayout.LayoutParams.MATCH_PARENT);
//        etAmount.setLayoutParams(textParams);
//        if (tvTextSize != 0) {
//            etAmount.setTextSize(tvTextSize);
//        }
    }

    public void setOnAmountChangeListener(OnAmountChangeListener onAmountChangeListener) {
        this.mListener = onAmountChangeListener;
    }

    public void setOnNumClickListener(OnNumClickListener onNumClickListener) {
        this.mNumListener = onNumClickListener;
    }

    public void setGoods_storage(int goods_storage) {
        this.goods_storage = goods_storage;
    }
    public void setMin_count(int min_count) {
        this.min_count = min_count;
    }

    public void setNum(int num) {
        amount = num;
        etAmount.setText(amount + "");
        if(amount== min_count){
            btnDecrease.setImageResource(R.mipmap.cart_icon_disable_reduce);
        }else {
            btnDecrease.setImageResource(R.mipmap.cart_icon_reduce);
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btnDecrease) {
            if (amount > min_count) {
                amount--;
                if (mListener != null) {
                    mListener.onAmountChange(this, amount);
                }
            }
        } else if (i == R.id.btnIncrease) {
            if (amount < goods_storage) {
                amount++;
                if (mListener != null) {
                    mListener.onAmountChange(this, amount);
                }
            }else {
                Toast.makeText(getContext(),getContext().getString(R.string.count_beyond)
                        +goods_storage,Toast.LENGTH_SHORT).show();
            }
        } else if (i == R.id.etAmount) {
            if (mNumListener != null) {
                mNumListener.onNumClick(this);
            }
        }

        etAmount.clearFocus();
    }


    public interface OnAmountChangeListener {
        void onAmountChange(View view, int amount);
    }

    public interface OnNumClickListener {
        void onNumClick(View view);
    }

}
