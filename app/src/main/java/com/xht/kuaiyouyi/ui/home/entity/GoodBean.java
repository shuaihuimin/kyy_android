package com.xht.kuaiyouyi.ui.home.entity;

/**
 * Created by shuaihuimin on 2018/6/20.
 */

public class GoodBean {

        /**
         * goods_id : 100000
         * goods_name : 劳力士ROLEX-潜航者型 116610-LV-97200自动机械钢带男表联保正品
         * goods_image : http://127.0.0.1/data/upload/shop/store/goods/1/1_04752627678636481.jpg
         * goods_price : 70000.00
         * goods_salenum : 3
         */

        private String goods_id;
        private String goods_name;
        private String goods_image;
        private String goods_price;
        private String goods_salenum;

    public GoodBean(String goods_id, String goods_name, String goods_image, String goods_price, String goods_salenum) {
        this.goods_id = goods_id;
        this.goods_name = goods_name;
        this.goods_image = goods_image;
        this.goods_price = goods_price;
        this.goods_salenum = goods_salenum;
    }

    public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_image() {
            return goods_image;
        }

        public void setGoods_image(String goods_image) {
            this.goods_image = goods_image;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public String getGoods_salenum() {
            return goods_salenum;
        }

        public void setGoods_salenum(String goods_salenum) {
            this.goods_salenum = goods_salenum;
        }
}
