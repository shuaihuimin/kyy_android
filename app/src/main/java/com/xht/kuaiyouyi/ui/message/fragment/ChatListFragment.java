package com.xht.kuaiyouyi.ui.message.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.event.ChatListUpdateUnreadEvent;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.message.activity.ChatActivity;
import com.xht.kuaiyouyi.ui.message.adapter.ChatListAdapter;
import com.xht.kuaiyouyi.ui.message.db.ChatListBean;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ChatListFragment extends BaseFragment{
    @BindView(R.id.lv_content)
    ListView lv_content;
    @BindView(R.id.rl_empty_message_list)
    RelativeLayout rl_empty_message_list;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;
    private List<ChatListBean.DataBean> mChatListBeans;
    private ChatListAdapter mChatListAdapter;

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        lv_content.setDividerHeight(Utils.dp2px(getContext(),0.5f));
        lv_content.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mChatListBeans.get(position).getNo_read_msg_num()>0){
                    Login.Companion.getInstance().setUnread_msg_num((Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num())-mChatListBeans.get(position).getNo_read_msg_num())+"");
                    EventBus.getDefault().post(new MessageEvent(MessageEvent.UNREAD_MSG_NUM));
                }
                Intent intent = new Intent(getContext(),ChatActivity.class);
                intent.putExtra(ChatActivity.IS_FORM_CHATLIST,true);
                intent.putExtra(ChatActivity.CHAT_ID,mChatListBeans.get(position).getU_id());
                intent.putExtra(ChatActivity.STORE_NAME,mChatListBeans.get(position).getStore_name());
                intent.putExtra(ChatActivity.STORE_AVATAR,mChatListBeans.get(position).getStore_avatar());
                intent.putExtra(ChatActivity.STORE_ID,Long.valueOf(mChatListBeans.get(position).getStore_id()));
                startActivity(intent);
            }
        });
        EventBus.getDefault().register(this);
        mChatListBeans = new ArrayList<>();
        mChatListAdapter = new ChatListAdapter(getContext(),mChatListBeans);
        lv_content.setAdapter(mChatListAdapter);
        requestChatList();
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestChatList();
            }
        });
    }

    private void requestChatList() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CHAT_LIST())
                .addParam("f_id", Login.Companion.getInstance().getUid())
                .withPOST(new NetCallBack<ChatListBean>() {
                    @NotNull
                    @Override
                    public Class<ChatListBean> getRealType() {
                        return ChatListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        error_retry_view.setVisibility(View.VISIBLE);
                        rl_empty_message_list.setVisibility(View.GONE);
                        //Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull ChatListBean chatListBean) {
                        if(getActivity()==null){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        if(chatListBean.getData() != null){
                            mChatListBeans.clear();
                            mChatListBeans.addAll(chatListBean.getData());
                            mChatListAdapter.notifyDataSetChanged();
                            if(mChatListBeans.size()>0){
                                rl_empty_message_list.setVisibility(View.GONE);
                            }else {
                                rl_empty_message_list.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }, false);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_chat_list;
    }

    //收发消息的监听事件，更新最近消息
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getChatListEvent(ChatListBean.DataBean dataBean) {
        String store_avatar = "";
        String store_name = "";
        int unReadNum=0;//未读数
        for(int i=0;i<mChatListBeans.size();i++){
            if(mChatListBeans.get(i).getStore_id().equals(dataBean.getStore_id())){
                store_avatar = mChatListBeans.get(i).getStore_avatar();
                store_name = mChatListBeans.get(i).getStore_name();
                unReadNum = mChatListBeans.get(i).getNo_read_msg_num();
                mChatListBeans.remove(i);
                break;
            }
        }
        if(!TextUtils.isEmpty(store_avatar)){
            dataBean.setStore_name(store_name);
            dataBean.setStore_avatar(store_avatar);
            dataBean.setNo_read_msg_num(dataBean.getNo_read_msg_num()+unReadNum);
        }
        mChatListBeans.add(0,dataBean);
        mChatListAdapter.notifyDataSetChanged();
    }

    //关闭聊天页面，刷新对应store_id为已读状态
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getChatListEvent(ChatListUpdateUnreadEvent chatListUpdateUnreadEvent) {
        for(int i=0;i<mChatListBeans.size();i++){
            if(mChatListBeans.get(i).getStore_id().equals(chatListUpdateUnreadEvent.getStore_id())){
                mChatListBeans.get(i).setNo_read_msg_num(0);
                break;
            }
        }
        mChatListAdapter.notifyDataSetChanged();
    }

    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

}
