package com.xht.kuaiyouyi.ui.mine.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.mine.entity.ServiceHelpBean;

public class ServiceHelpAdapter extends BaseAdapter {
    private Context mContext;
    private ServiceHelpBean mServiceHelpBean;

    public ServiceHelpAdapter(Context context, ServiceHelpBean serviceHelpBean) {
        this.mContext = context;
        this.mServiceHelpBean = serviceHelpBean;
    }

    @Override
    public int getCount() {
        if (mServiceHelpBean.getClass_list()!=null && mServiceHelpBean.getClass_list().size()>0) {
            return mServiceHelpBean.getClass_list().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mServiceHelpBean.getClass_list().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.item_service_help, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_question = convertView.findViewById(R.id.tv_question);
            viewHolder.v_line = convertView.findViewById(R.id.v_line);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tv_question.setText(mServiceHelpBean.getClass_list().get(position).getAc_name());
        if(position == mServiceHelpBean.getClass_list().size()-1){
            viewHolder.v_line.setVisibility(View.GONE);
        }
        return convertView;
    }


    private class ViewHolder {
        private TextView tv_question;
        private View v_line;
    }
}
