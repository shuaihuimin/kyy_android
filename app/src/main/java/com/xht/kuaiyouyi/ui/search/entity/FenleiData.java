package com.xht.kuaiyouyi.ui.search.entity;

import java.util.List;

/**
 * Created by shuaihuimin on 2018/6/21.
 */

public class FenleiData {

    private List<ClassRightArrBean> class_right_arr;
    private List<BannerArrBean> banner_arr;

    public List<ClassRightArrBean> getClass_right_arr() {
        return class_right_arr;
    }

    public void setClass_right_arr(List<ClassRightArrBean> class_right_arr) {
        this.class_right_arr = class_right_arr;
    }

    public List<BannerArrBean> getBanner_arr() {
        return banner_arr;
    }

    public void setBanner_arr(List<BannerArrBean> banner_arr) {
        this.banner_arr = banner_arr;
    }

    public static class ClassRightArrBean {
        /**
         * gc_id : 200
         * gc_name : 家纺
         * class3 : [{"gc_id":"206","gc_name":"床品件套","gc_image":""},{"gc_id":"207","gc_name":"被子","gc_image":""},{"gc_id":"208","gc_name":"枕芯枕套","gc_image":""},{"gc_id":"209","gc_name":"床单被罩","gc_image":""},{"gc_id":"210","gc_name":"毛巾被/毯","gc_image":""},{"gc_id":"211","gc_name":"床垫/床褥","gc_image":""},{"gc_id":"212","gc_name":"蚊帐/凉席","gc_image":""},{"gc_id":"213","gc_name":"抱枕坐垫","gc_image":""},{"gc_id":"214","gc_name":"毛巾家纺","gc_image":""},{"gc_id":"215","gc_name":"电热毯","gc_image":""},{"gc_id":"216","gc_name":"窗帘/窗纱","gc_image":""},{"gc_id":"217","gc_name":"酒店用品","gc_image":""}]
         */

        private String gc_id;
        private String gc_name;
        private List<Class3Bean> class3;

        public String getGc_id() {
            return gc_id;
        }

        public void setGc_id(String gc_id) {
            this.gc_id = gc_id;
        }

        public String getGc_name() {
            return gc_name;
        }

        public void setGc_name(String gc_name) {
            this.gc_name = gc_name;
        }

        public List<Class3Bean> getClass3() {
            return class3;
        }

        public void setClass3(List<Class3Bean> class3) {
            this.class3 = class3;
        }

        public static class Class3Bean {
            /**
             * gc_id : 206
             * gc_name : 床品件套
             * gc_image :
             */

            private String gc_id;
            private String gc_name;
            private String gc_image;

            public String getGc_id() {
                return gc_id;
            }

            public void setGc_id(String gc_id) {
                this.gc_id = gc_id;
            }

            public String getGc_name() {
                return gc_name;
            }

            public void setGc_name(String gc_name) {
                this.gc_name = gc_name;
            }

            public String getGc_image() {
                return gc_image;
            }

            public void setGc_image(String gc_image) {
                this.gc_image = gc_image;
            }
        }
    }

    public static class BannerArrBean {
        /**
         * href :
         * img : http://192.168.0.2/data/upload/shop/goods_class/04849387912371887.jpg
         */

        private String href;
        private String img;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }
    }
}

