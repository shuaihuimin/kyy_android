package com.xht.kuaiyouyi.ui.goods.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.goods.StoreActivity;
import com.xht.kuaiyouyi.ui.goods.entity.StoregoodsBean;
import com.xht.kuaiyouyi.ui.mine.setting.CurrencyActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.utils.Utils;

import java.util.List;

public class StoreAdapter extends BaseQuickAdapter<StoregoodsBean.ListBean,BaseViewHolder> {
    private Context context;
    private List<StoregoodsBean.CurrencytypeBean> currlist;
    private int flag;
    private int height;
    public StoreAdapter(Context context,int layoutResId, @Nullable List<StoregoodsBean.ListBean> data,List<StoregoodsBean.CurrencytypeBean> currlist,int flag,int height) {
        super(layoutResId, data);
        this.context=context;
        this.currlist=currlist;
        this.flag=flag;
        this.height=height;
    }

    public StoreAdapter(@Nullable List<StoregoodsBean.ListBean> data) {
        super(data);
    }

    public StoreAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final StoregoodsBean.ListBean item) {
        double currency=0;String price="";
        if (item.getIs_enquiry().equals("0")) {
            helper.getView(R.id.tv_inquiry).setVisibility(View.GONE);
            helper.getView(R.id.tv_price).setVisibility(View.VISIBLE);
            helper.getView(R.id.tv_dollar).setVisibility(View.VISIBLE);
        } else {
            helper.getView(R.id.tv_inquiry).setVisibility(View.VISIBLE);
            helper.getView(R.id.tv_price).setVisibility(View.GONE);
            helper.getView(R.id.tv_dollar).setVisibility(View.GONE);
        }
        if(currlist.size()!=0) {
            if (Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.HKD)) {
                currency = Double.parseDouble(item.getGoods_price()) * currlist.get(0).getHKD().getRate();
                price = Utils.getDisplayMoney(currency);
                helper.setText(R.id.tv_price, "¥"+item.getGoods_price())
                        .setText(R.id.tv_dollar, "(~" + currlist.get(0).getHKD().getSymbol() + price + ")");
            } else if (Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.MOP)) {
                currency = Double.parseDouble(item.getGoods_price()) * currlist.get(0).getMOP().getRate();
                price = Utils.getDisplayMoney(currency);
                helper.setText(R.id.tv_price, "¥"+item.getGoods_price())
                        .setText(R.id.tv_dollar, "(~" + currlist.get(0).getMOP().getSymbol() + price + ")");
            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.RMB)){
                helper.setText(R.id.tv_price, "¥"+item.getGoods_price());
                helper.getView(R.id.tv_dollar).setVisibility(View.GONE);
            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.USA)){
                currency = Double.parseDouble(item.getGoods_price()) * currlist.get(0).getUSD().getRate();
                price = Utils.getDisplayMoney(currency);
                helper.setText(R.id.tv_price, "¥"+item.getGoods_price())
                        .setText(R.id.tv_dollar, "(~" + currlist.get(0).getMOP().getSymbol() + price + ")");
            }
        }
        if(item.getIs_second_hand()==1){
            helper.setText(R.id.tv_name,Utils.secondLabel(item.getGoods_name()));
        }else {
            helper.setText(R.id.tv_name,item.getGoods_name());
        }
        Glide.with(KyyApp.context).load(item.getGoods_image()).into((ImageView) helper.getView(R.id.iv_img));
        if(flag==1){
            ViewGroup.LayoutParams layoutParams=helper.getView(R.id.iv_img).getLayoutParams();
            layoutParams.height= height;
        }
        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("goods_id",item.getGoods_id());
                Intent intent=new Intent();
                intent.setClass(context, GoodsActivity.class);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

        if(flag==0 && helper.getPosition()!=0){
            helper.setGone(R.id.view_line,false);
        }else if (flag==0 && helper.getPosition()==0){
            helper.setGone(R.id.view_line,true);
        }
    }
}
