package com.xht.kuaiyouyi.ui.cart;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.cart.adapter.CartAdapter;
import com.xht.kuaiyouyi.ui.cart.adapter.CompanyListAdapter;
import com.xht.kuaiyouyi.ui.cart.entity.CartIndexBean;
import com.xht.kuaiyouyi.ui.cart.entity.CompanyListBean;
import com.xht.kuaiyouyi.ui.cart.order.ConfirmOrderActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.CartActivity;
import com.xht.kuaiyouyi.ui.goods.EnterprisePop;
import com.xht.kuaiyouyi.utils.CommonPopupWindow;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/**
 * Created by shuaihuimin on 2018/6/13.
 * 购物车没有询价商品
 */

public class CartFragment extends BaseFragment {
    @BindView(R.id.elv_content)
    ExpandableListView elv_content;
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_right)
    TextView tv_right;
    @BindView(R.id.srl_refresh)
    SmartRefreshLayout srl_refresh;

    @BindView(R.id.rl_pay)
    RelativeLayout rl_pay;
    @BindView(R.id.tv_total_price)
    TextView tv_total_price;
    @BindView(R.id.tv_total_price_rmb)
    TextView tv_total_price_rmb;
    @BindView(R.id.bt_pay)
    Button bt_pay;

    @BindView(R.id.ll_delete_and_collection)
    LinearLayout ll_delete_and_collection;
    @BindView(R.id.tv_remove_to_collection)
    TextView tv_remove_to_collection;
    @BindView(R.id.tv_delete)
    TextView tv_delete;

    @BindView(R.id.iv_check)
    ImageView iv_check;
    @BindView(R.id.rl_all_check)
    RelativeLayout rl_all_check;


    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;


    @BindView(R.id.ll_cart_empty)
    LinearLayout ll_cart_empty;
    @BindView(R.id.bt_to_home_page)
    Button bt_to_home_page;

    private CartAdapter mCartAdapter;

    private boolean isEditMode;//是否是编辑模式
    private String company_id;
    private CommonPopupWindow popupCompany;

    private boolean isAllCheck = false;

    private OnCartToHomePageListener mOnCartToHomePageListener;

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setText(R.string.edit);
        iv_left.setVisibility(View.GONE);
        tv_title.setText(R.string.purchase);
        DialogUtils.createTipAllLoadDialog(getContext());
        iv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new MessageEvent(MessageEvent.CART));
            }
        });
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEditMode) {
                    isEditMode = false;
                    rl_pay.setVisibility(View.VISIBLE);
                    ll_delete_and_collection.setVisibility(View.GONE);
                    tv_right.setText(R.string.edit);
                    if (mCartAdapter != null) {
                        tv_total_price_rmb.setText("¥" + Utils.getDisplayMoney(mCartAdapter.getTotalAmount()));
                    }
                } else {
                    isEditMode = true;
                    rl_pay.setVisibility(View.GONE);
                    ll_delete_and_collection.setVisibility(View.VISIBLE);
                    tv_right.setText(R.string.complete);
                }
            }
        });
        rl_all_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAllCheck(!isAllCheck);
                mCartAdapter.setAllCheckMode(isAllCheck, new CartAdapter.OnAllCheckModeChangeListener() {
                    @Override
                    public void onChange(double totalPrice) {
                        //全选状态的改变
                        tv_total_price_rmb.setText("¥" + Utils.getDisplayMoney(totalPrice));
                        tv_total_price.setText(Utils.getCurrencyDisplayMoney(((KyyApp)(KyyApp.context)).cartIndexBean.getCurrencytype(),totalPrice));
                    }
                });
            }
        });
        srl_refresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                requestCartIndex();
            }
        });
        tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCartAdapter != null) {
                    if(!TextUtils.isEmpty(mCartAdapter.getSeletedCartId())){
                        mCartAdapter.requestDeleteSelectedGoods(null);
                    }else {
                        Toast.makeText(getContext(), getContext().getString(R.string.no_select_goods), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        bt_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCartAdapter!=null){
                    if(mCartAdapter.getTotalAmount()>0){
                        requestCompanyList();
                    }else {
                        Toast.makeText(getContext(), getString(R.string.no_select_goods), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        tv_remove_to_collection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCartAdapter != null) {
                    if(!TextUtils.isEmpty(mCartAdapter.getSeletedCartId())){
                        mCartAdapter.requestRemoveToCollection(null);
                    }else {
                        Toast.makeText(getContext(), getContext().getString(R.string.no_select_goods), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        bt_load_again.setVisibility(View.VISIBLE);
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCartIndex();
            }
        });

        //设置不能点击头部折叠
        elv_content.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });

        bt_to_home_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() instanceof CartActivity){
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(CartActivity.TO_HOME_PAGE,true);
                    goToActivity(MainActivity.class,bundle);
                }else {
                    if(mOnCartToHomePageListener!=null){
                        mOnCartToHomePageListener.onCartToHomePage();
                    }
                }
            }
        });



        if(Login.Companion.getInstance().isLogin()){
            DialogUtils.createTipAllLoadDialog(getContext());
            if(((KyyApp)(KyyApp.context)).cartIndexBean!=null){
                mCartAdapter = new CartAdapter(getContext());
                elv_content.setAdapter(mCartAdapter);
                for (int i = 0; i < ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().size(); i++) {
                    elv_content.expandGroup(i);
                }
                setListener();
            }
            requestCartIndex();
        }

    }

    private void setAllCheck(boolean isCheck) {
        isAllCheck = isCheck;
        if(isAllCheck){
            iv_check.setImageResource(R.mipmap.pop_icon_tick);
        }else {
            iv_check.setImageResource(R.mipmap.address_icon_unselected_nor);
        }
    }

    private void requestCartIndex() {
        try {
        if(error_retry_view.getVisibility()==View.VISIBLE){
            DialogUtils.createTipAllLoadDialog(getContext());
        }

            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CART_LIST())
                    .withPOST(new NetCallBack<CartIndexBean>() {

                        @NotNull
                        @Override
                        public Class<CartIndexBean> getRealType() {
                            return CartIndexBean.class;
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(getActivity()==null){
                                return;
                            }
                            DialogUtils.moven();
                            error_retry_view.setVisibility(View.VISIBLE);
                            elv_content.setVisibility(View.GONE);
                            ll_cart_empty.setVisibility(View.GONE);
                            if (srl_refresh.getState() == RefreshState.Refreshing) {
                                srl_refresh.finishRefresh();
                            }
                            //Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onSuccess(@NonNull CartIndexBean cartIndexBean) {
                            if(getActivity()==null){
                                return;
                            }
                            DialogUtils.moven();
                            error_retry_view.setVisibility(View.GONE);
                            elv_content.setVisibility(View.VISIBLE);
                            if (srl_refresh.getState() == RefreshState.Refreshing) {
                                srl_refresh.finishRefresh();
                            }
                            if (cartIndexBean.getStore_list() != null&&cartIndexBean.getStore_list().size()>0) {
                                ll_cart_empty.setVisibility(View.GONE);
                                if(((KyyApp)(KyyApp.context)).cartIndexBean==null){
                                    ((KyyApp)(KyyApp.context)).cartIndexBean = cartIndexBean;
                                    mCartAdapter = new CartAdapter(getContext());
                                    elv_content.setAdapter(mCartAdapter);
                                    setListener();
                                }else {
                                    setNewData(cartIndexBean);
                                    mCartAdapter.notifyDataSetChanged();
                                    EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_MAIN_CART));
                                    setAllCheck(mCartAdapter.checkIsAllChecked());
                                }
                                for (int i = 0; i < ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().size(); i++) {
                                    elv_content.expandGroup(i);
                                }
                                tv_total_price_rmb.setText("¥" + Utils.getDisplayMoney(mCartAdapter.getTotalAmount()));
                                tv_total_price.setText(Utils.getCurrencyDisplayMoney(((KyyApp)(KyyApp.context)).cartIndexBean.getCurrencytype(),mCartAdapter.getTotalAmount()));
                            }else {
                                ll_cart_empty.setVisibility(View.VISIBLE);
                            }
                        }
                    }, false);
        }catch (Exception e){
        }

    }

    private void setListener() {
        mCartAdapter.setOnAmountChangeListener(new CartAdapter.OnAmountChangeListener() {
            @Override
            public void onAmountChange(double totalPrice) {
                tv_total_price_rmb.setText("¥" + Utils.getDisplayMoney(totalPrice));
                Utils.getCurrencyDisplayMoney(((KyyApp)(KyyApp.context)).cartIndexBean.getCurrencytype(),totalPrice);
                tv_total_price.setText(Utils.getCurrencyDisplayMoney(((KyyApp)(KyyApp.context)).cartIndexBean.getCurrencytype(),totalPrice));
                if(((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().size()==0){
                    ll_cart_empty.setVisibility(View.VISIBLE);
                }
            }
        });
        mCartAdapter.setOnItemAllCheckedListener(new CartAdapter.onItemAllCheckedListener() {
            @Override
            public void allChecked(boolean IsAllChecked) {
                setAllCheck(IsAllChecked);
            }
        });
    }

    //选择采购方式
    private void requestCompanyList() {
        DialogUtils.createTipAllLoadDialog(getContext());
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ORDER_COMPANY_LIST())
                .withPOST(new NetCallBack<CompanyListBean>() {

                    @NotNull
                    @Override
                    public Class<CompanyListBean> getRealType() {
                        return CompanyListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        DialogUtils.moven();
                        ToastU.INSTANCE.showToast(getString(R.string.not_network));
                    }

                    @Override
                    public void onSuccess(@NonNull final CompanyListBean companyListBean) {
                        if(getActivity()==null){
                            return;
                        }
                        DialogUtils.moven();
                        if(companyListBean.getList()!=null && companyListBean.getList().size()>1){
                            final CompanyListAdapter companyListAdapter = new CompanyListAdapter(getActivity(),companyListBean);
                            View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_procurement_method,null);
                            if(popupCompany==null){
                                final int[] selectPosition = {0};
                                companyListBean.getList().get(selectPosition[0]).setSelect(true);
                                company_id=companyListBean.getList().get(selectPosition[0]).getCompany_id();
                                popupCompany = new CommonPopupWindow.Builder(getActivity())
                                        .setView(view)
                                        .setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dp2px(getContext(),330))
                                        .setBackGroundLevel(0.5f)//取值范围0.0f-1.0f 值越小越暗
                                        .create();
                                ListView lv_company_list = view.findViewById(R.id.lv_company_list);
                                Button bt_submit = view.findViewById(R.id.bt_submit);
                                ImageView iv_close = view.findViewById(R.id.iv_close);
                                lv_company_list.setAdapter(companyListAdapter);
                                bt_submit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        confirmorder();
                                        popupCompany.dismiss();
                                    }
                                });
                                lv_company_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        if(position!= selectPosition[0]){
                                            companyListBean.getList().get(position).setSelect(true);
                                            companyListBean.getList().get(selectPosition[0]).setSelect(false);
                                            selectPosition[0] =position;
                                            company_id=companyListBean.getList().get(position).getCompany_id();
                                            companyListAdapter.notifyDataSetChanged();
                                        }
                                    }
                                });
                                iv_close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        popupCompany.dismiss();
                                    }
                                });
                            }
                            popupCompany.setAnimationStyle(R.style.Dialog_buttom_anim);
                            popupCompany.showAtLocation(view, Gravity.BOTTOM, 0, 0);
                            Utils.SetWindBg(getActivity());
                        }else if(companyListBean.getList()!=null && companyListBean.getList().size()==1){
                            company_id=companyListBean.getList().get(0).getCompany_id();
                            confirmorder();
                        }else if(companyListBean.getList().size()==0){
                            Log.i("info","-------zlmzlmzlm");
                            EnterprisePop.showpop(getActivity());
                        }
                    }
                }, false);
    }

    //确认订单
    private void confirmorder(){
        Log.i("info","---------cart_str"+mCartAdapter.getSeletedCartIdandNum());
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CONFIRM_ORDER())
                .addParam("company_id",company_id)
                .addParam("ifcart","1")
                .addParam("cart_str",mCartAdapter.getSeletedCartIdandNum())
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        ToastU.INSTANCE.showToast(getString(R.string.not_network));
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        if(getActivity()==null){
                            return;
                        }
                        Bundle bundle=new Bundle();
                        bundle.putString("order_content",s);
                        goToActivity(ConfirmOrderActivity.class,bundle);
                    }
                },false);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_cart;
    }

    @Override
    public void onResume() {
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onResume();
        if(Login.Companion.getInstance().isLogin()&&!isHidden()){
            refreshData();
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(hidden){
            if(isEditMode){
                tv_right.performClick();
            }
        }

        if(!hidden){
            refreshData();
        }

    }

    private void refreshData() {
        if(Login.Companion.getInstance().isLogin()&&!isHidden()){
            requestCartIndex();
        }
    }

    public void setIv_leftVisiable(){
        iv_left.setVisibility(View.VISIBLE);
        iv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity()!=null){
                    getActivity().finish();
                }
            }
        });
    }


    /**
     * 记录下购物车已选中的商品
     * @param newData 从服务器拿回来的新数据
     */
    private void setNewData(CartIndexBean newData) {
        try {
            for(int i=0;i<newData.getStore_list().size();i++){
                for(int j=0;j<((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().size();j++){
                    //是否是同一个店铺
                    if(newData.getStore_list().get(i).getStore_id().equals(((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(j).getStore_id())){
                        for(int a=0;a<newData.getStore_list().get(i).getGoods().size();a++){
                            for(int b=0;b<((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(j).getGoods().size();b++){
                                if(newData.getStore_list().get(i).getGoods().get(a).getCart_id()
                                        .equals(((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(j).getGoods().get(b).getCart_id())){
                                    if(((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().get(j).getGoods().get(b).isCheck()){
                                        newData.getStore_list().get(i).getGoods().get(a).setCheck(true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }catch (IndexOutOfBoundsException e){

        }
        for(CartIndexBean.StoreListBean storeListBean:newData.getStore_list()){
            boolean isAllChildCheck = true;
            int canSelected = 0;//不是无效和无货商品的数量
            for (CartIndexBean.StoreListBean.GoodsBean bean : storeListBean.getGoods()) {
                if (canSelect(bean)) {
                    canSelected++;
                    if (!bean.isCheck()) {
                        isAllChildCheck = false;
                        break;
                    }
                }
            }
            if(canSelected == 0){
                isAllChildCheck = false;
            }
            storeListBean.setCheck(isAllChildCheck);
        }

        ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().clear();
        ((KyyApp)(KyyApp.context)).cartIndexBean.getStore_list().addAll(newData.getStore_list());
        ((KyyApp)(KyyApp.context)).cartIndexBean.setCurrencytype(newData.getCurrencytype());
    }

    public interface OnCartToHomePageListener{
        void onCartToHomePage();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if(context instanceof MainActivity){
                mOnCartToHomePageListener = (OnCartToHomePageListener) context;
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnCartToHomePageListener");
        }
    }

    /**
     * 返回是否是可选的商品
     * 不可选的包含失效与无货，无货包含数量为0或者批发商品数量小于最小批发量这两种情况
     * @param bean
     * @return
     */
    public static boolean canSelect(CartIndexBean.StoreListBean.GoodsBean bean) {
        return bean.getGoods_state() == 1 && !(bean.getGoods_storage()==0
                || (bean.getBatch()!=null&&bean.getBatch().size()>0&&bean.getGoods_storage()<bean.getBatch().get(0).getMix()));
    }

    @Override
    public void onPause() {
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.REFRESH_MAIN_CART)){
            mCartAdapter.notifyDataSetChanged();
        }
    }
}
