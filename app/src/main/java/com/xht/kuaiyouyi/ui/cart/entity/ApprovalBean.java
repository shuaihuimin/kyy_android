package com.xht.kuaiyouyi.ui.cart.entity;

import java.util.List;

public class ApprovalBean {

    private List<String> approve_member;
    private List<String> copy_member;

    public List<String> getApprove_member() {
        return approve_member;
    }

    public void setApprove_member(List<String> approve_member) {
        this.approve_member = approve_member;
    }

    public List<String> getCopy_member() {
        return copy_member;
    }

    public void setCopy_member(List<String> copy_member) {
        this.copy_member = copy_member;
    }
}
