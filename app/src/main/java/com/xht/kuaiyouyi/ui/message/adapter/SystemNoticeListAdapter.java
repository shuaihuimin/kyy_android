package com.xht.kuaiyouyi.ui.message.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.message.bean.SystemNoticeListBean;
import com.xht.kuaiyouyi.utils.Utils;

public class SystemNoticeListAdapter extends BaseAdapter {
    private Context mContext;
    private SystemNoticeListBean mSystemNoticeListBean;

    public SystemNoticeListAdapter(Context mContext, SystemNoticeListBean systemNoticeListBean) {
        this.mContext = mContext;
        this.mSystemNoticeListBean = systemNoticeListBean;
    }

    @Override
    public int getCount() {
        if(mSystemNoticeListBean.getMessage_list()!=null){
            return mSystemNoticeListBean.getMessage_list().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mSystemNoticeListBean.getMessage_list().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.item_message_notice, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_message = convertView.findViewById(R.id.tv_message);
            viewHolder.tv_time = convertView.findViewById(R.id.tv_time);
            viewHolder.tv_point = convertView.findViewById(R.id.tv_point);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_time.setText(Utils.getDisplayDataAndTime(mSystemNoticeListBean.getMessage_list().get(position).getArticle_time()));
        viewHolder.tv_message.setText(mSystemNoticeListBean.getMessage_list().get(position).getArticle_title());
        viewHolder.tv_point.setVisibility(View.INVISIBLE);
        return convertView;
    }

    private class ViewHolder{
        private TextView tv_time,tv_message,tv_point;
    }
}
