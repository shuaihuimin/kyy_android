package com.xht.kuaiyouyi.ui.search.entity;

/**
 * Created by shuaihuimin on 2018/6/20.
 */

public class ClassArrBean{
    private String gc_id;
    private String gc_name;
    private boolean select=false;
    public ClassArrBean(String gc_id, String gc_name) {
        this.gc_id = gc_id;
        this.gc_name = gc_name;
    }
    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
    public String getGc_id() {
        return gc_id;
    }

    public void setGc_id(String gc_id) {
        this.gc_id = gc_id;
    }

    public String getGc_name() {
        return gc_name;
    }

    public void setGc_name(String gc_name) {
        this.gc_name = gc_name;
    }

}
