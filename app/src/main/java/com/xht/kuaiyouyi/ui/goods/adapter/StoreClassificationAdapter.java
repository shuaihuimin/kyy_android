package com.xht.kuaiyouyi.ui.goods.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSONObject;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.goods.StoreClassificationActivity;
import com.xht.kuaiyouyi.ui.goods.StoreTypelistActivity;
import com.xht.kuaiyouyi.ui.goods.entity.StoreClassBean;
import com.xht.kuaiyouyi.ui.search.adapter.SectionedRecyclerViewAdapter;
import com.xht.kuaiyouyi.ui.search.adapter.SortingAdapter;
import com.xht.kuaiyouyi.ui.search.entity.DescHolder;
import com.xht.kuaiyouyi.ui.search.entity.HeaderHolder;
import com.xht.kuaiyouyi.ui.search.entity.ScreenBean;
import com.xht.kuaiyouyi.utils.HotelUtils;

import java.util.List;

public class StoreClassificationAdapter extends SectionedRecyclerViewAdapter<HeaderHolder,DescHolder,RecyclerView.ViewHolder> implements View.OnClickListener {
    private OnItemClickListener mItemClickListener;
    private List<StoreClassBean.StoreCateBean> list;
    private LayoutInflater mInflater;
    private SparseBooleanArray mBooleanMap;
    private Context context;
    private JSONObject jsonObject;

    public StoreClassificationAdapter(Context context,List<StoreClassBean.StoreCateBean> list){
        this.context=context;
        this.list=list;
        mInflater = LayoutInflater.from(context);
        mBooleanMap = new SparseBooleanArray();
    }

    public void setData(List<StoreClassBean.StoreCateBean> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @Override
    protected int getSectionCount() {
        return HotelUtils.isEmpty(list)?0:list.size();
    }

    @Override
    protected int getItemCountForSection(int section) {
        if(list.get(section).getChild()==null){
            return 0;
        }else {
            int count = list.get(section).getChild().size();
            if (count >= 4 && !mBooleanMap.get(section)) {
                count = 4;
            }
            return count;
        }
    }

    @Override
    protected boolean hasFooterInSection(int section) {
        return false;
    }

    @Override
    protected HeaderHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
        return new HeaderHolder(mInflater.inflate(R.layout.storetype_head_item_layout, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected DescHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.typelist_content_item_layout, parent,false);
        DescHolder descHolder=new DescHolder(view);
        descHolder.itemView.setOnClickListener(this);
        return descHolder;
    }

    @Override
    protected void onBindSectionHeaderViewHolder(final HeaderHolder holder, final int section) {
        holder.openView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isOpen = mBooleanMap.get(section);
                //String text = isOpen ? "展开" : "关闭";
                mBooleanMap.put(section, !isOpen);

                    if(isOpen){
                        holder.openView.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.mipmap.class_icon_open_nor),null);
                    }else {
                        holder.openView.setCompoundDrawablesWithIntrinsicBounds(null,null,context.getResources().getDrawable(R.mipmap.class_icon_close_nor),null);
                    }
                notifyDataSetChanged();
            }
        });

        holder.titleView.setText(list.get(section).getStc_name());
        if(list.get(section).getChild()==null || list.get(section).getChild().size()==0){
            holder.titleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle=new Bundle();
                    bundle.putString("store_id",list.get(section).getStore_id());
                    bundle.putString("stc_id",list.get(section).getStc_id());
                    bundle.putString("keyword",list.get(section).getStc_name());
                    Intent intent=new Intent();
                    intent.putExtras(bundle);
                    intent.setClass(context,StoreTypelistActivity.class);
                    context.startActivity(intent);

                }
            });
            if(section==0){
                holder.view2.setVisibility(View.VISIBLE);
                holder.view3.setVisibility(View.GONE);
            }else {
                holder.view2.setVisibility(View.GONE);
                holder.view3.setVisibility(View.VISIBLE);
            }

        }else {
            if(section==0){
                holder.view2.setVisibility(View.VISIBLE);
                holder.view3.setVisibility(View.GONE);
            }else {
                holder.view2.setVisibility(View.GONE);
                holder.view3.setVisibility(View.VISIBLE);
            }
        }


        if(list.get(section).getChild()==null || list.get(section).getChild().size()==0){
            holder.openView.setVisibility(View. GONE);
            holder.tv_next.setVisibility(View.VISIBLE);
        }else {
            if(list.get(section).getChild().size()<=4){
                holder.openView.setVisibility(View. VISIBLE);
                holder.tv_next.setVisibility(View.GONE);
            }else {
                holder.openView.setVisibility(View.VISIBLE);
                holder.tv_next.setVisibility(View.GONE);
            }
        }

        if(section==list.size()-1 && list.get(section).getChild()==null){
            holder.view4.setVisibility(View.VISIBLE);
        }else {
            holder.view4.setVisibility(View.GONE);
        }

        //holder.openView.setText(mBooleanMap.get(section) ? "关闭" : "展开");

    }

    @Override
    protected void onBindSectionFooterViewHolder(RecyclerView.ViewHolder holder, int section) {

    }

    @Override
    protected void onBindItemViewHolder(DescHolder holder, int section, int position) {
        holder.descView.setText(list.get(section).getChild().get(position).getStc_name());
        if (list.get(section).getChild().get(position).isSelect() == true){
            holder.descView.setBackground(context.getResources().getDrawable(R.drawable.specifications_bg));
        }else{
            holder.descView.setBackground(context.getResources().getDrawable(R.drawable.specifications_fbg));
        }
        jsonObject = new JSONObject();
        jsonObject.put("section",section);
        jsonObject.put("position",position);
        holder.itemView.setTag(jsonObject);
    }

    @Override
    public void onClick(View v) {
        if (mItemClickListener!=null){
            mItemClickListener.onItemClick((JSONObject)v.getTag());
        }
    }

    public interface OnItemClickListener{
        void onItemClick(JSONObject jsonObject);
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }
}
