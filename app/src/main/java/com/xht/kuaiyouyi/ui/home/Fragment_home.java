package com.xht.kuaiyouyi.ui.home;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.OnTwoLevelListener;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.header.TwoLevelHeader;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.goods.StoreActivity;
import com.xht.kuaiyouyi.ui.home.activity.BrandActivity;
import com.xht.kuaiyouyi.ui.home.activity.BrandDetilsActivity;
import com.xht.kuaiyouyi.ui.home.activity.CooperationCustomerActivity;
import com.xht.kuaiyouyi.ui.home.activity.PowerSupplierActivity;
import com.xht.kuaiyouyi.ui.home.activity.SeacondMarketActivity;
import com.xht.kuaiyouyi.ui.home.activity.SearchGoodsActivity;
import com.xht.kuaiyouyi.ui.home.adapter.HomeRecyclerAdapter;
import com.xht.kuaiyouyi.ui.home.entity.HomeData;
import com.xht.kuaiyouyi.ui.home.entity.SearchHotWordBean;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.ui.search.TypelistActivity;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.GlideImageLoader;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.ProportionscreenUtils;
import com.xht.kuaiyouyi.utils.SharedPreferencesUtils;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.BadgeView;
import com.xht.kuaiyouyi.widget.RefreshHeaderLayout;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;
import com.zxing_android.CaptureActivity;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static android.content.ContentValues.TAG;

/**
 * Created by shuaihuimin on 2018/6/13.
 * 首页fragment
 */


public class Fragment_home extends BaseFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ll_top)
    LinearLayout ll_top;
    @BindView(R.id.mRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.recycler_home)
    RecyclerView recycler_home;
    @BindView(R.id.iv_scan)
    ImageView iv_scan;
    @BindView(R.id.tvSearch)
    TextView tvSearch;
    @BindView(R.id.bt_enterprise_im)
    ImageView btnIm;
    @BindView(R.id.iv_settop)
    ImageView iv_settop;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;
    //下拉二楼
    @BindView(R.id.two_header)
    TwoLevelHeader two_header;
    @BindView(R.id.refresh_head)
    RefreshHeaderLayout refresh_head;
    @BindView(R.id.rl_second_floor)
    RelativeLayout rl_second_floor;
    

    private HomeRecyclerAdapter homeRecyclerAdapter;
    private List<HomeData.GoodsListBean> recyclerlist=new ArrayList<>();
    private HomeData homeData;

    //广告图数据源
    private List<HomeData.WebHtmlBean> advertising_list=new ArrayList<>();
    //轮播图数据源
    private List<String> lunbolist=new ArrayList<>();
    //汇率
    private List<HomeData.CurrencytypeBean> currlist=new ArrayList<>();
    private Gson gson=new Gson();


    private final int PERMISSION_CAMERA_REQUEST_CODE = 20;
    private String kyy="";

    private ViewGroup.LayoutParams params;
    private float wideth;
    private float height;
    private int screen;
    //判断加载更多的时候不重新添加头部
    private int sate=0;
    private String gc_id=null;
    private String goods_id=null;
    private String type;
    private String type_id;
    private Banner banner;
    RelativeLayout rl_pull_up_tip;
    TextView tv_pull_up_tip;
    //recyclerview头部
    private View bannerview;

    private BadgeView mBadgeView;


    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mBadgeView = new BadgeView(getContext(),btnIm,10,4);
//        gethotsearch();
        //RecyclerView item布局，设置数据
        int img_height=(Utils.getWindowWidth(getActivity())-8)/2;
        homeRecyclerAdapter=new HomeRecyclerAdapter(getActivity(),R.layout.home_recycler_item_layout,recyclerlist,currlist,img_height);
        recycler_home.isNestedScrollingEnabled();
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL,false);
        recycler_home.setLayoutManager(manager);
        recycler_home.setAdapter(homeRecyclerAdapter);
        getData();
        click();
        initrefresh();
        showUnreadMsgNum();

        initTwoLevel();
    }

    private void initTwoLevel() {
        refresh_head.setHeaderBackgroundResource(android.R.color.transparent);
        refresh_head.setTextColor(getResources().getColor(android.R.color.white)
                ,getResources().getColor(R.color.grey_7)
                ,getResources().getColor(android.R.color.white));
        refresh_head.setHeaderIconResource(R.mipmap.refresh_loading);
        //调整二楼下拉的灵敏度
        two_header.setMaxRage(10.0f);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home_layout;
    }

    private final void onScrolledDown() {
        toolbar.setBackgroundColor(this.getResources().getColor(R.color._ffffff));
        iv_scan.setImageResource(R.mipmap.class_icon_scan_nor);
        btnIm.setImageResource(R.mipmap.set_icon_message_nor);
        tvSearch.setBackgroundResource(R.drawable.search_bg);
        iv_settop.setVisibility(View.GONE);
    }

    private final void  onScrolledUp() {
        Log.e(TAG, " onScrolledUp ");
        toolbar.setBackgroundColor(this.getResources().getColor(R.color._ffffff));
        iv_scan.setImageResource(R.mipmap.class_icon_scan_nor);
        btnIm.setImageResource(R.mipmap.set_icon_message_nor);
        tvSearch.setBackgroundResource(R.drawable.search_bg);
        iv_settop.setVisibility(View.GONE);
    }

    private final void onScrolledToBottom() {
        toolbar.setBackgroundColor(this.getResources().getColor(R.color.translucent));
        iv_scan.setImageResource(R.mipmap.home_icon_scan_nor);
        btnIm.setImageResource(R.mipmap.home_icon_message_nor);
        tvSearch.setBackgroundResource(R.drawable.home_seach_bg);
        iv_settop.setVisibility(View.GONE);
    }

    private final void onScrolledToTop() {
        toolbar.setBackgroundColor(this.getResources().getColor(R.color._ffffff));
        iv_scan.setImageResource(R.mipmap.class_icon_scan_nor);
        btnIm.setImageResource(R.mipmap.set_icon_message_nor);
        tvSearch.setBackgroundResource(R.drawable.search_bg);
        iv_settop.setVisibility(View.VISIBLE);
    }

    private final void onScrolling() {
        ll_top.setBackgroundColor(Color.argb(1, 255, 255, 255));
    }

    private void click(){
        recycler_home.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(recyclerView.canScrollVertically(-1)){
                    onScrolledToTop();
                }else if (recyclerView.canScrollVertically(1)) {
                    onScrolledToBottom();
                } else if (dy < 0) {
                    onScrolledUp();
                } else if (dy > 0) {
                    onScrolledDown();
                }
            }
        });

        homeRecyclerAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Bundle bundle=new Bundle();
                bundle.putString("goods_id",recyclerlist.get(position).getGoods_id());
                goToActivity(GoodsActivity.class,bundle);
            }
        });

        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });


        iv_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(v.getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                    Intent intent = new Intent(getActivity(), CaptureActivity.class);
                    startActivityForResult(intent, 100);
                }else {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},PERMISSION_CAMERA_REQUEST_CODE);
                }
            }
        });

        btnIm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.Companion.getInstance().isLogin()){
                    goToActivity(MessageActivity.class);
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString(TypelistActivity.CATE_ID,"");
                bundle.putInt("flag",0);
                goToActivity(SearchGoodsActivity.class,bundle);
            }
        });
        iv_settop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recycler_home.scrollToPosition(0);
                toolbar.setBackgroundColor(getActivity().getResources().getColor(R.color.translucent));
                iv_scan.setImageResource(R.mipmap.home_icon_scan_nor);
                btnIm.setImageResource(R.mipmap.home_icon_message_nor);
                tvSearch.setBackgroundResource(R.drawable.home_seach_bg);
                iv_settop.setVisibility(View.GONE);

            }
        });

    }

    private void getData(){
        if(kyy.equals("") && sate==0){
            DialogUtils.createTipAllLoadDialog(getActivity());
        }
        if(SharedPreferencesUtils.contains(getActivity(),"homeData")){
            String  data= SharedPreferencesUtils.get(getActivity(),"homeData","").toString();
            homeData=gson.fromJson(data,HomeData.class);
            kyy="";
            setData();
        }
        kyy="";
        init();
    }

    //获取数据
    private void init(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_HOME())
                .withLoadPOST(new NetCallBack<HomeData>(){
                    @NotNull
                    @Override
                    public Class<HomeData> getRealType() {
                        return HomeData.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        DialogUtils.moven();
                        btnIm.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogUtils.createTipImageAndTextDialog(getActivity(),getString(R.string.not_network),R.mipmap.png_icon_popup_wrong);
                            }
                        },1000);
                        if(smartRefreshLayout.getState() == RefreshState.Refreshing){
                            smartRefreshLayout.finishRefresh();
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                       if(!SharedPreferencesUtils.contains(getActivity(),"homeData")){
                            error_retry_view.setVisibility(View.VISIBLE);
                            smartRefreshLayout.setVisibility(View.GONE);
                            iv_settop.setVisibility(View.GONE);
                       }
                    }

                    @Override
                    public void onSuccess(@NonNull HomeData data) {
                        if(getActivity()==null){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        smartRefreshLayout.setVisibility(View.VISIBLE);
                        iv_settop.setVisibility(View.VISIBLE);
                        DialogUtils.moven();
                        if(smartRefreshLayout.getState() == RefreshState.Refreshing){
                            smartRefreshLayout.finishRefresh();
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                        homeData=data;
                        String hdata=gson.toJson(data);
                        SharedPreferencesUtils.put(getActivity(),"homeData",hdata);
                        if(homeData.getGoods_list()==null || homeData.getGoods_list().size()==0){
                            smartRefreshLayout.finishLoadMoreWithNoMoreData();
                        }else {
                            setData();
                        }



                    }
                },false,kyy);
    }

    private void setData(){
        if(kyy.equals("")){
            recyclerlist.clear();
            advertising_list.clear();
            lunbolist.clear();
            homeRecyclerAdapter.removeAllHeaderView();
        }
        currlist.clear();
        for(int i=0;i<homeData.getBanner().size();i++){
            lunbolist.add(homeData.getBanner().get(i).getImage());
        }
        for(int i=0;i<homeData.getWeb_html().size();i++){
            advertising_list.add(homeData.getWeb_html().get(i));
        }

        for(int i=0;i<homeData.getGoods_list().size();i++){
            recyclerlist.add(homeData.getGoods_list().get(i));
        }
        currlist.add(homeData.getCurrencytype());
        kyy=homeData.getKyy();
        homeRecyclerAdapter.notifyDataSetChanged();
        if(sate==0){
            setHead();
        }
    }

    private void setHead(){
        bannerview=View.inflate(getActivity(),R.layout.home_recycler_head1_layout,null);
        rl_pull_up_tip = bannerview.findViewById(R.id.rl_pull_up_tip);
        tv_pull_up_tip = bannerview.findViewById(R.id.tv_pull_up_tip);
        two_header.setEnableTwoLevel(true);
        if(Login.Companion.getInstance().is_show_pull_up_tip().equals("1")){
            rl_pull_up_tip.setVisibility(View.VISIBLE);
            ObjectAnimator animation = ObjectAnimator.ofFloat(tv_pull_up_tip, "translationY", 80f);
            animation.setDuration(1000L);
            animation.setRepeatMode(ValueAnimator.REVERSE);
            animation.setRepeatCount(ValueAnimator.INFINITE);
            animation.start();
        }else {
            tv_pull_up_tip.clearAnimation();
            rl_pull_up_tip.setVisibility(View.GONE);
        }
        homeRecyclerAdapter.addHeaderView(bannerview,0);
        if(lunbolist.size()!=0){
            //head1
            banner=(Banner) bannerview.findViewById(R.id.banner);
            screen=Utils.getWindowWidth(getActivity());
            height=ProportionscreenUtils.getHeight(375,170,(int) screen);
            params=banner.getLayoutParams();
            params.height=(int) height;
            banner.setLayoutParams(params);
            //设置banner样式
            banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
            //设置图片加载器
            banner.setImageLoader(new GlideImageLoader());
            //设置图片集合
            banner.setImages(lunbolist);
            //设置banner动画效果
            // banner.setBannerAnimation(Transformer.Accordion);
            //设置轮播时间
            banner.setDelayTime(3000);
            //设置指示器位置（当banner模式中有指示器时）
            banner.setIndicatorGravity(BannerConfig.CENTER);
            //banner设置方法全部调用完毕时最后调用
            banner.start();

            banner.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int position) {
                    type=homeData.getBanner().get(position).getUrl_type();
                    type_id=homeData.getBanner().get(position).getUrl_value();
                    if(!TextUtils.isEmpty(type) && !TextUtils.isEmpty(type_id)){
                        Bundle bundle=new Bundle();
                        if(type.equals("1")){
                            bundle.putString("goods_id",type_id);
                            goToActivity(GoodsActivity.class,bundle);
                        }else if(type.equals("2")){
                            bundle.putString(TypelistActivity.KEYWORD,"");
                            bundle.putString(TypelistActivity.CATE_ID,type_id);
                            goToActivity(TypelistActivity.class,bundle);
                        }else if(type.equals("3")){
                            bundle.putString("brand_id",type_id);
                            goToActivity(BrandDetilsActivity.class,bundle);
                        }else if(type.equals("4")){
                            bundle.putString(TypelistActivity.KEYWORD,type_id);
                            bundle.putString(TypelistActivity.CATE_ID,"");
                            goToActivity(TypelistActivity.class,bundle);
                        }else if(type.equals("5")){
                            bundle.putString("store_id", type_id);
                            goToActivity(StoreActivity.class, bundle);
                        }
                    }

                }
            });
        }

        TextView tv_secondary_market=(TextView) bannerview.findViewById(R.id.tv_secondary_market);
        tv_secondary_market.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(SeacondMarketActivity.class);
            }
        });

        TextView tv_brand=(TextView) bannerview.findViewById(R.id.tv_brand);
        tv_brand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(BrandActivity.class);
            }
        });
        TextView tv_goods_supplier=(TextView) bannerview.findViewById(R.id.tv_goods_supplier);
        tv_goods_supplier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(PowerSupplierActivity.class);
            }
        });
        TextView tv_big_customers=(TextView) bannerview.findViewById(R.id.tv_big_customers);
        tv_big_customers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(CooperationCustomerActivity.class);
            }
        });

        if(advertising_list!=null && advertising_list.size()!=0){
            for(int i=0;i<advertising_list.size();i++){
                if(advertising_list.get(i).getStyle_name().equals("10")){
                    View view=View.inflate(getActivity(),R.layout.home_recycler_head2_layout,null);
                    homeRecyclerAdapter.addHeaderView(view,i+1);
                    //head2
                    if(!TextUtils.isEmpty(advertising_list.get(i).getBg_color())){
                        RelativeLayout relativeLayout=(RelativeLayout) view.findViewById(R.id.relative_head);
                        relativeLayout.setBackgroundColor(Color.parseColor(advertising_list.get(i).getBg_color()));
                        if(advertising_list.get(i).getBg_color().equals("#f6f6f6")){
                            View view_line=(View) view.findViewById(R.id.view);
                            view_line.setVisibility(View.GONE);
                        }
                    }
                    TextView textView=(TextView) view.findViewById(R.id.tv_title);
                    if(!TextUtils.isEmpty(advertising_list.get(i).getWeb_name())){
                        textView.setText(advertising_list.get(i).getWeb_name());
                    }
                    if(!TextUtils.isEmpty(advertising_list.get(i).getTitle_color())){
                        textView.setTextColor(Color.parseColor(advertising_list.get(i).getTitle_color()));
                    }
                    GridLayout gridLayout=(GridLayout) view.findViewById(R.id.grid_home_head);
                    int rowCount = 0;
                    int width = getActivity().getWindowManager().getDefaultDisplay().getWidth()/3;
                    for (int n = 0; n < 2; n++) { // 控制行
                            for(int m=0;m<3;m++){// 控制列
                                GridLayout.Spec rowspec=GridLayout.spec(n);
                                GridLayout.Spec columnspec=GridLayout.spec(m);
                                GridLayout.LayoutParams layoutParams=new GridLayout.LayoutParams(rowspec,columnspec);
                                layoutParams.width=width;
                                layoutParams.height=width;
                                ImageView imageView = new ImageView(getActivity());
                                imageView.setBackgroundColor(getActivity().getResources().getColor(R.color._ffffff));
                                if(advertising_list.get(i).getBrand_list()!=null && advertising_list.get(i).getBrand_list().size()!=0){
                                    if(!TextUtils.isEmpty(advertising_list.get(i).getBrand_list().get(rowCount).getUrl_type()) &&
                                            !TextUtils.isEmpty(advertising_list.get(i).getBrand_list().get(rowCount).getUrl_value())){
                                        type=advertising_list.get(i).getBrand_list().get(rowCount).getUrl_type();
                                        type_id=advertising_list.get(i).getBrand_list().get(rowCount).getUrl_value();
                                        imgclick(imageView,type,type_id);
                                    }
                                    if(!TextUtils.isEmpty(advertising_list.get(i).getBrand_list().get(rowCount).getPic_img())){
                                        Glide.with(getActivity())
                                                .load(advertising_list.get(i).getBrand_list().get(rowCount).getPic_img())
                                                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                                                .into(imageView);
                                    }
                                }
                                rowCount++;
                                Log.i("info","---------rowcount"+rowCount);
                                gridLayout.addView(imageView,layoutParams);
                            }
                    }
                }else if(advertising_list.get(i).getStyle_name().equals("30")){
                    View view=View.inflate(getActivity(),R.layout.home_recycler_head5_layout,null);
                    homeRecyclerAdapter.addHeaderView(view,i+1);
                    //head2
                    if(!TextUtils.isEmpty(advertising_list.get(i).getBg_color())){
                        RelativeLayout relativeLayout=(RelativeLayout) view.findViewById(R.id.relative_head);
                        relativeLayout.setBackgroundColor(Color.parseColor(advertising_list.get(i).getBg_color()));
                        if(advertising_list.get(i).getBg_color().equals("#f6f6f6")){
                            View view_line=(View) view.findViewById(R.id.view);
                            view_line.setVisibility(View.GONE);
                        }
                    }
                    TextView textView=(TextView) view.findViewById(R.id.tv_title);
                    if(!TextUtils.isEmpty(advertising_list.get(i).getWeb_name())){
                        textView.setText(advertising_list.get(i).getWeb_name());
                    }
                    if(!TextUtils.isEmpty(advertising_list.get(i).getTitle_color())){
                        textView.setTextColor(Color.parseColor(advertising_list.get(i).getTitle_color()));
                    }
                    ImageView iv_head5_big=view.findViewById(R.id.iv_head5_big);
                    if(advertising_list.get(i).getAdv().getPic_img()!=null){
                        Glide.with(getActivity()).load(advertising_list.get(i).getAdv().getPic_img())
                                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_rectangle))
                                .into(iv_head5_big);
                        type=advertising_list.get(i).getAdv().getUrl_type();
                        type_id=advertising_list.get(i).getAdv().getUrl_value();
                        imgclick(iv_head5_big,type,type_id);

                    }
                    GridLayout gridLayout=(GridLayout) view.findViewById(R.id.grid_home_head);
                    int rowCount = 0;
                    for (int n = 0; n < 2; n++) { // 控制行
                        for(int m=0;m<3;m++){// 控制列
                            GridLayout.Spec rowspec=GridLayout.spec(n);
                            GridLayout.Spec columnspec=GridLayout.spec(m);
                            GridLayout.LayoutParams layoutParams=new GridLayout.LayoutParams(rowspec,columnspec);
                            View headview=View.inflate(getActivity(),R.layout.item_homehead5_layout,null);
                            ImageView imageView = (ImageView) headview.findViewById(R.id.iv_pic);
                            ViewGroup.LayoutParams params=imageView.getLayoutParams();
                            int width =(Utils.getWindowWidth(getActivity())-Utils.dp2px(getActivity(),60))/3;
                            params.width=width;
                            params.height=width;
                            TextView tv_name=(TextView) headview.findViewById(R.id.tv_name);
                            TextView tv_price=(TextView) headview.findViewById(R.id.tv_price);
                            TextView tv_inquiry=(TextView)headview.findViewById(R.id.tv_inquiry);
                            if(advertising_list.get(i).getGoods_recommend_list()!=null && advertising_list.get(i).getGoods_recommend_list().size()!=0){
                                if(!TextUtils.isEmpty(advertising_list.get(i).getGoods_recommend_list().get(rowCount).getGoods_pic())){
                                    Glide.with(getActivity()).load(advertising_list.get(i).getGoods_recommend_list().get(rowCount).getGoods_pic())
                                            .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_rectangle))
                                            .into(imageView);
                                    final String goodsid=advertising_list.get(i).getGoods_recommend_list().get(rowCount).getGoods_id();
                                    imageView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Bundle bundle=new Bundle();
                                            bundle.putString("goods_id",goodsid);
                                            goToActivity(GoodsActivity.class,bundle);
                                        }
                                    });
                                }
                                if(!TextUtils.isEmpty(advertising_list.get(i).getGoods_recommend_list().get(rowCount).getGoods_name())){
                                    tv_name.setText(advertising_list.get(i).getGoods_recommend_list().get(rowCount).getGoods_name());
                                }
                                //判断是否为询价商品
                                if(advertising_list.get(i).getGoods_recommend_list().get(rowCount).getIs_enquiry().equals("1")){
                                    tv_inquiry.setVisibility(View.VISIBLE);
                                    tv_price.setVisibility(View.GONE);
                                }else {
                                    tv_inquiry.setVisibility(View.GONE);
                                    tv_price.setVisibility(View.VISIBLE);
                                    tv_price.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(advertising_list.get(i).getGoods_recommend_list().get(rowCount).getGoods_price())));
                                }

                            }
                            rowCount++;
                            gridLayout.addView(headview,layoutParams);
                        }
                    }

                }else if(advertising_list.get(i).getStyle_name().equals("2")){
                    View view=View.inflate(getActivity(),R.layout.home_recycler_head3_layout,null);
                    homeRecyclerAdapter.addHeaderView(view,i+1);
                    //head3
                    if(!TextUtils.isEmpty(advertising_list.get(i).getBg_color())){
                        RelativeLayout relativeLayout=(RelativeLayout) view.findViewById(R.id.relative_head);
                        relativeLayout.setBackgroundColor(Color.parseColor(advertising_list.get(i).getBg_color()));
                        if(advertising_list.get(i).getBg_color().equals("#f6f6f6")){
                            View view_line=(View) view.findViewById(R.id.view);
                            view_line.setVisibility(View.GONE);
                        }
                    }
                    TextView textView=(TextView) view.findViewById(R.id.tv_title);
                    if(!TextUtils.isEmpty(advertising_list.get(i).getWeb_name())){
                        textView.setText(advertising_list.get(i).getWeb_name());
                    }
                    if(!TextUtils.isEmpty(advertising_list.get(i).getTitle_color())){
                        textView.setTextColor(Color.parseColor(advertising_list.get(i).getTitle_color()));
                    }

                    ImageView image1_head3=(ImageView) view.findViewById(R.id.iv_big1);
                    int wind=Utils.getWindowWidth(getActivity())-29;
                    float proportion= (float)(1.0/2.0);
                    wideth=wind*proportion;
                    height=ProportionscreenUtils.getHeight(173,110,(int) wideth);
                    params=image1_head3.getLayoutParams();
                    params.height=(int)height;
                    image1_head3.setLayoutParams(params);
                    //
                    if(advertising_list.get(i).getRecommend_list()!=null && advertising_list.get(i).getRecommend_list().size()>0){
                        Glide.with(getActivity())
                                .load(advertising_list.get(i).getRecommend_list().get(0).getPic_img())
                                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_rectangle))
                                .into(image1_head3);
                        type=advertising_list.get(i).getRecommend_list().get(0).getUrl_type();
                        type_id=advertising_list.get(i).getRecommend_list().get(0).getUrl_value();
                        imgclick(image1_head3,type,type_id);
                    }

                    ImageView image2_head3=(ImageView) view.findViewById(R.id.iv_big2);
                    params=image2_head3.getLayoutParams();
                    params.height=(int)height;
                    image2_head3.setLayoutParams(params);
                    if(advertising_list.get(i).getRecommend_list().size()>=2){
                            Glide.with(getActivity())
                                    .load(advertising_list.get(i).getRecommend_list().get(1).getPic_img())
                                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_rectangle))
                                    .into(image2_head3);
                            type=advertising_list.get(i).getRecommend_list().get(1).getUrl_type();
                            type_id=advertising_list.get(i).getRecommend_list().get(1).getUrl_value();
                            imgclick(image2_head3,type,type_id);

                        LinearLayout linearLayout=(LinearLayout)view.findViewById(R.id.line_head3);
                        for(int n=2;n<advertising_list.get(i).getRecommend_list().size();n++){
                            View hotview = View.inflate(getActivity(),R.layout.item_homehead_hot_layout,null);
                            ImageView imageView=(ImageView) hotview.findViewById(R.id.iv_item_head);
                            Glide.with(getActivity())
                                    .load(advertising_list.get(i).getRecommend_list().get(n).getPic_img())
                                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_column))
                                    .into(imageView);
                            type=advertising_list.get(i).getRecommend_list().get(n).getUrl_type();
                            type_id=advertising_list.get(i).getRecommend_list().get(n).getUrl_value();
                            imgclick(imageView,type,type_id);
                            linearLayout.addView(hotview);
                        }
                    }

                }else if(advertising_list.get(i).getStyle_name().equals("20")){
                    View view=View.inflate(getActivity(),R.layout.home_recycler_head6_layout,null);
                    homeRecyclerAdapter.addHeaderView(view,i+1);
                    if(!TextUtils.isEmpty(advertising_list.get(i).getBg_color())){
                        RelativeLayout relativeLayout=(RelativeLayout) view.findViewById(R.id.relative_head);
                        relativeLayout.setBackgroundColor(Color.parseColor(advertising_list.get(i).getBg_color()));
                        if(advertising_list.get(i).getBg_color().equals("#f6f6f6")){
                            View view_line=(View) view.findViewById(R.id.view);
                            view_line.setVisibility(View.GONE);
                        }
                    }
                    TextView textView=(TextView) view.findViewById(R.id.tv_title);
                    if(!TextUtils.isEmpty(advertising_list.get(i).getWeb_name())){
                        textView.setText(advertising_list.get(i).getWeb_name());
                    }
                    if(!TextUtils.isEmpty(advertising_list.get(i).getTitle_color())){
                        textView.setTextColor(Color.parseColor(advertising_list.get(i).getTitle_color()));
                    }
                    if(advertising_list.get(i).getRecommend_list()!=null && advertising_list.get(i).getRecommend_list().size()>0){
                        ImageView big_image=(ImageView) view.findViewById(R.id.iv_big);
                        screen=Utils.getWindowWidth(getActivity())-24;
                        height=ProportionscreenUtils.getHeight(351,160,screen);
                        params=big_image.getLayoutParams();
                        params.height=(int)height;
                        big_image.setLayoutParams(params);
                        if(!TextUtils.isEmpty(advertising_list.get(i).getRecommend_list().get(0).getPic_img())){
                            Glide.with(getActivity())
                                    .load(advertising_list.get(i).getRecommend_list().get(0).getPic_img())
                                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_rectangle))
                                    .into(big_image);
                        }

                        if(!TextUtils.isEmpty(advertising_list.get(i).getRecommend_list().get(0).getUrl_type()) &&
                                !TextUtils.isEmpty(advertising_list.get(i).getRecommend_list().get(0).getUrl_value())){
                            type=advertising_list.get(i).getRecommend_list().get(0).getUrl_type();
                            type_id=advertising_list.get(i).getRecommend_list().get(0).getUrl_value();
                            imgclick(big_image,type,type_id);
                        }
                        LinearLayout linearLayout=(LinearLayout) view.findViewById(R.id.line_head6);
                        for(int n=1;n<advertising_list.get(i).getRecommend_list().size();n++){
                            View hotview = View.inflate(getActivity(),R.layout.item_homehead_hot_layout,null);
                            ImageView imageView=(ImageView) hotview.findViewById(R.id.iv_item_head);
                            Glide.with(getActivity())
                                    .load(advertising_list.get(i).getRecommend_list().get(n).getPic_img())
                                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_column))
                                    .into(imageView);
                            type=advertising_list.get(i).getRecommend_list().get(n).getUrl_type();
                            type_id=advertising_list.get(i).getRecommend_list().get(n).getUrl_value();
                            imgclick(imageView,type,type_id);
                            linearLayout.addView(hotview);
                        }

                    }

                }else if(advertising_list.get(i).getStyle_name().equals("21")){
                    View view=View.inflate(getActivity(),R.layout.home_recycler_head5_layout,null);
                    homeRecyclerAdapter.addHeaderView(view,i+1);
                    if(!TextUtils.isEmpty(advertising_list.get(i).getBg_color())){
                        RelativeLayout relativeLayout=(RelativeLayout) view.findViewById(R.id.relative_head);
                        relativeLayout.setBackgroundColor(Color.parseColor(advertising_list.get(i).getBg_color()));
                        if(advertising_list.get(i).getBg_color().equals("#f6f6f6")){
                            View view_line=(View) view.findViewById(R.id.view);
                            view_line.setVisibility(View.GONE);
                        }
                    }
                    TextView textView=(TextView) view.findViewById(R.id.tv_title);
                    if(!TextUtils.isEmpty(advertising_list.get(i).getWeb_name())){
                        textView.setText(advertising_list.get(i).getWeb_name());
                    }
                    if(!TextUtils.isEmpty(advertising_list.get(i).getTitle_color())){
                        textView.setTextColor(Color.parseColor(advertising_list.get(i).getTitle_color()));
                    }
                    if(advertising_list.get(i).getRecommend_list()!=null && advertising_list.get(i).getRecommend_list().size()>0){
                        ImageView big_image=(ImageView) view.findViewById(R.id.iv_head5_big);
                        screen=Utils.getWindowWidth(getActivity())-24;
                        height=ProportionscreenUtils.getHeight(351,160,screen);
                        params=big_image.getLayoutParams();
                        params.height=(int)height;
                        big_image.setLayoutParams(params);
                        if(!TextUtils.isEmpty(advertising_list.get(i).getRecommend_list().get(0).getPic_img())){
                            Glide.with(getActivity())
                                    .load(advertising_list.get(i).getRecommend_list().get(0).getPic_img())
                                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_rectangle))
                                    .into(big_image);
                        }

                        if(!TextUtils.isEmpty(advertising_list.get(i).getRecommend_list().get(0).getUrl_type()) &&
                                !TextUtils.isEmpty(advertising_list.get(i).getRecommend_list().get(0).getUrl_value())){
                            type=advertising_list.get(i).getRecommend_list().get(0).getUrl_type();
                            type_id=advertising_list.get(i).getRecommend_list().get(0).getUrl_value();
                            imgclick(big_image,type,type_id);
                        }

                        GridLayout gridLayout=(GridLayout) view.findViewById(R.id.grid_home_head);
                        int rowCount = 1;
                        int width = getActivity().getWindowManager().getDefaultDisplay().getWidth()/3;
                        for (int n = 0; n < 2; n++) { // 控制行
                            for(int m=0;m<3;m++){// 控制列
                                GridLayout.Spec rowspec=GridLayout.spec(n);
                                GridLayout.Spec columnspec=GridLayout.spec(m);
                                GridLayout.LayoutParams layoutParams=new GridLayout.LayoutParams(rowspec,columnspec);
                                layoutParams.width=width;
                                layoutParams.height=width;
                                ImageView imageView = new ImageView(getActivity());
                                imageView.setBackgroundColor(getActivity().getResources().getColor(R.color._ffffff));
                                if(!TextUtils.isEmpty(advertising_list.get(i).getRecommend_list().get(rowCount).getUrl_type()) &&
                                        !TextUtils.isEmpty(advertising_list.get(i).getRecommend_list().get(rowCount).getUrl_value())){
                                    type=advertising_list.get(i).getRecommend_list().get(rowCount).getUrl_type();
                                    type_id=advertising_list.get(i).getRecommend_list().get(rowCount).getUrl_value();
                                    imgclick(imageView,type,type_id);
                                }
                                if(!TextUtils.isEmpty(advertising_list.get(i).getRecommend_list().get(rowCount).getPic_img())){
                                    Glide.with(getActivity())
                                            .load(advertising_list.get(i).getRecommend_list().get(rowCount).getPic_img())
                                            .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                                            .into(imageView);
                                }
                                rowCount++;
                                Log.i("info","---------rowcount"+rowCount);
                                gridLayout.addView(imageView,layoutParams);
                            }
                        }

                    }
                }else if(advertising_list.get(i).getStyle_name().equals("3")){

                    View view=View.inflate(getActivity(),R.layout.home_recycler_head4_layout,null);
                    homeRecyclerAdapter.addHeaderView(view,i+1);
                    if(!TextUtils.isEmpty(advertising_list.get(i).getBg_color())){
                        RelativeLayout relativeLayout=(RelativeLayout) view.findViewById(R.id.relative_head);
                        relativeLayout.setBackgroundColor(Color.parseColor(advertising_list.get(i).getBg_color()));
                        if(advertising_list.get(i).getBg_color().equals("#f6f6f6")){
                            View view_line=(View) view.findViewById(R.id.view);
                            view_line.setVisibility(View.GONE);
                        }
                    }
                    TextView textView=(TextView) view.findViewById(R.id.tv_title);
                    if(!TextUtils.isEmpty(advertising_list.get(i).getWeb_name())){
                        textView.setText(advertising_list.get(i).getWeb_name());
                    }
                    if(!TextUtils.isEmpty(advertising_list.get(i).getTitle_color())){
                        textView.setTextColor(Color.parseColor(advertising_list.get(i).getTitle_color()));
                    }

                    ImageView image_head4=(ImageView) view.findViewById(R.id.iv_big);
                    if(!TextUtils.isEmpty(advertising_list.get(i).getAdv().getPic_img())){
                        Glide.with(getActivity()).load(advertising_list.get(i).getAdv().getPic_img())
                                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_rectangle))
                                .into(image_head4);
                        type=advertising_list.get(i).getAdv().getUrl_type();
                        type_id=advertising_list.get(i).getAdv().getUrl_value();
                        imgclick(image_head4,type,type_id);
                    }
                    screen=Utils.getWindowWidth(getActivity())-24;
                    height=ProportionscreenUtils.getHeight(351,160,screen);
                    params=image_head4.getLayoutParams();
                    params.height=(int)height;
                    image_head4.setLayoutParams(params);

                    LinearLayout line_decoration=view.findViewById(R.id.line_head4);
                    if(advertising_list.get(i).getGoods_recommend_list()!=null && advertising_list.get(i).getGoods_recommend_list().size()!=0){
                        for(int n=0;n<advertising_list.get(i).getGoods_recommend_list().size();n++){
                            View hotview = View.inflate(getActivity(),R.layout.item_homehead_advertising_hot_layout,null);
                            TextView nameview=hotview.findViewById(R.id.tv_name);
                            nameview.setText(advertising_list.get(i).getGoods_recommend_list().get(n).getGoods_name());
                            ImageView imageView=(ImageView) hotview.findViewById(R.id.iv_pic);
                            Glide.with(getActivity())
                                    .load(advertising_list.get(i).getGoods_recommend_list().get(n).getGoods_pic())
                                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_column))
                                    .into(imageView);
                            TextView priceview=(TextView) hotview.findViewById(R.id.tv_price);
                            TextView dollarview=(TextView) hotview.findViewById(R.id.tv_dollar);
                            TextView inquiryview=hotview.findViewById(R.id.tv_inquiry);
                            //判断是否为询价商品
                            if(advertising_list.get(i).getGoods_recommend_list().get(n).getIs_enquiry().equals("1")){
                                inquiryview.setVisibility(View.VISIBLE);
                                priceview.setVisibility(View.GONE);
                                dollarview.setVisibility(View.GONE);
                            }else {
                                dollarview.setVisibility(View.GONE);
                                inquiryview.setVisibility(View.GONE);
                                priceview.setVisibility(View.VISIBLE);
                                priceview.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(advertising_list.get(i).getGoods_recommend_list().get(n).getGoods_price())));
                            }

                            line_decoration.addView(hotview);
                            final String goodsid=advertising_list.get(i).getGoods_recommend_list().get(n).getGoods_id();
                            imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Bundle bundle=new Bundle();
                                    bundle.putString("goods_id",goodsid);
                                    goToActivity(GoodsActivity.class,bundle);
                                }
                            });
                        }
                    }
                }else {
                    View headView=View.inflate(getActivity(),R.layout.fragment_home_comm_title,null);
                    TextView titleView=(TextView) headView.findViewById(R.id.tv_title);
                    titleView.setText(advertising_list.get(i).getWeb_name());
                    homeRecyclerAdapter.addHeaderView(headView,i+1);
                }
            }
         }
        //head6
        if(recyclerlist.size()!=0){
            View headView=View.inflate(getActivity(),R.layout.fragment_home_comm_title,null);
            TextView titleView=(TextView) headView.findViewById(R.id.tv_title);
            titleView.setText(getString(R.string.youlike));
            View view_line=(View) headView.findViewById(R.id.view);
            view_line.setVisibility(View.GONE);
            homeRecyclerAdapter.addHeaderView(headView,advertising_list.size()+1);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSION_CAMERA_REQUEST_CODE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                goToActivity(com.zxing_android.CaptureActivity.class);
            }else {
                Toast.makeText(getContext(),getString(R.string.tip_permission_camera),Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initrefresh(){
        smartRefreshLayout.setOnMultiPurposeListener(new SimpleMultiPurposeListener(){
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                smartRefreshLayout.setNoMoreData(false);
                sate=1;
                init();
            }
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                smartRefreshLayout.setNoMoreData(false);
                sate=0;
                kyy="";
                init();
            }
            @Override
            public void onHeaderMoving(RefreshHeader header, boolean isDragging, float percent, int offset, int headerHeight, int maxDragHeight) {
                rl_second_floor.setTranslationY(Math.min(offset - rl_second_floor.getHeight(), smartRefreshLayout.getLayout().getHeight() - rl_second_floor.getHeight()));
            }

        });
        two_header.setOnTwoLevelListener(new OnTwoLevelListener() {
            @Override
            public boolean onTwoLevel(@NonNull RefreshLayout refreshLayout) {
                EventBus.getDefault().post(new MessageEvent(MessageEvent.TO_PROMOTIONAL_VIDEO_ACTIVITY));
                if(Login.Companion.getInstance().is_show_pull_up_tip().equals("1")){
                    Login.Companion.getInstance().set_show_pull_up_tip("0");
                    rl_pull_up_tip.setVisibility(View.GONE);
                }
                return false;
            }
        });
    }

    private void imgclick(View view, final String type, final String type_id){
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(type) && !TextUtils.isEmpty(type_id)){
                    Bundle bundle=new Bundle();
                    if(type.equals("1")){
                        bundle.putString("goods_id",type_id);
                        goToActivity(GoodsActivity.class,bundle);
                    }else if(type.equals("2")){
                        bundle.putString(TypelistActivity.KEYWORD,"");
                        bundle.putString(TypelistActivity.CATE_ID,type_id);
                        goToActivity(TypelistActivity.class,bundle);
                    }else if(type.equals("3")){
                        bundle.putString("brand_id",type_id);
                        goToActivity(BrandDetilsActivity.class,bundle);
                    }else if(type.equals("4")){
                        bundle.putString(TypelistActivity.KEYWORD,type_id);
                        bundle.putString(TypelistActivity.CATE_ID,"");
                        goToActivity(TypelistActivity.class,bundle);
                    }else if(type.equals("5")){
                        bundle.putString("store_id", type_id);
                        goToActivity(StoreActivity.class, bundle);
                    }
                }
            }
        });
    }

    //加载热搜词
    private void gethotsearch(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_SEARCH_HOT())
                .withPOST(new NetCallBack<SearchHotWordBean>() {

                    @NotNull
                    @Override
                    public Class<SearchHotWordBean> getRealType() {
                        return SearchHotWordBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {

                    }

                    @Override
                    public void onSuccess(@NonNull SearchHotWordBean searchHotWordBean) {
                        if(searchHotWordBean!=null && searchHotWordBean.getSearch().size()!=0){
                            int m=Utils.getIntRandom(searchHotWordBean.getSearch().size());
                            Log.i("info","-------m"+m);
                            tvSearch.setText(searchHotWordBean.getSearch().get(Utils.getIntRandom(searchHotWordBean.getSearch().size())).getName());
                        }
                    }
                },false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isHidden()){
//            gethotsearch();
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            kyy="";
            sate=0;
            init();
//            gethotsearch();
        }
    }

    @Override
    public void updateUnreadMsg() {
        showUnreadMsgNum();
    }

    private void showUnreadMsgNum() {
        if(Login.Companion.getInstance().isLogin()){
            mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
        }
    }

}
