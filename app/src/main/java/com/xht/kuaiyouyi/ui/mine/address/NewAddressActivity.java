package com.xht.kuaiyouyi.ui.mine.address;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.login.entity.AreaCodeBean;
import com.xht.kuaiyouyi.ui.mine.entity.CheckAddressListBean;
import com.xht.kuaiyouyi.ui.mine.entity.NewAddressBean;
import com.xht.kuaiyouyi.ui.mine.entity.ReceiveAddressListBean;
import com.xht.kuaiyouyi.utils.CommonPopupWindow;
import com.xht.kuaiyouyi.utils.CommonUtil;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NewAddressActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_left)
    ImageView iv_back;
    @BindView(R.id.tv_right)
    TextView tv_right;

    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.tv_mobile_area_code)
    TextView tv_mobile_area_code;
    @BindView(R.id.et_mobile_phone)
    EditText et_mobile_phone;
    @BindView(R.id.tv_standby_area_code)
    TextView tv_standby_area_code;
    @BindView(R.id.et_standby_phone)
    EditText et_standby_phone;
    @BindView(R.id.tv_telephone_area_code)
    TextView tv_telephone_area_code;
    @BindView(R.id.et_telephone)
    EditText et_telephone;
    @BindView(R.id.et_zone_code)
    EditText et_zone_code;
    @BindView(R.id.tv_10)
    TextView tv_10;
    @BindView(R.id.rl_area)
    RelativeLayout rl_area;
    @BindView(R.id.et_address)
    EditText et_address;
    @BindView(R.id.switch_default)
    Switch switch_default;
    @BindView(R.id.tv_selected_area)
    TextView tv_selected_area;
    @BindView(R.id.btn_baocun)
    Button btn_baocun;
    @BindView(R.id.tv_setting_default)
    TextView tv_setting_default;

    private int mDefault;
    private CommonPopupWindow popupWindow;
    private AddressPickerView addressPickerView;

    private int mType;
    public static final int TYPE_NEW_RECEIVE_ADDRESS = 0;
    public static final int TYPE_EDIT_RECEIVE_ADDRESS = 1;
    public static final int TYPE_NEW_CHECK_ADDRESS = 2;
    public static final int TYPE_EDIT_CHECK_ADDRESS = 3;

    //编辑地址时，上个页面传来的数据
    public static final String RECEIVE_ADDRESS_BEAN = "receive_address_bean";
    public static final String CHECK_ADDRESS_BEAN = "check_address_bean";

    private String mMobileAreaCode = "86";
    private String mStandbyAreaCode = "86";
    private String mTelephoneAreaCode = "86";
    private OptionsPickerView mOptionsPickerView;
    private final String[] AREA_CODE = {"86", "853", "852"};
    private List<String> mAreaDisplayList;
    private List<AreaCodeBean> mPickerViewData;

    private CheckAddressListBean.AddressBean mCheckAddressBean;
    private ReceiveAddressListBean.AddressBean mReceiveAddressBean;

    private boolean mMobileSelected = false;
    private boolean mStandbySelected = false;
    private boolean mTelephoneSelect = false;

    private String mAreaInfoId="";


    @Override
    protected int getLayout() {
        return R.layout.activity_newaddress;
    }

    @Override
    protected void initView() {
        mType = getIntent().getIntExtra(Contant.TYPE, 0);
        tv_mobile_area_code.setText("+" + mMobileAreaCode);
        tv_standby_area_code.setText("+" + mStandbyAreaCode);
        tv_telephone_area_code.setText("+" + mTelephoneAreaCode);
        click();

        switch (mType) {
            case TYPE_NEW_RECEIVE_ADDRESS:
                tv_title.setText(R.string.new_receive_address);
                tv_name.setText(R.string.newaddress_name);
                tv_right.setVisibility(View.GONE);
                break;
            case TYPE_EDIT_RECEIVE_ADDRESS:
                tv_title.setText(R.string.edit_receive_address);
                tv_name.setText(R.string.newaddress_name);
                mReceiveAddressBean = (ReceiveAddressListBean.AddressBean) getIntent().getSerializableExtra(RECEIVE_ADDRESS_BEAN);
                mAreaInfoId = mReceiveAddressBean.getArea_id()+"_"+mReceiveAddressBean.getCity_id();
                if(!TextUtils.isEmpty(mReceiveAddressBean.getCity_id_2())){
                    mAreaInfoId=mAreaInfoId + "_"+mReceiveAddressBean.getCity_id_2();
                }
                tv_right.setText(R.string.delete);
                tv_right.setVisibility(View.VISIBLE);
                showData(mReceiveAddressBean);
                break;
            case TYPE_NEW_CHECK_ADDRESS:
                tv_title.setText(R.string.new_check_address);
                tv_name.setText(R.string.contact_name);
                tv_setting_default.setText(R.string.setting_default_check_address);
                tv_right.setVisibility(View.GONE);
                break;
            case TYPE_EDIT_CHECK_ADDRESS:
                tv_title.setText(R.string.edit_check_address);
                tv_name.setText(R.string.contact_name);
                tv_setting_default.setText(R.string.setting_default_check_address);
                mCheckAddressBean = (CheckAddressListBean.AddressBean) getIntent().getSerializableExtra(CHECK_ADDRESS_BEAN);
                mAreaInfoId = mCheckAddressBean.getF_area_id()+"_"+mCheckAddressBean.getF_city_id();
                if(!TextUtils.isEmpty(mCheckAddressBean.getF_city_id_2())){
                    mAreaInfoId=mAreaInfoId+"_"+mCheckAddressBean.getF_city_id_2();
                }
                tv_right.setText(R.string.delete);
                tv_right.setVisibility(View.VISIBLE);
                showData(mCheckAddressBean);
                break;
        }
    }

    /**
     * 显示收支票地址编辑的信息
     *
     * @param checkAddressBean 上一页面传进来的已有信息
     */
    private void showData(CheckAddressListBean.AddressBean checkAddressBean) {
        et_name.setText(checkAddressBean.getAddress_check_true_name());
        tv_mobile_area_code.setText("+" + checkAddressBean.getMobile_zone1());
        mMobileAreaCode = checkAddressBean.getMobile_zone1();
        et_mobile_phone.setText(checkAddressBean.getAddress_check_phone());
        if (!TextUtils.isEmpty(checkAddressBean.getAddress_check_phone_spare())) {
            et_standby_phone.setText(checkAddressBean.getAddress_check_phone_spare());
            tv_standby_area_code.setText("+" + checkAddressBean.getMobile_zone1_spare());
            mStandbyAreaCode = checkAddressBean.getMobile_zone1_spare();
        }
        if(!TextUtils.isEmpty(checkAddressBean.getMobile_zone1_spare())){
            mStandbyAreaCode = checkAddressBean.getMobile_zone1_spare();
        }
        if (!TextUtils.isEmpty(checkAddressBean.getTel_phone())) {
            et_telephone.setText(checkAddressBean.getTel_phone());
            tv_telephone_area_code.setText("+" + checkAddressBean.getMobile_zone2());
            mTelephoneAreaCode = checkAddressBean.getMobile_zone2();
            if (mTelephoneAreaCode.equals(AREA_CODE[0])) {
                et_zone_code.setText(checkAddressBean.getTel_zone());
            } else {
                et_zone_code.setVisibility(View.GONE);
                tv_10.setVisibility(View.GONE);
            }
        } else {
            et_zone_code.setVisibility(View.VISIBLE);
            tv_10.setVisibility(View.VISIBLE);
        }
        tv_selected_area.setText(checkAddressBean.getAddress_check_area_info());
        et_address.setText(checkAddressBean.getAddress_check_address());
        if (checkAddressBean.getIs_default().equals("1")) {
            switch_default.setChecked(true);
        } else {
            switch_default.setChecked(false);
        }
    }

    /**
     * 显示收货地址编辑的信息
     *
     * @param receiveAddressBean 上一页面传进来的已有信息
     */
    private void showData(ReceiveAddressListBean.AddressBean receiveAddressBean) {
        et_name.setText(receiveAddressBean.getTrue_name());
        tv_mobile_area_code.setText("+" + receiveAddressBean.getMobile_zone1());
        mMobileAreaCode = receiveAddressBean.getMobile_zone1();
        et_mobile_phone.setText(receiveAddressBean.getMob_phone());
        if (!TextUtils.isEmpty(receiveAddressBean.getMob_phone_spare())) {
            et_standby_phone.setText(receiveAddressBean.getMob_phone_spare());
            tv_standby_area_code.setText("+" + receiveAddressBean.getMobile_zone1_spare());
        }

        if(!TextUtils.isEmpty(receiveAddressBean.getMobile_zone1_spare())){
            mStandbyAreaCode = receiveAddressBean.getMobile_zone1_spare();
        }

        if (!TextUtils.isEmpty(receiveAddressBean.getTel_phone())) {
            et_telephone.setText(receiveAddressBean.getTel_phone());
            tv_telephone_area_code.setText("+" + receiveAddressBean.getMobile_zone2());
            mTelephoneAreaCode = receiveAddressBean.getMobile_zone2();
            if (mTelephoneAreaCode.equals(AREA_CODE[0])) {
                et_zone_code.setText(receiveAddressBean.getTel_zone());
            } else {
                et_zone_code.setVisibility(View.GONE);
                tv_10.setVisibility(View.GONE);
            }
        } else {
            et_zone_code.setVisibility(View.VISIBLE);
            tv_10.setVisibility(View.VISIBLE);
        }
        tv_selected_area.setText(receiveAddressBean.getArea_info());
        et_address.setText(receiveAddressBean.getAddress());
        if (receiveAddressBean.getIs_default().equals("1")) {
            switch_default.setChecked(true);
        } else {
            switch_default.setChecked(false);
        }
    }

    private void click() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tip = "";
                if(mType==TYPE_EDIT_CHECK_ADDRESS){
                    tip = getResources().getString(R.string.are_you_sure_delete_check_address);
                }else if(mType==TYPE_EDIT_RECEIVE_ADDRESS){
                    tip = getResources().getString(R.string.are_you_sure_delete_receive_address);
                }
                DialogUtils.createTwoBtnDialog(NewAddressActivity.this,
                        tip,
                        getResources().getString(R.string.dialog_confirm),
                        getResources().getString(R.string.dialog_cancel),
                        new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(Dialog dialog) {
                                requestAddressDelete();
                            }
                        },null,false,true);
            }
        });
        rl_area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null && popupWindow.isShowing()) return;
                final View upView = LayoutInflater.from(NewAddressActivity.this).inflate(R.layout.pop_address_layout, null);
                //测量View的宽高
                CommonUtil.measureWidthAndHeight(upView);
                popupWindow = new CommonPopupWindow.Builder(NewAddressActivity.this)
                        .setView(upView)
                        .setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                        .setBackGroundLevel(0.8f)//取值范围0.0f-1.0f 值越小越暗
                        .create();
                popupWindow.showAtLocation(upView, Gravity.BOTTOM, 0, 0);
                addressPickerView = (AddressPickerView) upView.findViewById(R.id.apvAddress);
                addressPickerView.setOnAddressPickerSure(new AddressPickerView.OnAddressPickerSureListener() {
                    @Override
                    public void onSureClick(String address, String provinceCode, String cityCode, String districtCode) {
                        tv_selected_area.setText(address);
                        mAreaInfoId=provinceCode+"_"+cityCode;
                        if(!TextUtils.isEmpty(districtCode)){
                            mAreaInfoId=mAreaInfoId + "_"+districtCode;
                        }
                        popupWindow.dismiss();
                    }
                });
            }
        });

        switch_default.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mDefault = 1;
                } else {
                    mDefault = 0;
                }
            }
        });

        btn_baocun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = et_name.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(NewAddressActivity.this, R.string.name_empty_tip, Toast.LENGTH_SHORT).show();
                    return;
                }
                String mobile_phone = et_mobile_phone.getText().toString().trim();
                if (TextUtils.isEmpty(mobile_phone)) {
                    Toast.makeText(NewAddressActivity.this, R.string.mobile_phone_empty_tip, Toast.LENGTH_SHORT).show();
                    return;
                }
                String selected_area = tv_selected_area.getText().toString().trim();
                if (TextUtils.isEmpty(selected_area)) {
                    Toast.makeText(NewAddressActivity.this, R.string.area_empty_tip, Toast.LENGTH_SHORT).show();
                    return;
                }
                String address = et_address.getText().toString().trim();
                if (TextUtils.isEmpty(address)) {
                    Toast.makeText(NewAddressActivity.this, R.string.address_empty_tip, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!TextUtils.isEmpty(et_telephone.getText().toString().trim()) &&mTelephoneAreaCode.equals(AREA_CODE[0])&&
                        TextUtils.isEmpty(et_zone_code.getText().toString().trim())) {
                    Toast.makeText(NewAddressActivity.this, R.string.zone_code_empty_tip, Toast.LENGTH_SHORT).show();
                    return;
                }

                String telephone = et_telephone.getText().toString().trim();
                String standbyMobile = et_standby_phone.getText().toString().trim();

                String teleAreaCode = "";
                String teleZone = "";
                String standbyAreaCode = "";
                if (!TextUtils.isEmpty(telephone)) {
                    teleAreaCode = mTelephoneAreaCode;
                    if(mTelephoneAreaCode.equals(AREA_CODE[0])){
                        teleZone = et_zone_code.getText().toString().trim();
                    }
                }
                if (!TextUtils.isEmpty(standbyMobile)) {
                    standbyAreaCode = mStandbyAreaCode;
                }

                if (mType == TYPE_NEW_RECEIVE_ADDRESS) {
                    requestSaveReceiveAddress(name, mAreaInfoId, address, telephone, mobile_phone,
                            mDefault + "", mMobileAreaCode, teleAreaCode,
                            teleZone, standbyAreaCode, standbyMobile);
                }
                if (mType == TYPE_NEW_CHECK_ADDRESS) {
                    requestSaveCheckAddress(name, mAreaInfoId, address, telephone, mobile_phone,
                            mDefault + "", mMobileAreaCode, teleAreaCode, teleZone,
                            standbyAreaCode, standbyMobile);
                }
                if (mType == TYPE_EDIT_RECEIVE_ADDRESS) {
                    requestEditReceiveAddress(mReceiveAddressBean.getAddress_id(), name, address,
                            mAreaInfoId, telephone, mobile_phone, mDefault + "",
                            mMobileAreaCode, teleAreaCode, teleZone, standbyAreaCode, standbyMobile);
                }
                if (mType == TYPE_EDIT_CHECK_ADDRESS) {
                    requestEditCheckAddress(mCheckAddressBean.getAddress_check_id(), name,
                            mAreaInfoId, address, telephone, mobile_phone, mDefault + "",
                            mMobileAreaCode, teleAreaCode, teleZone, standbyAreaCode, standbyMobile);
                }

            }
        });

        tv_mobile_area_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMobileSelected = true;
                mStandbySelected = false;
                mTelephoneSelect = false;
                selectAreaCode();
            }
        });
        tv_standby_area_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMobileSelected = false;
                mStandbySelected = true;
                mTelephoneSelect = false;
                selectAreaCode();
            }
        });
        tv_telephone_area_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMobileSelected = false;
                mStandbySelected = false;
                mTelephoneSelect = true;
                selectAreaCode();
            }
        });

    }

    private void selectAreaCode() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(tv_telephone_area_code.getApplicationWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);

        if (mOptionsPickerView == null) {
            mOptionsPickerView = new OptionsPickerBuilder(NewAddressActivity.this, new OnOptionsSelectListener() {
                @Override
                public void onOptionsSelect(int options1, int options2, int options3, View v) {
                    if (mMobileSelected) {
                        mMobileAreaCode = AREA_CODE[options1];
                        tv_mobile_area_code.setText("+" + mMobileAreaCode);

                        return;
                    }
                    if (mStandbySelected) {
                        mStandbyAreaCode = AREA_CODE[options1];
                        tv_standby_area_code.setText("+" + mStandbyAreaCode);
                        return;
                    }
                    if (mTelephoneSelect) {
                        mTelephoneAreaCode = AREA_CODE[options1];
                        tv_telephone_area_code.setText("+" + mTelephoneAreaCode);
                        if (mTelephoneAreaCode.equals(AREA_CODE[0])) {
                            et_zone_code.setVisibility(View.VISIBLE);
                            tv_10.setVisibility(View.VISIBLE);
                        } else {
                            et_zone_code.setVisibility(View.GONE);
                            tv_10.setVisibility(View.GONE);
                        }
                        return;
                    }
                }
            })
                    .setLayoutRes(R.layout.picker_view_phone_area_code, new CustomListener() {
                        @Override
                        public void customLayout(View v) {
                            TextView tv_cancel = v.findViewById(R.id.tv_cancel);
                            TextView tv_confirm = v.findViewById(R.id.tv_confirm);
                            tv_confirm.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mOptionsPickerView.returnData();

                                    mOptionsPickerView.dismiss();
                                }
                            });
                            tv_cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mOptionsPickerView.dismiss();
                                }
                            });
                        }
                    })
                    .isDialog(false)
                    .setOutSideCancelable(false)
                    .build();
            setPickViewData();
            mOptionsPickerView.setPicker(mPickerViewData);
        }
        mOptionsPickerView.show();
    }

    private void setPickViewData() {
        if (mAreaDisplayList == null) {
            mAreaDisplayList = new ArrayList<>();
            mAreaDisplayList.add(getResources().getString(R.string.cn) + AREA_CODE[0]);
            mAreaDisplayList.add(getResources().getString(R.string.cn_macau) + AREA_CODE[1]);
            mAreaDisplayList.add(getResources().getString(R.string.cn_hk) + AREA_CODE[2]);
        }
        if (mPickerViewData == null) {
            mPickerViewData = new ArrayList<>();
            for (int i = 0; i < AREA_CODE.length; i++) {
                mPickerViewData.add(new AreaCodeBean(mAreaDisplayList.get(i)));
            }
        }

    }

    @Override
    public void onBackPressed() {
        if (mType == TYPE_NEW_RECEIVE_ADDRESS || mType == TYPE_NEW_CHECK_ADDRESS) {
            if(!et_name.getText().toString().equals("")||
                    !et_mobile_phone.getText().toString().equals("")||
                    !et_standby_phone.getText().toString().equals("")||
                    !et_telephone.getText().toString().equals("")||
                    !et_address.getText().toString().equals("")||
                    !mMobileAreaCode.equals(AREA_CODE[0])||
                    !mStandbyAreaCode.equals(AREA_CODE[0])||
                    !mTelephoneAreaCode.equals(AREA_CODE[0])||
                    !tv_selected_area.getText().toString().equals("")||
                    switch_default.isChecked()){
                showTipDialog();
                return;
            }else {
                //当选中的国别号是86，才去对比区号0759
                if(mTelephoneAreaCode.equals(AREA_CODE[0]) && !et_zone_code.getText().toString().equals("")){
                    showTipDialog();
                    return;
                }
            }
        }
        if (mType == TYPE_EDIT_RECEIVE_ADDRESS) {
            if(!et_name.getText().toString().equals(mReceiveAddressBean.getTrue_name())||
                    !et_mobile_phone.getText().toString().equals(mReceiveAddressBean.getMob_phone())||
                    !et_standby_phone.getText().toString().equals(mReceiveAddressBean.getMob_phone_spare()==null?"":mReceiveAddressBean.getMob_phone_spare())||
                    !et_telephone.getText().toString().equals(mReceiveAddressBean.getTel_phone()==null?"":mReceiveAddressBean.getTel_phone())||
                    !et_address.getText().toString().equals(mReceiveAddressBean.getAddress())||
                    !mMobileAreaCode.equals(mReceiveAddressBean.getMobile_zone1())||
                    !mStandbyAreaCode.equals(TextUtils.isEmpty(mReceiveAddressBean.getMobile_zone1_spare())?AREA_CODE[0]:mReceiveAddressBean.getMobile_zone1_spare())||
                    !mTelephoneAreaCode.equals(TextUtils.isEmpty(mReceiveAddressBean.getMobile_zone2())?AREA_CODE[0]:mReceiveAddressBean.getMobile_zone2())||
                    !tv_selected_area.getText().toString().equals(mReceiveAddressBean.getArea_info())||
                    !mReceiveAddressBean.getIs_default().equals(switch_default.isChecked()?"1":"0")){
                showTipDialog();
                return;
            }else {
                //当选中的国别号是86，才去对比区号0759
                if(mTelephoneAreaCode.equals(AREA_CODE[0]) && !et_zone_code.getText().toString().equals(mReceiveAddressBean.getTel_zone()==null?"":mReceiveAddressBean.getTel_zone())){
                    showTipDialog();
                    return;
                }
            }
        }
        if (mType == TYPE_EDIT_CHECK_ADDRESS) {
            if(!et_name.getText().toString().equals(mCheckAddressBean.getAddress_check_true_name())||
                    !et_mobile_phone.getText().toString().equals(mCheckAddressBean.getAddress_check_phone())||
                    !et_standby_phone.getText().toString().equals(mCheckAddressBean.getAddress_check_phone_spare()==null?"":mCheckAddressBean.getAddress_check_phone_spare())||
                    !et_telephone.getText().toString().equals(mCheckAddressBean.getTel_phone()==null?"":mCheckAddressBean.getTel_phone())||
                    !et_address.getText().toString().equals(mCheckAddressBean.getAddress_check_address()==null?"":mCheckAddressBean.getAddress_check_address())||
                    !mMobileAreaCode.equals(mCheckAddressBean.getMobile_zone1())||
                    !mStandbyAreaCode.equals(TextUtils.isEmpty(mCheckAddressBean.getMobile_zone1_spare())?AREA_CODE[0]:mCheckAddressBean.getMobile_zone1_spare())||
                    !mTelephoneAreaCode.equals(TextUtils.isEmpty(mCheckAddressBean.getMobile_zone2())?AREA_CODE[0]:mCheckAddressBean.getMobile_zone2())||
                    !tv_selected_area.getText().toString().equals(mCheckAddressBean.getAddress_check_area_info())||
                    !mCheckAddressBean.getIs_default().equals(switch_default.isChecked()?"1":"0")){
                showTipDialog();
                return;
            }else {
                //当选中的国别号是86，才去对比区号0759
                if(mTelephoneAreaCode.equals(AREA_CODE[0]) && !et_zone_code.getText().toString().equals(mCheckAddressBean.getTel_zone()==null?"":mCheckAddressBean.getTel_zone())){
                    showTipDialog();
                    return;
                }
            }
        }
        super.onBackPressed();

    }

    private void showTipDialog() {
        DialogUtils.createTwoBtnDialog(this, getString(R.string.address_exit_tip), getString(R.string.dialog_confirm), getString(R.string.dialog_cancel), new DialogUtils.OnRightBtnListener() {
            @Override
            public void setOnRightListener(Dialog dialog) {
                finish();
            }
        }, new DialogUtils.OnLeftBtnListener() {
            @Override
            public void setOnLeftListener(Dialog dialog) {
            }
        },false,true);
    }


    /**
     * 增加收货地址
     *
     * @param true_name          名字
     * @param area_info_id          省市区
     * @param address            剩下的街道地址
     * @param tel_phone
     * @param mob_phone
     * @param is_default
     * @param mobile_zone1       手机国别号
     * @param mobile_zone2       固话国别号
     * @param tel_zone           固话电话区号
     * @param mobile_zone1_spare 备用手机国别号
     * @param mob_phone_spare    备用手机号码
     */
    private void requestSaveReceiveAddress(String true_name, String area_info_id, String address,
                                           String tel_phone, String mob_phone, String is_default,
                                           String mobile_zone1, String mobile_zone2,
                                           String tel_zone, String mobile_zone1_spare,
                                           String mob_phone_spare) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_NEW_ADDRESS())
                .addParam("true_name", true_name)
                .addParam("area_info_id", area_info_id)
                .addParam("address", address)
                .addParam("tel_phone", tel_phone)
                .addParam("mob_phone", mob_phone)
                .addParam("is_default", is_default)
                .addParam("mobile_zone1", mobile_zone1)
                .addParam("mobile_zone2", mobile_zone2)
                .addParam("tel_zone", tel_zone)
                .addParam("mobile_zone1_spare", mobile_zone1_spare)
                .addParam("mob_phone_spare", mob_phone_spare)
                .withPOST(new NetCallBack<NewAddressBean>() {

                    @NotNull
                    @Override
                    public Class<NewAddressBean> getRealType() {
                        return NewAddressBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(NewAddressActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull NewAddressBean newAddressBean) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(NewAddressActivity.this, R.string.save_address_succeful, Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);
                        finish();
                    }
                }, false);
    }


    /**
     * @param address_check_true_name
     * @param address_check_area_info_id
     * @param address_check_address
     * @param tel_phone                 固话
     * @param address_check_phone       手机号码
     * @param is_default
     * @param mobile_zone1              手机国别号
     * @param mobile_zone2              固话国别号
     * @param tel_zone                  固定电话区号
     * @param mobile_zone1_spare        备用手机国别号
     * @param address_check_phone_spare 备用手机电话
     */
    private void requestSaveCheckAddress(String address_check_true_name, String address_check_area_info_id, String address_check_address,
                                         String tel_phone, String address_check_phone, String is_default,
                                         String mobile_zone1, String mobile_zone2,
                                         String tel_zone, String mobile_zone1_spare,
                                         String address_check_phone_spare) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_NEW_CHECK_ADDRESS())
                .addParam("address_check_true_name", address_check_true_name)
                .addParam("address_check_area_info_id", address_check_area_info_id)
                .addParam("address_check_address", address_check_address)
                .addParam("address_check_phone", address_check_phone)
                .addParam("is_default", is_default)
                .addParam("tel_phone", tel_phone)
                .addParam("mobile_zone1", mobile_zone1)
                .addParam("mobile_zone2", mobile_zone2)
                .addParam("tel_zone", tel_zone)
                .addParam("mobile_zone1_spare", mobile_zone1_spare)
                .addParam("address_check_phone_spare", address_check_phone_spare)
                .withPOST(new NetCallBack<NewAddressBean>() {

                    @NotNull
                    @Override
                    public Class<NewAddressBean> getRealType() {
                        return NewAddressBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(NewAddressActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull NewAddressBean newAddressBean) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(NewAddressActivity.this, R.string.save_address_succeful, Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);
                        finish();
                    }
                }, false);
    }


    private void requestEditCheckAddress(String address_check_id, String address_check_true_name, String address_check_area_info_id,
                                         String address_check_address, String tel_phone, String address_check_phone,
                                         String is_default, String mobile_zone1,
                                         String mobile_zone2, String tel_zone,
                                         String mobile_zone1_spare,
                                         String address_check_phone_spare) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_EDIT_CHECK_ADDRESS())
                .addParam("address_check_id", address_check_id)
                .addParam("address_check_true_name", address_check_true_name)
                .addParam("address_check_area_info_id", address_check_area_info_id)
                .addParam("address_check_address", address_check_address)
                .addParam("tel_phone", tel_phone)
                .addParam("address_check_phone", address_check_phone)
                .addParam("is_default", is_default)
                .addParam("mobile_zone1", mobile_zone1)
                .addParam("mobile_zone2", mobile_zone2)
                .addParam("tel_zone", tel_zone)
                .addParam("mobile_zone1_spare", mobile_zone1_spare)
                .addParam("address_check_phone_spare", address_check_phone_spare)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(NewAddressActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(NewAddressActivity.this, R.string.save_address_succeful, Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);
                        finish();
                    }
                }, false);
    }

    private void requestEditReceiveAddress(String address_id, String true_name, String address,
                                           String area_info_id, String tel_phone, String mob_phone,
                                           String is_default, String mobile_zone1,
                                           String mobile_zone2, String tel_zone,
                                           String mobile_zone1_spare,
                                           String mob_phone_spare) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_EDIT_ADDRESS())
                .addParam("address_id", address_id)
                .addParam("true_name", true_name)
                .addParam("address", address)
                .addParam("area_info_id", area_info_id)
                .addParam("tel_phone", tel_phone)
                .addParam("mob_phone", mob_phone)
                .addParam("is_default", is_default)
                .addParam("mobile_zone1", mobile_zone1)
                .addParam("mobile_zone2", mobile_zone2)
                .addParam("tel_zone", tel_zone)
                .addParam("mobile_zone1_spare", mobile_zone1_spare)
                .addParam("mob_phone_spare", mob_phone_spare)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(NewAddressActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(NewAddressActivity.this, R.string.save_address_succeful, Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);
                        finish();
                    }
                }, false);
    }

    private void requestAddressDelete() {
        NetUtil.Companion.getInstance().url(mType==TYPE_EDIT_CHECK_ADDRESS?KyyConstants.INSTANCE.getURL_DELETE_CHECK_ADDRESS():KyyConstants.INSTANCE.getURL_DELETE_ADDRESS())
                .addParam(mType==TYPE_EDIT_CHECK_ADDRESS?"address_check_id":"address_id",mType==TYPE_EDIT_CHECK_ADDRESS?mCheckAddressBean.getAddress_check_id():mReceiveAddressBean.getAddress_id())
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(NewAddressActivity.this, R.string.delete_succeful, Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);
                        finish();
                    }
                },false);
    }
}
