package com.xht.kuaiyouyi.ui.mine.entity;

import java.util.List;

public class OrderListBean {

    /**
     * kyy : eyJQIjoxLCJLIjoia3l5XzAyNyJ9
     * order_main_list : [{"store_id":"1","order_main_state":2,"store_name":"快优易自营","order_main_deal_one_state":"2","order_main_deal_active":"2","order_main_company_id":"91","order_main_second_pay_end":1,"order_main_pay_two":"8.40","order_main_id":"253","order_main_state_name":"待发货","order_main_count":"1","order_main_sn":"1000000000037601","order_main_amount":"12.00","order_main_deal_two_state":1,"order_main_pay_one":"3.60","order_goods_arr":[{"goods_name":"螺丝刀 AS 2123130","goods_num":"1","goods_price":"12.00","goods_id":"100516","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05832546260955428_240.jpg","goods_spec":"型号：AS,螺纹：2123130"}]},{"store_id":"1","order_main_state":2,"store_name":"快优易自营","order_main_deal_active":"1","order_main_company_id":"91","order_main_id":"241","order_main_state_name":"待收货","order_main_count":"1","order_main_sn":"1000000000036401","order_main_amount":"10.00","order_goods_arr":[{"goods_name":"【鼎力】自行走曲臂式","goods_num":"1","goods_price":"10.00","goods_id":"100344","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796245285261627_240.png"}]},{"store_id":"1","order_main_state":2,"store_name":"快优易自营","order_main_deal_active":"1","order_main_company_id":"91","order_main_id":"146","order_main_state_name":"待发货","order_main_count":"1","order_main_sn":"1000000000026901","order_main_amount":"10.00","order_goods_arr":[{"goods_name":"【鼎力】自行走曲臂式","goods_num":"1","goods_price":"10.00","goods_id":"100344","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796245285261627_240.png"}]},{"store_id":"1","order_main_state":2,"store_name":"快优易自营","order_main_deal_active":"1","order_main_company_id":"91","order_main_id":"145","order_main_state_name":"待发货","order_main_count":"1","order_main_sn":"1000000000026801","order_main_amount":"10.00","order_goods_arr":[{"goods_name":"【鼎力】自行走曲臂式","goods_num":"1","goods_price":"10.00","goods_id":"100344","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796245285261627_240.png"}]},{"store_id":"1","order_main_state":2,"store_name":"快优易自营","order_main_deal_active":"1","order_main_company_id":"0","order_main_id":"127","order_main_state_name":"待发货","order_main_count":"1","order_main_sn":"1000000000025001","order_main_amount":"100.00","order_goods_arr":[{"goods_name":"劳力士Rolex 蚝式恒动 115234-CA-72190自动机械钢带男表联保正品 666 22","goods_num":"1","goods_price":"100.00","goods_id":"100378","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627843241680_240.jpg","goods_spec":"颜色：666,尺码：22"}]},{"store_id":"1","order_main_state":2,"store_name":"快优易自营","order_main_deal_active":"1","order_main_company_id":"0","order_main_id":"126","order_main_state_name":"待发货","order_main_count":"1","order_main_sn":"1000000000024901","order_main_amount":"100.00","order_goods_arr":[{"goods_name":"劳力士Rolex 蚝式恒动 115234-CA-72190自动机械钢带男表联保正品 666 22","goods_num":"1","goods_price":"100.00","goods_id":"100378","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627843241680_240.jpg","goods_spec":"颜色：666,尺码：22"}]}]
     */

    private String kyy;
    private List<OrderMainListBean> order_main_list;

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public List<OrderMainListBean> getOrder_main_list() {
        return order_main_list;
    }

    public void setOrder_main_list(List<OrderMainListBean> order_main_list) {
        this.order_main_list = order_main_list;
    }

    public static class OrderMainListBean {
        /**
         * store_id : 1
         * order_main_state : 2
         * store_name : 快优易自营
         * order_main_deal_one_state : 2
         * order_main_deal_active : 2
         * order_main_company_id : 91
         * order_main_second_pay_end : 1
         * order_main_pay_two : 8.40
         * order_main_id : 253
         * order_main_state_name : 待发货
         * order_main_count : 1
         * order_main_sn : 1000000000037601
         * order_main_amount : 12.00
         * order_main_deal_two_state : 1
         * order_main_pay_one : 3.60
         * order_goods_arr : [{"goods_name":"螺丝刀 AS 2123130","goods_num":"1","goods_price":"12.00","goods_id":"100516","goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05832546260955428_240.jpg","goods_spec":"型号：AS,螺纹：2123130"}]
         */

        private String store_id;
        private int order_main_state;
        private String store_name;
        private int order_main_deal_one_state;
        private int order_main_deal_active;
        private int order_main_pay_active;
        private int order_main_company_id;
        private int order_main_second_pay_end;
        private int order_main_first_pay_end;
        private double order_main_pay_two;
        private String order_main_id;
        private String order_main_state_name;
        private String order_main_count;
        private String order_main_sn;
        private double order_main_amount;
        private int order_main_deal_two_state;
        private double order_main_pay_one;
        private boolean is_pay_order;//是否有付款权限
        private boolean is_cancel_order;//是否有取消订单的权限
        private List<OrderGoodsArrBean> order_goods_arr;

        public String getStore_id() {
            return store_id;
        }

        public void setStore_id(String store_id) {
            this.store_id = store_id;
        }

        public int getOrder_main_state() {
            return order_main_state;
        }

        public void setOrder_main_state(int order_main_state) {
            this.order_main_state = order_main_state;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }

        public int getOrder_main_deal_one_state() {
            return order_main_deal_one_state;
        }

        public void setOrder_main_deal_one_state(int order_main_deal_one_state) {
            this.order_main_deal_one_state = order_main_deal_one_state;
        }

        public int getOrder_main_deal_active() {
            return order_main_deal_active;
        }

        public void setOrder_main_deal_active(int order_main_deal_active) {
            this.order_main_deal_active = order_main_deal_active;
        }

        public int getOrder_main_pay_active() {
            return order_main_pay_active;
        }

        public void setOrder_main_pay_active(int order_main_pay_active) {
            this.order_main_pay_active = order_main_pay_active;
        }

        public int getOrder_main_company_id() {
            return order_main_company_id;
        }

        public void setOrder_main_company_id(int order_main_company_id) {
            this.order_main_company_id = order_main_company_id;
        }

        public int getOrder_main_first_pay_end() {
            return order_main_first_pay_end;
        }

        public void setOrder_main_first_pay_end(int order_main_first_pay_end) {
            this.order_main_first_pay_end = order_main_first_pay_end;
        }

        public int getOrder_main_second_pay_end() {
            return order_main_second_pay_end;
        }

        public void setOrder_main_second_pay_end(int order_main_second_pay_end) {
            this.order_main_second_pay_end = order_main_second_pay_end;
        }

        public double getOrder_main_pay_two() {
            return order_main_pay_two;
        }

        public void setOrder_main_pay_two(double order_main_pay_two) {
            this.order_main_pay_two = order_main_pay_two;
        }

        public String getOrder_main_id() {
            return order_main_id;
        }

        public void setOrder_main_id(String order_main_id) {
            this.order_main_id = order_main_id;
        }

        public String getOrder_main_state_name() {
            return order_main_state_name;
        }

        public void setOrder_main_state_name(String order_main_state_name) {
            this.order_main_state_name = order_main_state_name;
        }

        public String getOrder_main_count() {
            return order_main_count;
        }

        public void setOrder_main_count(String order_main_count) {
            this.order_main_count = order_main_count;
        }

        public String getOrder_main_sn() {
            return order_main_sn;
        }

        public void setOrder_main_sn(String order_main_sn) {
            this.order_main_sn = order_main_sn;
        }

        public double getOrder_main_amount() {
            return order_main_amount;
        }

        public void setOrder_main_amount(double order_main_amount) {
            this.order_main_amount = order_main_amount;
        }

        public int getOrder_main_deal_two_state() {
            return order_main_deal_two_state;
        }

        public void setOrder_main_deal_two_state(int order_main_deal_two_state) {
            this.order_main_deal_two_state = order_main_deal_two_state;
        }

        public double getOrder_main_pay_one() {
            return order_main_pay_one;
        }

        public void setOrder_main_pay_one(double order_main_pay_one) {
            this.order_main_pay_one = order_main_pay_one;
        }

        public List<OrderGoodsArrBean> getOrder_goods_arr() {
            return order_goods_arr;
        }

        public void setOrder_goods_arr(List<OrderGoodsArrBean> order_goods_arr) {
            this.order_goods_arr = order_goods_arr;
        }

        public static class OrderGoodsArrBean {
            /**
             * goods_name : 螺丝刀 AS 2123130
             * goods_num : 1
             * goods_price : 12.00
             * goods_id : 100516
             * goods_image : http://192.168.0.2/data/upload/shop/store/goods/1/1_05832546260955428_240.jpg
             * goods_spec : 型号：AS,螺纹：2123130
             */

            private String goods_name;
            private int goods_num;
            private double goods_price;
            private String goods_id;
            private String goods_image;
            private String goods_spec;
            private int is_second_hand;

            public int getIs_second_hand() {
                return is_second_hand;
            }

            public void setIs_second_hand(int is_second_hand) {
                this.is_second_hand = is_second_hand;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public int getGoods_num() {
                return goods_num;
            }

            public void setGoods_num(int goods_num) {
                this.goods_num = goods_num;
            }

            public double getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(double goods_price) {
                this.goods_price = goods_price;
            }

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getGoods_image() {
                return goods_image;
            }

            public void setGoods_image(String goods_image) {
                this.goods_image = goods_image;
            }

            public String getGoods_spec() {
                return goods_spec;
            }

            public void setGoods_spec(String goods_spec) {
                this.goods_spec = goods_spec;
            }
        }

        public boolean isIs_pay_order() {
            return is_pay_order;
        }

        public void setIs_pay_order(boolean is_pay_order) {
            this.is_pay_order = is_pay_order;
        }

        public boolean isIs_cancel_order() {
            return is_cancel_order;
        }

        public void setIs_cancel_order(boolean is_cancel_order) {
            this.is_cancel_order = is_cancel_order;
        }
    }
}
