package com.xht.kuaiyouyi.ui.message.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.cart.order.OrderDetilsActivity;
import com.xht.kuaiyouyi.ui.enterprise.Constant;
import com.xht.kuaiyouyi.ui.enterprise.activity.ApprovalDetailEnterpriseActivity;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

/**
 * 华为推送点击通知都是打开这个activity然后再跳转的
 *
 * 自定义动作的内容格式（后端使用到）：
 * intent:#Intent;launchFlags=0x10000000;component=com.xht.kuaiyouyi.debug/com.xht.kuaiyouyi.ui.message.activity.HuaweiPushActivity;S.ext1=helloExt1;S.ext2=helloExt2;end
 */
public class HuaweiPushActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huaweipush);
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent handleIntent) {
        Bundle bundle = handleIntent.getExtras();
        String type = "";
        String order_main_id = "";
        String company_id = "";
        String approve_order_id = "";
        String goods_id = "";
        String u_id = "";
        Intent intent = new Intent();

        if (Login.Companion.getInstance().isLogin()) {
            if (bundle != null) {
                Utils.log("huaweipush", bundle.keySet().toString());
                type = bundle.getString("type");
                u_id = bundle.getString("u_id");
            }
            if (TextUtils.isEmpty(type) || TextUtils.isEmpty(u_id) || !u_id.equals(Login.Companion.getInstance().getUid())) {
                intent.setClass(this, MessageActivity.class);
            } else {
                switch (type) {
                    case "1":
                        order_main_id = bundle.getString("order_main_id");
                        if (!TextUtils.isEmpty(order_main_id)) {
                            intent.setClass(this, OrderDetilsActivity.class);
                            intent.putExtra("order_main_id", order_main_id);
                            break;
                        }
                    case "2":
                        company_id = bundle.getString("company_id");
                        approve_order_id = bundle.getString("approve_order_id");
                        if (!TextUtils.isEmpty(company_id) && !TextUtils.isEmpty(approve_order_id)) {
                            intent.setClass(this, ApprovalDetailEnterpriseActivity.class);
                            intent.putExtra(ApprovalDetailEnterpriseActivity.APPROVE_ORDER_ID, approve_order_id);
                            intent.putExtra(Constant.COMPANY_ID, company_id);
                            break;
                        }
                    case "3":
                        intent.setClass(this, MessageActivity.class);
                        break;
                    case "4":
                        goods_id = intent.getStringExtra("goods_id");
                        if (TextUtils.isEmpty(goods_id)) {
                            intent.putExtra("goods_id", goods_id);
                            intent.setClass(this, GoodsActivity.class);
                            break;
                        }
                    default:
                        intent.setClass(this, MessageActivity.class);
                        break;
                }
            }
        } else {
            intent.setClass(this, MainActivity.class);
            intent.putExtra(MainActivity.KEY_HUAWEI_PUSH_TO_LOGIN, true);
        }
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
        finish();
    }
}
