package com.xht.kuaiyouyi.ui.search.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.search.entity.RightArrBean;
import com.xht.kuaiyouyi.ui.search.entity.TypeDescHolder;
import com.xht.kuaiyouyi.ui.search.entity.TypeHeaderHolder;

import java.util.List;

public class TepyAdapter extends SectionedRecyclerViewAdapter<TypeHeaderHolder,TypeDescHolder,RecyclerView.ViewHolder> implements View.OnClickListener{
    private Context context;
    private List<RightArrBean> list;
    private LayoutInflater mInflater;
    private SparseBooleanArray mBooleanMap;
    private OnItemClickListener mItemClickListener;
    private JSONObject jsonObject;

    public TepyAdapter(Context context, List<RightArrBean> list) {
        this.context = context;
        this.list = list;
        mInflater = LayoutInflater.from(context);
        mBooleanMap = new SparseBooleanArray();
    }

    public void setData(List<RightArrBean> list){
        this.list=list;
        notifyDataSetChanged();
    }

    @Override
    protected int getSectionCount() {
        if(list!=null && list.size()!=0){
            return list.size();
        }else {
            return 0;
        }
    }

    @Override
    protected int getItemCountForSection(int section) {
        if(list.get(section).getClass3()!=null && list.get(section).getClass3().size()!=0){
            return list.get(section).getClass3().size();
        }else {
            return 0;
        }
    }

    @Override
    protected boolean hasFooterInSection(int section) {
        return false;
    }

    @Override
    protected TypeHeaderHolder onCreateSectionHeaderViewHolder(ViewGroup parent, int viewType) {
        return new TypeHeaderHolder(mInflater.inflate(R.layout.item_type_textview, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder onCreateSectionFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    protected TypeDescHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_type_imageview, parent,false);
        TypeDescHolder descHolder=new TypeDescHolder(view);
        descHolder.itemView.setOnClickListener(this);
        return descHolder;
    }

    @Override
    protected void onBindSectionHeaderViewHolder(TypeHeaderHolder holder, int section) {
        if(section<list.size()){
            holder.titleView.setText(list.get(section).getGc_name());
        }
    }

    @Override
    protected void onBindSectionFooterViewHolder(RecyclerView.ViewHolder holder, int section) {
    }

    @Override
    protected void onBindItemViewHolder(TypeDescHolder holder, int section, int position) {
            if(list.size()!=0 && list.get(section).getClass3().size()!=0){
                if(position<list.get(section).getClass3().size()) {
                    holder.nameView.setText(list.get(section).getClass3().get(position).getGc_name());
                    Glide.with(context).load(list.get(section).getClass3().get(position).getGc_image())
                            .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                            .into(holder.imageView);
                    jsonObject = new JSONObject();
                    jsonObject.put("section",section);
                    jsonObject.put("position",position);
                    holder.itemView.setTag(jsonObject);
                }
            }
    }


    @Override
    public void onClick(View v) {
        if (mItemClickListener!=null){
            mItemClickListener.onItemClick((JSONObject)v.getTag());
            /*JSONObject jsonObject = (JSONObject)v.getTag();
            int section = (Integer) jsonObject.get("section");
            int position = (Integer)jsonObject.get("position");
            boolean b = list.get(section).getTagInfo().get(position).isSelect() == true?false:true;
            list.get(section).getTagInfo().get(position).setSelect(b);
            notifyDataSetChanged();*/
        }
    }

    public interface OnItemClickListener{
        void onItemClick(JSONObject position);
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }
}
