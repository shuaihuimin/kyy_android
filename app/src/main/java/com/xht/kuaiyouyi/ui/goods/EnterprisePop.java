package com.xht.kuaiyouyi.ui.goods;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.utils.CommonPopupWindow;

public class EnterprisePop {
    private static CommonPopupWindow popupWindow;

    public static void showpop(final Activity context){
        View view = LayoutInflater.from(context).inflate(R.layout.addenterprise_pop, null);
        popupWindow = new CommonPopupWindow.Builder(context)
                .setView(view)
                .setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                .setBackGroundLevel(0.5f)//取值范围0.0f-1.0f 值越小越暗
                .create();
        TextView tv_upgrade=(TextView) view.findViewById(R.id.tv_upgrade);
        tv_upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(GoodsActivity.TO_HOME_ENTERPRISE,true);
                Intent intent=new Intent(context,MainActivity.class);
                intent.putExtras(bundle);
                context.startActivity(intent);
                popupWindow.dismiss();
                popupWindow=null;
            }
        });
        ImageView iv_close=(ImageView) view.findViewById(R.id.iv_close);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                popupWindow=null;
            }
        });
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
    }

}
