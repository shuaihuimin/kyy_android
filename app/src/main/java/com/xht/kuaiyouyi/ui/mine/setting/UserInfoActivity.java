package com.xht.kuaiyouyi.ui.mine.setting;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.CropOptions;
import com.jph.takephoto.model.TResult;
import com.jph.takephoto.model.TakePhotoOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.mine.entity.UploadFileBean;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.SignUtil;
import com.xht.kuaiyouyi.utils.SystemUtil;
import com.xht.kuaiyouyi.widget.ButtomDialog;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserInfoActivity extends TakePhotoActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_right)
    ImageView iv_right;
    @BindView(R.id.ll_my_icon)
    LinearLayout ll_my_icon;
    @BindView(R.id.iv_my_icon)
    ImageView iv_my_icon;
    @BindView(R.id.ll_member_name)
    LinearLayout ll_member_name;
    @BindView(R.id.tv_member_name)
    TextView tv_member_name;

    private ButtomDialog mButtomDialog;
    private Uri mImageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(getLayout());
        //绑定初始化ButterKnife
        ButterKnife.bind(this);
        initView();
    }


    @Override
    public void takeCancel() {
        super.takeCancel();
    }

    @Override
    public void takeFail(TResult result, String msg) {
        super.takeFail(result, msg);
    }

    @Override
    public void takeSuccess(TResult result) {
        super.takeSuccess(result);
        String uploadIconPath = result.getImage().getCompressPath();
        requestUploadIcon(uploadIconPath);

    }

    private void requestUploadIcon(final String uploadIconPath) {
        DialogUtils.createTipAllLoadDialog(this);
        String t = SystemUtil.INSTANCE.getTime(SystemUtil.INSTANCE.
                getTimeStr("" + System.currentTimeMillis()));
        Map<String,Object> map = new HashMap<>();
        map.put("device_uuid", NetUtil.Companion.getUuid());
        map.put("device_name", NetUtil.Companion.getDevice_name());
        map.put("type", "1");
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_UPLOADIMG())
                .addParam("device_uuid", NetUtil.Companion.getUuid())
                .addParam("device_name", NetUtil.Companion.getDevice_name())
                .addParam("token_id", Login.Companion.getInstance().getToken_id())
                .addParam("sign_time", t)
                .addParam("sign", SignUtil.sign(map, t))
                .addParam("type", "1")
                .addParam(new File(uploadIconPath).getName(),new File(uploadIconPath))
        .withPOSTFile(new NetCallBack<UploadFileBean>() {
            @NotNull
            @Override
            public Class<UploadFileBean> getRealType() {
                return UploadFileBean.class;
            }

            @Override
            public void onSuccess(@NonNull UploadFileBean uploadFileBean) {
                if(!isFinishing()){
                    DialogUtils.moven();
                    Glide.with(UserInfoActivity.this).load(uploadFileBean.getFile_name())
                            .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                            .into(iv_my_icon);
                    mButtomDialog.dismiss();
                    setResult(RESULT_OK);
                }
                Login.Companion.getInstance().setMemberAvatar(uploadFileBean.getFile_name());
                EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_AVATAR));
            }

            @Override
            public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                if(isFinishing()){
                    return;
                }
                DialogUtils.moven();
                Toast.makeText(UserInfoActivity.this,err,Toast.LENGTH_SHORT).show();
            }
        },false);
    }

    protected int getLayout() {
        return R.layout.activity_user_info;
    }

    protected void initView() {
        tv_title.setText(R.string.user_info);
        tv_member_name.setText(Login.Companion.getInstance().getUsername());
        Glide.with(this).load(Login.Companion.getInstance().getMemberAvatar())
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                .into(iv_my_icon);
        iv_right.setVisibility(View.GONE);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_member_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.createTipAllTextDialog(UserInfoActivity.this,getResources().getString(R.string.member_name_tip));
            }
        });
        ll_my_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showButtonDialog();
            }
        });
    }

    private void showButtonDialog() {
        if(mButtomDialog == null){
            View view = LayoutInflater.from(this).inflate(R.layout.dialog_buttom_three_btn,null);
            mButtomDialog = new ButtomDialog(this,view,false,true);
            final TextView tv_three = view.findViewById(R.id.tv_three);
            TextView tv_one = view.findViewById(R.id.tv_one);
            TextView tv_two = view.findViewById(R.id.tv_two);

            tv_three.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                }
            });
            tv_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),  System.currentTimeMillis() + ".jpg");
                    mImageUri = Uri.fromFile(file);
                    TakePhotoOptions takePhotoOptions = new TakePhotoOptions.Builder().create();
                    takePhotoOptions.setCorrectImage(true);
                    getTakePhoto().setTakePhotoOptions(takePhotoOptions);
                    getTakePhoto().onPickFromCaptureWithCrop(mImageUri, getCropOptions());
                }
            });
            tv_two.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtomDialog.dismiss();
                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),  System.currentTimeMillis() + ".jpg");
                    mImageUri = Uri.fromFile(file);
                    TakePhotoOptions takePhotoOptions = new TakePhotoOptions.Builder().create();
                    takePhotoOptions.setCorrectImage(true);
                    getTakePhoto().setTakePhotoOptions(takePhotoOptions);
                    getTakePhoto().onPickFromGalleryWithCrop(mImageUri, getCropOptions());
                }
            });
        }
        mButtomDialog.show();

    }

    private CropOptions getCropOptions() {
        CompressConfig config = new CompressConfig.Builder().setMaxSize(102400)
                .setMaxPixel(800)
                .enableReserveRaw(true)
                .create();
        getTakePhoto().onEnableCompress(config, false);
        CropOptions.Builder builder = new CropOptions.Builder();
        builder.setAspectX(800).setAspectY(800);
        builder.setWithOwnCrop(true);
        return builder.create();
    }


    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config=new Configuration();
        config.setToDefaults();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocales(res.getConfiguration().getLocales());
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(res.getConfiguration().locale);
        }else {
            config.locale = res.getConfiguration().locale;
        }
        res.updateConfiguration(config,res.getDisplayMetrics() );
        return res;
    }
}
