package com.xht.kuaiyouyi.ui.enterprise.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.enterprise.bean.ApprovalDetailBean;
import com.xht.kuaiyouyi.utils.Utils;

/**
 * 商品清单
 */
public class GoodsListAdapter extends RecyclerView.Adapter<GoodsListAdapter.ViewHolder> {
    private Context mContext;
    private ApprovalDetailBean.GoodsInfoBean mGoodsInfoBean;

    public GoodsListAdapter(Context mContext, ApprovalDetailBean.GoodsInfoBean goodsInfoBean) {
        this.mContext = mContext;
        this.mGoodsInfoBean = goodsInfoBean;
    }

    @Override
    public GoodsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_enterprise_goods_list, parent, false);
        return new GoodsListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GoodsListAdapter.ViewHolder holder, int position) {
        Glide.with(mContext).load(mGoodsInfoBean.getOrder_list().get(position)
                .getImage_60_url()).into(holder.iv_goods_img);
        if(mGoodsInfoBean.getOrder_list().get(position).getIs_second_hand()==1){
            holder.tv_goods_name.setText(Utils.secondLabel(mGoodsInfoBean.getOrder_list().get(position).getGoods_name()));
        }else {
            holder.tv_goods_name.setText(mGoodsInfoBean.getOrder_list().get(position).getGoods_name());
        }
        holder.tv_goods_model.setText(mGoodsInfoBean.getOrder_list().get(position).getGoods_spec());
        holder.tv_goods_price.setText("￥"+ Utils.getDisplayMoney(mGoodsInfoBean.getOrder_list().get(position).getGoods_price()));
        holder.tv_goods_num.setText("x"+mGoodsInfoBean.getOrder_list().get(position).getGoods_num());
        holder.tv_small_total.setText("￥"+Utils.getDisplayMoney(mGoodsInfoBean.getOrder_list().get(position).getGoods_pay_price()));
        holder.tv_goods_freight.setText("￥"+Utils.getDisplayMoney(mGoodsInfoBean.getOrder_list().get(position).getFreight_fee()));
        if(mGoodsInfoBean.getOrder_list().get(position).getCustoms_charges_fee()>0){
            holder.tv_test_fee.setVisibility(View.VISIBLE);
            holder.tv_test_fee_title.setVisibility(View.VISIBLE);
            holder.tv_test_fee.setText("￥"+Utils.getDisplayMoney(mGoodsInfoBean.getOrder_list().get(position).getCustoms_charges_fee()));
        }else {
            holder.tv_test_fee.setVisibility(View.GONE);
            holder.tv_test_fee_title.setVisibility(View.GONE);
        }
        if(position==mGoodsInfoBean.getOrder_list().size()-1){
            holder.v_line.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if(mGoodsInfoBean.getOrder_list()==null){
            return 0;
        }
        return mGoodsInfoBean.getOrder_list().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_goods_name, tv_goods_price, tv_goods_num, tv_goods_model,
                tv_small_total, tv_goods_freight, tv_test_fee,tv_test_fee_title;
        private ImageView iv_goods_img;
        private View v_line;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_goods_img = itemView.findViewById(R.id.iv_goods_img);
            tv_goods_name = itemView.findViewById(R.id.tv_goods_name);
            tv_goods_price = itemView.findViewById(R.id.tv_goods_price);
            tv_goods_num = itemView.findViewById(R.id.tv_goods_num);
            tv_goods_model = itemView.findViewById(R.id.tv_goods_model);
            tv_small_total = itemView.findViewById(R.id.tv_small_total);
            tv_goods_freight = itemView.findViewById(R.id.tv_goods_freight);
            tv_test_fee = itemView.findViewById(R.id.tv_test_fee);
            tv_test_fee_title = itemView.findViewById(R.id.tv_test_fee_title);
            v_line = itemView.findViewById(R.id.v_line);
        }


    }


}
