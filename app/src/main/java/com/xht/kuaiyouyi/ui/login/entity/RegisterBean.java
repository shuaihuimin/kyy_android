package com.xht.kuaiyouyi.ui.login.entity;

public class RegisterBean {
    /**
     * state_data : {"token":"322ceeeed801e148bb04bc4955669327","token_id":"26"}
     */

    private StateDataBean state_data;

    public StateDataBean getState_data() {
        return state_data;
    }

    public void setState_data(StateDataBean state_data) {
        this.state_data = state_data;
    }

    public static class StateDataBean {
        /**
         * token : 322ceeeed801e148bb04bc4955669327
         * token_id : 26
         */

        private String token;
        private String token_id;
        private String user_id;
        private String user_name;
        private String avatar;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getToken_id() {
            return token_id;
        }

        public void setToken_id(String token_id) {
            this.token_id = token_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }

}
