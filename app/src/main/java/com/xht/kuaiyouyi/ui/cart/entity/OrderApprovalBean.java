package com.xht.kuaiyouyi.ui.cart.entity;

import java.util.List;

public class OrderApprovalBean {

    /**
     * goods_list : {"beginTime":{"day":4,"hour":23,"min":59,"sec":37},"order_main_deal_active":"信用支付","order_main_pay_active_name":"支票支付","order_main_pay_currency_name":"葡币","order_main_pay_currency_symbol":"MOP$","order_main_pay_currency_rate":"1.1731","order_main_goods_total":"300.00","order_main_other_total":"0.00","order_main_amount":"300.00","order_main_pay_one":"300.00","order_main_pay_two":"0.00","supplement_amount":"0.00","order_main_state":"2000","order_main_state_name":"待付款","order_main_sn":"1000000000049701","order_list":[{"goods_id":"100362","goods_name":"【鼎力】自行走曲臂式GTBZ-AE","goods_spec":null,"image_60_url":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796293756348179_60.png","goods_price":"100.00","goods_num":"2","goods_pay_price":"200.00","freight_fee":0,"profit_rate":0,"customs_charges_fee":0},{"goods_id":"100477","goods_name":"惠普(HP) LaserJet Pro M1139 黑白激光多功能一体机 (打印 复印 扫描)","goods_spec":null,"image_60_url":"http://192.168.0.2/data/upload/shop/store/goods/22/22_05803917426286848_60.jpg","goods_price":"100.00","goods_num":"1","goods_pay_price":"100.00","freight_fee":0,"profit_rate":0,"customs_charges_fee":0}]}
     * approve_creator : 兰博基尼
     * company_id : 91
     * order_main_id : 360
     * approve_set : ["兰博基尼","2人会签","2人或签"]
     * copy_to_member : ["兰博基尼"]
     */

    private GoodsListBean goods_list;
    private String approve_creator;
    private int company_id;
    private int order_main_id;
    private List<String> approve_set;
    private List<String> copy_to_member;
    private List<GroupsBean> groups;

    public GoodsListBean getGoods_list() {
        return goods_list;
    }

    public void setGoods_list(GoodsListBean goods_list) {
        this.goods_list = goods_list;
    }

    public String getApprove_creator() {
        return approve_creator;
    }

    public void setApprove_creator(String approve_creator) {
        this.approve_creator = approve_creator;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public int getOrder_main_id() {
        return order_main_id;
    }

    public void setOrder_main_id(int order_main_id) {
        this.order_main_id = order_main_id;
    }

    public List<String> getApprove_set() {
        return approve_set;
    }

    public void setApprove_set(List<String> approve_set) {
        this.approve_set = approve_set;
    }

    public List<String> getCopy_to_member() {
        return copy_to_member;
    }

    public void setCopy_to_member(List<String> copy_to_member) {
        this.copy_to_member = copy_to_member;
    }

    public static class GoodsListBean {
        /**
         * beginTime : {"day":4,"hour":23,"min":59,"sec":37}
         * order_main_deal_active : 信用支付
         * order_main_pay_active_name : 支票支付
         * order_main_pay_currency_name : 葡币
         * order_main_pay_currency_symbol : MOP$
         * order_main_pay_currency_rate : 1.1731
         * order_main_goods_total : 300.00
         * order_main_other_total : 0.00
         * order_main_amount : 300.00
         * order_main_pay_one : 300.00
         * order_main_pay_two : 0.00
         * supplement_amount : 0.00
         * order_main_state : 2000
         * order_main_state_name : 待付款
         * order_main_sn : 1000000000049701
         * order_list : [{"goods_id":"100362","goods_name":"【鼎力】自行走曲臂式GTBZ-AE","goods_spec":null,"image_60_url":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796293756348179_60.png","goods_price":"100.00","goods_num":"2","goods_pay_price":"200.00","freight_fee":0,"profit_rate":0,"customs_charges_fee":0},{"goods_id":"100477","goods_name":"惠普(HP) LaserJet Pro M1139 黑白激光多功能一体机 (打印 复印 扫描)","goods_spec":null,"image_60_url":"http://192.168.0.2/data/upload/shop/store/goods/22/22_05803917426286848_60.jpg","goods_price":"100.00","goods_num":"1","goods_pay_price":"100.00","freight_fee":0,"profit_rate":0,"customs_charges_fee":0}]
         */

        private BeginTimeBean beginTime;
        private String company_count_money;
        private String company_remain_money;
        private String order_main_deal_active;
        private String order_main_pay_active;
        private String order_main_pay_currency_name;
        private String order_main_pay_currency_symbol;
        private String order_main_pay_currency_rate;
        private String order_main_goods_total;
        private String order_main_other_total;
        private String order_main_amount;
        private String order_main_pay_one;
        private String order_main_pay_two;
        private String supplement_amount;
        private String order_main_state;
        private String order_main_state_name;
        private String order_main_sn;
        private String order_main_pay_first;
        private String order_main_pay_second;
        private List<OrderListBean> order_list;

        public BeginTimeBean getBeginTime() {
            return beginTime;
        }

        public void setBeginTime(BeginTimeBean beginTime) {
            this.beginTime = beginTime;
        }

        public String getCompany_count_money() {
            return company_count_money;
        }

        public void setCompany_count_money(String company_count_money) {
            this.company_count_money = company_count_money;
        }

        public String getCompany_remain_money() {
            return company_remain_money;
        }

        public void setCompany_remain_money(String company_remain_money) {
            this.company_remain_money = company_remain_money;
        }

        public String getOrder_main_deal_active() {
            return order_main_deal_active;
        }

        public void setOrder_main_deal_active(String order_main_deal_active) {
            this.order_main_deal_active = order_main_deal_active;
        }

        public String getOrder_main_pay_active() {
            return order_main_pay_active;
        }

        public void setOrder_main_pay_active(String order_main_pay_active) {
            this.order_main_pay_active = order_main_pay_active;
        }

        public String getOrder_main_pay_currency_name() {
            return order_main_pay_currency_name;
        }

        public void setOrder_main_pay_currency_name(String order_main_pay_currency_name) {
            this.order_main_pay_currency_name = order_main_pay_currency_name;
        }

        public String getOrder_main_pay_currency_symbol() {
            return order_main_pay_currency_symbol;
        }

        public void setOrder_main_pay_currency_symbol(String order_main_pay_currency_symbol) {
            this.order_main_pay_currency_symbol = order_main_pay_currency_symbol;
        }

        public String getOrder_main_pay_currency_rate() {
            return order_main_pay_currency_rate;
        }

        public void setOrder_main_pay_currency_rate(String order_main_pay_currency_rate) {
            this.order_main_pay_currency_rate = order_main_pay_currency_rate;
        }

        public String getOrder_main_goods_total() {
            return order_main_goods_total;
        }

        public void setOrder_main_goods_total(String order_main_goods_total) {
            this.order_main_goods_total = order_main_goods_total;
        }

        public String getOrder_main_other_total() {
            return order_main_other_total;
        }

        public void setOrder_main_other_total(String order_main_other_total) {
            this.order_main_other_total = order_main_other_total;
        }

        public String getOrder_main_amount() {
            return order_main_amount;
        }

        public void setOrder_main_amount(String order_main_amount) {
            this.order_main_amount = order_main_amount;
        }

        public String getOrder_main_pay_one() {
            return order_main_pay_one;
        }

        public void setOrder_main_pay_one(String order_main_pay_one) {
            this.order_main_pay_one = order_main_pay_one;
        }

        public String getOrder_main_pay_two() {
            return order_main_pay_two;
        }

        public void setOrder_main_pay_two(String order_main_pay_two) {
            this.order_main_pay_two = order_main_pay_two;
        }

        public String getOrder_main_pay_first() {
            return order_main_pay_first;
        }

        public void setOrder_main_pay_first(String order_main_pay_first) {
            this.order_main_pay_first = order_main_pay_first;
        }

        public String getOrder_main_pay_second() {
            return order_main_pay_second;
        }

        public void setOrder_main_pay_second(String order_main_pay_second) {
            this.order_main_pay_second = order_main_pay_second;
        }

        public String getSupplement_amount() {
            return supplement_amount;
        }

        public void setSupplement_amount(String supplement_amount) {
            this.supplement_amount = supplement_amount;
        }

        public String getOrder_main_state() {
            return order_main_state;
        }

        public void setOrder_main_state(String order_main_state) {
            this.order_main_state = order_main_state;
        }

        public String getOrder_main_state_name() {
            return order_main_state_name;
        }

        public void setOrder_main_state_name(String order_main_state_name) {
            this.order_main_state_name = order_main_state_name;
        }

        public String getOrder_main_sn() {
            return order_main_sn;
        }

        public void setOrder_main_sn(String order_main_sn) {
            this.order_main_sn = order_main_sn;
        }

        public List<OrderListBean> getOrder_list() {
            return order_list;
        }

        public void setOrder_list(List<OrderListBean> order_list) {
            this.order_list = order_list;
        }

        public static class BeginTimeBean {
            /**
             * day : 4
             * hour : 23
             * min : 59
             * sec : 37
             */

            private int day;
            private int hour;
            private int min;
            private int sec;

            public int getDay() {
                return day;
            }

            public void setDay(int day) {
                this.day = day;
            }

            public int getHour() {
                return hour;
            }

            public void setHour(int hour) {
                this.hour = hour;
            }

            public int getMin() {
                return min;
            }

            public void setMin(int min) {
                this.min = min;
            }

            public int getSec() {
                return sec;
            }

            public void setSec(int sec) {
                this.sec = sec;
            }
        }

        public static class OrderListBean {
            /**
             * goods_id : 100362
             * goods_name : 【鼎力】自行走曲臂式GTBZ-AE
             * goods_spec : null
             * image_60_url : http://192.168.0.2/data/upload/shop/store/goods/1/1_05796293756348179_60.png
             * goods_price : 100.00
             * goods_num : 2
             * goods_pay_price : 200.00
             * freight_fee : 0
             * profit_rate : 0
             * customs_charges_fee : 0
             */

            private String goods_id;
            private String goods_name;
            private String goods_spec;
            private String image_60_url;
            private String goods_price;
            private String goods_num;
            private String goods_pay_price;
            private String freight_fee;
            private int profit_rate;
            private String customs_charges_fee;
            private int is_second_hand;

            public int getIs_second_hand() {
                return is_second_hand;
            }

            public void setIs_second_hand(int is_second_hand) {
                this.is_second_hand = is_second_hand;
            }


            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getGoods_spec() {
                return goods_spec;
            }

            public void setGoods_spec(String goods_spec) {
                this.goods_spec = goods_spec;
            }

            public String getImage_60_url() {
                return image_60_url;
            }

            public void setImage_60_url(String image_60_url) {
                this.image_60_url = image_60_url;
            }

            public String getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(String goods_price) {
                this.goods_price = goods_price;
            }

            public String getGoods_num() {
                return goods_num;
            }

            public void setGoods_num(String goods_num) {
                this.goods_num = goods_num;
            }

            public String getGoods_pay_price() {
                return goods_pay_price;
            }

            public void setGoods_pay_price(String goods_pay_price) {
                this.goods_pay_price = goods_pay_price;
            }

            public String getFreight_fee() {
                return freight_fee;
            }

            public void setFreight_fee(String freight_fee) {
                this.freight_fee = freight_fee;
            }

            public int getProfit_rate() {
                return profit_rate;
            }

            public void setProfit_rate(int profit_rate) {
                this.profit_rate = profit_rate;
            }

            public String getCustoms_charges_fee() {
                return customs_charges_fee;
            }

            public void setCustoms_charges_fee(String customs_charges_fee) {
                this.customs_charges_fee = customs_charges_fee;
            }
        }
    }

    public List<GroupsBean> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupsBean> groups) {
        this.groups = groups;
    }

    public static class GroupsBean{

        /**
         * id : 1
         * company_id : 91
         * group_name : 默认分组
         * create_time : 0
         * update_time : 1536138318
         * is_default : 1
         * is_delete : 0
         * group_approves : [{"id":"136","approve_name":"默认审批流程","company_id":"91","is_default":"1","approve_group_id":"1"},{"id":"76","approve_name":"是飞洒发","company_id":"91","is_default":"0","approve_group_id":"1"},{"id":"122","approve_name":"沙发是否大杀发大水打发","company_id":"91","is_default":"0","approve_group_id":"1"},{"id":"123","approve_name":"司法法师打发的发达","company_id":"91","is_default":"0","approve_group_id":"1"},{"id":"124","approve_name":"舒服撒大","company_id":"91","is_default":"0","approve_group_id":"1"}]
         */

        private String id;
        private String company_id;
        private String group_name;
        private String create_time;
        private String update_time;
        private String is_default;
        private String is_delete;
        private boolean is_group_select;
        private List<GroupApprovesBean> group_approves;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCompany_id() {
            return company_id;
        }

        public void setCompany_id(String company_id) {
            this.company_id = company_id;
        }

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getUpdate_time() {
            return update_time;
        }

        public void setUpdate_time(String update_time) {
            this.update_time = update_time;
        }

        public String getIs_default() {
            return is_default;
        }

        public void setIs_default(String is_default) {
            this.is_default = is_default;
        }

        public String getIs_delete() {
            return is_delete;
        }

        public void setIs_delete(String is_delete) {
            this.is_delete = is_delete;
        }

        public boolean isIs_group_select() {
            return is_group_select;
        }

        public void setIs_group_select(boolean is_group_select) {
            this.is_group_select = is_group_select;
        }

        public List<GroupApprovesBean> getGroup_approves() {
            return group_approves;
        }

        public void setGroup_approves(List<GroupApprovesBean> group_approves) {
            this.group_approves = group_approves;
        }

        public static class GroupApprovesBean {
            /**
             * id : 136
             * approve_name : 默认审批流程
             * company_id : 91
             * is_default : 1
             * approve_group_id : 1
             */

            private String id;
            private String approve_name;
            private String company_id;
            private String is_default;
            private String approve_group_id;
            private boolean is_child_select;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getApprove_name() {
                return approve_name;
            }

            public void setApprove_name(String approve_name) {
                this.approve_name = approve_name;
            }

            public String getCompany_id() {
                return company_id;
            }

            public void setCompany_id(String company_id) {
                this.company_id = company_id;
            }

            public String getIs_default() {
                return is_default;
            }

            public void setIs_default(String is_default) {
                this.is_default = is_default;
            }

            public String getApprove_group_id() {
                return approve_group_id;
            }

            public void setApprove_group_id(String approve_group_id) {
                this.approve_group_id = approve_group_id;
            }

            public boolean isIs_child_select() {
                return is_child_select;
            }

            public void setIs_child_select(boolean is_child_select) {
                this.is_child_select = is_child_select;
            }
        }
    }
}
