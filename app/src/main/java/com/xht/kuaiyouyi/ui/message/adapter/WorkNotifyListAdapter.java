package com.xht.kuaiyouyi.ui.message.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.message.bean.WorkNotifyListBean;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

public class WorkNotifyListAdapter extends BaseAdapter {
    private Context mContext;
    private WorkNotifyListBean mWorkNotifyListBean;

    public WorkNotifyListAdapter(Context mContext, WorkNotifyListBean workNotifyListBean) {
        this.mContext = mContext;
        this.mWorkNotifyListBean = workNotifyListBean;
    }

    @Override
    public int getCount() {
        if(mWorkNotifyListBean.getMessage_list()!=null){
            return mWorkNotifyListBean.getMessage_list().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mWorkNotifyListBean.getMessage_list().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.item_message_work_notify, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_message = convertView.findViewById(R.id.tv_message);
            viewHolder.tv_time = convertView.findViewById(R.id.tv_time);
            viewHolder.tv_point = convertView.findViewById(R.id.tv_point);
            viewHolder.tv_status = convertView.findViewById(R.id.tv_status);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_time.setText(Utils.getDisplayDataAndTime(mWorkNotifyListBean.getMessage_list().get(position).getCreate_time()));

        if(mWorkNotifyListBean.getMessage_list().get(position).getMember_id()
                .equals(Login.Companion.getInstance().getUid())){
            viewHolder.tv_message.setText(mWorkNotifyListBean.getMessage_list().get(position).getData());
        }else {
            viewHolder.tv_message.setText(mWorkNotifyListBean.getMessage_list().get(position).getTo_data());
        }

        if(mWorkNotifyListBean.getMessage_list().get(position).getIs_read()==1){
            viewHolder.tv_point.setVisibility(View.INVISIBLE);
        }else {
            viewHolder.tv_point.setVisibility(View.VISIBLE);
        }

        int type = mWorkNotifyListBean.getMessage_list().get(position).getType();
        if(type==10){
            viewHolder.tv_status.setText(R.string.approval_repeal);
            viewHolder.tv_status.setVisibility(View.VISIBLE);
        }else if(type==11){
            viewHolder.tv_status.setText(R.string.approval_reject);
            viewHolder.tv_status.setVisibility(View.VISIBLE);
        }else if(type==12){
            viewHolder.tv_status.setText(R.string.approval_agree);
            viewHolder.tv_status.setVisibility(View.VISIBLE);
        }else {
            viewHolder.tv_status.setVisibility(View.INVISIBLE);
        }
        return convertView;
    }

    private class ViewHolder{
        private TextView tv_time,tv_message,tv_point,tv_status;
    }
}
