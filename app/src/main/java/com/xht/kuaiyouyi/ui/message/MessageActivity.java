package com.xht.kuaiyouyi.ui.message;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.message.bean.UnreadMessageBean;
import com.xht.kuaiyouyi.ui.message.fragment.ChatListFragment;
import com.xht.kuaiyouyi.ui.message.fragment.SystemMessageFragment;
import com.xht.kuaiyouyi.ui.message.fragment.VerifyMessageFragment;
import com.xht.kuaiyouyi.ui.message.fragment.WorkNotifyFragment;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.BadgeView;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import q.rorbin.badgeview.QBadgeView;

public class MessageActivity extends BaseActivity implements VerifyMessageFragment.OnVerifyMessageReadNumUpdate,
        WorkNotifyFragment.OnWorkNotifyReadNumUpdate, SystemMessageFragment.OnSystemMessageReadNumUpdate {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.vp_content)
    ViewPager vp_content;
    @BindView(R.id.ll_tab)
    LinearLayout ll_tab;
    @BindView(R.id.ll_badge_view)
    LinearLayout ll_badge_view;

    @BindView(R.id.tv_tab_chat)
    TextView tv_tab_chat;
    @BindView(R.id.tv_tab_work_notify)
    TextView tv_tab_work_notify;
    @BindView(R.id.tv_tab_system_message)
    TextView tv_tab_system_message;
    @BindView(R.id.tv_tab_verify_message)
    TextView tv_tab_verify_message;

    private List<BaseFragment> mFragments;
    private MyAdapter mAdapter;

    private final int TYPE_CHAT = 0;
    private final int TYPE_WORK_NOTIFY = 1;
    private final int TYPE_SYSTEM_MESSAGE = 2;
    private final int TYPE_VERIFY_MESSAGE = 3;

    private int mUnreadWorkNotifyNum;
    private int mUnreadSystemMessageNum;
    private int mUnreadVerifyMessageNum;
    private QBadgeView mWorkNotifyBadgeView;
    private QBadgeView mSystemMessageBadgeView;
    private QBadgeView mVerifyMessageBadgeView;

    private int mHandleUnreadNum;//被处理的未读数量

    @Override
    protected int getLayout() {
        return R.layout.activity_message;
    }

    @Override
    protected void initView() {
        tv_title.setText(R.string.message_centre);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mFragments = new ArrayList<>();
        ChatListFragment chatListFragment = new ChatListFragment();
        WorkNotifyFragment workNotifyFragment = new WorkNotifyFragment();
        SystemMessageFragment systemMessageFragment = new SystemMessageFragment();
        VerifyMessageFragment verifyMessageFragment = new VerifyMessageFragment();
//        SystemNoticeFragment systemNoticeFragment = new SystemNoticeFragment();

        mFragments.add(chatListFragment);
        mFragments.add(workNotifyFragment);
        mFragments.add(systemMessageFragment);
        mFragments.add(verifyMessageFragment);
//        mFragments.add(systemNoticeFragment);

        mAdapter = new MyAdapter(getSupportFragmentManager());
        vp_content.setAdapter(mAdapter);
        addBadgeView();
        setTabSelectUI(TYPE_CHAT);
        requestUnreadMsg();

        vp_content.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setTabSelectUI(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tv_tab_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp_content.setCurrentItem(TYPE_CHAT);
            }
        });
        tv_tab_work_notify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp_content.setCurrentItem(TYPE_WORK_NOTIFY);
            }
        });
        tv_tab_system_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp_content.setCurrentItem(TYPE_SYSTEM_MESSAGE);
            }
        });
        tv_tab_verify_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp_content.setCurrentItem(TYPE_VERIFY_MESSAGE);
            }
        });

    }

    private void setTabSelectUI(int position) {
        for (int i = 0; i < ll_tab.getChildCount(); i++) {
            if (i == position) {
                ((TextView) (((ViewGroup) (ll_tab.getChildAt(i))).getChildAt(0))).setTextColor(getResources().getColor(R.color.blue_normal));
                ((View) (((ViewGroup) (ll_tab.getChildAt(i))).getChildAt(1))).setBackgroundResource(R.color.blue_normal);
            } else {
                ((TextView) (((ViewGroup) (ll_tab.getChildAt(i))).getChildAt(0))).setTextColor(getResources().getColor(R.color.grey_4));
                ((View) (((ViewGroup) (ll_tab.getChildAt(i))).getChildAt(1))).setBackgroundResource(android.R.color.transparent);
            }
        }
    }

    private void addBadgeView() {
        for (int i = 0; i < ll_tab.getChildCount(); i++) {
            if (i == TYPE_WORK_NOTIFY) {
                mWorkNotifyBadgeView = new BadgeView(this, ll_badge_view.getChildAt(i), 10, 4);
            } else if (i == TYPE_SYSTEM_MESSAGE) {
                mSystemMessageBadgeView = new BadgeView(this, ll_badge_view.getChildAt(i), 10, 4);
            } else if (i == TYPE_VERIFY_MESSAGE) {
                mVerifyMessageBadgeView = new BadgeView(this, ll_badge_view.getChildAt(i), 10, 4);
            }
        }
    }

    @Override
    public void onVerifyMessageNumReadUpdate(int num) {
        if (mUnreadVerifyMessageNum >= num) {
            mUnreadVerifyMessageNum = mUnreadVerifyMessageNum - num;
            mVerifyMessageBadgeView.setBadgeNumber(mUnreadVerifyMessageNum);
            mHandleUnreadNum =mHandleUnreadNum+num;
            Login.Companion.getInstance().setUnread_msg_num((Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num())-num)+"");
        }
    }

    @Override
    public void onWorkNotifyReadNumUpdate(int num) {
        if (mUnreadWorkNotifyNum >= num) {
            mUnreadWorkNotifyNum = mUnreadWorkNotifyNum - num;
            mWorkNotifyBadgeView.setBadgeNumber(mUnreadWorkNotifyNum);
            mHandleUnreadNum =mHandleUnreadNum+num;
            Login.Companion.getInstance().setUnread_msg_num((Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num())-num)+"");
        }
    }

    @Override
    public void onSystemMessageReadUpdate(int num) {
        if (mUnreadSystemMessageNum >= num) {
            mUnreadSystemMessageNum = mUnreadSystemMessageNum - num;
            mSystemMessageBadgeView.setBadgeNumber(mUnreadSystemMessageNum);
            mHandleUnreadNum =mHandleUnreadNum+num;
            Login.Companion.getInstance().setUnread_msg_num((Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num())-num)+"");
        }
    }

    @Override
    protected void onDestroy() {
        if(mHandleUnreadNum>0){
            Utils.getUnreadMsg();
        }
        super.onDestroy();
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public BaseFragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

        }

    }

    /**
     * 获取未读消息数
     */
    private void requestUnreadMsg() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_UNREAD_MSG())
                .withPOST(new NetCallBack<UnreadMessageBean>() {

                    @NotNull
                    @Override
                    public Class<UnreadMessageBean> getRealType() {
                        return UnreadMessageBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {

                    }

                    @Override
                    public void onSuccess(@NonNull UnreadMessageBean unreadMessageBean) {
                        if (unreadMessageBean.getMessage() != null) {
                            mUnreadVerifyMessageNum = unreadMessageBean.getMessage().getNewcompany();
                            mUnreadWorkNotifyNum = unreadMessageBean.getMessage().getNewapporove();
                            mUnreadSystemMessageNum = unreadMessageBean.getMessage().getNewsystem();
                            EventBus.getDefault().post(new MessageEvent(MessageEvent.UNREAD_MSG_NUM));

                            mVerifyMessageBadgeView.setBadgeNumber(mUnreadVerifyMessageNum);
                            mWorkNotifyBadgeView.setBadgeNumber(mUnreadWorkNotifyNum);
                            mSystemMessageBadgeView.setBadgeNumber(mUnreadSystemMessageNum);
                        }

                    }
                }, false);
    }

    @Override
    public void onBackPressed() {
        if (KyyApp.activitynum == 1) {
            goToActivity(MainActivity.class);
        }
        super.onBackPressed();
    }
}
