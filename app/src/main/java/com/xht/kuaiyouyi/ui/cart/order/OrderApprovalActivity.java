package com.xht.kuaiyouyi.ui.cart.order;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.cart.adapter.OrderApprovalAdapter;
import com.xht.kuaiyouyi.ui.cart.entity.ApprovalBean;
import com.xht.kuaiyouyi.ui.cart.entity.OrderApprovalBean;
import com.xht.kuaiyouyi.ui.cart.widget.ApprovalProcessDialog;
import com.xht.kuaiyouyi.ui.enterprise.Constant;
import com.xht.kuaiyouyi.ui.enterprise.activity.ApprovalDetailEnterpriseActivity;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.utils.Utils;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;

public class OrderApprovalActivity extends BaseActivity implements ApprovalProcessDialog.OnSelectPosition{
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_procurement)
    EditText tv_procurement;
    @BindView(R.id.tv_expected_time)
    TextView tv_expected_time;
    @BindView(R.id.edit_apply_event)
    EditText edit_apply_event;
    @BindView(R.id.btn_submit_procurement_apply)
    Button btn_submit_procurement_apply;
    @BindView(R.id.recycler_orderapproval)
    RecyclerView recycler_orderapproval;
    @BindView(R.id.relative_approval_group)
    RelativeLayout relative_approval_group;
    @BindView(R.id.tv_approval_group)
    TextView tv_approval_group;
    @BindView(R.id.tv_approval_process)
    TextView tv_approval_process;
    @BindView(R.id.relative_approver_people)
    RelativeLayout relative_approver_people;

    @BindView(R.id.relative_phase_payment)
    RelativeLayout relative_phase_payment;
    @BindView(R.id.relative_down_payment)
    RelativeLayout relative_down_payment;
    @BindView(R.id.linear_phase)
    LinearLayout linear_phase;
    @BindView(R.id.tv_freight_always)
    TextView tv_freight_always;
    @BindView(R.id.tv_total_amount_goods)
    TextView tv_total_amount_goods;
    @BindView(R.id.tv_total_amount_order)
    TextView tv_total_amount_order;
    @BindView(R.id.tv_order_down_payment)
    TextView tv_order_down_payment;
    @BindView(R.id.tv_order_tail_section)
    TextView tv_order_tail_section;
    @BindView(R.id.tv_order_added_new)
    TextView tv_order_added_new;
    @BindView(R.id.tv_added_new)
    TextView tv_added_new;
    @BindView(R.id.tv_first_phase_payment)
    TextView tv_first_phase_payment;
    @BindView(R.id.tv_order_first_phase_payment)
    TextView tv_order_first_phase_payment;
    @BindView(R.id.tv_order_second_phase_payment)
    TextView tv_order_second_phase_payment;

    @BindView(R.id.linear_orderdetils_stage_payment)
    LinearLayout linear_orderdetils_stage_payment;
    @BindView(R.id.tv_first_phase_price)
    TextView tv_first_phase_price;
    @BindView(R.id.tv_secon_phase_price)
    TextView tv_secon_phase_price;
    @BindView(R.id.first_phase_price)
    TextView first_phase_price;
    @BindView(R.id.secon_phase_price)
    TextView secon_phase_price;

    @BindView(R.id.tv_orderdetils_trade_way)
    TextView tv_orderdetils_trade_way;
    @BindView(R.id.tv_orderdetils_pay_way)
    TextView tv_orderdetils_pay_way;
    @BindView(R.id.tv_orderdetils_pay_currency)
    TextView tv_orderdetils_pay_currency;
    @BindView(R.id.tv_orderdetils_amount_total)
    TextView tv_orderdetils_amount_total;
    @BindView(R.id.tv_trading_currency)
    TextView tv_trading_currency;

    @BindView(R.id.relative_cc_people)
    RelativeLayout relative_cc_people;
    @BindView(R.id.flowlayout_cc)
    TagFlowLayout flowlayout_cc;
    @BindView(R.id.flowlayout_approver)
    TagFlowLayout flowlayout_approver;

    //公司id，订单id，预期时间，申请事由
    private String company_id;
    private String order_main_id;
    private String delivery_time;
    private String approval_id;
    private int mYear;
    private int mMonth;
    private int mDay;
    private Gson gson=new Gson();
    private OrderApprovalBean orderApprovalBean;
    private ApprovalProcessDialog.OnSelectPosition onSelectPosition;
    private ApprovalBean mapprovalBean;
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:
                    //审批人
                    if(mapprovalBean.getApprove_member()!=null && mapprovalBean.getApprove_member().size()!=0){
                        relative_approver_people.setVisibility(View.VISIBLE);
                        final List<String> datas=new ArrayList<>();
                        for(int i=0;i<mapprovalBean.getApprove_member().size();i++){
                            datas.add(mapprovalBean.getApprove_member().get(i));
                        }
                        TagAdapter<String> adapter= new TagAdapter<String >(datas) {
                            @Override
                            public View getView(FlowLayout parent, int position, String o) {
                                View view =View.inflate(OrderApprovalActivity.this, R.layout.approval_people,null);
                                TextView textView =view.findViewById(R.id.tv_apply_people);
                                textView.setText(o);
                                ImageView imageView=view.findViewById(R.id.iv_img);
                                //view.setBackgroundResource(R.drawable.flowlayout_item_text_bg);
                                if(position==datas.size()-1){
                                    imageView.setVisibility(View.GONE);
                                }
                                return view;
                            }
                        };
                        flowlayout_approver.setAdapter(adapter);
                    }else {
                        relative_approver_people.setVisibility(View.GONE);
                    }

                    //抄送人
                    if(mapprovalBean.getCopy_member()!=null && mapprovalBean.getCopy_member().size()!=0){
                        relative_cc_people.setVisibility(View.VISIBLE);
                        List<String> copys=new ArrayList<>();
                        for(int i=0;i<mapprovalBean.getCopy_member().size();i++){
                            copys.add(mapprovalBean.getCopy_member().get(i));
                        }
                        TagAdapter<String> ccadapter= new TagAdapter<String >(copys) {
                            @Override
                            public View getView(FlowLayout parent, int position, String o) {
                                View view =View.inflate(OrderApprovalActivity.this, R.layout.approval_people,null);
                                TextView textView =view.findViewById(R.id.tv_apply_people);
                                textView.setText(o);
                                ImageView imageView=view.findViewById(R.id.iv_img);
                                imageView.setVisibility(View.GONE);
                                return view;
                            }
                        };
                        flowlayout_cc.setAdapter(ccadapter);
                    }else {
                        relative_cc_people.setVisibility(View.GONE);
                    }

            }
        }
    };
    @Override
    protected int getLayout() {
        return R.layout.activity_orderapproval;
    }

    @Override
    protected void initView() {
        onSelectPosition=this;
        tv_title.setText(getString(R.string.purchasing_application));
        String order_approval=getIntent().getStringExtra("order_approval");
        orderApprovalBean=gson.fromJson(order_approval,OrderApprovalBean.class);
        order_main_id=Integer.toString(orderApprovalBean.getOrder_main_id());
        company_id=Integer.toString(orderApprovalBean.getCompany_id());
        //采购人
        tv_procurement.setText(orderApprovalBean.getApprove_creator());
        //总运费，商品总价，订单总价
        tv_freight_always.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_other_total())));
        tv_total_amount_goods.setText("¥"+ Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_goods_total())));
        tv_total_amount_order.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_amount())));
        //付款信息
        if(orderApprovalBean.getGoods_list().getOrder_main_deal_active().equals("2")){
            tv_orderdetils_trade_way.setText(getString(R.string.credit_payment));
        }else {
            tv_orderdetils_trade_way.setText(getString(R.string.full_payment));
        }
        if(orderApprovalBean.getGoods_list().getOrder_main_pay_active().equals("1")){
            tv_orderdetils_pay_way.setText(getString(R.string.bank_transfer));
        }else if(orderApprovalBean.getGoods_list().getOrder_main_pay_active().equals("2")){
            tv_orderdetils_pay_way.setText(getString(R.string.cheque_account));
        }else {
            tv_orderdetils_pay_way.setText(getString(R.string.online_transfer));
        }
        tv_orderdetils_pay_currency.setText(orderApprovalBean.getGoods_list().getOrder_main_pay_currency_name());
//        if(orderApprovalBean.getGoods_list().getOrder_main_pay_currency_name().equals("¥")){
//            tv_orderdetils_amount_total.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_pay_one())));
//        }else {
            tv_orderdetils_amount_total.setText(orderApprovalBean.getGoods_list().getOrder_main_pay_currency_symbol()
                    + Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_amount())*
                    Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_pay_currency_rate())));
        //}
        tv_trading_currency.setText("1:"+orderApprovalBean.getGoods_list().getOrder_main_pay_currency_rate());
        //首尾款
        if(orderApprovalBean.getGoods_list().getOrder_main_deal_active().equals("2")){
            relative_phase_payment.setVisibility(View.VISIBLE);
            relative_down_payment.setVisibility(View.VISIBLE);
            linear_phase.setVisibility(View.VISIBLE);
            linear_orderdetils_stage_payment.setVisibility(View.VISIBLE);
            tv_order_down_payment.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_pay_one())));
            tv_order_tail_section.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_pay_two())));
            if(Double.parseDouble(orderApprovalBean.getGoods_list().getSupplement_amount())>0){
                tv_added_new.setVisibility(View.VISIBLE);
                tv_order_added_new.setVisibility(View.VISIBLE);
                tv_order_added_new.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getSupplement_amount())));
                tv_first_phase_payment.setText(getString(R.string.first_phase_payment_new));
                if(orderApprovalBean.getGoods_list().getOrder_main_pay_active().equals("3")){
                    first_phase_price.setText(getString(R.string.order_detils_first_detils_tow));
                }else {
                    first_phase_price.setText(getString(R.string.order_detils_first_detils));
                }
            }
            tv_order_first_phase_payment.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_pay_first())));
            tv_order_second_phase_payment.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_pay_second())));

            tv_first_phase_price.setText(orderApprovalBean.getGoods_list().getOrder_main_pay_currency_symbol()
                    +Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_pay_first())*
                    Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_pay_currency_rate())));
            tv_secon_phase_price.setText(orderApprovalBean.getGoods_list().getOrder_main_pay_currency_symbol()
                    +Utils.getDisplayMoney(Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_pay_second())*
                    Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_pay_currency_rate())));
            if(orderApprovalBean.getGoods_list().getOrder_main_pay_active().equals("3")){
                first_phase_price.setText(getString(R.string.order_detils_first_detils_new_tow));
                secon_phase_price.setText(getString(R.string.order_detils_second_detils_tow));
            }
        }
        //商品清单
        OrderApprovalAdapter orderApprovalAdapter=new OrderApprovalAdapter(OrderApprovalActivity.this,R.layout.item_enterprise_goods_list,orderApprovalBean.getGoods_list().getOrder_list());
        recycler_orderapproval.setLayoutManager(new LinearLayoutManager(OrderApprovalActivity.this));
        recycler_orderapproval.setAdapter(orderApprovalAdapter);

        click();
    }

    private void click(){
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderApprovalActivity.this.finish();
            }
        });
        tv_expected_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar ca = Calendar.getInstance();
                mYear = ca.get(Calendar.YEAR);
                mMonth = ca.get(Calendar.MONTH);
                mDay = ca.get(Calendar.DAY_OF_MONTH);
                showDatePicker();
            }
        });

        tv_added_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double company_remain_money=Double.parseDouble(orderApprovalBean.getGoods_list().getOrder_main_pay_second())
                        -Double.parseDouble(orderApprovalBean.getGoods_list().getSupplement_amount());
                DialogUtils.createOneBtnDialog(OrderApprovalActivity.this, "（" + getString(R.string.order_line_credit) + "¥"
                        + company_remain_money + getString(R.string.order_insufficient_credit)
                        + "¥" + orderApprovalBean.getGoods_list().getOrder_main_pay_second() +
                        getString(R.string.order_supplement) + "¥" + orderApprovalBean.getGoods_list().getSupplement_amount() + "）", getString(R.string.know), new DialogUtils.OnLeftBtnListener() {
                    @Override
                    public void setOnLeftListener(Dialog dialog) {

                    }
                },false,false);
            }
        });

        btn_submit_procurement_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_submit_procurement_apply.setClickable(false);
                if(TextUtils.isEmpty(tv_expected_time.getText())){
                    ToastU.INSTANCE.showToast(getString(R.string.expected_time));
                    btn_submit_procurement_apply.setClickable(true);
                    return;
                }
                if(TextUtils.isEmpty(edit_apply_event.getText())){
                    btn_submit_procurement_apply.setClickable(true);
                    ToastU.INSTANCE.showToast(getString(R.string.input_apply_event));
                    return;
                }
                if(TextUtils.isEmpty(approval_id)){
                    btn_submit_procurement_apply.setClickable(true);
                    ToastU.INSTANCE.showToast(getString(R.string.approval_xuanze));
                    return;
                }
                if(TextUtils.isEmpty(tv_procurement.getText().toString().trim())){
                    btn_submit_procurement_apply.setClickable(true);
                    ToastU.INSTANCE.showToast(getString(R.string.approval_name));
                    return;
                }
                NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CREATE_APPROVE())
                        .addParam("company_id",company_id)
                        .addParam("order_main_id",order_main_id)
                        .addParam("delivery_time",delivery_time)
                        .addParam("apply_reason",edit_apply_event.getText().toString().trim())
                        .addParam("approve_id",approval_id)
                        .addParam("approve_creator",tv_procurement.getText().toString().trim())
                        .withPOST(new NetCallBack<String>() {

                            @NotNull
                            @Override
                            public Class<String> getRealType() {
                                return String.class;
                            }

                            @Override
                            public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                                ToastU.INSTANCE.showToast(err);
                                btn_submit_procurement_apply.setClickable(true);
                            }

                            @Override
                            public void onSuccess(@NonNull String s) {
                                btn_submit_procurement_apply.setClickable(true);
                                try {
                                    JSONObject jsonObject=new JSONObject(s);
                                    Bundle bundle=new Bundle();
                                    bundle.putString(ApprovalDetailEnterpriseActivity.APPROVE_ORDER_ID,jsonObject.getString("approve_order_id"));
                                    bundle.putString(Constant.COMPANY_ID,company_id);
                                    goToActivity(ApprovalDetailEnterpriseActivity.class,bundle);
                                    OrderApprovalActivity.this.finish();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },false);
            }
        });

        relative_approval_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApprovalProcessDialog approvalProcessDialog=new ApprovalProcessDialog(OrderApprovalActivity.this,orderApprovalBean.getGroups(),onSelectPosition);
                approvalProcessDialog.show();
            }
        });
    }


    //时间选择器
    private void showDatePicker(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(OrderApprovalActivity.this,R.style.MyDatePickerDialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        mYear = year;
                        mMonth = month;
                        mDay = dayOfMonth;
                        String days;
                        if (mMonth + 1 < 10) {
                            if (mDay < 10) {
                                days = new StringBuffer().append(mYear).append("年").append("0").
                                        append(mMonth + 1).append("月").append("0").append(mDay).append("日").toString();
                            } else {
                                days = new StringBuffer().append(mYear).append("年").append("0").
                                        append(mMonth + 1).append("月").append(mDay).append("日").toString();
                            }

                        } else {
                            if (mDay < 10) {
                                days = new StringBuffer().append(mYear).append("年").
                                        append(mMonth + 1).append("月").append("0").append(mDay).append("日").toString();
                            } else {
                                days = new StringBuffer().append(mYear).append("年").
                                        append(mMonth + 1).append("月").append(mDay).append("日").toString();
                            }

                        }
                        tv_expected_time.setText(days);
                        delivery_time=Utils.data(days);
                    }
                },
                mYear, mMonth, mDay);

            //设置起始日期和结束日期
            DatePicker datePicker = datePickerDialog.getDatePicker();
            datePicker.setMinDate(System.currentTimeMillis()-1000);
            datePickerDialog.updateDate(mYear,mMonth,mDay);
            datePickerDialog.show();

    }

    @Override
    public void onSelectPosition(int groupPosition, int childPosition) {
        tv_approval_group.setText(orderApprovalBean.getGroups().get(groupPosition).getGroup_name());
        tv_approval_process.setText(orderApprovalBean.getGroups().get(groupPosition).getGroup_approves().get(childPosition).getApprove_name());
        approval_id=orderApprovalBean.getGroups().get(groupPosition).getGroup_approves().get(childPosition).getId();
        getapproval();
    }

    //获取审批人，抄送人
    private void getapproval(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_APPROVAL_PEOPLE())
                .addParam("company_id",company_id)
                .addParam("approve_id",approval_id)
                .addParam("order_amount",orderApprovalBean.getGoods_list().getOrder_main_amount())
                .withPOST(new NetCallBack<ApprovalBean>() {

                    @NotNull
                    @Override
                    public Class<ApprovalBean> getRealType() {
                        return ApprovalBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {

                    }

                    @Override
                    public void onSuccess(@NonNull ApprovalBean approvalBean) {
                        if(isFinishing()){
                            return;
                        }
                        mapprovalBean=approvalBean;
                        handler.sendEmptyMessage(1);
                    }
                },false);

    }
}
