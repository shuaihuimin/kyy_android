package com.xht.kuaiyouyi.ui.search.entity;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

/**
 * Created by shuaihuimin on 2018/6/28.
 */

public class TypelistScreenData implements MultiItemEntity {
    /**
     * flag : 1
     * err_code : 0
     * msg : 获取成功！
     * data : {"brand_array":[{"brand_id":"143","brand_name":"施华洛世奇","brand_initial":"S","check":false},{"brand_id":"169","brand_name":"斯沃琪","brand_initial":"S","check":false},{"brand_id":"165","brand_name":"tissot","brand_initial":"T","check":false},{"brand_id":"164","brand_name":"欧米茄","brand_initial":"O","check":false},{"brand_id":"157","brand_name":"帝舵","brand_initial":"D","check":false},{"brand_id":"155","brand_name":"卡西欧","brand_initial":"K","check":false},{"brand_id":"170","brand_name":"阿玛尼","brand_initial":"A","check":false},{"brand_id":"146","brand_name":"Disney","brand_initial":"D","check":false},{"brand_id":"145","brand_name":"CK","brand_initial":"C","check":false},{"brand_id":"149","brand_name":"梅花","brand_initial":"M","check":false},{"brand_id":"326","brand_name":"玛克","brand_initial":"M","check":false},{"brand_id":"173","brand_name":"ooh Dear","brand_initial":"O","check":false},{"brand_id":"172","brand_name":"lux-women","brand_initial":"L","check":false},{"brand_id":"366","brand_name":"迅成","brand_initial":"X","check":false},{"brand_id":"327","brand_name":"美好家","brand_initial":"M","check":false},{"brand_id":"328","brand_name":"溢彩年华","brand_initial":"Y","check":false},{"brand_id":"329","brand_name":"欧司朗","brand_initial":"O","check":false},{"brand_id":"330","brand_name":"世家洁具","brand_initial":"S","check":false},{"brand_id":"343","brand_name":"三光云彩","brand_initial":"S","check":false},{"brand_id":"342","brand_name":"富安娜","brand_initial":"F","check":false},{"brand_id":"338","brand_name":"好事达","brand_initial":"H","check":false},{"brand_id":"337","brand_name":"索客","brand_initial":"S","check":false},{"brand_id":"336","brand_name":"罗莱","brand_initial":"L","check":false},{"brand_id":"345","brand_name":"乐扣乐扣","brand_initial":"L","check":false},{"brand_id":"335","brand_name":"爱仕达","brand_initial":"A","check":false},{"brand_id":"334","brand_name":"生活诚品","brand_initial":"S","check":false},{"brand_id":"344","brand_name":"乔曼帝","brand_initial":"Q","check":false},{"brand_id":"171","brand_name":"亨得利","brand_initial":"H","check":false},{"brand_id":"168","brand_name":"瑞士军刀","brand_initial":"R","check":false},{"brand_id":"147","brand_name":"佐卡伊","brand_initial":"Z","check":false},{"brand_id":"150","brand_name":"高仕","brand_initial":"G","check":false},{"brand_id":"151","brand_name":"宝玑","brand_initial":"B","check":false},{"brand_id":"152","brand_name":"一生一石","brand_initial":"Y","check":false},{"brand_id":"153","brand_name":"IDee","brand_initial":"I","check":false},{"brand_id":"154","brand_name":"elle","brand_initial":"E","check":false},{"brand_id":"156","brand_name":"爱卡","brand_initial":"A","check":false},{"brand_id":"158","brand_name":"新秀","brand_initial":"X","check":false},{"brand_id":"159","brand_name":"九钻","brand_initial":"J","check":false},{"brand_id":"160","brand_name":"卡地亚","brand_initial":"K","check":false},{"brand_id":"161","brand_name":"蓝色多瑙河","brand_initial":"L","check":false},{"brand_id":"162","brand_name":"浪琴","brand_initial":"L","check":false},{"brand_id":"163","brand_name":"百利恒","brand_initial":"B","check":false},{"brand_id":"166","brand_name":"新光饰品","brand_initial":"X","check":false},{"brand_id":"167","brand_name":"英雄","brand_initial":"Y","check":false},{"brand_id":"144","brand_name":"万宝龙","brand_initial":"W","check":false}],"attr_array":[{"name":"尺码1","value":[{"attr_value_id":"96","attr_value_name":"110","attr_id":"10","check":false},{"attr_value_id":"97","attr_value_name":"120","attr_id":"10","check":false},{"attr_value_id":"98","attr_value_name":"XXS","attr_id":"10","check":false},{"attr_value_id":"99","attr_value_name":"XS","attr_id":"10","check":false},{"attr_value_id":"100","attr_value_name":"S","attr_id":"10","check":false},{"attr_value_id":"101","attr_value_name":"M","attr_id":"10","check":false},{"attr_value_id":"102","attr_value_name":"L","attr_id":"10","check":false},{"attr_value_id":"103","attr_value_name":"XL","attr_id":"10","check":false},{"attr_value_id":"104","attr_value_name":"XXL","attr_id":"10","check":false},{"attr_value_id":"105","attr_value_name":"3XL","attr_id":"10","check":false},{"attr_value_id":"106","attr_value_name":"4XL","attr_id":"10","check":false},{"attr_value_id":"107","attr_value_name":"5XL","attr_id":"10","check":false},{"attr_value_id":"108","attr_value_name":"均码","attr_id":"10","check":false},{"attr_value_id":"109","attr_value_name":"135","attr_id":"10","check":false},{"attr_value_id":"110","attr_value_name":"140","attr_id":"10","check":false},{"attr_value_id":"111","attr_value_name":"150","attr_id":"10","check":false},{"attr_value_id":"112","attr_value_name":"160","attr_id":"10","check":false}]},{"name":"适用季节2","value":[{"attr_value_id":"114","attr_value_name":"冬季","attr_id":"11","check":false},{"attr_value_id":"115","attr_value_name":"春季","attr_id":"11","check":false},{"attr_value_id":"116","attr_value_name":"夏季","attr_id":"11","check":false},{"attr_value_id":"117","attr_value_name":"秋季","attr_id":"11","check":false}]},{"name":"风格3","value":[{"attr_value_id":"119","attr_value_name":"甜美","attr_id":"12","check":false},{"attr_value_id":"120","attr_value_name":"瑞丽","attr_id":"12","check":false},{"attr_value_id":"121","attr_value_name":"洛丽塔","attr_id":"12","check":false},{"attr_value_id":"122","attr_value_name":"波西米亚","attr_id":"12","check":false},{"attr_value_id":"123","attr_value_name":"波普","attr_id":"12","check":false},{"attr_value_id":"124","attr_value_name":"沙滩","attr_id":"12","check":false},{"attr_value_id":"125","attr_value_name":"名族","attr_id":"12","check":false},{"attr_value_id":"126","attr_value_name":"欧美简约","attr_id":"12","check":false},{"attr_value_id":"127","attr_value_name":"森女","attr_id":"12","check":false},{"attr_value_id":"128","attr_value_name":"梦幻公主","attr_id":"12","check":false},{"attr_value_id":"129","attr_value_name":"韩版","attr_id":"12","check":false},{"attr_value_id":"130","attr_value_name":"清纯休闲","attr_id":"12","check":false},{"attr_value_id":"131","attr_value_name":"运动","attr_id":"12","check":false},{"attr_value_id":"132","attr_value_name":"街拍","attr_id":"12","check":false},{"attr_value_id":"133","attr_value_name":"街头嘻哈","attr_id":"12","check":false},{"attr_value_id":"134","attr_value_name":"英式学院","attr_id":"12","check":false},{"attr_value_id":"135","attr_value_name":"英伦学院","attr_id":"12","check":false},{"attr_value_id":"136","attr_value_name":"维多利亚","attr_id":"12","check":false},{"attr_value_id":"137","attr_value_name":"简约","attr_id":"12","check":false},{"attr_value_id":"138","attr_value_name":"百搭","attr_id":"12","check":false},{"attr_value_id":"139","attr_value_name":"田园乡村","attr_id":"12","check":false},{"attr_value_id":"140","attr_value_name":"嘻哈风格","attr_id":"12","check":false},{"attr_value_id":"141","attr_value_name":"商务正装","attr_id":"12","check":false},{"attr_value_id":"142","attr_value_name":"商务休闲","attr_id":"12","check":false},{"attr_value_id":"143","attr_value_name":"名媛淑女","attr_id":"12","check":false},{"attr_value_id":"143","attr_value_name":"名媛淑女","attr_id":"12","check":false}]}]}
     */


        private List<BrandArrayBean> brand_array;
        private List<AttrArrayBean> attr_array;

        public List<BrandArrayBean> getBrand_array() {
            return brand_array;
        }

        public void setBrand_array(List<BrandArrayBean> brand_array) {
            this.brand_array = brand_array;
        }

        public List<AttrArrayBean> getAttr_array() {
            return attr_array;
        }

        public void setAttr_array(List<AttrArrayBean> attr_array) {
            this.attr_array = attr_array;
        }

    @Override
    public int getItemType() {
        return 0;
    }

    public static class BrandArrayBean {
            /**
             * brand_id : 143
             * brand_name : 施华洛世奇
             * brand_initial : S
             * check : false
             */

            private String brand_id;
            private String brand_name;
            private String brand_initial;
            private boolean check;

            public String getBrand_id() {
                return brand_id;
            }

            public void setBrand_id(String brand_id) {
                this.brand_id = brand_id;
            }

            public String getBrand_name() {
                return brand_name;
            }

            public void setBrand_name(String brand_name) {
                this.brand_name = brand_name;
            }

            public String getBrand_initial() {
                return brand_initial;
            }

            public void setBrand_initial(String brand_initial) {
                this.brand_initial = brand_initial;
            }

            public boolean isCheck() {
                return check;
            }

            public void setCheck(boolean check) {
                this.check = check;
            }
        }

        public static class AttrArrayBean {
            /**
             * name : 尺码1
             * value : [{"attr_value_id":"96","attr_value_name":"110","attr_id":"10","check":false},{"attr_value_id":"97","attr_value_name":"120","attr_id":"10","check":false},{"attr_value_id":"98","attr_value_name":"XXS","attr_id":"10","check":false},{"attr_value_id":"99","attr_value_name":"XS","attr_id":"10","check":false},{"attr_value_id":"100","attr_value_name":"S","attr_id":"10","check":false},{"attr_value_id":"101","attr_value_name":"M","attr_id":"10","check":false},{"attr_value_id":"102","attr_value_name":"L","attr_id":"10","check":false},{"attr_value_id":"103","attr_value_name":"XL","attr_id":"10","check":false},{"attr_value_id":"104","attr_value_name":"XXL","attr_id":"10","check":false},{"attr_value_id":"105","attr_value_name":"3XL","attr_id":"10","check":false},{"attr_value_id":"106","attr_value_name":"4XL","attr_id":"10","check":false},{"attr_value_id":"107","attr_value_name":"5XL","attr_id":"10","check":false},{"attr_value_id":"108","attr_value_name":"均码","attr_id":"10","check":false},{"attr_value_id":"109","attr_value_name":"135","attr_id":"10","check":false},{"attr_value_id":"110","attr_value_name":"140","attr_id":"10","check":false},{"attr_value_id":"111","attr_value_name":"150","attr_id":"10","check":false},{"attr_value_id":"112","attr_value_name":"160","attr_id":"10","check":false}]
             */

            private String name;
            private List<ValueBean> value;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public List<ValueBean> getValue() {
                return value;
            }
            public void setValue(List<ValueBean> value){
                this.value=value;
            }

            public static class ValueBean {
                /**
                 * attr_value_id : 96
                 * attr_value_name : 110
                 * attr_id : 10
                 * check : false
                 */

                private String attr_value_id;
                private String attr_value_name;
                private String attr_id;
                private boolean check;

                public String getAttr_value_id() {
                    return attr_value_id;
                }

                public void setAttr_value_id(String attr_value_id) {
                    this.attr_value_id = attr_value_id;
                }

                public String getAttr_value_name() {
                    return attr_value_name;
                }

                public void setAttr_value_name(String attr_value_name) {
                    this.attr_value_name = attr_value_name;
                }

                public String getAttr_id() {
                    return attr_id;
                }

                public void setAttr_id(String attr_id) {
                    this.attr_id = attr_id;
                }

                public boolean isCheck() {
                    return check;
                }

                public void setCheck(boolean check) {
                    this.check = check;
                }
            }
        }

}
