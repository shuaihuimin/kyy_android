package com.xht.kuaiyouyi.ui.enterprise.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.enterprise.bean.UserCompanies;
import com.xht.kuaiyouyi.utils.Login;

import java.util.List;

public class MyEnterpriseListsAdapter extends BaseAdapter {
    private Context mContext;
    private List<UserCompanies.CompaniesBean> mCompaniesBeans;

    public MyEnterpriseListsAdapter(Context mContext, List<UserCompanies.CompaniesBean> mCompaniesBeans) {
        this.mContext = mContext;
        this.mCompaniesBeans = mCompaniesBeans;
    }

    @Override
    public int getCount() {
        return mCompaniesBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return mCompaniesBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.item_enterprise_list, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_enterprise_name = convertView.findViewById(R.id.tv_enterprise_name);
            viewHolder.tv_point = convertView.findViewById(R.id.tv_point);
            viewHolder.tv_enterprise_status = convertView.findViewById(R.id.tv_enterprise_status);
            viewHolder.iv_selected = convertView.findViewById(R.id.iv_selected);
            viewHolder.v_line = convertView.findViewById(R.id.v_line);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tv_enterprise_name.setText(mCompaniesBeans.get(position).getCompany_name());
        if (mCompaniesBeans.get(position).getMessage_count() > 0) {
            viewHolder.tv_point.setVisibility(View.VISIBLE);
        } else {
            viewHolder.tv_point.setVisibility(View.GONE);
        }
        if (mCompaniesBeans.size() - 1 == position) {
            viewHolder.v_line.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.v_line.setVisibility(View.VISIBLE);
        }

        GradientDrawable enterpriseStatusDrawable = (GradientDrawable) mContext.getResources().getDrawable(R.drawable.enterprise_list_status_bg);
        switch (mCompaniesBeans.get(position).getCompany_status()) {
            case 2:
                //已冻结
                viewHolder.tv_enterprise_status.setVisibility(View.VISIBLE);
                viewHolder.tv_enterprise_status.setText(R.string.enterprise_status_frozened);
                viewHolder.tv_enterprise_status.setTextColor(mContext.getResources().getColor(R.color.red_1));
                enterpriseStatusDrawable.setColor(mContext.getResources().getColor(R.color.red_2));
                break;
            case 3:
                //已解散
                viewHolder.tv_enterprise_status.setVisibility(View.VISIBLE);
                viewHolder.tv_point.setVisibility(View.GONE);
                viewHolder.tv_enterprise_status.setText(R.string.search_enterprise_dissolved);
                viewHolder.tv_enterprise_status.setTextColor(mContext.getResources().getColor(android.R.color.white));
                enterpriseStatusDrawable.setColor(mContext.getResources().getColor(R.color.grey_12));
                break;
            case 4:
                //已过期
                viewHolder.tv_enterprise_status.setVisibility(View.VISIBLE);
                viewHolder.tv_enterprise_status.setText(R.string.search_enterprise_expired);
                viewHolder.tv_enterprise_status.setTextColor(mContext.getResources().getColor(R.color.red_1));
                enterpriseStatusDrawable.setColor(mContext.getResources().getColor(R.color.red_alpha));
                break;
            default:
                viewHolder.tv_enterprise_status.setVisibility(View.GONE);
                break;
        }
        viewHolder.tv_enterprise_status.setBackground(enterpriseStatusDrawable);

        if(mCompaniesBeans.get(position).getCompany_id().equals(Login.Companion.getInstance().getEnterprise_company_id())){
            viewHolder.tv_enterprise_name.setTextColor(mContext.getResources().getColor(R.color.blue_normal));
            viewHolder.iv_selected.setVisibility(View.VISIBLE);
        }else {
            viewHolder.tv_enterprise_name.setTextColor(mContext.getResources().getColor(R.color.grey_4));
            viewHolder.iv_selected.setVisibility(View.GONE);
        }
        return convertView;
    }

    private class ViewHolder {
        private TextView tv_enterprise_name, tv_point, tv_enterprise_status;
        private View v_line;
        private ImageView iv_selected;
    }
}
