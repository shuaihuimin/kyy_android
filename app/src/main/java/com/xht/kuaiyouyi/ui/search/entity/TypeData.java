package com.xht.kuaiyouyi.ui.search.entity;

import java.util.List;

/**
 * Created by shuaihuimin on 2018/6/20.
 */

public class TypeData {

    private List<ClassArrBean> class_arr;

    public List<ClassArrBean> getClass_arr() {
        return class_arr;
    }

    public void setClass_arr(List<ClassArrBean> class_arr) {
        this.class_arr = class_arr;
    }

    public static class ClassArrBean {
        /**
         * gc_id : 1
         * gc_name : 机械设备
         */
        private boolean select=false;
        private String gc_id;
        private String gc_name;

        public boolean isSelect() {
            return select;
        }

        public void setSelect(boolean select) {
            this.select = select;
        }
        public String getGc_id() {
            return gc_id;
        }

        public void setGc_id(String gc_id) {
            this.gc_id = gc_id;
        }

        public String getGc_name() {
            return gc_name;
        }

        public void setGc_name(String gc_name) {
            this.gc_name = gc_name;
        }
    }
}
