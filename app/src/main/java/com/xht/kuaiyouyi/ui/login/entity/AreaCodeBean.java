package com.xht.kuaiyouyi.ui.login.entity;

import com.contrarywind.interfaces.IPickerViewData;

public class AreaCodeBean implements IPickerViewData {
    private String item;

    public AreaCodeBean(String item) {
        this.item = item;
    }

    @Override
    public String getPickerViewText() {
        return item;
    }
}
