package com.xht.kuaiyouyi.ui.home.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.home.adapter.SeacondMarketAdapter;
import com.xht.kuaiyouyi.ui.home.entity.SecondMarkerketBean;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.ui.search.TypelistActivity;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.widget.BadgeView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class SeacondMarketActivity extends BaseActivity {
    @BindView(R.id.smartrefreshlayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.iv_seach)
    ImageView iv_seach;
    @BindView(R.id.iv_right)
    ImageView iv_right;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;

    private BadgeView mBadgeView;

    private String kyy="";
    private SeacondMarketAdapter seacondMarketAdapter;
    private List<SecondMarkerketBean.GoodsListBean> list=new ArrayList<>();
    private SecondMarkerketBean.CurrencytypeBean currlist =null;
    private int state=0;

    @Override
    protected int getLayout() {
        return R.layout.activity_seacond_market;
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        mBadgeView = new BadgeView(this,iv_right,10,4);
        getData();
        showUnreadMsgNum();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        iv_seach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString(TypelistActivity.CATE_ID,"");
                bundle.putInt("flag",2);
                bundle.putInt(TypelistActivity.Is_second_hand,1);
                goToActivity(SearchGoodsActivity.class,bundle);
            }
        });
        iv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Login.Companion.getInstance().isLogin()){
                    goToActivity(MessageActivity.class);
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                smartRefreshLayout.setNoMoreData(false);
                list.clear();
                kyy="";
                getData();
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                smartRefreshLayout.setNoMoreData(false);
                getData();
            }
        });
        bt_load_again.setVisibility(View.VISIBLE);
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.clear();
                kyy="";
                getData();
            }
        });
    }

    private void getData(){
        DialogUtils.createTipAllLoadDialog(SeacondMarketActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_SEACOND_MARKET())
                .addParam("keyword","")
                .withLoadPOST(new NetCallBack<SecondMarkerketBean>() {

                    @NotNull
                    @Override
                    public Class<SecondMarkerketBean> getRealType() {
                        return SecondMarkerketBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Refreshing){
                            smartRefreshLayout.finishRefresh();
                        }

                        if(TextUtils.isEmpty(kyy)){
                            error_retry_view.setVisibility(View.VISIBLE);
                            smartRefreshLayout.setVisibility(View.GONE);
                        }else {
                            ToastU.INSTANCE.showToast(getString(R.string.not_network));
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull SecondMarkerketBean secondMarkerketBean) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        smartRefreshLayout.setVisibility(View.VISIBLE);
                        error_retry_view.setVisibility(View.GONE);
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Refreshing){
                            smartRefreshLayout.finishRefresh();
                        }
                        if(secondMarkerketBean.getCurrencytype()!=null){

                        }
                        if(secondMarkerketBean.getGoods_list()!=null && secondMarkerketBean.getGoods_list().size()!=0){
                            list.addAll(secondMarkerketBean.getGoods_list());
                            currlist=secondMarkerketBean.getCurrencytype();
                        }else {
                            if(!TextUtils.isEmpty(kyy)){
                                smartRefreshLayout.finishLoadMoreWithNoMoreData();
                                smartRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
                            }
                        }
                        kyy=secondMarkerketBean.getKyy();
                        if(state==0){
                            state=1;
                            seacondMarketAdapter=new SeacondMarketAdapter(R.layout.store_recycler_item_layout,list,SeacondMarketActivity.this,currlist);
                            recyclerView.setLayoutManager(new LinearLayoutManager(SeacondMarketActivity.this));
                            recyclerView.setAdapter(seacondMarketAdapter);
                        }else {
                            seacondMarketAdapter.notifyDataSetChanged();
                        }
                    }
                },false,kyy);
    }

    private void showUnreadMsgNum() {
        if(Login.Companion.getInstance().isLogin()){
            mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.UNREAD_MSG_NUM)){
            showUnreadMsgNum();
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
