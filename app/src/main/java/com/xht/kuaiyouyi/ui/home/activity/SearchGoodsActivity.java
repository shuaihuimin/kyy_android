package com.xht.kuaiyouyi.ui.home.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.home.adapter.SearchLatelyWorldAdapter;
import com.xht.kuaiyouyi.ui.home.db.LatelyDBBean;
import com.xht.kuaiyouyi.ui.home.entity.SearchHotWordBean;
import com.xht.kuaiyouyi.ui.search.TypelistActivity;
import com.xht.kuaiyouyi.utils.TabLayoutUtils;
import com.xht.kuaiyouyi.utils.ToastU;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;
import org.litepal.LitePal;

import java.util.List;

import butterknife.BindView;

public class SearchGoodsActivity extends BaseActivity {
    @BindView(R.id.et_search)
    EditText et_search;
    @BindView(R.id.tv_cancel)
    TextView tv_cancel;
    @BindView(R.id.hsv_hot_word)
    HorizontalScrollView hsv_hot_word;
    @BindView(R.id.tv_clear_history)
    TextView tv_clear_history;
    @BindView(R.id.lv_search_history)
    ListView lv_search_history;
    @BindView(R.id.ll_search_hot_world)
    LinearLayout ll_search_hot_world;
    @BindView(R.id.tl_tabs)
    TabLayout tabLayout;

    private List<SearchHotWordBean.SearchBean> mSearchBeans;
    private List<LatelyDBBean> mLatelyDBBeans;
    private SearchLatelyWorldAdapter mSearchLatelyWorldAdapter;
    private int flag;
    private int[] tabs=new int[]{R.string.new_goods,R.string.second_hand};
    private int is_second_hand=0;

    @Override
    protected int getLayout() {
        return R.layout.activity_search_goods;
    }

    @Override
    protected void initView() {
        is_second_hand=getIntent().getIntExtra(TypelistActivity.Is_second_hand,0);
        flag=getIntent().getIntExtra("flag",0);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchGoodsActivity.this.finish();
            }
        });
        showTablist(tabs);
        requestHotWord();
        updateHistoryList();
        et_search.setHint(R.string.search_goods_tip);
        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    String keyword = et_search.getText().toString().trim();
                    if(!TextUtils.isEmpty(keyword)){
                        saveToDB(keyword);
                        updateHistoryList();
                        goToTypelistActivity(keyword);
                    }else {
                        Toast.makeText(getApplicationContext(),R.string.search_goods_tip,Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        tv_clear_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LitePal.deleteAll(LatelyDBBean.class,"is_second_hand=?",is_second_hand+"");
                updateHistoryList();
            }
        });
        lv_search_history.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String keyword = mLatelyDBBeans.get(position).getHot_world();
                et_search.setText(keyword);
                saveToDB(keyword);
                updateHistoryList();
                goToTypelistActivity(keyword);
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getText().equals("全新")){
                    is_second_hand=0;
                    updateHistoryList();
                }else {
                    is_second_hand=1;
                    updateHistoryList();
                }
                Log.i("info","--------is_second_hand"+is_second_hand);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void showTablist(int[] tabs){
            if(flag==2){
                tabLayout.addTab(tabLayout.newTab().setText(tabs[0]),false);
                tabLayout.addTab(tabLayout.newTab().setText(tabs[1]),true);
            }else {
                tabLayout.addTab(tabLayout.newTab().setText(tabs[0]),true);
                tabLayout.addTab(tabLayout.newTab().setText(tabs[1]),false);
            }
            TabLayoutUtils.reflex(tabLayout,75);
    }

    private void saveToDB(String keyword) {
        LatelyDBBean latelyDBBean = new LatelyDBBean();
        latelyDBBean.setHot_world(keyword);
        latelyDBBean.setIs_second_hand(is_second_hand);
        latelyDBBean.setSearch_time(System.currentTimeMillis());
        latelyDBBean.saveOrUpdate("hot_world=? AND is_second_hand=?",keyword,is_second_hand+"");
    }


    /**
     * 更新历史列表
     */
    private void updateHistoryList() {
        mLatelyDBBeans = LitePal.where("is_second_hand=?",is_second_hand+"").order("search_time desc").limit(5).find(LatelyDBBean.class);
        if(mLatelyDBBeans!=null){
            mSearchLatelyWorldAdapter = new SearchLatelyWorldAdapter(this,mLatelyDBBeans);
            lv_search_history.setAdapter(mSearchLatelyWorldAdapter);
        }
    }

    private void requestHotWord() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_SEARCH_HOT())
                .withPOST(new NetCallBack<SearchHotWordBean>() {
                    @NotNull
                    @Override
                    public Class<SearchHotWordBean> getRealType() {
                        return SearchHotWordBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        ToastU.INSTANCE.showToast(R.string.not_network);
                    }

                    @Override
                    public void onSuccess(@NonNull SearchHotWordBean searchHotWordBean) {
                        if(isFinishing()){
                            return;
                        }
                        mSearchBeans = searchHotWordBean.getSearch();
                        if (mSearchBeans != null && mSearchBeans.size() > 0) {
                            showData();
                        }
                    }
                }, false);

    }

    private void showData() {
//        et_search.setHint(mSearchBeans.get(Utils.getIntRandom(mSearchBeans.size())).getName());
        for(int i=0;i<mSearchBeans.size();i++){
            View view = View.inflate(this,R.layout.item_search_hot_word,null);
            final TextView tv_hot_word = view.findViewById(R.id.tv_hot_world);
            tv_hot_word.setText(mSearchBeans.get(i).getValue());
            ll_search_hot_world.addView(view);
            tv_hot_word.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveToDB(tv_hot_word.getText().toString().trim());
                    updateHistoryList();
                    goToTypelistActivity(tv_hot_word.getText().toString());
                }
            });
        }
    }

    private void goToTypelistActivity(String keyword){
        if(flag==0 || flag==2){
            Log.i("info","------zzzz1");
            Bundle bundle=new Bundle();
            bundle.putString(TypelistActivity.KEYWORD,keyword);
            bundle.putString(TypelistActivity.CATE_ID,"");
            bundle.putInt(TypelistActivity.Is_second_hand,is_second_hand);
            goToActivity(TypelistActivity.class,bundle);
            SearchGoodsActivity.this.finish();
        }if(flag==1){
            Log.i("info","------zzzz2");
            EventBus.getDefault().post(new MessageEvent(MessageEvent.KEYWORD,keyword,is_second_hand));
            SearchGoodsActivity.this.finish();
        }
    }
}
