package com.xht.kuaiyouyi.ui.message.bean;

import java.util.List;

public class SystemMessageListBean {


    /**
     * arr : [{"message_open":"1","message_time":"1535079437","message_body":"贵公司【哈--已解散】的信息更改审核已通过","type":0,"value_id":null},{"message_open":"1","message_time":"1535075538","message_body":"您的【qwerwr】于2018年08月24日被平台冻结，原因：【4564】，如有疑问，请致电00853-28855263","type":0,"value_id":null},{"message_open":"1","message_time":"1535074758","message_body":"您的【qwerwr】于2018年08月24日被平台冻结，原因：【huih】，如有疑问，请致电00853-28855263","type":0,"value_id":null},{"message_open":"1","message_time":"1535018324","message_body":"您的【qwerwr】于2018年08月23日被平台冻结，原因：【SDFSD】，如有疑问，请致电00853-28855263","type":0,"value_id":null},{"message_open":"1","message_time":"1534928222","message_body":"您创建的【qqq】审核不通过，原因：【12313】请重新提交，感谢您的配合短信已发送","type":0,"value_id":null},{"message_open":"1","message_time":"1534926090","message_body":"贵公司【联想】的信息更改审核不通过，原因：【123123】如有疑问，请致电00853-28855263","type":0,"value_id":null},{"message_open":"0","message_time":"1534490727","message_body":"订单号为【1000000000054001】采购申请审批通过，请尽快完成付款","type":1,"value_id":"384"},{"message_open":"1","message_time":"1534326728","message_body":"订单号为【1000000000049401】采购申请审批通过，请尽快完成付款","type":1,"value_id":"357"},{"message_open":"1","message_time":"1534320104","message_body":"您的訂單號爲【1000000000049302】的訂單已發貨。","type":1,"value_id":"356"},{"message_open":"1","message_time":"1534320103","message_body":"您的订单已经出库。","type":1,"value_id":"356"}]
     * kyy : eyJQIjoxLCJLIjoia3l5XzAwOCJ9
     */

    private String kyy;
    private List<ArrBean> arr;

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public List<ArrBean> getArr() {
        return arr;
    }

    public void setArr(List<ArrBean> arr) {
        this.arr = arr;
    }

    public static class ArrBean {
        /**
         * message_open : 1
         * message_time : 1535079437
         * message_body : 贵公司【哈--已解散】的信息更改审核已通过
         * type : 0
         * value_id : null
         */

        private int message_open;
        private String message_time;
        private String message_body;
        private int type;
        private String value_id;
        private String message_id;

        public int getMessage_open() {
            return message_open;
        }

        public void setMessage_open(int message_open) {
            this.message_open = message_open;
        }

        public String getMessage_time() {
            return message_time;
        }

        public void setMessage_time(String message_time) {
            this.message_time = message_time;
        }

        public String getMessage_body() {
            return message_body;
        }

        public void setMessage_body(String message_body) {
            this.message_body = message_body;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getValue_id() {
            return value_id;
        }

        public void setValue_id(String value_id) {
            this.value_id = value_id;
        }

        public String getMessage_id() {
            return message_id;
        }

        public void setMessage_id(String message_id) {
            this.message_id = message_id;
        }
    }
}
