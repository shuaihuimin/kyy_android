package com.xht.kuaiyouyi.ui.mine.setting;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.message.util.IMUtil;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import org.jetbrains.annotations.NotNull;
import org.litepal.LitePal;

import butterknife.BindView;
import cn.jpush.android.api.JPushInterface;

public class SettingActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView title;
    @BindView(R.id.iv_back)
    ImageView imgback;
    @BindView(R.id.iv_right)
    ImageView iv_right;
    @BindView(R.id.setting_head)
    ImageView setting_head;
    @BindView(R.id.login_exit)
    Button btnexit;
    @BindView(R.id.rl_user_info)
    RelativeLayout rl_user_info;
    @BindView(R.id.account_security)
    RelativeLayout account_security;
    @BindView(R.id.user_feedback)
    RelativeLayout user_feedback;
    @BindView(R.id.rl_clear_cache)
    RelativeLayout rl_clear_cache;
    @BindView(R.id.rl_about_kyy)
    RelativeLayout rl_about_kyy;

    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_phone)
    TextView tv_phone;

    private final int REQUEST_CODE = 1000;

    @Override
    protected int getLayout() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initView() {
        Glide.with(this).load(Login.Companion.getInstance().getMemberAvatar())
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                .into(setting_head);
        title.setText(R.string.account_setting);
        iv_right.setVisibility(View.GONE);
        tv_name.setText(Login.Companion.getInstance().getUsername());
        click();

    }

    private void click() {
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingActivity.this.finish();
            }
        });
        btnexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogUtils.createTwoBtnDialog(SettingActivity.this,
                        "确定要退出登录吗?", getString(R.string.dialog_confirm),
                        getString(R.string.dialog_cancel),
                        new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(Dialog dialog) {
                                requestLogout();
                            }
                        }, new DialogUtils.OnLeftBtnListener() {
                            @Override
                            public void setOnLeftListener(Dialog dialog) {

                            }
                        }, true, true);


            }
        });

        rl_user_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(SettingActivity.this,
                        UserInfoActivity.class), REQUEST_CODE);
            }
        });

        user_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(UserFeedbackActivity.class);
            }
        });

        rl_clear_cache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.createTwoBtnDialog(SettingActivity.this,
                        getString(R.string.sure_clear_cache), getString(R.string.dialog_confirm),
                        getString(R.string.dialog_cancel),
                        new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(final Dialog dialog) {
                                DialogUtils.createTipAllLoadDialog(SettingActivity.this);
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Utils.deleteDir(getCacheDir());
                                        Utils.deleteDir(getFilesDir());
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                DialogUtils.moven();
                                                DialogUtils.createTipImageAndTextDialog(
                                                        SettingActivity.this,
                                                        getString(R.string.clear_cache_succeful),
                                                        R.mipmap.png_icon_popup_ok);
                                            }
                                        });
                                    }
                                }).start();
                            }
                        }, new DialogUtils.OnLeftBtnListener() {
                            @Override
                            public void setOnLeftListener(Dialog dialog) {

                            }
                        }, true, true);

            }
        });

        rl_about_kyy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(AboutKyyActivity.class);
            }
        });

        account_security.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(AccountSecurityActivity.class);
            }
        });
    }

    /**
     * 登出,call api解绑华为的token
     */
    private void requestLogout() {
        DialogUtils.createTipAllLoadDialog(this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_QUIT_LOGIN())
                .addParam("member_id", Login.Companion.getInstance().getUid())
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(SettingActivity.this,err,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(!isFinishing()){
                            DialogUtils.moven();
                        }
                        exit();
                    }
                }, false);
    }

    private void exit() {
        Login.Companion.getInstance().setToken("");
        Login.Companion.getInstance().setToken_id("");
        Login.Companion.getInstance().setMemberAvatar("");
        Login.Companion.getInstance().setUsername("");
        Login.Companion.getInstance().setPhone("");
        Login.Companion.getInstance().setUid("");
        Login.Companion.getInstance().setEnterprise_company_id("");
        Login.Companion.getInstance().setUnread_msg_num("0");
        Login.Companion.getInstance().setMain_num("0");
        ((KyyApp)(KyyApp.context)).cartIndexBean = null;//退出的时候要置空
        if (IMUtil.socket != null) {
            IMUtil.socket.disconnect();
            //取消所有的监听事件
            IMUtil.socket.off();
        }
        LitePal.useDefault();
        if(!Utils.isHUAWEI()){
            JPushInterface.deleteAlias(getApplicationContext(), 0);
            Login.Companion.getInstance().set_set_alias("0");
        }
        Intent intent = new Intent(SettingActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tv_phone.setText(Login.Companion.getInstance().getPhone());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            Glide.with(this).load(Login.Companion.getInstance().getMemberAvatar())
                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                    .into(setting_head);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
