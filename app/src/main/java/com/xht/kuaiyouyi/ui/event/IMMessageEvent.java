package com.xht.kuaiyouyi.ui.event;

/**
 * 聊天消息的通知提示
 */
public class IMMessageEvent {
    private String message;
    private String store_name;
    private String chat_id;
    private String store_avatar;
    private String store_id;

    public IMMessageEvent(String message, String store_name, String chat_id, String store_avatar, String store_id) {
        this.message = message;
        this.store_name = store_name;
        this.chat_id = chat_id;
        this.store_avatar = store_avatar;
        this.store_id = store_id;
    }

    public String getMessage() {
        return message;
    }

    public String getStore_name() {
        return store_name;
    }

    public String getChat_id() {
        return chat_id;
    }

    public String getStore_avatar() {
        return store_avatar;
    }

    public String getStore_id() {
        return store_id;
    }
}


