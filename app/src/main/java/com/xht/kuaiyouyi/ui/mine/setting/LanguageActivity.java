package com.xht.kuaiyouyi.ui.mine.setting;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import butterknife.BindView;

public class LanguageActivity extends BaseActivity {
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_right)
    TextView tv_right;
    @BindView(R.id.ll_simple_chinese)
    LinearLayout ll_simple_chinese;
    @BindView(R.id.iv_simple_chinese_check)
    ImageView iv_simple_chinese_check;
    @BindView(R.id.ll_traditional_chinese)
    LinearLayout ll_traditional_chinese;
    @BindView(R.id.iv_traditional_chinese_check)
    ImageView iv_traditional_chinese_check;

    public static final String SIMPLE_CHINESE = "1";//简体中文
    public static final String TRADITIONAL_CHINESE = "2";//繁体中文
    private String mSelectLanguage = "";


    @Override
    protected int getLayout() {
        return R.layout.activity_language;
    }

    @Override
    protected void initView() {
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setText(R.string.complete);
        tv_title.setText(R.string.language_setting);
        changeCheckedStatus(Login.Companion.getInstance().getLanguageCode());
        iv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ll_simple_chinese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeCheckedStatus(SIMPLE_CHINESE);
            }

        });
        ll_traditional_chinese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeCheckedStatus(TRADITIONAL_CHINESE);
            }
        });
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainActivity();
            }
        });
    }

    /**
     * 显示状态
     *
     * @param checkLanguage 需要改变的语言
     */
    private void changeCheckedStatus(String checkLanguage) {
        switch (checkLanguage) {
            case SIMPLE_CHINESE:
                if (!mSelectLanguage.equals(SIMPLE_CHINESE)) {
                    iv_simple_chinese_check.setVisibility(View.VISIBLE);
                    iv_traditional_chinese_check.setVisibility(View.GONE);
                    mSelectLanguage = SIMPLE_CHINESE;
                }
                break;
            case TRADITIONAL_CHINESE:
                if (!mSelectLanguage.equals(TRADITIONAL_CHINESE)) {
                    iv_simple_chinese_check.setVisibility(View.GONE);
                    iv_traditional_chinese_check.setVisibility(View.VISIBLE);
                    mSelectLanguage = TRADITIONAL_CHINESE;
                }
                break;
        }
    }

    private void goToMainActivity() {
        Login.Companion.getInstance().setLanguageCode(mSelectLanguage + "");
        Utils.setLanguage(this);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}



