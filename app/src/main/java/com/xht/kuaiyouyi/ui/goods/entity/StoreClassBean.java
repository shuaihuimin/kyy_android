package com.xht.kuaiyouyi.ui.goods.entity;

import java.util.List;

public class StoreClassBean {

    /**
     * flag : 1
     * err_code : 0
     * msg :
     * data : {"store_cate":[{"stc_id":"1","stc_name":"季节","stc_parent_id":"0","stc_state":"1","store_id":"1","stc_sort":"0","child":[{"stc_id":"2","stc_name":"春","stc_parent_id":"1","stc_state":"1","store_id":"1","stc_sort":"0"},{"stc_id":"3","stc_name":"夏","stc_parent_id":"1","stc_state":"1","store_id":"1","stc_sort":"0"},{"stc_id":"13","stc_name":"fsd fs d","stc_parent_id":"1","stc_state":"1","store_id":"1","stc_sort":"0"}]},{"stc_id":"4","stc_name":"国别","stc_parent_id":"0","stc_state":"1","store_id":"1","stc_sort":"0","child":[{"stc_id":"12","stc_name":"sd fs d","stc_parent_id":"4","stc_state":"1","store_id":"1","stc_sort":"0"}]}]}
     */

        private List<StoreCateBean> store_cate;

        public List<StoreCateBean> getStore_cate() {
            return store_cate;
        }

        public void setStore_cate(List<StoreCateBean> store_cate) {
            this.store_cate = store_cate;
        }

        public static class StoreCateBean {
            /**
             * stc_id : 1
             * stc_name : 季节
             * stc_parent_id : 0
             * stc_state : 1
             * store_id : 1
             * stc_sort : 0
             * child : [{"stc_id":"2","stc_name":"春","stc_parent_id":"1","stc_state":"1","store_id":"1","stc_sort":"0"},{"stc_id":"3","stc_name":"夏","stc_parent_id":"1","stc_state":"1","store_id":"1","stc_sort":"0"},{"stc_id":"13","stc_name":"fsd fs d","stc_parent_id":"1","stc_state":"1","store_id":"1","stc_sort":"0"}]
             */

            private String stc_id;
            private String stc_name;
            private String stc_parent_id;
            private String stc_state;
            private String store_id;
            private String stc_sort;
            private List<ChildBean> child;

            public String getStc_id() {
                return stc_id;
            }

            public void setStc_id(String stc_id) {
                this.stc_id = stc_id;
            }

            public String getStc_name() {
                return stc_name;
            }

            public void setStc_name(String stc_name) {
                this.stc_name = stc_name;
            }

            public String getStc_parent_id() {
                return stc_parent_id;
            }

            public void setStc_parent_id(String stc_parent_id) {
                this.stc_parent_id = stc_parent_id;
            }

            public String getStc_state() {
                return stc_state;
            }

            public void setStc_state(String stc_state) {
                this.stc_state = stc_state;
            }

            public String getStore_id() {
                return store_id;
            }

            public void setStore_id(String store_id) {
                this.store_id = store_id;
            }

            public String getStc_sort() {
                return stc_sort;
            }

            public void setStc_sort(String stc_sort) {
                this.stc_sort = stc_sort;
            }

            public List<ChildBean> getChild() {
                return child;
            }

            public void setChild(List<ChildBean> child) {
                this.child = child;
            }

            public static class ChildBean {
                /**
                 * stc_id : 2
                 * stc_name : 春
                 * stc_parent_id : 1
                 * stc_state : 1
                 * store_id : 1
                 * stc_sort : 0
                 */

                private String stc_id;
                private String stc_name;
                private String stc_parent_id;
                private String stc_state;
                private String store_id;
                private String stc_sort;
                private boolean select;
                public boolean isSelect() {
                    return select;
                }

                public void setSelect(boolean select) {
                    this.select = select;
                }

                public String getStc_id() {
                    return stc_id;
                }

                public void setStc_id(String stc_id) {
                    this.stc_id = stc_id;
                }

                public String getStc_name() {
                    return stc_name;
                }

                public void setStc_name(String stc_name) {
                    this.stc_name = stc_name;
                }

                public String getStc_parent_id() {
                    return stc_parent_id;
                }

                public void setStc_parent_id(String stc_parent_id) {
                    this.stc_parent_id = stc_parent_id;
                }

                public String getStc_state() {
                    return stc_state;
                }

                public void setStc_state(String stc_state) {
                    this.stc_state = stc_state;
                }

                public String getStore_id() {
                    return store_id;
                }

                public void setStore_id(String store_id) {
                    this.store_id = store_id;
                }

                public String getStc_sort() {
                    return stc_sort;
                }

                public void setStc_sort(String stc_sort) {
                    this.stc_sort = stc_sort;
                }
            }
        }
}
