package com.xht.kuaiyouyi.ui.message.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.TImage;
import com.jph.takephoto.model.TResult;
import com.jph.takephoto.model.TakePhotoOptions;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.kuaiyouyi.BuildConfig;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.event.ChatListUpdateUnreadEvent;
import com.xht.kuaiyouyi.ui.goods.StoreActivity;
import com.xht.kuaiyouyi.ui.message.FaceManager;
import com.xht.kuaiyouyi.ui.message.adapter.ChatContentAdapter;
import com.xht.kuaiyouyi.ui.message.adapter.FaceAdapter;
import com.xht.kuaiyouyi.ui.message.adapter.FaceVPAdapter;
import com.xht.kuaiyouyi.ui.message.bean.MsgBean;
import com.xht.kuaiyouyi.ui.message.db.ChatListBean;
import com.xht.kuaiyouyi.ui.message.db.ChatMsgBean;
import com.xht.kuaiyouyi.ui.message.db.FileCache;
import com.xht.kuaiyouyi.ui.message.util.IMUtil;
import com.xht.kuaiyouyi.ui.mine.entity.UploadFileBean;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.FileUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.SignUtil;
import com.xht.kuaiyouyi.utils.SystemUtil;
import com.xht.kuaiyouyi.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import top.zibin.luban.CompressionPredicate;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;
import top.zibin.luban.OnRenameListener;

public class ChatActivity extends TakePhotoActivity {
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_right)
    TextView tv_right;
    @BindView(R.id.iv_face)
    ImageView iv_face;
    @BindView(R.id.et_input)
    EditText et_input;
    @BindView(R.id.vp_faces)
    ViewPager vp_faces;
    @BindView(R.id.ll_face_view)
    LinearLayout ll_face_view;
    @BindView(R.id.ll_points)
    LinearLayout ll_points;
    @BindView(R.id.lv_chat_content)
    ListView lv_chat_content;
    @BindView(R.id.tv_send)
    TextView tv_send;
    @BindView(R.id.srl_loadmore)
    SmartRefreshLayout srl_loadmore;

    @BindView(R.id.ll_more)
    LinearLayout ll_more;
    @BindView(R.id.tv_album)
    TextView tv_album;
    @BindView(R.id.tv_photo)
    TextView tv_photo;
    @BindView(R.id.tv_file)
    TextView tv_file;
    @BindView(R.id.iv_more)
    ImageView iv_more;

    private List<ImageView> mDotView;//选中点view
    private List<ChatMsgBean.ListBean> mListBeans;//聊天内容的数据源
    private ChatContentAdapter mAdapter;//聊天内容的适配器

    private String mChat_id;
    private String mStore_name;
    private String mStore_avatar;
    private long mStoreId;

    private long mGoods_id;
    private String mGoods_name;
    private String mGoods_img;
    private String mGoods_price;
    private boolean is_enquiry;
    private boolean is_second_hand;
    private boolean is_from_chatlist;//是否是从消息列表进入

    public static final String CHAT_ID = "chat_id";
    public static final String STORE_NAME = "store_name";
    public static final String STORE_AVATAR = "store_avatar";
    public static final String STORE_ID = "store_id";

    //商品的信息
    public static final String GOODS_ID = "goods_id";
    public static final String GOODS_NAME = "goods_name";
    public static final String GOODS_IMG = "goods_img";
    public static final String GOODS_PRICE = "goods_price";
    public static final String IS_ENQUIRY = "isenquiry";
    public static final String IS_SECONG_HAND = "is_second_hand";
    public static final String IS_FORM_CHATLIST = "is_from_chatlist";


    public static final int MENU_ITEM_COPY_ID = 1;
    public static final int PERMISSION_REQUEST_FILE = 15269;
    public static final int REQUEST_FILE_CODE = 15000;


    private String mKyy = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(getLayout());
        //绑定初始化ButterKnife
        ButterKnife.bind(this);
        initView();
        IMUtil.now_chat_store_id = mStoreId;
    }

    private void initData() {
        mChat_id = getIntent().getStringExtra(CHAT_ID);
        mStore_name = getIntent().getStringExtra(STORE_NAME);
        mStore_avatar = getIntent().getStringExtra(STORE_AVATAR);
        mStoreId = getIntent().getLongExtra(STORE_ID, 0);
        if (mStoreId == 0) {
            tv_right.setVisibility(View.GONE);
        }

        //商品的信息
        mGoods_id = getIntent().getLongExtra(GOODS_ID, 0);
        mGoods_name = getIntent().getStringExtra(GOODS_NAME);
        mGoods_img = getIntent().getStringExtra(GOODS_IMG);
        mGoods_price = getIntent().getStringExtra(GOODS_PRICE);
        is_enquiry = getIntent().getBooleanExtra(IS_ENQUIRY, false);
        is_second_hand = getIntent().getBooleanExtra(IS_SECONG_HAND, false);

        is_from_chatlist = getIntent().getBooleanExtra(IS_FORM_CHATLIST, false);

        if (!TextUtils.isEmpty(mStore_name)) {
            tv_title.setText(mStore_name);
        }
        mListBeans = new ArrayList<>();
        mAdapter = new ChatContentAdapter(ChatActivity.this, mListBeans, mStore_avatar);
        lv_chat_content.setAdapter(mAdapter);
        requestChatLog();
    }

    protected int getLayout() {
        return R.layout.activity_chat;
    }

    protected void initView() {
        EventBus.getDefault().register(this);
        registerForContextMenu(lv_chat_content);
        iv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setText(R.string.goods_login_store);
        iv_face.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_more.getVisibility() == View.VISIBLE) {
                    ll_more.setVisibility(View.GONE);
                }
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (ll_face_view.getVisibility() == View.VISIBLE) {
                    ll_face_view.setVisibility(View.GONE);
                    imm.showSoftInput(et_input, InputMethodManager.SHOW_IMPLICIT);
                } else {
                    imm.hideSoftInputFromWindow(ll_face_view.getApplicationWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ll_face_view.setVisibility(View.VISIBLE);
                }
                lv_chat_content.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lv_chat_content.setSelection(lv_chat_content.getCount() - 1);
                    }
                }, 500);
            }
        });

        //在xml中设置发送动作之后，还有在代码中设置下面两个属性才能在多行的情况下显示发送按钮
        et_input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        et_input.setSingleLine(false);
        et_input.setMaxLines(4);

//        //弹出键盘
//        cet_input.requestFocus();
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputManager.showSoftInput(cet_input, 0);
//            }
//        }, 200);

        et_input.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    //点击输入框的事件
                    ll_face_view.setVisibility(View.GONE);
                    ll_more.setVisibility(View.GONE);
                    lv_chat_content.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            lv_chat_content.setSelection(lv_chat_content.getCount() - 1);
                        }
                    }, 500);
                }
                return false;
            }

        });

        lv_chat_content.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (ll_face_view.getVisibility() == View.VISIBLE) {
                    ll_face_view.setVisibility(View.GONE);
                }
                if (ll_more.getVisibility() == View.VISIBLE) {
                    ll_more.setVisibility(View.GONE);
                }
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ll_face_view.getApplicationWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                return false;
            }
        });

        tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMsg(MsgBean.MSG_TYPE_TEXT, 0, null, null,null);
            }
        });

        et_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    tv_send.setVisibility(View.VISIBLE);
                    iv_more.setVisibility(View.GONE);
                } else {
                    tv_send.setVisibility(View.GONE);
                    iv_more.setVisibility(View.VISIBLE);
                }
            }
        });

        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!is_from_chatlist){
                    requestUpdateMsgRead();
                }
                Intent intent = new Intent(ChatActivity.this, StoreActivity.class);
                intent.putExtra("store_id", mStoreId + "");
                startActivity(intent);
            }
        });

        iv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_face_view.getVisibility() == View.VISIBLE) {
                    ll_face_view.setVisibility(View.GONE);
                }
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (ll_more.getVisibility() == View.VISIBLE) {
                    ll_more.setVisibility(View.GONE);
                    imm.showSoftInput(et_input, InputMethodManager.SHOW_IMPLICIT);
                } else {
                    imm.hideSoftInputFromWindow(ll_face_view.getApplicationWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ll_more.setVisibility(View.VISIBLE);
                }
                lv_chat_content.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lv_chat_content.setSelection(lv_chat_content.getCount() - 1);
                    }
                }, 500);
            }
        });
        tv_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTakePhoto().onPickFromGallery();
            }
        });
        tv_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), System.currentTimeMillis() + ".jpg");
                Uri imageUri = Uri.fromFile(file);
                CompressConfig config = new CompressConfig.Builder()
                        .setMaxSize(500 * 1024)
                        .create();
                TakePhotoOptions takePhotoOptions = new TakePhotoOptions.Builder().create();
                takePhotoOptions.setCorrectImage(true);
                getTakePhoto().setTakePhotoOptions(takePhotoOptions);
                getTakePhoto().onEnableCompress(config, false);
                getTakePhoto().onPickFromCapture(imageUri);
            }
        });
        tv_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                    selectFile();
                }else {
                    ActivityCompat.requestPermissions(ChatActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PERMISSION_REQUEST_FILE);
                }
            }
        });

        initFace();
        initData();
        mAdapter.setListener(new ChatContentAdapter.OnSendGoodsInfoClickListener() {
            @Override
            public void onSendGoodsInfo(int position) {
                sendMsg(MsgBean.MSG_TYPE_GOODS_INFO, position, null, null,null);
            }
        });

        srl_loadmore.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                requestChatLog();
            }
        });

    }

    private void selectFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent,REQUEST_FILE_CODE);
    }

    /**
     * 发消息
     *
     * @param msgType      消息类型，使用注解来限制参数的值，传入的值不对编辑器就会报错
     * @param postion      msgType==MsgBean.MSG_TYPE_GOODS_INFO的时候用到
     * @param imgUrl       msgType==MsgBean.MSG_TYPE_IMG的时候用到
     * @param fileLocalPath msgType==MsgBean.MSG_TYPE_IMG或者MsgBean.MSG_TYPE_FILE的时候用到
     * @param fileBean msgType==MsgBean.MSG_TYPE_FILE的时候用到
     */
    private void sendMsg(@MsgBean.MsgType int msgType, int postion, String imgUrl, String fileLocalPath, MsgBean.FileBean fileBean) {
        if (msgType == MsgBean.MSG_TYPE_TEXT && TextUtils.isEmpty(et_input.getText().toString().trim())) {
            return;
        }
        if (IMUtil.socket.connected()) {
            MsgBean sendMsgBean = new MsgBean();
            sendMsgBean.setF_id(Long.valueOf(Login.Companion.getInstance().getUid()));
            sendMsgBean.setF_ip(Utils.getIPAddress(ChatActivity.this));
            sendMsgBean.setF_name(Login.Companion.getInstance().getUsername());
            sendMsgBean.setT_id(Long.valueOf(mChat_id));
            sendMsgBean.setT_name(mStore_name);
            sendMsgBean.setStore_id(mStoreId);
            sendMsgBean.setGoods_id(mGoods_id);
            sendMsgBean.setMsg_type(msgType);
            sendMsgBean.setAdd_time(Utils.getSignTime());
            MsgBean.UserBean userBean = new MsgBean.UserBean();
            userBean.setU_id(Long.valueOf(Login.Companion.getInstance().getUid()));
            userBean.setAvatar(Login.Companion.getInstance().getMemberAvatar());
            userBean.setU_name(Login.Companion.getInstance().getUsername());
            sendMsgBean.setUser(userBean);
            MsgBean.StoreBean storeBean = new MsgBean.StoreBean();
            storeBean.setStore_name(mStore_name);
            storeBean.setStore_avatar(mStore_avatar);
            storeBean.setStore_id(mStoreId);
            sendMsgBean.setStore_info(storeBean);
            if (msgType == MsgBean.MSG_TYPE_TEXT) {
                sendMsgBean.setT_msg(et_input.getText().toString().trim());
            } else if (msgType == MsgBean.MSG_TYPE_GOODS_INFO) {
                MsgBean.GoodsInfoBean goodsInfoBean = new MsgBean.GoodsInfoBean();
                goodsInfoBean.setPic(mListBeans.get(postion).getGoods_info().getPic());
                goodsInfoBean.setGoods_id(mGoods_id);
                goodsInfoBean.setStore_id(mStoreId);
                goodsInfoBean.setIs_enquiry(mListBeans.get(postion).getGoods_info().getIs_enquiry());
                goodsInfoBean.setGoods_name(mListBeans.get(postion).getGoods_info().getGoods_name());
                goodsInfoBean.setGoods_promotion_price(mListBeans.get(postion).getGoods_info().getGoods_promotion_price());
                sendMsgBean.setGoods_info(goodsInfoBean);
                sendMsgBean.setT_msg(BuildConfig.DOMAIN + "shop/index.php?act=goods&op=index&goods_id=" + mGoods_id);
            } else if (msgType == MsgBean.MSG_TYPE_IMG) {
                sendMsgBean.setT_msg("<img src=\"" + imgUrl + "\" />");
                MsgBean.PicSizeBean picSizeBean = new MsgBean.PicSizeBean();
                int[] imgOption = Utils.getImageWidthAndHeight(fileLocalPath);
                picSizeBean.setWidth(imgOption[0]);
                picSizeBean.setHeight(imgOption[1]);
                sendMsgBean.setPicSize(picSizeBean);
            }else if(msgType == MsgBean.MSG_TYPE_FILE){
                sendMsgBean.setT_msg(new Gson().toJson(fileBean));
                MsgBean.FileBean sendFileBean = new MsgBean.FileBean();
                sendFileBean.setFile_type(fileBean.getFile_type());
                sendFileBean.setFile_name(fileBean.getFile_name());
                sendFileBean.setFile_size(fileBean.getFile_size());
                sendFileBean.setFile_url(fileBean.getFile_url());
                sendMsgBean.setFile(sendFileBean);
            }
            String jsonSendMsg = new Gson().toJson(sendMsgBean);
            try {
                IMUtil.socket.emit("app_send_msg", new JSONObject(jsonSendMsg));
                if(msgType == MsgBean.MSG_TYPE_FILE){
                    //发送成功之后缓存本地路径
                    FileCache fileCache = new FileCache();
                    fileCache.setFile_length(fileBean.getFile_size());
                    fileCache.setFile_url(fileBean.getFile_url());
                    fileCache.setFile_name(fileBean.getFile_name());
                    fileCache.setFile_type(fileBean.getFile_type());
                    fileCache.setFile_local_path(fileLocalPath);
                    fileCache.save();
                }
                if(msgType == MsgBean.MSG_TYPE_TEXT){
                    clearInputText();
                }
                Utils.log("imutil-log", "app_send_msg:" + jsonSendMsg);
            } catch (JSONException e) {
                e.printStackTrace();
                Utils.log("imutil-log", "app_send_msg:" + "发送失败");
                return;
            }


            //聊天内容数据
            ChatMsgBean.ListBean listBean = new ChatMsgBean.ListBean();
            listBean.setF_id(Long.valueOf(Login.Companion.getInstance().getUid()));
            listBean.setStore_id(mStoreId);
            listBean.setAdd_time(Utils.getSignTime());
            listBean.setMsg_type(msgType);
            listBean.setT_id(Long.parseLong(mChat_id));
            if (msgType == MsgBean.MSG_TYPE_TEXT) {
                listBean.setT_msg(et_input.getText().toString().trim());
            } else if (msgType == MsgBean.MSG_TYPE_GOODS_INFO) {
                ChatMsgBean.ListBean.GoodsInfoBean goodsInfoBean = new ChatMsgBean.ListBean.GoodsInfoBean();
                goodsInfoBean.setGoods_id(sendMsgBean.getGoods_info().getGoods_id() + "");
                goodsInfoBean.setGoods_name(sendMsgBean.getGoods_info().getGoods_name());
                goodsInfoBean.setIs_enquiry(sendMsgBean.getGoods_info().getIs_enquiry());
                if (sendMsgBean.getGoods_info().getIs_enquiry() == 0) {
                    goodsInfoBean.setGoods_promotion_price(sendMsgBean.getGoods_info().getGoods_promotion_price());
                }
                goodsInfoBean.setPic(sendMsgBean.getGoods_info().getPic());
                listBean.setGoods_info(goodsInfoBean);
                listBean.setIs_show_button(false);
                listBean.setT_msg(sendMsgBean.getT_msg());
            } else if (msgType == MsgBean.MSG_TYPE_IMG) {
                listBean.setT_msg("<img src=\"" + imgUrl + "\" />");
                listBean.setImageWidth(sendMsgBean.getPicSize().getWidth());
                listBean.setImageHeight(sendMsgBean.getPicSize().getHeight());
                listBean.setLocalImgPath(fileLocalPath);
            }else if (msgType == MsgBean.MSG_TYPE_FILE) {
                listBean.setT_msg(new Gson().toJson(fileBean));
                MsgBean.FileBean sendFileBean = new MsgBean.FileBean();
                sendFileBean.setFile_type(fileBean.getFile_type());
                sendFileBean.setFile_name(fileBean.getFile_name());
                sendFileBean.setFile_size(fileBean.getFile_size());
                sendFileBean.setFile_url(fileBean.getFile_url());
                listBean.setFile(sendFileBean);
            }
            mListBeans.add(listBean);
            if (!isFinishing()) {
                mAdapter.notifyDataSetChanged();
                lv_chat_content.setSelection(lv_chat_content.getCount() - 1);
            }

            //聊天列表数据
            ChatListBean.DataBean dataBean = new ChatListBean.DataBean();
            dataBean.setStore_id(mStoreId + "");
            dataBean.setStore_name(mStore_name);
            dataBean.setMsg_type(listBean.getMsg_type());
            dataBean.setNo_read_msg_num(0);
            dataBean.setT_msg(listBean.getT_msg());
            dataBean.setTime(listBean.getAdd_time());
            dataBean.setU_id(mChat_id);
            //通知聊天列表更新
            EventBus.getDefault().post(dataBean);
        }else {
            Toast.makeText(this,R.string.chat_disconnected,Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 清空输入框的内容
     */
    private void clearInputText() {
        et_input.post(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    et_input.setText("");
                }
            }
        });
    }

    private void initFace() {
        final List<View> facePageViews = new ArrayList<>();
        //加2是为了加上左右滑动的效果
        for (int i = 0; i < FaceManager.getInstance().getPageCount() + 2; i++) {
            GridView gridView = new GridView(this);
            FaceAdapter adapter = new FaceAdapter(this, i);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    View img = view.findViewById(R.id.iv_item_face);
                    if (FaceManager.getInstance().getEmojiCount() > (Integer) img.getTag()) {
//                        ToastU.INSTANCE.showToast(img.getTag().toString());
                        int startIndex = et_input.getSelectionStart();
                        String emoji = FaceManager.getInstance().getSourceByImgTag((Integer) img.getTag());
                        SpannableString span = FaceManager.getInstance().getEmotionContent(
                                ChatActivity.this, emoji);
                        et_input.getText().insert(startIndex, span);
                    }

                }
            });
            gridView.setAdapter(adapter);
            gridView.setNumColumns(8);
            gridView.setBackgroundColor(Color.TRANSPARENT);
            gridView.setHorizontalSpacing(1);
            gridView.setVerticalSpacing(1);
            gridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
            gridView.setCacheColorHint(0);
            gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
            gridView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            gridView.setGravity(Gravity.CENTER);
            facePageViews.add(gridView);
        }

        FaceVPAdapter adapter = new FaceVPAdapter(facePageViews);
        vp_faces.setAdapter(adapter);
        vp_faces.setCurrentItem(1);
        vp_faces.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (0 == position) {
                    vp_faces.setCurrentItem(position + 1);
                } else if (position == facePageViews.size() - 1) {
                    vp_faces.setCurrentItem(position - 1);
                } else {
                    setSelDot(position - 1);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mDotView = new ArrayList<>();
        for (int i = 0; i < facePageViews.size() - 2; i++) {
            ImageView imgView = new ImageView(this);
            imgView.setBackgroundResource(R.drawable.indicator_dot_nor);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    new ViewGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));
            DisplayMetrics dm = getResources().getDisplayMetrics();
            params.leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, dm);
            params.rightMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, dm);
            ll_points.addView(imgView, params);
            mDotView.add(imgView);
        }
        setSelDot(0);
    }

    private void setSelDot(int nSelIndex) {
        for (int i = 0; i < mDotView.size(); i++) {
            if (nSelIndex == i) {
                mDotView.get(i).setBackgroundResource(
                        R.drawable.indicator_dot_sel);
            } else {
                mDotView.get(i).setBackgroundResource(
                        R.drawable.indicator_dot_nor);
            }
        }
    }

    //收消息的监听事件
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getListBeanEvent(ChatMsgBean.ListBean listBean) {
        //更新消息显示
        if (listBean.getStore_id() == mStoreId) {
            mListBeans.add(listBean);
            mAdapter.notifyDataSetChanged();
            lv_chat_content.setSelection(lv_chat_content.getBottom());
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void takeCancel() {
        super.takeCancel();
    }

    @Override
    public void takeFail(TResult result, String msg) {
        super.takeFail(result, msg);
    }

    @Override
    protected void onPause() {
        super.onPause();
        IMUtil.now_chat_store_id = 0;
    }

    @Override
    public void takeSuccess(final TResult result) {
        super.takeSuccess(result);
        String imgPath = "";
        if (result.getImage().getFromType() == TImage.FromType.OTHER) {
            //从相册选择图片回来要压缩才上传，第三方库从相册选择图片并设置压缩会出现压缩了的图片被旋转了的问题
            imgPath = result.getImage().getOriginalPath();
            Luban.with(this)
                    .load(imgPath)
                    .ignoreBy(500)
                    .setTargetDir(getCacheDir().getAbsolutePath())
                    .setFocusAlpha(false)
                    .filter(new CompressionPredicate() {
                        @Override
                        public boolean apply(String path) {
                            return !TextUtils.isEmpty(path);
                        }
                    })
                    .setRenameListener(new OnRenameListener() {
                        @Override
                        public String rename(String filePath) {
                            try {
                                MessageDigest md = MessageDigest.getInstance("MD5");
                                md.update(filePath.getBytes());
                                return new BigInteger(1, md.digest()).toString(32) + new File(filePath).getName();
                            } catch (NoSuchAlgorithmException e) {
                                e.printStackTrace();
                            }
                            return "";
                        }
                    })
                    .setCompressListener(new OnCompressListener() {
                        @Override
                        public void onStart() {
                        }

                        @Override
                        public void onSuccess(File file) {
                            requestUploadFile(file.getPath(),"3");
                        }

                        @Override
                        public void onError(Throwable e) {

                        }
                    }).launch();
        } else {
            imgPath = result.getImage().getCompressPath();
            requestUploadFile(imgPath,"3");
        }
    }

    /**
     *
     * @param fileLocalPath  本地文件路径
     * @param type 3是传图片，4是传文件
     */
    private void requestUploadFile(final String fileLocalPath, final String type) {
        DialogUtils.createTipAllLoadDialog(this);
        final File file = new File(fileLocalPath);
        String t = SystemUtil.INSTANCE.getTime(SystemUtil.INSTANCE.
                getTimeStr("" + System.currentTimeMillis()));
        Map<String, Object> map = new HashMap<>();
        map.put("device_uuid", NetUtil.Companion.getUuid());
        map.put("device_name", NetUtil.Companion.getDevice_name());
        map.put("type", type);
        map.put("store_id", mStoreId+"");
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_UPLOADIMG())
                .addParam("device_uuid", NetUtil.Companion.getUuid())
                .addParam("device_name", NetUtil.Companion.getDevice_name())
                .addParam("token_id", Login.Companion.getInstance().getToken_id())
                .addParam("sign_time", t)
                .addParam("sign", SignUtil.sign(map, t))
                .addParam("type", type)
                .addParam("store_id", mStoreId+"")
                .addParam(file.getName(), file)
                .withPOSTFile(new NetCallBack<UploadFileBean>() {
                    @NotNull
                    @Override
                    public Class<UploadFileBean> getRealType() {
                        return UploadFileBean.class;
                    }

                    @Override
                    public void onSuccess(@NonNull UploadFileBean uploadFileBean) {
                        DialogUtils.moven();
                        if (!TextUtils.isEmpty(uploadFileBean.getFile_name()) && type.equals("3")) {
                            sendMsg(MsgBean.MSG_TYPE_IMG, 0, uploadFileBean.getFile_name(), fileLocalPath,null);
                        }
                        if (!TextUtils.isEmpty(uploadFileBean.getFile_name()) && type.equals("4")) {
                            MsgBean.FileBean fileBean = new MsgBean.FileBean();
                            fileBean.setFile_name(uploadFileBean.getFile_name_short());
                            fileBean.setFile_size(file.length());
                            fileBean.setFile_url(uploadFileBean.getFile_name());
                            fileBean.setFile_type(Utils.getFileType(uploadFileBean.getFile_name_short()));
                            sendMsg(MsgBean.MSG_TYPE_FILE, 0, null,fileLocalPath, fileBean);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        DialogUtils.moven();
                        Toast.makeText(getApplicationContext(), err, Toast.LENGTH_SHORT).show();
                    }
                }, false);
    }

    private void requestChatLog() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CHAT_LOG())
                .addParam("f_id", Login.Companion.getInstance().getUid())
                .addParam("t_id", mChat_id)
                .addParam("store_id", mStoreId)
                .withLoadPOST(new NetCallBack<ChatMsgBean>() {
                    @NotNull
                    @Override
                    public Class<ChatMsgBean> getRealType() {
                        return ChatMsgBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        if (srl_loadmore.getState() == RefreshState.Refreshing) {
                            srl_loadmore.finishRefresh();
                        }
                        Toast.makeText(getApplicationContext(), err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull ChatMsgBean chatMsgBean) {
                        if(isFinishing()){
                            return;
                        }
                        if (srl_loadmore.getState() == RefreshState.Refreshing) {
                            srl_loadmore.finishRefresh();
                        }
                        if (chatMsgBean.getList() != null) {
                            if (chatMsgBean.getList().size() == 0) {
                                srl_loadmore.setEnableRefresh(false);
                            }
                            mListBeans.addAll(0, chatMsgBean.getList());
                            if (TextUtils.isEmpty(mKyy)) {
                                if (!TextUtils.isEmpty(mGoods_name)) {
                                    ChatMsgBean.ListBean listBean = new ChatMsgBean.ListBean();
                                    listBean.setF_id(Long.valueOf(Login.Companion.getInstance().getUid()));
                                    listBean.setF_name(Login.Companion.getInstance().getUsername());
                                    listBean.setStore_id(mStoreId);
                                    listBean.setT_id(Long.valueOf(mChat_id));
                                    listBean.setAdd_time(Utils.getSignTime());
                                    listBean.setMsg_type(MsgBean.MSG_TYPE_GOODS_INFO);
                                    listBean.setIs_show_button(true);
                                    ChatMsgBean.ListBean.GoodsInfoBean goodsInfoBean = new ChatMsgBean.ListBean.GoodsInfoBean();
                                    goodsInfoBean.setGoods_id(mGoods_id + "");
                                    goodsInfoBean.setGoods_name(mGoods_name);
                                    goodsInfoBean.setIs_enquiry(is_enquiry ? 1 : 0);
                                    if (!is_enquiry) {
                                        goodsInfoBean.setGoods_promotion_price(mGoods_price);
                                    }
                                    goodsInfoBean.setPic(mGoods_img);
                                    listBean.setGoods_info(goodsInfoBean);
                                    listBean.setIs_show_button(true);
                                    listBean.setMsg_type(MsgBean.MSG_TYPE_GOODS_INFO);
                                    mListBeans.add(listBean);
                                }
                                mAdapter.notifyDataSetChanged();
                                lv_chat_content.setSelection(lv_chat_content.getBottom());
                            } else {
                                mAdapter.notifyDataSetChanged();
                                lv_chat_content.setSelection(chatMsgBean.getList().size());
                            }
                        }
                        mKyy = chatMsgBean.getKyy();
                    }
                }, false, mKyy);
    }

    @Override
    public void onBackPressed() {
        requestUpdateMsgRead();
        EventBus.getDefault().post(new ChatListUpdateUnreadEvent(mStoreId+""));
        super.onBackPressed();
    }

    private void requestUpdateMsgRead() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CHAT_UPDATE_MSG_READ())
                .addParam("f_id", mChat_id)
                .addParam("t_id", Login.Companion.getInstance().getUid())
                .addParam("store_id", mStoreId+"")
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(!is_from_chatlist){
                            Utils.getUnreadMsg();
                        }
                    }
                }, false);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        int position = ((AdapterView.AdapterContextMenuInfo)menuInfo).position;
        if(mAdapter.getItemViewType(position) == ChatContentAdapter.TYPE_SEND_TEXT || mAdapter.getItemViewType(position) == ChatContentAdapter.TYPE_RECEIVE_TEXT){
            menu.add(0, MENU_ITEM_COPY_ID, Menu.NONE, R.string.copy);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case MENU_ITEM_COPY_ID:
                int position = ((AdapterView.AdapterContextMenuInfo)item.getMenuInfo()).position;
                Utils.getCopy(getApplicationContext(),mListBeans.get(position).getT_msg());
                Toast.makeText(getApplicationContext(),R.string.copy_su,Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_FILE_CODE && resultCode == RESULT_OK && data!=null){
            String filePath = FileUtils.getPath(getApplicationContext(),data.getData());
            if(!TextUtils.isEmpty(filePath) && new File(filePath).length()<=5*1024*1024){
                requestUploadFile(filePath,"4");
            }else {
                Toast.makeText(getApplicationContext(),R.string.file_length_too_big,Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_REQUEST_FILE){
            if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
               selectFile();
            }else {
                Toast.makeText(getApplicationContext(), R.string.tip_permission_storage, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
