package com.xht.kuaiyouyi.ui.home.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.utils.ProportionscreenUtils;
import com.xht.kuaiyouyi.utils.Utils;

import butterknife.BindView;

public class CooperationCustomerActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.relative)
    LinearLayout linearLayout;
    @Override
    protected int getLayout() {
        return R.layout.activity_cooperation_customer;
    }

    @Override
    protected void initView() {
        tv_title.setText(getString(R.string.big_customers));
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CooperationCustomerActivity.this.finish();
            }
        });

        LinearLayout.LayoutParams layoutParams= (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        double wideth=Utils.getWindowWidth(CooperationCustomerActivity.this);
        double height= ProportionscreenUtils.getHeight(375,125,(int) wideth);
        layoutParams.height=(int) height;

    }
}
