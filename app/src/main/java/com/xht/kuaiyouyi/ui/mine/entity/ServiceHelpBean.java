package com.xht.kuaiyouyi.ui.mine.entity;

import java.util.List;

public class ServiceHelpBean {

    /**
     * class_list : [{"ac_id":"1","ac_name":"商城公告"},{"ac_id":"3","ac_name":"店主之家"},{"ac_id":"4","ac_name":"支付方式"},{"ac_id":"5","ac_name":"售后服务"},{"ac_id":"6","ac_name":"客服中心"},{"ac_id":"7","ac_name":"关于我们"}]
     * tel : 00853-28855263
     * store_member_id : 1
     */

    private String tel;
    private int store_member_id;
    private List<ClassListBean> class_list;

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getStore_member_id() {
        return store_member_id;
    }

    public void setStore_member_id(int store_member_id) {
        this.store_member_id = store_member_id;
    }

    public List<ClassListBean> getClass_list() {
        return class_list;
    }

    public void setClass_list(List<ClassListBean> class_list) {
        this.class_list = class_list;
    }

    public static class ClassListBean {
        /**
         * ac_id : 1
         * ac_name : 商城公告
         */

        private String ac_id;
        private String ac_name;

        public String getAc_id() {
            return ac_id;
        }

        public void setAc_id(String ac_id) {
            this.ac_id = ac_id;
        }

        public String getAc_name() {
            return ac_name;
        }

        public void setAc_name(String ac_name) {
            this.ac_name = ac_name;
        }
    }
}
