package com.xht.kuaiyouyi.ui.message.util;

import android.support.annotation.NonNull;

import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;

import org.jetbrains.annotations.NotNull;

public class MessageUtil {

    /**
     * 工作通知/验证消息 读取操作
     */
    public static void requestReadCompanyMessage(String company_message_id){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_READ_COMPANY_MSG())
                .addParam("company_message_id",company_message_id)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {

                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                    }
                }, false);
    }

    /**
     * 系统消息读取操作
     */
    public static void requestReadSystemMessage(String message_id){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_READ_SYSTEM_MSG())
                .addParam("message_id",message_id)
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {

                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                    }
                }, false);
    }


}
