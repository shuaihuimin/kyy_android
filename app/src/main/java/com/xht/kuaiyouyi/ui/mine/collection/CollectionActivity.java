package com.xht.kuaiyouyi.ui.mine.collection;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.ui.mine.adapter.FragmentAdapter;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.widget.BadgeView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class CollectionActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView backimg;
    @BindView(R.id.tl_tabs)
    TabLayout tabLayout;
    @BindView(R.id.vp_fragment)
    ViewPager viewPager;
    @BindView(R.id.iv_right)
    ImageView iv_right;

    private BadgeView mBadgeView;

    public final static int TAB_GOODS_INDEX = 0;
    public final static int TAB_STORE_INDEX = 1;
    private int type;


    private List<Fragment> fragments=new ArrayList<>();
    private int[] tabs=new int[]{R.string.goods,R.string.store};
    @Override
    protected int getLayout() {
        return R.layout.activity_collection;
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        mBadgeView = new BadgeView(this,iv_right,10,4);
        iv_right.setVisibility(View.VISIBLE);
        iv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(MessageActivity.class);
            }
        });
        backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CollectionActivity.this.finish();
            }
        });
        type=getIntent().getIntExtra(Contant.TYPE,0);
        showTablist(tabs);
        showUnreadMsgNum();
    }

    private void showUnreadMsgNum() {
        mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
    }

    private void showTablist(int[] tabs){
        for (int i = 0; i < tabs.length; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(tabs[i]));
            switch (i) {
                case TAB_GOODS_INDEX:
                    fragments.add(GoodsCollectionFragment.newInstance());
                    break;
                case TAB_STORE_INDEX:
                    fragments.add(StoreCollectionFragment.newInstance());
                    break;
                default:
                    fragments.add(GoodsCollectionFragment.newInstance());
                    break;
            }
        }
        viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager(), fragments));
        viewPager.setCurrentItem(type);//要设置到viewpager.setAdapter后才起作用
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setVerticalScrollbarPosition(type);
        //tlTabs.setupWithViewPager方法内部会remove所有的tabs，这里重新设置一遍tabs的text，否则tabs的text不显示
        for (int i = 0; i < tabs.length; i++) {
            tabLayout.getTabAt(i).setText(tabs[i]);
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event){
        if(event.getMessage().equals(MessageEvent.UNREAD_MSG_NUM)){
            showUnreadMsgNum();
        }
    }
}
