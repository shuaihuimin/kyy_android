package com.xht.kuaiyouyi.ui.enterprise.bean;

public class InviteBean {

    /**
     * invite : 1
     */

    private int invite;

    public int getInvite() {
        return invite;
    }

    public void setInvite(int invite) {
        this.invite = invite;
    }
}
