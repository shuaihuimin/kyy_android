package com.xht.kuaiyouyi.ui.cart.order;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.cart.entity.PayBean;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.utils.PayResult;
import com.xht.kuaiyouyi.utils.ToastU;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;

public class PayTreasureActivity extends BaseActivity {
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_amount_payable)
    TextView tv_amount_payable;

    private String orderInfo;
    private static final int SDK_PAY_FLAG = 1;
    public static final String ORDERSTR="orderstr";
    private AlertDialog mDialog;
    private PayBean payBean;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @SuppressWarnings("unused")
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    @SuppressWarnings("unchecked")
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                    /**
                     对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
                    Log.i("info","----------"+resultStatus+"---"+resultInfo);

                    // 判断resultStatus 为9000则代表支付成功
                    if (TextUtils.equals(resultStatus, "9000")) {
                        // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
                        payBean= com.alibaba.fastjson.JSONObject.parseObject(resultInfo,PayBean.class);
                        Toast.makeText(PayTreasureActivity.this, "支付成功", Toast.LENGTH_SHORT).show();
                        getPayresults();
                    } else {
                        // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
                        Toast.makeText(PayTreasureActivity.this, "支付失败", Toast.LENGTH_SHORT).show();
                    }
                    //btn_submit.setText(resultStatus+"--"+resultInfo);
                    break;
                }
                default:
                    break;
            }
        };
    };
    @Override
    protected int getLayout() {
        return R.layout.activity_paytreasure;
    }

    @Override
    protected void initView() {
        tv_title.setText(getString(R.string.checkstand));
        tv_amount_payable.setText(getIntent().getStringExtra("amount_total"));
        orderInfo=getIntent().getStringExtra(ORDERSTR);
        btn_submit.setText(getString(R.string.alipay_detils)+getIntent().getStringExtra("amount_total"));
        iv_back.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   showDialog();

               }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
                       Runnable payRunnable = new Runnable() {
                        @Override
                        public void run() {
                            PayTask alipay = new PayTask(PayTreasureActivity.this);
                            Map<String, String> result = alipay.payV2(orderInfo,true);
                            Message msg = new Message();
                            msg.what = SDK_PAY_FLAG;
                            msg.obj = result;
                            mHandler.sendMessage(msg);
                        }
                     };
                    // 必须异步调用
                       Thread payThread = new Thread(payRunnable);
                        payThread.start();
           }
       });
    }



    private void showDialog(){
        if(mDialog==null){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            mDialog = builder.create();
            View view = LayoutInflater.from(this).inflate(R.layout.dialog_checkstand_layout,null);
            Button bt_cancel = view.findViewById(R.id.bt_cancel);
            Button bt_confirm = view.findViewById(R.id.bt_confirm);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.setCancelable(true);
            bt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PayTreasureActivity.this.finish();
                    mDialog.dismiss();
                }
            });
            bt_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();
            //setContentView一定要放在show后面才行显示出页面
            mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            mDialog.setContentView(view);
        }else {
            mDialog.show();
        }

    }

    private void getPayresults(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_PAY_RESULT())
                .addParam("trade_no",payBean.getAlipay_trade_app_pay_response().getTrade_no())
                .addParam("out_trade_no",payBean.getAlipay_trade_app_pay_response().getOut_trade_no())
                .addParam("payment_code","alipay")
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        ToastU.INSTANCE.showToast(err);
                    }

                    @Override
                    public void onSuccess(@NonNull String data) {
                        if(data!=null){
                            JSONObject jsonObject= null;
                            try {
                                jsonObject = new JSONObject(data);
                                if(jsonObject.getBoolean("trade_status")){
                                    EventBus.getDefault().post(new MessageEvent(MessageEvent.ORDERDETILS));
                                    EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_ORDER_LIST));
                                    EventBus.getDefault().post(new MessageEvent(MessageEvent.REFRESH_APPROVAL_DETAIL));
                                    if(isFinishing()){
                                        return;
                                    }
                                    PayTreasureActivity.this.finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                },false);
    }

}
