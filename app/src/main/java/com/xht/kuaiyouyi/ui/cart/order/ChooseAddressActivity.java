package com.xht.kuaiyouyi.ui.cart.order;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.mine.adapter.CheckAddressListAdapter;
import com.xht.kuaiyouyi.ui.mine.adapter.ReceiveAddressListAdapter;
import com.xht.kuaiyouyi.ui.mine.address.NewAddressActivity;
import com.xht.kuaiyouyi.ui.mine.entity.CheckAddressListBean;
import com.xht.kuaiyouyi.ui.mine.entity.ReceiveAddressListBean;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class ChooseAddressActivity extends BaseActivity {
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;
    @BindView(R.id.rl_empty_approval_list)
    RelativeLayout rl_empty_approval_list;


    @BindView(R.id.ll_content)
    LinearLayout ll_content;
    @BindView(R.id.listview_chooseaddress)
    ListView lv_chooseaddress;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.bt_add_address)
    Button bt_add_address;

    private ReceiveAddressListBean mreceiveAddress;
    private CheckAddressListBean mcheckAddress;
    public final static int FLAG=1;
    public final static int REQUEST_CODE = 1000;
    //收货地址或支票地址
    private int mType;
    public static int TYPE_RECEIVE=1;
    public static int TYPE_CHECK=2;


    @Override
    protected int getLayout() {
        return R.layout.activity_chooseaddress;
    }

    @Override
    protected void initView() {
        mType=getIntent().getIntExtra("type",0);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseAddressActivity.this.finish();
            }
        });
        lv_chooseaddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mType==TYPE_RECEIVE){
                    Intent intent=new Intent(ChooseAddressActivity.this,ConfirmOrderActivity.class);
                    intent.putExtra("address",mreceiveAddress.getAddress().get(position).getAddress());
                    intent.putExtra("area_info",mreceiveAddress.getAddress().get(position).getArea_info());
                    intent.putExtra("name",mreceiveAddress.getAddress().get(position).getTrue_name());
                    intent.putExtra("address_id",mreceiveAddress.getAddress().get(position).getAddress_id());
                    intent.putExtra("mob_phone",mreceiveAddress.getAddress().get(position).getMob_phone());
                    intent.putExtra("area_id",mreceiveAddress.getAddress().get(position).getArea_id());
                    setResult(RESULT_OK,intent);
                }

                if(mType==TYPE_CHECK){
                    Intent intent=new Intent(ChooseAddressActivity.this,ChoicePaymentActivity.class);
                    intent.putExtra("address",mcheckAddress.getAddress().get(position).getAddress_check_address());
                    intent.putExtra("area_info",mcheckAddress.getAddress().get(position).getAddress_check_area_info());
                    intent.putExtra("name",mcheckAddress.getAddress().get(position).getAddress_check_true_name());
                    intent.putExtra("address_id",mcheckAddress.getAddress().get(position).getAddress_check_id());
                    intent.putExtra("mob_phone",mcheckAddress.getAddress().get(position).getAddress_check_phone());
                    setResult(RESULT_OK,intent);
                }
                ChooseAddressActivity.this.finish();
            }
        });
        bt_add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mType == TYPE_RECEIVE) {
                    Intent intent = new Intent(ChooseAddressActivity.this, NewAddressActivity.class);
                    intent.putExtra(Contant.TYPE, NewAddressActivity.TYPE_NEW_RECEIVE_ADDRESS);
                    startActivityForResult(intent, REQUEST_CODE);
                }
                if (mType == TYPE_CHECK) {
                    Intent intent = new Intent(ChooseAddressActivity.this, NewAddressActivity.class);
                    intent.putExtra(Contant.TYPE, NewAddressActivity.TYPE_NEW_CHECK_ADDRESS);
                    startActivityForResult(intent, REQUEST_CODE);
                }

            }
        });

        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getdata();
            }
        });

        getdata();
    }

    private void getdata(){
        DialogUtils.createTipAllLoadDialog(this);
        if(mType == TYPE_RECEIVE){
            tv_title.setText(getString(R.string.chooseaddress));
            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ADDRESS_LIST())
                    .withPOST(new NetCallBack<ReceiveAddressListBean>() {
                        @NotNull
                        @Override
                        public Class<ReceiveAddressListBean> getRealType() {
                            return ReceiveAddressListBean.class;
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(isFinishing()){
                                return;
                            }
                            DialogUtils.moven();
                            ll_content.setVisibility(View.GONE);
                            rl_empty_approval_list.setVisibility(View.GONE);
                            error_retry_view.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onSuccess(@NonNull ReceiveAddressListBean receiveAddressListBean) {
                            if(isFinishing()){
                                return;
                            }
                            DialogUtils.moven();
                            ll_content.setVisibility(View.VISIBLE);
                            error_retry_view.setVisibility(View.GONE);
                            mreceiveAddress=receiveAddressListBean;
                            ReceiveAddressListAdapter receiveAddressListAdapter = new ReceiveAddressListAdapter(ChooseAddressActivity.this, receiveAddressListBean,null, ChooseAddressActivity.this,FLAG);
                            lv_chooseaddress.setAdapter(receiveAddressListAdapter);
                            if(receiveAddressListBean.getAddress().size()==0){
                                rl_empty_approval_list.setVisibility(View.VISIBLE);
                            }
                        }
                    }, false);
        }
        if(mType == TYPE_CHECK){
            tv_title.setText(getString(R.string.checkaddress));
            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CHECK_ADDRESS_LIST())
                    .withPOST(new NetCallBack<CheckAddressListBean>() {
                        @NotNull
                        @Override
                        public Class<CheckAddressListBean> getRealType() {
                            return CheckAddressListBean.class;
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(isFinishing()){
                                return;
                            }
                            DialogUtils.moven();
                            ll_content.setVisibility(View.GONE);
                            error_retry_view.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onSuccess(@NonNull CheckAddressListBean checkAddressListBean) {
                            if(isFinishing()){
                                return;
                            }
                            DialogUtils.moven();
                            ll_content.setVisibility(View.VISIBLE);
                            error_retry_view.setVisibility(View.GONE);
                            mcheckAddress=checkAddressListBean;
                            CheckAddressListAdapter checkAddressListAdapter = new CheckAddressListAdapter(ChooseAddressActivity.this, checkAddressListBean,null,ChooseAddressActivity.this,FLAG);
                            lv_chooseaddress.setAdapter(checkAddressListAdapter);
                        }
                    }, false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            getdata();
        }
    }
}
