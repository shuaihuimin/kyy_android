package com.xht.kuaiyouyi.ui.cart.entity;

public class CurrencytypeBean {
    /**
     * CNY : {"name":"人民币","symbol":"￥","currency":"CNY","cackeKey":"","rate":1}
     * HKD : {"name":"港币","symbol":"HK$","currency":"HKD","cackeKey":"cacheCurrencyHKD","rate":1.138434}
     * MOP : {"name":"澳门币","symbol":"MOP$","currency":"MOP","cackeKey":"cacheCurrencyMOP","rate":1.170138}
     * USD : {"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.144987}
     */

    private CNYBean CNY;
    private HKDBean HKD;
    private MOPBean MOP;
    private USDBean USD;

    public CNYBean getCNY() {
        return CNY;
    }

    public void setCNY(CNYBean CNY) {
        this.CNY = CNY;
    }

    public HKDBean getHKD() {
        return HKD;
    }

    public void setHKD(HKDBean HKD) {
        this.HKD = HKD;
    }

    public MOPBean getMOP() {
        return MOP;
    }

    public void setMOP(MOPBean MOP) {
        this.MOP = MOP;
    }

    public USDBean getUSD() {
        return USD;
    }

    public void setUSD(USDBean USD) {
        this.USD = USD;
    }

    public static class CNYBean {
        /**
         * name : 人民币
         * symbol : ￥
         * currency : CNY
         * cackeKey :
         * rate : 1
         */

        private String name;
        private String symbol;
        private String currency;
        private String cackeKey;
        private int rate;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getCackeKey() {
            return cackeKey;
        }

        public void setCackeKey(String cackeKey) {
            this.cackeKey = cackeKey;
        }

        public int getRate() {
            return rate;
        }

        public void setRate(int rate) {
            this.rate = rate;
        }
    }

    public static class HKDBean {
        /**
         * name : 港币
         * symbol : HK$
         * currency : HKD
         * cackeKey : cacheCurrencyHKD
         * rate : 1.138434
         */

        private String name;
        private String symbol;
        private String currency;
        private String cackeKey;
        private double rate;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getCackeKey() {
            return cackeKey;
        }

        public void setCackeKey(String cackeKey) {
            this.cackeKey = cackeKey;
        }

        public double getRate() {
            return rate;
        }

        public void setRate(double rate) {
            this.rate = rate;
        }
    }

    public static class MOPBean {
        /**
         * name : 澳门币
         * symbol : MOP$
         * currency : MOP
         * cackeKey : cacheCurrencyMOP
         * rate : 1.170138
         */

        private String name;
        private String symbol;
        private String currency;
        private String cackeKey;
        private double rate;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getCackeKey() {
            return cackeKey;
        }

        public void setCackeKey(String cackeKey) {
            this.cackeKey = cackeKey;
        }

        public double getRate() {
            return rate;
        }

        public void setRate(double rate) {
            this.rate = rate;
        }
    }

    public static class USDBean {
        /**
         * name : 美元
         * symbol : US$
         * currency : USD
         * cackeKey : cacheCurrencyUSD
         * rate : 0.144987
         */

        private String name;
        private String symbol;
        private String currency;
        private String cackeKey;
        private double rate;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getCackeKey() {
            return cackeKey;
        }

        public void setCackeKey(String cackeKey) {
            this.cackeKey = cackeKey;
        }

        public double getRate() {
            return rate;
        }

        public void setRate(double rate) {
            this.rate = rate;
        }
    }
}