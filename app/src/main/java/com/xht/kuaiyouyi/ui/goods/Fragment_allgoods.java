package com.xht.kuaiyouyi.ui.goods;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.classic.common.MultipleStatusView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.adapter.StoreAdapter;
import com.xht.kuaiyouyi.ui.goods.entity.StoregoodsBean;
import com.xht.kuaiyouyi.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class Fragment_allgoods extends BaseFragment {
    @BindView(R.id.multipleStatusView)
    MultipleStatusView multipleStatusView;
    @BindView(R.id.mRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.recycler_store)
    RecyclerView recyclerView;
    @BindView(R.id.radiogroup)
    RadioGroup radioGroup;
    @BindView(R.id.iv_classification)
    ImageView iv_classification;
    @BindView(R.id.btn_price)
    RadioButton btn_price;
    @BindView(R.id.linear)
    LinearLayout linearLayout;
    private StoreAdapter adapter;
    private List<StoregoodsBean.ListBean> list=new ArrayList<>();
    private List<StoregoodsBean.CurrencytypeBean> currlist=new ArrayList<>();
    private String store_id;
    private String kyy="";
    private boolean isrefresh;
    private boolean orderflag=false;
    private String key="4";
    private String order="0";
    private int flag=1;
    private int img_height;
    private int state=0;
    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(list.size()==0){
                multipleStatusView.showEmpty();
            }
        }
    };

    public static Fragment_allgoods newInstance(String storeid){
        Bundle args = new Bundle();
        args.putString("store_id",storeid);
        Fragment_allgoods fragment = new Fragment_allgoods();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        store_id=getArguments().getString("store_id");
        img_height=(Utils.getWindowWidth(getActivity())-8)/2;
        adapter=new StoreAdapter(getActivity(),R.layout.item_brand_detils_recycler,list,currlist,0,img_height);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(state!=0){
                    if (dy < 0) {
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.STORE_DOWN));
                    } else if (dy > 0) {
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.STORE_UP));
                    }
                }else {
                    state=1;
                    EventBus.getDefault().post(new MessageEvent(MessageEvent.STORE_DOWN));
                }
            }
        });
        linearLayout.setVisibility(View.VISIBLE);
        getinit(store_id,key,order);
        initload();
        click();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_newstores_layout;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.i("info","--------zlm2");
        EventBus.getDefault().post(new MessageEvent(MessageEvent.STORE_DOWN));
    }

    private void getinit(String store_id, final String key, String order){
        if(kyy.equals("")){
            multipleStatusView.showLoading();
        }
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_STORE_ALLGOODS())
                .addParam("store_id",store_id)
                .addParam("store_type","2")
                .addParam("order",order)
                .addParam("key",key)
                .withLoadPOST(new NetCallBack<StoregoodsBean>() {

                    @NotNull
                    @Override
                    public Class<StoregoodsBean> getRealType() {
                        return StoregoodsBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(TextUtils.isEmpty(kyy)){
                            multipleStatusView.showError();
                        }else {
                            Toast.makeText(getContext(), getString(R.string.not_network), Toast.LENGTH_SHORT).show();
                        }

                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull StoregoodsBean storegoodsBean) {
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                        multipleStatusView.showContent();
                        currlist.add(storegoodsBean.getCurrencytype());
                        if(storegoodsBean.getList()!=null && storegoodsBean.getList().size()!=0){
                            for(int i=0;i<storegoodsBean.getList().size();i++){
                                list.add(storegoodsBean.getList().get(i));
                            }
                        }else {
                            if(!TextUtils.isEmpty(kyy)){
                                smartRefreshLayout.finishLoadMoreWithNoMoreData();
                                //smartRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
                            }
                        }
                        kyy=storegoodsBean.getKyy();
                        adapter.notifyDataSetChanged();
                        handler.sendEmptyMessage(0);
                    }
                },false,kyy);
    }

    private void initload(){
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                smartRefreshLayout.setNoMoreData(false) ;
                getinit(store_id,key,order);
            }
        });

    }

    private void click(){
        multipleStatusView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getinit(store_id,key,order);
            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.btn_recommended:
                        if(orderflag){
                            orderflag=false;
                        }else {
                            orderflag=true;
                        }
                        kyy="";
                        key="4";
                        order="0";
                        list.clear();
                        getinit(store_id,key,order);
                        btn_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_price_nor),null);
                        break;
                    case R.id.btn_sales:
                        if(orderflag){
                            orderflag=false;
                        }else {
                            orderflag=true;
                        }
                        kyy="";
                        key="1";
                        order="0";
                        list.clear();
                        getinit(store_id,key,order);
                        btn_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_price_nor),null);
                        break;
                    case R.id.btn_newproduct:
                        if(orderflag){
                            orderflag=false;
                        }else {
                            orderflag=true;
                        }
                        kyy="";
                        key="2";
                        order="0";
                        list.clear();
                        getinit(store_id,key,order);
                        btn_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_price_nor),null);
                        break;
//                    case R.id.btn_price:
//                        kyy="";
//                        key="3";
//                        list.clear();
//                        order = "0";
//                        btn_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.shop_icon_priceup_sel),null);
//                        getinit(store_id,key,order);
//                        break;

                }
            }
        });

        btn_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kyy="";
                key="3";
                list.clear();
                if(orderflag) {
                    orderflag = false;
                    order = "0";
                    btn_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.shop_icon_priceup_sel),null);
                }else{
                    orderflag=true;
                    order="1";
                    btn_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.shop_icon_pricedown_sel),null);
                }
                getinit(store_id,key,order);
            }
        });
        iv_classification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag==0){
                    flag=1;
                    iv_classification.setImageResource(R.mipmap.search_icon_palace_nor);
                    adapter=new StoreAdapter(getActivity(),R.layout.item_brand_detils_recycler,list,currlist,0,img_height);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setAdapter(adapter);


                }else {
                    flag=0;
                    iv_classification.setImageResource(R.mipmap.search_icon_list_nor);
                    adapter=new StoreAdapter(getActivity(),R.layout.home_recycler_item_layout,list,currlist,1,img_height);
                    GridLayoutManager manager = new GridLayoutManager(getActivity(), 2,GridLayoutManager.VERTICAL,false);
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setAdapter(adapter);
                }
            }
        });
    }

}
