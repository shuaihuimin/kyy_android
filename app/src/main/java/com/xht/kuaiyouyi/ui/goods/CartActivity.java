package com.xht.kuaiyouyi.ui.goods;


import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.cart.CartFragment;
import com.xht.kuaiyouyi.ui.event.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class CartActivity extends BaseActivity {
    CartFragment fragment_cart;
    public final static String TO_HOME_PAGE = "to_home_page";

    @Override
    protected int getLayout() {
        return R.layout.activity_cart;
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        fragment_cart = (CartFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_cart);
        fragment_cart.setIv_leftVisiable();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.ORDERDETILS)){
            CartActivity.this.finish();
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
