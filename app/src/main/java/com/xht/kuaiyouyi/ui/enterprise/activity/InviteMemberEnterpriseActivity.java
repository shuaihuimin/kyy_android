package com.xht.kuaiyouyi.ui.enterprise.activity;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.enterprise.Constant;
import com.xht.kuaiyouyi.ui.enterprise.bean.InviteBean;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class InviteMemberEnterpriseActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.cet_enterprise_submit)
    EditText cet_enterprise_submit;
    @BindView(R.id.bt_enterprise_submit)
    Button bt_enterprise_submit;

    private String mCompanyId;

    @Override
    protected int getLayout() {
        return R.layout.activity_invite_member_enterprise;
    }

    @Override
    protected void initView() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_title.setText(R.string.enterprise_invite_member);
        mCompanyId = getIntent().getStringExtra(Constant.COMPANY_ID);
        bt_enterprise_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String keyword = cet_enterprise_submit.getText().toString().trim();
                if(!TextUtils.isEmpty(keyword)){
                    requestInvite(mCompanyId,keyword);
                }
            }
        });
        cet_enterprise_submit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0){
                    bt_enterprise_submit.setEnabled(true);
                }else {
                    bt_enterprise_submit.setEnabled(false);
                }
            }
        });
    }

    private void requestInvite(String companyId, String keyword) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_INVITE())
                .addParam("company_id",companyId)
                .addParam("keyword",keyword)
                .withPOST(new NetCallBack<InviteBean>() {
                    @NotNull
                    @Override
                    public Class<InviteBean> getRealType() {
                        return InviteBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.createTipImageAndTextDialog(
                                InviteMemberEnterpriseActivity.this, err,
                                R.mipmap.png_icon_popup_warning);
                    }

                    @Override
                    public void onSuccess(@NonNull InviteBean inviteBean) {
                        if(isFinishing()){
                            return;
                        }
                        if(inviteBean.getInvite() == 1){
                            DialogUtils.createTipImageAndTextDialog(
                                    InviteMemberEnterpriseActivity.this,
                                    getResources().getString(R.string.enterprise_send_successful),
                                    R.mipmap.png_icon_popup_ok);
                            tv_title.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            },1000);
                        }
                    }
                },false);
    }
}
