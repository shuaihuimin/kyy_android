package com.xht.kuaiyouyi.ui.mine.order;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.cart.order.OrderDetilsActivity;
import com.xht.kuaiyouyi.ui.mine.adapter.OrderListAdapter;
import com.xht.kuaiyouyi.ui.mine.collection.OnShowEmptyView;
import com.xht.kuaiyouyi.ui.mine.entity.OrderListBean;
import com.xht.kuaiyouyi.utils.Login;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class OrderListfragment extends BaseFragment {
    @BindView(R.id.srl_loadmore)
    SmartRefreshLayout srl_loadmore;
    @BindView(R.id.lv_content)
    ListView lv_content;
    @BindView(R.id.rl_empty_order_list)
    RelativeLayout rl_empty_order_list;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;

    private int mStateType;
    private int mOrderType;

    public static final int TYPE_MINE_ORDER = 1;
    public static final int TYPE_ENTERPRISE_ORDER = 2;
    public String mKyy = "";

    private OrderListBean mData;
    private OrderListAdapter mAdapter;


    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mKyy = "";
        requestOrderList();
        srl_loadmore.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                requestOrderList();
            }
        });
        lv_content.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString("order_main_id"
                        ,mData.getOrder_main_list().get(position).getOrder_main_id());
                goToActivity(OrderDetilsActivity.class,bundle);
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestOrderList();
            }
        });
    }

    public void requestOrderList() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ORDER_LIST())
                .addParam("state_type", mStateType + "")
                .addParam("type", mOrderType + "")
                .addParam("company_id", mOrderType==TYPE_ENTERPRISE_ORDER? Login.Companion.getInstance().getEnterprise_company_id():"")
                .withLoadPOST(new NetCallBack<OrderListBean>() {
                    @NotNull
                    @Override
                    public Class<OrderListBean> getRealType() {
                        return OrderListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {

                        if(getContext()!=null){
                            if(TextUtils.isEmpty(mKyy)){
                                rl_empty_order_list.setVisibility(View.GONE);
                                error_retry_view.setVisibility(View.VISIBLE);
                            }else {
                                Toast.makeText(getContext(), getString(R.string.not_network), Toast.LENGTH_SHORT).show();
                            }
                            if(srl_loadmore.getState() == RefreshState.Loading){
                                srl_loadmore.finishLoadMore();
                            }
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull OrderListBean orderListBean) {
                        if(getActivity() == null){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        if(TextUtils.isEmpty(mKyy)){
                            srl_loadmore.setNoMoreData(false);
                            if(orderListBean.getOrder_main_list()==null||orderListBean.getOrder_main_list().size()==0){
                                rl_empty_order_list.setVisibility(View.VISIBLE);
                                return;
                            }
                            if(orderListBean.getOrder_main_list()!=null){
                                mData = orderListBean;
                                mAdapter = new OrderListAdapter(getContext(),mData,mStateType==MineOrderActivity.TAB_ALL_ORDER);
                                mAdapter.setShowEmptyListener(new OnShowEmptyView() {
                                    @Override
                                    public void onShowEmptyView() {
                                        rl_empty_order_list.setVisibility(View.VISIBLE);
                                    }
                                });
                                lv_content.setAdapter(mAdapter);
                                srl_loadmore.setEnableLoadMore(true);
                                mKyy = orderListBean.getKyy();
                            }
                        }else {
                            srl_loadmore.finishLoadMore();
                            if(orderListBean.getOrder_main_list()!=null){
                                if(orderListBean.getOrder_main_list().size()>0){
                                    mData.getOrder_main_list().addAll(orderListBean.getOrder_main_list());
                                    mAdapter.notifyDataSetChanged();
                                    mKyy=orderListBean.getKyy();
                                }else {
                                    srl_loadmore.setNoMoreData(true);
                                }
                            }
                        }
                    }
                }, false, mKyy);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_oderlist;
    }

    /**
     *
     * @param stateType  订单的状态
     * @param orderType  我的订单或者企业订单
     */
    public void setType(int stateType, int orderType) {
        this.mStateType = stateType;
        this.mOrderType = orderType;
    }

}
