package com.xht.kuaiyouyi.ui.login.activity;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.ui.login.LoginUtil;
import com.xht.kuaiyouyi.ui.login.entity.LoginBean;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/**
 * Created by shuaihuimin on 2018/6/25.
 * 注册时设置密码
 * 找回密码
 * 修改密码
 */

public class SetPasswordActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView imageback;
    @BindView(R.id.tv_title)
    TextView titleview;
    @BindView(R.id.cet_password_one)
    EditText cet_password_one;
    @BindView(R.id.cet_password_two)
    EditText cet_password_two;
    @BindView(R.id.btn_submit)
    Button submitbtn;
    @BindView(R.id.iv_see_password_one)
    ImageView iv_see_password_one;
    @BindView(R.id.iv_see_password_two)
    ImageView iv_see_password_two;
    private String mPhone;
    private String mVerifyCode;
    private String mPassword;
    private String mPasswordAgain;
    private String mType;
    private String mAreaCode;


    @Override
    protected int getLayout() {
        return R.layout.changepsd_layout;
    }

    @Override
    protected void initView() {
        cet_password_one.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
        cet_password_two.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
        mType =getIntent().getStringExtra(Contant.TYPE);
        switch (mType) {
            case Contant.TYPE_REGISTER:
                mPhone =getIntent().getStringExtra(Contant.PHONE);
                mVerifyCode =getIntent().getStringExtra(Contant.VERIFY_CODE);
                mAreaCode =getIntent().getStringExtra(Contant.AREA_CODE);
                titleview.setText(R.string.set_password);
                break;
            case Contant.TYPE_RESET_PASSWORD:
                mPhone =getIntent().getStringExtra(Contant.PHONE);
                mVerifyCode =getIntent().getStringExtra(Contant.VERIFY_CODE);
                titleview.setText(R.string.find_password);
                break;
            case Contant.TYPE_CHANGE_PASSWORD:
                titleview.setText(R.string.change_login_password);
                break;
        }


        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        iv_see_password_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cet_password_two.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    //设置密码不可见
                    cet_password_two.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                    iv_see_password_two.setImageResource(R.mipmap.login_icon_closedeyes_nor);
                }else {
                    //设置密码可见
                    cet_password_two.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    iv_see_password_two.setImageResource(R.mipmap.login_icon_eye_nor);
                }
                cet_password_two.setSelection(cet_password_two.getText().length());
            }
        });
        iv_see_password_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cet_password_one.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    //设置密码不可见
                    cet_password_one.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                    iv_see_password_one.setImageResource(R.mipmap.login_icon_closedeyes_nor);
                }else {
                    //设置密码可见
                    cet_password_one.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    iv_see_password_one.setImageResource(R.mipmap.login_icon_eye_nor);
                }
                cet_password_one.setSelection(cet_password_two.getText().length());

            }
        });

        submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPassword=cet_password_one.getText().toString().trim();
                mPasswordAgain=cet_password_two.getText().toString().trim();
                if(TextUtils.isEmpty(mPassword) || TextUtils.isEmpty(mPasswordAgain)){
                    Toast.makeText(SetPasswordActivity.this,R.string.password_empty_tip,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!mPasswordAgain.equals(mPassword)){
                    Toast.makeText(SetPasswordActivity.this,R.string.passwords_differnce,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                submitPwd();
            }
        });

        cet_password_one.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setBtnEnable();
            }
        });
        cet_password_two.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setBtnEnable();
            }
        });
    }

    //提交密码
    private void submitPwd(){
        if(mType.equals(Contant.TYPE_REGISTER)){//注册设置密码
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGIN())
                .addParam("username", mPhone)
                .addParam("password", mPassword)
                .addParam("area_code",mAreaCode)
                .addParam("client", "android")
                .addParam("key_code", mVerifyCode)
                .addParam("type","3")
                .addParam("l_l_p_type", Utils.isHUAWEI()?"1":"")
                .addParam("l_l_p_token", Login.Companion.getInstance().getHuawei_push_token())
                .withPOST(new NetCallBack<LoginBean>() {

                    @NotNull
                    @Override
                    public Class<LoginBean> getRealType() {
                        return LoginBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(SetPasswordActivity.this,err,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull LoginBean loginBean) {
                        if(isFinishing()){
                            return;
                        }
                        LoginUtil.loginSaveDataAndInit(getApplicationContext(),loginBean,mPhone);
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.EVENT_ClOSE_LOGINACTIVITY));
                        goToActivity(RegisteredsufActivity.class);
                        finish();
                    }
                },false);
        }else if(mType.equals(Contant.TYPE_RESET_PASSWORD)){//忘记密码
            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_FIND_PASSWORD())
                    .addParam("phone",mPhone)
                    .addParam("captcha",mVerifyCode)
                    .addParam("client","android")
                    .addParam("password",mPassword)
                    .addParam("password_confirm",mPassword)
                    .withPOST(new NetCallBack<String>() {

                        @Override
                        public void onSuccess(@NonNull String s) {
                            if(isFinishing()){
                                return;
                            }
                            DialogUtils.createTipImageAndTextDialog(SetPasswordActivity.this,getString(R.string.reset_password_successful),R.mipmap.png_icon_popup_ok);
                            iv_see_password_one.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            },1000);
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(isFinishing()){
                                return;
                            }
                            Toast.makeText(SetPasswordActivity.this,err,
                                    Toast.LENGTH_SHORT).show();
                        }

                        @NotNull
                        @Override
                        public Class<String> getRealType() {
                            return String.class;
                        }
                    },false);
        }else if(mType.equals(Contant.TYPE_CHANGE_PASSWORD)){//修改密码
            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_MODIFY_PASSWORD())
                    .addParam("password",mPassword)
                    .addParam("password_confirm",mPassword)
                    .addParam("type","1")
                    .addParam("status", "0")
                    .withPOST(new NetCallBack<String>() {

                        @NotNull
                        @Override
                        public Class<String> getRealType() {
                            return String.class;
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(isFinishing()){
                                return;
                            }
                            Toast.makeText(SetPasswordActivity.this,
                                    err,Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onSuccess(@NonNull String o) {
                            if(isFinishing()){
                                return;
                            }
                            DialogUtils.createTipImageAndTextDialog(SetPasswordActivity.this,getString(R.string.change_password_successful),R.mipmap.png_icon_popup_ok);
                            iv_see_password_one.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            },1000);
                        }
                    },false);

        }
    }

    private void setBtnEnable(){
        if(cet_password_one.getText().toString().trim().length()>0
                &&cet_password_two.getText().toString().trim().length()>0){
            submitbtn.setEnabled(true);
        }else {
            submitbtn.setEnabled(false);
        }
    }


}
