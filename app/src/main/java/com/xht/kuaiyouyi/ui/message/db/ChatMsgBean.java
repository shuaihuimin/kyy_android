package com.xht.kuaiyouyi.ui.message.db;

import com.xht.kuaiyouyi.ui.message.bean.MsgBean;

import java.util.List;

/**
 * 聊天内容表
 */
public class ChatMsgBean {


    /**
     * list : [{"m_id":"2159","f_id":"28","f_name":"runmin","f_ip":"192.168.0.184","t_id":"1","t_name":"xht_kyy","t_msg":"http://192.168.0.2/shop/index.php?act=goods&op=index&goods_id=100362","add_time":"1539655585","store_id":"1","msg_type":"1","goods_info":{"goods_id ":101356,"goods_name ":"cc ","goods_promotion_price ":"10.00 ","is_enquiry ":0,"pic ":"http: //www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05824026729561921_1280.jpg","store_id":1},"time":"2018-10-16 10:06:25"}]
     * kyy : eyJQIjoxLCJLIjoia3l5XzAxMCJ9
     */

    private String kyy;
    private List<ListBean> list;

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * m_id : 2159
         * f_id : 28
         * f_name : runmin
         * f_ip : 192.168.0.184
         * t_id : 1
         * t_name : xht_kyy
         * t_msg : http://192.168.0.2/shop/index.php?act=goods&op=index&goods_id=100362
         * add_time : 1539655585
         * store_id : 1
         * msg_type : 1
         * goods_info : {"goods_id ":101356,"goods_name ":"cc ","goods_promotion_price ":"10.00 ","is_enquiry ":0,"pic ":"http: //www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05824026729561921_1280.jpg","store_id":1}
         * time : 2018-10-16 10:06:25
         */

        private long m_id;
        private long f_id;
        private String f_name;
        private String f_ip;
        private long t_id;
        private String t_name;
        private String t_msg;
        private String add_time;
        private long store_id;
        private int msg_type;
        private GoodsInfoBean goods_info;
        private MsgBean.FileBean file;
        private String time;

        //图片需要的字段
        private int imageWidth;
        private int imageHeight;
        private String localImgPath;//本地图片的路径

        private boolean is_show_button;//带Button的商品信息

        public long getM_id() {
            return m_id;
        }

        public void setM_id(long m_id) {
            this.m_id = m_id;
        }

        public long getF_id() {
            return f_id;
        }

        public void setF_id(long f_id) {
            this.f_id = f_id;
        }

        public String getF_name() {
            return f_name;
        }

        public void setF_name(String f_name) {
            this.f_name = f_name;
        }

        public String getF_ip() {
            return f_ip;
        }

        public void setF_ip(String f_ip) {
            this.f_ip = f_ip;
        }

        public long getT_id() {
            return t_id;
        }

        public void setT_id(long t_id) {
            this.t_id = t_id;
        }

        public String getT_name() {
            return t_name;
        }

        public void setT_name(String t_name) {
            this.t_name = t_name;
        }

        public String getT_msg() {
            return t_msg;
        }

        public void setT_msg(String t_msg) {
            this.t_msg = t_msg;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public long getStore_id() {
            return store_id;
        }

        public void setStore_id(long store_id) {
            this.store_id = store_id;
        }

        public int getMsg_type() {
            return msg_type;
        }

        public void setMsg_type(int msg_type) {
            this.msg_type = msg_type;
        }

        public GoodsInfoBean getGoods_info() {
            return goods_info;
        }

        public void setGoods_info(GoodsInfoBean goods_info) {
            this.goods_info = goods_info;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public static class GoodsInfoBean {
            /**
             * goods_id  : 101356
             * goods_name  : cc
             * goods_promotion_price  : 10.00
             * is_enquiry  : 0
             * pic  : http: //www.dev.kuaiyouyi.com/data/upload/shop/store/goods/1/1_05824026729561921_1280.jpg
             * store_id : 1
             */

            private String goods_id;
            private String goods_name;
            private String goods_promotion_price;
            private int is_enquiry;
            private String pic;
            private String store_id;

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getGoods_promotion_price() {
                return goods_promotion_price;
            }

            public void setGoods_promotion_price(String goods_promotion_price) {
                this.goods_promotion_price = goods_promotion_price;
            }

            public int getIs_enquiry() {
                return is_enquiry;
            }

            public void setIs_enquiry(int is_enquiry) {
                this.is_enquiry = is_enquiry;
            }

            public String getPic() {
                return pic;
            }

            public void setPic(String pic) {
                this.pic = pic;
            }

            public String getStore_id() {
                return store_id;
            }

            public void setStore_id(String store_id) {
                this.store_id = store_id;
            }
        }

        public static class FileBean{

            /**
             * file_name : 2.mp4
             * file_size : 1744303
             * file_type : mp4
             * file_url : #
             */

            private String file_name;
            private long file_size;
            private String file_type;
            private String file_url;

            public String getFile_name() {
                return file_name;
            }

            public void setFile_name(String file_name) {
                this.file_name = file_name;
            }

            public long getFile_size() {
                return file_size;
            }

            public void setFile_size(long file_size) {
                this.file_size = file_size;
            }

            public String getFile_type() {
                return file_type;
            }

            public void setFile_type(String file_type) {
                this.file_type = file_type;
            }

            public String getFile_url() {
                return file_url;
            }

            public void setFile_url(String file_url) {
                this.file_url = file_url;
            }
        }

        public MsgBean.FileBean getFile() {
            return file;
        }

        public void setFile(MsgBean.FileBean file) {
            this.file = file;
        }

        public int getImageWidth() {
            return imageWidth;
        }

        public void setImageWidth(int imageWidth) {
            this.imageWidth = imageWidth;
        }

        public int getImageHeight() {
            return imageHeight;
        }

        public void setImageHeight(int imageHeight) {
            this.imageHeight = imageHeight;
        }

        public String getLocalImgPath() {
            return localImgPath;
        }

        public void setLocalImgPath(String localImgPath) {
            this.localImgPath = localImgPath;
        }

        public boolean isIs_show_button() {
            return is_show_button;
        }

        public void setIs_show_button(boolean is_show_button) {
            this.is_show_button = is_show_button;
        }
    }
}
