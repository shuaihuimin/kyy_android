package com.xht.kuaiyouyi.ui.message.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.Space;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.BuildConfig;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.message.FaceManager;
import com.xht.kuaiyouyi.ui.message.activity.FilePreviewActivity;
import com.xht.kuaiyouyi.ui.message.activity.PhotoViewActivity;
import com.xht.kuaiyouyi.ui.message.bean.MsgBean;
import com.xht.kuaiyouyi.ui.message.db.ChatMsgBean;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class ChatContentAdapter extends BaseAdapter {
    private Context mContext;
    private List<ChatMsgBean.ListBean> mListBeans;
    private String mStore_avatar;
    public static final int TYPE_SEND_TEXT = 0;
    public static final int TYPE_SEND_IMG = 1;
    public static final int TYPE_RECEIVE_TEXT = 2;
    public static final int TYPE_RECEIVE_IMG = 3;
    public static final int TYPE_GOODS_INFO_WITH_BUTTON = 4;
    public static final int TYPE_GOODS_INFO = 5;
    public static final int TYPE_BLANK = 6;//空白，一些历史数据不需要显示
    public static final int TYPE_SEND_FILE = 7;
    public static final int TYPE_RECEIVE_FILE = 8;

    //缓存图片
    private RequestOptions mGlideOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL);

    private OnSendGoodsInfoClickListener mListener;

    public ChatContentAdapter(Context context, List<ChatMsgBean.ListBean> listBeans, String store_avatar) {
        this.mContext = context;
        this.mListBeans = listBeans;
        this.mStore_avatar = store_avatar;
    }

    @Override
    public int getCount() {
        if (mListBeans == null) {
            return 0;
        }
        return mListBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return mListBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        if (mListBeans.get(position).getF_id() == Long.valueOf(Login.Companion.getInstance().getUid())) {
            if (mListBeans.get(position).getMsg_type() == MsgBean.MSG_TYPE_IMG) {
                return TYPE_SEND_IMG;
            } else if (mListBeans.get(position).getMsg_type() == MsgBean.MSG_TYPE_GOODS_INFO) {
                if(mListBeans.get(position).getGoods_info()==null){
                    return TYPE_BLANK;
                }
                if (mListBeans.get(position).isIs_show_button()) {
                    return TYPE_GOODS_INFO_WITH_BUTTON;
                } else {
                    return TYPE_GOODS_INFO;
                }
            } else if (mListBeans.get(position).getMsg_type() == MsgBean.MSG_TYPE_FILE && mListBeans.get(position).getFile()!=null) {
                return TYPE_SEND_FILE;
            }else {
                return TYPE_SEND_TEXT;
            }
        } else {
            if (mListBeans.get(position).getMsg_type() == MsgBean.MSG_TYPE_IMG) {
                return TYPE_RECEIVE_IMG;
            } else if (mListBeans.get(position).getMsg_type() == MsgBean.MSG_TYPE_FILE&& mListBeans.get(position).getFile()!=null) {
                return TYPE_RECEIVE_FILE;
            }else {
                return TYPE_RECEIVE_TEXT;
            }
        }

    }

    @Override
    public int getViewTypeCount() {
        return 9;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        int itemType = getItemViewType(position);
        if (itemType == TYPE_SEND_TEXT) {
            SendTextViewHolder sendTextViewHolder;
            if (convertView == null) {
                sendTextViewHolder = new SendTextViewHolder();
                convertView = View.inflate(mContext, R.layout.item_chat_send_text, null);
                sendTextViewHolder.tv_time = convertView.findViewById(R.id.tv_time);
                sendTextViewHolder.iv_avatar = convertView.findViewById(R.id.iv_avatar);
                sendTextViewHolder.tv_msg = convertView.findViewById(R.id.tv_msg);
                convertView.setTag(sendTextViewHolder);
            } else {
                sendTextViewHolder = (SendTextViewHolder) convertView.getTag();
            }
            //数据填充
            SpannableString msg = FaceManager.getInstance().getEmotionContent(mContext, mListBeans.get(position).getT_msg());
            sendTextViewHolder.tv_msg.setText(msg);
            setTimeView(position, sendTextViewHolder.tv_time);
            setAvatar(Login.Companion.getInstance().getMemberAvatar(),sendTextViewHolder.iv_avatar);
        }

        if (itemType == TYPE_RECEIVE_TEXT) {
            ReceiveTextViewHolder receiveTextViewHolder;
            if (convertView == null) {
                receiveTextViewHolder = new ReceiveTextViewHolder();
                convertView = View.inflate(mContext, R.layout.item_chat_receive_text, null);
                receiveTextViewHolder.tv_time = convertView.findViewById(R.id.tv_time);
                receiveTextViewHolder.iv_avatar = convertView.findViewById(R.id.iv_avatar);
                receiveTextViewHolder.tv_msg = convertView.findViewById(R.id.tv_msg);
                convertView.setTag(receiveTextViewHolder);
            } else {
                receiveTextViewHolder = (ReceiveTextViewHolder) convertView.getTag();
            }
            //数据填充
            final SpannableString msg = FaceManager.getInstance().getEmotionContent(mContext, mListBeans.get(position).getT_msg());
            Pattern pattern = Pattern.compile(BuildConfig.GOODS_URL_REGEX);
            Matcher matcher = pattern.matcher(mListBeans.get(position).getT_msg());
            int startPosition = 0;
            while (matcher.find(startPosition)) {
                final String url = matcher.group();
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void updateDrawState(TextPaint ds) {
                        ds.setColor(ContextCompat.getColor(mContext, R.color.blue_normal));
                        ds.setUnderlineText(false);
                    }

                    @Override
                    public void onClick(View widget) {
                        Uri uri = Uri.parse(url);
                        String goods_id = uri.getQueryParameter("goods_id");
                        if (!TextUtils.isEmpty(goods_id)) {
                            toGoodsActivity(goods_id);
                        }
                    }
                };
                msg.setSpan(clickableSpan, matcher.end() - url.length(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                startPosition = matcher.end();
            }
            receiveTextViewHolder.tv_msg.setText(msg);
            receiveTextViewHolder.tv_msg.setMovementMethod(LinkMovementMethod.getInstance());

            setTimeView(position, receiveTextViewHolder.tv_time);
            setAvatar(mStore_avatar,receiveTextViewHolder.iv_avatar);
        }
        if (itemType == TYPE_RECEIVE_IMG) {
            ReceiveImgViewHolder receiveImgViewHolder;
            if (convertView == null) {
                receiveImgViewHolder = new ReceiveImgViewHolder();
                convertView = View.inflate(mContext, R.layout.item_chat_receive_img, null);
                receiveImgViewHolder.tv_time = convertView.findViewById(R.id.tv_time);
                receiveImgViewHolder.iv_avatar = convertView.findViewById(R.id.iv_avatar);
                receiveImgViewHolder.iv_img = convertView.findViewById(R.id.iv_img);
                convertView.setTag(receiveImgViewHolder);
            } else {
                receiveImgViewHolder = (ReceiveImgViewHolder) convertView.getTag();
            }
            //数据填充
            setAvatar(mStore_avatar,receiveImgViewHolder.iv_avatar);
            setImg(position, receiveImgViewHolder.iv_img);
            setTimeView(position, receiveImgViewHolder.tv_time);
            receiveImgViewHolder.iv_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toPhotoViewActivity(v, position);
                }
            });
        }

        if (itemType == TYPE_SEND_IMG) {
            SendImgViewHolder sendImgViewHolder;
            if (convertView == null) {
                sendImgViewHolder = new SendImgViewHolder();
                convertView = View.inflate(mContext, R.layout.item_chat_send_img, null);
                sendImgViewHolder.tv_time = convertView.findViewById(R.id.tv_time);
                sendImgViewHolder.iv_avatar = convertView.findViewById(R.id.iv_avatar);
                sendImgViewHolder.iv_img = convertView.findViewById(R.id.iv_img);
                convertView.setTag(sendImgViewHolder);
            } else {
                sendImgViewHolder = (SendImgViewHolder) convertView.getTag();
            }
            //数据填充
            setAvatar(Login.Companion.getInstance().getMemberAvatar(),sendImgViewHolder.iv_avatar);
            setImg(position, sendImgViewHolder.iv_img);
            setTimeView(position, sendImgViewHolder.tv_time);
            sendImgViewHolder.iv_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toPhotoViewActivity(v, position);
                }
            });
        }

        if (itemType == TYPE_GOODS_INFO_WITH_BUTTON) {
            ButtonGoodsInfoViewHolder buttonGoodsInfoViewHolder;
            if (convertView == null) {
                buttonGoodsInfoViewHolder = new ButtonGoodsInfoViewHolder();
                convertView = View.inflate(mContext, R.layout.item_chat_goods_info_with_button, null);
                buttonGoodsInfoViewHolder.tv_goods_name = convertView.findViewById(R.id.tv_goods_name);
                buttonGoodsInfoViewHolder.tv_goods_price = convertView.findViewById(R.id.tv_goods_price);
                buttonGoodsInfoViewHolder.tv_send_goods_info = convertView.findViewById(R.id.tv_send_goods_info);
                buttonGoodsInfoViewHolder.iv_goods_img = convertView.findViewById(R.id.iv_goods_img);
                convertView.setTag(buttonGoodsInfoViewHolder);
            } else {
                buttonGoodsInfoViewHolder = (ButtonGoodsInfoViewHolder) convertView.getTag();
            }
            Glide.with(mContext).load(mListBeans.get(position).getGoods_info().getPic()).into(buttonGoodsInfoViewHolder.iv_goods_img);
            buttonGoodsInfoViewHolder.tv_goods_name.setText(mListBeans.get(position).getGoods_info().getGoods_name());
            if (mListBeans.get(position).getGoods_info().getIs_enquiry() == 1) {
                buttonGoodsInfoViewHolder.tv_goods_price.setText(R.string.contact_to_inquiry);
            } else {
                buttonGoodsInfoViewHolder.tv_goods_price.setText(mListBeans.get(position).getGoods_info().getGoods_promotion_price());
            }
            buttonGoodsInfoViewHolder.tv_send_goods_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onSendGoodsInfo(position);
                    }
                }
            });
        }
        if (itemType == TYPE_GOODS_INFO) {
            GoodsInfoViewHolder goodsInfoViewHolder;
            if (convertView == null) {
                goodsInfoViewHolder = new GoodsInfoViewHolder();
                convertView = View.inflate(mContext, R.layout.item_chat_goods_info, null);
                goodsInfoViewHolder.tv_goods_name = convertView.findViewById(R.id.tv_goods_name);
                goodsInfoViewHolder.tv_goods_price = convertView.findViewById(R.id.tv_goods_price);
                goodsInfoViewHolder.iv_goods_img = convertView.findViewById(R.id.iv_goods_img);
                goodsInfoViewHolder.tv_time = convertView.findViewById(R.id.tv_time);
                goodsInfoViewHolder.rl_goods_info = convertView.findViewById(R.id.rl_goods_info);
                convertView.setTag(goodsInfoViewHolder);
            } else {
                goodsInfoViewHolder = (GoodsInfoViewHolder) convertView.getTag();
            }
            setTimeView(position, goodsInfoViewHolder.tv_time);
            Glide.with(mContext).load(mListBeans.get(position).getGoods_info().getPic()).into(goodsInfoViewHolder.iv_goods_img);
            goodsInfoViewHolder.tv_goods_name.setText(mListBeans.get(position).getGoods_info().getGoods_name());
            if (mListBeans.get(position).getGoods_info().getIs_enquiry()==1) {
                goodsInfoViewHolder.tv_goods_price.setText(R.string.contact_to_inquiry);
            } else {
                goodsInfoViewHolder.tv_goods_price.setText(mListBeans.get(position).getGoods_info().getGoods_promotion_price());
            }
            goodsInfoViewHolder.rl_goods_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toGoodsActivity(mListBeans.get(position).getGoods_info().getGoods_id() + "");
                }
            });
        }
        if(itemType == TYPE_BLANK){
            BlankViewHolder blankViewHolder;
            if (convertView == null) {
                blankViewHolder = new BlankViewHolder();
                convertView = View.inflate(mContext,R.layout.item_chat_blank,null);
                blankViewHolder.space_blank = convertView.findViewById(R.id.space_blank);
                convertView.setTag(blankViewHolder);
            }else {
                blankViewHolder = (BlankViewHolder) convertView.getTag();
            }
        }
        if (itemType == TYPE_SEND_FILE) {
            SendFileViewHolder sendFileViewHolder;
            if (convertView == null) {
                sendFileViewHolder = new SendFileViewHolder();
                convertView = View.inflate(mContext, R.layout.item_chat_send_file, null);
                sendFileViewHolder.iv_avatar = convertView.findViewById(R.id.iv_avatar);
                sendFileViewHolder.iv_file_type = convertView.findViewById(R.id.iv_file_type);
                sendFileViewHolder.tv_file_length = convertView.findViewById(R.id.tv_file_length);
                sendFileViewHolder.tv_file_name = convertView.findViewById(R.id.tv_file_name);
                sendFileViewHolder.tv_time = convertView.findViewById(R.id.tv_time);
                sendFileViewHolder.rl_content = convertView.findViewById(R.id.rl_content);
                convertView.setTag(sendFileViewHolder);
            } else {
                sendFileViewHolder = (SendFileViewHolder) convertView.getTag();
            }
            setTimeView(position, sendFileViewHolder.tv_time);
            sendFileViewHolder.rl_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toFilePreviewActivity(position);
                }
            });
            setAvatar(Login.Companion.getInstance().getMemberAvatar(),sendFileViewHolder.iv_avatar);
            setFileType(sendFileViewHolder.iv_file_type,position);
            sendFileViewHolder.tv_file_name.setText(mListBeans.get(position).getFile().getFile_name());
            sendFileViewHolder.tv_file_length.setText(Utils.getDisplayLength(mListBeans.get(position).getFile().getFile_size()));
        }

        if (itemType == TYPE_RECEIVE_FILE) {
            ReceiveFileViewHolder receiveFileViewHolder;
            if (convertView == null) {
                receiveFileViewHolder = new ReceiveFileViewHolder();
                convertView = View.inflate(mContext, R.layout.item_chat_receive_file, null);
                receiveFileViewHolder.iv_avatar = convertView.findViewById(R.id.iv_avatar);
                receiveFileViewHolder.iv_file_type = convertView.findViewById(R.id.iv_file_type);
                receiveFileViewHolder.tv_file_length = convertView.findViewById(R.id.tv_file_length);
                receiveFileViewHolder.tv_file_name = convertView.findViewById(R.id.tv_file_name);
                receiveFileViewHolder.tv_time = convertView.findViewById(R.id.tv_time);
                receiveFileViewHolder.rl_content = convertView.findViewById(R.id.rl_content);
                convertView.setTag(receiveFileViewHolder);
            } else {
                receiveFileViewHolder = (ReceiveFileViewHolder) convertView.getTag();
            }
            setTimeView(position, receiveFileViewHolder.tv_time);
            receiveFileViewHolder.rl_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toFilePreviewActivity(position);
                }
            });
            setAvatar(mStore_avatar,receiveFileViewHolder.iv_avatar);
            setFileType(receiveFileViewHolder.iv_file_type,position);
            receiveFileViewHolder.tv_file_name.setText(mListBeans.get(position).getFile().getFile_name());
            receiveFileViewHolder.tv_file_length.setText(Utils.getDisplayLength(mListBeans.get(position).getFile().getFile_size()));
        }
        return convertView;
    }

    private void setFileType(ImageView iv_file_type,int position) {
        if(mListBeans.get(position).getFile().getFile_name().endsWith(".pdf")){
            iv_file_type.setImageResource(R.mipmap.send_icon_pdf);
        }else if(mListBeans.get(position).getFile().getFile_name().endsWith(".ppt")
                ||mListBeans.get(position).getFile().getFile_name().endsWith(".pptx")){
            iv_file_type.setImageResource(R.mipmap.send_icon_ppt);
        }else if(mListBeans.get(position).getFile().getFile_name().endsWith(".docx")
                ||mListBeans.get(position).getFile().getFile_name().endsWith(".doc")){
            iv_file_type.setImageResource(R.mipmap.send_icon_docx);
        }else if(mListBeans.get(position).getFile().getFile_name().endsWith(".xls")
                ||mListBeans.get(position).getFile().getFile_name().endsWith(".xlsx")){
            iv_file_type.setImageResource(R.mipmap.send_icon_xls);
        }else {
            iv_file_type.setImageResource(R.mipmap.send_icon_file);
        }
    }

    private void toGoodsActivity(String goodsid) {
        Intent intent = new Intent(mContext, GoodsActivity.class);
        intent.putExtra("goods_id", goodsid);
        mContext.startActivity(intent);
    }

    //设置图片
    private void setImg(int position, ImageView iv_img) {
        int imgWidth = mListBeans.get(position).getImageWidth();
        int imgHeight = mListBeans.get(position).getImageHeight();

        int viewMixWidth = iv_img.getMinimumWidth();
        int viewMaxWidth = iv_img.getMaxWidth();
        int viewMixHeight = iv_img.getMinimumHeight();
        int viewMaxHeight = iv_img.getMaxHeight();
        if (imgWidth != 0 && imgHeight != 0) {
            int viewWidth = 0;
            int viewHeight = 0;
            if (imgHeight > imgWidth) {
                if (imgHeight >= viewMaxHeight) {
                    //取view的最大高度
                    viewHeight = viewMaxHeight;
                } else if (viewMixHeight <= imgHeight && imgHeight < viewMaxHeight) {
                    //取图片的高度
                    viewHeight = imgHeight;
                } else {
                    //取view的最小高度
                    viewHeight = viewMixHeight;
                }
                viewWidth = viewHeight * imgWidth / imgHeight;
            } else {
                if (imgWidth >= viewMaxWidth) {
                    //取view的最大宽度
                    viewWidth = viewMaxWidth;
                } else if (viewMixWidth <= imgWidth && imgWidth < viewMaxWidth) {
                    //取图片的宽度
                    viewWidth = imgWidth;
                } else {
                    //取view的最小宽度
                    viewWidth = viewMixWidth;
                }
                viewHeight = viewWidth * imgHeight / imgWidth;
            }

            iv_img.getLayoutParams().width = viewWidth;
            iv_img.getLayoutParams().height = viewHeight;
        } else {
            iv_img.getLayoutParams().width = viewMixWidth;
            iv_img.getLayoutParams().height = viewMixHeight;
        }
        String localImgPath = mListBeans.get(position).getLocalImgPath();
        if (Utils.isFileExist(localImgPath)) {
            Glide.with(mContext).load(new File(localImgPath)).into(iv_img);
        } else {
            Glide.with(mContext).load(mListBeans.get(position).getT_msg().split("\"")[1]).apply(mGlideOptions).into(iv_img);
        }
    }

    //设置头像
    private void setAvatar(String avatatUrl, ImageView iv_img) {
//        Glide.with(mContext).load(avatatUrl).into(iv_img);
        Glide.with(mContext).load(avatatUrl)
                .apply(RequestOptions.bitmapTransform(new RoundedCornersTransformation(Utils.dp2px(mContext,4f),0))).into(iv_img);
    }

    private void toPhotoViewActivity(View view, int position) {
        Intent intent = new Intent(mContext, PhotoViewActivity.class);
        if (Utils.isFileExist(mListBeans.get(position).getLocalImgPath())) {
            intent.putExtra(PhotoViewActivity.KEY_IMG, mListBeans.get(position).getLocalImgPath());
            intent.putExtra(PhotoViewActivity.TYPE, PhotoViewActivity.TYPE_PATH);
        } else {
            intent.putExtra(PhotoViewActivity.KEY_IMG, mListBeans.get(position).getT_msg().split("\"")[1]);
            intent.putExtra(PhotoViewActivity.TYPE, PhotoViewActivity.TYPE_URL);
        }
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation((Activity) mContext, view, mContext.getResources().getString(R.string.app_name));
        ActivityCompat.startActivity(mContext, intent, options.toBundle());
    }

    private void toFilePreviewActivity(int position){
        Intent intent = new Intent(mContext, FilePreviewActivity.class);
        intent.putExtra(FilePreviewActivity.KEY_FILE_URL,mListBeans.get(position).getFile().getFile_url());
        intent.putExtra(FilePreviewActivity.KEY_FILE_NAME,mListBeans.get(position).getFile().getFile_name());
        intent.putExtra(FilePreviewActivity.KEY_FILE_LENGTH,mListBeans.get(position).getFile().getFile_size());
        mContext.startActivity(intent);
    }

    private void setTimeView(int position, TextView tv_time) {
        if (position > 0 && Utils.getDisplayDataAndTime(mListBeans.get(position).getAdd_time() + "")
                .equals(Utils.getDisplayDataAndTime(mListBeans.get(position - 1).getAdd_time() + ""))) {
            tv_time.setVisibility(View.GONE);
        } else {
            tv_time.setVisibility(View.VISIBLE);
            tv_time.setText(Utils.getDisplayDataAndTime(mListBeans.get(position).getAdd_time() + ""));
        }
    }

    private class SendTextViewHolder {
        private TextView tv_time, tv_msg;
        private ImageView iv_avatar;
    }

    private class ReceiveTextViewHolder {
        private TextView tv_time, tv_msg;
        private ImageView iv_avatar;
    }

    private class ReceiveImgViewHolder {
        private TextView tv_time;
        private ImageView iv_avatar, iv_img;
    }

    private class SendImgViewHolder {
        private TextView tv_time;
        private ImageView iv_avatar, iv_img;
    }

    private class ButtonGoodsInfoViewHolder {
        private TextView tv_goods_name, tv_goods_price, tv_send_goods_info;
        private ImageView iv_goods_img;
    }

    private class GoodsInfoViewHolder {
        private TextView tv_time, tv_goods_name, tv_goods_price;
        private ImageView iv_goods_img;
        private RelativeLayout rl_goods_info;
    }

    private class BlankViewHolder {
        private Space space_blank;
    }

    private class SendFileViewHolder {
        private TextView tv_file_name, tv_file_length,tv_time;
        private ImageView iv_avatar,iv_file_type;
        private RelativeLayout rl_content;
    }

    private class ReceiveFileViewHolder {
        private TextView tv_file_name, tv_file_length,tv_time;
        private ImageView iv_avatar,iv_file_type;
        private RelativeLayout rl_content;
    }

    public interface OnSendGoodsInfoClickListener {
        void onSendGoodsInfo(int position);
    }

    public void setListener(OnSendGoodsInfoClickListener listener) {
        mListener = listener;
    }

}
