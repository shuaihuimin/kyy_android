package com.xht.kuaiyouyi.ui.cart.entity;

public class DeleteGoodsBean {

    /**
     * del : 1
     */

    private int del;

    public int getDel() {
        return del;
    }

    public void setDel(int del) {
        this.del = del;
    }
}
