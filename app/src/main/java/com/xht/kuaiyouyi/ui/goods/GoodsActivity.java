package com.xht.kuaiyouyi.ui.goods;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.xht.kuaiyouyi.KyyApp;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.cart.adapter.CompanyListAdapter;
import com.xht.kuaiyouyi.ui.cart.entity.CompanyListBean;
import com.xht.kuaiyouyi.ui.cart.order.ConfirmOrderActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.adapter.GoodsgaugeAdapter;
import com.xht.kuaiyouyi.ui.goods.entity.GoodsData;
import com.xht.kuaiyouyi.ui.goods.entity.GoodsGagueData;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.ui.message.activity.ChatActivity;
import com.xht.kuaiyouyi.ui.mine.setting.CurrencyActivity;
import com.xht.kuaiyouyi.ui.search.entity.ScreenBean;
import com.xht.kuaiyouyi.utils.CommonPopupWindow;
import com.xht.kuaiyouyi.utils.CommonUtil;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.SquareGlideImageLoader;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.AmountView;
import com.xht.kuaiyouyi.widget.BadgeView;
import com.xht.kuaiyouyi.widget.ButtomDialog;
import com.xht.kuaiyouyi.widget.CustomListView;
import com.xht.kuaiyouyi.widget.RoundImageView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;

public class GoodsActivity extends BaseActivity implements GoodsgaugeAdapter.OnItemClickListener{
    @BindView(R.id.iv_back)
    ImageView imageback;
    @BindView(R.id.radioGroup_fragmentgoods)
    RadioGroup radioGroup;
    @BindView(R.id.rb_goods)
    RadioButton rb_goods;
    @BindView(R.id.rb_detils)
    RadioButton rb_detils;
    @BindView(R.id.tv_collecte)
    TextView tv_collecte;
    @BindView(R.id.tv_service)
    TextView tv_service;
    @BindView(R.id.tv_store)
    TextView tv_store;
    @BindView(R.id.btn_add_cart)
    Button btn_add_cart;
    @BindView(R.id.btn_buy_now)
    Button btn_buy_now;
    @BindView(R.id.btn_customer_service)
    Button btn_customer_service;
    @BindView(R.id.banner_goods)
    Banner banner;
    @BindView(R.id.tv_content)
    TextView tv_content;
    @BindView(R.id.linear_selected)
    LinearLayout linear_selected;
    @BindView(R.id.linear_freight)
    LinearLayout linear_freight;
    @BindView(R.id.tv_selected)
    TextView tv_selected;
    @BindView(R.id.tv_fuwu)
    TextView tv_fuwu;
    @BindView(R.id.tv_goods_freight)
    TextView tv_goods_freight;
    @BindView(R.id.tv_next)
    TextView tv_next;
    @BindView(R.id.fragmentgoods)
    FrameLayout frameLayout;
    @BindView(R.id.linear_price)
    LinearLayout linear_price;
    @BindView(R.id.relative_stare)
    RelativeLayout relative_stare;
    @BindView(R.id.iv_starehead)
    RoundImageView iv_starehead;
    @BindView(R.id.iv_cart)
    ImageView iv_cart;
    @BindView(R.id.iv_service)
    ImageView iv_service;
    @BindView(R.id.tv_starename)
    TextView tv_starename;
    @BindView(R.id.goods_shelves)
    TextView goods_shelves;
    @BindView(R.id.tv_goods_share)
    TextView tv_goods_share;
    @BindView(R.id.nestedscrollview)
    NestedScrollView nestedScrollView;
    @BindView(R.id.radioGroup_goodsdetils)
    RadioGroup radioGroup_goodsdetils;
    @BindView(R.id.radioGroup_goods)
    RadioGroup radioGroup_goods;
    @BindView(R.id.rb_i)
    RadioButton rb_i;
    @BindView(R.id.rb_s)
    RadioButton rb_s;
    @BindView(R.id.rb_c)
    RadioButton rb_c;
    @BindView(R.id.rb_a)
    RadioButton rb_a;
    @BindView(R.id.rb_introduce)
    RadioButton rb_introduce;
    @BindView(R.id.rb_specifications)
    RadioButton rb_specifications;
    @BindView(R.id.rb_case)
    RadioButton rb_case;
    @BindView(R.id.rb_anli)
    RadioButton rb_anli;
    @BindView(R.id.fragment_cart)
    FrameLayout fragment_cart;
    @BindView(R.id.error_retry_view)
    RelativeLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;
    @BindView(R.id.r1)
    LinearLayout r1;
    @BindView(R.id.relative)
    RelativeLayout relative;
    @BindView(R.id.iv_back_err)
    ImageView iv_back_err;
    @BindView(R.id.relative_invalid_goods)
    RelativeLayout relative_invalid_goods;
    @BindView(R.id.iv_invalid_goods_servers)
    ImageView iv_invalid_goods_servers;
    @BindView(R.id.iv_back_goods_servers)
    ImageView iv_back_goods_servers;
    private CustomListView listView;
    private GoodsData.GoodsBean mGoodBean;
    private GoodsData.StoreBean storeBean;
    //轮播
    private List<String> listlunbo = new ArrayList<>();
    //汇率
    private GoodsData.CurrencytypeBean currencytypeBean;
    //团购价格
    private List<GoodsData.GoodsBean.BatchPriceBean> listbatchPrice=new ArrayList<>();
    private String id;
    private boolean iscollection;
    private String goods_body, goods_attr;
    //计算汇率
    private double currency=0;
    private String price="";
    private TextView tv_price;
    private TextView tv_dallor;
    private TextView tv_inquiry;
    private TextView tv_num;
    private CommonPopupWindow popupShare;


    //筛选规格
    private CommonPopupWindow popupFreight;
    private ButtomDialog mSelectSpecDialog;
    private CommonPopupWindow popupCompany;
    private GoodsgaugeAdapter goodsgaugeAdapter;
    //规格数据
    private List<ScreenBean> list = new ArrayList<>();
    //默认规格数据
    private List<String> goodsspec_list=new ArrayList<>();
    //所有规格的集合
    private List<GoodsGagueData.SpecAllInfoBean> listdata;
    //规格中批发价格集合
    private List<GoodsGagueData.BatchPriceBean> listprice=new ArrayList<>();
    //用户选择的规格
    private String check_gague;
    private ImageView img_goods;
    private ImageView image_clear;
    private TextView tv_gagueprice;
    private TextView tv_goodsguige;
    private TextView tv_inventory;
    private TextView tv_gaguecar;
    private TextView tv_gagueshop;
    private TextView tv_goodsdollar;
    private TextView tv_goodsinquiry;
    private TextView tv_gaguequer;
    private TextView tv_customer;
    private AmountView amountView;
    private LinearLayout line;
    private String goodsid;
    private String store_id;
    private String goodsattribute="";
    //缓存Fragment或上次显示的Fragment
    private BaseFragment tempFragment;
    private int position = 0;
    private List<BaseFragment> fragments = new ArrayList<>();
    //选择采购方式，企业或者个人id
    private String company_id;
    //商品id加数量
    private String cart_str;
    //默认商品添加数量
    private String addcarnum="0";
    private String default_avatat;
    private SHARE_MEDIA mshare_media;
    private boolean sate=false;
    public final static String TO_HOME_ENTERPRISE = "to_home_enterprise";

    private BadgeView mBadgeView;
    private BadgeView mBadgeView_invalid;

    private int height,sly;
    private float y;

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    //判断是否是下架商品
                    if(mGoodBean.getGoods_state().equals("0")){
                        goods_shelves.setVisibility(View.VISIBLE);
                        btn_buy_now.setClickable(false);
                        btn_add_cart.setClickable(false);
                        btn_buy_now.setBackgroundColor(getResources().getColor(R.color.blue_disable));
                        btn_add_cart.setBackgroundColor(getResources().getColor(R.color.grayPrimary));
                    }

                    //判断是否有运费
                    if(!TextUtils.isEmpty(mGoodBean.getTransport_id())){
                        if(mGoodBean.getTransport_id().equals("0")){
                            tv_fuwu.setText("¥"+mGoodBean.getGoods_freight());
                            tv_next.setVisibility(View.GONE);
                            if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.RMB)){
                                tv_goods_freight.setVisibility(View.GONE);
                            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.HKD)){
                                currency = Double.parseDouble(mGoodBean.getGoods_freight()) * currencytypeBean.getHKD().getRate();
                                price=Utils.getDisplayMoney(currency);
                                tv_goods_freight.setText("(~" + currencytypeBean.getHKD().getSymbol() + price + ")");
                            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.MOP)){
                                currency = Double.parseDouble(mGoodBean.getGoods_freight()) * currencytypeBean.getMOP().getRate();
                                price=Utils.getDisplayMoney(currency);
                                tv_goods_freight.setText("(~" + currencytypeBean.getMOP().getSymbol() + price + ")");
                            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.USA)){
                                currency = Double.parseDouble(mGoodBean.getGoods_freight()) * currencytypeBean.getUSD().getRate();
                                price=Utils.getDisplayMoney(currency);
                                tv_goods_freight.setText("(~" + currencytypeBean.getUSD().getSymbol() + price + ")");
                            }
                        }else if(mGoodBean.getTransport_id().equals("1")){
                            tv_next.setVisibility(View.VISIBLE);
                            tv_fuwu.setText(getString(R.string.goods_freight_detils));
                        }else if(mGoodBean.getTransport_id().equals("2")){
                            tv_next.setVisibility(View.GONE);
                            tv_fuwu.setText(getString(R.string.free_shipping));
                        }
                    }

                    //判断是否收藏
                    if (iscollection==false) {
                        tv_collecte.setSelected(true);
                    } else {
                        tv_collecte.setSelected(false);
                    }
                    //判断是否是询价商品
                    if(mGoodBean.getIs_enquiry().equals("0")){
                        btn_add_cart.setVisibility(View.VISIBLE);
                        btn_buy_now.setVisibility(View.VISIBLE);
                        btn_customer_service.setVisibility(View.GONE);
                        tv_service.setVisibility(View.VISIBLE);
                    }else {
                        btn_add_cart.setVisibility(View.GONE);
                        btn_buy_now.setVisibility(View.GONE);
                        btn_customer_service.setVisibility(View.VISIBLE);
                        tv_service.setVisibility(View.GONE);
                    }
                    if (listlunbo!=null && listlunbo.size()!=0){
                        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
                        banner.setImageLoader(new SquareGlideImageLoader());
                        banner.setImages(listlunbo);
                        banner.setDelayTime(3000);
                        banner.setIndicatorGravity(BannerConfig.CENTER);
                        banner.start();
                    }
                    ViewGroup.LayoutParams layoutParams=banner.getLayoutParams();
                    layoutParams.height=Utils.getWindowWidth(GoodsActivity.this);
                    if(storeBean.getStore_label()!=null){
                        Glide.with(GoodsActivity.this).load(storeBean.getStore_label())
                                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                                .into(iv_starehead);
                    }
                    tv_starename.setText(storeBean.getStore_name());
                    if(mGoodBean.getIs_second_hand()==1){
                        tv_content.setText(Utils.secondLabel(mGoodBean.getGoods_name()));
                    }else {
                        tv_content.setText(mGoodBean.getGoods_name());
                    }
                    if(!TextUtils.isEmpty(mGoodBean.getUnit_name())){
                        tv_selected.setText(goodsattribute+addcarnum+mGoodBean.getUnit_name());
                    }else {
                        tv_selected.setText(goodsattribute+addcarnum+getString(R.string.goods_num));
                    }

                    cart_str=mGoodBean.getGoods_id()+"|"+addcarnum;
                    //判断是否有团购商品，创建展示价格布局
                    if(listbatchPrice.size()==0){
                        linear_price.removeAllViews();
                        View view=View.inflate(GoodsActivity.this,R.layout.item_goods_detils_price,null);
                        tv_price=(TextView) view.findViewById(R.id.tv_pricee);
                        tv_dallor=(TextView) view.findViewById(R.id.tv_dallor);
                        tv_inquiry=(TextView) view.findViewById(R.id.tv_inquiry);
//                        tv_num=(TextView)view.findViewById(R.id.tv_num);
//                        if(mGoodBean.getGoods_storage()==0){
//                            tv_num.setVisibility(View.GONE);
//                        }else {
//                            tv_num.setVisibility(View.VISIBLE);
//                            tv_num.setText(mGoodBean.getGoods_storage()+getString(R.string.goods_num));
//                        }
                        linear_price.addView(view);
                        if(mGoodBean.getIs_enquiry().equals("0")){
                            tv_inquiry.setVisibility(View.GONE);
                            tv_price.setText("¥"+Utils.getDisplayMoney(mGoodBean.getGoods_price()));
                            if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.RMB)){
                                tv_dallor.setVisibility(View.GONE);
                            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.HKD)){
                                currency = mGoodBean.getGoods_price() * currencytypeBean.getHKD().getRate();
                                price=Utils.getDisplayMoney(currency);
                                tv_dallor.setText("(~" + currencytypeBean.getHKD().getSymbol() + price + ")");
                            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.MOP)){
                                currency = mGoodBean.getGoods_price() * currencytypeBean.getMOP().getRate();
                                price=Utils.getDisplayMoney(currency);
                                tv_dallor.setText("(~" + currencytypeBean.getMOP().getSymbol() + price + ")");
                            }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.USA)){
                                currency = mGoodBean.getGoods_price() * currencytypeBean.getUSD().getRate();
                                price=Utils.getDisplayMoney(currency);
                                tv_dallor.setText("(~" + currencytypeBean.getUSD().getSymbol() + price + ")");
                            }
                        }else {
                            tv_inquiry.setVisibility(View.VISIBLE);
                            tv_price.setVisibility(View.GONE);
                            tv_dallor.setVisibility(View.GONE);
                        }
                    }else if(listbatchPrice.size()!=0 && listbatchPrice.size()>0){
                        linear_price.removeAllViews();
                        for(int i=0;i<listbatchPrice.size();i++){
                            View view=View.inflate(GoodsActivity.this,R.layout.item_goods_detils_price,null);
                            LinearLayout linearLayout=(LinearLayout) view.findViewById(R.id.linear);
                            ViewGroup.LayoutParams params=linearLayout.getLayoutParams();
                            params.width=getWindowManager().getDefaultDisplay().getWidth()/3;
                            tv_price=(TextView) view.findViewById(R.id.tv_pricee);
                            tv_dallor=(TextView) view.findViewById(R.id.tv_dallor);
                            tv_inquiry=(TextView) view.findViewById(R.id.tv_inquiry);
                            tv_num=(TextView)view.findViewById(R.id.tv_num);
                            tv_num.setVisibility(View.VISIBLE);
                            if(!TextUtils.isEmpty(mGoodBean.getUnit_name())){
                                tv_num.setText(listbatchPrice.get(i).getBatch()+mGoodBean.getUnit_name());
                            }else {
                                tv_num.setText(listbatchPrice.get(i).getBatch()+getString(R.string.goods_num));
                            }
                            linear_price.addView(view);
                            if(mGoodBean.getIs_enquiry().equals("0")){
                                tv_inquiry.setVisibility(View.GONE);
                                tv_price.setText("¥"+Utils.getDisplayMoney(listbatchPrice.get(i).getPrice()));
                                if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.RMB)){
                                    tv_dallor.setVisibility(View.GONE);
                                }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.HKD)){
                                    currency = listbatchPrice.get(i).getPrice() * currencytypeBean.getHKD().getRate();
                                    price=Utils.getDisplayMoney(currency);
                                    tv_dallor.setText("(~" + currencytypeBean.getHKD().getSymbol() + price + ")");
                                }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.MOP)){
                                    currency = listbatchPrice.get(i).getPrice() * currencytypeBean.getMOP().getRate();
                                    price=Utils.getDisplayMoney(currency);
                                    tv_dallor.setText("(~" + currencytypeBean.getMOP().getSymbol() + price + ")");
                                }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.USA)){
                                    currency = listbatchPrice.get(i).getPrice() * currencytypeBean.getUSD().getRate();
                                    price=Utils.getDisplayMoney(currency);
                                    tv_dallor.setText("(~" + currencytypeBean.getUSD().getSymbol() + price + ")");
                                }
                            }else {
                                tv_inquiry.setVisibility(View.VISIBLE);
                                tv_price.setVisibility(View.GONE);
                                tv_dallor.setVisibility(View.GONE);
                            }
                        }
                    }
                    initfragment();
                    break;

                case 2:
                    if (iscollection==false) {
                        tv_collecte.setSelected(true);
                    } else {
                        tv_collecte.setSelected(false);

                    }
                    break;
                case 3:
                    if(amountView!=null){
                        amountView.setAmount(Integer.parseInt(addcarnum));
                    }
            }
        }
    };

    @Override
    protected int getLayout() {
        return R.layout.activity_goods;
    }

    @Override
    protected void initView() {
        mBadgeView = new BadgeView(this,iv_service,10,4);
        mBadgeView_invalid=new BadgeView(this,iv_invalid_goods_servers,10,4);
        EventBus.getDefault().register(this);
        Uri uri =  getIntent().getData();
        if (uri != null) {
            goodsid = uri.getQueryParameter("goodsid");
        }else {
            goodsid = getIntent().getStringExtra("goods_id");
        }
        height=Utils.getWindowWidth(GoodsActivity.this)/5*4;
        if(tv_content.length()>20){
            sly=Utils.dp2px(GoodsActivity.this,702);
        }else {
            sly=Utils.dp2px(GoodsActivity.this,684);
        }
        getGoodsinfo(goodsid);
        click();
        showUnreadMsgNum();

    }



    private void showUnreadMsgNum() {
        mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
        mBadgeView_invalid.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
    }

    private void initfragment() {
        fragments.clear();
        fragments.add(Fragment_goodsDetils.newInstance(goods_body));
        fragments.add(Fragment_specifications.newInstance(goods_attr));
        fragments.add(Fragment_case.newInstance(goodsid));
        fragments.add(Fragment_purchase.newInstance());
        BaseFragment baseFragment = getFragment(position);
        switchFragment(tempFragment, baseFragment);
    }

    /**
     * 根据位置得到对应的 Fragment
     * @param position
     * @return
     */
    private BaseFragment getFragment(int position){
        if(fragments != null && fragments.size()>0){
            return fragments.get(position);
        }
        return null;
    }

    /**
     * 切换Fragment
     * @param fragment
     * @param nextFragment
     */
    private void switchFragment(Fragment fragment,BaseFragment nextFragment) {
        if (tempFragment != nextFragment) {
            tempFragment = nextFragment;
            if (nextFragment != null) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                //判断nextFragment是否添加成功
                if (!nextFragment.isAdded()) {
                    //隐藏当前的Fragment
                    if (fragment != null) {
                        transaction.hide(fragment);
                    }
                    //添加Fragment
                    transaction.add(R.id.fragmentgoods, nextFragment).commit();
                } else {
                    //隐藏当前Fragment
                    if (fragment != null) {
                        transaction.hide(fragment);
                    }
                    transaction.show(nextFragment).commitAllowingStateLoss();
                }
            }
        }
    }

    private void click() {
        iv_back_goods_servers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoodsActivity.this.finish();
            }
        });
        iv_back_err.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoodsActivity.this.finish();
            }
        });
        iv_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.Companion.getInstance().isLogin()){
                    goToActivity(CartActivity.class);
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
        iv_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.Companion.getInstance().isLogin()){
                    goToActivity(MessageActivity.class);
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
        iv_invalid_goods_servers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.Companion.getInstance().isLogin()){
                    goToActivity(MessageActivity.class);
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(KyyApp.activitynum>1){
                    GoodsActivity.this.finish();
                }else {
                    goToActivity(MainActivity.class);
                    GoodsActivity.this.finish();
                }
            }
        });

        rb_goods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_goods.setChecked(true);
                nestedScrollView.scrollTo(0,0);
                rb_goods.setTextSize(16);
                rb_detils.setTextSize(14);
            }
        });
        rb_detils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_detils.setClickable(true);
                rb_goods.setTextSize(14);
                rb_detils.setTextSize(16);
                if(y<=0){
                    nestedScrollView.scrollTo(0,sly);
                }else {
                    nestedScrollView.scrollTo(0,(int) y);
                }
                position=0;

            }
        });
        rb_i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(y<=0){
                    nestedScrollView.scrollTo(0,sly);
                }else {
                    nestedScrollView.scrollTo(0,(int) y);
                }
            }
        });
        rb_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(y<=0){
                    nestedScrollView.scrollTo(0,sly);
                }else {
                    nestedScrollView.scrollTo(0,(int) y);
                }
            }
        });
        rb_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(y<=0){
                    nestedScrollView.scrollTo(0,sly);
                }else {
                    nestedScrollView.scrollTo(0,(int) y);
                }
            }
        });
        rb_s.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(y<=0){
                    nestedScrollView.scrollTo(0,sly);
                }else {
                    nestedScrollView.scrollTo(0,(int) y);
                }
            }
        });
        radioGroup_goodsdetils.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb_introduce:
                        position=0;
                        rb_i.setChecked(true);
                        break;
                    case R.id.rb_specifications:
                        position=1;
                        rb_s.setChecked(true);
                        break;
                    case R.id.rb_anli:
                        position=2;
                        rb_a.setChecked(true);
                        break;
                    case R.id.rb_case:
                        position=3;
                        rb_c.setChecked(true);
                        break;
                }
                BaseFragment baseFragment = getFragment(position);
                switchFragment(tempFragment, baseFragment);
            }
        });

        radioGroup_goods.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                ViewGroup.LayoutParams layoutParams= frameLayout.getLayoutParams();
                int loog=Utils.getWindoHeight(GoodsActivity.this)-Utils.dp2px(GoodsActivity.this,120);
                switch (checkedId){
                    case R.id.rb_i:
                        position=0;
                        rb_introduce.setChecked(true);
                        layoutParams.height= ViewGroup.LayoutParams.WRAP_CONTENT;
                        break;
                    case R.id.rb_s:
                        position=1;
                        rb_specifications.setChecked(true);
                        layoutParams.height= ViewGroup.LayoutParams.WRAP_CONTENT;
                        break;
                    case R.id.rb_a:
                        position=2;
                        rb_anli.setChecked(true);
                        frameLayout.getLayoutParams().height=loog;
                        break;
                    case R.id.rb_c:
                        position=3;
                        rb_case.setChecked(true);
                        frameLayout.getLayoutParams().height=loog;
                        break;
                }
                BaseFragment baseFragment = getFragment(position);
                switchFragment(tempFragment, baseFragment);
//
            }
        });


        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                y= radioGroup_goodsdetils.getY();
                if(scrollY>=y || scrollY>=sly){
                    radioGroup_goods.setVisibility(View.VISIBLE);
                    radioGroup_goodsdetils.setVisibility(View.INVISIBLE);
                    rb_detils.setChecked(true);
                    rb_goods.setTextSize(14);
                    rb_detils.setTextSize(16);
                    rb_i.setClickable(true);
                    rb_a.setClickable(true);
                    rb_c.setClickable(true);
                    rb_s.setClickable(true);
                    rb_introduce.setClickable(false);
                    rb_anli.setClickable(false);
                    rb_specifications.setClickable(false);
                    rb_case.setClickable(false);
                }else {
                    rb_i.setClickable(false);
                    rb_a.setClickable(false);
                    rb_c.setClickable(false);
                    rb_s.setClickable(false);
                    rb_introduce.setClickable(true);
                    rb_anli.setClickable(true);
                    rb_specifications.setClickable(true);
                    rb_case.setClickable(true);
                    radioGroup_goods.setVisibility(View.GONE);
                    radioGroup_goodsdetils.setVisibility(View.VISIBLE);
                    rb_goods.setChecked(true);
                    rb_goods.setTextSize(16);
                    rb_detils.setTextSize(14);
                }
            }
        });

        tv_collecte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Login.Companion.getInstance().isLogin() == false) {
                    goToActivity(LoginActivity.class);

                } else {
                    if (iscollection==false) {
                        //取消收藏
                        setcollection("1");
                    } else {
                        //收藏
                        setcollection("0");
                    }
                }
            }
        });

        btn_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectSpecDialog=null;
                showpopu(1);
            }
        });

        btn_buy_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectSpecDialog=null;
                showpopu(2);
            }
        });
        btn_customer_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactService();
            }
        });
        linear_selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectSpecDialog=null;
                showpopu(0);
            }
        });
        linear_freight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(mGoodBean.getTransport_id()) && mGoodBean.getTransport_id().equals("1")){
                    final View upView = LayoutInflater.from(GoodsActivity.this).inflate(R.layout.pop_goods_instructions, null);
                    //测量View的宽高
                    CommonUtil.measureWidthAndHeight(upView);
                    if (popupFreight == null ){
                        popupFreight = new CommonPopupWindow.Builder(GoodsActivity.this)
                                .setView(upView)
                                .setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                                .setBackGroundLevel(0.8f)//取值范围0.0f-1.0f 值越小越暗
                                .create();
                        ImageView imageView=(ImageView) upView.findViewById(R.id.iv_back);
                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                popupFreight.dismiss();
                            }
                        });
                    }
                    popupFreight.showAtLocation(upView, Gravity.BOTTOM, 0, 0);
                    Utils.SetWindBg(GoodsActivity.this);
                }
            }
        });
        tv_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(store_id)){
                    Bundle bundle = new Bundle();
                    bundle.putString("store_id", store_id);
                    goToActivity(StoreActivity.class, bundle);
                }
            }
        });
        relative_stare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("store_id", store_id);
                goToActivity(StoreActivity.class, bundle);
            }
        });
        tv_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactService();
            }
        });

        //分享
        tv_goods_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View view = LayoutInflater.from(GoodsActivity.this).inflate(R.layout.pop_goods_share_layout, null);
                //测量View的宽高
                CommonUtil.measureWidthAndHeight(view);
                if (popupShare == null ){
                    popupShare = new CommonPopupWindow.Builder(GoodsActivity.this)
                            .setView(view)
                            .setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                            .setBackGroundLevel(0.8f)//取值范围0.0f-1.0f 值越小越暗
                            .create();
                    TextView tv_share_weixin=(TextView) view.findViewById(R.id.tv_share_weixin);
                    tv_share_weixin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setShare(SHARE_MEDIA.WEIXIN);
                            mshare_media=SHARE_MEDIA.WEIXIN;
                            popupShare.dismiss();
                        }
                    });
                    TextView tv_share_ding=(TextView) view.findViewById(R.id.tv_share_ding);
                    tv_share_ding.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setShare(SHARE_MEDIA.DINGTALK);
                            mshare_media=SHARE_MEDIA.DINGTALK;
                            popupShare.dismiss();
                        }
                    });
                    TextView tv_share_whatsapp=(TextView) view.findViewById(R.id.tv_share_whatsapp);
                    tv_share_whatsapp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setShare(SHARE_MEDIA.WHATSAPP);
                            mshare_media=SHARE_MEDIA.WHATSAPP;
                            popupShare.dismiss();
                        }
                    });
                    TextView tv_share_copy=(TextView) view.findViewById(R.id.tv_share_copy);
                    tv_share_copy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DialogUtils.createTipAllTextDialog(GoodsActivity.this,getString(R.string.copy_su));
                            Utils.getCopy(GoodsActivity.this,mGoodBean.getGoods_share_title());
                            popupShare.dismiss();
                        }
                    });

                    TextView tv_cancel_share=(TextView) view.findViewById(R.id.tv_cancel_share);
                    tv_cancel_share.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupShare.dismiss();
                        }
                    });
                }
                popupShare.showAtLocation(view, Gravity.BOTTOM, 0, 0);
                Utils.SetWindBg(GoodsActivity.this);
            }
        });

        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getGoodsinfo(goodsid);
            }
        });

    }

    /**
     * 联系客服
     */
    private void contactService() {
        if(Login.Companion.getInstance().isLogin()){
            if(storeBean!=null && !TextUtils.isEmpty(storeBean.getDefault_im())){
                Bundle bundle = new Bundle();
                bundle.putString(ChatActivity.CHAT_ID,storeBean.getDefault_im());
                bundle.putString(ChatActivity.STORE_AVATAR,storeBean.getDefault_avatar());
                bundle.putString(ChatActivity.STORE_NAME,storeBean.getStore_name());
                try {
                    bundle.putLong(ChatActivity.STORE_ID, Long.parseLong(store_id));
                }catch (Exception e){
                    bundle.putLong(ChatActivity.STORE_ID, 0);
                }
                if(TextUtils.isEmpty(goodsid)){
                    bundle.putLong(ChatActivity.GOODS_ID,0);
                }else {
                    bundle.putLong(ChatActivity.GOODS_ID,Long.valueOf(goodsid));
                }
                bundle.putString(ChatActivity.GOODS_NAME,mGoodBean.getGoods_name());
                bundle.putString(ChatActivity.GOODS_IMG,mGoodBean.getGoods_image());
                bundle.putString(ChatActivity.GOODS_PRICE,"¥" + mGoodBean.getGoods_price());
                bundle.putBoolean(ChatActivity.IS_ENQUIRY,mGoodBean.getIs_enquiry().equals("1"));
                bundle.putBoolean(ChatActivity.IS_SECONG_HAND,mGoodBean.getIs_second_hand()==1);
                goToActivity(ChatActivity.class,bundle);
            }else {
                //ToastU.INSTANCE.showToast(getString(R.string.no_customer_service));
            }
        }else {
            goToActivity(LoginActivity.class);
        }
    }


    /**
     * 初始化规格popupwindow上控件
     * flag=0(已选规格)
     * flag=1(加入采购单)
     * flag=2(立即下单)
     */
    private void initpopupwindow(View view, final int flag) {
        line=(LinearLayout) view.findViewById(R.id.line);
        listView = (CustomListView) view.findViewById(R.id.listview_pop);
        img_goods = view.findViewById(R.id.img_goods);
        image_clear = view.findViewById(R.id.image_clear);
        tv_gagueprice = view.findViewById(R.id.tv_goodsprice);
        tv_goodsdollar=view.findViewById(R.id.tv_goodsinquiry);
        tv_goodsinquiry=view.findViewById(R.id.tv_goodsinquiry);
        tv_goodsguige = view.findViewById(R.id.tv_goods);
        tv_goodsdollar=view.findViewById(R.id.tv_goodsdollar);
        tv_inventory = view.findViewById(R.id.tv_inventory);
        tv_gaguecar = view.findViewById(R.id.tv_gaguecar);
        tv_gagueshop = view.findViewById(R.id.tv_gagueshop);
        tv_gaguequer=view.findViewById(R.id.tv_gaguequer);
        tv_customer=view.findViewById(R.id.tv_customer_service);
        if(mGoodBean.getIs_enquiry().equals("0")){
            tv_customer.setVisibility(View.GONE);
            if(flag==0){
                tv_gaguequer.setVisibility(View.GONE);
                tv_gaguecar.setVisibility(View.VISIBLE);
                tv_gagueshop.setVisibility(View.VISIBLE);

            }else {
                tv_gaguequer.setVisibility(View.VISIBLE);
                tv_gaguecar.setVisibility(View.GONE);
                tv_gagueshop.setVisibility(View.GONE);
            }
        }else {
            tv_customer.setVisibility(View.VISIBLE);
            tv_gaguequer.setVisibility(View.GONE);
            tv_gaguecar.setVisibility(View.GONE);
            tv_gagueshop.setVisibility(View.GONE);
        }
        amountView = view.findViewById(R.id.amuntView);
        if (mGoodBean != null) {
            if(mGoodBean.getGoods_storage()>0 && mGoodBean.getGoods_storage()>=Integer.parseInt(mGoodBean.getGoods_min_count())){
                tv_inventory.setText(getString(R.string.goods_inventory_more));
            }else {
                tv_inventory.setText(getString(R.string.goods_inventory_nor));
            }
            if(!TextUtils.isEmpty(mGoodBean.getUnit_name())){
                tv_goodsguige.setText(getString(R.string.goods_selected) + goodsattribute+addcarnum+mGoodBean.getUnit_name());
            }else {
                tv_goodsguige.setText(getString(R.string.goods_selected) + goodsattribute+addcarnum+getString(R.string.goods_num));
            }
            if(mGoodBean.getIs_enquiry().equals("0")){
                tv_gagueprice.setText("¥" + Utils.getDisplayMoney(mGoodBean.getGoods_price()));
                tv_gagueprice.setVisibility(View.VISIBLE);
                tv_goodsdollar.setVisibility(View.VISIBLE);
                tv_goodsinquiry.setVisibility(View.GONE);
                if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.RMB)){
                    tv_goodsdollar.setVisibility(View.GONE);
                }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.HKD)){
                    currency = mGoodBean.getGoods_price() * currencytypeBean.getHKD().getRate();
                    price = Utils.getDisplayMoney(currency);
                    tv_goodsdollar.setVisibility(View.VISIBLE);
                    tv_goodsdollar.setText("(~" + currencytypeBean.getHKD().getSymbol() + price + ")");
                }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.MOP)){
                    currency = mGoodBean.getGoods_price() * currencytypeBean.getMOP().getRate();
                    price = Utils.getDisplayMoney(currency);
                    tv_goodsdollar.setVisibility(View.VISIBLE);
                    tv_goodsdollar.setText("(~" + currencytypeBean.getMOP().getSymbol() + price + ")");
                }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.USA)){
                    currency = mGoodBean.getGoods_price() * currencytypeBean.getMOP().getRate();
                    price = Utils.getDisplayMoney(currency);
                    tv_goodsdollar.setVisibility(View.VISIBLE);
                    tv_goodsdollar.setText("(~" + currencytypeBean.getUSD().getSymbol() + price + ")");
                }
            }else {
                tv_gagueprice.setVisibility(View.GONE);
                tv_goodsdollar.setVisibility(View.GONE);
                tv_goodsinquiry.setVisibility(View.VISIBLE);
            }
            amountView.setGoods_storage(mGoodBean.getGoods_storage());
            if (mGoodBean.getGoods_image() != null)
                Glide.with(GoodsActivity.this).load(mGoodBean.getGoods_image())
                        .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                        .into(img_goods);
        }

        amountView.setNum(Integer.parseInt(addcarnum));
        if(sate==false){
            sate=true;
            amountView.setAmount(Integer.parseInt(addcarnum));
        }
        amountView.setOnAmountChangeListener(new AmountView.OnAmountChangeListener() {
            @Override
            public void onAmountChange(View view, int amount) {
                if(amount>mGoodBean.getGoods_storage()){
                    DialogUtils.createTipImageAndTextDialog(GoodsActivity.this,getString(R.string.count_beyond),R.mipmap.png_icon_popup_warning);
                    addcarnum=mGoodBean.getGoods_storage()+"";
                    amountView.setNum(mGoodBean.getGoods_storage());
                }else {
                    addcarnum=amount+"";
                    cart_str=mGoodBean.getGoods_id()+"|"+addcarnum;
                    if(!TextUtils.isEmpty(mGoodBean.getUnit_name())){
                        tv_selected.setText(goodsattribute+addcarnum+mGoodBean.getUnit_name());
                    }else {
                        tv_selected.setText(goodsattribute+addcarnum+getString(R.string.goods_num));
                    }
                    if(!TextUtils.isEmpty(mGoodBean.getUnit_name())){
                        tv_goodsguige.setText(getString(R.string.goods_selected) + goodsattribute+addcarnum+mGoodBean.getUnit_name());
                    }else {
                        tv_goodsguige.setText(getString(R.string.goods_selected) + goodsattribute+addcarnum+getString(R.string.goods_num));
                    }

                    if(listprice.size()!=0){
                        tv_gagueprice.setVisibility(View.VISIBLE);
                        tv_goodsinquiry.setVisibility(View.GONE);
                        int mix,max;
                        for(int i=0;i<listprice.size();i++){
                            mix=listprice.get(i).getMix();
                            max=listprice.get(i).getMax();
                            if(i==listprice.size()-1){
                                if(amount>=mix){
                                    tv_gagueprice.setText("¥" +Utils.getDisplayMoney(listprice.get(i).getValue()));
                                    setprice(listprice.get(i).getValue());
                                }
                            }else {
                                if(amount>=mix && amount<max){
                                    tv_gagueprice.setText("¥" +Utils.getDisplayMoney(listprice.get(i).getValue()));
                                    setprice(listprice.get(i).getValue());
                                }
                            }
                        }
                    }
                }
            }
        });

        image_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectSpecDialog.dismiss();
            }
        });
        tv_gaguecar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Login.Companion.getInstance().isLogin() == false) {
                    goToActivity(LoginActivity.class);
                } else if(mGoodBean.getGoods_storage()==0){
                    goods_shelves.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            DialogUtils.createTipAllTextDialog(GoodsActivity.this,getString(R.string.insufficient_inventory));
                        }
                    },1000);
                } else if(!mGoodBean.isInform_switch()){
                    goods_shelves.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            DialogUtils.createTipAllTextDialog(GoodsActivity.this,getString(R.string.buy_stores));
                        }
                    },1000);
                }else if(mGoodBean.getGoods_state().equals("0")){
                    ToastU.INSTANCE.showToast(getString(R.string.goods_shelves));
                }else {
                    setcar();
                    mSelectSpecDialog.dismiss();
                }

            }
        });
        tv_gagueshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mGoodBean!=null){
                    if (Login.Companion.getInstance().isLogin() == false) {
                        goToActivity(LoginActivity.class);
                    } else if(mGoodBean.getGoods_storage()==0){
                        goods_shelves.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogUtils.createTipAllTextDialog(GoodsActivity.this,getString(R.string.insufficient_inventory));
                            }
                        },1000);
                    } else if(!mGoodBean.isInform_switch()){
                        goods_shelves.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogUtils.createTipAllTextDialog(GoodsActivity.this,getString(R.string.buy_stores));
                            }
                        },1000);
                    }else if(mGoodBean.getGoods_state().equals("0")){
                        ToastU.INSTANCE.showToast(getString(R.string.goods_shelves));
                    }else {
                        requestCompanyList();
                        mSelectSpecDialog.dismiss();
                    }
                }
            }
        });

        tv_gaguequer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag==1){
                    if(mGoodBean.getGoods_storage()==0){
                        goods_shelves.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogUtils.createTipAllTextDialog(GoodsActivity.this,getString(R.string.insufficient_inventory));
                            }
                        },1000);
                        return;
                    }
                    setcar();
                }else if(flag==2){
                    if(mGoodBean.getGoods_storage()==0){
                        goods_shelves.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                DialogUtils.createTipAllTextDialog(GoodsActivity.this,getString(R.string.insufficient_inventory));
                            }
                        },1000);
                        return;
                    }
                    requestCompanyList();
                }
                mSelectSpecDialog.dismiss();
            }
        });

        tv_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactService();
            }
        });

        //商品规格
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_GOODSSPEC())
                .addParam("goods_id", goodsid)
                .withPOST(new NetCallBack<GoodsGagueData>() {

                    @NotNull
                    @Override
                    public Class<GoodsGagueData> getRealType() {
                        return GoodsGagueData.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        ToastU.INSTANCE.showToast(getString(R.string.not_network));
                    }

                    @Override
                    public void onSuccess(@NonNull GoodsGagueData goodsGagueData) {
                        list.clear();
                        String head_name, head_id, id, name;
                        ScreenBean screenBean = null;
                        listdata = new ArrayList<>();
                        if (goodsGagueData != null) {
                            if(goodsGagueData.getSpec_all_info()!=null && goodsGagueData.getSpec_all_info().size()!=0){
                                listdata.addAll(goodsGagueData.getSpec_all_info());
                            }
                            if (goodsGagueData.getSpec_checked() != null && goodsGagueData.getSpec_checked().size()!=0) {
                                for (int i = 0; i < goodsGagueData.getSpec_checked().size(); i++) {
                                    head_id = goodsGagueData.getSpec_checked().get(i).getId() + "";
                                    head_name = goodsGagueData.getSpec_checked().get(i).getName();
                                    ArrayList<ScreenBean.TagInfo> listtag = new ArrayList<>();
                                    for (int j = 0; j < goodsGagueData.getSpec_checked().get(i).getItem().size(); j++) {
                                        id = goodsGagueData.getSpec_checked().get(i).getItem().get(j).getId() + "";
                                        name = goodsGagueData.getSpec_checked().get(i).getItem().get(j).getName();
                                        ScreenBean.TagInfo tagInfo = new ScreenBean.TagInfo(id, name);
                                        listtag.add(tagInfo);
                                        screenBean = new ScreenBean(head_id, head_name, listtag);
                                    }
                                    list.add(screenBean);
                                }
                                goodsgaugeAdapter = new GoodsgaugeAdapter(list, goodsspec_list, GoodsActivity.this);
                                listView.setAdapter(goodsgaugeAdapter);
                                goodsgaugeAdapter.setItemClickListener(GoodsActivity.this);

                            }
                        }
                    }
                }, false);

    }

    //商品数据
    private void getGoodsinfo(String goodsid) {
        DialogUtils.createTipAllLoadDialog(GoodsActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_GOODSDETAILS())
                .addParam("goods_id", goodsid)
                .withPOST(new NetCallBack<GoodsData>() {

                    @NotNull
                    @Override
                    public Class<GoodsData> getRealType() {
                        return GoodsData.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        if(errCode==60009){
                            r1.setVisibility(View.GONE);
                            fragment_cart.setVisibility(View.GONE);
                            relative.setVisibility(View.GONE);
                            relative_invalid_goods.setVisibility(View.VISIBLE);
                            error_retry_view.setVisibility(View.GONE);
                        }else {
                            r1.setVisibility(View.GONE);
                            fragment_cart.setVisibility(View.GONE);
                            relative.setVisibility(View.GONE);
                            relative_invalid_goods.setVisibility(View.GONE);
                            error_retry_view.setVisibility(View.VISIBLE);
                        }
                        DialogUtils.moven();

                    }

                    @Override
                    public void onSuccess(@NonNull GoodsData goodsData) {
                        if(isFinishing()){
                            return;
                        }
                        r1.setVisibility(View.VISIBLE);
                        relative.setVisibility(View.VISIBLE);
                        fragment_cart.setVisibility(View.VISIBLE);
                        error_retry_view.setVisibility(View.GONE);
                        relative_invalid_goods.setVisibility(View.GONE);
                        DialogUtils.moven();
                        if(goodsData.getGoods()!=null){
                            id = goodsData.getGoods().getGoods_id();
                            iscollection = goodsData.getGoods().isLog_id();
                            goods_body = goodsData.getGoods().getGoods_body();
                            goods_attr = goodsData.getGoods().getGoods_attr();
                            mGoodBean = goodsData.getGoods();
                            if(goodsData.getGoods().getGoods_storage()!=0 && goodsData.getGoods().getGoods_storage()>=Integer.parseInt(goodsData.getGoods().getGoods_min_count())){
                                if(!TextUtils.isEmpty(goodsData.getGoods().getGoods_min_count())){
                                    addcarnum=goodsData.getGoods().getGoods_min_count();
                                }
                            }
                            handler.sendEmptyMessage(3);
                        }
                        if(goodsData.getStore()!=null){
                            store_id = goodsData.getStore().getStore_id();
                            storeBean=goodsData.getStore();
                        }
                        listbatchPrice.clear();
                        if(goodsData.getGoods().getBatch_price()!=null){
                            for(int i=0;i<goodsData.getGoods().getBatch_price().size();i++){
                                listbatchPrice.add(goodsData.getGoods().getBatch_price().get(i));
                            }
                        }
                        currencytypeBean=goodsData.getCurrencytype();
                        listlunbo.clear();
                        if (goodsData.getGoods().getGoods_image_mobile() != null) {
                            for (int i = 0; i < goodsData.getGoods().getGoods_image_mobile().size(); i++) {
                                listlunbo.add(goodsData.getGoods().getGoods_image_mobile().get(i));
                            }
                        }
                        goodsspec_list.clear();
                        if (goodsData.getGoods().getGoods_spec() != null && goodsData.getGoods().getGoods_spec().size()!=0) {
                            for (int i = 0; i < goodsData.getGoods().getGoods_spec().size(); i++) {
                                goodsspec_list.add(goodsData.getGoods().getGoods_spec().get(i));
                            }
                            StringBuilder sb = new StringBuilder();
                            for(int i=0;i<goodsspec_list.size();i++){
                                if (sb.length() > 0) {
                                    sb.append(",");
                                }
                                sb.append(goodsspec_list.get(i));
                            }
                            goodsattribute=sb.toString()+",";
                        }
                        if(mGoodBean!=null && storeBean!=null){
                            handler.sendEmptyMessage(1);
                        }

                    }
                }, false);
    }

    //收藏，删除收藏
    private void setcollection(String type) {
        if(!TextUtils.isEmpty(goodsid)) {
            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_FAVORITE())
                    .addParam("fid", goodsid)
                    .addParam("type", type)
                    .addParam("fav_type", "1")
                    .withPOST(new NetCallBack<String>() {

                        @NotNull
                        @Override
                        public Class<String> getRealType() {
                            return String.class;
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(isFinishing()){
                                return;
                            }
                            ToastU.INSTANCE.showToast(getString(R.string.not_network));
                        }

                        @Override
                        public void onSuccess(@NonNull String s) {
                            if(isFinishing()){
                                return;
                            }
                            if (iscollection == false) {
                                iscollection = true;
                                DialogUtils.createTipImageAndTextDialog(GoodsActivity.this, getString(R.string.cancel), R.mipmap.png_icon_popup_ok);
                                Drawable drawableTop = getResources().getDrawable(
                                        R.mipmap.commodity_icon_collection_nor);
                                tv_collecte.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop, null, null);
                            } else {
                                iscollection = false;
                                DialogUtils.createTipImageAndTextDialog(GoodsActivity.this, getString(R.string.successful), R.mipmap.png_icon_popup_ok);
                                Drawable drawableTop = getResources().getDrawable(
                                        R.mipmap.commodity_icon_collection_sel);
                                tv_collecte.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop, null, null);

                            }
                            handler.sendEmptyMessage(2);
                        }
                    }, false);
        }
    }

    //添加购物车
    private void setcar() {
        if(!TextUtils.isEmpty(goodsid)) {
            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ADD_CART())
                    .addParam("goods_id", goodsid)
                    .addParam("quantity", addcarnum)
                    .withPOST(new NetCallBack<String>() {

                        @NotNull
                        @Override
                        public Class<String> getRealType() {
                            return String.class;
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(isFinishing()){
                                return;
                            }
                            ToastU.INSTANCE.showToast(getString(R.string.not_network));
                        }

                        @Override
                        public void onSuccess(@NonNull String string) {
                            if(isFinishing()){
                                return;
                            }
                            DialogUtils.createTipImageAndTextDialog(GoodsActivity.this, getString(R.string.cart_prompt), R.mipmap.png_icon_popup_ok);
                        }
                    }, false);
        }
    }

    //选择采购方式
    private void requestCompanyList() {
        DialogUtils.createTipAllLoadDialog(GoodsActivity.this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ORDER_COMPANY_LIST())
                .withPOST(new NetCallBack<CompanyListBean>() {

                    @NotNull
                    @Override
                    public Class<CompanyListBean> getRealType() {
                        return CompanyListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        ToastU.INSTANCE.showToast(getString(R.string.not_network));
                        return;
                    }

                    @Override
                    public void onSuccess(@NonNull final CompanyListBean companyListBean) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();

                        if(companyListBean.getList()!=null&&companyListBean.getList().size()>1){
                            final CompanyListAdapter companyListAdapter = new CompanyListAdapter(GoodsActivity.this,companyListBean);
                            View view = LayoutInflater.from(GoodsActivity.this).inflate(R.layout.dialog_procurement_method,null);
                            if(popupCompany==null){
                                final int[] selectPosition = {0};
                                companyListBean.getList().get(selectPosition[0]).setSelect(true);
                                company_id=companyListBean.getList().get(selectPosition[0]).getCompany_id();
                                popupCompany = new CommonPopupWindow.Builder(GoodsActivity.this)
                                        .setView(view)
                                        .setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dp2px(getApplicationContext(),330))
                                        .setBackGroundLevel(0.5f)//取值范围0.0f-1.0f 值越小越暗
                                        .create();
                                ListView lv_company_list = view.findViewById(R.id.lv_company_list);
                                Button bt_submit = view.findViewById(R.id.bt_submit);
                                lv_company_list.setAdapter(companyListAdapter);
                                bt_submit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        confirmorder();
                                        popupCompany.dismiss();
                                    }
                                });
                                lv_company_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        if(position!= selectPosition[0]){
                                            companyListBean.getList().get(position).setSelect(true);
                                            companyListBean.getList().get(selectPosition[0]).setSelect(false);
                                            selectPosition[0] =position;
                                            company_id=companyListBean.getList().get(position).getCompany_id();
                                            companyListAdapter.notifyDataSetChanged();
                                        }
                                    }
                                });
                                ImageView iv_close = view.findViewById(R.id.iv_close);
                                iv_close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(popupCompany.isShowing()){
                                            popupCompany.dismiss();
                                        }
                                    }
                                });
                            }
                            Utils.SetWindBg(GoodsActivity.this);
                            popupCompany.showAtLocation(view, Gravity.BOTTOM, 0, 0);
                        }else if(companyListBean.getList()!=null && companyListBean.getList().size()==1){
                            company_id=companyListBean.getList().get(0).getCompany_id();
                            confirmorder();
                        }else if(companyListBean.getList()==null || companyListBean.getList().size()==0){
                            EnterprisePop.showpop(GoodsActivity.this);
                        }
                    }
                }, false);
    }

    //确认订单
    private void confirmorder(){
        if(!TextUtils.isEmpty(company_id) && !TextUtils.isEmpty(cart_str)){
            NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_CONFIRM_ORDER())
                    .addParam("company_id",company_id)
                    .addParam("ifcart","0")
                    .addParam("cart_str",cart_str)
                    .withPOST(new NetCallBack<String>() {

                        @NotNull
                        @Override
                        public Class<String> getRealType() {
                            return String.class;
                        }

                        @Override
                        public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                            if(isFinishing()){
                                return;
                            }
                            ToastU.INSTANCE.showToast(getString(R.string.not_network));
                        }

                        @Override
                        public void onSuccess(@NonNull String s) {
                            if(isFinishing()){
                                return;
                            }
                            Bundle bundle=new Bundle();
                            bundle.putString("order_content",s);
                            goToActivity(ConfirmOrderActivity.class,bundle);
                        }
                    },false);
        }
    }

    //选择规格后的回调刷新
    @Override
    public void onItemClick(com.alibaba.fastjson.JSONObject jsonObject) {
        check_gague="";
        int pot=jsonObject.getInteger("pot");
        String list=jsonObject.getString("list");
        goodsspec_list.remove(pot);
        goodsspec_list.add(pot,list);
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<goodsspec_list.size();i++){
            if (sb.length() > 0) {
                sb.append("|");
            }
            sb.append(goodsspec_list.get(i));
        }
        check_gague=sb.toString();
        for(int i=0;i<listdata.size();i++){
            if(check_gague.equals(listdata.get(i).getGoods_spec())){
                goodsid=listdata.get(i).getGoods_id();
                cart_str=goodsid+"|"+addcarnum;
                if(mGoodBean.getGoods_storage()>0 && mGoodBean.getGoods_storage()>=Integer.parseInt(mGoodBean.getGoods_min_count())){
                    tv_inventory.setText(getString(R.string.goods_inventory_more));
                }else {
                    tv_inventory.setText(getString(R.string.goods_inventory_nor));
                }
                if(!TextUtils.isEmpty(listdata.get(i).getGoods_spec())){
                    goodsattribute=listdata.get(i).getGoods_spec().replace("|",",")+",";
                }
                if(!TextUtils.isEmpty(mGoodBean.getUnit_name())){
                    tv_goodsguige.setText(getString(R.string.goods_selected) + goodsattribute+addcarnum+mGoodBean.getUnit_name());
                }else {
                    tv_goodsguige.setText(getString(R.string.goods_selected) + goodsattribute+addcarnum+getString(R.string.goods_num));
                }

                if(listdata.get(i).getIs_enquiry().equals("0")){
                    tv_gagueprice.setText("¥" +Utils.getDisplayMoney(listdata.get(i).getGoods_price()));
                    tv_gagueprice.setVisibility(View.VISIBLE);
                    tv_goodsinquiry.setVisibility(View.GONE);
                    if(listprice.size()!=0){
                        int mix,max;
                        for(int j=0;j<listprice.size();j++){
                            mix=listprice.get(j).getMix();
                            max=listprice.get(j).getMax();
                            if(j==listprice.size()-1){
                                if(Integer.parseInt(addcarnum)>=mix){
                                    tv_gagueprice.setText("¥" +Utils.getDisplayMoney(listprice.get(j).getValue()));
                                    setprice(listprice.get(i).getValue());
                                }
                            }else {
                                if(Integer.parseInt(addcarnum)>=mix && Integer.parseInt(addcarnum)<=max){
                                    tv_gagueprice.setText("¥" +Utils.getDisplayMoney(listprice.get(j).getValue()));
                                    setprice(listprice.get(j).getValue());
                                }
                            }
                        }
                    }else{
                        setprice(listdata.get(i).getGoods_price());
                    }
                }else {
                    tv_gagueprice.setVisibility(View.GONE);
                    tv_goodsdollar.setVisibility(View.GONE);
                    tv_goodsinquiry.setVisibility(View.VISIBLE);
                }
                amountView.setGoods_storage(listdata.get(i).getGoods_storage());
                if(listdata.get(i).getGoods_storage()<Integer.parseInt(addcarnum)){
                    addcarnum=listdata.get(i).getGoods_storage()+"";
                    amountView.setNum(listdata.get(i).getGoods_storage());
                }
                if(listdata.get(i).getGoods_storage()==0){
                    addcarnum="0";
                    amountView.setNum(0);
                }
                Glide.with(GoodsActivity.this).load(listdata.get(i).getGoods_image()).into(img_goods);
            }
        }
        //刷新商品
        getGoodsinfo(goodsid);
        initfragment();
    }

    //
    private void showpopu(int flag){
        if (Login.Companion.getInstance().isLogin() == false && flag!=0) {
            goToActivity(LoginActivity.class);
        }else if(!mGoodBean.isInform_switch() && flag!=0){
            goods_shelves.postDelayed(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.createTipAllTextDialog(GoodsActivity.this,getString(R.string.buy_stores));
                }
            },1000);
        }else {
            if(mSelectSpecDialog==null){
                View upView = LayoutInflater.from(GoodsActivity.this).inflate(R.layout.goods_specifications_layout, null);
                mSelectSpecDialog = new ButtomDialog(this,upView,true, true);
                WindowManager manager = this.getWindowManager();
                DisplayMetrics outMetrics = new DisplayMetrics();
                manager.getDefaultDisplay().getMetrics(outMetrics);
                int height = outMetrics.heightPixels;
                mSelectSpecDialog.setHeight(height/4*3);
                mSelectSpecDialog.show();
                initpopupwindow(upView,flag);
            }else {
                mSelectSpecDialog.show();
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UMShareAPI.get(this).release();
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    //价格显示
    private void setprice(double money){
        if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.RMB)){
            tv_goodsdollar.setVisibility(View.GONE);
        }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.HKD)){
            currency = money * currencytypeBean.getHKD().getRate();
            price = Utils.getDisplayMoney(currency);
            tv_goodsdollar.setVisibility(View.VISIBLE);
            tv_goodsdollar.setText("(~" + currencytypeBean.getHKD().getSymbol() + price + ")");
        }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.MOP)){
            currency = money * currencytypeBean.getMOP().getRate();
            price = Utils.getDisplayMoney(currency);
            tv_goodsdollar.setVisibility(View.VISIBLE);
            tv_goodsdollar.setText("(~" + currencytypeBean.getMOP().getSymbol() + price + ")");
        }else if(Login.Companion.getInstance().getCurrencyCode().equals(CurrencyActivity.USA)){
            currency = money * currencytypeBean.getUSD().getRate();
            price = Utils.getDisplayMoney(currency);
            tv_goodsdollar.setVisibility(View.VISIBLE);
            tv_goodsdollar.setText("(~" + currencytypeBean.getUSD().getSymbol() + price + ")");
        }
    }

    //分享
    private void setShare(SHARE_MEDIA share_media){
        //判断是否已经赋予权限
        if(Build.VERSION.SDK_INT>=23){
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED&&ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED&&ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                shareUtil(share_media);
            }else {
                String[] mPermissionList = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_LOGS, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.SET_DEBUG_APP,Manifest.permission.SYSTEM_ALERT_WINDOW,Manifest.permission.GET_ACCOUNTS,Manifest.permission.WRITE_APN_SETTINGS};
                ActivityCompat.requestPermissions(this,mPermissionList,123);
            }
        }else {
            shareUtil(share_media);
        }
    }

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {

        }
        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            //Toast.makeText(GoodsActivity.this,"成功了",Toast.LENGTH_LONG).show();
        }
        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            String a=t.getMessage();
            String regEx="[^0-9]";
            Pattern p = Pattern.compile(regEx);
            Matcher m = p.matcher(a);
            String code=m.replaceAll("").trim();
            if(platform==SHARE_MEDIA.DINGTALK && code.equals("2008")){
                DialogUtils.createTipAllTextDialog(GoodsActivity.this,getResources().getString(R.string.share_erro_ding));
            }else if(platform==SHARE_MEDIA.WEIXIN && code.equals("2008")){
                DialogUtils.createTipAllTextDialog(GoodsActivity.this,getResources().getString(R.string.share_erro_weixin));
            }else if(platform==SHARE_MEDIA.WHATSAPP && code.equals("2008")){
                DialogUtils.createTipAllTextDialog(GoodsActivity.this,getResources().getString(R.string.share_erro_whatsapp));
            }
        }
        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            Toast.makeText(GoodsActivity.this,"取消了",Toast.LENGTH_LONG).show();
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==123){
            setShare(mshare_media);
        }
    }

    private void shareUtil(SHARE_MEDIA share_media){
        if(share_media==SHARE_MEDIA.WHATSAPP){
            new ShareAction(GoodsActivity.this)
                    .setPlatform(share_media)
                    .setCallback(shareListener)
                    .withText(mGoodBean.getGoods_share_title())
                    .share();
        }else {
            UMImage thumb =  new UMImage(GoodsActivity.this, listlunbo.get(0));
            UMWeb web = new UMWeb(mGoodBean.getGoods_share());
            web.setTitle(mGoodBean.getGoods_name());//标题
            web.setThumb(thumb);  //缩略图
            web.setDescription(mGoodBean.getGoods_share_title());//描述
            new ShareAction(GoodsActivity.this)
                    .setPlatform(share_media)
                    .setCallback(shareListener)
                    .withMedia(web)
                    .withText(mGoodBean.getGoods_share_title())
                    .share();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(KyyApp.activitynum>1){
            GoodsActivity.this.finish();
        }else {
            goToActivity(MainActivity.class);
            GoodsActivity.this.finish();
        }
    }

    public interface OnGoodsToHomePageListener{
        void onGoodsToHomePage();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event){
        if(event.getMessage().equals(MessageEvent.UNREAD_MSG_NUM)){
            showUnreadMsgNum();
        }
    }
}
