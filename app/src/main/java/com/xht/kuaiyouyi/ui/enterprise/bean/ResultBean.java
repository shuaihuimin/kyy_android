package com.xht.kuaiyouyi.ui.enterprise.bean;

public class ResultBean<T> {


    /**
     * flag : 1
     * err_code : 0
     * msg :
     * data : null
     */

    private int flag;
    private int err_code;
    private String msg;
    private T data;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getErr_code() {
        return err_code;
    }

    public void setErr_code(int err_code) {
        this.err_code = err_code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
