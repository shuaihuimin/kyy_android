package com.xht.kuaiyouyi.ui.mine.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.mine.entity.LogisticsBean;
import com.xht.kuaiyouyi.utils.Utils;

public class LogisticsAdapter extends BaseAdapter {
    private Context mContext;
    private LogisticsBean mLogisticsBean;

    public LogisticsAdapter(Context context, LogisticsBean logisticsBean) {
        this.mContext = context;
        this.mLogisticsBean = logisticsBean;
    }

    @Override
    public int getCount() {
        if (mLogisticsBean.getLogistics_info() != null) {
            return mLogisticsBean.getLogistics_info().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mLogisticsBean.getLogistics_info().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.item_logistics, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_line_top = convertView.findViewById(R.id.tv_line_top);
            viewHolder.tv_line_bottom = convertView.findViewById(R.id.tv_line_bottom);
            viewHolder.tv_status = convertView.findViewById(R.id.tv_status);
            viewHolder.tv_time = convertView.findViewById(R.id.tv_time);
            viewHolder.iv_point = convertView.findViewById(R.id.iv_point);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (position == 0) {
            viewHolder.tv_line_top.setVisibility(View.INVISIBLE);
            viewHolder.iv_point.setImageResource(R.mipmap.purchase_icon_point_sel);
            viewHolder.tv_status.setTextColor(mContext.getResources().getColor(R.color.blue_normal));
        } else {
            viewHolder.tv_line_top.setVisibility(View.VISIBLE);
            viewHolder.iv_point.setImageResource(R.mipmap.purchase_icon_point_nor);
            viewHolder.tv_status.setTextColor(mContext.getResources().getColor(R.color.grey_4));
        }
        if (position == mLogisticsBean.getLogistics_info().size() - 1) {
            viewHolder.tv_line_bottom.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.tv_line_bottom.setVisibility(View.VISIBLE);
        }
        viewHolder.tv_status.setText(mLogisticsBean.getLogistics_info().get(position)
                .getOrder_logistic_text());
        viewHolder.tv_time.setText(Utils.getDisplayDataAndTime(mLogisticsBean.getLogistics_info()
                .get(position).getOrder_logistic_crtime()));
        return convertView;
    }

    private class ViewHolder {
        private TextView tv_line_top, tv_line_bottom, tv_status, tv_time;
        private ImageView iv_point;
    }

}

