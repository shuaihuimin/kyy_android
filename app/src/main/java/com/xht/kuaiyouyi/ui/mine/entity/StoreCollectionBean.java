package com.xht.kuaiyouyi.ui.mine.entity;

import java.util.List;

public class StoreCollectionBean {

    /**
     * flag : 1
     * err_code : 0
     * msg :
     * data : {"favorites_list":[{"fav_id":"1","fav_time":"1524119073","fav_type":"store","store_id":"1","store_name":"快优易自营","fav_num":3,"img":"http://192.168.0.2/data/upload/shop/store/05773858668529672_sm.jpg"}]}
     */
        private List<FavoritesListBean> favorites_list;
        private String kyy;

        public List<FavoritesListBean> getFavorites_list() {
            return favorites_list;
        }

        public void setFavorites_list(List<FavoritesListBean> favorites_list) {
            this.favorites_list = favorites_list;
        }

        public static class FavoritesListBean {
            /**
             * fav_id : 1
             * fav_time : 1524119073
             * fav_type : store
             * store_id : 1
             * store_name : 快优易自营
             * fav_num : 3
             * img : http://192.168.0.2/data/upload/shop/store/05773858668529672_sm.jpg
             */

            private String fav_id;
            private String fav_time;
            private String fav_type;
            private String store_id;
            private String store_name;
            private int fav_num;
            private String img;
            private String is_second_hand;


            public String getIs_second_hand() {
                return is_second_hand;
            }

            public void setIs_second_hand(String is_second_hand) {
                this.is_second_hand = is_second_hand;
            }

            public String getFav_id() {
                return fav_id;
            }

            public void setFav_id(String fav_id) {
                this.fav_id = fav_id;
            }

            public String getFav_time() {
                return fav_time;
            }

            public void setFav_time(String fav_time) {
                this.fav_time = fav_time;
            }

            public String getFav_type() {
                return fav_type;
            }

            public void setFav_type(String fav_type) {
                this.fav_type = fav_type;
            }

            public String getStore_id() {
                return store_id;
            }

            public void setStore_id(String store_id) {
                this.store_id = store_id;
            }

            public String getStore_name() {
                return store_name;
            }

            public void setStore_name(String store_name) {
                this.store_name = store_name;
            }

            public int getFav_num() {
                return fav_num;
            }

            public void setFav_num(int fav_num) {
                this.fav_num = fav_num;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }
        }

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    @Override
    public String toString() {
        return "StoreCollectionBean{" +
                "favorites_list=" + favorites_list +
                ", kyy='" + kyy + '\'' +
                '}';
    }
}
