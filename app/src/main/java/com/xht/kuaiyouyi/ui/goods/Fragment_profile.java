package com.xht.kuaiyouyi.ui.goods;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.adapter.StoreProfileAdapter;
import com.xht.kuaiyouyi.ui.goods.entity.StoreProfileData;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 *店铺-公司档案
 */
public class Fragment_profile extends BaseFragment {
    private StoreProfileAdapter adapter;
    private String store_id;
    private StoreProfileData storeProfile;
    private List<StoreProfileData.ImageArrBean> list;
    private List<StoreProfileData> listdata = new ArrayList<>();
    @BindView(R.id.tv_content)
    TextView tv_content;
    @BindView(R.id.tv_more)
    TextView tv_more;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.linear)
    LinearLayout linear_more;
    @BindView(R.id.linear_hot)
    LinearLayout linear_hot;
    @BindView(R.id.nestedscrollview)
    NestedScrollView nestedscrollview;
    @BindView(R.id.linear1)
    LinearLayout linear1;
    @BindView(R.id.linear2)
    LinearLayout linear2;

    private boolean isOpen=false;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(storeProfile.getInfo()!=null){
                linear1.setVisibility(View.VISIBLE);
                if(TextUtils.isEmpty(storeProfile.getInfo().getStore_name()) && TextUtils.isEmpty(storeProfile.getInfo().getDetail_content())){
                    linear1.setVisibility(View.GONE);
                }
                if(!TextUtils.isEmpty(storeProfile.getInfo().getStore_name())){
                    tv_title.setText(storeProfile.getInfo().getStore_name());
                }
                if(!TextUtils.isEmpty(storeProfile.getInfo().getDetail_content())){
                    tv_content.setText(Html.fromHtml(storeProfile.getInfo().getDetail_content()));
                }

                Log.i("info","---------"+tv_content.getWidth());
                if(!TextUtils.isEmpty(storeProfile.getInfo().getDetail_content()) && storeProfile.getInfo().getDetail_content().length()>75){
                    tv_content.setLines(3);
                    linear_more.setVisibility(View.VISIBLE);
                    settext();
                }

            }
            if(list!=null && list.size()!=0){
                linear2.setVisibility(View.VISIBLE);

                for (int i = 0; i < list.size(); i++) {
                    View view = View.inflate(getActivity(), R.layout.item_fragment_profile_hot, null);
                    ImageView imageView = (ImageView) view.findViewById(R.id.iv_image);
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    imageView.setAdjustViewBounds(true);
                    Glide.with(getActivity()).load(list.get(i).getImage())
                            .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                            .into(imageView);
                    TextView textView = (TextView) view.findViewById(R.id.tv_name);
                    textView.setText(list.get(i).getImage_name());
                    final String img=list.get(i).getImage();
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle=new Bundle();
                            bundle.putInt("pos",0);
                            bundle.putString("img",img);
                            goToActivity(ImagesDisplayActivity.class,bundle);
                        }
                    });
                    linear_hot.addView(view);
                }
            }
        }
    };

    public static Fragment_profile newInstance(String storeid) {
        Bundle args = new Bundle();
        args.putString("store_id", storeid);
        Fragment_profile fragment = new Fragment_profile();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        store_id = getArguments().getString("store_id");
        getinit(store_id);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_profile_layout;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.i("info","--------zlm");
        EventBus.getDefault().post(new MessageEvent(MessageEvent.STORE_UP));
    }

    private void getinit(String store_id) {
        DialogUtils.createTipAllLoadDialog(getActivity());
        NetUtil.Companion.getInstance()
                .url(KyyConstants.INSTANCE.getURL_STORE_PROFILE())
                .addParam("store_id", store_id)
                .withPOST(new NetCallBack<StoreProfileData>() {

                    @NotNull
                    @Override
                    public Class<StoreProfileData> getRealType() {
                        return StoreProfileData.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        DialogUtils.moven();
                        DialogUtils.createTipImageAndTextDialog(getActivity(), "加载失败", R.mipmap.png_icon_popup_wrong);
                    }

                    @Override
                    public void onSuccess(@NonNull StoreProfileData storeProfileData) {
                        if(getActivity()==null){
                            return;
                        }
                        DialogUtils.moven();
                        storeProfile = storeProfileData;
                        listdata.add(storeProfileData);
                        list = new ArrayList<>();
                        if(storeProfileData.getImage_arr()!=null && storeProfileData.getImage_arr().size()!=0){
                            list.addAll(storeProfileData.getImage_arr());
                        }
                        handler.sendEmptyMessage(0);


                    }
                }, false);
    }

    private void settext() {
        //1.3.7折叠字体
        tv_content.post(new Runnable() {
            @Override
            public void run () {
                int tvLine = tv_content.getLineCount();
                //Log.e("txtPart", "run: " + txtPart);
                if (tvLine > 3) {
                    tv_more.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (isOpen) {
                                tv_content.setMaxLines(3);
                                isOpen = false;
                                tv_more.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.introduce_icon_open_nor),null);
                            } else {
                                tv_content.setMaxLines(Integer.MAX_VALUE);
                                isOpen = true;
                                tv_more.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_close_nor),null);
                            }
                        }
                    });
                }
            }
        });
    }
}
