package com.xht.kuaiyouyi.ui.goods;

import android.os.Bundle;
import android.view.View;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseFragment;

public class Fragment_purchase extends BaseFragment {

    public static Fragment_purchase newInstance(){
        Fragment_purchase fragment = new Fragment_purchase();
        return fragment;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_purchase_layout;
    }
}