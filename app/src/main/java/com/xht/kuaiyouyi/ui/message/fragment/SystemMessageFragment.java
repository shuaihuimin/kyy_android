package com.xht.kuaiyouyi.ui.message.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.MainActivity;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.cart.order.OrderDetilsActivity;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.message.adapter.SystemMessageListAdapter;
import com.xht.kuaiyouyi.ui.message.bean.SystemMessageListBean;
import com.xht.kuaiyouyi.ui.message.util.MessageUtil;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.widget.DeletePopupWindow;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/**
 * 跳订单详情页，跳商品详情页，跳企业首页，不跳转
 */
public class SystemMessageFragment extends BaseFragment {
    @BindView(R.id.lv_content)
    ListView lv_content;
    @BindView(R.id.srl_loadmore)
    SmartRefreshLayout srl_loadmore;
    @BindView(R.id.rl_empty_message_list)
    RelativeLayout rl_empty_message_list;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;

    private String mKyy = "";
    private SystemMessageListBean mSystemMessageListBean;
    private SystemMessageListAdapter mAdapter;

    private int mReadNum;//已读数
    private OnSystemMessageReadNumUpdate mCallback;

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        lv_content.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int type = mSystemMessageListBean.getArr().get(position).getType();
                if (type != 0) {
                    //可以跳转的情况
                    if (mSystemMessageListBean.getArr().get(position).getMessage_open() == 0) {
                        MessageUtil.requestReadSystemMessage(mSystemMessageListBean.getArr().get(position).getMessage_id());
                        mSystemMessageListBean.getArr().get(position).setMessage_open(1);
                        mAdapter.notifyDataSetChanged();
                        if (type == 1 || type == 2) {
                            mCallback.onSystemMessageReadUpdate(1);
                        }
                    }

                    Bundle bundle = new Bundle();
                    String value_id = mSystemMessageListBean.getArr().get(position).getValue_id();
                    if (!TextUtils.isEmpty(value_id)) {
                        if (type == 1) {
                            //跳转到订单详情
                            Intent intent = new Intent(getContext(), OrderDetilsActivity.class);
                            intent.putExtra("order_main_id",value_id);
                            startActivity(intent);
                        } else if (type == 2) {
                            //跳转到商品详情
                            bundle.putString("goods_id", value_id);
                            goToActivity(GoodsActivity.class, bundle);
                        } else if (type == 3) {
                            //跳转到企业首页
                            Login.Companion.getInstance().setEnterprise_company_id(value_id);
                            bundle.putBoolean(VerifyMessageFragment.TO_COMPANY_INDEX, true);
                            goToActivity(MainActivity.class, bundle);
                        }
                    }

                }
            }
        });

        lv_content.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new DeletePopupWindow(getContext(), view).show().setOnClickDeleteListener(new DeletePopupWindow.OnClickDeleteListener() {
                    @Override
                    public void onDelete() {
                        requestDeleteMsg(position);
                    }
                });
                return true;
            }
        });

        srl_loadmore.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                requesSystemMessageList();
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requesSystemMessageList();
            }
        });
    }

    private void requesSystemMessageList() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_SYSTEM_MESSAGE_LIST())
                .withLoadPOST(new NetCallBack<SystemMessageListBean>() {
                    @NotNull
                    @Override
                    public Class<SystemMessageListBean> getRealType() {
                        return SystemMessageListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        if(TextUtils.isEmpty(mKyy)){
                            error_retry_view.setVisibility(View.VISIBLE);
                            rl_empty_message_list.setVisibility(View.GONE);
                        }else {
                            Toast.makeText(getContext(), getString(R.string.not_network), Toast.LENGTH_SHORT).show();
                        }
                        if (srl_loadmore.getState() == RefreshState.Loading) {
                            srl_loadmore.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull SystemMessageListBean systemMessageListBean) {
                        if(getActivity()==null){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        if (TextUtils.isEmpty(mKyy)) {
                            mKyy = systemMessageListBean.getKyy();
                            if (systemMessageListBean.getArr() == null || systemMessageListBean.getArr().size() == 0) {
                                rl_empty_message_list.setVisibility(View.VISIBLE);
                                return;
                            }
                            srl_loadmore.setEnableLoadMore(true);
                            mSystemMessageListBean = systemMessageListBean;
                            mAdapter = new SystemMessageListAdapter(getContext(), mSystemMessageListBean);
                            lv_content.setAdapter(mAdapter);
                        } else {
                            srl_loadmore.finishLoadMore();
                            if (systemMessageListBean.getArr() != null && systemMessageListBean.getArr().size() > 0) {
                                mSystemMessageListBean.getArr().addAll(systemMessageListBean.getArr());
                                mAdapter.notifyDataSetChanged();
                                mKyy = systemMessageListBean.getKyy();
                            } else {
                                srl_loadmore.setNoMoreData(true);
                            }
                        }

                    }
                }, false, mKyy);
    }

    private void requestDeleteMsg(final int position) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_DELETE_SYSTEM_MSG())
                .addParam("message_id", mSystemMessageListBean.getArr().get(position).getMessage_id())
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                        if (srl_loadmore.getState() == RefreshState.Loading) {
                            srl_loadmore.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(getActivity()==null){
                            return;
                        }
                        if (srl_loadmore.getState() == RefreshState.Loading) {
                            srl_loadmore.finishLoadMore();
                        }
                        DialogUtils.createTipImageAndTextDialog(getContext(),
                                getContext().getString(R.string.delete_succeful),
                                R.mipmap.png_icon_popup_ok);
                        mSystemMessageListBean.getArr().remove(position);
                        mAdapter.notifyDataSetChanged();
                        if (mSystemMessageListBean.getArr().size() == 0) {
                            rl_empty_message_list.setVisibility(View.VISIBLE);
                        }
                    }
                }, false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //第一次显示在用户面前的时候
        if (isVisibleToUser) {
            if (TextUtils.isEmpty(mKyy)) {
                requesSystemMessageList();
            }
        }
        //第一次隐藏在用户面前
        if (!isVisibleToUser && !TextUtils.isEmpty(mKyy)) {
            if (mSystemMessageListBean != null && mSystemMessageListBean.getArr() != null) {
                mReadNum = 0;
                for (SystemMessageListBean.ArrBean bean : mSystemMessageListBean.getArr()) {
                    if (bean.getType() == 0) {
                        //不跳转的情况
                        if (bean.getMessage_open() == 0) {
                            bean.setMessage_open(1);
                            mReadNum++;
                        }
                    }
                }
                if (mReadNum > 0) {
                    mCallback.onSystemMessageReadUpdate(mReadNum);
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_message;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnSystemMessageReadNumUpdate) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnVerifyMessageReadNumUpdate");
        }
    }

    public interface OnSystemMessageReadNumUpdate {
        //已读数更新
        void onSystemMessageReadUpdate(int num);
    }

}
