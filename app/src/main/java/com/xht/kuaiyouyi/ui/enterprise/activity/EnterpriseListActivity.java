package com.xht.kuaiyouyi.ui.enterprise.activity;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.enterprise.adapter.MyEnterpriseListsAdapter;
import com.xht.kuaiyouyi.ui.enterprise.bean.UserCompanies;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.utils.Login;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;

public class EnterpriseListActivity extends BaseActivity {
    @BindView(R.id.iv_left)
    ImageView iv_left;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_right)
    TextView tv_right;
    //企业列表
    @BindView(R.id.lv_enterprise_list)
    ListView lv_enterprise_list;

    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;

    private List<UserCompanies.CompaniesBean> mCompaniesBeans;
    private MyEnterpriseListsAdapter mAdapter;


    @Override
    protected int getLayout() {
        return R.layout.activity_enterprise_list;
    }

    @Override
    protected void initView() {
        requestEnterpriseList();
        iv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_title.setText(R.string.enterprise_my);
        tv_right.setText(R.string.enterprise_append);
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(SearchEnterpriseActivity.class);
            }
        });

        lv_enterprise_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Login.Companion.getInstance().setEnterprise_company_id(mCompaniesBeans.get(position).getCompany_id());
                setResult(RESULT_OK);
                finish();
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestEnterpriseList();
            }
        });

    }

    private void requestEnterpriseList() {
        DialogUtils.createTipAllLoadDialog(this);
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_EP_LIST())
                .withPOST(new NetCallBack<UserCompanies>() {
                    @NotNull
                    @Override
                    public Class<UserCompanies> getRealType() {
                        return UserCompanies.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        //Toast.makeText(EnterpriseListActivity.this, err, Toast.LENGTH_SHORT).show();
                        error_retry_view.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onSuccess(@NonNull UserCompanies userCompanies) {
                        if(isFinishing()){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        DialogUtils.moven();
                        if (userCompanies.getCompanies() != null) {
                            mCompaniesBeans = userCompanies.getCompanies();
                            mAdapter = new MyEnterpriseListsAdapter(EnterpriseListActivity.this, mCompaniesBeans);
                            lv_enterprise_list.setAdapter(mAdapter);
                        }
                    }
                }, false);
    }
}
