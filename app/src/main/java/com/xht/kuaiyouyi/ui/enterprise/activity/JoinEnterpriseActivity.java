package com.xht.kuaiyouyi.ui.enterprise.activity;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class JoinEnterpriseActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_company_name)
    TextView tv_company_name;
    @BindView(R.id.iv_company_icon)
    ImageView iv_company_icon;
    @BindView(R.id.et_user_name)
    EditText et_user_name;
    @BindView(R.id.et_reason)
    EditText et_reason;
    @BindView(R.id.bt_enterprise_commit)
    Button bt_enterprise_commit;

    public static final String COMPANY_NAME = "company_name";
    public static final String COMPANY_ID = "company_id";
    public static final String COMPANY_LOGO = "company_logo";

    private String mCompanyName;
    private String mCompanyId;
    private String mCompanyLogo;

    @Override
    protected int getLayout() {
        return R.layout.activity_enterprise_join;
    }

    @Override
    protected void initView() {
        mCompanyName = getIntent().getStringExtra(COMPANY_NAME);
        mCompanyId = getIntent().getStringExtra(COMPANY_ID);
        mCompanyLogo = getIntent().getStringExtra(COMPANY_LOGO);
        Glide.with(this).load(mCompanyLogo)
                .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                .into(iv_company_icon);
        tv_title.setText(R.string.enterprise_apply_join);
        tv_company_name.setText(mCompanyName);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        bt_enterprise_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = et_user_name.getText().toString().trim();
                String reason = et_reason.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(getApplicationContext(), R.string.name_empty_tip, Toast.LENGTH_SHORT).show();
                    return;
                }
                requestSavejoin(name, reason);
            }
        });

        et_user_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setCommitBtnEnable();
            }
        });
    }

    private void setCommitBtnEnable() {
        if (et_user_name.getText().toString().trim().length() > 0) {
            bt_enterprise_commit.setEnabled(true);
        } else {
            bt_enterprise_commit.setEnabled(false);
        }
    }

    private void requestSavejoin(String name, String reason) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_EP_SAVEJOIN())
                .addParam("company_id", mCompanyId)
                .addParam("reason", reason)
                .addParam("name", name)
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.createTipImageAndTextDialog(
                                JoinEnterpriseActivity.this, err,
                                R.mipmap.pop_icon_waring);
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.createOneBtnDialog(JoinEnterpriseActivity.this
                                , getString(R.string.enterprise_join_send_successful)
                                , getString(R.string.dialog_confirm), new DialogUtils.OnLeftBtnListener() {
                                    @Override
                                    public void setOnLeftListener(Dialog dialog) {
                                        tv_company_name.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                finish();
                                            }
                                        },500);
                                    }
                                }
                                , true, true);
                    }
                }, false);
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(et_user_name.getText().toString().trim())) {
            DialogUtils.createTwoBtnDialog(this,
                    getResources().getString(R.string.enterprise_goback_tip),
                    getResources().getString(R.string.dialog_confirm),
                    getResources().getString(R.string.dialog_cancel),
                    new DialogUtils.OnRightBtnListener() {
                        @Override
                        public void setOnRightListener(Dialog dialog) {
                            finish();
                        }
                    }, new DialogUtils.OnLeftBtnListener() {
                        @Override
                        public void setOnLeftListener(Dialog dialog) {

                        }
                    }, false, false);
        } else {
            finish();
        }

    }
}
