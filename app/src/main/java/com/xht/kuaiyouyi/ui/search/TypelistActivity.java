package com.xht.kuaiyouyi.ui.search;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.classic.common.MultipleStatusView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.home.activity.SearchGoodsActivity;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.MessageActivity;
import com.xht.kuaiyouyi.ui.search.adapter.SectionedSpanSizeLookup;
import com.xht.kuaiyouyi.ui.search.adapter.SortingAdapter;
import com.xht.kuaiyouyi.ui.search.adapter.TypelistAdapter;
import com.xht.kuaiyouyi.ui.search.entity.ScreenBean;
import com.xht.kuaiyouyi.ui.search.entity.TypelistData;
import com.xht.kuaiyouyi.ui.search.entity.TypelistScreenData;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.BadgeView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by shuaihuimin on 2018/6/27.
 * 分类商品综合筛选
 */

public class TypelistActivity extends BaseActivity implements SortingAdapter.OnItemClickListener{
    @BindView(R.id.btn_back)
    ImageView imageback;
    @BindView(R.id.home_icon_message_nor)
    ImageView iv_message;
    @BindView(R.id.tvSearch)
    TextView searchview;
    @BindView(R.id.iv_switch)
    ImageView iv_switch;
    @BindView(R.id.radioGroup_fragmenthome)
    RadioGroup radioGroup;
    @BindView(R.id.rb_zonghe)
    RadioButton rb_zonghe;
    @BindView(R.id.rb_xiaoliang)
    RadioButton rb_xiaoliang;
    @BindView(R.id.rb_price)
    RadioButton rb_price;
    @BindView(R.id.rb_scree)
    TextView rb_scree;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.right_view)
    RelativeLayout rigthview;
    @BindView(R.id.typelist_recycler)
    RecyclerView typelistrecycler;
    @BindView(R.id.typeright_recycler)
    RecyclerView typerightrecycler;
    @BindView(R.id.tv_chongzi)
    TextView tv_chongzi;
    @BindView(R.id.tv_queren)
    TextView tv_queren;
    @BindView(R.id.edit_mini)
    EditText edit_mini;
    @BindView(R.id.edit_max)
    EditText edit_max;
    @BindView(R.id.mRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.multipleStatusView)
    MultipleStatusView multipleStatusView;

    private BadgeView mBadgeView;
    private List<TypelistData.GoodsListBean.ItemBean> list=new ArrayList<TypelistData.GoodsListBean.ItemBean>();
    private TypelistAdapter typelistAdapter;
    private List<TypelistData.CurrencytypeBean> currlist=new ArrayList<>();

    private SortingAdapter sortingAdapter;
    private List<ScreenBean> listright=new ArrayList<>();
    private List<String> type_list=new ArrayList<>();
    private List<String> brand_list=new ArrayList<>();
    private String check_brand="";
    private String check_attr="";
    private String order_keyword="goods_id";
    private String keyword;
    private int is_second_hand=0;
    private String order="desc";
    private Boolean b=false;
    private String cate_id;
    private String cateid;
    private int flag=0;
    private String min;
    private String max;
    private int img_heghit;

    public final static String KEYWORD = "keyword";
    public final static String CATE_ID="cate_id";
    public final static String Is_second_hand="is_second_hand";

    private boolean isrefresh;
    private String kyy="";
    private int state=0;
    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(list.size()==0){
                multipleStatusView.showEmpty();
                multipleStatusView.setClickable(false);
            }
        }
    };

    @Override
    protected int getLayout() {
        return R.layout.typelist_layout;
    }

    @Override
    protected void initView() {
        mBadgeView = new BadgeView(this,iv_message,10,4);
        EventBus.getDefault().register(this);
        img_heghit=(Utils.getWindowWidth(TypelistActivity.this)-8)/2;
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.RIGHT);
        typelistrecycler.setLayoutManager(new LinearLayoutManager(TypelistActivity.this));
        typelistAdapter=new TypelistAdapter(TypelistActivity.this,flag,R.layout.store_recycler_item_layout,list,currlist,img_heghit);
        typelistrecycler.setAdapter(typelistAdapter);

        //右侧边栏筛选列表
        sortingAdapter = new SortingAdapter(this,listright,1);
        GridLayoutManager manager1 = new GridLayoutManager(this,3);
        //设置header
        manager1.setSpanSizeLookup(new SectionedSpanSizeLookup(sortingAdapter,manager1));
        typerightrecycler.setLayoutManager(manager1);
        typerightrecycler.setAdapter(sortingAdapter);
        sortingAdapter.setItemClickListener(this);
        cate_id=getIntent().getStringExtra(CATE_ID);
        Log.i("info","------cate"+cate_id);
        keyword=getIntent().getStringExtra(KEYWORD);
        is_second_hand=getIntent().getIntExtra(Is_second_hand,0);
        if(!TextUtils.isEmpty(keyword)){
            searchview.setText(keyword);
        }
        getinfo(check_brand,check_attr,order_keyword,order,is_second_hand);
        click();
        initlode();
        initrefresh();
    }

    //商品列表
    private void getinfo(final String brand_id, final String type_id, final String order_keyword, final String order,int is_second){
        if(kyy.equals("")){
            multipleStatusView.showLoading();
        }
        if(!TextUtils.isEmpty(edit_max.getText()) && !TextUtils.isEmpty(edit_mini.getText())){
            double big=Double.parseDouble(edit_max.getText().toString());
            double small=Double.parseDouble(edit_mini.getText().toString());
            if(small>big){
                edit_max.setText(small+"");
                edit_mini.setText(big+"");
            }
        }
         min=edit_mini.getText().toString();
         max=edit_max.getText().toString();

        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_GOODSLIST())
                .addParam("check_brand",brand_id==null?"":brand_id)
                .addParam("keyword",keyword==null?"":keyword)
                .addParam("gc_id",cate_id==null?"":cate_id)
                .addParam("check_attr",type_id==null?"":type_id)
                .addParam("order_keyword",order_keyword)
                .addParam("order",order)
                .addParam("price_min",min==null?"":min)
                .addParam("price_max",max==null?"":max)
                .addParam("is_second_hand",is_second)
                .withLoadPOST(new NetCallBack<TypelistData>() {

                    @NotNull
                    @Override
                    public Class<TypelistData> getRealType() {
                        return TypelistData.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        if(TextUtils.isEmpty(kyy)){
                            multipleStatusView.showError();
                            multipleStatusView.setClickable(true);
                        }else {
                            ToastU.INSTANCE.showToast(errCode + "--" + getString(R.string.not_network));
                        }

                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Refreshing){
                            smartRefreshLayout.finishRefresh();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull TypelistData typelistData) {
                        if(isFinishing()){
                            return;
                        }
                        currlist.clear();
                        multipleStatusView.showContent();
                        if(smartRefreshLayout.getState() == RefreshState.Refreshing){
                            smartRefreshLayout.finishRefresh();
                        }
                        if(smartRefreshLayout.getState() == RefreshState.Loading){
                            smartRefreshLayout.finishLoadMore();
                        }
                        if(typelistData.getGoods_list()!=null && typelistData.getGoods_list().getItem().size()!=0){
                            currlist.add(typelistData.getCurrencytype());
                            list.addAll(typelistData.getGoods_list().getItem());
                            cate_id=typelistData.getGc_id();
                            Log.i("info","------zzzz4"+cate_id);
                        }else {
                            if(!TextUtils.isEmpty(kyy)){
                                smartRefreshLayout.finishLoadMoreWithNoMoreData();
                                smartRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
                            }
                        }
                        kyy=typelistData.getKyy();
                        typelistAdapter.notifyDataSetChanged();
                        handler.sendEmptyMessage(0);
                        if(state==0){
                            Log.i("info","------zzzz3");
                            getscreeninfo(cate_id);
                        }
                    }
                },false,kyy);
    }

    //商品筛选
    private void getscreeninfo(String cate_id){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_GET_GOODSCEEN())
                .addParam("cate_id",cate_id==null?"":cate_id)
                .addParam("keyword",keyword==null?"":keyword)
                .withPOST(new NetCallBack<TypelistScreenData>() {

                    @NotNull
                    @Override
                    public Class<TypelistScreenData> getRealType() {
                        return TypelistScreenData.class;

                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        ToastU.INSTANCE.showToast(err);
                    }

                    @Override
                    public void onSuccess(@NonNull TypelistScreenData typelistScreenData) {
                        if(isFinishing()){
                            return;
                        }
                        listright.clear();
                        state=1;
                        if(typelistScreenData==null){
                            return;
                        }
                        String head_id=null,head_name=null,id = null,name=null;
                        ScreenBean screenBean2=null;
                        if(typelistScreenData.getBrand_array()!=null && typelistScreenData.getBrand_array().size()!=0){
                            head_name="品牌";
                            ArrayList<ScreenBean.TagInfo> listtag = new ArrayList<>();
                            for(int i=0;i<typelistScreenData.getBrand_array().size();i++){
                                id=typelistScreenData.getBrand_array().get(i).getBrand_id();
                                name=typelistScreenData.getBrand_array().get(i).getBrand_name();
                                ScreenBean.TagInfo tagInfo1=new ScreenBean.TagInfo(id,name);
                                listtag.add(tagInfo1);
                                screenBean2=new ScreenBean(head_id,head_name, listtag);
                            }
                            listright.add(screenBean2);
                        }
                        if(typelistScreenData.getAttr_array()!=null && typelistScreenData.getAttr_array().size()!=0){
                               for(int i=0;i<typelistScreenData.getAttr_array().size();i++){
                                   head_name=typelistScreenData.getAttr_array().get(i).getName();
                                   ArrayList<ScreenBean.TagInfo> listtag = new ArrayList<>();
                                   if(typelistScreenData.getAttr_array().get(i).getValue()!=null && typelistScreenData.getAttr_array().get(i).getValue().size()!=0){
                                       for(int j=0;j<typelistScreenData.getAttr_array().get(i).getValue().size();j++){
                                               name=typelistScreenData.getAttr_array().get(i)
                                                       .getValue().get(j).getAttr_value_name();
                                               id=typelistScreenData.getAttr_array().get(i)
                                                       .getValue().get(j).getAttr_value_id();
                                               ScreenBean.TagInfo tagInfo1=new ScreenBean.TagInfo(id,name);
                                               listtag.add(tagInfo1);
                                               screenBean2=new ScreenBean(head_id,head_name, listtag);
                                       }
                                   }else {
                                       screenBean2=new ScreenBean(head_id,head_name, listtag);
                                   }

                                   listright.add(screenBean2);
                               }
                        }
                        sortingAdapter.notifyDataSetChanged();
                    }

                },false);
    }

    private void click(){
        multipleStatusView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getinfo(check_brand,check_attr,order_keyword,order,is_second_hand);
            }
        });
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                rb_zonghe.setEnabled(true);
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                rb_zonghe.setEnabled(true);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        searchview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cate_id="";
                kyy="";
                list.clear();
                Bundle bundle=new Bundle();
                bundle.putString(CATE_ID,"");
                bundle.putInt("flag",1);
                goToActivity(SearchGoodsActivity.class,bundle);
            }
        });
        imageback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TypelistActivity.this.finish();

            }
        });

        rb_scree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //rb_scree.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_screen_sel),null);
                drawer.openDrawer(rigthview);
            }
        });

        tv_chongzi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelect();
            }
        });
        tv_queren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kyy="";
                list.clear();
                order_keyword="goods_id";
                order="desc";
                getinfo(check_brand,check_attr,order_keyword,order,is_second_hand);
                Log.i("info","-----type_id"+"cate"+cate_id);
                drawer.closeDrawer(rigthview);
//                rb_zonghe.setEnabled(true);
            }
        });
        rb_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kyy="";
                list.clear();
                order_keyword="goods_price";
                rb_scree.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_screen_nor),null);
                if(b==false){
                    b=true;
                    rb_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.shop_icon_pricedown_sel),null);
                    order="asc";
                } else{
                    b =false;
                    rb_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.shop_icon_priceup_sel),null);
                    order="desc";
                }

                getinfo(check_brand,check_attr,order_keyword,order,is_second_hand);

            }
        });
        rb_xiaoliang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b){
                    b=false;
                }else {
                    b=true;
                }
                kyy="";
                list.clear();
                order_keyword="goods_salenum";
                order="desc";
                rb_scree.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_screen_nor),null);
                rb_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_price_nor),null);
                getinfo(check_brand,check_attr,order_keyword,order,is_second_hand);
            }
        });

        rb_zonghe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(b){
                    b=false;
                }else {
                    b=true;
                }
                kyy="";
                list.clear();
                order_keyword="goods_id";
                order="desc";
                rb_scree.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_screen_nor),null);
                rb_price.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.mipmap.search_icon_price_nor),null);
                getinfo(check_brand,check_attr,order_keyword,order,is_second_hand);
            }
        });

        iv_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag==0){
                    flag=1;
                    iv_switch.setImageResource(R.mipmap.search_icon_list_nor);
                    GridLayoutManager manager = new GridLayoutManager(TypelistActivity.this, 2,GridLayoutManager.VERTICAL,false);
                    typelistrecycler.setLayoutManager(manager);
                    typelistAdapter=new TypelistAdapter(TypelistActivity.this,flag,R.layout.home_recycler_item_layout,list,currlist,img_heghit);
                    typelistrecycler.setAdapter(typelistAdapter);
                }else {
                    flag=0;
                    iv_switch.setImageResource(R.mipmap.search_icon_palace_nor);
                    typelistrecycler.setLayoutManager(new LinearLayoutManager(TypelistActivity.this));
                    typelistAdapter=new TypelistAdapter(TypelistActivity.this,flag,R.layout.store_recycler_item_layout,list,currlist,img_heghit);
                    typelistrecycler.setAdapter(typelistAdapter);
                }
            }
        });
        iv_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.Companion.getInstance().isLogin()){
                    goToActivity(MessageActivity.class);
                }else {
                    goToActivity(LoginActivity.class);
                }
            }
        });

        edit_mini.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //判断是否是“完成”键
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    //隐藏软键盘
                    InputMethodManager imm = (InputMethodManager) v
                            .getContext().getSystemService(
                                    Context.INPUT_METHOD_SERVICE);
                    if (imm.isActive()) {
                        imm.hideSoftInputFromWindow(
                                v.getApplicationWindowToken(), 0);
                    }
                    return true;
                }
                return false;
            }
        });

        edit_max.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //判断是否是“完成”键
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    //隐藏软键盘
                    InputMethodManager imm = (InputMethodManager) v
                            .getContext().getSystemService(
                                    Context.INPUT_METHOD_SERVICE);
                    if (imm.isActive()) {
                        imm.hideSoftInputFromWindow(
                                v.getApplicationWindowToken(), 0);
                    }
                    return true;
                }
                return false;
            }
        });


    }

    @Override
    public void onItemClick(JSONObject jsonObject) {
        int section = jsonObject.getInteger("section");
        int position = jsonObject.getInteger("position");
        boolean b = listright.get(section).getTagInfo().get(position).isSelect() == true ? false : true;
        listright.get(section).getTagInfo().get(position).setSelect(b);
        sortingAdapter.notifyDataSetChanged();
        String id = listright.get(section).getTagInfo().get(position).getId();
        if (section == 0){
            check_brand="";
            if(b){
                brand_list.add(id);
            }else {
                brand_list.remove(id);
            }
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<brand_list.size();i++){
                if (sb.length() > 0) {//该步即不会第一位有逗号，也防止最后一位拼接逗号！
                    sb.append(",");
                }
                sb.append(brand_list.get(i));
            }
            check_brand=sb.toString();
            Log.i("info","-----check_brand"+check_brand);
        }else{
            check_attr="";
            if(b){
                type_list.add(id);
            }else {
                type_list.remove(id);
            }
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<type_list.size();i++){
                if (sb.length() > 0) {
                    sb.append("_");
                }
                sb.append(type_list.get(i));
            }
            check_attr=sb.toString();
            Log.i("info","-----check_arrt"+check_attr);
        }

        String name=jsonObject.getString("name");
        String title = listright.get(section).getTitle();
        title = title.indexOf(name) == -1 ? title += name + "," : title.replace(name + ",", "");
        listright.get(section).setTitle(title);

//        if(!TextUtils.isEmpty(check_brand) || !TextUtils.isEmpty(check_attr) || !TextUtils.isEmpty(edit_max.getText().toString()) || !TextUtils.isEmpty(edit_mini.getText().toString())){
//            rb_scree.setTextColor(getResources().getColor(R.color.blue_normal));
//        }else {
//            rb_scree.setTextColor(getResources().getColor(R.color.grey_4));
//        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        showUnreadMsgNum();
    }

    private void showUnreadMsgNum() {
        mBadgeView.setBadgeNumber(Integer.valueOf(Login.Companion.getInstance().getUnread_msg_num()));
    }

    //重置
    void clearSelect(){
        for (int i = 0;i<listright.size();i++){
            for (int j = 0;j<listright.get(i).getTagInfo().size();j++){
                listright.get(i).getTagInfo().get(j).setSelect(false);
            }
        }
        edit_mini.setText("");
        edit_max.setText("");
        type_list.clear();
        brand_list.clear();
        check_brand="";
        check_attr="";
        for (int i=0;i<listright.size();i++){
            listright.get(i).setTitle("");
        }
        kyy="";
        list.clear();
        order_keyword="goods_id";
        order="desc";
        getinfo(check_brand,check_attr,order_keyword,order,is_second_hand);
        sortingAdapter.notifyDataSetChanged();
    }

    private void initlode(){
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                smartRefreshLayout.setNoMoreData(false);
                getinfo(check_brand,check_attr,order_keyword,order,is_second_hand);
            }
        });

    }

    private void initrefresh(){
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                smartRefreshLayout.setNoMoreData(false);
                kyy="";
//                edit_mini.setText("");
//                edit_max.setText("");
                list.clear();
                getinfo(check_brand,check_attr,order_keyword,order,is_second_hand);

            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if(messageEvent.getMessage().equals(MessageEvent.KEYWORD)){
            state=0;
            cate_id="";
            keyword=messageEvent.getKeyword();
            is_second_hand=messageEvent.getIs_second_hand();
            searchview.setText(keyword);
            kyy="";
            list.clear();
            check_brand="";
            check_attr="";
            getinfo(check_brand,check_attr,order_keyword,order,is_second_hand);
        }

        if(messageEvent.getMessage().equals(MessageEvent.UNREAD_MSG_NUM)){
            showUnreadMsgNum();
        }
    }

    @Override
    protected void onDestroy() {
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
}
