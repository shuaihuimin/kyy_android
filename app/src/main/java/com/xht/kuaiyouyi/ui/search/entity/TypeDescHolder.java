package com.xht.kuaiyouyi.ui.search.entity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.xht.kuaiyouyi.R;

public class TypeDescHolder extends RecyclerView.ViewHolder {
    public TextView nameView;
    public ImageView imageView;
    public TypeDescHolder(View itemView) {
        super(itemView);
        initView();
    }

    private void initView(){
        nameView=(TextView)itemView.findViewById(R.id.tv_name);
        imageView=(ImageView)itemView.findViewById(R.id.imageView);
    }
}
