package com.xht.kuaiyouyi.ui;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.cart.CartFragment;
import com.xht.kuaiyouyi.ui.enterprise.Fragment_enterprise;
import com.xht.kuaiyouyi.ui.event.IMMessageEvent;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.ui.goods.CartActivity;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.home.Fragment_home;
import com.xht.kuaiyouyi.ui.home.activity.PromotionalVideoActivity;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.activity.ChatActivity;
import com.xht.kuaiyouyi.ui.message.fragment.VerifyMessageFragment;
import com.xht.kuaiyouyi.ui.mine.Fragment_mine;
import com.xht.kuaiyouyi.ui.search.Fragment_type;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.BadgeView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import q.rorbin.badgeview.QBadgeView;


public class MainActivity extends BaseActivity implements CartFragment.OnCartToHomePageListener,GoodsActivity.OnGoodsToHomePageListener {
    @BindView(R.id.radioGroup_fragmenthome)
    RadioGroup radioGroup;
    @BindView(R.id.view_badgeview)
    View view_badgeview;
    @BindView(R.id.rl_main_navigation_button)
    RelativeLayout rl_main_navigation_button;

    private ArrayList<BaseFragment> fragments;
    //缓存Fragment或上次显示的Fragment
    private BaseFragment tempFragment;
    private int position = 0;
    private long exitTime;
    public static final int LOGIN_REQUEST_CODE = 1200;
    //首页或者分类页面
    private int mPrePosition;

    private Fragment_home mFragmentHome;
    private Fragment_type mFragmentType;
    private Fragment_enterprise mFragmenEnterprise;
    private CartFragment mFragmentCart;
    private Fragment_mine mFragmentMine;

    private final String KEY_HOME = "key_home";
    private final String KEY_TYPE = "key_type";
    private final String KEY_ENTERPRISE = "key_enterprise";
    private final String KEY_CART = "key_cart";
    private final String KEY_MINE = "key_mine";


    public static final String KEY_HUAWEI_PUSH_TO_LOGIN = "key_huawei_push_to_login";

    private QBadgeView mBadgeViewNum;

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mFragmentHome = (Fragment_home) getSupportFragmentManager().getFragment(savedInstanceState, KEY_HOME);
            mFragmentType = (Fragment_type) getSupportFragmentManager().getFragment(savedInstanceState, KEY_TYPE);
            mFragmenEnterprise = (Fragment_enterprise) getSupportFragmentManager().getFragment(savedInstanceState, KEY_ENTERPRISE);
            mFragmentCart = (CartFragment) getSupportFragmentManager().getFragment(savedInstanceState, KEY_CART);
            mFragmentMine = (Fragment_mine) getSupportFragmentManager().getFragment(savedInstanceState, KEY_MINE);
        }
        initFragment();
        EventBus.getDefault().register(this);

        //初始化小红点数量提示
        mBadgeViewNum = new BadgeView(this,view_badgeview,10,4f);
        handleHuaweiPushIntent(getIntent());
    }

    private void initFragment() {
        fragments = new ArrayList<>();

        if (mFragmentHome == null) {
            mFragmentHome = new Fragment_home();
        }
        if (mFragmentType == null) {
            mFragmentType = new Fragment_type();
        }
        if (mFragmenEnterprise == null) {
            mFragmenEnterprise = new Fragment_enterprise();
        }
        if (mFragmentCart == null) {
            mFragmentCart = new CartFragment();
        }
        if (mFragmentMine == null) {
            mFragmentMine = new Fragment_mine();
        }

        fragments.add(mFragmentHome);
        fragments.add(mFragmentType);
        fragments.add(mFragmenEnterprise);
        fragments.add(mFragmentCart);
        fragments.add(mFragmentMine);
        initListener();
    }

    private void initListener() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_home: //首页
                        position = 0;
                        break;
                    case R.id.rb_type: //分类
                        position = 1;
                        break;
                    case R.id.rb_enterpriswe: //企业购
                        mPrePosition = position;
                        position = 2;

                        break;
                    case R.id.rb_cart: //购物车
                        mPrePosition = position;
                        position = 3;
                        break;
                    case R.id.rb_mine: //我的
                        mPrePosition = position;
                        position = 4;
                        break;
                }
                BaseFragment baseFragment = getFragment(position);
                switchFragment(tempFragment, baseFragment);

                if (position == 2 || position == 3 || position == 4) {
                    if (!Login.Companion.getInstance().isLogin()) {
                        startActivityForResult(new Intent(MainActivity.this,
                                LoginActivity.class), LOGIN_REQUEST_CODE);
                    }
                }

            }
        });

        //跳到首页fragment
        switchFragment(tempFragment, getFragment(0));
    }

    /**
     * 根据位置得到对应的 Fragment
     *
     * @param position
     * @return
     */
    private BaseFragment getFragment(int position) {
        if (fragments != null && fragments.size() > 0) {
            return fragments.get(position);
        }
        return null;
    }

    /**
     * 切换Fragment
     *
     * @param fragment
     * @param nextFragment
     */
    private void switchFragment(Fragment fragment, BaseFragment nextFragment) {
        if (tempFragment != nextFragment) {
            tempFragment = nextFragment;
            if (nextFragment != null) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                //判断nextFragment是否添加成功
                if (!nextFragment.isAdded()) {
                    //隐藏当前的Fragment
                    if (fragment != null) {
                        transaction.hide(fragment);
                    }
                    //添加Fragment
                    transaction.add(R.id.fragmenthome, nextFragment).commit();
                } else {
                    //隐藏当前Fragment
                    if (fragment != null) {
                        transaction.hide(fragment);
                    }
                    transaction.show(nextFragment).commitAllowingStateLoss();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            moveTaskToBack(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //跳去登录页面，但是没有登录
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            if (mPrePosition == 0) {
                //radioGroup.check(R.id.rb_home); 这个方法会使onCheckedChanged监听回调多次
                ((RadioButton) radioGroup.findViewById(R.id.rb_home)).setChecked(true);
            }
            if (mPrePosition == 1) {
                ((RadioButton) radioGroup.findViewById(R.id.rb_type)).setChecked(true);
            }
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getBooleanExtra(VerifyMessageFragment.TO_COMPANY_INDEX, false)) {
            ((RadioButton) radioGroup.findViewById(R.id.rb_enterpriswe)).setChecked(true);
        }
        if (intent.getBooleanExtra(CartActivity.TO_HOME_PAGE, false)) {
            ((RadioButton) radioGroup.findViewById(R.id.rb_home)).setChecked(true);
        }
        if(intent.getBooleanExtra(GoodsActivity.TO_HOME_ENTERPRISE,false)){
            ((RadioButton) radioGroup.findViewById(R.id.rb_enterpriswe)).setChecked(true);
        }
        handleHuaweiPushIntent(intent);
    }

    /**
     * 从购物车跳转到首页的监听
     */
    @Override
    public void onCartToHomePage() {
        ((RadioButton) radioGroup.findViewById(R.id.rb_home)).setChecked(true);
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {
        if (event.getMessage().equals(MessageEvent.UNREAD_MSG_NUM)) {
            //通知fragment更新小红点角标
            for (BaseFragment fragment : fragments) {
                if (fragment.isAdded()) {
                    fragment.updateUnreadMsg();
                }
            }
        }
        if (event.getMessage().equals(MessageEvent.REFRESH_MAIN_NUM)) {
            if(Integer.valueOf(Login.Companion.getInstance().getMain_num()) > 0){
                mBadgeViewNum.setBadgeText("");
            }else {
                mBadgeViewNum.setBadgeNumber(0);
            }
        }

        if (event.getMessage().equals(MessageEvent.TO_PROMOTIONAL_VIDEO_ACTIVITY)) {
            Intent intent = new Intent(this, PromotionalVideoActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.anim_in,R.anim.anim_out);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(IMMessageEvent event) {
        sendNotification(event.getMessage(),event.getStore_name(),event.getChat_id()
                ,event.getStore_avatar(),event.getStore_id());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Login.Companion.getInstance().isLogin()) {
            if(Integer.valueOf(Login.Companion.getInstance().getMain_num()) > 0){
                mBadgeViewNum.setBadgeText("");
            }else {
                mBadgeViewNum.setBadgeNumber(0);
            }
            Utils.getMainNum();
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mFragmentHome != null && mFragmentHome.isAdded()) {
            getSupportFragmentManager().putFragment(outState, KEY_HOME, mFragmentHome);
        }
        if (mFragmentType != null && mFragmentType.isAdded()) {
            getSupportFragmentManager().putFragment(outState, KEY_TYPE, mFragmentType);
        }
        if (mFragmenEnterprise != null && mFragmenEnterprise.isAdded()) {
            getSupportFragmentManager().putFragment(outState, KEY_ENTERPRISE, mFragmenEnterprise);
        }
        if (mFragmentCart != null && mFragmentCart.isAdded()) {
            getSupportFragmentManager().putFragment(outState, KEY_CART, mFragmentCart);
        }
        if (mFragmentMine != null && mFragmentMine.isAdded()) {
            getSupportFragmentManager().putFragment(outState, KEY_MINE, mFragmentMine);
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onGoodsToHomePage() {
        ((RadioButton) radioGroup.findViewById(R.id.rb_enterpriswe)).setChecked(true);
    }

    private void handleHuaweiPushIntent(Intent intent){
        if(intent.getBooleanExtra(KEY_HUAWEI_PUSH_TO_LOGIN,false)){
            startActivity(new Intent(this,LoginActivity.class));
        }
    }

    private void sendNotification(String message, String store_name,String chat_id,String store_avatar,String store_id) {
        Intent intent = new Intent();
        intent.putExtra(ChatActivity.CHAT_ID,chat_id);
        intent.putExtra(ChatActivity.STORE_AVATAR,store_avatar);
        intent.putExtra(ChatActivity.STORE_NAME,store_name);
        try {
            intent.putExtra(ChatActivity.STORE_ID, Long.parseLong(store_id));
        }catch (Exception e){
            intent.putExtra(ChatActivity.STORE_ID, 0);
        }
        intent.setClass(this, ChatActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) SystemClock.uptimeMillis(), intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this
                ,getString(R.string.app_name))
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.logo))
                .setSmallIcon(R.mipmap.small_icon48)
                .setContentTitle(store_name)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //适配8.0通知的显示
            CharSequence name = getString(R.string.app_name);
            String description = getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(
                    getString(R.string.app_name), name, importance);
            channel.setDescription(description);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(Integer.valueOf(store_id),mBuilder.build());

    }

}
