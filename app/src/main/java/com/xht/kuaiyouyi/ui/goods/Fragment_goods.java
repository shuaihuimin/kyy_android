package com.xht.kuaiyouyi.ui.goods;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.goods.entity.GoodsData;
import com.xht.kuaiyouyi.ui.goods.entity.GoodsGagueData;
import com.xht.kuaiyouyi.ui.home.BannerViewHolder;
import com.xht.kuaiyouyi.ui.search.adapter.SortingAdapter;
import com.xht.kuaiyouyi.ui.search.entity.ScreenBean;
import com.xht.kuaiyouyi.utils.CommonPopupWindow;
import com.xht.kuaiyouyi.utils.CommonUtil;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.widget.AmountView;
import com.zhouwei.mzbanner.MZBannerView;
import com.zhouwei.mzbanner.holder.MZHolderCreator;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class Fragment_goods extends BaseFragment implements SortingAdapter.OnItemClickListener{
    @BindView(R.id.banner_goods)
    MZBannerView mzBannerView;
    @BindView(R.id.tv_content)
    TextView tv_content;
    @BindView(R.id.tv_pricee)
    TextView tv_price;
    @BindView(R.id.tv_sold)
    TextView tv_sold;
    @BindView(R.id.linear_selected)
    LinearLayout linear_selected;
    @BindView(R.id.tv_selected)
    TextView tv_selected;
    @BindView(R.id.tv_fuwu)
    TextView tv_fuwu;
    private RecyclerView recycler_goods;
    private GoodsData.GoodsBean mGoodBean;
    private List<String> listlunbo=new ArrayList<>();

    //筛选规格
    private CommonPopupWindow popupWindow;
    private List<ScreenBean> list=new ArrayList<>();
    private List<String> gaugelist=new ArrayList<>();
    private List<String> goodsspec_list;
    private List<GoodsGagueData> listdata;
    private GoodsGagueData goodsGagueData;
    private SortingAdapter sortingAdapter;
    private String check_gague;
    private ImageView img_goods;
    private ImageView image_clear;
    private TextView tv_gagueprice;
    private TextView tv_goodsid;
    private TextView tv_inventory;
    private TextView tv_gaguecar;
    private TextView tv_gagueshop;
    private AmountView amountView;
    private String goodsid;


    Handler handler=new Handler(){

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:
                    mzBannerView.setDelayedTime(3000);
                    mzBannerView.setPages(listlunbo, new MZHolderCreator<BannerViewHolder>() {
                        @Override
                        public BannerViewHolder createViewHolder() {
                            return new BannerViewHolder();
                        }
                    });
                    mzBannerView.start();
                    tv_content.setText(mGoodBean.getGoods_name());
                    tv_price.setText("¥"+mGoodBean.getGoods_price());
                    tv_sold.setText("已售："+mGoodBean.getGoods_salenum());
                    Log.i("info","--------listimage"+mGoodBean.getGoods_image());
                    break;

                case 2:

                    break;
            }
        }
    };

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        getGoodsinfo();
        click();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_goods_layout;
    }

    //商品数据
    private void getGoodsinfo(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_GOODSDETAILS())
                .addParam("goods_id","100400")
                .withPOST(new NetCallBack<GoodsData>() {

                    @NotNull
                    @Override
                    public Class<GoodsData> getRealType() {
                        return GoodsData.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        Log.i("info","----------------"+err);

                    }

                    @Override
                    public void onSuccess(@NonNull GoodsData goodsData) {
                        if(goodsData!=null){
                            goodsspec_list=new ArrayList<>();
                            mGoodBean=goodsData.getGoods();
                            if(goodsData.getGoods().getGoods_image_mobile()!=null){
                                for (int i=0;i<goodsData.getGoods().getGoods_image_mobile().size();i++){
                                    listlunbo.add(goodsData.getGoods().getGoods_image_mobile().get(i));
                                }
                            }
                            if(goodsData.getGoods().getGoods_spec()!=null){
                                for(int i=0;i<goodsData.getGoods().getGoods_spec().size();i++){
                                    goodsspec_list.add(goodsData.getGoods().getGoods_spec().get(i));
                                }
                            }

                        }
                        handler.sendEmptyMessage(1);
                    }
                },false);
    }

    private void click(){
        linear_selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null && popupWindow.isShowing()) return;
                final View upView = LayoutInflater.from(getActivity()).inflate(R.layout.goods_specifications_layout, null);
                //测量View的宽高
                CommonUtil.measureWidthAndHeight(upView);
                popupWindow = new CommonPopupWindow.Builder(getActivity())
                        .setView(upView)
                        .setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT,  ViewGroup.LayoutParams.WRAP_CONTENT)
                        .setBackGroundLevel(0.8f)//取值范围0.0f-1.0f 值越小越暗
                        .create();
                popupWindow.showAtLocation(upView, Gravity.BOTTOM, 0, 0);
                initpopupwindow(upView);
            }
        });

    }

    //初始化popupwindow上控件
    private void initpopupwindow(View view){
        img_goods=view.findViewById(R.id.img_goods);
        image_clear=view.findViewById(R.id.image_clear);
        tv_gagueprice=view.findViewById(R.id.tv_goodsprice);
        tv_goodsid=view.findViewById(R.id.tv_goods);
        tv_inventory=view.findViewById(R.id.tv_inventory);

        if(mGoodBean!=null ){
            tv_inventory.setText("库存："+mGoodBean.getGoods_storage());
            tv_goodsid.setText("商品编号"+mGoodBean.getGoods_id());
            tv_price.setText("¥"+mGoodBean.getGoods_price());
            if(mGoodBean.getGoods_image()!=null)
            Glide.with(getActivity()).load(mGoodBean.getGoods_image())
                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                    .into(img_goods);
        }

        tv_gaguecar=view.findViewById(R.id.tv_gaguecar);
        tv_gagueshop=view.findViewById(R.id.tv_gagueshop);
        amountView=view.findViewById(R.id.amuntView);
        amountView.setGoods_storage(10);
        amountView.setOnAmountChangeListener(new AmountView.OnAmountChangeListener() {
            @Override
            public void onAmountChange(View view, int amount) {
                ToastU.INSTANCE.showToast(amount+"");
            }
        });

        image_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        tv_gaguecar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setcar();
            }
        });
        tv_gagueshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_GOODSSPEC())
                .addParam("goods_id","100400")
                .withPOST(new NetCallBack<GoodsGagueData>() {

                    @NotNull
                    @Override
                    public Class<GoodsGagueData> getRealType() {
                        return GoodsGagueData.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        Log.i("info","----------------gague"+err);
                    }

                    @Override
                    public void onSuccess(@NonNull GoodsGagueData goodsGagueData) {
                        list.clear();
                        String head_name,head_id,id,name;
                        ScreenBean screenBean2=null;
                        listdata=new ArrayList<>();
                        if(goodsGagueData!=null){
                            listdata.add(goodsGagueData);
                            if(goodsGagueData.getSpec_checked()!=null) {
                                for (int i = 0; i < goodsGagueData.getSpec_checked().size(); i++) {
                                    head_id = goodsGagueData.getSpec_checked().get(i).getId() + "";
                                    head_name = goodsGagueData.getSpec_checked().get(i).getName();
                                    ArrayList<ScreenBean.TagInfo> listtag = new ArrayList<>();
                                    for (int j = 0; j < goodsGagueData.getSpec_checked().get(i).getItem().size(); j++) {
                                        id = goodsGagueData.getSpec_checked().get(i).getItem().get(j).getId() + "";
                                        name = goodsGagueData.getSpec_checked().get(i).getItem().get(j).getName();
                                        ScreenBean.TagInfo tagInfo = new ScreenBean.TagInfo(id, name);
                                        listtag.add(tagInfo);
                                        screenBean2 = new ScreenBean(head_id, head_name, listtag);
                                    }
                                    list.add(screenBean2);
                                }
                                sortingAdapter.notifyDataSetChanged();
                            }
                        }
                    }
               },false);

        if(list!=null && goodsspec_list!=null){
            //设置默认规格
            for(int i=0;i<list.size();i++){
                for(int j=0;j<list.get(i).getTagInfo().size();j++){
                    if(list.get(i).getTagInfo().get(j).equals(goodsspec_list.get(i)))
                        list.get(i).getTagInfo().get(j).setSelect(true);
                        sortingAdapter.notifyDataSetChanged();
                }
            }
        }

    }

    @Override
    public void onItemClick(JSONObject jsonObject) {
        int section = jsonObject.getInteger("section");
        int position = jsonObject.getInteger("position");
        for(int i=0;i<list.get(section).getTagInfo().size();i++){
            if(list.get(section).getTagInfo().get(i).isSelect()==true){
                list.get(section).getTagInfo().get(i).setSelect(false);
                String s=list.get(section).getTagInfo().get(i).getName();
                if(gaugelist!=null)
                gaugelist.remove(s);
            }
        }
        list.get(section).getTagInfo().get(position).setSelect(true);
        String name = list.get(section).getTagInfo().get(position).getName();
        gaugelist.add(name);
        sortingAdapter.notifyDataSetChanged();
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<gaugelist.size();i++){
            if (sb.length() > 0) {
                sb.append("|");
            }
            sb.append(gaugelist.get(i));
        }
        check_gague=sb.toString();
        //更新商品数据
        for(int j=0;j<listdata.get(0).getSpec_all_info().size();j++){
            if(check_gague.equals(listdata.get(0).getSpec_all_info().get(j).getGoods_spec())){
                Glide.with(getActivity())
                        .load(listdata.get(0).getSpec_all_info().get(j).getGoods_image())
                        .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                        .into(img_goods);
                tv_gagueprice.setText(listdata.get(0).getSpec_all_info().get(j).getGoods_promotion_price());
                goodsid=listdata.get(0).getSpec_all_info().get(j).getGoods_id();
                tv_goodsid.setText(listdata.get(0).getSpec_all_info().get(j).getGoods_id());
                tv_inventory.setText("库存"+listdata.get(0).getSpec_all_info().get(j).getGoods_storage());
            }
        }
    }

    //添加购物车
    private void setcar(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ADD_CART())
                .addParam("goods_id","100344")
                .addParam("quantity","1")
                .addParam("bl_id","")
                .withPOST(new NetCallBack<String>() {

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        ToastU.INSTANCE.showToast(err);
                    }

                    @Override
                    public void onSuccess(@NonNull String string) {
                        try {
                            org.json.JSONObject jsonObject=new org.json.JSONObject(string);
                            int err_code=jsonObject.getInt("err_code");
                            String msg=jsonObject.getString("msg");
                            if(err_code==0){
                                ToastU.INSTANCE.showToast("成功加入购物车！");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },false);
    }
}
