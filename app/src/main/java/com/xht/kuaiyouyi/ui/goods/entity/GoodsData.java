package com.xht.kuaiyouyi.ui.goods.entity;

import java.util.List;

public class GoodsData {


    /**
     * goods : {"goods_id":"100635","log_id":true,"goods_spec":[],"spec_name":[],"goods_name":"百年纪念2018春季新款尖头酒杯跟低帮鞋12312","goods_price":"100.00","goods_attr":"http://127.0.0.1/shop/index.php?act=compare&op=goods_info_list&type=2&gid=100635","is_enquiry":"0","goods_marketprice":"200.00","goods_promotion_price":"100.00","goods_storage":100,"goods_salenum":"0","goods_image_mobile":["http://127.0.0.1/data/upload/shop/store/goods/1/1_05872175307322597_360.png"],"goods_image":"http://127.0.0.1/data/upload/shop/store/goods/1/1_05872175307322597_360.png","goods_image_num":1,"is_batch":"1","batch_price":[{"batch":"1-19","price":"100"},{"batch":"20-49","price":"99"},{"batch":"≥50","price":"98"}],"inform_switch":false,"goods_body":"http://127.0.0.1/shop/index.php?act=compare&op=goods_info_list&gid=100635","goods_case":[{"content":"","image":""}]}
     * store : {"store_id":"1","store_name":"快优易自营","store_label":"http://127.0.0.1/data/upload/shop/store/05872276552346277.png","store_phone":"","store_desccredit":"0","store_servicecredit":"0","store_deliverycredit":"0"}
     * remain : null
     * currencytype : {"CNY":{"name":"人民币","symbol":"￥","currency":"CNY","cackeKey":"","rate":0},"HKD":{"name":"港币","symbol":"HK$","currency":"HKD","cackeKey":"cacheCurrencyHKD","rate":1.1504},"MOP":{"name":"葡币","symbol":"MOP$","currency":"MOP","cackeKey":"cacheCurrencyMOP","rate":1.18275},"USD":{"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.1466}}
     */

    private GoodsBean goods;
    private StoreBean store;
    private Object remain;
    private CurrencytypeBean currencytype;

    public GoodsBean getGoods() {
        return goods;
    }

    public void setGoods(GoodsBean goods) {
        this.goods = goods;
    }

    public StoreBean getStore() {
        return store;
    }

    public void setStore(StoreBean store) {
        this.store = store;
    }

    public Object getRemain() {
        return remain;
    }

    public void setRemain(Object remain) {
        this.remain = remain;
    }

    public CurrencytypeBean getCurrencytype() {
        return currencytype;
    }

    public void setCurrencytype(CurrencytypeBean currencytype) {
        this.currencytype = currencytype;
    }

    public static class GoodsBean {
        /**
         * goods_id : 100635
         * log_id : true
         * goods_spec : []
         * spec_name : []
         * goods_name : 百年纪念2018春季新款尖头酒杯跟低帮鞋12312
         * goods_price : 100.00
         * goods_attr : http://127.0.0.1/shop/index.php?act=compare&op=goods_info_list&type=2&gid=100635
         * is_enquiry : 0
         * goods_marketprice : 200.00
         * goods_promotion_price : 100.00
         * goods_storage : 100
         * goods_salenum : 0
         * goods_image_mobile : ["http://127.0.0.1/data/upload/shop/store/goods/1/1_05872175307322597_360.png"]
         * goods_image : http://127.0.0.1/data/upload/shop/store/goods/1/1_05872175307322597_360.png
         * goods_image_num : 1
         * is_batch : 1
         * batch_price : [{"batch":"1-19","price":"100"},{"batch":"20-49","price":"99"},{"batch":"≥50","price":"98"}]
         * inform_switch : false
         * goods_body : http://127.0.0.1/shop/index.php?act=compare&op=goods_info_list&gid=100635
         * goods_case : [{"content":"","image":""}]
         */

        private String goods_id;
        private boolean log_id;
        private String goods_name;
        private double goods_price;
        private String goods_attr;
        private String is_enquiry;
        private String goods_marketprice;
        private String goods_promotion_price;
        private int goods_storage;
        private String goods_salenum;
        private String goods_image;
        private int goods_image_num;
        private String is_batch;
        private boolean inform_switch;
        private String goods_state;
        private String goods_body;
        private List<String> goods_spec;
        private List<String> spec_name;
        private List<String> goods_image_mobile;
        private List<BatchPriceBean> batch_price;
        private List<GoodsCaseBean> goods_case;
        private String goods_share;
        private String goods_share_title;
        private String transport_id;
        private String goods_freight;
        private String goods_min_count;
        private String unit_name;
        private int is_second_hand;


        public int getIs_second_hand() {
            return is_second_hand;
        }

        public void setIs_second_hand(int is_second_hand) {
            this.is_second_hand = is_second_hand;
        }

        public String getUnit_name() {
            return unit_name;
        }

        public void setUnit_name(String unit_name) {
            this.unit_name = unit_name;
        }

        public String getGoods_min_count() {
            return goods_min_count;
        }

        public void setGoods_min_count(String goods_min_count) {
            this.goods_min_count = goods_min_count;
        }

        public String getTransport_id() {
            return transport_id;
        }

        public void setTransport_id(String transport_id) {
            this.transport_id = transport_id;
        }

        public String getGoods_freight() {
            return goods_freight;
        }

        public void setGoods_freight(String goods_freight) {
            this.goods_freight = goods_freight;
        }

        public String getGoods_share() {
            return goods_share;
        }

        public void setGoods_share(String goods_share) {
            this.goods_share = goods_share;
        }

        public String getGoods_share_title() {
            return goods_share_title;
        }

        public void setGoods_share_title(String goods_share_title) {
            this.goods_share_title = goods_share_title;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public boolean isLog_id() {
            return log_id;
        }

        public void setLog_id(boolean log_id) {
            this.log_id = log_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public double getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(double goods_price) {
            this.goods_price = goods_price;
        }

        public String getGoods_attr() {
            return goods_attr;
        }

        public void setGoods_attr(String goods_attr) {
            this.goods_attr = goods_attr;
        }

        public String getIs_enquiry() {
            return is_enquiry;
        }

        public void setIs_enquiry(String is_enquiry) {
            this.is_enquiry = is_enquiry;
        }

        public String getGoods_state() {
            return goods_state;
        }

        public void setGoods_state(String goods_state) {
            this.goods_state = goods_state;
        }

        public String getGoods_marketprice() {
            return goods_marketprice;
        }

        public void setGoods_marketprice(String goods_marketprice) {
            this.goods_marketprice = goods_marketprice;
        }

        public String getGoods_promotion_price() {
            return goods_promotion_price;
        }

        public void setGoods_promotion_price(String goods_promotion_price) {
            this.goods_promotion_price = goods_promotion_price;
        }

        public int getGoods_storage() {
            return goods_storage;
        }

        public void setGoods_storage(int goods_storage) {
            this.goods_storage = goods_storage;
        }

        public String getGoods_salenum() {
            return goods_salenum;
        }

        public void setGoods_salenum(String goods_salenum) {
            this.goods_salenum = goods_salenum;
        }

        public String getGoods_image() {
            return goods_image;
        }

        public void setGoods_image(String goods_image) {
            this.goods_image = goods_image;
        }

        public int getGoods_image_num() {
            return goods_image_num;
        }

        public void setGoods_image_num(int goods_image_num) {
            this.goods_image_num = goods_image_num;
        }

        public String getIs_batch() {
            return is_batch;
        }

        public void setIs_batch(String is_batch) {
            this.is_batch = is_batch;
        }

        public boolean isInform_switch() {
            return inform_switch;
        }

        public void setInform_switch(boolean inform_switch) {
            this.inform_switch = inform_switch;
        }

        public String getGoods_body() {
            return goods_body;
        }

        public void setGoods_body(String goods_body) {
            this.goods_body = goods_body;
        }

        public List<String> getGoods_spec() {
            return goods_spec;
        }

        public void setGoods_spec(List<String> goods_spec) {
            this.goods_spec = goods_spec;
        }

        public List<String> getSpec_name() {
            return spec_name;
        }

        public void setSpec_name(List<String> spec_name) {
            this.spec_name = spec_name;
        }

        public List<String> getGoods_image_mobile() {
            return goods_image_mobile;
        }

        public void setGoods_image_mobile(List<String> goods_image_mobile) {
            this.goods_image_mobile = goods_image_mobile;
        }

        public List<BatchPriceBean> getBatch_price() {
            return batch_price;
        }

        public void setBatch_price(List<BatchPriceBean> batch_price) {
            this.batch_price = batch_price;
        }

        public List<GoodsCaseBean> getGoods_case() {
            return goods_case;
        }

        public void setGoods_case(List<GoodsCaseBean> goods_case) {
            this.goods_case = goods_case;
        }

        public static class BatchPriceBean {
            /**
             * batch : 1-19
             * price : 100
             */

            private String batch;
            private double price;

            public String getBatch() {
                return batch;
            }

            public void setBatch(String batch) {
                this.batch = batch;
            }

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }
        }

        public static class GoodsCaseBean {
            /**
             * content :
             * image :
             */
            private String name;
            private String content;
            private String image;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }
    }

    public static class StoreBean {
        /**
         * store_id : 1
         * store_name : 快优易自营
         * store_label : http://127.0.0.1/data/upload/shop/store/05872276552346277.png
         * store_phone :
         * store_desccredit : 0
         * store_servicecredit : 0
         * store_deliverycredit : 0
         */

        private String store_id;
        private String default_avatar;
        private String default_im;
        private String store_name;
        private String store_label;
        private String store_phone;
        private String store_desccredit;
        private String store_servicecredit;
        private String store_deliverycredit;

        public String getStore_id() {
            return store_id;
        }

        public void setStore_id(String store_id) {
            this.store_id = store_id;
        }

        public String getDefault_avatar() {
            return default_avatar;
        }

        public void setDefault_avatar(String default_avatar) {
            this.default_avatar = default_avatar;
        }

        public String getDefault_im() {
            return default_im;
        }

        public void setDefault_im(String default_im) {
            this.default_im = default_im;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }

        public String getStore_label() {
            return store_label;
        }

        public void setStore_label(String store_label) {
            this.store_label = store_label;
        }

        public String getStore_phone() {
            return store_phone;
        }

        public void setStore_phone(String store_phone) {
            this.store_phone = store_phone;
        }

        public String getStore_desccredit() {
            return store_desccredit;
        }

        public void setStore_desccredit(String store_desccredit) {
            this.store_desccredit = store_desccredit;
        }

        public String getStore_servicecredit() {
            return store_servicecredit;
        }

        public void setStore_servicecredit(String store_servicecredit) {
            this.store_servicecredit = store_servicecredit;
        }

        public String getStore_deliverycredit() {
            return store_deliverycredit;
        }

        public void setStore_deliverycredit(String store_deliverycredit) {
            this.store_deliverycredit = store_deliverycredit;
        }
    }

    public static class CurrencytypeBean {
        /**
         * CNY : {"name":"人民币","symbol":"￥","currency":"CNY","cackeKey":"","rate":0}
         * HKD : {"name":"港币","symbol":"HK$","currency":"HKD","cackeKey":"cacheCurrencyHKD","rate":1.1504}
         * MOP : {"name":"葡币","symbol":"MOP$","currency":"MOP","cackeKey":"cacheCurrencyMOP","rate":1.18275}
         * USD : {"name":"美元","symbol":"US$","currency":"USD","cackeKey":"cacheCurrencyUSD","rate":0.1466}
         */

        private CNYBean CNY;
        private HKDBean HKD;
        private MOPBean MOP;
        private USDBean USD;

        public CNYBean getCNY() {
            return CNY;
        }

        public void setCNY(CNYBean CNY) {
            this.CNY = CNY;
        }

        public HKDBean getHKD() {
            return HKD;
        }

        public void setHKD(HKDBean HKD) {
            this.HKD = HKD;
        }

        public MOPBean getMOP() {
            return MOP;
        }

        public void setMOP(MOPBean MOP) {
            this.MOP = MOP;
        }

        public USDBean getUSD() {
            return USD;
        }

        public void setUSD(USDBean USD) {
            this.USD = USD;
        }

        public static class CNYBean {
            /**
             * name : 人民币
             * symbol : ￥
             * currency : CNY
             * cackeKey :
             * rate : 0
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private int rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public int getRate() {
                return rate;
            }

            public void setRate(int rate) {
                this.rate = rate;
            }
        }

        public static class HKDBean {
            /**
             * name : 港币
             * symbol : HK$
             * currency : HKD
             * cackeKey : cacheCurrencyHKD
             * rate : 1.1504
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class MOPBean {
            /**
             * name : 葡币
             * symbol : MOP$
             * currency : MOP
             * cackeKey : cacheCurrencyMOP
             * rate : 1.18275
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }

        public static class USDBean {
            /**
             * name : 美元
             * symbol : US$
             * currency : USD
             * cackeKey : cacheCurrencyUSD
             * rate : 0.1466
             */

            private String name;
            private String symbol;
            private String currency;
            private String cackeKey;
            private double rate;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCackeKey() {
                return cackeKey;
            }

            public void setCackeKey(String cackeKey) {
                this.cackeKey = cackeKey;
            }

            public double getRate() {
                return rate;
            }

            public void setRate(double rate) {
                this.rate = rate;
            }
        }
    }
}
