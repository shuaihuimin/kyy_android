package com.xht.kuaiyouyi.ui.base;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import butterknife.ButterKnife;

public abstract class BaseActivity extends FragmentActivity {
    protected boolean isTrans;
    private ConnectivityManager manager;

    //设置布局
    protected abstract int getLayout();
    //初始化布局
    protected abstract void initView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(getLayout());
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            initStatusBar(false);
//        }
        //绑定初始化ButterKnife
        ButterKnife.bind(this);
        initView();
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    public void initStatusBar(boolean isTransparent) {
//        Window window = getWindow();
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        if(isTransparent){
//            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
//        }else{
//            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//        }
//        isTrans = isTransparent;
//        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//        window.setStatusBarColor(Color.TRANSPARENT);
//        Utils.FlymeSetStatusBarLightMode(getWindow(),true);
//    }

    //设置打印方法
    public void showToast(String text){
        Toast.makeText(this, text,Toast.LENGTH_SHORT).show();
    }

    /**
     * 自杀
     */
    public void killSelf() {
        finish();
    }

    /**
     * 快捷跳转到另一个Activity，可传递数据
     *
     * @param activityClass 目标
     * @param bundle        数据
     */
    public void goToActivity(Class activityClass, Bundle bundle) {

        Intent intent = new Intent(this, activityClass);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /**
     * 快捷跳转到另一个Activity
     *
     * @param activityClass 目标
     */
    public void goToActivity(Class activityClass) {

        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
    }

    public void closekeyboard(){
        InputMethodManager imm = (InputMethodManager) getSystemService(BaseActivity.this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }

    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config=new Configuration();
        config.setToDefaults();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocales(res.getConfiguration().getLocales());
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(res.getConfiguration().locale);
        }else {
            config.locale = res.getConfiguration().locale;
        }
        res.updateConfiguration(config,res.getDisplayMetrics() );
        return res;
    }

}
