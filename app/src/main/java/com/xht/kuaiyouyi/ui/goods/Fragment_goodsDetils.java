package com.xht.kuaiyouyi.ui.goods;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.event.MessageEvent;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.NestedWebView;
import com.xht.kuaiyouyi.widget.NoScrollWebView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

public class Fragment_goodsDetils extends BaseFragment{
    @BindView(R.id.webview_goodsdetils)
    NestedWebView webView;
    Mobile mobile=new Mobile();
    ViewGroup.LayoutParams layoutParams;
    private String goods_body="",goods_attr="";

    public static Fragment_goodsDetils newInstance(String goods_body){
        Bundle args = new Bundle();
        args.putString("goods_body",goods_body);
        Fragment_goodsDetils fragment_goodsDetils = new Fragment_goodsDetils();
        fragment_goodsDetils.setArguments(args);
        return fragment_goodsDetils;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        goods_body=getArguments().getString("goods_body");
        Log.i("info","-------gooooooo"+goods_body);
        goods_attr=getArguments().getString("goods_attr");
        webView.setDrawingCacheEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setNestedScrollingEnabled(false);
        webView.loadUrl(goods_body);
        webView.setWebViewClient(mClient);
    }
    private WebViewClient mClient = new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
            mobile.onGetWebContentHeight();
        }
    };

    private class Mobile {
        @JavascriptInterface
        public void onGetWebContentHeight() {
            //重新调整webview高度
            webView.post(new Runnable() {
                @Override
                public void run() {
                   // webView.measure(0,0);
                    int hegit=webView.getMeasuredHeight();
                    if(getActivity()!=null){
                        if(hegit<Utils.getWindoHeight(getActivity())){
                                Log.i("info","-------meme2222"+hegit+"--"+Utils.getWindoHeight(getActivity()));
                                layoutParams=  webView.getLayoutParams();
                                layoutParams.height=Utils.getWindoHeight(getActivity())-Utils.dp2px(getActivity(),120);
                                webView.setLayoutParams(layoutParams);
                        }
                    }
                }
            });
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_goodsdetils_layout;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
}
