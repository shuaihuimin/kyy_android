package com.xht.kuaiyouyi.ui.message.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.ui.enterprise.Constant;
import com.xht.kuaiyouyi.ui.enterprise.activity.ApprovalDetailEnterpriseActivity;
import com.xht.kuaiyouyi.ui.message.adapter.WorkNotifyListAdapter;
import com.xht.kuaiyouyi.ui.message.bean.WorkNotifyListBean;
import com.xht.kuaiyouyi.ui.message.util.MessageUtil;
import com.xht.kuaiyouyi.utils.DialogUtils;
import com.xht.kuaiyouyi.widget.DeletePopupWindow;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/**
 * 工作消息    都可以跳转 跳审批详情页
 */
public class WorkNotifyFragment extends BaseFragment{
    @BindView(R.id.lv_content)
    ListView lv_content;
    @BindView(R.id.srl_loadmore)
    SmartRefreshLayout srl_loadmore;
    @BindView(R.id.rl_empty_message_list)
    RelativeLayout rl_empty_message_list;
    @BindView(R.id.error_retry_view)
    LinearLayout error_retry_view;
    @BindView(R.id.bt_load_again)
    Button bt_load_again;

    private String mKyy="";
    private WorkNotifyListBean mWorkNotifyListBean;
    private WorkNotifyListAdapter mAdapter;

    private OnWorkNotifyReadNumUpdate mCallback;

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        srl_loadmore.setEnableRefresh(false);
        mKyy="";
        requestWorkNotifyList();
        lv_content.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mWorkNotifyListBean.getMessage_list().get(position).getIs_read()==0){
                    MessageUtil.requestReadCompanyMessage(mWorkNotifyListBean.getMessage_list().get(position).getId());
                    mCallback.onWorkNotifyReadNumUpdate(1);
                    mWorkNotifyListBean.getMessage_list().get(position).setIs_read(1);
                    mAdapter.notifyDataSetChanged();
                }

                String approve_order_id = mWorkNotifyListBean.getMessage_list().get(position).getApprove_order_id();
                String company_id = mWorkNotifyListBean.getMessage_list().get(position).getCompany_id();
                if(!TextUtils.isEmpty(approve_order_id)&&!TextUtils.isEmpty(company_id)){
                    Bundle bundle = new Bundle();
                    bundle.putString(ApprovalDetailEnterpriseActivity.APPROVE_ORDER_ID,mWorkNotifyListBean.getMessage_list().get(position).getApprove_order_id());
                    bundle.putString(Constant.COMPANY_ID,mWorkNotifyListBean.getMessage_list().get(position).getCompany_id());
                    goToActivity(ApprovalDetailEnterpriseActivity.class,bundle);
                }
            }
        });
        lv_content.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new DeletePopupWindow(getContext(),view).show().setOnClickDeleteListener(new DeletePopupWindow.OnClickDeleteListener() {
                    @Override
                    public void onDelete() {
                        requestDeleteMsg(position);
                    }
                });
                return true;
            }
        });
        srl_loadmore.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                requestWorkNotifyList();
            }
        });
        bt_load_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestWorkNotifyList();
            }
        });
    }

    private void requestDeleteMsg(final int position) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_DELETE_WORK_VERIFY_MSG())
                .addParam("company_message_id",mWorkNotifyListBean.getMessage_list().get(position).getId())
                .withPOST(new NetCallBack<String>() {
                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(getActivity()==null){
                            return;
                        }
                        DialogUtils.createTipImageAndTextDialog(getContext(),
                                getContext().getString(R.string.delete_succeful),
                                R.mipmap.png_icon_popup_ok);
                        mWorkNotifyListBean.getMessage_list().remove(position);
                        mAdapter.notifyDataSetChanged();
                        if(mWorkNotifyListBean.getMessage_list().size()==0){
                            rl_empty_message_list.setVisibility(View.VISIBLE);
                        }
                    }
                }, false);
    }

    private void requestWorkNotifyList() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_WORK_NOTIFY_LIST())
                .withLoadPOST(new NetCallBack<WorkNotifyListBean>() {
                    @NotNull
                    @Override
                    public Class<WorkNotifyListBean> getRealType() {
                        return WorkNotifyListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NotNull String err) {
                        if(getActivity()==null){
                            return;
                        }
                        if(TextUtils.isEmpty(mKyy)){
                            error_retry_view.setVisibility(View.VISIBLE);
                            rl_empty_message_list.setVisibility(View.GONE);
                        }else{
                            Toast.makeText(getContext(), getString(R.string.not_network), Toast.LENGTH_SHORT).show();
                        }
                        if(srl_loadmore.getState() == RefreshState.Loading){
                            srl_loadmore.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull WorkNotifyListBean workNotifyListBean) {
                        if(getActivity()==null){
                            return;
                        }
                        error_retry_view.setVisibility(View.GONE);
                        if(TextUtils.isEmpty(mKyy)){
                            mKyy = workNotifyListBean.getKyy();
                            if(workNotifyListBean.getMessage_list()==null||workNotifyListBean.getMessage_list().size()==0){
                                rl_empty_message_list.setVisibility(View.VISIBLE);
                                return;
                            }
                            srl_loadmore.setEnableLoadMore(true);
                            mWorkNotifyListBean = workNotifyListBean;
                            mAdapter = new WorkNotifyListAdapter(getContext(),mWorkNotifyListBean);
                            lv_content.setAdapter(mAdapter);
                        }else {
                            if(workNotifyListBean.getMessage_list()!=null&&workNotifyListBean.getMessage_list().size()>0){
                                mWorkNotifyListBean.getMessage_list().addAll(workNotifyListBean.getMessage_list());
                                mAdapter.notifyDataSetChanged();
                                mKyy = workNotifyListBean.getKyy();
                            }else {
                                srl_loadmore.setNoMoreData(true);
                            }
                            srl_loadmore.finishLoadMore();
                        }
                    }
                }, false,mKyy);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_message;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnWorkNotifyReadNumUpdate) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnWorkNotifyReadNumUpdate");
        }
    }

    public interface OnWorkNotifyReadNumUpdate{
        //已读数更新
        void onWorkNotifyReadNumUpdate(int num);
    }
}
