package com.xht.kuaiyouyi.ui.mine.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.login.Contant;
import com.xht.kuaiyouyi.utils.Login;

import butterknife.BindView;

public class AccountSecurityActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.relative_modified_phone)
    RelativeLayout mod_phone;
    @BindView(R.id.relative_modify_password)
    RelativeLayout mod_password;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @Override
    protected int getLayout() {
        return R.layout.activity_accountsecurity;
    }

    @Override
    protected void initView() {
        tv_title.setText(R.string.account_security);
        tv_name.setText(Login.Companion.getInstance().getUsername());
        click();
    }

    private void click(){
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountSecurityActivity.this.finish();
            }
        });
        mod_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Contant.TYPE,Contant.TYPE_CHANGE_PHONE);
                goToActivity(AuthenticationActivity.class,bundle);
            }
        });
        mod_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Contant.TYPE,Contant.TYPE_CHANGE_PASSWORD);
                goToActivity(AuthenticationActivity.class,bundle);
            }
        });
    }

}
