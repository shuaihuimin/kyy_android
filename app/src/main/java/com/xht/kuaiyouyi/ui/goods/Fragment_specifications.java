package com.xht.kuaiyouyi.ui.goods;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.base.BaseFragment;
import com.xht.kuaiyouyi.utils.Utils;
import com.xht.kuaiyouyi.widget.NestedWebView;
import com.xht.kuaiyouyi.widget.NoScrollWebView;

import butterknife.BindView;

public class Fragment_specifications extends BaseFragment {
    @BindView(R.id.webview_specification)
    NestedWebView webView;
    private String goods_attr="";
    public static int height;
    Mobile mobile=new Mobile();
    ViewGroup.LayoutParams layoutParams;

    public static Fragment_specifications newInstance(String goods_attr){
        Bundle args = new Bundle();
        args.putString("goods_attr",goods_attr);
        Fragment_specifications fragment = new Fragment_specifications();
        fragment.setArguments(args);
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        goods_attr=getArguments().getString("goods_attr");
        layoutParams=  webView.getLayoutParams();
        layoutParams.height= (int) (Utils.getWindoHeight(getActivity())*1.1);
        webView.setLayoutParams(layoutParams);
        webView.setDrawingCacheEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(goods_attr);
        webView.setWebViewClient(mClient);
        webView.setNestedScrollingEnabled(false);
    }
    private WebViewClient mClient = new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
            mobile.onGetWebContentHeight();
        }
    };

    private class Mobile {
        @JavascriptInterface
        public void onGetWebContentHeight() {
            //重新调整webview高度
            webView.post(new Runnable() {
                @Override
                public void run() {
                    // webView.measure(0,0);
//                    height=webView.getMeasuredHeight();
//                    int lll=Utils.getWindoHeight(getActivity())-Utils.dp2px(getActivity(),120);
//                    Log.i("info","------heheheh33"+height+"---"+lll);
//                    if(height>Utils.getWindoHeight(getActivity())-Utils.dp2px(getActivity(),120)){
//                        Log.i("info","-------hehe333"+height+"--"+Utils.getWindoHeight(getActivity()));
//                        layoutParams.height= ViewGroup.LayoutParams.WRAP_CONTENT;
//                        webView.setLayoutParams(layoutParams);
//                    }
                }
            });
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_specifications_layout;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}
