package com.xht.kuaiyouyi.ui.cart.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.cart.entity.OrderDetilsBean;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.goods.StoreActivity;
import com.xht.kuaiyouyi.ui.message.activity.ChatActivity;
import com.xht.kuaiyouyi.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GoodsListAdapter extends BaseAdapter{
    private Context mContext;
    private OrderDetilsBean.GoodsBean.OrderListBean mgoodsBean;

    public GoodsListAdapter(Context mcontext, OrderDetilsBean.GoodsBean.OrderListBean goodsBean) {
        this.mContext = mcontext;
        this.mgoodsBean = goodsBean;
    }

    @Override
    public int getCount() {
        if(mgoodsBean.getGoods_list()!=null){
            return mgoodsBean.getGoods_list().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mgoodsBean.getGoods_list().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ChildViewHolder childViewHolder;
        if(convertView==null){
            convertView=View.inflate(mContext,R.layout.item_confirmorder_single_goods,null);
            childViewHolder=new ChildViewHolder(convertView);
            convertView.setTag(childViewHolder);
        }else {
            childViewHolder= (ChildViewHolder) convertView.getTag();
        }
        if(position==0){
            childViewHolder.view.setVisibility(View.VISIBLE);
        }else {
            childViewHolder.view.setVisibility(View.GONE);
        }
        childViewHolder.relative_goods_single.setVisibility(View.VISIBLE);
        childViewHolder.relative_afew.setVisibility(View.GONE);
        if(mgoodsBean.getGoods_list().get(position).getIs_second_hand()==1){
            childViewHolder.tv_name.setText(Utils.secondLabel(mgoodsBean.getGoods_list().get(position).getGoods_name()));
        }else {
            childViewHolder.tv_name.setText(mgoodsBean.getGoods_list().get(position).getGoods_name());
        }
        childViewHolder.tv_dollar.setText("¥"+ Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getGoods_list().get(position).getGoods_price())));
        childViewHolder.tv_order_num.setText("x"+mgoodsBean.getGoods_list().get(position).getGoods_num());
        childViewHolder.tv_total_price.setText("¥"+ Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getGoods_list().get(position).getGoods_pay_price())));
        childViewHolder.tv_model.setText(mgoodsBean.getGoods_list().get(position).getGoods_spec());
        Glide.with(mContext).load(mgoodsBean.getGoods_list().get(position).getImage_240_url()).into(childViewHolder.iv_goods_img);
        //运费
        childViewHolder.tv_goods_freight.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getGoods_list().get(position).getFreight_fee())));
        childViewHolder.tv_test_fee.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getGoods_list().get(position).getCustoms_charges_fee())));
        final String goods_id=mgoodsBean.getGoods_list().get(position).getGoods_id();
        childViewHolder.relative_goods_single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("goods_id",goods_id);
                Intent intent=new Intent(mContext, GoodsActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
        if(mgoodsBean.getGoods_list().get(position).getCustoms_charges_fee().equals("0")){
            childViewHolder.tv_test_fee.setVisibility(View.GONE);
            childViewHolder.tv_fee.setVisibility(View.GONE);
        }else {
            childViewHolder.tv_test_fee.setVisibility(View.VISIBLE);
            childViewHolder.tv_fee.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    static class ChildViewHolder {
        @BindView(R.id.view2)
        View view;
        @BindView(R.id.iv_img)
        ImageView iv_goods_img;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_model)
        TextView tv_model;
        @BindView(R.id.tv_dollar)
        TextView tv_dollar;
        @BindView(R.id.tv_order_num)
        TextView tv_order_num;
        @BindView(R.id.tv_total_price)
        TextView tv_total_price;
        @BindView(R.id.tv_goods_num)
        TextView tv_goods_num;
        @BindView(R.id.tv_goods_freight)
        TextView tv_goods_freight;
        @BindView(R.id.tv_test_fee)
        TextView tv_test_fee;
        @BindView(R.id.tv_fee)
        TextView tv_fee;
        @BindView(R.id.relative_freight)
        RelativeLayout relative_freight;
        @BindView(R.id.relative_afew)
        RelativeLayout relative_afew;
        @BindView(R.id.linear_confirmorder)
        LinearLayout linear_confirmorder;
        @BindView(R.id.relative_goods_single)
        RelativeLayout relative_goods_single;

        public ChildViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
