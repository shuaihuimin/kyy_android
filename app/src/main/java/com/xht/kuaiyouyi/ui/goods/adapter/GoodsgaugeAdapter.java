package com.xht.kuaiyouyi.ui.goods.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.search.entity.ScreenBean;
import com.xht.kuaiyouyi.widget.MTagFlowLayout;
import com.xht.kuaiyouyi.widget.MyTagAdapter;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GoodsgaugeAdapter extends BaseAdapter{
    private List<ScreenBean> list;
    private List<String> goodsspec_list;
    private Context context;
    private List<String> typelist=new ArrayList<>();
    private OnItemClickListener mItemClickListener;

    public GoodsgaugeAdapter(List<ScreenBean> list,  List<String> goodsspec_list,Context context) {
        this.list = list;
        this.goodsspec_list=goodsspec_list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView==null){
            convertView=View.inflate(context, R.layout.item_goods_specifications_layout,null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else {
            viewHolder= (ViewHolder) convertView.getTag();
        }
        List<String> datas=new ArrayList<>();
        for(int i=0;i<list.get(position).getTagInfo().size();i++){
            datas.add(list.get(position).getTagInfo().get(i).getName());
        }
        final MyTagAdapter<String> adapter= new MyTagAdapter<String>(datas) {
            @Override
            public View getView(FlowLayout parent, int position, String o) {
                TextView view =new TextView(context);
                view.setText(o);
                view.setBackgroundResource(R.drawable.tag_bg);
                view.setTextColor(R.drawable.text_color);
                return view;

            }
        };
        viewHolder.tagFlowLayout.setAdapter(adapter);
        viewHolder.tagFlowLayout.setMaxSelectCount(1);
        viewHolder.tv_title.setText(list.get(position).getName());
        for(int i=0;i<list.get(position).getTagInfo().size();i++){
            for(int j=0;j<goodsspec_list.size();j++){
                if(list.get(position).getTagInfo().get(i).getName().equals(goodsspec_list.get(j))) {
                    adapter.setSelectedList(i);
                    typelist.add(position,list.get(position).getTagInfo().get(i).getName());
                    adapter.notifyDataChanged();
                }
            }
        }

        viewHolder.tagFlowLayout.setOnSelectListener(new MTagFlowLayout.OnSelectListener() {
            @Override
            public void onSelected(Set<Integer> selectPosSet) {

            }
        });

        final int pot=position;
         viewHolder.tagFlowLayout.setOnTagClickListener(new MTagFlowLayout.OnTagClickListener() {
             @Override
             public boolean onTagClick(View view, int position, FlowLayout parent) {
                 String type=list.get(pot).getTagInfo().get(position).getName();
                 typelist.add(pot,type);
                 JSONObject jsonObject = new JSONObject();
                 jsonObject.put("pot",pot);
                 jsonObject.put("list",type);
                 mItemClickListener.onItemClick(jsonObject);
                 return false;
             }
         });

        return convertView;
    }

    class ViewHolder{
        @BindView(R.id.id_flowlayout)
        MTagFlowLayout tagFlowLayout;
        @BindView(R.id.tv_title)
        TextView tv_title;
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public interface OnItemClickListener{
        void onItemClick(JSONObject jsonObject);
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }
}
