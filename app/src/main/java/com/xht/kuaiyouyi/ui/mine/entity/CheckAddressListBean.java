package com.xht.kuaiyouyi.ui.mine.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 收支票地址列表
 */
public class CheckAddressListBean implements Serializable{

    private List<AddressBean> address;

    public List<AddressBean> getAddress() {
        return address;
    }

    public void setAddress(List<AddressBean> address) {
        this.address = address;
    }

    public static class AddressBean implements Serializable{
        /**
         * address_check_id : 57
         * f_member_id : 1
         * address_check_true_name : 朱俊杰
         * f_area_id : 1437
         * f_city_id : 97
         * f_city_id_2 : 0
         * address_check_area_info : 内蒙古 乌海市 海南区
         * address_check_address : 123123123
         * address_check_phone : 13631224512
         * is_default : 1
         * mobile_zone1 : 86
         * mobile_zone2 : 86
         * tel_zone : 0756
         * tel_phone : 440440
         * address_check_phone_spare : 13631224513
         * mobile_zone1_spare : 86
         */

        private String address_check_id;
        private String f_member_id;
        private String address_check_true_name;
        private String f_area_id;
        private String f_city_id;
        private String f_city_id_2;
        private String address_check_area_info;
        private String address_check_address;
        private String address_check_phone;
        private String is_default;
        private String mobile_zone1;
        private String mobile_zone2;
        private String tel_zone="";
        private String tel_phone;
        private String address_check_phone_spare;
        private String mobile_zone1_spare;

        public String getAddress_check_id() {
            return address_check_id;
        }

        public void setAddress_check_id(String address_check_id) {
            this.address_check_id = address_check_id;
        }

        public String getF_member_id() {
            return f_member_id;
        }

        public void setF_member_id(String f_member_id) {
            this.f_member_id = f_member_id;
        }

        public String getAddress_check_true_name() {
            return address_check_true_name;
        }

        public void setAddress_check_true_name(String address_check_true_name) {
            this.address_check_true_name = address_check_true_name;
        }

        public String getF_area_id() {
            return f_area_id;
        }

        public void setF_area_id(String f_area_id) {
            this.f_area_id = f_area_id;
        }

        public String getF_city_id() {
            return f_city_id;
        }

        public void setF_city_id(String f_city_id) {
            this.f_city_id = f_city_id;
        }

        public String getF_city_id_2() {
            return f_city_id_2;
        }

        public void setF_city_id_2(String f_city_id_2) {
            this.f_city_id_2 = f_city_id_2;
        }

        public String getAddress_check_area_info() {
            return address_check_area_info;
        }

        public void setAddress_check_area_info(String address_check_area_info) {
            this.address_check_area_info = address_check_area_info;
        }

        public String getAddress_check_address() {
            return address_check_address;
        }

        public void setAddress_check_address(String address_check_address) {
            this.address_check_address = address_check_address;
        }

        public String getAddress_check_phone() {
            return address_check_phone;
        }

        public void setAddress_check_phone(String address_check_phone) {
            this.address_check_phone = address_check_phone;
        }

        public String getIs_default() {
            return is_default;
        }

        public void setIs_default(String is_default) {
            this.is_default = is_default;
        }

        public String getMobile_zone1() {
            return mobile_zone1;
        }

        public void setMobile_zone1(String mobile_zone1) {
            this.mobile_zone1 = mobile_zone1;
        }

        public String getMobile_zone2() {
            return mobile_zone2;
        }

        public void setMobile_zone2(String mobile_zone2) {
            this.mobile_zone2 = mobile_zone2;
        }

        public String getTel_zone() {
            return tel_zone;
        }

        public void setTel_zone(String tel_zone) {
            this.tel_zone = tel_zone;
        }

        public String getTel_phone() {
            return tel_phone;
        }

        public void setTel_phone(String tel_phone) {
            this.tel_phone = tel_phone;
        }

        public String getAddress_check_phone_spare() {
            return address_check_phone_spare;
        }

        public void setAddress_check_phone_spare(String address_check_phone_spare) {
            this.address_check_phone_spare = address_check_phone_spare;
        }

        public String getMobile_zone1_spare() {
            return mobile_zone1_spare;
        }

        public void setMobile_zone1_spare(String mobile_zone1_spare) {
            this.mobile_zone1_spare = mobile_zone1_spare;
        }
    }
}
