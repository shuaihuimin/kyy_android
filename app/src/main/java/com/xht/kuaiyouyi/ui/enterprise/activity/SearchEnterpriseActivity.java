package com.xht.kuaiyouyi.ui.enterprise.activity;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.enterprise.adapter.SearchEnterpriseListsAdapter;
import com.xht.kuaiyouyi.ui.enterprise.bean.SearchCompanyBean;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;

public class SearchEnterpriseActivity extends BaseActivity {
    @BindView(R.id.et_search)
    EditText et_enterprise_search;
    @BindView(R.id.tv_cancel)
    TextView tv_cancel;
    @BindView(R.id.lv_enterprise_search_history)
    ListView lv_enterprise_search_history;
    @BindView(R.id.srl_refresh)
    SmartRefreshLayout srl_refresh;
    @BindView(R.id.ll_search_enterprise_empty)
    LinearLayout ll_search_enterprise_empty;

    private String mKyy;
    private List<SearchCompanyBean.CompanyBean> mCompanyBeans;
    private SearchEnterpriseListsAdapter mSearchEnterpriseListsAdapter;


    @Override
    protected int getLayout() {
        return R.layout.activity_search_enterprise;
    }

    @Override
    protected void initView() {
        srl_refresh.setEnableRefresh(false);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        et_enterprise_search.setHint(R.string.search_enterprise_tip);
        et_enterprise_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    String keyword = et_enterprise_search.getText().toString().trim();
                    if(!TextUtils.isEmpty(keyword)){
                        srl_refresh.setNoMoreData(false);
                        mKyy = "";
                        if(mSearchEnterpriseListsAdapter!=null&&mCompanyBeans!=null){
                            mCompanyBeans.clear();
                            mSearchEnterpriseListsAdapter.notifyDataSetChanged();
                        }
                        requestSearchCompany(keyword);
                    }
                }
                return false;
            }
        });
        srl_refresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                srl_refresh.setNoMoreData(false);
                requestSearchCompany("");
            }
        });
    }

    private void requestSearchCompany(String keyword) {
        if(TextUtils.isEmpty(mKyy)){
            DialogUtils.createTipAllLoadDialog(this);
        }
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_EP_SEARCH())
                .addParam("keyword",keyword)
                .withLoadPOST(new NetCallBack<SearchCompanyBean>() {
                    @NotNull
                    @Override
                    public Class<SearchCompanyBean> getRealType() {
                        return SearchCompanyBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        DialogUtils.moven();
                        Toast.makeText(getApplicationContext(),err,Toast.LENGTH_SHORT).show();
                        if(srl_refresh.getState() == RefreshState.Loading){
                            srl_refresh.finishLoadMore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull SearchCompanyBean searchCompanyBean) {
                        if(isFinishing()){
                            return;
                        }
                        if(srl_refresh.getState() == RefreshState.Loading){
                            srl_refresh.finishLoadMore();
                        }
                        if(TextUtils.isEmpty(mKyy)){
                            DialogUtils.moven();
                            mCompanyBeans = searchCompanyBean.getCompany();
                            mKyy = searchCompanyBean.getKyy();
                            if(mCompanyBeans==null || mCompanyBeans.size()==0){
                                ll_search_enterprise_empty.setVisibility(View.VISIBLE);
                            }
                            if(mCompanyBeans!=null && mCompanyBeans.size()>0){
                                ll_search_enterprise_empty.setVisibility(View.GONE);
                                mSearchEnterpriseListsAdapter = new SearchEnterpriseListsAdapter(SearchEnterpriseActivity.this,mCompanyBeans);
                                lv_enterprise_search_history.setAdapter(mSearchEnterpriseListsAdapter);
                            }
                        }else {
                            if(searchCompanyBean.getCompany().size()>0){
                                mKyy = searchCompanyBean.getKyy();
                                mCompanyBeans.addAll(searchCompanyBean.getCompany());
                                mSearchEnterpriseListsAdapter.notifyDataSetChanged();
                            }else {
                                srl_refresh.finishLoadMoreWithNoMoreData();
                                srl_refresh.setEnableFooterFollowWhenLoadFinished(true);
                            }
                        }

                    }
                },false,mKyy);
    }


}
