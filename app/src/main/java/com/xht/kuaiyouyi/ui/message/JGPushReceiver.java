package com.xht.kuaiyouyi.ui.message;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.cart.order.OrderDetilsActivity;
import com.xht.kuaiyouyi.ui.enterprise.Constant;
import com.xht.kuaiyouyi.ui.enterprise.activity.ApprovalDetailEnterpriseActivity;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.message.bean.PushBean;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.Utils;

import cn.jpush.android.api.JPushInterface;

/**
 * 有三种情况，第一种情况是跳转订单详情，第二种是跳转审批详情，第三种是跳转消息中心
 */
public class JGPushReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
            if(Login.Companion.getInstance().isLogin()){
                if(bundle != null){
                    String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
                    String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
                    String title = bundle.getString(JPushInterface.EXTRA_TITLE);
                    Utils.log("JGPushReceiver-",extras+","+message+","+title);
                    sendNotification(context,message,title,extras);
                }
                Utils.getUnreadMsg();
                Utils.getMainNum();
            }
        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {

        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {

        } else {

        }
    }

    private void sendNotification(Context context,String message,String title,String extras) {
        PushBean pushBean = new Gson().fromJson(extras, PushBean.class);
        Intent intent = new Intent();
        if(pushBean!=null&&!TextUtils.isEmpty(pushBean.getOrder_main_id())){
            intent.setClass(context, OrderDetilsActivity.class);
            intent.putExtra("order_main_id",pushBean.getOrder_main_id());
        }else if(pushBean!=null&&!TextUtils.isEmpty(pushBean.getApprove_order_id())&&!TextUtils.isEmpty(pushBean.getCompany_id())){
            intent.setClass(context, ApprovalDetailEnterpriseActivity.class);
            intent.putExtra(ApprovalDetailEnterpriseActivity.APPROVE_ORDER_ID,pushBean.getApprove_order_id());
            intent.putExtra(Constant.COMPANY_ID,pushBean.getCompany_id());
        }else if(pushBean!=null&&!TextUtils.isEmpty(pushBean.getGoods_id())){
            intent.setClass(context, GoodsActivity.class);
            intent.putExtra("goods_id",pushBean.getGoods_id());
        }else {
            intent.setClass(context,MessageActivity.class);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, (int) SystemClock.uptimeMillis(), intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context
                ,context.getString(R.string.app_name))
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),R.mipmap.logo))
                .setSmallIcon(R.mipmap.small_icon48)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //适配8.0通知的显示
            CharSequence name = context.getString(R.string.app_name);
            String description = context.getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(
                    context.getString(R.string.app_name), name, importance);
            channel.setDescription(description);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify((int) System.currentTimeMillis(),mBuilder.build());

    }
}
