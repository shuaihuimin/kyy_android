package com.xht.kuaiyouyi.ui.message.bean;

import java.util.List;

public class SystemNoticeListBean {


    /**
     * message_list : [{"article_id":"43","article_title":"地方规定规范","article_content":"多岁的水电费水电费水电费水电费是水电费水电费水电费水电费等胜多负少","article_time":"1530585335"},{"article_id":"42","article_title":"test","article_content":"的点点滴滴多多多多多多多多多多多多多多多多多","article_time":"1530585136"}]
     * kyy : eyJLIjoia3l5XzAyNSIsIk8iOiIxIiwiUCI6MX0=
     */

    private String kyy;
    private List<MessageListBean> message_list;

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public List<MessageListBean> getMessage_list() {
        return message_list;
    }

    public void setMessage_list(List<MessageListBean> message_list) {
        this.message_list = message_list;
    }

    public static class MessageListBean {
        /**
         * article_id : 43
         * article_title : 地方规定规范
         * article_content : 多岁的水电费水电费水电费水电费是水电费水电费水电费水电费等胜多负少
         * article_time : 1530585335
         */

        private String article_id;
        private String article_title;
        private String article_content;
        private String article_time;
        private String url;

        public String getArticle_id() {
            return article_id;
        }

        public void setArticle_id(String article_id) {
            this.article_id = article_id;
        }

        public String getArticle_title() {
            return article_title;
        }

        public void setArticle_title(String article_title) {
            this.article_title = article_title;
        }

        public String getArticle_content() {
            return article_content;
        }

        public void setArticle_content(String article_content) {
            this.article_content = article_content;
        }

        public String getArticle_time() {
            return article_time;
        }

        public void setArticle_time(String article_time) {
            this.article_time = article_time;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
