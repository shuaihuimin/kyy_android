package com.xht.kuaiyouyi.ui.cart.entity;

import java.util.List;

public class CartIndexBean {

    private List<StoreListBean> store_list;
    private CurrencytypeBean currencytype;

    public List<StoreListBean> getStore_list() {
        return store_list;
    }

    public void setStore_list(List<StoreListBean> store_list) {
        this.store_list = store_list;
    }

    public CurrencytypeBean getCurrencytype() {
        return currencytype;
    }

    public void setCurrencytype(CurrencytypeBean currencytype) {
        this.currencytype = currencytype;
    }

    public static class StoreListBean {
        /**
         * store_id : 1
         * store_name : 快优易自营
         * goods : [{"store_id":"1","goods_name":"劳力士Rolex 日志型系列 116200 63200 自动机械钢带男表联保正品","store_name":"快优易自营","cart_id":"413","spec_name":[],"goods_num":"2","is_enquiry":0,"goods_total":"105,600.00","goods_price":"52800.00","goods_id":"100009","goods_state":1,"goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627958339099_360.jpg"},{"store_id":"1","goods_name":"劳力士Rolex 蚝式恒动系列自动机械钢带男表正品116523-8DI-78593 2","store_name":"快优易自营","cart_id":"414","spec_name":[],"goods_num":"1","is_enquiry":0,"goods_total":"300.00","goods_price":"300.00","goods_id":"100240","goods_state":1,"goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627900055146_360.png"},{"store_id":"1","goods_name":"劳力士Rolex 宇宙计型迪通拿 自动机械皮带男表 正品116519 CR.TB 紅色 1.5","store_name":"快优易自营","cart_id":"420","spec_name":"颜色 尺码","goods_num":"1","is_enquiry":0,"goods_total":"1.00","goods_price":"1.00","goods_id":"100364","goods_state":1,"goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627931531971_360.jpg"},{"store_id":"1","goods_name":"d'f're'g","store_name":"快优易自营","cart_id":"434","spec_name":[],"goods_num":"0","is_enquiry":0,"goods_total":"0.00","goods_price":"200.00","goods_id":"100239","goods_state":0,"goods_image":"http://192.168.0.2/data/upload/shop/common/default_goods_image_360.gif"},{"store_id":"1","goods_name":"欧普手表 春季","store_name":"快优易自营","cart_id":"441","spec_name":"季节","goods_num":"0","is_enquiry":0,"goods_total":"0.00","goods_price":"999.00","goods_id":"100635","goods_state":1,"goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05869658213026872_360.jpg"}]
         * member_id : 1
         */

        private String store_id;
        private String store_name;
        private String member_id;
        private boolean isCheck;
        private List<GoodsBean> goods;

        public String getStore_id() {
            return store_id;
        }

        public void setStore_id(String store_id) {
            this.store_id = store_id;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }

        public String getMember_id() {
            return member_id;
        }

        public void setMember_id(String member_id) {
            this.member_id = member_id;
        }

        public boolean isCheck() {
            return isCheck;
        }

        public void setCheck(boolean check) {
            isCheck = check;
        }

        public List<GoodsBean> getGoods() {
            return goods;
        }

        public void setGoods(List<GoodsBean> goods) {
            this.goods = goods;
        }

        public static class GoodsBean {
            /**
             * store_id : 1
             * goods_name : 劳力士Rolex 日志型系列 116200 63200 自动机械钢带男表联保正品
             * store_name : 快优易自营
             * cart_id : 413
             * spec_name : []
             * goods_num : 2
             * is_enquiry : 0
             * goods_total : 105,600.00
             * goods_price : 52800.00
             * goods_id : 100009
             * goods_state : 1
             * goods_image : http://192.168.0.2/data/upload/shop/store/goods/1/1_04752627958339099_360.jpg
             */

            private String store_id;
            private String goods_name;
            private String store_name;
            private String cart_id;
            private int goods_num;
            private int is_enquiry;
            private double goods_total;
            private double goods_price;
            private int goods_min_count;
            private long goods_id;
            private int goods_state;
            private String goods_image;
            private String spec_name;
            private boolean isCheck;
            private int goods_storage;
            private String is_second_hand;

            public String getIs_second_hand() {
                return is_second_hand;
            }

            public void setIs_second_hand(String is_second_hand) {
                this.is_second_hand = is_second_hand;
            }
            private List<BatchBean> batch;

            public String getStore_id() {
                return store_id;
            }

            public void setStore_id(String store_id) {
                this.store_id = store_id;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getStore_name() {
                return store_name;
            }

            public void setStore_name(String store_name) {
                this.store_name = store_name;
            }

            public String getCart_id() {
                return cart_id;
            }

            public void setCart_id(String cart_id) {
                this.cart_id = cart_id;
            }

            public int getGoods_num() {
                return goods_num;
            }

            public void setGoods_num(int goods_num) {
                this.goods_num = goods_num;
            }

            public int getIs_enquiry() {
                return is_enquiry;
            }

            public void setIs_enquiry(int is_enquiry) {
                this.is_enquiry = is_enquiry;
            }

            public double getGoods_total() {
                return goods_total;
            }

            public void setGoods_total(double goods_total) {
                this.goods_total = goods_total;
            }

            public double getGoods_price() {
                return goods_price;
            }

            public void setGoods_price(double goods_price) {
                this.goods_price = goods_price;
            }

            public int getGoods_min_count() {
                return goods_min_count;
            }

            public void setGoods_min_count(int goods_min_count) {
                this.goods_min_count = goods_min_count;
            }

            public long getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(long goods_id) {
                this.goods_id = goods_id;
            }

            public int getGoods_state() {
                return goods_state;
            }

            public void setGoods_state(int goods_state) {
                this.goods_state = goods_state;
            }

            public String getGoods_image() {
                return goods_image;
            }

            public void setGoods_image(String goods_image) {
                this.goods_image = goods_image;
            }

            public String getSpec_name() {
                return spec_name;
            }

            public void setSpec_name(String spec_name) {
                this.spec_name = spec_name;
            }

            public boolean isCheck() {
                return isCheck;
            }

            public void setCheck(boolean check) {
                isCheck = check;
            }

            public int getGoods_storage() {
                return goods_storage;
            }

            public void setGoods_storage(int goods_storage) {
                this.goods_storage = goods_storage;
            }

            public List<BatchBean> getBatch() {
                return batch;
            }

            public void setBatch(List<BatchBean> batch) {
                this.batch = batch;
            }

            public static class BatchBean{
                private int mix;
                private double value;
                private int max = Integer.MAX_VALUE;

                public int getMix() {
                    return mix;
                }

                public void setMix(int mix) {
                    this.mix = mix;
                }

                public double getValue() {
                    return value;
                }

                public void setValue(double value) {
                    this.value = value;
                }

                public int getMax() {
                    return max;
                }

                public void setMax(int max) {
                    this.max = max;
                }
            }
        }
    }
}
