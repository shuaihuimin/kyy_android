package com.xht.kuaiyouyi.ui.mine.entity;

/**
 * 增加收货地址或者支票地址
 */
public class NewAddressBean {

    /**
     * address_id : 32
     */

    private int address_id;

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }
}
