package com.xht.kuaiyouyi.ui.message.bean;

import java.util.List;

public class WorkNotifyListBean {

    /**
     * message_list : [{"id":"1273","company_id":"60","member_id":"56","to_member_id":"1","data":null,"to_data":"xht_8sh邀请你加入新海通科技有限公司1","status":"1","type":"1","is_read":"1","create_time":"1533541359"},{"id":"198","company_id":"87","member_id":"61","to_member_id":"1","data":"你邀请1加入测试公司","to_data":"xht_zdm邀请你加入测试公司","status":"0","type":"1","is_read":"1","create_time":"1527578896"},{"id":"190","company_id":"60","member_id":"1","to_member_id":"0","data":"【新海通科技有限公司】拒绝了您的加入公司请求","to_data":null,"status":"0","type":"4","is_read":"1","create_time":"1527574140"},{"id":"189","company_id":"60","member_id":"1","to_member_id":"0","data":"【新海通科技有限公司】拒绝了您的加入公司请求","to_data":null,"status":"0","type":"4","is_read":"1","create_time":"1527574139"},{"id":"186","company_id":"60","member_id":"1","to_member_id":"0","data":"您加入【新海通科技有限公司】的请求已通过","to_data":null,"status":"0","type":"3","is_read":"1","create_time":"1527574131"},{"id":"104","company_id":"85","member_id":"1","to_member_id":"0","data":"您加入【我是你的优乐美】的请求已通过","to_data":null,"status":"0","type":"3","is_read":"1","create_time":"1527234826"},{"id":"102","company_id":"85","member_id":"1","to_member_id":"0","data":"您加入【我是你的优乐美】的请求已通过","to_data":null,"status":"0","type":"3","is_read":"1","create_time":"1527234659"},{"id":"100","company_id":"85","member_id":"1","to_member_id":"0","data":"【我是你的优乐美】拒绝了您的加入公司请求","to_data":null,"status":"0","type":"4","is_read":"1","create_time":"1527234600"},{"id":"98","company_id":"85","member_id":"1","to_member_id":"0","data":"您已被移除出【我是你的优乐美】","to_data":null,"status":"0","type":"8","is_read":"1","create_time":"1527234358"},{"id":"96","company_id":"85","member_id":"59","to_member_id":"1","data":"你邀请xht_kyy加入我是你的优乐美","to_data":"grace测试测试测试邀请你加入我是你的优乐美","status":"1","type":"1","is_read":"1","create_time":"1527233899"}]
     * kyy : eyJLIjoia3l5XzAyMyIsIk8iOiIxIiwiUCI6MX0=
     */

    private String kyy;
    private List<MessageListBean> message_list;

    public String getKyy() {
        return kyy;
    }

    public void setKyy(String kyy) {
        this.kyy = kyy;
    }

    public List<MessageListBean> getMessage_list() {
        return message_list;
    }

    public void setMessage_list(List<MessageListBean> message_list) {
        this.message_list = message_list;
    }

    public static class MessageListBean {
        /**
         * id : 1273
         * company_id : 60
         * member_id : 56
         * to_member_id : 1
         * data : null
         * to_data : xht_8sh邀请你加入新海通科技有限公司1
         * status : 1
         * type : 1
         * is_read : 1
         * create_time : 1533541359
         */

        private String id;
        private String company_id;
        private String member_id;
        private String to_member_id;
        private String data;
        private String to_data;
        private int status;
        private int type;
        private int is_read;
        private String create_time;
        private String approve_order_id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCompany_id() {
            return company_id;
        }

        public void setCompany_id(String company_id) {
            this.company_id = company_id;
        }

        public String getMember_id() {
            return member_id;
        }

        public void setMember_id(String member_id) {
            this.member_id = member_id;
        }

        public String getTo_member_id() {
            return to_member_id;
        }

        public void setTo_member_id(String to_member_id) {
            this.to_member_id = to_member_id;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        public String getTo_data() {
            return to_data;
        }

        public void setTo_data(String to_data) {
            this.to_data = to_data;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getIs_read() {
            return is_read;
        }

        public void setIs_read(int is_read) {
            this.is_read = is_read;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getApprove_order_id() {
            return approve_order_id;
        }

        public void setApprove_order_id(String approve_order_id) {
            this.approve_order_id = approve_order_id;
        }
    }
}
