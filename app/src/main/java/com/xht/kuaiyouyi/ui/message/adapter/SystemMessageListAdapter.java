package com.xht.kuaiyouyi.ui.message.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.message.bean.SystemMessageListBean;
import com.xht.kuaiyouyi.utils.Utils;

public class SystemMessageListAdapter extends BaseAdapter {
    private Context mContext;
    private SystemMessageListBean mSystemMessageListBean;

    public SystemMessageListAdapter(Context mContext, SystemMessageListBean systemMessageListBean) {
        this.mContext = mContext;
        this.mSystemMessageListBean = systemMessageListBean;
    }

    @Override
    public int getCount() {
        if(mSystemMessageListBean.getArr()!=null){
            return mSystemMessageListBean.getArr().size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mSystemMessageListBean.getArr().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.item_message_system, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_message = convertView.findViewById(R.id.tv_message);
            viewHolder.tv_time = convertView.findViewById(R.id.tv_time);
            viewHolder.tv_point = convertView.findViewById(R.id.tv_point);
            viewHolder.iv_next = convertView.findViewById(R.id.iv_next);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_time.setText(Utils.getDisplayDataAndTime(mSystemMessageListBean.getArr().get(position).getMessage_time()));
        if(mSystemMessageListBean.getArr().get(position).getMessage_open()==1){
            viewHolder.tv_point.setVisibility(View.GONE);
        }else {
            viewHolder.tv_point.setVisibility(View.VISIBLE);
        }
        viewHolder.tv_message.setText(mSystemMessageListBean.getArr().get(position).getMessage_body());
        if(mSystemMessageListBean.getArr().get(position).getType()==0){
            viewHolder.iv_next.setVisibility(View.INVISIBLE);
        }else {
            viewHolder.iv_next.setVisibility(View.VISIBLE);
        }
        return convertView;
    }



    private class ViewHolder{
        private TextView tv_time,tv_message,tv_point;
        private ImageView iv_next;
    }
}
