package com.xht.kuaiyouyi.ui.cart.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.ui.cart.entity.OrderDetilsBean;
import com.xht.kuaiyouyi.ui.cart.order.CommodityListActivity;
import com.xht.kuaiyouyi.ui.goods.GoodsActivity;
import com.xht.kuaiyouyi.ui.goods.StoreActivity;
import com.xht.kuaiyouyi.ui.login.activity.LoginActivity;
import com.xht.kuaiyouyi.ui.message.activity.ChatActivity;
import com.xht.kuaiyouyi.utils.Login;
import com.xht.kuaiyouyi.utils.ToastU;
import com.xht.kuaiyouyi.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderDetilsAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private OrderDetilsBean.GoodsBean mgoodsBean;

    public OrderDetilsAdapter(Context context, OrderDetilsBean.GoodsBean goodsBean) {
        this.mContext = context;
        this.mgoodsBean = goodsBean;
    }
    @Override
    public int getGroupCount() {
        if (mgoodsBean.getOrder_list() != null && mgoodsBean.getOrder_list().size() > 0) {
            return mgoodsBean.getOrder_list().size();
        }
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (mgoodsBean.getOrder_list().get(groupPosition).getGoods_list() != null ) {
            return 1;
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mgoodsBean.getOrder_list().get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        OrderGoodsAdapter.GroupViewHolder groupViewHolder;
        if(convertView==null){
            convertView = View.inflate(mContext, R.layout.item_confirmorder_goods_head, null);
            groupViewHolder=new OrderGoodsAdapter.GroupViewHolder(convertView);
            convertView.setTag(groupViewHolder);
        }else {
            groupViewHolder= (OrderGoodsAdapter.GroupViewHolder) convertView.getTag();
        }
        groupViewHolder.tv_store_name.setText(mgoodsBean.getOrder_list().get(groupPosition).getStore_name());
        final String store_id=mgoodsBean.getOrder_list().get(groupPosition).getStore_id();
        groupViewHolder.tv_store_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("store_id", store_id);
                Intent intent=new Intent(mContext,StoreActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
        groupViewHolder.tv_customer_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.Companion.getInstance().isLogin()){
                    if(!TextUtils.isEmpty(mgoodsBean.getOrder_list().get(groupPosition).getDefault_im())){
                        Bundle bundle = new Bundle();
                        bundle.putString(ChatActivity.CHAT_ID,mgoodsBean.getOrder_list().get(groupPosition).getDefault_im());
                        bundle.putString(ChatActivity.STORE_AVATAR,mgoodsBean.getOrder_list().get(groupPosition).getDefault_avatar());
                        bundle.putString(ChatActivity.STORE_NAME,mgoodsBean.getOrder_list().get(groupPosition).getStore_name());
                        try {
                            bundle.putLong(ChatActivity.STORE_ID, Long.parseLong(mgoodsBean.getOrder_list().get(groupPosition).getStore_id()));
                        }catch (Exception e){
                            bundle.putLong(ChatActivity.STORE_ID, 0);
                        }
                        Intent intent=new Intent(mContext,ChatActivity.class);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                    }else {
                        ToastU.INSTANCE.showToast(mContext.getString(R.string.no_customer_service));
                    }
                }else {
                    Intent intent=new Intent(mContext,LoginActivity.class);
                    mContext.startActivity(intent);
                }
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder childViewHolder;
        if(convertView==null){
            convertView=View.inflate(mContext,R.layout.item_confirmorder_single_goods,null);
            childViewHolder=new ChildViewHolder(convertView);
            convertView.setTag(childViewHolder);
        }else {
            childViewHolder= (ChildViewHolder) convertView.getTag();
        }
        childViewHolder.view2.setVisibility(View.GONE);
        childViewHolder.view3.setVisibility(View.GONE);
        if(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().size()!=0 && mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().size()>1){
            childViewHolder.relative_goods_single.setVisibility(View.GONE);
            childViewHolder.relative_afew.setVisibility(View.VISIBLE);
            childViewHolder.tv_goods_num.setText(mContext.getString(R.string.gong)+mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().size()+mContext.getString(R.string.jian));
            childViewHolder.linear_confirmorder.removeAllViews();
            for(int i=0;i<mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().size();i++){
                View view=View.inflate(mContext,R.layout.item_confirmorder_afew_goods,null);
                ImageView imageView=view.findViewById(R.id.iv_goods_image);
                Glide.with(mContext)
                        .load(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(i).getImage_240_url())
                        .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                        .into(imageView);
                childViewHolder.linear_confirmorder.addView(view,i);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle=new Bundle();
                        bundle.putInt(CommodityListActivity.FLAG,1);
                        bundle.putString("goods_detils", JSONObject.toJSONString(mgoodsBean.getOrder_list().get(groupPosition)));
                        Intent intent=new Intent(mContext, CommodityListActivity.class);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                    }
                });
            }

        }else {
            childViewHolder.relative_goods_single.setVisibility(View.VISIBLE);
            childViewHolder.relative_afew.setVisibility(View.GONE);
            if(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getIs_second_hand()==1){
                childViewHolder.tv_name.setText(Utils.secondLabel(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getGoods_name()));
            }else {
                childViewHolder.tv_name.setText(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getGoods_name());
            }

            childViewHolder.tv_dollar.setText("¥"+ Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getGoods_price())));
            childViewHolder.tv_order_num.setText("x"+mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getGoods_num());
            childViewHolder.tv_total_price.setText("¥"+ Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getGoods_pay_price())));
            childViewHolder.tv_model.setText(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getGoods_spec());
            Glide.with(mContext)
                    .load(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getImage_240_url())
                    .apply(RequestOptions.placeholderOf(R.mipmap.thumbnail_square))
                    .into(childViewHolder.iv_goods_img);
            //运费
            childViewHolder.tv_goods_freight.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getFreight_fee())));
            if(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getCustoms_charges_fee().equals("0")){
                childViewHolder.tv_test_fee.setVisibility(View.GONE);
                childViewHolder.tv_fee.setVisibility(View.GONE);
            }else {
                childViewHolder.tv_test_fee.setVisibility(View.VISIBLE);
                childViewHolder.tv_fee.setVisibility(View.VISIBLE);
                childViewHolder.tv_test_fee.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getCustoms_charges_fee())));
            }
            childViewHolder.tv_test_fee.setText("¥"+Utils.getDisplayMoney(Double.parseDouble(mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getCustoms_charges_fee())));
            final String goods_id=mgoodsBean.getOrder_list().get(groupPosition).getGoods_list().get(childPosition).getGoods_id();
            childViewHolder.relative_goods_single.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle=new Bundle();
                    bundle.putString("goods_id",goods_id);
                    Intent intent=new Intent(mContext, GoodsActivity.class);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            });
        }
        if(groupPosition==mgoodsBean.getOrder_list().size()-1){
            childViewHolder.view.setVisibility(View.GONE);
            childViewHolder.view4.setVisibility(View.GONE);
        }else{
            childViewHolder.view.setVisibility(View.GONE);
            childViewHolder.view4.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    static class ChildViewHolder {
        @BindView(R.id.iv_img)
        ImageView iv_goods_img;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_model)
        TextView tv_model;
        @BindView(R.id.tv_dollar)
        TextView tv_dollar;
        @BindView(R.id.tv_order_num)
        TextView tv_order_num;
        @BindView(R.id.tv_total_price)
        TextView tv_total_price;
        @BindView(R.id.tv_goods_num)
        TextView tv_goods_num;
        @BindView(R.id.tv_goods_freight)
        TextView tv_goods_freight;
        @BindView(R.id.tv_test_fee)
        TextView tv_test_fee;
        @BindView(R.id.tv_fee)
        TextView tv_fee;
        @BindView(R.id.relative_freight)
        RelativeLayout relative_freight;
        @BindView(R.id.relative_afew)
        RelativeLayout relative_afew;
        @BindView(R.id.linear_confirmorder)
        LinearLayout linear_confirmorder;
        @BindView(R.id.relative_goods_single)
        RelativeLayout relative_goods_single;
        @BindView(R.id.lin)
        View view;
        @BindView(R.id.lin2)
        View view4;
        @BindView(R.id.view2)
        View view2;
        @BindView(R.id.view)
        View view3;

        public ChildViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
