package com.xht.kuaiyouyi.ui.mine.setting;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.kuaiyouyi.R;
import com.xht.kuaiyouyi.api.KyyConstants;
import com.xht.kuaiyouyi.http.NetCallBack;
import com.xht.kuaiyouyi.http.NetUtil;
import com.xht.kuaiyouyi.ui.base.BaseActivity;
import com.xht.kuaiyouyi.ui.message.activity.ChatActivity;
import com.xht.kuaiyouyi.ui.mine.adapter.ServiceHelpAdapter;
import com.xht.kuaiyouyi.ui.mine.entity.ServiceHelpBean;
import com.xht.kuaiyouyi.utils.DialogUtils;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class ServiceHelpActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.lv_content)
    ListView lv_content;
    @BindView(R.id.ll_online_service)
    LinearLayout ll_online_service;
    @BindView(R.id.ll_phone_service)
    LinearLayout ll_phone_service;

    private ServiceHelpAdapter mAdapter;
    private ServiceHelpBean mServiceHelpBean;

    @Override
    protected int getLayout() {
        return R.layout.activity_service_help;
    }

    @Override
    protected void initView() {
        tv_title.setText(R.string.sevices_help);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        requestData();
        lv_content.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        ll_online_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(ChatActivity.class);
            }
        });
        ll_phone_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mServiceHelpBean.getTel())) {

                    DialogUtils.createTwoBtnDialog(ServiceHelpActivity.this,
                            getResources().getString(R.string.kyy_service_phone) + "\n"
                                    + mServiceHelpBean.getTel(), getString(R.string.call),
                            getString(R.string.dialog_cancel), new DialogUtils.OnRightBtnListener() {
                                @Override
                                public void setOnRightListener(Dialog dialog) {
                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                    intent.setData(Uri.parse("tel:" + mServiceHelpBean.getTel()));
                                    if (intent.resolveActivity(getPackageManager()) != null) {
                                        startActivity(intent);
                                    }
                                }
                            }, new DialogUtils.OnLeftBtnListener() {
                                @Override
                                public void setOnLeftListener(Dialog dialog) {

                                }
                            }, false, false);

                }

            }
        });

    }

    private void requestData() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_SERVICE_HELP())
                .withPOST(new NetCallBack<ServiceHelpBean>() {

                    @NotNull
                    @Override
                    public Class<ServiceHelpBean> getRealType() {
                        return ServiceHelpBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        Toast.makeText(ServiceHelpActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(@NonNull ServiceHelpBean serviceHelpBean) {
                        mServiceHelpBean = serviceHelpBean;
                        mAdapter = new ServiceHelpAdapter(ServiceHelpActivity.this, mServiceHelpBean);
                        lv_content.setAdapter(mAdapter);

                    }
                }, false);
    }

}
